﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ProjectSummary
{
    public class MsqlConnection:IDisposable
    {
        //static string ServerName = @"LAPTOP-JIHN657H\SQLEXPRESS2020";

        //static String username = "sa";
        //static String password = "server@123";
        ////static String password = "srks4$master";
        //static String port = "3306";
        //static String DB = "Taegutec_Sales_Budget";

        //public static String ServerName = @"" + ConfigurationManager.AppSettings["ServerName"]; 
        //public static String username = ConfigurationManager.AppSettings["username"]; //sa
                                                                                      
        //public static String password = ConfigurationManager.AppSettings["password"];
        //public static String port = "3306";
        //public static String DB = ConfigurationManager.AppSettings["DB"];

       // public SqlConnection sqlConnection = new SqlConnection(@"Data Source = " + ServerName + ";User ID = " + username + ";Password = " + password + ";Initial Catalog = " + DB + ";Persist Security Info=True");

         static string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
        public SqlConnection sqlConnection = new SqlConnection(connstring);

        public void open()
        {
            if (sqlConnection.State != System.Data.ConnectionState.Open)
                sqlConnection.Open();
        }

        public void close()
        {
            sqlConnection.Close();
        }
        void IDisposable.Dispose()
        { }
    }
}