//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectSummary.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tt_jsr_report_app
    {
        public int ID { get; set; }
        public string FAMILY_ID { get; set; }
        public string FAMILY_NAME { get; set; }
        public string SUBFAMILY_ID { get; set; }
        public string SUBFAMILY_NAME { get; set; }
        public string APPLICATION_CODE { get; set; }
        public string APPLICATION_DESC { get; set; }
        public string GOLD_FLAG { get; set; }
        public string TOP_FLAG { get; set; }
        public string FIVE_YEARS_FLAG { get; set; }
        public string BB_FLAG { get; set; }
        public string SPC_FLAG { get; set; }
        public Nullable<double> SALES_MTD_VALUE { get; set; }
        public Nullable<double> SALES_MTD_VALUE_LY { get; set; }
        public Nullable<double> GROWTH_MONTHLY { get; set; }
        public Nullable<double> BUDGET_MTD { get; set; }
        public Nullable<double> BUDGET_MONTHLY { get; set; }
        public Nullable<double> GP_MONTHLY { get; set; }
        public Nullable<double> SALES_YTD_VALUE { get; set; }
        public Nullable<double> SALES_YTD_VALUE_LY { get; set; }
        public Nullable<double> GROWTH_YEARLY { get; set; }
        public Nullable<double> BUDGET_YTD { get; set; }
        public Nullable<double> BUDGET_YEARLY { get; set; }
        public Nullable<double> GP_YEARLY { get; set; }
        public Nullable<double> SALES_MTD_QTY { get; set; }
        public Nullable<double> SALES_MTD_QTY_LY { get; set; }
        public Nullable<double> SALES_YTD_QTY { get; set; }
        public Nullable<double> SALES_YTD_QTY_LY { get; set; }
        public Nullable<double> GROWTH_YEARLY_QTY { get; set; }
        public string I_T_O_FLAG { get; set; }
        public string TEN_YEARS_FLAG { get; set; }
        public Nullable<double> BUDGET_MTD_QTY { get; set; }
        public Nullable<double> BUDGET_YTD_QTY { get; set; }
    }
}
