﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectSummary.Models
{
    public class ProjectCustomerDetails
    { 
       public decimal txtBusinessExpected { get; set; }
        public string txtCompetitionSpec { get; set; }
        public string txtComponent { get; set; }
        public string txtCustClass { get; set; }
        public decimal txtCustPotential { get; set; }
        public string txtExcaleteTo { get; set; }
        public string txtExistingBrand { get; set; }
        public string txtIndustry { get; set; }
        public string txtOwner { get; set; }
        public decimal txtProjPotential { get; set; }
        public string txtProjType { get; set; }
        public string txtReviewer { get; set; }
        public int txtStages { get; set; }
        public string txtTargetDate { get; set; }
        public string txtsalesEngg { get; set; }
        public decimal txtordertargetcuryear { get; set; }
        public decimal txtordertargetnextyear { get; set; }
        public string txtCompetitioninsert { get; set; }
        public string txttaegutecinsert { get; set; }
        public string txttaeguteccutter { get; set; }
        public string txtProdgroup { get; set; }
        public string txtchannelpartner { get; set; }
        public string Approve { get; set; }
        public string rdBtnTaegutec { get; set; }
        public string marketing { get; set; }
        public string custType { get; set; }
        public string Distributor { get; set; }
        public string MachineMake { get; set; }
        public string MachineType { get; set; }
        public string NoOfMachine { get; set; }
        public string HOIndustry { get; set; }
    }

    public class stagedetails
    {
        public int Stage_Number { get; set; }
        public string Goal { get; set; }
        public string Completion_Date { get; set; }
        public string Remarks { get; set; }
        public string Target_Date { get; set; }
        public string marketing { get; set; }
        public string Order_No { get; set; }
        public string Order_value { get; set; }
        public string ProjectNumber { get; set; }
    }

    public class Inputparam
    {
        public string distributedNo { get; set; }
        public string CustomerNo { get; set; }
    }

    public class customerdet
    {
        public string Customer_no { get; set; }
        public string customer_Name { get; set; }
//        public List<projectdet> projectlist { get; set; }
//public List<stagedetails> stagelist { get; set; }
//        public List<ProjectCustomerDetails> ProjectCustomerlist { get; set; }
    }

    public class projectdet
    {
        public string ProjectNo { get; set; }
        public string ProjectName { get; set; }
    }
}