//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectSummary.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tt_GALF6TA_DL
    {
        public int ID { get; set; }
        public string DLNO { get; set; }
        public string DLKEY { get; set; }
        public string DLDIS { get; set; }
        public string DLBRF { get; set; }
        public string DLVUL { get; set; }
        public string DLTEFF { get; set; }
        public string DLTEFT { get; set; }
    }
}
