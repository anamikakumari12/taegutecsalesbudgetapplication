//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectSummary.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tt_summary_final
    {
        public int id { get; set; }
        public string LINE_DESC { get; set; }
        public string gold_flag { get; set; }
        public string top_flag { get; set; }
        public string five_years_flag { get; set; }
        public string bb_flag { get; set; }
        public string SPC_flag { get; set; }
        public string item_id { get; set; }
        public string item_family_id { get; set; }
        public string item_family_name { get; set; }
        public string item_sub_family_id { get; set; }
        public string item_sub_family_name { get; set; }
        public string item_group_code { get; set; }
        public string insert_or_tool_flag { get; set; }
        public string item_code { get; set; }
        public string item_short_name { get; set; }
        public string item_description { get; set; }
        public string customer_region { get; set; }
        public string sales_qty_year_2 { get; set; }
        public string sales_qty_year_1 { get; set; }
        public string sales_qty_year_0 { get; set; }
        public string estimate_qty_next_year { get; set; }
        public string sales_value_year_2 { get; set; }
        public string sales_value_year_1 { get; set; }
        public string sales_value_year_0 { get; set; }
        public string estimate_value_next_year { get; set; }
        public string display_value { get; set; }
        public string sumFlag { get; set; }
        public string QuantityVariance_sales_qty_year_2 { get; set; }
        public string QuantityVariance_sales_qty_year_1 { get; set; }
        public string QuantityVariance_sales_qty_year_0 { get; set; }
        public string QuantityPercentage_sales_qty_year_2 { get; set; }
        public string QuantityPercentage_sales_qty_year_1 { get; set; }
        public string QuantityPercentage_sales_qty_year_0 { get; set; }
        public string ValuePercentage_sales_value_year_2 { get; set; }
        public string ValuePercentage_sales_value_year_1 { get; set; }
        public string ValuePercentage_sales_value_year_0 { get; set; }
        public string PricePerUnit_sales_value_year_2 { get; set; }
        public string PricePerUnit_sales_value_year_1 { get; set; }
        public string PricePerUnit_sales_value_year_0 { get; set; }
        public string PricePerUnit_sales_value_year_P { get; set; }
        public string priceschange_sales_value_year_2 { get; set; }
        public string priceschange_sales_value_year_1 { get; set; }
        public string priceschange_sales_value_year_0 { get; set; }
        public string ten_years_flag { get; set; }
        public string SFEED_flag { get; set; }
    }
}
