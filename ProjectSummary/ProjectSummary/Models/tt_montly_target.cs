//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectSummary.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tt_montly_target
    {
        public int id { get; set; }
        public string customer_number { get; set; }
        public int target_year { get; set; }
        public int target_month { get; set; }
        public Nullable<decimal> target_month_val { get; set; }
        public Nullable<decimal> open_order_bill { get; set; }
        public Nullable<decimal> fresh_order_bill { get; set; }
        public Nullable<int> save_flag { get; set; }
        public Nullable<int> submit_flag { get; set; }
        public string submitted_by { get; set; }
        public string bm_flag { get; set; }
        public string s_date { get; set; }
    }
}
