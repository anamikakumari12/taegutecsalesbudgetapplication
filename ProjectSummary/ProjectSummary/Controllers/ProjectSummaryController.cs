﻿using Newtonsoft.Json.Converters;
using ProjectSummary.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ProjectSummary.Controllers
{
    public class ProjectSummaryController : Controller
    {
        // GET: ProjectSummary
        Taegutec_Sales_BudgetEntities db = new Taegutec_Sales_BudgetEntities();
        public ActionResult Index(string id, string ProjectNo, string CustomerNo)
        {
            try
            {
                ProjectNo = ProjectNo.Replace(" ", "+");
                //string ProjectNumber1 = Encrypt(ProjectNo);
                //string ProjectNumber2 = Encrypt(CustomerNo);
                //string ProjectNumber3 = Encrypt(id);
                string ProjectNumber = Decrypt(ProjectNo);

                CustomerNo = CustomerNo.Replace(" ", "+");
                string CustomerNumber = Decrypt(CustomerNo);

                id = id.Replace(" ", "+");
                string ID = Decrypt(id);

                ViewBag.reviewerID = ID;
                ViewBag.ProjectNo = ProjectNumber;
                ViewBag.CustomerNo = CustomerNumber;
                ViewBag.ddlcustomerName = db.tt_mdpinfo.Where(m => m.customer_number == CustomerNumber).Select(m => m.customer_name).FirstOrDefault();
                ViewBag.ddlprojectName = db.tt_mdpinfo.Where(m => m.Project_Number == ProjectNumber).Select(m => m.Project_Title).FirstOrDefault();

            }
            catch(Exception ex)
            {
                IntoFile("IndexLog:" + ex.ToString());
            }
            return View();
        }

        //public string Encrypt(string clearText)
        //{
        //    string EncryptionKey = "MAKV2SPBNI99212";
        //    byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        //    using (Aes encryptor = Aes.Create())
        //    {
        //        Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
        //        encryptor.Key = pdb.GetBytes(32);
        //        encryptor.IV = pdb.GetBytes(16);
        //        using (MemoryStream ms = new MemoryStream())
        //        {
        //            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
        //            {
        //                cs.Write(clearBytes, 0, clearBytes.Length);
        //                cs.Close();
        //            }
        //            clearText = Convert.ToBase64String(ms.ToArray());
        //        }
        //    }
        //    return clearText;
        //}

        public JsonResult GetCustomerdet(Inputparam data)
        {
            //List<customerdet> menus = new List<customerdet>();
            List<customerdet> list = new List<customerdet>();

            try
            {
                DataSet dtcust = new DataSet();
                try
                {
                    using (MsqlConnection mc = new MsqlConnection())
                    {
                        using (SqlCommand cmd = new SqlCommand("sp_getCustomers_Projects", mc.sqlConnection))
                        {
                            using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                            {
                                mc.open();
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@Distributor_number", data.distributedNo);
                                cmd.Parameters.AddWithValue("@customer_number", data.CustomerNo);
                                sda.Fill(dtcust);
                                mc.close();
                            }
                        }
                    }
                }
                catch (Exception ex) { }

                string customaer = Convert.ToString(dtcust.Tables[0].Rows[0]["customer_number"]);
                var myData = dtcust.Tables[0].AsEnumerable().Select(r => new customerdet
                {
                    customer_Name = r.Field<string>("customer_number"),
                    Customer_no = r.Field<string>("customer_name"),

                });

                list = myData.ToList();
            }
            catch (Exception ex)
            {

            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProjectdet(string distributed_Name, string custno)
        {
            List<projectdet> list = new List<projectdet>();
            try
            {
                DataSet dtcust = new DataSet();
                try
                {
                    using (MsqlConnection mc = new MsqlConnection())
                    {
                        using (SqlCommand cmd = new SqlCommand("sp_getCustomers_Projects", mc.sqlConnection))
                        {
                            using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                            {
                                mc.open();
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@Distributor_number", distributed_Name);
                                cmd.Parameters.AddWithValue("@customer_number", custno);
                                sda.Fill(dtcust);
                                mc.close();
                            }
                        }
                    }
                }
                catch (Exception ex) { }

                var myData = dtcust.Tables[1].AsEnumerable().Select(r => new projectdet
                {
                    ProjectName = r.Field<string>("Project_title"),
                    ProjectNo = r.Field<string>("Project_Number")
                });
                list = myData.ToList();
            }
            catch (Exception ex)
            {

            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerAndProjects(string ProjectNo, string custno)
        {
            List<ProjectCustomerDetails> list = new List<ProjectCustomerDetails>();
            string res = "";
            string MachineMake = "", MachineType = "", HOIndustry = "", NoOfMachine = "";
            DataTable dtcust = new DataTable();
            DataSet dsProject = new DataSet();

            try
            {

                using (MsqlConnection mc = new MsqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand("getMDPInfo_byCustomer_number", mc.sqlConnection))
                    {
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            mc.open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@customer_number", custno);
                            cmd.Parameters.AddWithValue("@project_number", ProjectNo);
                            sda.Fill(dtcust);
                            mc.close();
                        }
                    }
                }
            }
            catch (Exception ex) {
                IntoFile("GetCustomerAndProjects:" + ex.ToString());
            }

            try
            {
                if (!DBNull.Value.Equals(dtcust.Rows[0]["IsApproved"]) || !DBNull.Value.Equals(dtcust.Rows[0]["IsRejected"]))
                {
                    int approved = Convert.ToInt32(dtcust.Rows[0]["IsApproved"]);
                    int rejected = Convert.ToInt32(dtcust.Rows[0]["IsRejected"]);

                    if (approved == 1 || rejected == 1)
                    {
                        res = "Approved";
                    }
                }
                
                if (!DBNull.Value.Equals(dtcust.Rows[0]["MMake"]))
                {
                    MachineMake = Convert.ToString(dtcust.Rows[0]["MMake"]);
                   
                }

                if (!DBNull.Value.Equals(dtcust.Rows[0]["MType"])) 
                {
                    MachineType = Convert.ToString(dtcust.Rows[0]["MType"]);
                   
                }

                if (!DBNull.Value.Equals(dtcust.Rows[0]["Industry"]))
                {
                    HOIndustry = Convert.ToString(dtcust.Rows[0]["Industry"]);
                    
                }
                if (!DBNull.Value.Equals(dtcust.Rows[0]["NoOfMachine"]))
                {
                    NoOfMachine = Convert.ToString(dtcust.Rows[0]["NoOfMachine"]);
                    
                }

            }
            catch(Exception ex)
            {
                IntoFile("Isapproved exception:" + ex.ToString());
            }
            try
            {
                using (MsqlConnection mc = new MsqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand("sp_getProjectDetails", mc.sqlConnection))
                    {
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            mc.open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@customer_number", custno);
                            cmd.Parameters.AddWithValue("@project_number", ProjectNo);
                            sda.Fill(dsProject);
                            mc.close();
                        }
                    }
                }
            }
            catch (Exception ex) {
                IntoFile("GetCustomerAndProjects in sp_getProjectDetails:" + ex.ToString());
            }

            decimal Business_Expected, OverAll_Potential, Potential_Lakhs, OrderTarget_CY, OrderTarget_NY;
            if (DBNull.Value.Equals(dsProject.Tables[2].Rows[0]["Business_Expected"]))
            {
                Business_Expected = Convert.ToDecimal(00.00);   
            }
            else
            {
                Business_Expected = Convert.ToDecimal(dsProject.Tables[2].Rows[0]["Business_Expected"]);
            }
            if (DBNull.Value.Equals(dsProject.Tables[2].Rows[0]["OverAll_Potential"]))
            {
                OverAll_Potential = Convert.ToDecimal(00.00);
            }
            else
            {
                OverAll_Potential = Convert.ToDecimal(dsProject.Tables[2].Rows[0]["OverAll_Potential"]);
            }
            if (DBNull.Value.Equals(dsProject.Tables[2].Rows[0]["Potential_Lakhs"]))
            {
                Potential_Lakhs = Convert.ToDecimal(00.00);
            }
            else
            {
                Potential_Lakhs = Convert.ToDecimal(dsProject.Tables[2].Rows[0]["Potential_Lakhs"]);
            }
            if (DBNull.Value.Equals(dsProject.Tables[2].Rows[0]["OrderTarget_CY"]))
            {
                OrderTarget_CY = Convert.ToDecimal(00.00);
            }
            else
            {
                OrderTarget_CY = Convert.ToDecimal(dsProject.Tables[2].Rows[0]["OrderTarget_CY"]);
            }
            if (DBNull.Value.Equals(dsProject.Tables[2].Rows[0]["OrderTarget_NY"]))
            {
                OrderTarget_NY = Convert.ToDecimal(00.00);
            }
            else
            {
                OrderTarget_NY = Convert.ToDecimal(dsProject.Tables[2].Rows[0]["OrderTarget_NY"]);
            }
           

            try
            {
                if (dsProject != null)
                {
                    if (dsProject.Tables[2] != null)
                    {
                        if (dsProject.Tables[2].Rows.Count > 0)
                        {
                            var myData = dsProject.Tables[2].AsEnumerable().Select(r => new ProjectCustomerDetails
                            {
                                txtBusinessExpected = Business_Expected,
                                txtCompetitionSpec = r.Field<string>("CompetitorCutter"),
                                txtComponent = r.Field<string>("Component"),
                                txtCustClass = r.Field<string>("customer_class"),
                                txtCustPotential = OverAll_Potential,
                                txtExcaleteTo = r.Field<string>("EscalatedTo"),
                                txtExistingBrand = r.Field<string>("Existing_Product"),
                                txtIndustry = r.Field<string>("sub_industry"),
                                txtOwner = r.Field<string>("Owner"),
                                txtsalesEngg = r.Field<string>("salesEngg"),
                                txtProjPotential = Potential_Lakhs,
                                txtProjType = r.Field<string>("project_type"),
                                txtReviewer = r.Field<string>("Reviewer"),
                                txtStages = r.Field<int>("NoOfStages"),
                                txtTargetDate = r.Field<string>("Main_Target_Date"),
                                txtordertargetcuryear = OrderTarget_CY,
                                txtordertargetnextyear = OrderTarget_NY,
                                txtCompetitioninsert = r.Field<string>("CompetitorInsert"),
                                txttaegutecinsert = r.Field<string>("TaegutecInsert"),
                                txttaeguteccutter = r.Field<string>("TaegutecCutter"),
                                txtProdgroup = r.Field<string>("ProductType"),
                                txtchannelpartner = r.Field<string>("channelpartner"),
                                Approve = res,
                                marketing = r.Field<string>("TechSupport_Flag"),
                                custType = r.Field<string>("cter"),
                                MachineMake = MachineMake,
                                MachineType = MachineType,
                                HOIndustry = HOIndustry,
                                NoOfMachine = NoOfMachine,
                                Distributor = r.Field<string>("distributor"),


                            }); ;
                            list = myData.ToList();

                        }
                    }
                }
            }
            catch(Exception ex)
            {
                IntoFile("GetCustomerAndProjects null exception:" + ex.ToString());
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public string Getcustclass(string custno)
        {
            string custclass = "";
            DataTable dtnewCustomer = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget_CRPData"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("SELECT cls.[ClsName] from [DBVW_DistibutorsCustomers] d, [DBVW_Classfication] cls WHERE  cls.ClsID =  d.ClsID AND d.[CRPID] = " + custno, connection);
                command.CommandType = CommandType.Text;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtnewCustomer);
                //return dtCustomerDetails;
                if (dtnewCustomer.Rows.Count > 0)
                {
                    custclass = dtnewCustomer.Rows[0].ItemArray[0].ToString();
                }
            }
            catch (Exception ex)
            {
                IntoFile("Getcustclass:" + ex.ToString());
            }
            return custclass;
        }

        public JsonResult GetProjectsStages(string customerNo, string projectNo)
        {

            //projectNo = projectNo.Replace(" ", "+");
            //string ProjectNumber = Decrypt(projectNo);

            //customerNo = customerNo.Replace(" ", "+");
            //string CustomerNumber = Decrypt(customerNo);

            List<stagedetails> list = new List<stagedetails>();
            string res = "";

            DataSet dsProject = new DataSet();
            DataTable dtcust = new DataTable();
            DataTable dtcust1 = new DataTable();
            try
            {

                using (MsqlConnection mc = new MsqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand("getMDPInfo_byCustomer_number", mc.sqlConnection))
                    {
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            mc.open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@customer_number", customerNo);
                            cmd.Parameters.AddWithValue("@project_number", projectNo);
                            sda.Fill(dtcust);
                            mc.close();
                        }
                    }
                }
            }
            catch (Exception ex) {
                IntoFile("GetProjectsStages:" + ex.ToString());
            }

            try
            {

                using (MsqlConnection mc = new MsqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand("getMDPStagedetails_byCustomer_Number", mc.sqlConnection))
                    {
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            mc.open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@customer_number", customerNo);
                            cmd.Parameters.AddWithValue("@Stage_Number", '2');
                            cmd.Parameters.AddWithValue("@project_number", projectNo);
                            sda.Fill(dtcust1);
                            mc.close();
                        }
                    }
                }
            }
            catch (Exception ex) {
                IntoFile("getMDPStagedetails_byCustomer_Number :" + ex.ToString());
            }

            try
            {
                using (MsqlConnection mc = new MsqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand("sp_getProjectDetails", mc.sqlConnection))
                    {
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            mc.open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@customer_number", customerNo);
                            cmd.Parameters.AddWithValue("@project_number", projectNo);
                            sda.Fill(dsProject);
                            mc.close();
                        }
                    }
                }
            }
            catch (Exception ex) {
                IntoFile("sp_getProjectDetails :" + ex.ToString());
            }

            string marketting=null, project_number=null;
            if (dtcust.Rows.Count > 0)
            {
                if (DBNull.Value.Equals(dtcust.Rows[0]["TechSupport_Flag"]))
                {
                    marketting = null;
                }
                else
                {
                    marketting = Convert.ToString(dtcust.Rows[0]["TechSupport_Flag"]);
                }
            }
            if (dtcust1.Rows.Count > 0)
            {
                if (DBNull.Value.Equals(dtcust1.Rows[0]["ProjectNumber_ref"]))
                {
                    project_number = null;
                }
                else
                {
                    project_number = Convert.ToString(dtcust1.Rows[0]["ProjectNumber_ref"]);
                }
            }

            try
            {
                if (dsProject != null)
                {
                    if (dsProject.Tables[1].Rows.Count > 0)
                    {
                        for (int i = 0; i <= dsProject.Tables[1].Rows.Count; i++)
                        {
                            var myData = dsProject.Tables[1].AsEnumerable().Select(r => new stagedetails
                            {
                                Stage_Number = r.Field<int>("Stage_Number"),
                                Goal = r.Field<string>("Goal"),
                                Completion_Date = r.Field<string>("Completion_Date"),
                                Remarks = r.Field<string>("Remarks"),
                                Target_Date = r.Field<string>("Target_Date"),
                                Order_No = r.Field<string>("Order_Number"),
                                Order_value = r.Field<string>("Order_Value"),
                                marketing = marketting,
                                ProjectNumber = project_number,
                            });
                            list = myData.ToList();

                        }
                    }

                }
            }
            catch(Exception ex)
            {
                IntoFile("sp_getProjectDetails Exception:" + ex.ToString());
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        public void IntoFile(string Msg)
        {
            try
            {
                //string appPath = @"C:/log.txt";
                string appPath = ConfigurationManager.AppSettings["ErrorLogFile"].ToString();
                using (StreamWriter writer = new StreamWriter(appPath, true)) //true => Append Text
                {
                    writer.WriteLine(System.DateTime.Now + ":  " + Msg + "\r \n");
                }
            }
            catch (Exception e7)
            {
                //ErrorLog(e7);
            }

        }

        public string Approve(string Id, string custno, string projectNo, string remarks)
        {
            string res = "";
            //Id = Id.Replace(" ", "+");
            //string reviewid= Decrypt(Id);

            //projectNo = projectNo.Replace(" ", "+");
            //string ProjectNumber = Decrypt(projectNo);

            //custno = custno.Replace(" ", "+");
            //string CustomerNumber = Decrypt(custno);

            //string reviewid = Base64Decode(Id);
            DataTable dtcust = new DataTable();
            var reviewerdet = db.tt_mdpinfo.Where(m => m.Reviewer == Id && m.customer_number == custno && m.Project_Number == projectNo).FirstOrDefault();
            if (reviewerdet != null)
            {
                try
                {
                    try
                    {

                        using (MsqlConnection mc = new MsqlConnection())
                        {
                            using (SqlCommand cmd = new SqlCommand("getMDPInfo_byCustomer_number", mc.sqlConnection))
                            {
                                using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                                {
                                    mc.open();
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@customer_number", custno);
                                    cmd.Parameters.AddWithValue("@project_number", projectNo);
                                    sda.Fill(dtcust);
                                    mc.close();
                                }
                            }
                        }
                       
                    }
                    catch (Exception ex) {
                        IntoFile("Approve Exception:" + ex.ToString());
                    }

                    string Reviewer = Convert.ToString(dtcust.Rows[0]["Reviewer"]);
                    string salesEngId = Convert.ToString(dtcust.Rows[0]["assigned_salesengineer_id"]);
                    string Escalated_To = Convert.ToString(dtcust.Rows[0]["Escalated_To"]);
                    string techSupportFlag = Convert.ToString(dtcust.Rows[0]["TechSupport_Flag"]);
                    string project_owner = Convert.ToString(dtcust.Rows[0]["project_owner"]);
                    string project_title = Convert.ToString(dtcust.Rows[0]["Project_Title"]);
                    string custname = Convert.ToString(dtcust.Rows[0]["customer_name"]);

                    using (MsqlConnection mc = new MsqlConnection())
                    {
                        using (SqlCommand cmd = new SqlCommand("approve_Project", mc.sqlConnection))
                        {

                            mc.open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@customer_number", custno);
                            cmd.Parameters.AddWithValue("@project_number", projectNo);
                            cmd.Parameters.AddWithValue("@IsSentForApproval", 0);
                            cmd.Parameters.AddWithValue("@IsApproved", 1);
                            cmd.Parameters.AddWithValue("@IsRejected", 0);
                            cmd.Parameters.AddWithValue("@Approval_Remarks", remarks);
                            cmd.ExecuteNonQuery();
                            mc.close();
                            res = "Success";
                        }
                    }
                    if (res == "Success")
                    {
                        SendmailAtApproval(project_owner, Reviewer, Escalated_To, project_title, projectNo, custname, custno, techSupportFlag, salesEngId);
                    }
                }
                catch (Exception ex)
                {
                    IntoFile("Approve:" + ex.ToString());
                }

            }
            else
            {
                res = "fail";
            }
            return res;
        }

        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            cipherText = cipherText.Replace(" ", "+");
            //byte[] cipherBytes = Convert.FromBase64String(cipherText);
            byte[] cipherBytes = System.Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public string Reject(string Id, string custno, string projectNo, string remarks)
        {
            string res = "";
            //projectNo = projectNo.Replace(" ", "+");
            //string ProjectNumber = Decrypt(projectNo);

            //custno = custno.Replace(" ", "+");
            //string CustomerNumber = Decrypt(custno);
            DataSet dtcust = new DataSet();
            try
            {
                try
                {
                    using (MsqlConnection mc = new MsqlConnection())
                    {
                        using (SqlCommand cmd = new SqlCommand("getMDPInfo_byCustomer_number", mc.sqlConnection))
                        {
                            using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                            {
                                mc.open();
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@customer_number", custno);
                                cmd.Parameters.AddWithValue("@project_number", projectNo);
                                sda.Fill(dtcust);
                                mc.close();
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    IntoFile("Reject:" + ex.ToString());
                }

                string Reviewer = Convert.ToString(dtcust.Tables[0].Rows[0]["Reviewer"]);
                string Escalated_To = Convert.ToString(dtcust.Tables[0].Rows[0]["Escalated_To"]);
                string project_owner = Convert.ToString(dtcust.Tables[0].Rows[0]["project_owner"]);
                string project_title = Convert.ToString(dtcust.Tables[0].Rows[0]["Project_Title"]);
                string custname = Convert.ToString(dtcust.Tables[0].Rows[0]["customer_name"]);

                using (MsqlConnection mc = new MsqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand("approve_Project", mc.sqlConnection))
                    {
                        mc.open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@customer_number", custno);
                        cmd.Parameters.AddWithValue("@project_number", projectNo);
                        cmd.Parameters.AddWithValue("@IsSentForApproval", 0);
                        cmd.Parameters.AddWithValue("@IsApproved", 0);
                        cmd.Parameters.AddWithValue("@IsRejected", 1);
                        cmd.Parameters.AddWithValue("@Approval_Remarks", remarks);
                        cmd.ExecuteNonQuery();
                        mc.close();
                        res = "Success";
                    }
                }
                if (res == "Success")
                {
                    SendmailAtRejection(project_owner, Reviewer, Escalated_To, project_title, projectNo, custname, custno, remarks);
                }
            }
            catch (Exception ex)
            {
                IntoFile("Reject exception:" + ex.ToString());
            }
            return res;
        }

        public void SendmailAtApproval(string project_owner, string reviewer, string escalteTo, string project_title, string project_number, string customer_name, string CustomerNumber,string techSupportFlag,string salesEngId)
        {
            //if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            MailMessage email;
            SmtpClient smtpc;
            try
            {
                email = new MailMessage();
                if(techSupportFlag =="Y")
                {
                    email.To.Add(new MailAddress(GetEmail_Id(project_owner))); //Destination Recipient e-mail address.
                    email.CC.Add(new MailAddress(GetEmail_Id(reviewer)));
                    email.CC.Add(new MailAddress(GetEmail_Id(escalteTo)));
                    email.CC.Add(new MailAddress(GetBMEmail_Id(CustomerNumber)));
                    email.CC.Add(new MailAddress(GetEmail_Id(salesEngId)));
                    email.Bcc.Add(new MailAddress("teja@knstek.com"));
                }
                else
                {
                    email.To.Add(new MailAddress(GetEmail_Id(project_owner))); //Destination Recipient e-mail address.
                    email.CC.Add(new MailAddress(GetEmail_Id(reviewer)));
                    email.CC.Add(new MailAddress(GetEmail_Id(escalteTo)));
                    email.Bcc.Add(new MailAddress("teja@knstek.com"));
                }
                email.Subject = "Sales-Budget & Performance Monitoring Project Creation";//Subject for your request
                email.Body = "A project is <b>approved</b> with following details <br/><br/>Project Name: " + project_title + "(" + project_number + ")" + "<br/><br/>Customer Name: " + customer_name + "(" + CustomerNumber + ")" + "<br/><br/> " + "<br/><br/>" + "From" + "<br/>" + "Sales-Budget & Performance Monitoring";
                email.IsBodyHtml = true;

                smtpc = new SmtpClient();
                smtpc.Send(email);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                //objFunc.LogError(ex);
            }
            finally
            {
                email = new MailMessage();
            }
        }

        public void SendmailAtRejection(string project_owner, string reviewer, string escalteTo, string project_title, string project_number, string customer_name, string CustomerNumber, string remarks)
        {
            //if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            MailMessage email;
            SmtpClient smtpc;
            try
            {
                email = new MailMessage();
                email.To.Add(new MailAddress(GetEmail_Id(project_owner))); //Destination Recipient e-mail address.
                email.CC.Add(new MailAddress(GetEmail_Id(reviewer)));
                email.CC.Add(new MailAddress(GetEmail_Id(escalteTo)));
                email.Subject = "Sales-Budget & Performance Monitoring Project Creation";//Subject for your request
                email.Body = "A project is <b>rejected</b> with following details <br/><br/>Rejection Remarks: " + remarks + "<br/><br/>Project Name: " + project_title + "(" + project_number + ")" + "<br/><br/>Customer Name: " + customer_name + "(" + CustomerNumber + ")" + "<br/><br/>" + "<br/><br/>" + "From" + "<br/>" + "Sales-Budget & Performance Monitoring";
                email.IsBodyHtml = true;

                smtpc = new SmtpClient();
                smtpc.Send(email);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                // objFunc.LogError(ex);
            }
            finally
            {
                email = new MailMessage();
            }
        }
        public string GetEmail_Id(string engineer_Id)
        {
            DataTable dtEnggInfo = new DataTable();
            string email = "";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("GetEmail_Id", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Engineer_Id", SqlDbType.VarChar, 10).Value = engineer_Id;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtEnggInfo);
                for (int i = 0; i < dtEnggInfo.Rows.Count; i++)
                {
                    email = dtEnggInfo.Rows[i].ItemArray[0].ToString();
                }
            }
            catch (Exception e)
            {
                //objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return email;


        }

        public string GetBMEmail_Id(string CustomerNumber)
        {
            DataTable dtEnggInfo = new DataTable();
            string email = "";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("GetBM_Email_Id", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@CustomerNumber", SqlDbType.VarChar, 10).Value = CustomerNumber;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtEnggInfo);
                for (int i = 0; i < dtEnggInfo.Rows.Count; i++)
                {
                    email = dtEnggInfo.Rows[i].ItemArray[0].ToString();
                }
            }
            catch (Exception e)
            {
                //objFunc.LogError(e);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return email;


        }
    }
}