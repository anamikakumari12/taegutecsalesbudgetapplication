﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSBA_DataAccessLayer;

namespace TSBA_BusinessAccessLayer
{
    public class ListPriceDownloadBL
    {
        ListPriceDownloadDAL objlistprice = new ListPriceDownloadDAL();
        public DataTable getListPriceDetailsBL()
        {
            return objlistprice.GetListPriceDetails();
        }

        public DataTable getCustomerDetailsBL(string distNum)
        {
            return objlistprice.GetCustomersBasedOnDistributor(distNum);
        }
    }
}
