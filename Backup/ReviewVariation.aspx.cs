﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.IO;
using System.IO.Compression;
using System.Web.Services;
using Ionic.Zip;
using System.Drawing;

namespace TaegutecSalesBudget
{
    public partial class ReviewVariation : System.Web.UI.Page
    {
        Reports objReports = new Reports();
        Budget objBudgets = new Budget();
        Review objRSum = new Review();
        AdminConfiguration objConfig = new AdminConfiguration();
        CommonFunctions objFunc = new CommonFunctions();
        //public static DataTable dtsalesbycust;
        public static string  cust_branchcode, cust_salesEngineerId, prdct_branchcode, prdct_salesEngineerId,cter;
        public static int customerGridLoadStatus, prdctGridLoadStatus;
        public static int? cust_negativeVal, cust_positiveVal, prdct_negativeVal, prdct_positiveVal;
        public static bool custflagValueInThsnd, custflagValueInLakh,
          prdctflagValueInThsnd, prdctflagValueInLakh, cvflagsort, pvflagsort;
        public static float byValueIn;
        public static DataTable dtcustsort, dtdlrsort, dtcustsorttotal, dtdlrsorttotal, dtcustalltotal, dtprdctsort, dtprdctsorttotals;
        public string roleId, strUserId;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            hdnsearch.Value = "";
            hdnsearchpro.Value = "";
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {
                Session["variance_search"] = "";
                Session["var_search"] = "";
                strUserId = Session["UserId"].ToString();
                roleId = Session["RoleId"].ToString();
                if (roleId == "HO")
                {
                    if (Session["cter"] == null )
                    {
                        Session["cter"] = "TTA";
                        cter = "TTA";

                    }
                    if (Session["cter"].ToString() == "DUR")
                    {
                        rdBtnDuraCab.Checked = true;
                        rdBtnTaegutec.Checked = false;
                        cter = "DUR";
                    }
                    else
                    {
                        rdBtnTaegutec.Checked = true;
                        rdBtnDuraCab.Checked = false;
                        cter = "TTA";
                    }
                }
                if (roleId != "HO")
                {
                    cter = null;
                    divCter.Visible = false;
                }
                LoadBranches();
                // ddlBranchList_SelectedIndexChanged(null,null);
                cust_salesEngineerId = "ALL"; cust_branchcode = "ALL";
                prdct_salesEngineerId = "ALL"; prdct_branchcode = "ALL";
                cust_negativeVal = 0; cust_positiveVal = 0; prdct_negativeVal = 0; prdct_positiveVal = 0;
                byValueIn = 1000;
                customerGridLoadStatus = 0;
                prdctGridLoadStatus = 0;
                cvflagsort = false; pvflagsort = false;
                custflagValueInThsnd = true;
                custflagValueInLakh = false;
                prdctflagValueInLakh = false;
                prdctflagValueInThsnd = true;
              
            }
        }
        #region Old Dropdowns code
        //protected void LoadBranches()
        //{
        //    DataTable dtBranchesList = new DataTable();
        //    if (Session["UserId"] != null)
        //    {
        //        if (roleId == "HO")
        //        {

        //            dtBranchesList = objReports.LoadUserInfo("ALL", null);
        //            if (dtBranchesList != null)
        //            {
        //                ddlBranchList.DataSource = dtBranchesList;
        //                ddlBranchList.DataTextField = "region_description";
        //                ddlBranchList.DataValueField = "BranchCode";
        //                ddlBranchList.DataBind();

        //                ddlBranchList.Items.Insert(0, "ALL");
        //                ddlBranchList_SelectedIndexChanged(null, null);
        //            }
        //        }
        //        else if (roleId == "TM")
        //        {
        //            dtBranchesList = objReports.LoadUserInfo(null, null, "ALL", strUserId, null);
        //            if (dtBranchesList != null)
        //            {

        //                ddlBranchList.DataSource = dtBranchesList;
        //                ddlBranchList.DataTextField = "region_description";
        //                ddlBranchList.DataValueField = "BranchCode";
        //                ddlBranchList.DataBind();
        //                ddlBranchList.Items.Insert(0, "ALL");
        //                ddlBranchList_SelectedIndexChanged(null, null);

        //            }

        //        }
        //        else if (roleId == "BM")
        //        {
        //            divBranch.Visible = false;
        //            dtBranchesList.Columns.Add("BranchDesc", typeof(string));
        //            dtBranchesList.Columns.Add("BranchCode", typeof(string));
        //            dtBranchesList.Rows.Add(Session["BranchDesc"], Session["BranchCode"]);
        //            if (dtBranchesList != null)
        //            {
        //                ddlBranchList.DataSource = dtBranchesList;
        //                ddlBranchList.DataTextField = "BranchDesc";
        //                ddlBranchList.DataValueField = "BranchCode";
        //                ddlBranchList.DataBind();
        //                ddlBranchList_SelectedIndexChanged(null, null);
        //                btns.Attributes["class"] = "col-md-3";
        //                divpstv.Attributes["class"] = "col-md-2";

        //            }
        //        }
        //        else if (roleId == "SE")
        //        {
        //            ddlBranchList.DataSource = dtBranchesList;
        //            ddlBranchList.DataTextField = "BranchDesc";
        //            ddlBranchList.DataValueField = "BranchCode";
        //            ddlBranchList.DataBind();
        //            divBranch.Visible = false;
        //            divSalesEngnr.Visible = false;
        //            lblnegtv.Attributes["class"] = "col-md-4 control-label2";

        //        }

        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);



        //    }
        //}
        //protected void ddlBranchList_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtSalesEngDetails = new DataTable();
        //    string branchcode = ddlBranchList.SelectedItem.Value;

        //    if (branchcode == "ALL" && roleId != "TM")
        //    {

        //        dtSalesEngDetails = objReports.LoadUserInfo(null, "ALL");

        //        if (dtSalesEngDetails != null)
        //        {

        //            ddlSalesEngineerList.DataSource = dtSalesEngDetails;
        //            ddlSalesEngineerList.DataTextField = "EngineerName";
        //            ddlSalesEngineerList.DataValueField = "EngineerId";
        //            ddlSalesEngineerList.DataBind();
        //            ddlSalesEngineerList.Items.Insert(0, "SELECT SALES ENGINEER");
        //            ddlSalesEngineerList.Items.Insert(0, "ALL");


        //        }

        //    }
        //    else if (branchcode == "ALL" && roleId == "TM")
        //    {
        //        dtSalesEngDetails = objReports.LoadUserInfo(null, null, null, strUserId, "ALL");

        //        if (dtSalesEngDetails != null)
        //        {

        //            ddlSalesEngineerList.DataSource = dtSalesEngDetails;
        //            ddlSalesEngineerList.DataTextField = "EngineerName";
        //            ddlSalesEngineerList.DataValueField = "EngineerId";
        //            ddlSalesEngineerList.DataBind();
        //            ddlSalesEngineerList.Items.Insert(0, "SELECT SALES ENGINEER");
        //            ddlSalesEngineerList.Items.Insert(0, "ALL");


        //        }

        //    }
        //    else
        //    {

        //        dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);
        //        if (dtSalesEngDetails != null)
        //        {
        //            ddlSalesEngineerList.DataSource = dtSalesEngDetails;
        //            ddlSalesEngineerList.DataTextField = "EngineerName";
        //            ddlSalesEngineerList.DataValueField = "EngineerId";
        //            ddlSalesEngineerList.DataBind();
        //            ddlSalesEngineerList.Items.Insert(0, "SELECT SALES ENGINEER");
        //            ddlSalesEngineerList.Items.Insert(0, "ALL");
        //        }
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //}
        #endregion
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            DataTable dtData = new DataTable();
            objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
            objRSum.roleId = roleId;
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);

            ddlBranchList.DataSource = dtData;
            ddlBranchList.DataTextField = "BranchDesc";
            ddlBranchList.DataValueField = "BranchCode";
            ddlBranchList.DataBind();
            if (roleId == "HO" || roleId == "TM" )
            {
                ddlBranchList.Items.Insert(0, "ALL");
            }
            if(roleId=="BM"){
                ddlBranchList.Items.Insert(0, branchcode);
                //ddlBranchList.DataTextField = branchcode;
                //ddlBranchList.DataValueField = branchcode;
                //ddlBranchList.DataBind();
                //ddlBranchList.ClearSelection();
                //ddlBranchList.SelectedItem.Value = branchcode;
            }
            if (roleId == "SE")
            {
                divBranch.Visible = false;
                divSalesEngnr.Visible = false;
                lblnegtv.Attributes["class"] = "col-md-4 control-label2";
            }
            else
            {
                ddlBranchList_SelectedIndexChanged(null, null);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void ddlBranchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            Session["variance_search"] = "";
            hdnsearch.Value = "";
            hdnsearchpro.Value = "";
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            objRSum.BranchCode = roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL" ? userId : ddlBranchList.SelectedItem.Value;
            if (roleId == "BM")
            {
                divBranch.Visible = false;
                objRSum.BranchCode = branchcode;
                btns.Attributes["class"] = "col-md-3";
                divpstv.Attributes["class"] = "col-md-2 controls";
            }
            if (roleId == "SE")
            {
                divBranch.Visible = false;
                divSalesEngnr.Visible = false;
                lblnegtv.Attributes["class"] = "col-md-4 control-label2";
            }
            objRSum.roleId = roleId;
            objRSum.flag = "SalesEngineer";
            objRSum.cter = cter;
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);

            if (dtData.Rows.Count != 0)
            {
                ddlSalesEngineerList.DataSource = dtData;
                ddlSalesEngineerList.DataTextField = "EngineerName";
                ddlSalesEngineerList.DataValueField = "EngineerId";
                ddlSalesEngineerList.DataBind();
                ddlSalesEngineerList.Items.Insert(0, "ALL");
            }
            else
            {
                ddlSalesEngineerList.DataSource = dtData;
                ddlSalesEngineerList.DataTextField = "EngineerName";
                ddlSalesEngineerList.DataValueField = "EngineerId";
                ddlSalesEngineerList.DataBind();
                ddlSalesEngineerList.Items.Insert(0, "NO SALES ENGINEER");
            }
            Session["var_report"] = null;
            salesbycustomer.DataSource = null;
            salesbycustomer.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }
        #region

        protected void reports_Click(object sender, EventArgs e)
        {
            Session["variance_search"] = "";
            hdnsearch.Value = "";
            hdnsearchpro.Value = "";
            review_variation();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void review_variation()
        {
            try
            {
                strUserId = Session["UserId"].ToString();
                roleId = Session["RoleId"].ToString();
                lbl_nodata.Visible = false;
                if (txtbxPositive.Text == "" && txtbxNegative.Text == "")
                {
                    return;
                }
                ButtonCustExport.Visible = true;
                salesbycustomer.Visible = true;
                // salesbycustomertotals.Visible = true;
                customerGridLoadStatus = 1;
                cust_negativeVal = string.IsNullOrEmpty(txtbxNegative.Text) ? (int?)null : (Convert.ToInt32(txtbxNegative.Text));
                cust_positiveVal = string.IsNullOrEmpty(txtbxPositive.Text) ? (int?)null : Convert.ToInt32(txtbxPositive.Text);
                if (byValueIn == 100000)
                {
                    ValInLakhs.Checked = true;
                }
                else
                {
                    ValInThsnd.Checked = true;
                }
                //cust_branchcode = ddlBranchList.SelectedItem.Value;
                //cust_salesEngineerId = ddlSalesEngineerList.SelectedItem.Value == "ALL" ? null : ddlSalesEngineerList.SelectedItem.Value;
                //cust_branchcode = ddlBranchList.SelectedItem.Value == "ALL" ? null : ddlBranchList.SelectedItem.Value;

                if (roleId == "SE")
                {
                    cust_salesEngineerId = Session["UserId"].ToString();
                    cust_branchcode = Session["BranchCode"].ToString();
                }
                else
                {
                    cust_salesEngineerId = ddlSalesEngineerList.SelectedItem.Value == "ALL" ? null : ddlSalesEngineerList.SelectedItem.Value;
                    cust_branchcode = ddlBranchList.SelectedItem.Value == "ALL" ? null : ddlBranchList.SelectedItem.Value;
                }
                if (roleId == "TM" && cust_branchcode == null)
                {
                    cust_branchcode = strUserId;
                }
                var culture = new CultureInfo("en-us", true)
                {
                    NumberFormat =
                    {
                        NumberGroupSizes = new int[] { 2, 2 }
                    }
                };
                DataTable dttotals = new DataTable();

                dttotals.Columns.Add("cust_name");
                dttotals.Columns.Add("cust_number");
                dttotals.Columns.Add("customer_type");
                dttotals.Columns.Add("Branch");
                dttotals.Columns.Add("EngineerName");
                dttotals.Columns.Add("sales_year_0");
                dttotals.Columns.Add("budget");
                dttotals.Columns.Add("ytdplan");
                dttotals.Columns.Add("ytd");
                dttotals.Columns.Add("variance");
                dttotals.Columns.Add("variancePct", typeof(int));
                DataTable dtcust = new DataTable();
                //if (cust_branchcode != "ALL")
                //{
                /*Case:Customer Toatl*/
                DataTable dtsalesbycust = new DataTable();
                dtsalesbycust = objReports.customer_variation(cust_branchcode, cust_salesEngineerId, -cust_negativeVal, cust_positiveVal, "C", byValueIn, cter);
                dtcustsort = dtsalesbycust.Copy();
                decimal sale = 0, budget = 0, ytdval = 0, variance = 0, variancePRcnt = 0, ytdplan = 0; ;
                for (int i = 0; i < dtsalesbycust.Rows.Count; i++)
                {
                    sale += dtsalesbycust.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[5].ToString());
                    budget += dtsalesbycust.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[6].ToString());
                    ytdplan += dtsalesbycust.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[7].ToString());
                    ytdval += dtsalesbycust.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[8].ToString());
                    variance += dtsalesbycust.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[9].ToString());
                }

                variancePRcnt = ytdplan == 0 ? 0 : (variance / ytdplan) * 100;
                dttotals = dtsalesbycust.Clone();
                dtcustsorttotal = dttotals.Clone();
                bool allCoulmnszero = objReports.DeleteEmptyRows(0, sale, budget, ytdplan, ytdval, variance, variancePRcnt);
                if (allCoulmnszero == false)
                {
                    //dtsalesbycust.Rows.Add("", "", "" , "CUSTOMER TOTAL", "",
                    //                        (Math.Round(sale).ToString("N0", culture)),
                    //                        (Math.Round(budget, 0).ToString("N0", culture)),
                    //                        (Math.Round(ytdplan).ToString("N0", culture)),
                    //                        (Math.Round(ytdval).ToString("N0", culture)),
                    //                        (Math.Round(variance).ToString("N0", culture)),
                    //                        Math.Round(variancePRcnt));

                    dttotals.Rows.Add("", "", "", "CUSTOMER TOTAL", "",
                                           (Math.Round(sale).ToString("N0", culture)),
                                           (Math.Round(budget, 0).ToString("N0", culture)),
                                            (Math.Round(ytdplan).ToString("N0", culture)),
                                           (Math.Round(ytdval).ToString("N0", culture)),
                                           (Math.Round(variance).ToString("N0", culture)),
                                           Math.Round(variancePRcnt));

                    //for sort

                    dtcustsorttotal.Rows.Add("", "", "", "CUSTOMER TOTAL", "",
                                           (Math.Round(sale).ToString("N0", culture)),
                                           (Math.Round(budget, 0).ToString("N0", culture)),
                                            (Math.Round(ytdplan).ToString("N0", culture)),
                                           (Math.Round(ytdval).ToString("N0", culture)),
                                           (Math.Round(variance).ToString("N0", culture)),
                                           Math.Round(variancePRcnt));
                }

                dtcust.Merge(dtsalesbycust);

                /*end*/

                /*Case:Dealer Total*/
                DataTable dtsalesbydlr = new DataTable();
                dtsalesbydlr = objReports.customer_variation(cust_branchcode, cust_salesEngineerId, -cust_negativeVal, cust_positiveVal, "D", byValueIn, cter);
                dtdlrsort = dtsalesbydlr.Copy();
              
                sale = 0; budget = 0; ytdval = 0; variance = 0; variancePRcnt = 0; ytdplan = 0;
                for (int i = 0; i < dtsalesbydlr.Rows.Count; i++)
                {
                    sale += dtsalesbydlr.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[5].ToString());
                    budget += dtsalesbydlr.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[6].ToString());
                    ytdplan += dtsalesbydlr.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[7].ToString());

                    ytdval += dtsalesbydlr.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[8].ToString());
                    variance += dtsalesbydlr.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[9].ToString());

                }

                variancePRcnt = ytdplan == 0 ? 0 : (variance / ytdplan) * 100;
                dtdlrsorttotal = new DataTable();
                dtdlrsorttotal = dttotals.Clone();

                bool allCoulmnszerodlr = objReports.DeleteEmptyRows(0, sale, budget, ytdplan, ytdval, variance, variancePRcnt);
                if (allCoulmnszerodlr == false)
                {
                    //dtsalesbydlr.Rows.Add("", "", "", "CHANNEL PARTNER TOTAL", "",
                                         
                    //                       (Math.Round(sale).ToString("N0", culture)),
                    //                        (Math.Round(budget).ToString("N0", culture)),
                    //                        (Math.Round(ytdplan).ToString("N0", culture)),
                    //                        (Math.Round(ytdval).ToString("N0", culture)),
                    //                        (Math.Round(variance).ToString("N0", culture)),
                    //                        Math.Round(variancePRcnt));
                    dttotals.Rows.Add("", "", "", "CHANNEL PARTNER TOTAL", "",
                                          (Math.Round(sale).ToString("N0", culture)),
                                           (Math.Round(budget).ToString("N0", culture)),
                                             (Math.Round(ytdplan).ToString("N0", culture)),
                                           (Math.Round(ytdval).ToString("N0", culture)),
                                           (Math.Round(variance).ToString("N0", culture)),
                                           Math.Round(variancePRcnt));


                    dtdlrsorttotal.Rows.Add("", "", "", "CHANNEL PARTNER TOTAL", "",
                                          (Math.Round(sale).ToString("N0", culture)),
                                           (Math.Round(budget).ToString("N0", culture)),
                                             (Math.Round(ytdplan).ToString("N0", culture)),
                                           (Math.Round(ytdval).ToString("N0", culture)),
                                           (Math.Round(variance).ToString("N0", culture)),
                                           Math.Round(variancePRcnt));
                    dtcust.Merge(dtsalesbydlr);
                }

                //Grand Total
                sale = 0; budget = 0; ytdval = 0; variance = 0; variancePRcnt = 0; ytdplan = 0;
                int count = 0;
                for (int i = 0; i < dtcust.Rows.Count; i++)
                {
                    if (dtcust.Rows[i].ItemArray[0].ToString() != "CUSTOMER TOTAL" && dtcust.Rows[i].ItemArray[0].ToString() != "CHANNEL PARTNER TOTAL")
                    {
                        sale += dtcust.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[5].ToString());
                        budget += dtcust.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[6].ToString());
                        ytdplan += dtcust.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[7].ToString());

                        ytdval += dtcust.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[8].ToString());
                        variance += dtcust.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[9].ToString());
                    }
                    else
                    {
                        count++;
                    }

                }

                variancePRcnt = ytdplan == 0 ? 0 : (variance / ytdplan) * 100;
                dtcustalltotal = new DataTable();
                dtcustalltotal = dttotals.Clone();

                bool allCoulmnszerototals = objReports.DeleteEmptyRows(0, sale, budget, ytdplan, ytdval, variance, variancePRcnt);
                if (allCoulmnszerototals == false)
                {
                    dttotals.Rows.Add("", "", "", "GRAND TOTAL", "",
                                            (Math.Round(sale).ToString("N0", culture)),
                                            (Math.Round(budget).ToString("N0", culture)),
                                            (Math.Round(ytdplan).ToString("N0", culture)),
                                            (Math.Round(ytdval).ToString("N0", culture)),
                                            (Math.Round(variance).ToString("N0", culture)),
                                            Math.Round(variancePRcnt));
                    dtcust.Rows.Add("", "", "", "GRAND TOTAL", "",
                                           (Math.Round(sale).ToString("N0", culture)),
                                           (Math.Round(budget).ToString("N0", culture)),
                                           (Math.Round(ytdplan).ToString("N0", culture)),
                                           (Math.Round(ytdval).ToString("N0", culture)),
                                           (Math.Round(variance).ToString("N0", culture)),
                                           Math.Round(variancePRcnt));

                    dtcustalltotal.Rows.Add("", "", "", "GRAND TOTAL", "",
                                           (Math.Round(sale).ToString("N0", culture)),
                                           (Math.Round(budget).ToString("N0", culture)),
                                           (Math.Round(ytdplan).ToString("N0", culture)),
                                           (Math.Round(ytdval).ToString("N0", culture)),
                                           (Math.Round(variance).ToString("N0", culture)),
                                           Math.Round(variancePRcnt));
                }
                else
                {
                    //lbl_nodata.Visible = true;
                    ButtonCustExport.Visible = false;
                    string scriptString = "<script type='text/javascript'> alert('No data found for the Variance entered');</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();alert('No data found for the Variance entered');", true);

                }
                if (dtcust.Rows.Count != 0)
                {
                    if (dtcust.Rows.Count > 0)
                    {
                        //dtcust.Rows[(dtcust.Rows.Count) - 1].Delete();
                        //dtcust.AcceptChanges();
                        //dtcust.Rows[(dtcust.Rows.Count) - 1].Delete();
                        //dtcust.AcceptChanges();
                        DataRow dr = dtcust.AsEnumerable().SingleOrDefault(r => r.Field<string>("Branch") == "GRAND TOTAL");
                        if (dr != null)
                        {
                            dr.Delete();
                            dtcust.AcceptChanges();
                        }
                        dr = dtcust.AsEnumerable().SingleOrDefault(r => r.Field<string>("Branch") == "CHANNEL PARTNER TOTAL");
                        if (dr != null)
                        {
                            dr.Delete();
                            dtcust.AcceptChanges();
                        }
                        dr = dtcust.AsEnumerable().SingleOrDefault(r => r.Field<string>("Branch") == "CUSTOMER TOTAL");
                        if (dr != null)
                        {
                            dr.Delete();
                            dtcust.AcceptChanges();
                        }
                        Session["var_report"] = dtcust;
                        salesbycustomer.DataSource = dtcust;
                        salesbycustomer.DataBind();
                        setsessions(dtcustsorttotal, dtdlrsorttotal, dtcustalltotal);
                        // salesbycustomertotals.DataSource = dttotals;
                        // salesbycustomertotals.DataBind();
                        if (ValInThsnd.Checked)
                        {
                            custflagValueInThsnd = true;
                            custflagValueInLakh = false;
                        }
                        else if (ValInLakhs.Checked)
                        {
                            custflagValueInLakh = true;
                            custflagValueInThsnd = false;
                        }

                        //txtbxNegative.Style.Add("border-color", "");
                        //txtbxPositive.Style.Add("border-color", "");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                    }
                }
                else
                {
                    Session["var_report"] = null;
                    salesbycustomer.DataSource = null;
                    salesbycustomer.DataBind();
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        private void setsessions(DataTable dtcustsorttotal, DataTable dtdlrsorttotal, DataTable dtcustalltotal)
        {
            try
            {
                Session["YTD_sale_17"] = 0;
                Session["B"] = 0;
                Session["YTD_plan"] = 0;
                Session["YTD_sale_18"] = 0;
                Session["variance"] = 0;
                Session["variance_prc"] = 0;

                Session["YTD_sale_17_cp"] = 0;
                Session["B_cp"] = 0;
                Session["YTD_plan_cp"] = 0;
                Session["YTD_sale_18_cp"] = 0;
                Session["variance_cp"] = 0;
                Session["variance_prc_cp"] = 0;

                Session["YTD_sale_17_total"] = 0;
                Session["B_total"] = 0;
                Session["YTD_plan_total"] = 0;
                Session["YTD_sale_18_total"] = 0;
                Session["variance_total"] = 0;
                Session["variance_prc_total"] = 0;

                if (dtcustsorttotal.Rows.Count > 0)
                {
                    if (dtcustsorttotal.Rows[0]["sales_year_0"] != DBNull.Value)
                    {
                        Session["YTD_sale_17"] = Convert.ToString(dtcustsorttotal.Rows[0]["sales_year_0"]);
                    }
                    if (dtcustsorttotal.Rows[0]["budget"] != DBNull.Value)
                    {
                        Session["B"] = Convert.ToString(dtcustsorttotal.Rows[0]["budget"]);
                    }
                    if (dtcustsorttotal.Rows[0]["ytdplan"] != DBNull.Value)
                    {
                        Session["YTD_plan"] = Convert.ToString(dtcustsorttotal.Rows[0]["ytdplan"]);
                    }
                    if (dtcustsorttotal.Rows[0]["ytd"] != DBNull.Value)
                    {
                        Session["YTD_sale_18"] = Convert.ToString(dtcustsorttotal.Rows[0]["ytd"]);
                    }
                    if (dtcustsorttotal.Rows[0]["variance"] != DBNull.Value)
                    {
                        Session["variance"] = Convert.ToString(dtcustsorttotal.Rows[0]["variance"]);
                    }
                    if (dtcustsorttotal.Rows[0]["variancePct"] != DBNull.Value)
                    {
                        Session["variance_prc"] = Convert.ToString(dtcustsorttotal.Rows[0]["variancePct"]);
                    }
                }


                //

                if (dtdlrsorttotal.Rows.Count > 0)
                {
                    if (dtdlrsorttotal.Rows[0]["sales_year_0"] != DBNull.Value)
                    {
                        Session["YTD_sale_17_cp"] = Convert.ToString(dtdlrsorttotal.Rows[0]["sales_year_0"]);
                    }
                    if (dtdlrsorttotal.Rows[0]["budget"] != DBNull.Value)
                    {
                        Session["B_cp"] = Convert.ToString(dtdlrsorttotal.Rows[0]["budget"]);
                    }
                    if (dtdlrsorttotal.Rows[0]["ytdplan"] != DBNull.Value)
                    {
                        Session["YTD_plan_cp"] = Convert.ToString(dtdlrsorttotal.Rows[0]["ytdplan"]);
                    }
                    if (dtdlrsorttotal.Rows[0]["ytd"] != DBNull.Value)
                    {
                        Session["YTD_sale_18_cp"] = Convert.ToString(dtdlrsorttotal.Rows[0]["ytd"]);
                    }
                    if (dtdlrsorttotal.Rows[0]["variance"] != DBNull.Value)
                    {
                        Session["variance_cp"] = Convert.ToString(dtdlrsorttotal.Rows[0]["variance"]);
                    }
                    if (dtdlrsorttotal.Rows[0]["variancePct"] != DBNull.Value)
                    {
                        Session["variance_prc_cp"] = Convert.ToString(dtdlrsorttotal.Rows[0]["variancePct"]);
                    }
                }

                // grand total
                if (dtcustalltotal.Rows.Count > 0)
                {
                    if (dtcustalltotal.Rows[0]["sales_year_0"] != DBNull.Value)
                    {
                        Session["YTD_sale_17_total"] = Convert.ToString(dtcustalltotal.Rows[0]["sales_year_0"]);
                    }
                    if (dtcustalltotal.Rows[0]["budget"] != DBNull.Value)
                    {
                        Session["B_total"] = Convert.ToString(dtcustalltotal.Rows[0]["budget"]);
                    }
                    if (dtcustalltotal.Rows[0]["ytdplan"] != DBNull.Value)
                    {
                        Session["YTD_plan_total"] = Convert.ToString(dtcustalltotal.Rows[0]["ytdplan"]);
                    }
                    if (dtcustalltotal.Rows[0]["ytd"] != DBNull.Value)
                    {
                        Session["YTD_sale_18_total"] = Convert.ToString(dtcustalltotal.Rows[0]["ytd"]);
                    }
                    if (dtcustalltotal.Rows[0]["variance"] != DBNull.Value)
                    {
                        Session["variance_total"] = Convert.ToString(dtcustalltotal.Rows[0]["variance"]);
                    }
                    if (dtcustalltotal.Rows[0]["variancePct"] != DBNull.Value)
                    {
                        Session["variance_prc_total"] = Convert.ToString(dtcustalltotal.Rows[0]["variancePct"]);
                    }
                }

                hdnYTD_sale_17.Value = Convert.ToString(Session["YTD_sale_17"]);
                hdnB.Value = Convert.ToString(Session["B"]);
                hdnYTD_plan.Value = Convert.ToString(Session["YTD_plan"]);
                hdnYTD_sale_18.Value = Convert.ToString(Session["YTD_sale_18"]);
                hdnvariance.Value = Convert.ToString(Session["variance"]);
                hdnvariance_prc.Value = Convert.ToString(Session["variance_prc"]);


                hdnYTD_sale_17_cp.Value = Convert.ToString(Session["YTD_sale_17_cp"]);
                hdnB_cp.Value = Convert.ToString(Session["B_cp"]);
                hdnYTD_plan_cp.Value = Convert.ToString(Session["YTD_plan_cp"]);
                hdnYTD_sale_18_cp.Value = Convert.ToString(Session["YTD_sale_18_cp"]);
                hdnvariance_cp.Value = Convert.ToString(Session["variance_cp"]);
                hdnvariance_prc_cp.Value = Convert.ToString(Session["variance_prc_cp"]);

                hdnYTD_sale_17_total.Value = Convert.ToString(Session["YTD_sale_17_total"]);
                hdnB_total.Value = Convert.ToString(Session["B_total"]);
                hdnYTD_plan_total.Value = Convert.ToString(Session["YTD_plan_total"]);
                hdnYTD_sale_18_total.Value = Convert.ToString(Session["YTD_sale_18_total"]);
                hdnvariance_total.Value = Convert.ToString(Session["variance_total"]);
                hdnvariance_prc_total.Value = Convert.ToString(Session["variance_prc_total"]);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void productReview_Click(object sender, EventArgs e)
        {
            strUserId = Session["UserId"].ToString();
            roleId = Session["RoleId"].ToString();
            lbl_nodata.Visible = false;
            ButtonPrdctExport.Visible = true;
            if (txtbxPositive.Text == "" && txtbxNegative.Text == "")
            {
                //txtbxNegative.Style.Add("border-color", "red");
                //txtbxPositive.Style.Add("border-color", "red");
                return;
            }
            prdctGridLoadStatus = 1;
            GridViewProduct.Visible = true;
            //GridViewProductTotals.Visible = true;

            prdct_negativeVal = string.IsNullOrEmpty(txtbxNegative.Text) ? (int?)null : (Convert.ToInt32(txtbxNegative.Text));
            prdct_positiveVal = string.IsNullOrEmpty(txtbxPositive.Text) ? (int?)null : Convert.ToInt32(txtbxPositive.Text);
            if (byValueIn == 100000)
            {
                ValInLakhs.Checked = true;
            }
            else
            {
                ValInThsnd.Checked = true;
            }
            if (roleId == "SE")
            {
                prdct_salesEngineerId = Session["UserId"].ToString();
                prdct_branchcode = Session["BranchCode"].ToString();
            }
            else
            {
                prdct_salesEngineerId = ddlSalesEngineerList.SelectedItem.Value;
                prdct_branchcode = ddlBranchList.SelectedItem.Value;
            }
            if (roleId == "TM" && prdct_branchcode == "ALL")
            {
                prdct_branchcode = strUserId;
            }
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtprdct = new DataTable();
            dtprdct = objReports.product_variation(prdct_branchcode, prdct_salesEngineerId, -prdct_negativeVal, prdct_positiveVal, byValueIn, cter);
            dtprdctsort = dtprdct.Copy();
            GridViewProduct.DataSource = dtprdct;
            GridViewProduct.DataBind();
            decimal sale = 0, budget = 0, ytdval = 0, variance = 0, variancePRcnt = 0, ytdplan = 0; ;
            for (int i = 0; i < dtprdct.Rows.Count; i++)
            {
                sale += dtprdct.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtprdct.Rows[i].ItemArray[1].ToString());
                budget += dtprdct.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtprdct.Rows[i].ItemArray[2].ToString());
                ytdplan += dtprdct.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtprdct.Rows[i].ItemArray[3].ToString());

                ytdval += dtprdct.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtprdct.Rows[i].ItemArray[4].ToString());
                variance += dtprdct.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtprdct.Rows[i].ItemArray[5].ToString());
            }

            variancePRcnt = ytdplan == 0 ? 0 : (variance / ytdplan) * 100;
            //dtprdct.Rows.Clear();
            bool allCoulmnszerototals = objReports.DeleteEmptyRows(0, sale, budget, ytdplan, ytdval, variance, variancePRcnt);
            if (allCoulmnszerototals == false)
            {
                dtprdct.Rows.Add("PRODUCT TOTAL",
                                        (Math.Round(sale, 0).ToString("N0", culture)),
                                        (Math.Round(budget, 0).ToString("N0", culture)),
                                         (Math.Round(ytdplan, 0).ToString("N0", culture)),
                                        (Math.Round(ytdval, 0).ToString("N0", culture)),
                                        (Math.Round(variance).ToString("N0", culture)),

                                        Math.Round(variancePRcnt));
                dtprdctsorttotals = dtprdct.Clone();
                dtprdctsorttotals.Rows.Add("PRODUCT TOTAL",
                                       (Math.Round(sale, 0).ToString("N0", culture)),
                                       (Math.Round(budget, 0).ToString("N0", culture)),
                                        (Math.Round(ytdplan, 0).ToString("N0", culture)),
                                       (Math.Round(ytdval, 0).ToString("N0", culture)),
                                       (Math.Round(variance).ToString("N0", culture)),
                                       Math.Round(variancePRcnt));

            }
            else
            {
                ButtonPrdctExport.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();alert('No data found for the Variance entered');", true);

            }
            if (dtprdct.Rows.Count != 0)
            {
                if (dtprdct.Rows.Count > 0)
                {
                    dtprdct.Rows[(dtprdct.Rows.Count) - 1].Delete();
                    dtprdct.AcceptChanges();
                }
                Session["variance_reportpro"] = dtprdct;
                GridViewProduct.DataSource = dtprdct;
                GridViewProduct.DataBind();
                setsessions_pro(dtprdctsorttotals);
        
                if (ValInThsnd.Checked)
                {
                    prdctflagValueInThsnd = true;
                    prdctflagValueInLakh = false;
                }
                if (ValInLakhs.Checked)
                {
                    prdctflagValueInThsnd = false;
                    prdctflagValueInLakh = true;
                }

                txtbxNegative.Style.Add("border-color", "");
                txtbxPositive.Style.Add("border-color", "");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

            }
        }

        private void setsessions_pro(DataTable dtprdctsorttotals)
        {
            try
            {
                Session["YTDSALE17_PRO"] = 0;
                Session["B2018_PRO"] = 0;
                Session["YTDPLAN2018_PRO"] = 0;
                Session["YTDSALE2018_PRO"] = 0;
                Session["VARIANCE_PRO"] = 0;
                Session["VARIANCE_PRC_PRO"] = 0;

                if (dtprdctsorttotals.Rows.Count > 0)
                {
                    if (dtprdctsorttotals.Rows[0]["sales_year_0"] != DBNull.Value)
                    {
                        Session["YTDSALE17_PRO"] = Convert.ToString(dtprdctsorttotals.Rows[0]["sales_year_0"]);
                    }
                    if (dtprdctsorttotals.Rows[0]["budget"] != DBNull.Value)
                    {
                        Session["B2018_PRO"] = Convert.ToString(dtprdctsorttotals.Rows[0]["budget"]);
                    }
                    if (dtprdctsorttotals.Rows[0]["ytdplan"] != DBNull.Value)
                    {
                        Session["YTDPLAN2018_PRO"] = Convert.ToString(dtprdctsorttotals.Rows[0]["ytdplan"]);
                    }
                    if (dtprdctsorttotals.Rows[0]["ytd"] != DBNull.Value)
                    {
                        Session["YTDSALE2018_PRO"] = Convert.ToString(dtprdctsorttotals.Rows[0]["ytd"]);
                    }
                    if (dtprdctsorttotals.Rows[0]["variance"] != DBNull.Value)
                    {
                        Session["VARIANCE_PRO"] = Convert.ToString(dtprdctsorttotals.Rows[0]["variance"]);
                    }
                    if (dtprdctsorttotals.Rows[0]["pvariancePct"] != DBNull.Value)
                    {
                        Session["VARIANCE_PRC_PRO"] = Convert.ToString(dtprdctsorttotals.Rows[0]["pvariancePct"]);
                    }
                }

                hdnYTDSALE17_PRO.Value = Convert.ToString(Session["YTDSALE17_PRO"]);
                hdnB2018_PRO.Value = Convert.ToString(Session["B2018_PRO"]);
                hdnYTDPLAN2018_PRO.Value = Convert.ToString(Session["YTDPLAN2018_PRO"]);
                hdnYTDSALE2018_PRO.Value = Convert.ToString(Session["YTDSALE2018_PRO"]);
                hdnVARIANCE_PRO.Value = Convert.ToString(Session["VARIANCE_PRO"]);
                hdnVARIANCE_PRC_PRO.Value = Convert.ToString(Session["VARIANCE_PRC_PRO"]);


            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void Review_CheckedChanged(object sender, EventArgs e)
        {
            txtbxNegative.Style.Add("border-color", "");
            txtbxPositive.Style.Add("border-color", "");
            //string branch = ddlBranchList.SelectedItem.Value;
            //string se = ddlSalesEngineerList.SelectedItem.Value;
            cust_salesEngineerId = cust_salesEngineerId == null ? "ALL" : cust_salesEngineerId;
            cust_branchcode = cust_branchcode == null ? "ALL" : cust_branchcode;
            prdct_salesEngineerId = prdct_salesEngineerId == null ? "ALL" : prdct_salesEngineerId;
            prdct_branchcode = prdct_branchcode == null ? "ALL" : prdct_branchcode;
            ButtonCustExport.Visible = false;
            ButtonPrdctExport.Visible = false;
            if (customerReview.Checked)
            {
                productReviewBtn.Visible = false;
                reports.Visible = true;
                salesbycustomer.Visible = true;
               // salesbycustomertotals.Visible = true;
                GridViewProduct.Visible = false;
                //GridViewProductTotals.Visible = false;
                try
                {
                    txtbxNegative.Text = ""; txtbxPositive.Text = "";
                  
                }
                catch (NullReferenceException ex)
                {
                }
                if (cust_negativeVal.ToString() == "0")
                {
                    txtbxNegative.Text = "";
                }
                else
                    txtbxNegative.Text = cust_negativeVal.ToString();
                if (cust_positiveVal.ToString() == "0")
                {
                    txtbxPositive.Text = "";
                }
                else
                {
                    txtbxPositive.Text = cust_positiveVal.ToString();
                }
               
                //if (custflagValueInThsnd == true)
                //{
                //    //ValInLakhs.Attributes.Remove("checked");
                //    //ValInThsnd.Attributes.Add("checked","checked");
                //    ValInLakhs.Checked = false;
                //    ValInThsnd.Checked = true;
                //}
                //else
                //{
                //    ValInLakhs.Checked = true;
                //    ValInThsnd.Checked = false;
                //}
                if (customerGridLoadStatus == 1)
                {
                    ButtonCustExport.Visible = true;
                    ButtonPrdctExport.Visible = false;
                }
                if (prdctGridLoadStatus == 1)
                {
                    txtbxNegative.Text = prdct_negativeVal.ToString();
                    txtbxPositive.Text = prdct_positiveVal.ToString();
                    reports_Click(null, null);

                }
            }
            if (productReview.Checked)
            {
                productReviewBtn.Visible = true;
                reports.Visible = false;
                salesbycustomer.Visible = false;
              //  salesbycustomertotals.Visible = false;
                GridViewProduct.Visible = true;
                //GridViewProductTotals.Visible = true;
                try
                {
                    txtbxNegative.Text = ""; txtbxPositive.Text = "";
                  
                }
                catch (NullReferenceException ex)
                {
                  
                }

                //
                if (prdct_negativeVal.ToString() == "0")
                {
                    txtbxNegative.Text = "";
                }
                else
                    txtbxNegative.Text = prdct_negativeVal.ToString();
                if (prdct_positiveVal.ToString() == "0")
                {
                    txtbxPositive.Text = "";
                }
                else
                {
                    txtbxPositive.Text = prdct_positiveVal.ToString();
                }
                //prdctflagValueInThsnd =prdctflagValueInThsnd ==true ?ValInThsnd.Checked=true:ValInThsnd.Checked=false;
                //prdctflagValueInLakh = prdctflagValueInLakh == true ? ValInLakhs.Checked = true : ValInLakhs.Checked = false;
                //if (prdctflagValueInThsnd == true)
                //{
                //    ValInLakhs.Checked = false;
                //    ValInThsnd.Checked = true;
                //}
                //else
                //{
                //    ValInLakhs.Checked = true;
                //    ValInThsnd.Checked = false;
                //}
                if (prdctGridLoadStatus == 1)
                {
                    ButtonPrdctExport.Visible = true;
                    ButtonCustExport.Visible = false;
                }
                //Newly addded
                if (customerGridLoadStatus == 1)
                {
                    txtbxNegative.Text = cust_negativeVal.ToString();
                    txtbxPositive.Text = cust_positiveVal.ToString();
                    prdct_branchcode = cust_branchcode;
                    productReview_Click(null, null);

                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }


        protected void salesby_cust_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int BudgetYear = objConfig.getBudgetYear();
                int ActualYear = objConfig.getActualYear() - 1;
                int ActualMonth = objConfig.getActualMonth();
                string Year = "";
                if (ActualMonth == 12) { Year = ActualYear + ""; }
                else { Year = ActualYear + "P"; }
                string ActualValueyear1 = (ActualYear - 1).ToString();
                string ActualValueNextYear = (ActualYear + 1).ToString();
                e.Row.Cells[5].Text = "YTD SALE" + " " + ActualYear;
                e.Row.Cells[6].Text = BudgetYear + " " + "B";
                e.Row.Cells[7].Text = "YTD PLAN" + " " + BudgetYear;
                e.Row.Cells[8].Text = "YTD SALE" + " " + BudgetYear;
                //e.Row.Cells[5].Text = "VARIANCE";
                //. e.Row.Cells[6].Text = "VARIANCE IN %";
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblYtdPlan = e.Row.FindControl("lbl_YtdPlan") as Label;
                var lblYTDSale = e.Row.FindControl("lbl_YtdSale") as Label;
                var lbl_customername = e.Row.FindControl("Labelcust_name") as Label;
                if (lbl_customername != null)
                    if (!lbl_customername.Text.ToString().Contains("TOTAL"))
                        if (lblYtdPlan != null)
                        {
                            if (lblYtdPlan.Text == "0" && (lblYTDSale.Text != "0"))
                            {
                                e.Row.Cells[5].CssClass = "tdHighlight";
                            }
                        }
            }
            //int rowcount = salesbycustomer.Rows.Count;
            //salesbycustomer.Rows[rowcount - 1].Visible = false;
        }


        private byte[] Compress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(ms, CompressionMode.Compress, true);
            zs.Write(b, 0, b.Length);
            zs.Close();
            return ms.ToArray();
        }

        /// This method takes the compressed byte stream as parameter
        /// and return a decompressed bytestream.

        private byte[] Decompress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(new MemoryStream(b),
                                           CompressionMode.Decompress, true);
            byte[] buffer = new byte[4096];
            int size;
            while (true)
            {
                size = zs.Read(buffer, 0, buffer.Length);
                if (size > 0)
                    ms.Write(buffer, 0, size);
                else break;
            }
            zs.Close();
            return ms.ToArray();
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            pageStatePersister1.Load();
            String vState = pageStatePersister1.ViewState.ToString();
            byte[] pBytes = System.Convert.FromBase64String(vState);
            pBytes = Decompress(pBytes);
            LosFormatter mFormat = new LosFormatter();
            Object ViewState = mFormat.Deserialize(System.Convert.ToBase64String(pBytes));
            return new Pair(pageStatePersister1.ControlState, ViewState);
        }

        protected override void SavePageStateToPersistenceMedium(Object pViewState)
        {
            Pair pair1;
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            Object ViewState;
            if (pViewState is Pair)
            {
                pair1 = ((Pair)pViewState);
                pageStatePersister1.ControlState = pair1.First;
                ViewState = pair1.Second;
            }
            else
            {
                ViewState = pViewState;
            }
            LosFormatter mFormat = new LosFormatter();
            StringWriter mWriter = new StringWriter();
            mFormat.Serialize(mWriter, ViewState);
            String mViewStateStr = mWriter.ToString();
            byte[] pBytes = System.Convert.FromBase64String(mViewStateStr);
            pBytes = Compress(pBytes);
            String vStateStr = System.Convert.ToBase64String(pBytes);
            pageStatePersister1.ViewState = vStateStr;
            pageStatePersister1.Save();
        }

        protected void salesby_prdct_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                
                int BudgetYear = objConfig.getBudgetYear();
                int ActualYear = objConfig.getActualYear() - 1;
                int ActualMonth = objConfig.getActualMonth();
                string Year = "";
                if (ActualMonth == 12) { Year = ActualYear + ""; }
                else { Year = ActualYear + "P"; }


                string ActualValueyear1 = (ActualYear - 1).ToString();
                string ActualValueNextYear = (ActualYear + 1).ToString();

                e.Row.Cells[1].Text = "YTD  SALE " + " " + ActualYear;
                e.Row.Cells[2].Text = BudgetYear + "B";
                e.Row.Cells[3].Text = "YTD PLAN" + " " + BudgetYear;
                e.Row.Cells[4].Text = "YTD  SALE " + " " + BudgetYear;
                e.Row.Cells[5].Text = "VARIANCE";

            }
        }
        protected void byValueIn_CheckedChanged(Object sender, EventArgs e)
        {
            Session["variance_search"] = "";
            Session["var_search"] = "";
            if (txtbxPositive.Text == "" && txtbxNegative.Text == "" && reports.Visible == true && customerGridLoadStatus == 1)
            {
                txtbxNegative.Style.Add("border-color", "red");
                txtbxPositive.Style.Add("border-color", "red");
                return;
            }
            if (txtbxPositive.Text == "" && txtbxNegative.Text == "" && productReviewBtn.Visible == true && prdctGridLoadStatus == 1)
            {
                txtbxNegative.Style.Add("border-color", "red");
                txtbxPositive.Style.Add("border-color", "red");
                return;
            }
            ButtonPrdctExport.Visible = false;
            ButtonCustExport.Visible = false;
            if (ValInThsnd.Checked)
            {
                byValueIn = 1000;
                if (customerGridLoadStatus == 1 && reports.Visible == true)
                {
                    reports_Click(null, null);
                }
                if (prdctGridLoadStatus == 1 && productReviewBtn.Visible == true)
                {
                    productReview_Click(null, null);
                }
            }

            if (ValInLakhs.Checked)
            {
                byValueIn = 100000;
                if (customerGridLoadStatus == 1 && reports.Visible == true)
                {
                    reports_Click(null, null);
                }
                if (prdctGridLoadStatus == 1 && productReviewBtn.Visible == true)
                {
                    productReview_Click(null, null);
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }

        //protected void custexports_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DataTable dt = (DataTable)Session["var_report"];
        //        DataTable dtNew = new DataTable();
        //        string search = Convert.ToString(Session["var_search"]);
        //        DataTable dtcheck = CheckforSearchedData(dt);           
        //        dtNew = addGrandTotalToExcelTable(dtcheck);
        //        strUserId = Session["UserId"].ToString();
        //        roleId = Session["RoleId"].ToString();
        //        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "CustomerVariance.xls"));
        //        Response.ContentType = "application/ms-excel";
        //        string Branch = ddlBranchList.SelectedItem.Text;
        //        string salesenggname = ddlSalesEngineerList.SelectedItem.Text;
        //        string cust_negativeVal = txtbxNegative.Text;
        //        string cust_positiveVal = txtbxPositive.Text;

        //        string branch = "ALL";
        //        string salesengineer = Session["UserName"].ToString();

        //        if (roleId == "SE")
        //        {
        //            branch = Session["BranchDesc"].ToString();
        //            salesengineer = Session["UserName"].ToString();

        //        }
        //        if (roleId == "HO" || roleId == "TM")
        //        {
        //            branch = ddlBranchList.SelectedItem.Text;
        //        }
        //        if (roleId == "HO" || roleId == "BM" || roleId == "TM")
        //        {
        //            salesengineer = ddlSalesEngineerList.SelectedItem.Text;

        //        }


        //        using (StringWriter sw = new StringWriter())
        //        {
        //            using (HtmlTextWriter htw = new HtmlTextWriter(sw))
        //            {
        //                //  Create a table to contain the grid
        //                Table table = new Table();

        //                //  include the gridline settings
        //                table.GridLines = salesbycustomer.GridLines;
        //                //  add the header row to the table
        //                if (salesbycustomer.HeaderRow != null)
        //                {
        //                    PrepareControlForExport(salesbycustomer.HeaderRow);
        //                    table.Rows.Add(salesbycustomer.HeaderRow);
        //                    salesbycustomer.DataSource = dtNew;
        //                }
        //                else
        //                {
        //                    salesbycustomer.DataSource = null;
        //                }
        //                //  add each of the data rows to the table
        //                foreach (GridViewRow row in salesbycustomer.Rows)
        //                {
        //                    PrepareControlForExport(row);
        //                    table.Rows.Add(row);
        //                }

        //                table.Rows[0].Height = 30;
        //                table.Rows[0].BackColor = Color.LightSeaGreen;
        //                table.Rows[0].Font.Bold = true;
        //                //  add the footer row to the table

        //                //  render the table into the htmlwriter


        //                sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px; '>Variance Review report</td></table>");
        //                sw.WriteLine("<table style='margin-left: 180px;'>");

        //                if (Session["RoleId"].ToString() == "HO")
        //                {
        //                    string territory;
        //                    if (rdBtnTaegutec.Checked)
        //                    {                                
        //                        territory = "TAEGUTEC";
        //                    }
        //                    else
        //                    {
        //                        territory = "DURACARB";
        //                    }
        //                  //  sw.WriteLine("TERRITORY :" + "" + territory + "<br/>");
        //                    sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");
        //                }
        //              //  sw.WriteLine("BRANCH :" + "" + branch + "<br/>");
        //                sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> BRANCH :" + "</td><td colspan=8 style='font-style: italic;'>" + Branch + "</td></tr>");
        //               // sw.WriteLine("SALES ENGINEER:" + "" + salesengineer + "<br/>");
        //                sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> SALES ENGINEER :" + "</td><td colspan=8 style='font-style: italic;'>" + salesenggname + "</td></tr>");

        //               // sw.WriteLine("NEGATIVE :" + "" + cust_negativeVal.ToString() + "<br/>");
        //                sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> NEGATIVE :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + cust_negativeVal + "</td></tr>");

        //               // sw.WriteLine("POSITIVE :" + "" + cust_positiveVal.ToString() + "<br/>");
        //                sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> POSITIVE :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + cust_positiveVal + "</td></tr>");

        //                if (!String.IsNullOrEmpty(hdnsearch.Value))
        //                {
        //                    sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'>SEARCH TEXT:" + "</td><td colspan=8 style='font-style:italic; text-align: left;'>" + hdnsearch.Value + "</td></tr>");
        //                }
        //                sw.WriteLine("</table><br/>");
                        
        //                //if (ValInThsnd.Checked)
        //                //{
        //                //   sw.WriteLine("VALUE IN :" + "" + "THOUSANDS " + "<br/>");
        //                //}
        //                //else
        //                //{
        //                //    sw.WriteLine("VALUE IN :" + "" + "LAKHS " + "<br/>");
        //                //}
        //                table.RenderControl(htw);
        //            }

        //            Response.Write(sw.ToString());
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //            Response.End();
        //            salesbycustomer.DataSource = dtcheck;
        //            salesbycustomer.DataBind();
                  
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }


        //}

        protected void custexports_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = (DataTable)Session["var_report"];
                DataTable dtNew = new DataTable();
                DataTable dtcheck = CheckforSearchedData(dt);
                dtNew = addGrandTotalToExcelTable(dtcheck);
                ExportReport(dtNew, (DataTable)Session["var_report"]);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        private void ExportReport(DataTable dtNew, DataTable dtExport)
        {
            //review_overall();
            try
            {
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "CustomerVariance.xls"));
                Response.ContentType = "application/ms-excel";
            
                string branch = "ALL";
                string salesengineer = Convert.ToString(Session["UserName"]);

                if (roleId == "SE")
                {
                    branch = Session["BranchDesc"].ToString();
                    salesengineer = Session["UserName"].ToString();
                }
               
                if (roleId == "HO" || roleId == "TM")
                {
                    branch = ddlBranchList.SelectedItem.Text;
                }
                if (roleId == "HO" || roleId == "BM" || roleId == "TM")
                {
                    salesengineer = ddlSalesEngineerList.SelectedItem.Text;

                }

               // string Branch = ddlBranchList.SelectedItem.Text;
              //  string salesenggname = ddlSalesEngineerList.SelectedItem.Text;
                string cust_negativeVal = txtbxNegative.Text;
                string cust_positiveVal = txtbxPositive.Text;
                string hdnsearch = Convert.ToString(Session["var_search"]);
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a table to contain the grid
                        Table table = new Table();

                        //  include the gridline settings
                        table.GridLines = salesbycustomer.GridLines;
                        //  add the header row to the table
                        if (salesbycustomer.HeaderRow != null)
                        {
                            PrepareControlForExport(salesbycustomer.HeaderRow);
                            table.Rows.Add(salesbycustomer.HeaderRow);
                            salesbycustomer.DataSource = dtNew;
                        }
                        else
                        {
                            salesbycustomer.DataSource = null;
                        }
                        //  add each of the data rows to the table
                        salesbycustomer.DataBind();
                        foreach (GridViewRow row in salesbycustomer.Rows)
                        {
                            PrepareControlForExport(row);
                            table.Rows.Add(row);
                        }

                        table.Rows[0].Height = 30;
                        table.Rows[0].BackColor = Color.LightSeaGreen;
                        table.Rows[0].Font.Bold = true;
                        //  add the footer row to the table

                        //  render the table into the htmlwriter


                        sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px; '>Variance Review report</td></table>");
                        sw.WriteLine("<table style='margin-left: 180px;'>");

                        if (Session["RoleId"].ToString() == "HO")
                        {
                            string territory;
                            if (rdBtnTaegutec.Checked)
                            {
                                territory = "TAEGUTEC";
                            }
                            else
                            {
                                territory = "DURACARB";
                            }
                            //  sw.WriteLine("TERRITORY :" + "" + territory + "<br/>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");
                        }
                        if (Session["RoleId"].ToString() == "BM")
                        {
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["BranchDesc"]) + "</td></tr>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(ddlSalesEngineerList.SelectedItem) + "</td></tr>");
                        }
                        else if (Session["RoleId"].ToString() == "SE")
                        {
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["BranchDesc"]) + "</td></tr>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["UserName"]) + "</td></tr>");
                        }
                        else
                        {
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(ddlBranchList.SelectedItem) + "</td></tr>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(ddlSalesEngineerList.SelectedItem) + "</td></tr>");

                        }
                        //  sw.WriteLine("BRANCH :" + "" + branch + "<br/>");
                      //  sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> BRANCH :" + "</td><td colspan=8 style='font-style: italic;'>" + branch + "</td></tr>");
                        // sw.WriteLine("SALES ENGINEER:" + "" + salesengineer + "<br/>");
                     //   sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> SALES ENGINEER :" + "</td><td colspan=8 style='font-style: italic;'>" + salesenggname + "</td></tr>");

                        // sw.WriteLine("NEGATIVE :" + "" + cust_negativeVal.ToString() + "<br/>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> NEGATIVE :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + cust_negativeVal + "</td></tr>");

                        // sw.WriteLine("POSITIVE :" + "" + cust_positiveVal.ToString() + "<br/>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> POSITIVE :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + cust_positiveVal + "</td></tr>");

                        if (!String.IsNullOrEmpty(hdnsearch))
                        {
                            //sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> POSITIVE :" +  "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + cust_positiveVal + "</td></tr>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'>SEARCH TEXT:" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + hdnsearch + "</td></tr>");
                        }
                        sw.WriteLine("</table><br/>");

                        //if (ValInThsnd.Checked)
                        //{
                        //   sw.WriteLine("VALUE IN :" + "" + "THOUSANDS " + "<br/>");
                        //}
                        //else
                        //{
                        //    sw.WriteLine("VALUE IN :" + "" + "LAKHS " + "<br/>");
                        //}
                        table.RenderControl(htw);
                    }
                    Response.Write(sw.ToString());
                   // ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                    
                    DataRow dr = dtExport.AsEnumerable().SingleOrDefault(r => r.Field<string>("cust_name") == "CUSTOMER TOTAL : ");
                    if (dr != null)
                    {
                        dr.Delete();
                        dtExport.AcceptChanges();
                    }
                    dr = dtExport.AsEnumerable().SingleOrDefault(r => r.Field<string>("cust_name") == "CHANNEL PARTNER TOTAL : ");
                    if (dr != null)
                    {
                        dr.Delete();
                        dtExport.AcceptChanges();
                    }
                    dr = dtExport.AsEnumerable().SingleOrDefault(r => r.Field<string>("cust_name") == "GRAND TOTAL : ");
                    if (dr != null)
                    {
                        dr.Delete();
                        dtExport.AcceptChanges();
                    }
                    salesbycustomer.DataSource = dtExport;
                    salesbycustomer.DataBind();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        private DataTable CheckforSearchedData(DataTable dt)
        {
            //DataTable dtOutput = new DataTable();
           // string search = hdnsearch.Value;
            string search = Convert.ToString(Session["var_search"]);
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            try
            {
                if (!string.IsNullOrEmpty(search))
                {
                    if (search.Contains(' '))
                    {
                        searchList = search.Split(' ').ToList();
                    }
                    else
                    {
                        searchList.Add(search);
                    }

                    for (int i = 0; i < searchList.Count; i++)
                    {
                        DataTable dtOutput = new DataTable();
                        dtOutput.Columns.Add("cust_name");
                        dtOutput.Columns.Add("cust_number");
                        dtOutput.Columns.Add("customer_type");
                        dtOutput.Columns.Add("Branch");
                        dtOutput.Columns.Add("EngineerName");
                        dtOutput.Columns.Add("sales_year_0");
                        dtOutput.Columns.Add("budget");
                        dtOutput.Columns.Add("ytdplan");
                        dtOutput.Columns.Add("ytd");
                        dtOutput.Columns.Add("variance");
                        dtOutput.Columns.Add("variancePct");

                        filteredRows = dt.Select("CONVERT(cust_name, System.String) LIKE '%" + searchList[i].Trim()
                              + "%' OR CONVERT(cust_number, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(customer_type, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(Branch, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(EngineerName, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(sales_year_0, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(ytdplan, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(variance, System.String) LIKE '%" + searchList[i].Trim()
                              + "%' OR CONVERT(variancePct, System.String) LIKE '%" + searchList[i].Trim()
                              + "%'");
                        foreach (DataRow dr in filteredRows)
                        {
                            dtOutput.Rows.Add(dr.ItemArray);
                        }
                        dt = dtOutput;
                    }

                }
                //else
                //{
                //    dtOutput = dt;
                //}
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            return dt;
        }
        private DataTable addGrandTotalToExcelTable(DataTable dtAdd)
        {
           // string search = hdnsearch.Value;
            string search = Convert.ToString(Session["var_search"]);
            try
            {
                DataRow dr = dtAdd.NewRow();
                DataRow dr1 = dtAdd.NewRow();
                DataRow dr2 = dtAdd.NewRow();

                dr["cust_name"] = "CUSTOMER TOTAL : ";
                dr1["cust_name"] = "CHANNEL PARTNER TOTAL : ";
                dr2["cust_name"] = "GRAND TOTAL : ";

                if (!string.IsNullOrEmpty(search))
                {
                    if (dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")) != null)
                    {
                        dr["sales_year_0"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["sales_year_0"])).ToString());
                        dr["budget"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["budget"])).ToString());
                        dr["ytdplan"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["ytdplan"])).ToString());
                        dr["ytd"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["ytd"])).ToString());
                        dr["variance"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        if (Convert.ToDecimal(dr["ytdplan"]) != 0)
                            dr["variancePct"] = Math.Round(Convert.ToDecimal(dr["variance"]) / Convert.ToDecimal(dr["ytdplan"]) * 100, 0);
                        else
                            dr["variancePct"] = 0;
                    }

                    else
                    {
                        dr["sales_year_0"] = 0;
                        dr["budget"] = 0;
                        dr["ytdplan"] = 0;
                        dr["ytd"] = 0;                        
                        dr["variance"] = 0;
                        dr["variancePct"] = 0;
                    }
                    if (dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")) != null)
                    {
                        dr1["sales_year_0"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["sales_year_0"])).ToString());
                        dr1["budget"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["budget"])).ToString());
                        dr1["ytdplan"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["ytdplan"])).ToString());
                        dr1["ytd"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["ytd"])).ToString());
                        dr1["variance"] = Convert.ToInt32(dtAdd.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        if (Convert.ToDecimal(dr1["ytdplan"]) != 0)
                            dr1["variancePct"] = Math.Round(Convert.ToDecimal(dr1["variance"]) / Convert.ToDecimal(dr1["ytdplan"]) * 100, 0);
                        else
                            dr1["variancePct"] = 0;
                    }
                  else
                    {
                        dr1["sales_year_0"] = 0;
                        dr1["budget"] = 0;
                        dr1["ytdplan"] = 0;
                        dr1["ytd"] = 0;
                        dr1["variance"] = 0;
                        dr1["variancePct"] = 0;
                    }
                    if (dtAdd.Rows.Count != null)
                    {
                        dr2["sales_year_0"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_year_0"])).ToString());
                        dr2["budget"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString());
                        dr2["ytdplan"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytdplan"])).ToString());
                        dr2["ytd"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString());
                        dr2["variance"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        if (Convert.ToDecimal(dr2["ytdplan"]) != 0)
                            dr2["variancePct"] = Math.Round(Convert.ToDecimal(dr2["variance"]) / Convert.ToDecimal(dr2["ytdplan"]) * 100, 0);
                        else
                            dr2["variancePct"] = 0;
                    }
                    else
                    {
                        dr2["sales_year_0"] = 0;
                        dr2["budget"] = 0;
                        dr2["ytdplan"] = 0;
                        dr2["ytd"] = 0;
                        dr2["variance"] = 0;
                        dr2["variancePct"] = 0;
                    }
                }
                else
                {
                    dr["sales_year_0"] = Convert.ToDecimal(Session["YTD_sale_17"]);
                    dr1["sales_year_0"] = Convert.ToDecimal(Session["YTD_sale_17_cp"]);
                    dr2["sales_year_0"] = Convert.ToDecimal(Session["YTD_sale_17_total"]);

                    dr["budget"] = Convert.ToDecimal(Session["B"]);
                    dr1["budget"] = Convert.ToDecimal(Session["B_cp"]);
                    dr2["budget"] = Convert.ToDecimal(Session["B_total"]);

                    dr["ytdplan"] = Convert.ToDecimal(Session["YTD_plan"]);
                    dr1["ytdplan"] = Convert.ToDecimal(Session["YTD_plan_cp"]);
                    dr2["ytdplan"] = Convert.ToDecimal(Session["YTD_plan_total"]);

                    dr["ytd"] = Convert.ToDecimal(Session["YTD_sale_18"]);
                    dr1["ytd"] = Convert.ToDecimal(Session["YTD_sale_18_cp"]);
                    dr2["ytd"] = Convert.ToDecimal(Session["YTD_sale_18_total"]);

                    dr["variance"] = Convert.ToDecimal(Session["variance"]);
                    dr1["variance"] = Convert.ToDecimal(Session["variance_cp"]);
                    dr2["variance"] = Convert.ToDecimal(Session["variance_total"]);

                    dr["variancePct"] = Convert.ToDecimal(Session["variance_prc"]);
                    dr1["variancePct"] = Convert.ToDecimal(Session["variance_prc_cp"]);
                    dr2["variancePct"] = Convert.ToDecimal(Session["variance_prc_total"]);

                }
                dtAdd.Rows.Add(dr);
                dtAdd.Rows.Add(dr1);
                dtAdd.Rows.Add(dr2);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);

            }
             return dtAdd;
        }
        private static void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    //control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is HiddenField)
                {
                    control.Controls.Remove(current);
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }
                if (current.HasControls())
                {
                    PrepareControlForExport(current);
                }
            }
        }

        //protected void prdctexports_Click(object sender, EventArgs e)
        //{
        //    DataTable dt = (DataTable)Session["var_reportpro"];
        //    DataTable dtNew = new DataTable();
        //    DataTable dtcheck = CheckforSearchedData_pro(dt);
        //    //dtNew = addGrandTotalToExcelTable_pro(dtcheck);
        //    strUserId = Session["UserId"].ToString();
        //    roleId = Session["RoleId"].ToString();
        //    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ProductVariance.xls"));
        //    Response.ContentType = "application/ms-excel";

        //    string Branch = ddlBranchList.SelectedItem.Text;
        //    string salesenggname = ddlSalesEngineerList.SelectedItem.Text;
        //    string cust_negativeVal = txtbxNegative.Text;
        //    string cust_positiveVal = txtbxPositive.Text;

        //    string branch = "ALL";
        //    string salesengineer = Session["UserName"].ToString();

        //    if (roleId == "SE")
        //    {
        //        branch = Session["BranchDesc"].ToString();
        //        salesengineer = Session["UserName"].ToString();
        //    }

        //    if (roleId == "HO" || roleId == "TM")
        //    {
        //        branch = ddlBranchList.SelectedItem.Text;
        //    }
        //    if (roleId == "HO" || roleId == "BM" || roleId == "TM")
        //    {
        //        salesengineer = ddlSalesEngineerList.SelectedItem.Text;
        //    }

        //    using (StringWriter sw = new StringWriter())
        //    {
        //        using (HtmlTextWriter htw = new HtmlTextWriter(sw))
        //        {
        //            //  Create a table to contain the grid
        //            Table table = new Table();

        //            //  include the gridline settings
        //            table.GridLines = GridViewProduct.GridLines;
        //            //  add the header row to the table
        //            if (GridViewProduct.HeaderRow != null)
        //            {
        //                PrepareControlForExport(GridViewProduct.HeaderRow);
        //                table.Rows.Add(GridViewProduct.HeaderRow);
        //            }
        //            //  add each of the data rows to the table
        //            foreach (GridViewRow row in GridViewProduct.Rows)
        //            {
        //                PrepareControlForExport(row);
        //                table.Rows.Add(row);
        //            }

        //            table.Rows[0].Height = 30;
        //            table.Rows[0].BackColor = Color.LightSeaGreen;
        //            table.Rows[0].Font.Bold = true;
        //            //  add the footer row to the table
        //            sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px; '>PRODUCT REVIEW</td></table>");
        //            sw.WriteLine("<table style='margin-left: 200px;'>");
        //            //  render the table into the htmlwriter
        //            if (Session["RoleId"].ToString() == "HO")
        //            {
        //                string territory;
        //                if (rdBtnTaegutec.Checked)
        //                {
        //                    territory = "TAEGUTEC";
        //                }
        //                else
        //                {
        //                    territory = "DURACARB";
        //                }
        //                sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");
        //          //  sw.WriteLine("TERRITORY :" + "" + territory + "<br/>");
        //            }
        //          //  sw.WriteLine("BRANCH :" + "" + branch + "<br/>");
        //            sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> BRANCH :" + "</td><td colspan=8 style='font-style: italic;'>" + Branch + "</td></tr>");
        //           // sw.WriteLine("SALES ENGINEER:" + "" + salesengineer + "<br/>");
        //            sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> SALES ENGINEER :" + "</td><td colspan=8 style='font-style: italic;'>" + salesenggname + "</td></tr>");
        //           // sw.WriteLine("NEGATIVE :" + "" + prdct_negativeVal.ToString() + "<br/>");
        //            sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> NEGATIVE :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + cust_negativeVal + "</td></tr>");

        //           // sw.WriteLine("POSITIVE :" + "" + prdct_positiveVal.ToString() + "<br/>");
        //            sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> POSITIVE :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + cust_positiveVal + "</td></tr>");

        //            //if (ValInThsnd.Checked)
        //            //{
        //            //    sw.WriteLine("VALUE IN :" + "" + "THOUSANDS " + "<br/>");
        //            //}
        //            //else
        //            //{
        //            //    sw.WriteLine("VALUE IN :" + "" + "LAKHS " + "<br/>");
        //            //}
        //            table.RenderControl(htw);
        //        }

        //        Response.Write(sw.ToString());
        //        Response.End();

        //    }

        //    //sw.WriteLine("BRANCH :" + "" + branch + "<br/>");
        //    //sw.WriteLine("SALES ENGINEER:" + "" + salesengineer + "<br/>");
        //    //sw.WriteLine("NEGATIVE :" + "" + prdct_negativeVal.ToString() + "<br/>");
        //    //sw.WriteLine("POSITIVE :" + "" + prdct_negativeVal.ToString() + "<br/>");
        //    //if (ValInThsnd.Checked)
        //    //{
        //    //    sw.WriteLine("VALUE IN :" + "" + "THOUSANDS " + "<br/>");
        //    //}
        //    //else
        //    //{
        //    //    sw.WriteLine("VALUE IN :" + "" + "LAKHS " + "<br/>");
        //    //}
        //    //GridViewProduct.RenderControl(ht);
        //    ////GridViewProductTotals.RenderControl(ht);

        //    //Response.Write(sw.ToString());
        //    //Response.End();
        //}

        protected void prdctexports_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = (DataTable)Session["variance_reportpro"];
                DataTable dtNew = new DataTable();
                DataTable dtcheck = CheckforSearchedData_pro(dt);
                dtNew = addGrandTotalToExcelTable_pro(dtcheck);
                ExportReportpro(dtNew, (DataTable)Session["variance_reportpro"]);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        private void ExportReportpro(DataTable dtNew, DataTable dtExport)
        {     
            try
            {
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ProductVariance.xls"));
                Response.ContentType = "application/ms-excel";
                //string Branch = ddlBranchList.SelectedItem.Text;
                //string salesenggname = ddlSalesEngineerList.SelectedItem.Text;
                string cust_negativeVal = txtbxNegative.Text;
                string cust_positiveVal = txtbxPositive.Text;
                string hdnsearch = Convert.ToString(Session["variance_search"]);

            string branch = "ALL";
            string salesengineer = Session["UserName"].ToString();

            if (roleId == "SE")
            {
                branch = Session["BranchDesc"].ToString();
                salesengineer = Session["UserName"].ToString();
            }

            if (roleId == "HO" || roleId == "TM")
            {
                branch = ddlBranchList.SelectedItem.Text;
            }
            if (roleId == "HO" || roleId == "BM" || roleId == "TM")
            {
                salesengineer = ddlSalesEngineerList.SelectedItem.Text;
            }

            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    //  Create a table to contain the grid
                    Table table = new Table();

                    //  include the gridline settings
                    table.GridLines = GridViewProduct.GridLines;
                    //  add the header row to the table
                    if (GridViewProduct.HeaderRow != null)
                    {
                       PrepareControlForExport(GridViewProduct.HeaderRow);
                       table.Rows.Add(GridViewProduct.HeaderRow);
                       GridViewProduct.DataSource=dtNew;
                    }
                     else
                     {
                     GridViewProduct.DataSource = null;
                     }
                     GridViewProduct.DataBind();
                    //  add each of the data rows to the table
                    foreach (GridViewRow row in GridViewProduct.Rows)
                    {
                        PrepareControlForExport(row);
                        table.Rows.Add(row);
                    }

                    table.Rows[0].Height = 30;
                    table.Rows[0].BackColor = Color.LightSeaGreen;
                    table.Rows[0].Font.Bold = true;
                    //  add the footer row to the table
                    sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px; '>PRODUCT REVIEW</td></table>");
                    sw.WriteLine("<table style='margin-left: 200px;'>");
                    //  render the table into the htmlwriter
                    if (Session["RoleId"].ToString() == "HO")
                    {
                        string territory;
                        if (rdBtnTaegutec.Checked)
                        {
                            territory = "TAEGUTEC";
                        }
                        else
                        {
                            territory = "DURACARB";
                        }
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");
                  //  sw.WriteLine("TERRITORY :" + "" + territory + "<br/>");
                    }
                  //  sw.WriteLine("BRANCH :" + "" + branch + "<br/>");
                  //  sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> BRANCH :" + "</td><td colspan=8 style='font-style: italic;'>" + Branch + "</td></tr>");
                   // sw.WriteLine("SALES ENGINEER:" + "" + salesengineer + "<br/>");
                   // sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> SALES ENGINEER :" + "</td><td colspan=8 style='font-style: italic;'>" + salesenggname + "</td></tr>");
                   // sw.WriteLine("NEGATIVE :" + "" + prdct_negativeVal.ToString() + "<br/>");
                    if (Session["RoleId"].ToString() == "BM")
                    {
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["BranchDesc"]) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(ddlSalesEngineerList.SelectedItem) + "</td></tr>");
                    }
                    else if (Session["RoleId"].ToString() == "SE")
                    {
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["BranchDesc"]) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["UserName"]) + "</td></tr>");
                    }
                    else
                    {
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(ddlBranchList.SelectedItem) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(ddlSalesEngineerList.SelectedItem) + "</td></tr>");

                    }
                    sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> NEGATIVE :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + cust_negativeVal + "</td></tr>");

                   // sw.WriteLine("POSITIVE :" + "" + prdct_positiveVal.ToString() + "<br/>");
                    sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> POSITIVE :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + cust_positiveVal + "</td></tr>");

                    if (!String.IsNullOrEmpty(hdnsearch))
                    {
                       
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'>SEARCH TEXT:" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + hdnsearch + "</td></tr>");
                    }
                    sw.WriteLine("</table><br/>");
                    //if (ValInThsnd.Checked)
                    //{
                    //    sw.WriteLine("VALUE IN :" + "" + "THOUSANDS " + "<br/>");
                    //}
                    //else
                    //{
                    //    sw.WriteLine("VALUE IN :" + "" + "LAKHS " + "<br/>");
                    //}
                    table.RenderControl(htw);
                }

                Response.Write(sw.ToString());
                DataRow dr = dtExport.AsEnumerable().SingleOrDefault(r => r.Field<string>("prdct_name") == "PRODUCT TOTAL : ");
                if (dr != null)
                {
                    dr.Delete();
                    dtExport.AcceptChanges();
                }
       
                GridViewProduct.DataSource = dtExport;
                GridViewProduct.DataBind();
                Response.End();
                }
             }
           catch (Exception ex)
             {
                objFunc.LogError(ex);
             }
           }
         
     
        private DataTable CheckforSearchedData_pro(DataTable dt)
        {
            //DataTable dtOutput = new DataTable();
            //string search = hdnsearchpro.Value;
            string search = Convert.ToString(Session["variance_search"]);
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            try
            {
                if (!string.IsNullOrEmpty(search))
                {
                    if (search.Contains(' '))
                    {
                        searchList = search.Split(' ').ToList();
                    }
                    else
                    {
                        searchList.Add(search);
                    }

                    for (int i = 0; i < searchList.Count; i++)
                    {
                        DataTable dtOutput = new DataTable();              
                        dtOutput.Columns.Add("prdct_name");
                        dtOutput.Columns.Add("sales_year_0");
                        dtOutput.Columns.Add("budget");
                        dtOutput.Columns.Add("ytdplan");
                        dtOutput.Columns.Add("ytd");
                        dtOutput.Columns.Add("variance");
                        dtOutput.Columns.Add("pvariancePct");


                        filteredRows = dt.Select("CONVERT(prdct_name, System.String) LIKE '%" + searchList[i].Trim()
                              + "%' OR CONVERT(sales_year_0, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(ytdplan, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(variance, System.String) LIKE '%" + searchList[i].Trim()
                              + "%' OR CONVERT(pvariancePct, System.String) LIKE '%" + searchList[i].Trim()
                              + "%'");
                        foreach (DataRow dr in filteredRows)
                        {
                            dtOutput.Rows.Add(dr.ItemArray);
                        }
                        dt = dtOutput;
                    }

                }
                //else
                //{
                //    dtOutput = dt;
                //}
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            return dt;
        }

        private DataTable addGrandTotalToExcelTable_pro(DataTable dtAdd)
        {
            string search = Convert.ToString(Session["variance_search"]);
            try
            {
                DataRow dr = dtAdd.NewRow();
                dr["prdct_name"] = "PRODUCT TOTAL : ";

                if (!string.IsNullOrEmpty(search))
                {
                    if (dtAdd.Rows.Count != null)
                    {
                        dr["sales_year_0"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_year_0"])).ToString());
                        dr["budget"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString());
                        dr["ytdplan"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytdplan"])).ToString());
                        dr["ytd"] = Convert.ToInt32(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString());
                        dr["variance"] = Convert.ToString(dtAdd.AsEnumerable().Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        if (Convert.ToDecimal(dr["ytdplan"]) != 0)
                            dr["pvariancePct"] = Math.Round(Convert.ToDecimal(dr["variance"]) / Convert.ToDecimal(dr["ytdplan"]) * 100, 0);
                        else
                            dr["pvariancePct"] = 0;
                    }

                    else
                    {
                        dr["sales_year_0"] = 0;
                        dr["budget"] = 0;
                        dr["ytdplan"] = 0;
                        dr["ytd"] = 0;
                        dr["variance"] = 0;
                       // dr["pvariancePct"] = 0;
                    }
                }
                else{

                    dr["sales_year_0"] = Convert.ToDecimal(Session["YTDSALE17_PRO"]);
                    dr["budget"] = Convert.ToDecimal(Session["B2018_PRO"]);
                    dr["ytdplan"] = Convert.ToDecimal(Session["YTDPLAN2018_PRO"]);
                    dr["ytd"] = Convert.ToDecimal(Session["YTDSALE2018_PRO"]);
                    dr["variance"] = Convert.ToDecimal(Session["VARIANCE_PRO"]);
                    dr["pvariancePct"] = Convert.ToDecimal(Session["VARIANCE_PRC_PRO"]);
                   
                }
                    dtAdd.Rows.Add(dr);
                
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return dtAdd;
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
        }

        protected void salesbycustomer_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = new DataTable();
            if (dtcustsort != null)
            {
                DataView dataView = new DataView(dtcustsort);
                dataView.Sort = e.SortExpression + " " + ConvertSortDirection(e.SortDirection);
                dt = dataView.ToTable();
                dt.Merge(dtcustsorttotal);

            }
            if (dtdlrsort != null)
            {
                DataTable dtdlr = new DataTable();
                DataView dataView = new DataView(dtdlrsort);
                dataView.Sort = e.SortExpression + " " + ConvertSortDirection(e.SortDirection);
                dtdlr = dataView.ToTable();
                dtdlr.Merge(dtdlrsorttotal);
                dt.Merge(dtdlr);

            }
            dt.Merge(dtcustalltotal);
            salesbycustomer.DataSource = dt;
            salesbycustomer.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }

        protected void productview_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = new DataTable();
            if (dtprdctsort != null)
            {
                DataTable dtdlr = new DataTable();
                DataView dataView = new DataView(dtprdctsort);
                dataView.Sort = e.SortExpression + " " + ConvertSortDirection(e.SortDirection);
                dt = dataView.ToTable();

            }
            dt.Merge(dtprdctsorttotals);
            GridViewProduct.DataSource = dt;
            GridViewProduct.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }
        private string ConvertSortDirection(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;
            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;
                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }

        protected void ibtn_VariancePerSort_Click(object sender, ImageClickEventArgs e)
        {

            if (cvflagsort == false)
            {
                salesbycustomer_Sorting(null, new GridViewSortEventArgs("variancePct", SortDirection.Ascending));
                cvflagsort = true;
            }
            else
            {
                salesbycustomer_Sorting(null, new GridViewSortEventArgs("variancePct", SortDirection.Descending));
                cvflagsort = false;
            }

        }
        protected void ibtn_PVariancePerSort_Click(object sender, ImageClickEventArgs e)
        {

            if (pvflagsort == true)
            {
                productview_Sorting(null, new GridViewSortEventArgs("pvariancePct", SortDirection.Ascending));
                pvflagsort = false;
            }
            else
            {
                productview_Sorting(null, new GridViewSortEventArgs("pvariancePct", SortDirection.Descending));
                pvflagsort = true;
            }

        }

        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alertClickFilter();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();$('#alertmsg').show().delay(5000).fadeOut();", true);
            hdnsearch.Value = "";
            hdnsearchpro.Value = "";
            if (rdBtnTaegutec.Checked)
            {
                Session["cter"] = "TTA";
                cter = "TTA";
                LoadBranches();
            }
            if (rdBtnDuraCab.Checked)
            {
                Session["cter"] = "DUR";
                cter = "DUR";
                LoadBranches();
            }
            ButtonCustExport.Visible = false;
            ButtonPrdctExport.Visible = false;
            salesbycustomer.DataSource = null;
            salesbycustomer.DataBind();
           // salesbycustomertotals.DataSource = null;
           // salesbycustomertotals.DataBind();
            GridViewProduct.DataSource = null;
            GridViewProduct.DataBind();
            customerGridLoadStatus = 0;
            prdctGridLoadStatus = 0;           
        }

        [WebMethod]
        public static List<string> setGrandtotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            HttpContext.Current.Session["var_search"] = search;
            dt = (DataTable)HttpContext.Current.Session["var_report"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    dtOutput = new DataTable();
                    dtOutput.Columns.Add("cust_name");
                    dtOutput.Columns.Add("cust_number");
                    dtOutput.Columns.Add("customer_type");
                    dtOutput.Columns.Add("Branch");
                    dtOutput.Columns.Add("EngineerName");
                    dtOutput.Columns.Add("sales_year_0");
                    dtOutput.Columns.Add("budget");
                    dtOutput.Columns.Add("ytdplan");
                    dtOutput.Columns.Add("ytd");
                    dtOutput.Columns.Add("variance");
                    dtOutput.Columns.Add("variancePct");
                      if (!string.IsNullOrEmpty(search))
                      {
                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }
                        for (int i = 0; i < searchList.Count; i++)
                        {
                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("cust_name");
                            dtOutput.Columns.Add("cust_number");
                            dtOutput.Columns.Add("customer_type");
                            dtOutput.Columns.Add("Branch");
                            dtOutput.Columns.Add("EngineerName");
                            dtOutput.Columns.Add("sales_year_0");
                            dtOutput.Columns.Add("budget");
                            dtOutput.Columns.Add("ytdplan");
                            dtOutput.Columns.Add("ytd");
                            dtOutput.Columns.Add("variance");
                            dtOutput.Columns.Add("variancePct");

                            filteredRows = dt.Select("CONVERT(cust_name, System.String) LIKE '%" + searchList[i].Trim()
                              + "%' OR CONVERT(cust_number, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(customer_type, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(Branch, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(EngineerName, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(sales_year_0, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(ytdplan, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(variance, System.String) LIKE'%" + searchList[i].Trim()
                              + "%' OR CONVERT(variancePct, System.String) LIKE'%" + searchList[i].Trim()
                              + "%'");
                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = dtOutput;
                        }
                    }
                    //else{

                    //     dtOutput = dt;
                    //}
                    string YTD_sale_17 = "0";
                    string B = "0";
                    string YTD_plan = "0";
                    string YTD_sale_18 = "0";
                    string variance = "0";
                    decimal variance_prc = 0;

                    string YTD_sale_17_cp = "0";
                    string B_cp = "0";
                    string YTD_plan_cp = "0";
                    string YTD_sale_18_cp = "0";
                    string variance_cp = "0";
                    decimal variance_prc_cp = 0;

                    string YTD_sale_17_total = "0";
                    string B_total = "0";
                    string YTD_plan_total = "0";
                    string YTD_sale_18_total = "0";
                    string variance_total = "0";
                    decimal variance_prc_total = 0;


                    if (dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")) != null)
                    {

                        YTD_sale_17 = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["sales_year_0"])).ToString());
                        B = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["budget"])).ToString());
                        YTD_plan = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["ytdplan"])).ToString());
                        YTD_sale_18 = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["ytd"])).ToString());
                        variance = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("C")).Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        if (Convert.ToDecimal(YTD_plan) != 0)
                            variance_prc = Math.Round((Convert.ToDecimal(variance) / Convert.ToDecimal(YTD_plan)) * 100, 0);
                    }

                    if (dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")) != null)
                    {
                        YTD_sale_17_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["sales_year_0"])).ToString());
                        B_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["budget"])).ToString());
                        YTD_plan_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["ytdplan"])).ToString());
                        YTD_sale_18_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["ytd"])).ToString());
                        variance_cp = Convert.ToString(dt.AsEnumerable().Where(y => y.Field<string>("customer_type").Equals("D")).Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        if (Convert.ToDecimal(YTD_plan_cp) != 0)
                            variance_prc_cp = Math.Round((Convert.ToDecimal(variance_cp) / Convert.ToDecimal(YTD_plan_cp)) * 100, 0);
                    }

                    if (dt.Rows.Count !=null)
                    {
                        YTD_sale_17_total = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_year_0"])).ToString());
                        B_total = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString());
                        YTD_plan_total = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytdplan"])).ToString());
                        YTD_sale_18_total = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString());
                        variance_total = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        if (Convert.ToDecimal(YTD_plan_total) != 0)
                            variance_prc_total = Math.Round((Convert.ToDecimal(variance_total) / Convert.ToDecimal(YTD_plan_total)) * 100, 0);
                    }


                    hdndata.Add(Convert.ToString(YTD_sale_17));
                    hdndata.Add(Convert.ToString(B));
                    hdndata.Add(Convert.ToString(YTD_plan));
                    hdndata.Add(Convert.ToString(YTD_sale_18));
                    hdndata.Add(Convert.ToString(variance));
                    hdndata.Add(Convert.ToString(variance_prc));

                    hdndata.Add(Convert.ToString(YTD_sale_17_cp));
                    hdndata.Add(Convert.ToString(B_cp));
                    hdndata.Add(Convert.ToString(YTD_plan_cp));
                    hdndata.Add(Convert.ToString(YTD_sale_18_cp));
                    hdndata.Add(Convert.ToString(variance_cp));
                    hdndata.Add(Convert.ToString(variance_prc_cp));

                    hdndata.Add(Convert.ToString(YTD_sale_17_total));
                    hdndata.Add(Convert.ToString(B_total));
                    hdndata.Add(Convert.ToString(YTD_plan_total));
                    hdndata.Add(Convert.ToString(YTD_sale_18_total));
                    hdndata.Add(Convert.ToString(variance_total));
                    hdndata.Add(Convert.ToString(variance_prc_total));
                }
            }
            return hdndata;
        }



        [WebMethod]
        public static List<string> setGrandtotalpro(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata1 = new List<string>();
            List<string> searchList1 = new List<string>();
            DataRow[] filteredRows;
            HttpContext.Current.Session["variance_search"] = search;
            dt = (DataTable)HttpContext.Current.Session["variance_reportpro"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(search))
                    {
                        if (search.Contains(' '))
                        {
                            searchList1 = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList1.Add(search);
                        }
                        for (int i = 0; i < searchList1.Count; i++)
                        {

                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("prdct_name");
                            dtOutput.Columns.Add("sales_year_0");
                            dtOutput.Columns.Add("budget");
                            dtOutput.Columns.Add("ytdplan");
                            dtOutput.Columns.Add("ytd");
                            dtOutput.Columns.Add("variance");
                            dtOutput.Columns.Add("pvariancePct");

                            filteredRows = dt.Select("CONVERT(prdct_name, System.String) LIKE '%" + searchList1[i].Trim()
                              + "%' OR CONVERT(sales_year_0, System.String) LIKE'%" + searchList1[i].Trim()
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + searchList1[i].Trim()
                              + "%' OR CONVERT(ytdplan, System.String) LIKE'%" + searchList1[i].Trim()
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + searchList1[i].Trim()
                              + "%' OR CONVERT(variance, System.String) LIKE'%" + searchList1[i].Trim()
                              + "%' OR CONVERT(pvariancePct, System.String) LIKE'%" + searchList1[i].Trim()
                              + "%'");
                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = dtOutput;
                        }
                    }

                    string YTDSALE17_PRO = "0";
                    string B2018_PRO = "0";
                    string YTDPLAN2018_PRO = "0";
                    string YTDSALE2018_PRO = "0";
                    string VARIANCE_PRO = "0";
                    decimal VARIANCE_PRC_PRO = 0;

                    if (dt.Rows.Count != null)
                    {
                        YTDSALE17_PRO = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_year_0"])).ToString());
                        B2018_PRO = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString());
                        YTDPLAN2018_PRO = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytdplan"])).ToString());
                        YTDSALE2018_PRO = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString());
                        VARIANCE_PRO = Convert.ToString(dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["variance"])).ToString());
                        if (Convert.ToDecimal(YTDPLAN2018_PRO) != 0)
                            VARIANCE_PRC_PRO = Math.Round((Convert.ToDecimal(VARIANCE_PRO) / Convert.ToDecimal(YTDPLAN2018_PRO)) * 100, 0);
                    }

                    hdndata1.Add(Convert.ToString(YTDSALE17_PRO));
                    hdndata1.Add(Convert.ToString(B2018_PRO));
                    hdndata1.Add(Convert.ToString(YTDPLAN2018_PRO));
                    hdndata1.Add(Convert.ToString(YTDSALE2018_PRO));
                    hdndata1.Add(Convert.ToString(VARIANCE_PRO));
                    hdndata1.Add(Convert.ToString(VARIANCE_PRC_PRO));
                }
            }
            return hdndata1;
        }
          #endregion
    }
}