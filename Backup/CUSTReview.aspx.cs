﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class CUSTReview : System.Web.UI.Page
    {   
        Reports objReports = new Reports();
        Budget objBudgets = new Budget();
        Review objRSum = new Review();
        CommonFunctions objCom = new CommonFunctions();
        public static string cter;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {
                hdnsearch.Value = "";
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                cter = null;

                if (Session["RoleId"].ToString() == "HO" || Session["RoleId"].ToString() == "TM")
                {
                    if (Session["cter"] == null)
                    {
                        Session["cter"] = "TTA";
                        cter = "TTA";
                    }

                    if (Convert.ToString(Session["cter"]) == "DUR")
                    {
                        rdBtnDuraCab.Checked = true;
                        rdBtnTaegutec.Checked = false;
                        cter = "DUR";
                    }
                    else
                    {
                        rdBtnTaegutec.Checked = true;
                        rdBtnDuraCab.Checked = false;
                        cter = "TTA";
                    }
                    cterDiv.Visible = true;
                    LoadBranches();
                    // LoadCustomers();
                    ddlBranchList_SelectedIndexChanged(null, null);
                    // SalesEngList_SelectedIndexChanged(null, null);

                }

                else if (Session["RoleId"].ToString() == "BM")
                {
                    cterDiv.Visible = false;
                    string username = Session["UserName"].ToString();
                    string branchcode = Session["BranchCode"].ToString();
                    string branchDec = Session["BranchDesc"].ToString();
                    Session["SelectedBranchList"] = "'" + branchcode + "'";
                    //bind branch
                    ddlBranchList.Items.Insert(0, branchcode);
                    divBranch.Visible = false;
                    // sales engineers loading
                    DataTable dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);
                    if (dtSalesEngDetails != null)
                    {
                        ddlSalesEngineerList.DataSource = dtSalesEngDetails;
                        ddlSalesEngineerList.DataTextField = "EngineerName";
                        ddlSalesEngineerList.DataValueField = "EngineerId";
                        ddlSalesEngineerList.DataBind();
                        ddlSalesEngineerList.Items.Insert(0, "ALL");
                    }
                    else
                    {
                        ddlSalesEngineerList.DataSource = null;
                        ddlSalesEngineerList.DataBind();
                    }
                    //customer loading
                    DataTable dtData = objBudgets.LoadCustomerDetails(null, null);
                    ddlCustomerList.DataSource = dtData;
                    if (dtData != null)
                    {
                        ddlSalesEngineerList.DataSource = dtData;
                        ddlCustomerList.DataTextField = "customer_short_name";
                        ddlBranchList.DataValueField = "customer_number";
                        ddlCustomerList.DataBind();
                        ddlCustomerList.Items.Insert(0, "ALL");
                    }
                    //  LoadCustomers();
                    SalesEngList_SelectedIndexChanged(null, null);

                }

                else if (Session["RoleId"].ToString() == "SE")
                {
                    cterDiv.Visible = false;
                    string username = Session["UserName"].ToString();
                    string branchcode = Session["BranchCode"].ToString();
                    string branchDec = Session["BranchDesc"].ToString();
                    // string customer = Convert.ToString(Session["EngineerId"]);
                    Session["SelectedBranchList"] = "'" + branchcode + "'";
                    Session["SelectedSalesEngineers"] = "'" + strUserId + "'";
                    //bind branch
                    ddlBranchList.Items.Insert(0, branchcode);
                    divBranch.Visible = false;
                    //bind Sales engineer
                    ddlSalesEngineerList.Items.Insert(0, strUserId);
                    divSE.Visible = false;
                    LoadCustomers();
                    objRSum.flag = "CustomerType";
                    objRSum.salesengineer_id = strUserId;
                    DataTable dtData1 = objRSum.getFilterAreaValue(objRSum);
                    if (dtData1.Rows.Count != 0)
                    {
                        ddlCustomerList.DataSource = dtData1;
                        ddlCustomerList.DataTextField = "customer_short_name";
                        ddlCustomerList.DataValueField = "customer_number";
                        ddlCustomerList.DataBind();
                        ddlCustomerList.Items.Insert(0, "ALL");
                    }
                    else
                    {
                        ddlCustomerList.DataSource = dtData1;
                        ddlCustomerList.DataTextField = "customer_short_name";
                        ddlCustomerList.DataValueField = "customer_number";
                        ddlCustomerList.DataBind();
                        ddlCustomerList.Items.Insert(0, "NO CUSTOMER");
                    }

                }
                txtYear.Items.Insert(0, new ListItem(Convert.ToString(DateTime.Now.Year), Convert.ToString(DateTime.Now.Year)));
                txtYear.Items.Insert(1, new ListItem(Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1), Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1)));
            }
        }
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            DataTable dtData = new DataTable();
            objRSum.BranchCode = userId; 
            objRSum.roleId = roleId;
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);
            ddlBranchList.DataSource = dtData;
            ddlBranchList.DataTextField = "BranchDesc";
            ddlBranchList.DataValueField = "BranchCode";
            ddlBranchList.DataBind();
            ddlBranchList.Items.Insert(0, "ALL");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void LoadCustomers()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string customer = Convert.ToString(Session["EngineerId"]);
            DataTable dtData = new DataTable();
            //objRSum.customer_number = userId;
            //objRSum.roleId = roleId;
            //objRSum.flag = "Branch";
            //objRSum.cter = cter;
            dtData = objBudgets.LoadCustomerDetails(null, null);
            ddlCustomerList.DataSource = dtData;
            // ddlCustomerList.DataTextField = "BranchDesc";
            ddlCustomerList.DataValueField = "customer_short_name";
            ddlCustomerList.DataBind();
            ddlCustomerList.Items.Insert(0, "ALL");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void ddlBranchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            hdnsearch.Value = "";
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            objRSum.BranchCode = roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL" ? userId : ddlBranchList.SelectedItem.Value;
            objRSum.roleId = roleId;
            objRSum.flag = "SalesEngineer";
            objRSum.cter = cter;
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);

            if (dtData.Rows.Count != 0)
            {
                ddlSalesEngineerList.DataSource = dtData;
                ddlSalesEngineerList.DataTextField = "EngineerName";
                ddlSalesEngineerList.DataValueField = "EngineerId";
                ddlSalesEngineerList.DataBind();
                ddlSalesEngineerList.Items.Insert(0, "ALL");
            }
            else
            {
                ddlSalesEngineerList.DataSource = dtData;
                ddlSalesEngineerList.DataTextField = "EngineerName";
                ddlSalesEngineerList.DataValueField = "EngineerId";
                ddlSalesEngineerList.DataBind();
                ddlSalesEngineerList.Items.Insert(0, "NO SALES ENGINEER");
            }

            objRSum.flag = "CustomerType";
            DataTable dtData1 = objRSum.getFilterAreaValue(objRSum);

            if (dtData1.Rows.Count != 0)
            {
                ddlCustomerList.DataSource = dtData1;
                ddlCustomerList.DataTextField = "customer_short_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "ALL");
            }
            else
            {
                ddlCustomerList.DataSource = dtData1;
                ddlCustomerList.DataTextField = "customer_short_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "NO CUSTOMER");
            }

             custargetvssales.DataSource = null;
             custargetvssales.DataBind();
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void SalesEngList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            int count = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            foreach (ListItem val in ddlSalesEngineerList.Items)
            {
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }

            name_code = "'" + name_code;
            string SalesengList = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
            string SalesengnameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

            if (count == ddlSalesEngineerList.Items.Count)
            {
                Session["SelectedSalesEngineers"] = "ALL";
            }

            else
            {
                Session["SelectedSalesEngineers"] = SalesengList;
            }

            if (SalesengList == "'ALL'")
            {
                SalesengList = "";
            }
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
           // string branchcode = Convert.ToString(Session["SelectedBranchList"]);
            string branchcode = ddlBranchList.SelectedItem.Value;
            if (branchcode == "")
            {
                branchcode = "ALL";
            }
            objRSum.BranchCode = (roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL") ? userId : branchcode;
            objRSum.salesengineer_id = SalesengList.ToString() == "ALL" || SalesengList.ToString() == "" ? null : SalesengList;
           // objRSum.customer_type = ddlcustomertype.SelectedItem.Value;
            objRSum.roleId = roleId;
            objRSum.flag = "CustomerType";
            objRSum.cter = cter;
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);
          
            if (dtData.Rows.Count != 0)
            {
                ddlCustomerList.DataSource = dtData;
                ddlCustomerList.DataTextField = "customer_short_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "ALL");
            }
            else
            {
                ddlCustomerList.DataSource = dtData;
                ddlCustomerList.DataTextField = "customer_short_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "NO CUSTOMER");
            }

            //objRSum.flag = "SalesEngineer";
            //DataTable dtData1 = objRSum.getFilterAreaValue(objRSum);

            //if (dtData1.Rows.Count != 0)
            //{
            //    ddlSalesEngineerList.DataSource = dtData1;
            //    ddlSalesEngineerList.DataTextField = "EngineerName";
            //    ddlSalesEngineerList.DataValueField = "EngineerId";
            //    ddlSalesEngineerList.DataBind();
            //    ddlSalesEngineerList.Items.Insert(0, "ALL");
            //}
            //else
            //{
            //    ddlSalesEngineerList.DataSource = dtData1;
            //    ddlSalesEngineerList.DataTextField = "EngineerName";
            //    ddlSalesEngineerList.DataValueField = "EngineerId";
            //    ddlSalesEngineerList.DataBind();
            //    ddlSalesEngineerList.Items.Insert(0, "NO CUSTOMER");
            //}
            custargetvssales.DataSource = null;
            custargetvssales.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        //protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //    hdnsearch.Value = "";
        //    string roleId = Session["RoleId"].ToString();
        //    string userId = Session["UserId"].ToString();
        //    string branchcode = Session["BranchCode"].ToString();
            //objRSum.BranchCode = roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL" ? userId : ddlBranchList.SelectedItem.Value;
            //objRSum.roleId = roleId;
            //objRSum.flag = "CustomerType";
            //objRSum.cter = cter;
            //DataTable dtData = objRSum.getFilterAreaValue(objRSum);

            //if (dtData.Rows.Count != 0)
            //{
            //    ddlCustomerList.DataSource = dtData;
            //    ddlCustomerList.DataSource = "customer_short_name";
            //    ddlCustomerList.DataSource = "customer_short_name";
            //    ddlCustomerList.DataBind();
            //    ddlCustomerList.Items.Insert(0, "ALL");
            //}
            //else
            //{
            //    ddlCustomerList.DataSource = dtData;
            //    ddlCustomerList.DataTextField = "customer_short_name";
            //    ddlCustomerList.DataValueField = "customer_short_name";
            //    ddlCustomerList.DataBind();
            //    ddlCustomerList.Items.Insert(0, "NO SALES ENGINEER");
            //}
            //custargetvssales.DataSource = null;
            //custargetvssales.DataBind();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
         
      //  }

        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            hdnsearch.Value = "";
            if (rdBtnTaegutec.Checked)
            {
                Session["cter"] = "TTA";
                cter = "TTA";
            }
            if (rdBtnDuraCab.Checked)
            {
                Session["cter"] = "DUR";
                cter = "DUR";
            }
            custargetvssales.DataSource = null;
            custargetvssales.DataBind();
            LoadBranches();
            LoadCustomers();
            ddlBranchList_SelectedIndexChanged(null, null);
            SalesEngList_SelectedIndexChanged(null, null);
        }

        protected void reports_Click(object sender, EventArgs e)
        {
            try
            {
                hdnsearch.Value = "";
                DataTable dt = new DataTable();
                objRSum.Year = Convert.ToInt32(txtYear.SelectedValue);
                objRSum.cter = Convert.ToString(cter);
                objRSum.BranchCode = Convert.ToString(ddlBranchList.SelectedValue);
                objRSum.salesengineer_id = Convert.ToString(ddlSalesEngineerList.SelectedValue);                
                objRSum.customer_number = Convert.ToString(ddlCustomerList.SelectedValue);
                if (objRSum.BranchCode == "ALL") { objRSum.BranchCode = null; }
                if (objRSum.salesengineer_id == "ALL") { objRSum.salesengineer_id = null; }
                if (objRSum.customer_number == "ALL") { objRSum.customer_number = null; }

                dt = objRSum.cust_target_vs_sales(objRSum);
                Session["outputdt"] = dt;
                if (dt != null && dt.Rows.Count > 0)
                {
                    custargetvssales.DataSource = dt;
                    lblResult.Text = "All values are in lakh.";
                }
                else
                {
                    custargetvssales.DataSource = null;
                    lblResult.Text = "No records to display.";
                }
                custargetvssales.DataBind();
                setsessions(dt);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        private void setsessions(DataTable dt)
        {
            Session["annual"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Annual_Budget"])).ToString();
            Session["jan"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Jan_Sales"])).ToString();
            Session["feb"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Feb_Sales"])).ToString();
            Session["mar"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Mar_Sales"])).ToString();
            Session["apr"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Apr_Sales"])).ToString();
            Session["may"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["May_Sales"])).ToString();
            Session["jun"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Jun_Sales"])).ToString();
            Session["jul"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Jul_Sales"])).ToString();
            Session["aug"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Aug_Sales"])).ToString();
            Session["sep"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Sep_Sales"])).ToString();
            Session["oct"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Oct_Sales"])).ToString();
            Session["nov"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Nov_Sales"])).ToString();
            Session["dec"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Dec_Sales"])).ToString();

            Session["tot"] = (Convert.ToDecimal(Session["jan"]) + Convert.ToDecimal(Session["feb"]) + Convert.ToDecimal(Session["mar"]) + Convert.ToDecimal(Session["apr"]) + Convert.ToDecimal(Session["may"]) + Convert.ToDecimal(Session["jun"]) + Convert.ToDecimal(Session["jul"]) + Convert.ToDecimal(Session["aug"]) + Convert.ToDecimal(Session["sep"]) + Convert.ToDecimal(Session["oct"]) + Convert.ToDecimal(Session["nov"]) + Convert.ToDecimal(Session["dec"]));
            Session["ach"] = (Convert.ToDecimal(Session["annual"]) == 0) ? 0 : Math.Round((Convert.ToDecimal(Session["tot"]) / Convert.ToDecimal(Session["annual"])) * 100, 2);

            hdnannual.Value = Convert.ToString(Session["annual"]);
            hdnjan.Value = Convert.ToString(Session["jan"]);
            hdnfeb.Value = Convert.ToString(Session["feb"]);
            hdnmar.Value = Convert.ToString(Session["mar"]);
            hdnapr.Value = Convert.ToString(Session["apr"]);
            hdnmay.Value = Convert.ToString(Session["may"]);
            hdnjun.Value = Convert.ToString(Session["jun"]);
            hdnjul.Value = Convert.ToString(Session["jul"]);
            hdnaug.Value = Convert.ToString(Session["aug"]);
            hdnsep.Value = Convert.ToString(Session["sep"]);
            hdnoct.Value = Convert.ToString(Session["oct"]);
            hdnnov.Value = Convert.ToString(Session["nov"]);
            hdndec.Value = Convert.ToString(Session["dec"]);
            hdntot.Value = Convert.ToString(Session["tot"]);
            hdnach.Value = Convert.ToString(Session["ach"]);
        }

        protected void export_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                DataTable dtNew = new DataTable();
                objRSum.Year = Convert.ToInt32(txtYear.SelectedValue);
                objRSum.cter = Convert.ToString(cter);
                objRSum.BranchCode = Convert.ToString(ddlBranchList.SelectedValue);
                objRSum.salesengineer_id = Convert.ToString(ddlSalesEngineerList.SelectedValue);
                objRSum.customer_number = Convert.ToString(ddlCustomerList.SelectedValue);
                if (objRSum.BranchCode == "ALL") { objRSum.BranchCode = null; }
                if (objRSum.salesengineer_id == "ALL") { objRSum.salesengineer_id = null; }
                if (objRSum.customer_number == "ALL") { objRSum.customer_number = null; }               
                dt = objRSum.cust_target_vs_sales(objRSum);
                if (dt != null)
                {
                    custargetvssales.DataSource = dt;
                }
                else
                {
                    custargetvssales.DataSource = null;
                }
                custargetvssales.DataBind();
                dt = CheckforSearch(dt);
                setsessions(dt);
                dtNew = addGrandTotalToExceltable(dt);
                ExporttoExcel(dtNew, dt, "Cust_Target_vs_Sales");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        private DataTable CheckforSearch(DataTable dt)
        {
            DataTable dtOutput = new DataTable();
             string search = hdnsearch.Value;
           // string search = Convert.ToString(Session["var_search"]);
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            try
            {
                if (!string.IsNullOrEmpty(search))
                {
                    if (search.Contains(' '))
                    {
                        searchList = search.Split(' ').ToList();
                    }
                    else
                    {
                        searchList.Add(search);
                    }

                    for (int i = 0; i < searchList.Count; i++)
                    {
                        dtOutput = new DataTable();
                        dtOutput.Columns.Add("customer_number");
                        dtOutput.Columns.Add("customer_name");
                        dtOutput.Columns.Add("BRANCH");
                        dtOutput.Columns.Add("SE_NAME");
                        dtOutput.Columns.Add("Annual_Budget");
                        dtOutput.Columns.Add("Jan_Sales");
                        dtOutput.Columns.Add("Feb_Sales");
                        dtOutput.Columns.Add("Mar_Sales");
                        dtOutput.Columns.Add("Apr_Sales");
                        dtOutput.Columns.Add("May_Sales");
                        dtOutput.Columns.Add("Jun_Sales");
                        dtOutput.Columns.Add("Jul_Sales");
                        dtOutput.Columns.Add("Aug_Sales");
                        dtOutput.Columns.Add("Sep_Sales");
                        dtOutput.Columns.Add("Oct_Sales");
                        dtOutput.Columns.Add("Nov_Sales");
                        dtOutput.Columns.Add("Dec_Sales");
                        dtOutput.Columns.Add("Total_Sales");
                        dtOutput.Columns.Add("Achieved");

                        filteredRows = dt.Select("customer_number LIKE '%" + searchList[i].Trim()
                            + "%' OR customer_name LIKE '%" + searchList[i].Trim()
                            + "%' OR BRANCH LIKE '%" + searchList[i].Trim()
                            + "%' OR SE_NAME LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Annual_Budget, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Jan_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Feb_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Mar_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Apr_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(May_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Jun_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Jul_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Aug_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Sep_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Oct_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Nov_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Dec_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Total_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Achieved, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%'");
                        foreach (DataRow dr in filteredRows)
                        {
                            dtOutput.Rows.Add(dr.ItemArray);
                        }
                        dt = dtOutput;
                    }
                }
                else
                {
                    dtOutput = dt;
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return dtOutput;
        }

        private DataTable addGrandTotalToExceltable(DataTable dt)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["SE_NAME"] = "GRAND TOTAL : ";
                dr["Annual_Budget"] = Convert.ToString(Session["annual"]);
                dr["Jan_Sales"] = Convert.ToString(Session["jan"]);
                dr["Feb_Sales"] = Convert.ToString(Session["feb"]);
                dr["Mar_Sales"] = Convert.ToString(Session["mar"]);
                dr["Apr_Sales"] = Convert.ToString(Session["apr"]);
                dr["May_Sales"] = Convert.ToString(Session["may"]);
                dr["Jun_Sales"] = Convert.ToString(Session["jun"]);
                dr["Jul_Sales"] = Convert.ToString(Session["jul"]);
                dr["Aug_Sales"] = Convert.ToString(Session["aug"]);
                dr["Sep_Sales"] = Convert.ToString(Session["sep"]);
                dr["Oct_Sales"] = Convert.ToString(Session["oct"]);
                dr["Nov_Sales"] = Convert.ToString(Session["nov"]);
                dr["Dec_Sales"] = Convert.ToString(Session["dec"]);
                dr["Total_Sales"] = Convert.ToString(Session["tot"]);
                dr["Achieved"] = Convert.ToString(Session["ach"]) + "%";
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return dt;
        }

        private void ExporttoExcel(DataTable dtNew, DataTable dtExport, string filename)
        {
            try
            {
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", filename + ".xls"));
                Response.ContentType = "application/ms-excel";
               
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a table to contain the grid
                        Table table = new Table();
                        table.GridLines = custargetvssales.GridLines;
                        if (custargetvssales.HeaderRow != null)
                        {
                            PrepareControlForExport(custargetvssales.HeaderRow);
                            table.Rows.Add(custargetvssales.HeaderRow);
                        }
                        custargetvssales.DataSource = dtNew;
                        custargetvssales.DataBind();
                        foreach (GridViewRow row in custargetvssales.Rows)
                        {
                            PrepareControlForExport(row);
                            table.Rows.Add(row);
                        }

                        table.Rows[0].Height = 30;
                        table.Rows[0].BackColor = Color.LightSeaGreen;
                        table.Rows[0].Font.Bold = true;
                        sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px; '>Customer Annual Target Vs. Monthly Sales</td></table>");
                        sw.WriteLine("<table style='margin-left: 200px;'>");

                        //  render the table into the htmlwriter
                        if (Session["RoleId"].ToString() == "HO")
                        {
                            string territory;
                            if (rdBtnTaegutec.Checked)
                            {
                                territory = "TAEGUTEC";
                            }
                            else
                            {
                                territory = "DURACARB";
                            }
                            sw.WriteLine("<tr><td></td><td></td><td></td><td style='font-weight: bold;'>TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");
                        }
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>YEAR :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + Convert.ToString(txtYear.Text) + "</td></tr>");

                        if (Session["RoleId"].ToString() == "BM")
                        {
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["BranchDesc"]) + "</td></tr>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(ddlSalesEngineerList.SelectedItem) + "</td></tr>");
                        }
                        else if (Session["RoleId"].ToString() == "SE")
                        {
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["BranchDesc"]) + "</td></tr>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["UserName"]) + "</td></tr>");
                        }
                        else
                        {
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(ddlBranchList.SelectedItem) + "</td></tr>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(ddlSalesEngineerList.SelectedItem) + "</td></tr>");

                        }
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight:bold;'> CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(ddlCustomerList.SelectedItem) + "</td></tr>");

                        if (!String.IsNullOrEmpty(hdnsearch.Value))
                        {
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SEARCH TEXT :" + "</td><td colspan=8 style='font-style: italic;  text-align: left;'>" + Convert.ToString(hdnsearch.Value) + "</td></tr>");
                        }
                        sw.WriteLine("</table><br/>");
                        table.RenderControl(htw);
                    }
                    Response.Write(sw.ToString());
                    
                    custargetvssales.DataSource = dtExport;
                    custargetvssales.DataBind();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        private static void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    //control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is HiddenField)
                {
                    control.Controls.Remove(current);
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }
                if (current.HasControls())
                {
                    PrepareControlForExport(current);
                }
            }
        }

        [WebMethod]
        public static List<string> setGrandtotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            dt = (DataTable)HttpContext.Current.Session["outputdt"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(search))
                    {
                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }

                        for (int i = 0; i < searchList.Count; i++)
                        {
                            dtOutput = new DataTable();                         
                            dtOutput.Columns.Add("customer_number");
                            dtOutput.Columns.Add("customer_name");
                            dtOutput.Columns.Add("BRANCH");
                            dtOutput.Columns.Add("SE_NAME");
                            dtOutput.Columns.Add("Annual_Budget");
                            dtOutput.Columns.Add("Jan_Sales");
                            dtOutput.Columns.Add("Feb_Sales");
                            dtOutput.Columns.Add("Mar_Sales");
                            dtOutput.Columns.Add("Apr_Sales");
                            dtOutput.Columns.Add("May_Sales");
                            dtOutput.Columns.Add("Jun_Sales");
                            dtOutput.Columns.Add("Jul_Sales");
                            dtOutput.Columns.Add("Aug_Sales");
                            dtOutput.Columns.Add("Sep_Sales");
                            dtOutput.Columns.Add("Oct_Sales");
                            dtOutput.Columns.Add("Nov_Sales");
                            dtOutput.Columns.Add("Dec_Sales");
                            dtOutput.Columns.Add("Total_Sales");
                            dtOutput.Columns.Add("Achieved");

                            filteredRows = dt.Select("customer_number LIKE '%" + searchList[i].Trim()
                                + "%' OR customer_name LIKE '%" + searchList[i].Trim()
                                + "%' OR BRANCH LIKE '%" + searchList[i].Trim()
                                + "%' OR SE_NAME LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Annual_Budget, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Jan_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Feb_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Mar_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Apr_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(May_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Jun_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Jul_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Aug_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Sep_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Oct_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Nov_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Dec_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Total_Sales, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Achieved, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%'");
                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = dtOutput;
                        }
                    }
                    else
                    {
                        dtOutput = dt;
                    }
                    string annual = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Annual_Budget"])).ToString();
                    string jan = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Jan_Sales"])).ToString();
                    string feb = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Feb_Sales"])).ToString();
                    string mar = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Mar_Sales"])).ToString();
                    string apr = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Apr_Sales"])).ToString();
                    string may = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["May_Sales"])).ToString();
                    string jun = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Jun_Sales"])).ToString();
                    string jul = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Jul_Sales"])).ToString();
                    string aug = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Aug_Sales"])).ToString();
                    string sep = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Sep_Sales"])).ToString();
                    string oct = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Oct_Sales"])).ToString();
                    string nov = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Nov_Sales"])).ToString();
                    string dec = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Dec_Sales"])).ToString();

                    decimal tot = (Convert.ToDecimal(jan) + Convert.ToDecimal(feb) + Convert.ToDecimal(mar) + Convert.ToDecimal(apr) + Convert.ToDecimal(may) + Convert.ToDecimal(jun) + Convert.ToDecimal(jul) + Convert.ToDecimal(aug) + Convert.ToDecimal(sep) + Convert.ToDecimal(oct) + Convert.ToDecimal(nov) + Convert.ToDecimal(dec));
                    decimal ach = (Convert.ToDecimal(annual) == 0) ? 0 : Math.Round((Convert.ToDecimal(tot) / Convert.ToDecimal(annual)) * 100, 2);
                    hdndata.Add(Convert.ToString(annual));
                    hdndata.Add(Convert.ToString(jan));
                    hdndata.Add(Convert.ToString(feb));
                    hdndata.Add(Convert.ToString(mar));
                    hdndata.Add(Convert.ToString(apr));
                    hdndata.Add(Convert.ToString(may));
                    hdndata.Add(Convert.ToString(jun));
                    hdndata.Add(Convert.ToString(jul));
                    hdndata.Add(Convert.ToString(aug));
                    hdndata.Add(Convert.ToString(sep));
                    hdndata.Add(Convert.ToString(oct));
                    hdndata.Add(Convert.ToString(nov));
                    hdndata.Add(Convert.ToString(dec));
                    hdndata.Add(Convert.ToString(tot));
                    hdndata.Add(Convert.ToString(ach));
                }
            }
            return hdndata;
        }
    }
}
