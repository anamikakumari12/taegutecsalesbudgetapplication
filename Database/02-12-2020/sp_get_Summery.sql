USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_get_Summery]    Script Date: 02-12-2020 15:33:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getSummery 'ALL', 'ALL',  'ALL','ALL','TTA'
ALTER Procedure [dbo].[sp_get_Summery]
							(@p_branch_code		VARCHAR(20),
							@p_salesengineer_id	VARCHAR(50),
							@p_cust_dist_flag	VARCHAR(10),
							@p_customer_number	VARCHAR(10),
							@cter				VARCHAR(10) = NULL)
AS
BEGIN
	Declare @ActualYear int 
	Declare @BudgetYear int
	Declare @ActualMonth int
	set @ActualYear = [dbo].[get_profile_value]('B_ACTUAL_YEAR')
	set @BudgetYear= [dbo].[get_profile_value]('B_BUDGET_YEAR')
	set @ActualMonth = [dbo].[get_profile_value]('B_ACTUAL_MONTH')
	TRUNCATE TABLE tt_temp_summary_master
	IF (@p_customer_number <> 'ALL')
	Insert into tt_temp_summary_master
		SELECT 	
		'',
		tmp.gold_flag,
					tmp.top_flag,
					tmp.five_years_flag,
					tmp.bb_flag,
					tmp.SPC_flag,
					'' item_id,
					tmp.item_family_id,
					item_family_name,
					tmp.item_sub_family_id,
					item_sub_family_name,
					'' item_group_code,
					tmp.insert_or_tool_flag,
					tmp.item_code,
					'' item_short_name,
					tmp.item_description,
					''  Customer_region,

					sum(sales_qty_year_2) sales_qty_year_2,
					sum(sales_qty_year_1) sales_qty_year_1,
					--CEILING(CONVERT(DECIMAL(22,5),sum(sales_qty_year_0)) * (CONVERT(DECIMAL(22,5), 12) / @ActualMonth)) sales_qty_year_0,
					CEILING([dbo].[get_budget](@BudgetYear-1, tmp.item_code,  @p_customer_number, 'Q')) sales_qty_year_0,
					CEILING([dbo].[get_budget](@BudgetYear, tmp.item_code,  @p_customer_number, 'Q')) estimate_qty_next_year,

					sum(sales_value_year_2) sales_value_year_2,
					sum(sales_value_year_1) sales_value_year_1,
					--CEILING(CONVERT(DECIMAL(22,5),sum(sales_value_year_0)) * (CONVERT(DECIMAL(22,5), 12) / @ActualMonth)) sales_value_year_0,
					CEILING([dbo].[get_budget](@BudgetYear-1, tmp.item_code, @p_customer_number, 'V')) sales_value_year_0,
					CEILING([dbo].[get_budget](@BudgetYear, tmp.item_code, @p_customer_number, 'V')) estimate_value_next_year,
					tmp.ten_years_flag
		
					FROM (	SELECT 
							ig.gold_flag,
							ig.top_flag,
							ig.five_years_flag,
							ig.bb_flag,
							ig.SPC_flag,
							ig.ten_years_flag,
							'' item_id,
							itm.item_family_id,
							item_family_name,
							itm.item_sub_family_id,
							item_sub_family_name,
							'' item_group_code,
							itm.insert_or_tool_flag,
							itm.item_code,
							'' item_short_name,
							itm.item_description,
							''  Customer_region,
							-- sum(s.quantity)
							sales_qty_year_2=case when s.inv_year = (@ActualYear-2) then s.quantity END,
							sales_qty_year_1 = case when s.inv_year = (@ActualYear-1) then s.quantity END,
							sales_qty_year_0 = case when s.inv_year = (@ActualYear)  AND  s.inv_month <= @ActualMonth then s.quantity END,

							sales_value_year_2= case when s.inv_year = (@ActualYear-2) then s.value END,
							sales_value_year_1 = case when s.inv_year = (@ActualYear-1) then s.value END,
							sales_value_year_0 = case when s.inv_year = (@ActualYear)  AND  s.inv_month <= @ActualMonth then s.value END

							FROM   tt_item_master itm,
								   tt_item_family fly,
								   tt_item_sub_family sub_fly,
								   tt_salesnumbers s ,
								   tt_item_group ig

							WHERE  itm.item_family_id = fly.item_family_id
							AND		itm.item_code=ig.item_code
							AND		ig.budget_year=@BudgetYear
							AND    itm.item_sub_family_id = sub_fly.item_sub_family_id 
							AND    sub_fly.item_sub_family_status = 'ACTIVE' AND itm.item_status = 'Y' AND fly.item_family_status = 'ACTIVE'
							AND    itm.item_code = s.item_code 
							AND    s.customer_number = @p_customer_number
					
					UNION ALL
					SELECT 
							ig.gold_flag,
							ig.top_flag,
							ig.five_years_flag,
							ig.bb_flag,
							ig.SPC_flag,
							ig.ten_years_flag,
							'' item_id,
							itm.item_family_id,
							item_family_name,
							itm.item_sub_family_id,
							item_sub_family_name,
							'' item_group_code,
							itm.insert_or_tool_flag,
							itm.item_code,
							'' item_short_name,
							itm.item_description,
							''  Customer_region,
							-- sum(s.quantity)
							0 sales_qty_year_2,
							0 sales_qty_year_1 ,
							0 sales_qty_year_0 ,

							0 sales_value_year_2,
							0 sales_value_year_1 ,
							0 sales_value_year_0 

							FROM   tt_item_master itm,
								   tt_item_family fly,
								   tt_item_sub_family sub_fly	,
								   tt_item_group ig							   

							WHERE  itm.item_family_id = fly.item_family_id
							AND		itm.item_code=ig.item_code
							AND		ig.budget_year=@BudgetYear
							AND    itm.item_sub_family_id = sub_fly.item_sub_family_id 
							AND    sub_fly.item_sub_family_status = 'ACTIVE' AND itm.item_status = 'Y' AND fly.item_family_status = 'ACTIVE'
							AND    itm.item_code NOT IN (select im.item_code from tt_salesnumbers sn, tt_item_master im
							                             where sn.item_code = im.item_code AND sn.customer_number = @p_customer_number )
				) tmp
	
			GROUP BY 
				tmp.gold_flag,
				tmp.top_flag,
				tmp.five_years_flag,
				tmp.bb_flag,
				tmp.SPC_flag,
				tmp.ten_years_flag,
				tmp.item_family_id,
				tmp.item_family_name,
				tmp.item_sub_family_id,
				item_sub_family_name,
				tmp.insert_or_tool_flag,
				tmp.item_code,
				tmp.item_description
		 ORDER BY tmp.item_code ASC

	
	ELSE
	Insert into tt_temp_summary_master
		SELECT 
			   '',gold_flag
			  ,[top_flag]
			  ,[five_years_flag]
			  ,[bb_flag]
			  ,[SPC_flag]
			  ,'' item_id
			  ,[item_family_id]
			  ,[item_family_name]
			  ,[item_sub_family_id]
			  ,[item_sub_family_name]
			  ,'' item_group_code
			  ,[insert_or_tool_flag]
			  ,[item_code]
			  ,'' item_short_name
			  ,[item_description]
			  ,'' Region,
			---4 YERS OF QUANTITIES 
				SUM(sales_qty_year_2) sales_qty_year_2,
				SUM(sales_qty_year_1) sales_qty_year_1,
				--SUM(sales_qty_year_0) sales_qty_year_0,
				CEILING(dbo.get_budget_by_summary(@BudgetYear-1, 'Q', @p_cust_dist_flag, @p_salesengineer_id, @p_branch_code, item_code, @cter)) sales_qty_year_0,

				CEILING(dbo.get_budget_by_summary(@BudgetYear, 'Q', @p_cust_dist_flag, @p_salesengineer_id, @p_branch_code, item_code, @cter)) estimate_qty_next_year,

				---4 YERS OF VALUEs 
				SUM(sales_value_year_2) sales_value_year_2,
				SUM(sales_value_year_1) sales_value_year_1,
				--SUM( sales_value_year_0) sales_value_year_0,
				CEILING(dbo.get_budget_by_summary(@BudgetYear-1, 'V', @p_cust_dist_flag, @p_salesengineer_id, @p_branch_code,item_code , @cter)) sales_value_year_0,
				CEILING(dbo.get_budget_by_summary(@BudgetYear, 'V', @p_cust_dist_flag, @p_salesengineer_id, @p_branch_code,item_code , @cter)) estimate_value_next_year
				,ten_years_flag
		FROM  tt_temp_budget_Summery 
		WHERE          (@p_branch_code = 'ALL' OR (customer_region = @p_branch_code OR territory_engineer_id = @p_branch_code ))
				AND    (@p_salesengineer_id = 'ALL' OR (assigned_salesengineer_id = @p_salesengineer_id))
				AND    (@p_cust_dist_flag = 'ALL' OR (customer_type = @p_cust_dist_flag))
				AND    (@cter IS NULL or cter = @cter)
		GROUP BY 
				gold_flag
			  ,[top_flag]
			  ,[five_years_flag]
			  ,[bb_flag]
			  ,[SPC_flag]
			  --,[item_id]
			  ,[item_family_id]
			  ,[item_family_name]
			  ,[item_sub_family_id]
			  ,[item_sub_family_name]
			  --,[item_group_code]
			  ,[insert_or_tool_flag]
			  ,[item_code]
			 -- ,[item_short_name]
			  ,[item_description]
			  ,ten_years_flag

		ORDER BY item_code
RETURN
END


