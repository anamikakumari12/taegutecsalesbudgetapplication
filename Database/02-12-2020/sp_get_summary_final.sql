USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_get_summary_final]    Script Date: 02-12-2020 15:33:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_get_summary_final 'ALL', 'ALL',  'ALL','ALL','TTA',1000
-- =============================================
-- Author:		Anamika
-- Create date: December 5, 2016
-- Description:	Creating report for Budget Summary based on the selection of filters on the screen
-- =============================================
ALTER PROCEDURE [dbo].[sp_get_summary_final]
(
	@p_branch_code		VARCHAR(20),
	@p_salesengineer_id	VARCHAR(50),
	@p_cust_dist_flag	VARCHAR(10),
	@p_customer_number	VARCHAR(10),
	@cter				VARCHAR(10) = NULL,
	@valueF				INT	
)
AS
BEGIN
	DECLARE @value INT
	DECLARE @ActualYear INT 
	DECLARE @BudgetYear INT
	DECLARE @ActualMonth INT
	DECLARE @ActualYear_1 INT
	DECLARE @year VARCHAR(255)

	IF(@valueF=NULL OR @valueF=1000)
	BEGIN
		SET @valueF=1000
		SET @value=1
	END
	ELSE
		SET @value=100000/1000
	
	SET @ActualYear_1 = [dbo].[get_profile_value]('B_ACTUAL_YEAR')
	SET @BudgetYear= [dbo].[get_profile_value]('B_BUDGET_YEAR')
	SET @ActualMonth = [dbo].[get_profile_value]('B_ACTUAL_MONTH')

	IF(@ActualYear_1=@BudgetYear)
	BEGIN
		SET @year= @ActualYear_1-1
		SET @ActualYear= @ActualYear_1-1
	END
	ELSE
	BEGIN
		SET @ActualYear= @ActualYear_1
		SELECT @year=CASE WHEN @ActualMonth=12 OR @ActualYear<Year(GETDATE()) THEN CONVERT(VARCHAR(255),@ActualYear) ELSE CONVERT(VARCHAR(255),@ActualYear) + 'P' END
	END

	TRUNCATE TABLE [tt_summary_master]
	TRUNCATE TABLE [tt_temp_summary_master]
	TRUNCATE TABLE [tt_summary_final]

	EXEC sp_get_Summery @p_branch_code, @p_salesengineer_id,  @p_cust_dist_flag,@p_customer_number,@cter

	EXEC sp_get_display_order_detail
	
	INSERT INTO [dbo].[tt_summary_master]
           (
		   LINE_DESC
		   ,[gold_flag]
           ,[top_flag]
           ,[five_years_flag]
           ,[bb_flag]
           ,[SPC_flag]
           ,[item_family_name]
           ,[item_sub_family_name]
           ,[insert_or_tool_flag]
           ,[item_code]
           ,[item_short_name]
           ,[item_description]
           ,[customer_region]
		   ,[display_value]
           ,[sales_qty_year_2]
           ,[sales_qty_year_1]
           ,[sales_qty_year_0]
           ,[estimate_qty_next_year]
           ,[sales_value_year_2]
           ,[sales_value_year_1]
           ,[sales_value_year_0]
           ,[estimate_value_next_year]
		   ,[sumFlag])
			SELECT [Line_desc]
			   ,[Gold_flag]
			   ,[Top_flag]
			   ,[Five_year_flag]
			   ,[bb_flag]
			   ,[spc_flag]
			   ,[family_name]
			   ,[sub_family_name]
				  ,[ins_tool_flag]
			   ,[item_code]
			   ,[item_short_name]
			   ,[item_description]
			   ,[Customer_region]
			   
			   ,[display_value]
			   ,[sales_qty_year_2]
			   ,[sales_qty_year_1]
			   ,[sales_qty_year_0]
			   ,[estimate_qty_next_year]
			   ,[sales_value_year_2]
			   ,[sales_value_year_1]
			   ,[sales_value_year_0]
			   ,[estimate_value_next_year]
				  ,[sumFlag]
			FROM [dbo].[tt_bud_entry_display_order_detail]
			--WHERE item_code NOT IN (SELECT item_code FROM tt_item_group WHERE SFEED_flag='Y' AND budget_year = [dbo].[get_profile_value]('B_BUDGET_YEAR'))

	UPDATE t
		SET t.item_family_id= t1.item_family_id
		FROM tt_summary_master t JOIN tt_item_family t1 ON t.item_family_name=t1.item_family_name

	UPDATE t
		SET t.item_sub_family_id= t1.item_sub_family_id
		FROM tt_summary_master t JOIN tt_item_sub_family t1 ON t.item_sub_family_name=t1.item_sub_family_name

	UPDATE T
	SET T.[item_id] = TB.[item_id]
		,T.[item_group_code]=TB.[item_group_code]
		,T.[sales_qty_year_2] = TB.[sales_qty_year_2]
		,T.[sales_qty_year_1] = TB.[sales_qty_year_1]
		,T.[sales_qty_year_0] = TB.[sales_qty_year_0]
		,T.[estimate_qty_next_year] = CASE WHEN CONVERT(VARCHAR(255),TB.[estimate_qty_next_year]) IS NULL OR CONVERT(VARCHAR(255),TB.[estimate_qty_next_year])='' THEN 0 ELSE CONVERT(DECIMAL,TB.[estimate_qty_next_year]) END
		,T.[sales_value_year_2] = TB.[sales_value_year_2]
		,T.[sales_value_year_1] = TB.[sales_value_year_1]
		,T.[sales_value_year_0] = TB.[sales_value_year_0]
		,T.[estimate_value_next_year] = CASE WHEN CONVERT(VARCHAR(255),TB.[estimate_value_next_year]) IS NULL OR CONVERT(VARCHAR(255),TB.[estimate_value_next_year])='' THEN 0 ELSE CONVERT(DECIMAL,TB.[estimate_value_next_year]) END
	FROM tt_summary_master T JOIN tt_temp_summary_master TB 
		ON T.item_family_name=TB.item_family_name
		AND T.item_sub_family_name=TB.item_sub_family_name
		AND T.insert_or_tool_flag=TB.insert_or_tool_flag
		AND T.item_code=TB.item_code

	UPDATE tt_summary_master
		SET [sales_qty_year_2]=0.0
		WHERE [sales_qty_year_2] IS NULL AND sumFlag IS NOT NULL
		
	UPDATE tt_summary_master
		SET [sales_qty_year_1] = 0.0
		WHERE [sales_qty_year_1] IS NULL AND sumFlag IS NOT NULL
		
	UPDATE tt_summary_master
		SET [sales_qty_year_0] = 0.0
		WHERE [sales_qty_year_0] IS NULL AND sumFlag IS NOT NULL
		
	UPDATE tt_summary_master
		SET [estimate_qty_next_year] =0.0
		WHERE [estimate_qty_next_year]  IS NULL AND sumFlag IS NOT NULL
		
	UPDATE tt_summary_master
		SET [sales_value_year_2] = 0.0
		WHERE [sales_value_year_2] IS NULL AND sumFlag IS NOT NULL
		
	UPDATE tt_summary_master
		SET [sales_value_year_1] = 0.0
		WHERE [sales_value_year_1] IS NULL AND sumFlag IS NOT NULL

	UPDATE tt_summary_master
		SET [sales_value_year_0] = 0.0
		WHERE [sales_value_year_0] IS NULL AND sumFlag IS NOT NULL
		
	UPDATE tt_summary_master
		SET [estimate_value_next_year] = 0.0
		WHERE [estimate_value_next_year] IS NULL AND sumFlag IS NOT NULL

	IF OBJECT_ID(N'#TEMP', N'U') IS NOT NULL
		DROP TABLE #TEMP 
	SELECT T1.item_family_name ,
			T1.item_sub_family_name,
			T1.insert_or_tool_flag,
			SUM(T1.sales_qty_year_2) sales_qty_year_2, 
			SUM(T1.sales_qty_year_1) sales_qty_year_1,
			SUM(T1.sales_qty_year_0) sales_qty_year_0,
			SUM(estimate_qty_next_year) estimate_qty_next_year,
			SUM(sales_value_year_2) sales_value_year_2,
			SUM(sales_value_year_1) sales_value_year_1,
			SUM(sales_value_year_0) sales_value_year_0,
			SUM(estimate_value_next_year) estimate_value_next_year
		INTO #TEMP
		FROM 
			[tt_summary_master] T1 
		GROUP BY
				T1.item_family_name ,
				T1.item_sub_family_name,
				T1.insert_or_tool_flag
		
	UPDATE T
		SET T.[sales_qty_year_2] = T1.[sales_qty_year_2]
			,T.[sales_qty_year_1] = T1.[sales_qty_year_1]
			,T.[sales_qty_year_0] = T1.[sales_qty_year_0]
			,T.[estimate_qty_next_year] =T1.[estimate_qty_next_year]
			,T.[sales_value_year_2] = T1.[sales_value_year_2]
			,T.[sales_value_year_1] = T1.[sales_value_year_1]
			,T.[sales_value_year_0] = T1.[sales_value_year_0]
			,T.[estimate_value_next_year] = T1.[estimate_value_next_year]
		FROM tt_summary_master T JOIN #TEMP T1
			ON T.item_family_name=T1.item_family_name
			AND T.item_sub_family_name=T1.item_sub_family_name
			AND T1.insert_or_tool_flag='I'
			AND T.display_value LIKE 'Totals%Inserts'
		
	UPDATE T 
		SET T.[sales_qty_year_2] = T1.[sales_qty_year_2]
			,T.[sales_qty_year_1] = T1.[sales_qty_year_1]
			,T.[sales_qty_year_0] = T1.[sales_qty_year_0]
			,T.[estimate_qty_next_year] =T1.[estimate_qty_next_year]
			,T.[sales_value_year_2] = T1.[sales_value_year_2]
			,T.[sales_value_year_1] = T1.[sales_value_year_1]
			,T.[sales_value_year_0] = T1.[sales_value_year_0]
			,T.[estimate_value_next_year] = T1.[estimate_value_next_year]
		FROM tt_summary_master T JOIN #TEMP T1
			ON T.item_family_name=T1.item_family_name
			AND T.item_sub_family_name=T1.item_sub_family_name
			AND T1.insert_or_tool_flag='T'
			AND T.display_value LIKE 'Totals%Tools'

	UPDATE T
		SET T.[sales_qty_year_2] = T1.[sales_qty_year_2]
			,T.[sales_qty_year_1] = T1.[sales_qty_year_1]
			,T.[sales_qty_year_0] = T1.[sales_qty_year_0]
			,T.[estimate_qty_next_year] =T1.[estimate_qty_next_year]
			,T.[sales_value_year_2] = T1.[sales_value_year_2]
			,T.[sales_value_year_1] = T1.[sales_value_year_1]
			,T.[sales_value_year_0] = T1.[sales_value_year_0]
			,T.[estimate_value_next_year] = T1.[estimate_value_next_year]
		FROM tt_summary_master T JOIN #TEMP T1
			ON T.item_family_name=T1.item_family_name
			AND T.item_sub_family_name=T1.item_sub_family_name
			AND (T1.insert_or_tool_flag IS NULL OR T1.insert_or_tool_flag='' OR T1.insert_or_tool_flag='O')
			AND T.display_value LIKE 'Totals%Others'
		
	IF OBJECT_ID(N'#TEMP1', N'U') IS NOT NULL
		DROP TABLE #TEMP1
	SELECT T1.item_family_name ,
		T1.item_sub_family_name,
		T1.insert_or_tool_flag,
		SUM(T1.sales_qty_year_2) sales_qty_year_2, 
		SUM(T1.sales_qty_year_1) sales_qty_year_1,
		SUM(T1.sales_qty_year_0) sales_qty_year_0,
		SUM(estimate_qty_next_year) estimate_qty_next_year,
		SUM(sales_value_year_2) sales_value_year_2,
		SUM(sales_value_year_1) sales_value_year_1,
		SUM(sales_value_year_0) sales_value_year_0,
		SUM(estimate_value_next_year) estimate_value_next_year
	INTO #TEMP1
		FROM 
			[tt_summary_master] T1 
		WHERE
			T1.insert_or_tool_flag='Total'
			AND T1.sumFlag='typeSum'
		GROUP BY
			T1.item_family_name ,
			T1.item_sub_family_name,
			T1.insert_or_tool_flag
	
	UPDATE T
		SET T.[sales_qty_year_2]=T1.[sales_qty_year_2]
			,T.[sales_qty_year_1] = T1.[sales_qty_year_1]
			,T.[sales_qty_year_0] = T1.[sales_qty_year_0]
			,T.[estimate_qty_next_year] =T1.[estimate_qty_next_year]
			,T.[sales_value_year_2] = T1.[sales_value_year_2]
			,T.[sales_value_year_1] = T1.[sales_value_year_1]
			,T.[sales_value_year_0] = T1.[sales_value_year_0]
			,T.[estimate_value_next_year] = T1.[estimate_value_next_year]
		FROM tt_summary_master T JOIN #TEMP1 T1
			ON T.item_family_name=T1.item_family_name
			AND T.item_sub_family_name=T1.item_sub_family_name
			AND T1.insert_or_tool_flag='Total'
			AND T.sumFlag='SubFamilySum'

	IF OBJECT_ID(N'#TEMP2', N'U') IS NOT NULL
		DROP TABLE #TEMP2
	SELECT T1.item_family_name ,
		T1.item_sub_family_name,
		T1.insert_or_tool_flag,
		SUM(T1.sales_qty_year_2) sales_qty_year_2, 
		SUM(T1.sales_qty_year_1) sales_qty_year_1,
		SUM(T1.sales_qty_year_0) sales_qty_year_0,
		SUM(estimate_qty_next_year) estimate_qty_next_year,
		SUM(sales_value_year_2) sales_value_year_2,
		SUM(sales_value_year_1) sales_value_year_1,
		SUM(sales_value_year_0) sales_value_year_0,
		SUM(estimate_value_next_year) estimate_value_next_year
	INTO #TEMP2
		FROM 
			[tt_summary_master] T1 
		WHERE
			T1.insert_or_tool_flag='Total'
			AND T1.sumFlag='SubFamilySum'
		GROUP BY
			T1.item_family_name ,
			T1.item_sub_family_name,
			T1.insert_or_tool_flag
	
	UPDATE T
		SET T.[sales_qty_year_2]=T1.[sales_qty_year_2]
			,T.[sales_qty_year_1] = T1.[sales_qty_year_1]
			,T.[sales_qty_year_0] = T1.[sales_qty_year_0]
			,T.[estimate_qty_next_year] =T1.[estimate_qty_next_year]
			,T.[sales_value_year_2] = T1.[sales_value_year_2]
			,T.[sales_value_year_1] = T1.[sales_value_year_1]
			,T.[sales_value_year_0] = T1.[sales_value_year_0]
			,T.[estimate_value_next_year] = T1.[estimate_value_next_year]
		FROM tt_summary_master T 
			JOIN (SELECT T1.item_family_name,
					SUM(T1.sales_qty_year_2) sales_qty_year_2, 
					SUM(T1.sales_qty_year_1) sales_qty_year_1,
					SUM(T1.sales_qty_year_0) sales_qty_year_0,
					SUM(estimate_qty_next_year) estimate_qty_next_year,
					SUM(sales_value_year_2) sales_value_year_2,
					SUM(sales_value_year_1) sales_value_year_1,
					SUM(sales_value_year_0) sales_value_year_0,
					SUM(estimate_value_next_year) estimate_value_next_year
					FROM #TEMP1 T1
					GROUP BY
						T1.item_family_name ) T1
			ON t.item_family_name=t1.item_family_name
			AND T.sumFlag='FamilySum'

	UPDATE T
		SET 
			 T.[sales_qty_year_2]=T2.[sales_qty_year_2]
			,T.[sales_qty_year_1] = T2.[sales_qty_year_1]
			,T.[sales_qty_year_0] = T2.[sales_qty_year_0]
			,T.[estimate_qty_next_year] =T2.[estimate_qty_next_year]
			,T.[sales_value_year_2] = T2.[sales_value_year_2]
			,T.[sales_value_year_1] = T2.[sales_value_year_1]
			,T.[sales_value_year_0] = T2.[sales_value_year_0]
			,T.[estimate_value_next_year] = T2.[estimate_value_next_year]
			,T.item_family_id=''
			,T.item_family_name=''
		FROM tt_summary_master T ,
			(SELECT
				SUM(T1.sales_qty_year_2) sales_qty_year_2, 
				SUM(T1.sales_qty_year_1) sales_qty_year_1,
				SUM(T1.sales_qty_year_0) sales_qty_year_0,
				SUM(estimate_qty_next_year) estimate_qty_next_year,
				SUM(sales_value_year_2) sales_value_year_2,
				SUM(sales_value_year_1) sales_value_year_1,
				SUM(sales_value_year_0) sales_value_year_0,
				SUM(estimate_value_next_year) estimate_value_next_year
			FROM 
				[tt_summary_master] T1 
			WHERE
				T1.insert_or_tool_flag='Total'
				AND T1.sumFlag='FamilySum') T2
		WHERE
		 T.sumFlag='MainSum'

	UPDATE tt_summary_master
		SET	 sales_value_year_2=CONVERT(DECIMAL, sales_value_year_2)/@valueF
			,sales_value_year_1=CONVERT(DECIMAL, sales_value_year_1)/@valueF
			,sales_value_year_0=CONVERT(DECIMAL, sales_value_year_0)/@valueF
			, estimate_value_next_year= CONVERT(DECIMAL, estimate_value_next_year)/@valueF
			, QuantityVariance_sales_qty_year_2=(sales_qty_year_1-sales_qty_year_2)
			, QuantityVariance_sales_qty_year_1=(sales_qty_year_1-sales_qty_year_2)
			, QuantityVariance_sales_qty_year_0=(estimate_qty_next_year-sales_qty_year_1)
			, QuantityPercentage_sales_qty_year_2= CASE WHEN CONVERT(INT,sales_qty_year_2)<=0 THEN 0 ELSE (ROUND(CAST(sales_qty_year_1/sales_qty_year_2 AS DECIMAL(16,2)), 2) -1)*100 END
			, QuantityPercentage_sales_qty_year_1= CASE WHEN CONVERT(INT,sales_qty_year_2)<=0 THEN 0 ELSE (ROUND(CAST(sales_qty_year_1/sales_qty_year_2 AS DECIMAL(16,2)), 2) -1)*100 END
			, QuantityPercentage_sales_qty_year_0= CASE WHEN CONVERT(INT,sales_qty_year_1)<=0 THEN 0 ELSE (ROUND(CAST(estimate_qty_next_year/sales_qty_year_1 AS DECIMAL(16,2)), 2) -1)*100 END
			, ValuePercentage_sales_value_year_2 = CASE WHEN sales_value_year_2<=0 THEN 0 ELSE (ROUND(CAST(sales_value_year_1/sales_value_year_2 AS DECIMAL(16,2)), 2) -1)*100 END
			, ValuePercentage_sales_value_year_1 = CASE WHEN sales_value_year_2<=0 THEN 0 ELSE (ROUND(CAST(sales_value_year_1/sales_value_year_2 AS DECIMAL(16,2)), 2)-1)*100 END
			, ValuePercentage_sales_value_year_0 = CASE WHEN sales_value_year_1<=0 THEN 0 ELSE (ROUND(CAST(estimate_value_next_year/sales_value_year_1 AS DECIMAL(16,2)), 2) -1)*100  END
			, PricePerUnit_sales_value_year_2 = CASE WHEN sales_qty_year_2<=0 THEN 0 ELSE (ROUND(CAST(sales_value_year_2/sales_qty_year_2 AS DECIMAL(16,2)), 2)) END
			, PricePerUnit_sales_value_year_1 = CASE WHEN sales_qty_year_1<=0 THEN 0 ELSE (ROUND(CAST(sales_value_year_1/sales_qty_year_1 AS DECIMAL(16,2)), 2)) END
			, PricePerUnit_sales_value_year_0 = CASE WHEN sales_qty_year_0<=0 THEN 0 ELSE (ROUND(CAST(sales_value_year_0/sales_qty_year_0 AS DECIMAL(16,2)), 2)) END
			, PricePerUnit_sales_value_year_P = CASE WHEN estimate_qty_next_year<=0 THEN 0 ELSE estimate_value_next_year/estimate_qty_next_year END
		WHERE sumFlag IN ('products','SubFamilySum','MainSum','typeSum','FamilySum')

	UPDATE tt_summary_master
		SET
			priceschange_sales_value_year_2= CASE WHEN  PricePerUnit_sales_value_year_2<=0 THEN 0 ELSE (PricePerUnit_sales_value_year_1/PricePerUnit_sales_value_year_2 -1)*100 END,
			priceschange_sales_value_year_1= CASE WHEN  PricePerUnit_sales_value_year_1<=0 THEN 0 ELSE (PricePerUnit_sales_value_year_0/PricePerUnit_sales_value_year_1 -1)*100 END,
			priceschange_sales_value_year_0= CASE WHEN  PricePerUnit_sales_value_year_0<=0 THEN 0 ELSE (PricePerUnit_sales_value_year_P/PricePerUnit_sales_value_year_0 -1)*100 END
		WHERE sumFlag IN ('products','SubFamilySum','MainSum','typeSum','FamilySum')

	IF NOT EXISTS(SELECT * FROM tt_summary_master WHERE item_sub_family_name='TOOL REPAIRS' AND sumFlag='products' AND (insert_or_tool_flag=NULL OR insert_or_tool_flag=''))
	BEGIN
		DELETE FROM tt_summary_master WHERE item_description='TOTAL TOOL REPAIRS OTHERS'
	END
		
	INSERT INTO [dbo].[tt_summary_final]
           ([gold_flag]
           ,[top_flag]
           ,[five_years_flag]
           ,[bb_flag]
           ,[SPC_flag]
           ,[item_id]
           ,[item_family_id]
           ,[item_family_name]
           ,[item_sub_family_id]
           ,[item_sub_family_name]
           ,[item_group_code]
           ,[insert_or_tool_flag]
           ,[item_code]
           ,[item_short_name]
           ,[item_description]
           ,[customer_region]
           ,[sales_qty_year_2]
           ,[sales_qty_year_1]
           ,[sales_qty_year_0]
           ,[estimate_qty_next_year]
           ,[sales_value_year_2]
           ,[sales_value_year_1]
           ,[sales_value_year_0]
           ,[estimate_value_next_year]
           ,[sumFlag]
           ,[QuantityVariance_sales_qty_year_2]
           ,[QuantityVariance_sales_qty_year_1]
           ,[QuantityVariance_sales_qty_year_0]
           ,[QuantityPercentage_sales_qty_year_2]
           ,[QuantityPercentage_sales_qty_year_1]
           ,[QuantityPercentage_sales_qty_year_0]
           ,[ValuePercentage_sales_value_year_2]
           ,[ValuePercentage_sales_value_year_1]
           ,[ValuePercentage_sales_value_year_0]
           ,[PricePerUnit_sales_value_year_2]
           ,[PricePerUnit_sales_value_year_1]
           ,[PricePerUnit_sales_value_year_0]
           ,[PricePerUnit_sales_value_year_P]
           ,[priceschange_sales_value_year_2]
           ,[priceschange_sales_value_year_1]
           ,[priceschange_sales_value_year_0]
		   ,ten_years_flag
		   ,SFEED_flag)
		VALUES
           ('GOLD'
           ,'TOP'
           ,'5yrs'
           ,'BB'
           ,'SPC'
           ,'00'
           ,'01'
           ,'Family/ SubGroup/ Application'
           ,'45'
           ,'XXXXXXXXXXXXXXXXX'
           ,'FF'
           ,'Tools'
           ,'  '
           ,'XXxXXXXX XXX'
           ,''
           ,''
           ,CONVERT(VARCHAR(255),(@ActualYear-2))+' Pieces'
           ,CONVERT(VARCHAR(255),(@ActualYear-1))+' Pieces'
           ,CONVERT(VARCHAR(255),(@year))+' Pieces'
           ,CONVERT(VARCHAR(255),(@BudgetYear))+'B Pieces'
           ,CONVERT(VARCHAR(255),(@ActualYear-2))+' Rupee('+CONVERT(VARCHAR(255),@valueF)+''')'
           ,CONVERT(VARCHAR(255),(@ActualYear-1))+' Rupee('+CONVERT(VARCHAR(255),@valueF)+''')'
           ,CONVERT(VARCHAR(255),(@year))+' Rupee('+CONVERT(VARCHAR(255),@valueF)+''')'
           ,CONVERT(VARCHAR(255),(@BudgetYear))+'B Rupee('+CONVERT(VARCHAR(255),@valueF)+''')'
           ,'HidingHeading'
           ,CONVERT(VARCHAR(255),(@ActualYear-1))+'-'+CONVERT(VARCHAR(255),(@ActualYear-2))+' Incr.PCS'
           ,CONVERT(VARCHAR(255),(@ActualYear-1))+'-'+CONVERT(VARCHAR(255),(@ActualYear-2))+' Incr.PCS'
           ,CONVERT(VARCHAR(255),(@BudgetYear))+'B-'+CONVERT(VARCHAR(255),(@ActualYear-1))+' Incr.PCS'
           ,CONVERT(VARCHAR(255),(@ActualYear-1))+'/'+CONVERT(VARCHAR(255),(@ActualYear-2))+' Incr.Pcs(%)'
           ,CONVERT(VARCHAR(255),(@ActualYear-1))+'/'+CONVERT(VARCHAR(255),(@ActualYear-2))+' Incr.Pcs(%)'
           ,CONVERT(VARCHAR(255),(@BudgetYear))+'B/'+CONVERT(VARCHAR(255),(@ActualYear-1))+' Incr.Pcs(%)'
           ,CONVERT(VARCHAR(255),(@ActualYear-1))+'/'+CONVERT(VARCHAR(255),(@ActualYear-2))+' Incr.Rupee(%)'
           ,CONVERT(VARCHAR(255),(@ActualYear-1))+'/'+CONVERT(VARCHAR(255),(@ActualYear-2))+' Incr.Rupee(%)'
           ,CONVERT(VARCHAR(255),(@BudgetYear))+'B/'+CONVERT(VARCHAR(255),(@ActualYear-1))+' Incr.Rupee(%)'
           ,CONVERT(VARCHAR(255),(@ActualYear-2))+' RupeePerPcs'
           ,CONVERT(VARCHAR(255),(@ActualYear-1))+' RupeePerPcs'
           ,CONVERT(VARCHAR(255),(@year))+' RupeePerPcs'
           ,CONVERT(VARCHAR(255),(@BudgetYear))+'B RupeePerPcs'
           ,CONVERT(VARCHAR(255),(@ActualYear-1))+'/'+CONVERT(VARCHAR(255),(@ActualYear-2))+' PriceChange(%)'
           ,CONVERT(VARCHAR(255),(@ActualYear-1))+'/'+CONVERT(VARCHAR(255),(@ActualYear-2))+' PriceChange(%)'
           ,CONVERT(VARCHAR(255),(@BudgetYear))+'B/'+CONVERT(VARCHAR(255),(@ActualYear-1))+' PriceChange(%)'
		   ,'10yrs'
		   ,'SFEED')

	IF(@ActualYear_1<>@BudgetYear)
	BEGIN
		INSERT INTO tt_summary_final
			SELECT [LINE_DESC]
				,[gold_flag]
				,[top_flag]
				,[five_years_flag]
				,[bb_flag]
				,[SPC_flag]
				,[item_id]
				,[item_family_id]
				,[item_family_name]
				,[item_sub_family_id]
				,[item_sub_family_name]
				,[item_group_code]
				,[insert_or_tool_flag]
				,[item_code]
				,[item_short_name]
				,[item_description]
				,[customer_region]
				,CONVERT(INT,ROUND([sales_qty_year_2]/@value,0))
				,CONVERT(INT,ROUND([sales_qty_year_1]/@value,0))
				,CONVERT(INT,ROUND([sales_qty_year_0]/@value,0))
				,CONVERT(INT,ROUND([estimate_qty_next_year]/@value,0))
				,CONVERT(INT,ROUND([sales_value_year_2]/@value,0))
				,CONVERT(INT,ROUND([sales_value_year_1]/@value,0))
				,CONVERT(INT,ROUND([sales_value_year_0]/@value,0))
				,CONVERT(INT,ROUND([estimate_value_next_year]/@value,0))
				,[display_value]
				,[sumFlag]
				,CONVERT(INT,ROUND([QuantityVariance_sales_qty_year_2]/@value,0))
				,CONVERT(INT,ROUND([QuantityVariance_sales_qty_year_1]/@value,0))
				,CONVERT(INT,ROUND([QuantityVariance_sales_qty_year_0]/@value,0))
				,CONVERT(INT,ROUND([QuantityPercentage_sales_qty_year_2],0))
				,CONVERT(INT,ROUND([QuantityPercentage_sales_qty_year_1],0))
				,CONVERT(INT,ROUND([QuantityPercentage_sales_qty_year_0],0))
				,CONVERT(INT,ROUND([ValuePercentage_sales_value_year_2],0))
				,CONVERT(INT,ROUND([ValuePercentage_sales_value_year_1],0))
				,CONVERT(INT,ROUND([ValuePercentage_sales_value_year_0],0))
				,CONVERT(INT,ROUND([PricePerUnit_sales_value_year_2]/@value,0))
				,CONVERT(INT,ROUND([PricePerUnit_sales_value_year_1]/@value,0))
				,CONVERT(INT,ROUND([PricePerUnit_sales_value_year_0]/@value,0))
				,CONVERT(INT,ROUND([PricePerUnit_sales_value_year_P]/@value,0))
				,CONVERT(INT,ROUND([priceschange_sales_value_year_2],0))
				,CONVERT(INT,ROUND([priceschange_sales_value_year_1],0))
				,CONVERT(INT,ROUND([priceschange_sales_value_year_0],0))
				,''
				,''
			FROM [dbo].[tt_summary_master]
	END 
	ELSE
	BEGIN
		INSERT INTO tt_summary_final
			SELECT [LINE_DESC]
				,[gold_flag]
				,[top_flag]
				,[five_years_flag]
				,[bb_flag]
				,[SPC_flag]
				,[item_id]
				,[item_family_id]
				,[item_family_name]
				,[item_sub_family_id]
				,[item_sub_family_name]
				,[item_group_code]
				,[insert_or_tool_flag]
				,[item_code]
				,[item_short_name]
				,[item_description]
				,[customer_region]
				,CONVERT(INT,ROUND([sales_qty_year_2]/@value,0))
				,CONVERT(INT,ROUND([sales_qty_year_2]/@value,0))
				,CONVERT(INT,ROUND([sales_qty_year_1]/@value,0))
				,CONVERT(INT,ROUND([estimate_qty_next_year]/@value,0))
				,CONVERT(INT,ROUND([sales_value_year_2]/@value,0))
				,CONVERT(INT,ROUND([sales_value_year_2]/@value,0))
				,CONVERT(INT,ROUND([sales_value_year_1]/@value,0))
				,CONVERT(INT,ROUND([estimate_value_next_year]/@value,0))
				,[display_value]
				,[sumFlag]
				,CONVERT(INT,ROUND([QuantityVariance_sales_qty_year_2]/@value,0))
				,CONVERT(INT,ROUND([QuantityVariance_sales_qty_year_2]/@value,0))
				,CONVERT(INT,ROUND([QuantityVariance_sales_qty_year_1]/@value,0))
				,CONVERT(INT,ROUND([QuantityPercentage_sales_qty_year_2],0))
				,CONVERT(INT,ROUND([QuantityPercentage_sales_qty_year_2],0))
				,CONVERT(INT,ROUND([QuantityPercentage_sales_qty_year_1],0))
				,CONVERT(INT,ROUND([ValuePercentage_sales_value_year_2],0))
				,CONVERT(INT,ROUND([ValuePercentage_sales_value_year_2],0))
				,CONVERT(INT,ROUND([ValuePercentage_sales_value_year_1],0))
				,CONVERT(INT,ROUND([PricePerUnit_sales_value_year_2]/@value,0))
				,CONVERT(INT,ROUND([PricePerUnit_sales_value_year_2]/@value,0))
				,CONVERT(INT,ROUND([PricePerUnit_sales_value_year_1]/@value,0))
				,CONVERT(INT,ROUND([PricePerUnit_sales_value_year_0]/@value,0))
				,CONVERT(INT,ROUND([priceschange_sales_value_year_2],0))
				,CONVERT(INT,ROUND([priceschange_sales_value_year_2],0))
				,CONVERT(INT,ROUND([priceschange_sales_value_year_1],0))
				,''
				,''
			FROM [dbo].[tt_summary_master]
	END

	UPDATE tt_summary_final
		SET [gold_flag]=''
			,[top_flag]=''
			,[five_years_flag]=''
			,[bb_flag]=''
			,[SPC_flag]=''
			,[item_id]=''
			,[item_family_id]=''
			,[item_sub_family_id]=''
			,[item_group_code]=''
			,[item_code]=''
			,[item_short_name]=''
			,[item_description]=''
			,[customer_region]=''
			,[sales_qty_year_2]=''
			,[sales_qty_year_1]=''
			,[sales_qty_year_0]=''
			,[estimate_qty_next_year]=''
			,[sales_value_year_2]=''
			,[sales_value_year_1]=''
			,[sales_value_year_0]=''
			,[estimate_value_next_year]=''
			,[QuantityVariance_sales_qty_year_2]=''
			,[QuantityVariance_sales_qty_year_1]=''
			,[QuantityVariance_sales_qty_year_0]=''
			,[QuantityPercentage_sales_qty_year_2]=''
			,[QuantityPercentage_sales_qty_year_1]=''
			,[QuantityPercentage_sales_qty_year_0]=''
			,[ValuePercentage_sales_value_year_2]	=''
			,[ValuePercentage_sales_value_year_1]	=''
			,[ValuePercentage_sales_value_year_0]	=''
			,[PricePerUnit_sales_value_year_2]	=''
			,[PricePerUnit_sales_value_year_1]	=''
			,[PricePerUnit_sales_value_year_0]	=''
			,[PricePerUnit_sales_value_year_P]	=''
			,[priceschange_sales_value_year_2]	=''
			,[priceschange_sales_value_year_1]	=''
			,[priceschange_sales_value_year_0]	=''
		WHERE insert_or_tool_flag IN ('SubFamHeading','FamilyHeading')

	UPDATE tt_summary_final
		SET [item_description]=item_sub_family_name
		WHERE insert_or_tool_flag ='SubFamHeading'

	UPDATE tt_summary_final
		SET	  QuantityPercentage_sales_qty_year_2= QuantityPercentage_sales_qty_year_2+'%'
			, QuantityPercentage_sales_qty_year_1= QuantityPercentage_sales_qty_year_1 +'%'
			, QuantityPercentage_sales_qty_year_0= QuantityPercentage_sales_qty_year_0 +'%'
			, ValuePercentage_sales_value_year_2 = ValuePercentage_sales_value_year_2 +'%'
			, ValuePercentage_sales_value_year_1 = ValuePercentage_sales_value_year_1 +'%'
			, ValuePercentage_sales_value_year_0 = ValuePercentage_sales_value_year_0 +'%'
			,priceschange_sales_value_year_2 = priceschange_sales_value_year_2 +'%'
			,priceschange_sales_value_year_1 = priceschange_sales_value_year_1 +'%'
			,priceschange_sales_value_year_0 = priceschange_sales_value_year_0 +'%'
		WHERE sumFlag IN ('products','SubFamilySum','MainSum','typeSum','FamilySum')

UPDATE t SET t.ten_years_flag=ti.ten_years_flag
,t.gold_flag=ti.gold_flag
,t.bb_flag=ti.bb_flag
,t.SFEED_flag=ti.SFEED_flag
FROM tt_summary_final t JOIN tt_item_group ti ON t.item_code = ti.item_code

	SELECT [gold_flag]
		,[top_flag]
		,[five_years_flag]
		,[bb_flag]
		,[SPC_flag]
		,ten_years_flag
		,[SFEED_flag]
		,[item_id]
		,[item_family_id]
		,[item_family_name]
		,[item_sub_family_id]
		,[item_sub_family_name]
		,[item_group_code]
		,[insert_or_tool_flag]
		,[item_code]
		,[item_short_name]
		,[item_description]
		,[customer_region]
		,[sales_qty_year_2]
		,[sales_qty_year_1]
		,[sales_qty_year_0]
		,[estimate_qty_next_year]
		,[sales_value_year_2]
		,[sales_value_year_1]
		,[sales_value_year_0]
		,[estimate_value_next_year]
		,[sumFlag]
		,[QuantityVariance_sales_qty_year_2]
		,[QuantityVariance_sales_qty_year_1]
		,[QuantityVariance_sales_qty_year_0]
		,[QuantityPercentage_sales_qty_year_2]
		,[QuantityPercentage_sales_qty_year_1]
		,[QuantityPercentage_sales_qty_year_0]
		,[ValuePercentage_sales_value_year_2]
		,[ValuePercentage_sales_value_year_1]
		,[ValuePercentage_sales_value_year_0]
		,[PricePerUnit_sales_value_year_2]
		,[PricePerUnit_sales_value_year_1]
		,[PricePerUnit_sales_value_year_0]
		,[PricePerUnit_sales_value_year_P]
		,[priceschange_sales_value_year_2]
		,[priceschange_sales_value_year_1]
		,[priceschange_sales_value_year_0]
	FROM [dbo].[tt_summary_final]
END 







