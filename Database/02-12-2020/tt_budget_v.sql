USE [Taegutec_Sales_Budget]
GO

/****** Object:  View [dbo].[tt_budget_v]    Script Date: 02-12-2020 15:32:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER  View [dbo].[tt_budget_v] AS
SELECT tmp.gold_flag ,
	tmp.top_flag,
	tmp.five_years_flag,
	tmp.bb_flag,
	tmp.SPC_flag,
	tmp.item_id,
	tmp.item_family_id,
	item_family_name,
	tmp.item_sub_family_id,
	item_sub_family_name,
	tmp.item_group_code,
	tmp.insert_or_tool_flag,
	tmp.item_code,
	tmp.item_short_name,
	tmp.item_description,
	tmp.customer_number,

	CEILING( SUM(sales_qty_year_2)) sales_qty_year_2,
	CEILING( SUM(sales_qty_year_1)) sales_qty_year_1,
	--CEILING(SUM(sales_qty_year_0)* (CONVERT(DECIMAL(22,5), 12)/ CONVERT(DECIMAL(22,5), [dbo].[get_profile_value]('B_ACTUAL_MONTH')))) sales_qty_year_0,	
	--CEILING(SUM(sales_qty_year_0_Nov)* (CONVERT(DECIMAL(22,5), 12)/ CONVERT(DECIMAL(22,5), 11))) sales_qty_year_0_Nov,	
	--only for 2020 due to pandemic, 2020 budget qty is treated as sales qty, this needs to be reverted next year
	CEILING([dbo].[get_budget]( 2020, tmp.item_code, tmp.customer_number, 'Q')) sales_qty_year_0,
	CEILING([dbo].[get_budget]( 2020, tmp.item_code, tmp.customer_number, 'Q')) sales_qty_year_0_Nov,
	CEILING([dbo].[get_budget]( [dbo].[get_profile_value]('B_BUDGET_YEAR'), tmp.item_code, tmp.customer_number, 'Q')) estimate_qty_next_year,
	
	
	CEILING( SUM(sales_value_year_2)) sales_value_year_2 ,
	CEILING(SUM(sales_value_year_1)) sales_value_year_1,
	--CEILING(SUM(sales_value_year_0) * (CONVERT(DECIMAL(22,5), 12)/ CONVERT(DECIMAL(22,5), [dbo].[get_profile_value]('B_ACTUAL_MONTH')))) sales_value_year_0,
	--CEILING(SUM(sales_value_year_0_Nov) * (CONVERT(DECIMAL(22,5), 12)/ CONVERT(DECIMAL(22,5), 11))) sales_value_year_0_Nov,
	--only for 2020 due to pandemic, 2020 budget value is treated as sales value, this needs to be reverted next year
	CEILING([dbo].[get_budget]( 2020, tmp.item_code, tmp.customer_number, 'V')) sales_value_year_0,
	CEILING([dbo].[get_budget]( 2020, tmp.item_code, tmp.customer_number, 'V')) sales_value_year_0_Nov,
	CEILING([dbo].[get_budget]( [dbo].[get_profile_value]('B_BUDGET_YEAR'), tmp.item_code, tmp.customer_number, 'V')) estimate_value_next_year,
	[dbo].[get_review_flag]( [dbo].[get_profile_value]('B_BUDGET_YEAR'), tmp.item_code, tmp.customer_number) review_flag,
	
	tmp.salesengineer_id,
	tmp.status_sales_engineer,
	tmp.status_branch_manager,
	tmp.status_ho,
	tmp.budget_id,
	[dbo].[get_sales]( [dbo].[get_profile_value]('B_BUDGET_YEAR'), tmp.item_code, tmp.customer_number, 'R') estimate_rate_next_year

FROM (SELECT 
	itm.gold_flag,
	itm.top_flag,
	itm.five_years_flag,
	itm.bb_flag,
	itm.SPC_flag,
	itm.item_id,
	itm.item_family_id,
	item_family_name,
	itm.item_sub_family_id,
	item_sub_family_name,
	itm.item_group_code,
	itm.insert_or_tool_flag,
	itm.item_code,
	itm.item_short_name,
	itm.item_description,
	cust.customer_number,
	sales_qty_year_2 = case when s.inv_year = [dbo].[get_profile_value]('B_ACTUAL_YEAR')-2 then s.quantity end,	
	sales_qty_year_1 = case when s.inv_year = [dbo].[get_profile_value]('B_ACTUAL_YEAR')-1 then s.quantity end,	
	sales_qty_year_0 = case when s.inv_year = [dbo].[get_profile_value]('B_ACTUAL_YEAR') AND s.inv_month <= [dbo].[get_profile_value]('B_ACTUAL_MONTH') then s.quantity end,
	sales_qty_year_0_Nov = case when s.inv_year = [dbo].[get_profile_value]('B_ACTUAL_YEAR') AND s.inv_month <= 11 then s.quantity end,
	sales_value_year_2 = case when s.inv_year = [dbo].[get_profile_value]('B_ACTUAL_YEAR')-2 then s.value end,	
	sales_value_year_1 = case when s.inv_year = [dbo].[get_profile_value]('B_ACTUAL_YEAR')-1 then s.value end,	
	sales_value_year_0 = case when s.inv_year = [dbo].[get_profile_value]('B_ACTUAL_YEAR') AND s.inv_month <= [dbo].[get_profile_value]('B_ACTUAL_MONTH') then s.value end,
	sales_value_year_0_Nov = case when s.inv_year = [dbo].[get_profile_value]('B_ACTUAL_YEAR') AND s.inv_month <= 11 then s.value end,
	tt_budget_master_SFEED.salesengineer_id,
	tt_budget_master_SFEED.status_sales_engineer,
	tt_budget_master_SFEED.status_branch_manager,
	tt_budget_master_SFEED.status_ho,
	tt_budget_master_SFEED.budget_id

FROM   tt_item_master itm,
	tt_item_family fly,
	tt_item_sub_family sub_fly,
	tt_salesnumbers s,
	tt_customer_master cust
	LEFT OUTER JOIN tt_budget_master_SFEED
		ON cust.customer_number = tt_budget_master_SFEED.customer_number
		AND tt_budget_master_SFEED.budget_year = [dbo].[get_profile_value]('B_BUDGET_YEAR')
--	LEFT JOIN (
--	Select bn.*, bm.customer_number from tt_budget_master bm JOIn tt_budget_numbers bn ON bm.budget_id=bn.budget_id AND budget_year=2020
--			UNION ALL
--			Select bn.*, bm.customer_number from tt_budget_master_SFEED bm JOIn tt_budget_numbers_SFEED bn ON bm.budget_id=bn.budget_id AND budget_year=2020
--) bud ON bud.customer_number=cust.customer_number
WHERE  itm.item_family_id = fly.item_family_id
AND    itm.item_sub_family_id = sub_fly.item_sub_family_id 
AND    cust.customer_number = s.customer_number
AND    itm.item_code = s.item_code
AND sub_fly.item_sub_family_status = 'ACTIVE' AND itm.item_status = 'Y' AND fly.item_family_status = 'ACTIVE'
--AND itm.item_code=bud.item_code
UNION ALL
SELECT 
	itm.gold_flag,
	itm.top_flag,
	itm.five_years_flag,
	itm.bb_flag,
	itm.SPC_flag,
	itm.item_id,
	itm.item_family_id,
	item_family_name,
	itm.item_sub_family_id,
	item_sub_family_name,
	itm.item_group_code,
	itm.insert_or_tool_flag,
	itm.item_code,
	itm.item_short_name,
	itm.item_description,
	cust.customer_number,
	0 sales_qty_year_2,
	0 sales_qty_year_1,
	0 sales_qty_year_0,
	0 sales_qty_year_0_Nov,
	0 sales_value_year_2,
	0 sales_value_year_1,
	0 sales_value_year_0,
	0 sales_value_year_0_Nov,
	tt_budget_master_SFEED.salesengineer_id,
	tt_budget_master_SFEED.status_sales_engineer,
	tt_budget_master_SFEED.status_branch_manager,
	tt_budget_master_SFEED.status_ho,
	tt_budget_master_SFEED.budget_id

FROM   tt_item_master itm,
	tt_item_family fly,
	tt_item_sub_family sub_fly,
	tt_customer_master cust
	LEFT OUTER JOIN tt_budget_master_SFEED
		ON cust.customer_number = tt_budget_master_SFEED.customer_number
		AND tt_budget_master_SFEED.budget_year = [dbo].[get_profile_value]('B_BUDGET_YEAR')

WHERE  itm.item_family_id = fly.item_family_id
AND    itm.item_sub_family_id = sub_fly.item_sub_family_id 
AND    itm.item_code NOT IN (select s.item_code from tt_salesnumbers s where s.customer_number = cust.customer_number)
AND sub_fly.item_sub_family_status = 'ACTIVE' AND itm.item_status = 'Y' AND fly.item_family_status = 'ACTIVE'
) tmp

 
GROUP BY 
    tmp.gold_flag,
	tmp.top_flag,
	tmp.five_years_flag,
	tmp.bb_flag,
	tmp.SPC_flag,
	tmp.item_id,
	tmp.item_family_id,
	item_family_name,
	tmp.item_sub_family_id,
	item_sub_family_name,
	tmp.item_group_code,
	tmp.insert_or_tool_flag,
	tmp.item_code,
	tmp.item_short_name,
	tmp.item_description,
	tmp.customer_number,
	tmp.salesengineer_id,
	tmp.status_sales_engineer,
	tmp.status_branch_manager,
	tmp.status_ho,
	tmp.budget_id
























GO


