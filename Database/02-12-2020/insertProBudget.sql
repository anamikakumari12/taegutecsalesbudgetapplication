USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[insertProBudget]    Script Date: 02-12-2020 15:59:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[insertProBudget]
(@budget_year int,
 @customer_number varchar(50),
 @salesengineer_id varchar(50),

 @item_code varchar(50),
 @estimated_qty int = null,
 @estimated_rate decimal(22,5) =null,
 @estimated_value decimal(22,5) = null,

 @status_sales_engineer varchar(50) = null,
 @status_branch_manager varchar(50)= null,
 @status_ho varchar(50)= null,
 @Review_flag varchar(50) = null,

 @Error varchar(200) output,
 @ErrNum int output,
 @budget_id int = null,
 @ExistID int output
 ) 
As
Begin
	DECLARE @l_bud_count		INT;
	DECLARE @l_bud_line_count	INT;
	DECLARE @l_budget_id		INT;
	
	SELECT @l_bud_count = COUNT(DISTINCT (customer_number))
	FROM   tt_budget_master 
	WHERE  customer_number = @customer_number
	and budget_year = @budget_year
	PRINT @l_bud_count
	IF (@l_bud_count = 0)
		INSERT tt_budget_master
			(budget_year, 
			 customer_number,
			 salesengineer_id, 
			 status_sales_engineer, 
			 status_branch_manager,
			 status_ho
		) VALUES (
			@budget_year, 
			@customer_number, 
			@salesengineer_id,
			@status_sales_engineer , 
			@status_branch_manager, 
			@status_ho)
	
	SELECT @l_budget_id = budget_id 
	FROM   tt_budget_master
	WHERE budget_year = @budget_year 
	AND customer_number = @customer_number


	IF (@l_bud_count <> 0)
		--- update Statements
		UPDATE tt_budget_master
		SET --salesengineer_id = @salesengineer_id,
		 status_sales_engineer = @status_sales_engineer,
		 status_branch_manager = @status_branch_manager,
		 status_ho = @status_ho 
		WHERE budget_id = @l_budget_id

	

	-- Now do the same for the lines too
	-- check if the line exists, if does update else insert

	SELECT @l_bud_line_count =  COUNT (DISTINCT budget_id)
	FROM   tt_budget_numbers 
	WHERE  budget_id = @l_budget_id 
	AND    item_code = @item_code

	IF (@l_bud_line_count = 0)

		INSERT tt_budget_numbers  
			(budget_id ,	 
			[item_code] ,
			[estimated_qty] ,
			[estimated_rate] ,
			[estimated_value] ,
			Review_flag
		) Values(
			@l_budget_id,
			 @item_code,
			 @estimated_qty,
			 @estimated_rate,
			 @estimated_value,
			 @Review_flag)

	ELSE

		UPDATE tt_budget_numbers
		SET    estimated_qty = @estimated_qty,
			estimated_rate = @estimated_rate,
			estimated_value = @estimated_value,
			Review_flag = @Review_flag
		WHERE  budget_id = @l_budget_id
		AND    item_code = @item_code

		

		--if(@status_ho='APPROVE')
		--BEGIN
			DECLARE @budget_main_id INT
			SELECT @budget_main_id=budget_id FROM tt_budget_master where customer_number=@customer_number AND budget_year=[dbo].[get_profile_value]('B_BUDGET_YEAR')

			IF EXISTS(SELECT * FROM tt_budget_numbers WHERE budget_id=@budget_main_id AND item_code=@item_code)
			BEGIN
				UPDATE tt_budget_numbers SET estimated_qty=@estimated_qty,[estimated_rate]=@estimated_rate, [estimated_value]=@estimated_value
				WHERE budget_id=@budget_main_id AND item_code=@item_code
			END
			ELSE
			BEGIN

			INSERT INTO tt_budget_numbers
			(budget_id ,	 
			[item_code] ,
			[estimated_qty] ,
			[estimated_rate] ,
			[estimated_value] ,
			Review_flag
		) Values(
			@budget_main_id,
			 @item_code,
			 @estimated_qty,
			 @estimated_rate,
			 @estimated_value,
			 @Review_flag)
			 
			END

		--END

		SET @ExistID  = @l_budget_id
RETURN
END