USE [Taegutec_Sales_Budget]
GO
/****** Object:  UserDefinedFunction [dbo].[get_sales]    Script Date: 02-12-2020 15:34:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  FUNCTION [dbo].[get_sales] 
			(@p_year	int,
			@p_item_code	varchar(50),
			@p_cust_number	varchar(10),
			@p_qty_rate_flag	varchar(50))
RETURNS numeric(22,5) AS
BEGIN
	DECLARE @ret numeric(22,5) ;
	DECLARE @qty numeric(22,5);
	DECLARE @rate numeric(22,5);
	DECLARE @rate_new  numeric(22,5);
	DECLARE @val numeric(22,5);

		SELECT  @qty = SUM(quantity), @val = SUM(value)
		FROM   [dbo].[tt_salesnumbers]
		WHERE  inv_year = @p_year
		AND    item_code = ISNULL(@p_item_code, item_code)
		AND    customer_number = @p_cust_number


	IF (@p_qty_rate_flag = 'Q')
		SET @ret = @qty;
	ELSE
		BEGIN
			IF (@p_qty_rate_flag = 'V')
				SET @ret = @val
			ELSE

				BEGIN

					SELECT @rate_new = estimated_rate
					FROM   tt_budget_master m, tt_budget_numbers n
					WHERE  m.budget_id = n.budget_id	
					AND    m.customer_number = @p_cust_number
					AND    n.item_code = @p_item_code
					AND    m.budget_year = @p_year

				IF (@rate_new IS NULL) 
					BEGIN

					Select @ret=Unit_price from tt_SFEED_price 
					WHERE item_code=@p_item_code

					IF @ret=0 or @ret IS NULL
					BEGIN
							SELECT  @qty = SUM(quantity), @val = SUM(value)
							FROM   [dbo].[tt_salesnumbers]
							WHERE  inv_year = @p_year-1
							AND    item_code = @p_item_code
							AND    customer_number = @p_cust_number

							IF @qty = 0 
								SET @ret = 0
							ELSE
								SET @ret = @val/@qty SET @ret = @ret*[dbo].[get_profile_value]('INC_RATE')
						END
					END
				ELSE
					SET @RET = @rate_new

				END
		END
		

	RETURN @ret
END

