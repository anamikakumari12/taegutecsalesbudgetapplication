CREATE TABLE tt_userconfiguration(
ID INT IDENTITY(1,1),
UserId VARCHAR(100),
MappedUserId VARCHAR(MAX)
)
--Select CONCAT(',(''',EngineerId,''', ''',EngineerId,''')') from LoginInfo where RoleID='SE' and LoginStatus='1'
INSERT INTO tt_userconfiguration(UserId,MappedUserId)
VALUES('126', '126')
,('D19', 'D19')
,('57', '57')
,('111', '111')
,('D639', 'D639')
,('D658', 'D658')
,('114', '114')
,('03', '03')
,('D642', 'D642')
,('TS005', 'TS005')
,('124', '124')
,('09', '09')
,('64', '64')
,('622', '622')
,('D35', 'D35')
,('D28', 'D28')
,('84', '84')
,('D37', 'D37')
,('617', '617')
,('D39', 'D39')
,('D641', 'D641')
,('D24', 'D24')
,('73', '73')
,('58', '58')
,('TS004', 'TS004')
,('108', '108')
,('116', '116')
,('122', '122')
,('616', '616')
,('637', '637')
,('117', '117')
,('D38', 'D38')
,('101', '101')
,('TS003', 'TS003')
,('123', '123')
,('83', '83')
,('D645', 'D645')
,('17', '17')
,('113', '113')
,('619', '619')
,('115', '115')
,('74', '74')
,('661', '661')
,('107', '107')
,('59', '59')
,('120', '120')
,('109', '109')
,('D23', 'D23')
,('22', '22')
,('95', '95')
,('88', '88')
,('D36', 'D36')
,('621', '621')
,('69', '69')
,('121', '121')
,('55', '55')
,('125', '125')
,('119', '119')
,('D40', 'D40')
,('112', '112')
,('D26', 'D26')
,('564', '564')
,('618', '618')

INSERT INTO tt_userconfiguration(UserId,MappedUserId)
VALUES ('75', '124,75,74,121,564')
,('16', '03,617,16,58')
,('72', '126,57,114,72,123,619,107,55')
,('71', '71,616,101,83,113,115')
,('42', '622,42,125')
,('11', '111,116,637,11,22,88,119')
,('D12', 'D28,D38,D12')
,('80', '64,122,117,661,80,120,621')
,('89', '95,89,112,618')
,('45', '108,17,69,45')


INSERT INTO tt_userconfiguration(UserId,MappedUserId)
VALUES ('H_99',
'126,D19,57,111,D639,D658,114,29,03,D642,TS005,T01,T02,T03,T04,T05,T06,T07,T08,T09,T10,124,09,64,622,D35,D28,84,D37,617,D39,D641,D24,73,75,16,58,72,TS004,108,71,42,116,122,616,637,117,D38,101,11,TS003,123,83,D645,17,113,619,D12,115,74,661,80,107,59,120,D01,109,D23,22,95,88,D36,621,D659,69,121,55,125,119,D40,62,89,112,D26,45,564,618'
)

CREATE Procedure [dbo].[sp_UpdateUserinfo]
(@Empnumber varchar(50),
 @Empname varchar(50),
 @Empmail varchar(50),
 @Empcontact  bigint,
 @Empstatus int ,
 @Empusertype varchar(50),
 @Empusermapping VARCHAR(MAX)
)
As
Begin
IF EXISTS(Select * from LoginInfo WHERE EngineerId=@Empnumber) 
 update LoginInfo set EngineerId=@Empnumber,EngineerName=@Empname,EmailId=@Empmail,
    PhoneNumber=@Empcontact,LoginStatus=@Empstatus,RoleId=@Empusertype where EngineerId=@Empnumber

ELSE
BEGIN
	INSERT INTO LoginInfo(EngineerId, EngineerName, EmailId,RoleId,Password,LoginStatus,PhoneNumber)
	VALUES(@Empnumber,@Empname, @Empmail,@Empusertype,'Z6HePKN/oxTiAwtLKBMm4A==', @Empstatus,@Empcontact)
END

IF EXISTS(Select * from tt_userconfiguration WHERE UserId=@Empnumber) 
	UPDATE tt_userconfiguration SET MappedUserId= @Empusermapping WHERE UserId=@Empnumber
ELSE
	INSERT INTO tt_userconfiguration(UserId, MappedUserId)
	VALUES(@Empnumber, @Empusermapping)

SELECT  1 'Output'
END


CREATE PROCEDURE sp_getAllEnginners
AS
BEGIN
SELECT li.EngineerId,li.EngineerName,li.RoleId,rgn.region_description,li.EmailId,li.PhoneNumber,li.LoginStatus, rgn.region_code, t.MappedUserId
		FROM   LoginInfo li 
		LEFT OUTER JOIN tt_region rgn on rgn.region_code = li.BranchCode
		JOIN tt_userconfiguration t ON li.EngineerId=t.UserID

END


CREATE PROCEDURE sp_getMasterDropodwns
(@user_id VARCHAR(100),
@flag VARCHAR(100),
@cter VARCHAR(100)=null,
@branchcode VARCHAR(MAX)=null,
@assigned_salesengineer_id VARCHAR(MAX)=null
)
AS
BEGIN
DECLARE @QUERY VARCHAR(MAX), @MappedUsers VARCHAR(MAX)

Select @MappedUsers=CONCAT('''',Replace(MappedUserId,',',''','''),'''') from tt_userconfiguration where UserId=@user_id
IF(@flag='MappedUsers')
BEGIN
Select @MappedUsers MappedUsers
END
ELSE
BEGIN
IF(@flag='Branch')
BEGIN
SET @QUERY='Select distinct tr.region_code Branch_Code, tr.region_description Branch from tt_customer_master cm 
JOIN tt_region tr ON cm.Customer_region=tr.region_code where cm.assigned_salesengineer_id IN ('+@MappedUsers+')'
END
ELSE IF(@flag='Engineer')
BEGIN
SET @QUERY='Select distinct cm.assigned_salesengineer_id EngineerId, li.EngineerName  from tt_customer_master cm 
JOIN LoginInfo li ON cm.assigned_salesengineer_id=li.EngineerId where cm.assigned_salesengineer_id IN ('+@MappedUsers+')'
END
ELSE IF(@flag='Customer')
BEGIN
SET @QUERY='Select distinct customer_short_name, customer_name, customer_number   from tt_customer_master cm
where assigned_salesengineer_id IN ('+@MappedUsers+')'
END
ELSE IF(@flag='Company')
BEGIN
SET @QUERY='Select distinct cter  from tt_customer_master cm
where assigned_salesengineer_id IN ('+@MappedUsers+')'
END
IF(@cter IS NOT NULL AND @cter<>'')
SET @QUERY+=' AND cm.cter='''+@cter+''''

IF(@branchcode IS NOT NULL AND @branchcode<>'' AND @branchcode<>'ALL')
SET @QUERY+=' AND cm.Customer_region IN('''+@branchcode+''')'

IF(@assigned_salesengineer_id IS NOT NULL AND @assigned_salesengineer_id<>'' AND @assigned_salesengineer_id<>'ALL')
SET @QUERY+=' AND cm.assigned_salesengineer_id IN('''+@assigned_salesengineer_id+''')'


PRINT(@QUERY)
EXEC (@QUERY)
END
END

