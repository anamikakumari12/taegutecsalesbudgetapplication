﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using TSBA_BusinessAccessLayer;
using TSBA_BusinessObjects;

namespace TaegutecSalesBudget
{
    public partial class PendingQuotes : System.Web.UI.Page
    {
        CommonFunctions objCom = new CommonFunctions();
        public static string cter;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }
            //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            //scriptManager.RegisterPostBackControl();
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["RoleId"]) == "HO")
                {
                    if (Session["cter"] == null)
                    {
                        Session["cter"] = "TTA";
                        cter = "TTA";

                    }
                    if (Session["cter"].ToString() == "DUR")
                    {
                        rdBtnDuraCab.Checked = true;
                        rdBtnTaegutec.Checked = false;
                        cter = "DUR";
                    }
                    else
                    {
                        rdBtnTaegutec.Checked = true;
                        rdBtnDuraCab.Checked = false;
                        cter = "TTA";
                    }
                    cterDiv.Visible = true;
                }
                else
                {
                    cterDiv.Visible = false;
                }
                BindGrid();
            }
        }
        private void RegisterDownloadPostBackControl()
        {

        }
        private void BindGrid()
        {
            try
            {
                string start_date = string.Empty;
                string end_date = string.Empty;
                string selectedDate = txtDateRange.Text;
                if (!string.IsNullOrEmpty(selectedDate))
                {
                    if (selectedDate.Contains("/"))
                    {
                        string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        start_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                        end_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                    }
                }
                QuoteBO objQuoteBO = new QuoteBO();
                objQuoteBO.StartDate = start_date;
                objQuoteBO.EndDate = end_date;
                objQuoteBO.cter = cter;
                objQuoteBO.role_id = Convert.ToString(Session["RoleId"]);
                objQuoteBO.user_id = Convert.ToString(Session["UserId"]);
                QuoteBL objQuoteBL = new QuoteBL();
                DataTable dt = new DataTable();
                try
                {

                    dt = objQuoteBL.GetPendingTasksBL(objQuoteBO);
                    Session["PendingQuotes"] = dt;
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            grdPendingQuote.DataSource = dt;
                            grdPendingQuote.DataBind();
                            if (Convert.ToString(Session["RoleId"]) == "HO")
                            {
                                grdPendingQuote.Columns[5].Visible = true; //Ord Validity
                                grdPendingQuote.Columns[6].Visible = true;//Brk Qty
                                grdPendingQuote.Columns[7].Visible = true; //Sys Price
                                grdPendingQuote.Columns[8].Visible = true;//GP(Sys)
                                grdPendingQuote.Columns[9].Visible = true;// Rec. Price
                                grdPendingQuote.Columns[10].Visible = true;//Special Price
                                grdPendingQuote.Columns[11].Visible = true;//GP(Special)
                                foreach (GridViewRow gvr in grdPendingQuote.Rows)
                                {
                                    TextBox type = ((TextBox)gvr.FindControl("txtRecPrice"));
                                    type.Enabled = false;
                                }
                            }
                            else
                            {
                                grdPendingQuote.Columns[5].Visible = false; //Ord Validity
                                grdPendingQuote.Columns[6].Visible = false;//Brk Qty
                                grdPendingQuote.Columns[7].Visible = false; //Sys Price
                                grdPendingQuote.Columns[8].Visible = false;//GP(Sys)
                                grdPendingQuote.Columns[10].Visible = false;//Special Price
                                grdPendingQuote.Columns[11].Visible = false;//GP(Special)
                                if (Convert.ToString(Session["RoleId"]) == "SE")
                                {
                                    grdPendingQuote.Columns[9].Visible = false;// Rec. Price
                                }
                                else
                                {
                                    grdPendingQuote.Columns[9].Visible = true;// Rec. Price
                                }
                            }
                        }
                        else
                        {
                            lblresult.Text = "No pending quote is available.";
                            grdPendingQuote.DataSource = null;
                            grdPendingQuote.DataBind();
                        }
                    }
                    else
                    {
                        lblresult.Text = "No pending quote is available.";
                        grdPendingQuote.DataSource = null;
                        grdPendingQuote.DataBind();
                    }

                }
                catch (Exception ex)
                {
                    lblresult.Text = "Error in loading. Please try again.";
                    objCom.LogError(ex);
                }
                try
                {

                    dt = new DataTable();
                    dt = objQuoteBL.GetEscalatedQuotesBL(objQuoteBO);
                    Session["EscalatedQuotes"] = dt;
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            grdEscalatedQuote.DataSource = dt;
                            grdEscalatedQuote.DataBind();
                            if (Convert.ToString(Session["RoleId"]) == "HO")
                            {
                                grdEscalatedQuote.Columns[8].Visible = true;
                                grdEscalatedQuote.Columns[10].Visible = true;
                                grdEscalatedQuote.Columns[11].Visible = true;
                                grdEscalatedQuote.Columns[12].Visible = true;
                                grdEscalatedQuote.Columns[16].Visible = true;
                                grdEscalatedQuote.Columns[18].Visible = true;
                            }
                            else
                            {
                                grdEscalatedQuote.Columns[8].Visible = false;
                                grdEscalatedQuote.Columns[10].Visible = false;
                                grdEscalatedQuote.Columns[11].Visible = false;
                                grdEscalatedQuote.Columns[12].Visible = false;
                                grdEscalatedQuote.Columns[16].Visible = false;
                                grdEscalatedQuote.Columns[18].Visible = false;
                            }
                        }
                        else
                        {
                            lblEscresult.Text = "No escalated quote is available.";
                            grdEscalatedQuote.DataSource = null;
                            grdEscalatedQuote.DataBind();
                        }
                    }
                    else
                    {
                        lblEscresult.Text = "No escalated quote is available.";
                        grdEscalatedQuote.DataSource = null;
                        grdEscalatedQuote.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    lblEscresult.Text = "Error in loading. Please try again.";
                    objCom.LogError(ex);
                }
                try
                {

                    dt = new DataTable();
                    dt = objQuoteBL.GetIntEscalatedQuotesBL(objQuoteBO);
                    Session["IntEscalatedQuotes"] = dt;
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            grdIntEscalatedQuote.DataSource = dt;
                            grdIntEscalatedQuote.DataBind();
                            if (Convert.ToString(Session["RoleId"]) == "HO")
                            {
                                grdIntEscalatedQuote.Columns[8].Visible = true;
                                grdIntEscalatedQuote.Columns[10].Visible = true;
                                grdIntEscalatedQuote.Columns[11].Visible = true;
                                grdIntEscalatedQuote.Columns[12].Visible = true;
                                grdIntEscalatedQuote.Columns[16].Visible = true;
                                grdIntEscalatedQuote.Columns[18].Visible = true;
                            }
                            else
                            {
                                grdIntEscalatedQuote.Columns[8].Visible = false;
                                grdIntEscalatedQuote.Columns[10].Visible = false;
                                grdIntEscalatedQuote.Columns[11].Visible = false;
                                grdIntEscalatedQuote.Columns[12].Visible = false;
                                grdIntEscalatedQuote.Columns[16].Visible = false;
                                grdIntEscalatedQuote.Columns[18].Visible = false;
                            }
                        }
                        else
                        {
                            lblIEscresult.Text = "No escalated quote is available.";
                            grdIntEscalatedQuote.DataSource = null;
                            grdIntEscalatedQuote.DataBind();
                        }
                    }
                    else
                    {
                        lblIEscresult.Text = "No escalated quote is available.";
                        grdIntEscalatedQuote.DataSource = null;
                        grdIntEscalatedQuote.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    lblIEscresult.Text = "Error in loading. Please try again.";
                    objCom.LogError(ex);
                }
            }
            catch (Exception ex)
            {
                lblresult.Text = "Error in loading. Please try again.";
                objCom.LogError(ex);
            }
        }

        //protected void grdPendingQuote_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    QuoteBO objQuoteBO = new QuoteBO();
        //    QuoteBL objQuoteBL = new QuoteBL();
        //    try
        //    {
        //        GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
        //        var Comment = ((System.Web.UI.WebControls.TextBox)row.FindControl("txtComment")).Text;
        //        var OfferPrice = ((System.Web.UI.WebControls.TextBox)row.FindControl("txtOfferPrice")).Text;
        //        var MOQ = ((System.Web.UI.WebControls.TextBox)row.FindControl("txtMOQ")).Text;
        //        var refnum = ((System.Web.UI.WebControls.Label)row.FindControl("lblref")).Text;
        //        var item = ((System.Web.UI.WebControls.Label)row.FindControl("lblitem")).Text;
        //        String reason = row.Cells[16].Text;
        //        objQuoteBO.RefID = Convert.ToInt32(e.CommandArgument);
        //        objQuoteBO.Reason = Convert.ToString(Comment);
        //        objQuoteBO.ChangeBy = Convert.ToString(Session["LoginMailId"]);
        //        objQuoteBO.MOQ = Convert.ToString(MOQ);
        //        objQuoteBO.Offer_Price = Convert.ToString(OfferPrice);
        //        objQuoteBO.Ref_Number = Convert.ToString(refnum);
        //        objQuoteBO.Item_Number = Convert.ToString(item);
        //        objQuoteBO.role_id = Convert.ToString(Session["RoleId"]);
        //        if (e.CommandName == "Approve")
        //        {
        //            objQuoteBO.Status = "Approved";
        //            objQuoteBO = objQuoteBL.updateQuoteStatusBL(objQuoteBO);
        //            if (objQuoteBO.Err_code == 0)
        //            {
        //                if (!String.IsNullOrEmpty(Convert.ToString(objQuoteBO.to)))
        //                {
        //                    EmailDetails objEmail = new EmailDetails();
        //                    objEmail.toMailId = objQuoteBO.to;
        //                    objEmail.ccMailId = objQuoteBO.cc;
        //                    objEmail.subject = objQuoteBO.subject;
        //                    objEmail.body = objQuoteBO.message;
        //                    objCom.SendMail(objEmail);
        //                }
        //                BindGrid();
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "alert('" + objQuoteBO.Err_msg + "');", true);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "alert('" + objQuoteBO.Err_msg + "');", true);
        //            }
        //        }
        //        else if (e.CommandName == "Reject")
        //        {
        //            objQuoteBO.Status = "Rejected";
        //            objQuoteBO = objQuoteBL.updateQuoteStatusBL(objQuoteBO);
        //            if (objQuoteBO.Err_code == 0)
        //            {
        //                if (!String.IsNullOrEmpty(Convert.ToString(objQuoteBO.to)))
        //                {
        //                    EmailDetails objEmail = new EmailDetails();
        //                    objEmail.toMailId = objQuoteBO.to;
        //                    objEmail.ccMailId = objQuoteBO.cc;
        //                    objEmail.subject = objQuoteBO.subject;
        //                    objEmail.body = objQuoteBO.message;
        //                    objCom.SendMail(objEmail);
        //                }
        //                BindGrid();
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "alert('" + objQuoteBO.Err_msg + "');", true);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "alert('" + objQuoteBO.Err_msg + "');", true);
        //            }
        //        }
        //        else
        //        {
        //            return;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.LogError(ex);
        //    }
        //}
        //protected void grdEscalatedQuote_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    QuoteBO objQuoteBO = new QuoteBO();
        //    QuoteBL objQuoteBL = new QuoteBL();
        //    try
        //    {
        //        GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
        //        var Comment = ((System.Web.UI.WebControls.TextBox)row.FindControl("txtComment")).Text;
        //        var OfferPrice = ((System.Web.UI.WebControls.TextBox)row.FindControl("txtOfferPrice")).Text;
        //        var MOQ = ((System.Web.UI.WebControls.TextBox)row.FindControl("txtMOQ")).Text;
        //        var refnum = ((System.Web.UI.WebControls.Label)row.FindControl("lblref")).Text;
        //        var item = ((System.Web.UI.WebControls.Label)row.FindControl("lblitem")).Text;
        //        String reason = row.Cells[16].Text;
        //        objQuoteBO.RefID = Convert.ToInt32(e.CommandArgument);
        //        objQuoteBO.Reason = Convert.ToString(Comment);
        //        objQuoteBO.ChangeBy = Convert.ToString(Session["LoginMailId"]);
        //        objQuoteBO.MOQ = Convert.ToString(MOQ);
        //        objQuoteBO.Offer_Price = Convert.ToString(OfferPrice);
        //        objQuoteBO.Ref_Number = Convert.ToString(refnum);
        //        objQuoteBO.Item_Number = Convert.ToString(item);
        //        objQuoteBO.role_id = Convert.ToString(Session["RoleId"]);
        //        if (e.CommandName == "Approve")
        //        {
        //            if (objQuoteBO.role_id == "BM")
        //                objQuoteBO.Status = "Escalated By BM";
        //            else
        //                objQuoteBO.Status = "Approved";
        //            objQuoteBO = objQuoteBL.updateQuoteStatusBL(objQuoteBO);
        //            if (objQuoteBO.Err_code == 0)
        //            {
        //                if (!String.IsNullOrEmpty(Convert.ToString(objQuoteBO.to)))
        //                {
        //                    EmailDetails objEmail = new EmailDetails();
        //                    objEmail.toMailId = objQuoteBO.to;
        //                    objEmail.ccMailId = objQuoteBO.cc;
        //                    objEmail.subject = objQuoteBO.subject;
        //                    objEmail.body = objQuoteBO.message;
        //                    objCom.SendMail(objEmail);
        //                }
        //                BindGrid();
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "alert('" + objQuoteBO.Err_msg + "');", true);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "alert('" + objQuoteBO.Err_msg + "');", true);
        //            }
        //        }
        //        else if (e.CommandName == "Reject")
        //        {
        //            objQuoteBO.Status = "Rejected";
        //            objQuoteBO = objQuoteBL.updateQuoteStatusBL(objQuoteBO);
        //            if (objQuoteBO.Err_code == 0)
        //            {
        //                if (!String.IsNullOrEmpty(Convert.ToString(objQuoteBO.to)))
        //                {
        //                    EmailDetails objEmail = new EmailDetails();
        //                    objEmail.toMailId = objQuoteBO.to;
        //                    objEmail.ccMailId = objQuoteBO.cc;
        //                    objEmail.subject = objQuoteBO.subject;
        //                    objEmail.body = objQuoteBO.message;
        //                    objCom.SendMail(objEmail);
        //                }
        //                BindGrid();
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "alert('" + objQuoteBO.Err_msg + "');", true);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "alert('" + objQuoteBO.Err_msg + "');", true);
        //            }
        //        }
        //        else
        //        {
        //            return;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.LogError(ex);
        //    }
        //}
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            BindGrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "LoadTable()", true);
        }
        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (rdBtnTaegutec.Checked)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                }
                if (rdBtnDuraCab.Checked)
                {
                    Session["cter"] = "DUR";
                    cter = "DUR";
                }
                BindGrid();
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        [WebMethod]
        //public static string UpdateQuoteStatus(string id, string item, string ref_no, string status, string comment, string MOQ, string offerPrice, string multiorder_flag, string RecPrice)
        public static string UpdateQuoteStatus(List<QuoteStatus> objList)
        {
            string output = string.Empty;
            string status = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            List<QuoteStatusDB> quoteList = new List<QuoteStatusDB>();
            try
            {
                QuoteStatusDB objQuoteBO = new QuoteStatusDB();
                QuoteBL objQuoteBL = new QuoteBL();
                foreach (QuoteStatus obj in objList)
                {
                    objQuoteBO = new QuoteStatusDB();
                    objQuoteBO.ID = Convert.ToInt32(obj.id);
                    objQuoteBO.Reason = Convert.ToString(obj.comment);
                    objQuoteBO.changeby = Convert.ToString(HttpContext.Current.Session["UserId"]);
                    objQuoteBO.MOQ = Convert.ToString(obj.MOQ);
                    objQuoteBO.OfferPrice = Convert.ToString(obj.offerPrice);
                    objQuoteBO.RefNumber = Convert.ToString(obj.ref_no);
                    objQuoteBO.item = Convert.ToString(obj.item);
                    objQuoteBO.MultiOrderFlag = Convert.ToInt32(obj.multiorder_flag);
                    objQuoteBO.Status = Convert.ToString(obj.status);
                    objQuoteBO.flag = Convert.ToString(HttpContext.Current.Session["RoleId"]);
                    objQuoteBO.RecommendedPrice = Convert.ToString(obj.RecPrice);
                    objQuoteBO.Order_Validity = Convert.ToString(obj.Order_Validity);
                    objQuoteBO.Approved_OrderQty = Convert.ToString(obj.Approved_OrderQty);
                    objQuoteBO.Approved_OrderFreq = Convert.ToString(obj.Approved_OrderFreq);
                    if (Convert.ToString(HttpContext.Current.Session["RoleId"]) == "HO")
                    { 
                        objQuoteBO.HO_Approval_file = Convert.ToString(obj.file);
                        objQuoteBO.Internal_Remarks = Convert.ToString(obj.Internal_Remarks);
                }
                    else
                    objQuoteBO.BM_Approval_file = Convert.ToString(obj.file);
                    status = obj.status;
                    quoteList.Add(objQuoteBO);
                }
                DataTable dtQuote = new DataTable();
                dtQuote = CommonFunctions.ToDataTable<QuoteStatusDB>(quoteList);
                List<QuoteBO> objQuoteList = new List<QuoteBO>();
                objQuoteList = objQuoteBL.updateQuoteStatusBL(dtQuote);
                if (objQuoteList.Count > 0)
                {
                    foreach (QuoteBO objOutputBO in objQuoteList)
                    {
                        if (status== "Approved")
                        {
                            if (!String.IsNullOrEmpty(Convert.ToString(objOutputBO.to)))
                            {
                                EmailDetails objEmail = new EmailDetails();
                                string attachment = string.Empty;
                                if (objOutputBO.pdf_flag == 1)
                                {
                                    attachment = GenerateQuoteFormatByItem(objOutputBO.Ref_Number, objOutputBO.Quotation_no,null);
                                }
                                objEmail.toMailId = objOutputBO.to;
                                objEmail.ccMailId = objOutputBO.cc;
                                objEmail.subject = objOutputBO.subject;
                                objEmail.body = objOutputBO.message;
                                objEmail.attachment = attachment;
                                //objCom.SendMail(objEmail);
                                CommonFunctions.SendGridMail(objEmail).Wait();
                            }
                        }
                        else if ( status == "Escalated & Approved")
                        {
                            if (!String.IsNullOrEmpty(Convert.ToString(objOutputBO.to)))
                            {
                                EmailDetails objEmail = new EmailDetails();
                                string attachment = string.Empty;
                                if (objOutputBO.pdf_flag == 1)
                                {
                                    attachment = GenerateQuoteFormatByItem(objOutputBO.Ref_Number, objOutputBO.Quotation_no, quoteList[0].item);
                                }
                                objEmail.toMailId = objOutputBO.to;
                                objEmail.ccMailId = objOutputBO.cc;
                                objEmail.subject = objOutputBO.subject;
                                objEmail.body = objOutputBO.message;
                                objEmail.attachment = attachment;
                                //objCom.SendMail(objEmail);
                                CommonFunctions.SendGridMail(objEmail).Wait();
                            }
                        }
                        else if (status == "Escalated By BM")
                        {
                            if (!String.IsNullOrEmpty(Convert.ToString(objOutputBO.to)))
                            {
                                EmailDetails objEmail = new EmailDetails();
                                objEmail.toMailId = objOutputBO.to;
                                objEmail.ccMailId = objOutputBO.cc;
                                objEmail.subject = objOutputBO.subject;
                                objEmail.body = objOutputBO.message;
                                //objCom.SendMail(objEmail);
                                CommonFunctions.SendGridMail(objEmail).Wait();
                            }
                        }
                        output = "{\"code\":\"" + objOutputBO.Err_code + "\",\"msg\":\"" + objOutputBO.Err_msg + "\"}";
                    }
                }
                //objQuoteBO.QuoteID = Convert.ToInt32(id);
               // objQuoteBO.Reason = Convert.ToString(comment);
               // objQuoteBO.ChangeBy = Convert.ToString(HttpContext.Current.Session["UserId"]);
                //objQuoteBO.MOQ = Convert.ToString(MOQ);
                //objQuoteBO.Offer_Price = Convert.ToString(offerPrice);
                //objQuoteBO.Ref_Number = Convert.ToString(ref_no);
                //objQuoteBO.Item_Number = Convert.ToString(item);
                //objQuoteBO.MultiOrderFlag = Convert.ToInt32(multiorder_flag);
                //objQuoteBO.Status = Convert.ToString(status);
                //objQuoteBO.role_id = Convert.ToString(HttpContext.Current.Session["RoleId"]);
               // objQuoteBO.RecommendedPrice = Convert.ToString(RecPrice);
                //QuoteBO objOutputBO = new QuoteBO();
                //objOutputBO = objQuoteBL.updateQuoteStatusBL(objQuoteBO);
                //DataTable dt = new DataTable();
                //dt.Columns.Add("Message");
                //DataRow dr = dt.NewRow();
                //dr["Message"] = objOutputBO.Err_msg;
                //dt.Rows.Add(dr);
                //if (!String.IsNullOrEmpty(Convert.ToString(objOutputBO.to)))
                //{
                //    EmailDetails objEmail = new EmailDetails();
                //    objEmail.toMailId = objOutputBO.to;
                //    objEmail.ccMailId = objOutputBO.cc;
                //    objEmail.subject = objOutputBO.subject;
                //    objEmail.body = objOutputBO.message;
                //    objCom.SendMail(objEmail);
                //}
                //output = "{\"code\":\"" + objOutputBO.Err_code + "\",\"msg\":\"" + objOutputBO.Err_msg + "\"}";
            }
            catch (Exception ex)
            {
                output = "{\"code\":\"105\",\"msg\":\"" + ex.Message + "\"}";
                objCom.LogError(ex);
            }
            return output;
        }

        
        protected void grdPendingQuote_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtRecPrice = (TextBox)e.Row.FindControl("txtRecPrice");
                TextBox txtExpiryDate = (TextBox)e.Row.FindControl("txtExpiryDate");

                TextBox txtOrdValidity = (TextBox)e.Row.FindControl("txtOrdValidity");
                TextBox txtOrderQuant = (TextBox)e.Row.FindControl("txtOrderQuant");
                TextBox txtMOQ = (TextBox)e.Row.FindControl("txtMOQ");
                TextBox txtOfferPrice = (TextBox)e.Row.FindControl("txtOfferPrice");
                TextBox txtNewOfferPrice = (TextBox)e.Row.FindControl("txtNewOfferPrice");
                if (Convert.ToString(Session["RoleId"]) == "SE")
                {
                    txtOrdValidity.Enabled = false;
                    txtOrderQuant.Enabled = false;
                    txtMOQ.Enabled = false;
                    txtOfferPrice.Enabled = false;
                    txtNewOfferPrice.Enabled = false;
                    //e.Row.Cells[8]
                }
                else
                {
                    txtOrdValidity.Enabled = true;
                    txtOrderQuant.Enabled = true;
                    txtMOQ.Enabled = true;
                    txtOfferPrice.Enabled = true;
                    txtNewOfferPrice.Enabled = true;
                }

                    if (Convert.ToString(Session["RoleId"]) == "HO")
                {
                    txtRecPrice.ReadOnly = true;
                    txtExpiryDate.Enabled = true;
                }
                else
                {
                    txtRecPrice.ReadOnly = false;
                    txtExpiryDate.Enabled = false;
                }
            }
        }

        protected void grdEscalatedQuote_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lb = e.Row.FindControl("fileBM") as LinkButton;
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lb);
                LinkButton lb1 = e.Row.FindControl("fileCP") as LinkButton;
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lb1);
                TextBox txtRecPrice = (TextBox)e.Row.FindControl("txtEscRecPrice");
                //TextBox txtEscExpiryDate = (TextBox)e.Row.FindControl("txtEscExpiryDate");
                if (Convert.ToString(Session["RoleId"]) == "HO")
                {
                    txtRecPrice.ReadOnly = true;
                    txtRecPrice.Enabled = false;
                   // txtEscExpiryDate.ReadOnly = false;
                }
                else if (Convert.ToString(Session["RoleId"]) == "BM" || Convert.ToString(Session["RoleId"]) == "TM")
                {
                    txtRecPrice.Enabled = true;
                    txtRecPrice.ReadOnly = false;
                   // txtEscExpiryDate.ReadOnly = true;
                }
            }
        }

        protected void grdIntEscalatedQuote_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lb = e.Row.FindControl("IfileBM") as LinkButton;
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lb);
                LinkButton lb1 = e.Row.FindControl("IfileCP") as LinkButton;
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lb1);
                TextBox txtRecPrice = (TextBox)e.Row.FindControl("txtIEscRecPrice");
                //TextBox txtEscExpiryDate = (TextBox)e.Row.FindControl("txtEscExpiryDate");
                if (Convert.ToString(Session["RoleId"]) == "HO")
                {
                    txtRecPrice.ReadOnly = true;
                    txtRecPrice.Enabled = false;
                    // txtEscExpiryDate.ReadOnly = false;
                }
                else if (Convert.ToString(Session["RoleId"]) == "BM" || Convert.ToString(Session["RoleId"]) == "TM")
                {
                    txtRecPrice.Enabled = true;
                    txtRecPrice.ReadOnly = false;
                    // txtEscExpiryDate.ReadOnly = true;
                }
            }
        }

        #region reportGenerate

        private static string GenerateQuoteFormat(string ref_number, string quote_no)
        {
            string file = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                string html = string.Empty;
                string filename = string.Empty;
                string filepath = string.Empty;
                QuoteBL objQuoteBL = new QuoteBL();
                QuoteBO objQuoteBO = new QuoteBO();
                DataTable dt = new DataTable();

                html = "<!DOCTYPE html><html><head><style>table td{border:solid 1px #ddd;padding:5px;}</style></head><body>";
                objQuoteBO.Ref_Number = ref_number;
                objQuoteBO.Quotation_no = quote_no;
                dt = objQuoteBL.getQuoteFormatBL(objQuoteBO);

                html += Getheading(dt, quote_no);

                html += "</body></html>";
                filename = "Quote_" + Convert.ToString(quote_no.Replace('/', '_')) + ".pdf";
                filepath = ConfigurationManager.AppSettings["QuotePDF_Folder"].ToString();
                convertPDF(html, filepath, filename);
                file = String.Concat(filepath, filename);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return file;
        }

        private static string GenerateQuoteFormatByItem(string ref_number, string quote_no, string item)
        {
            string file = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                string html = string.Empty;
                string filename = string.Empty;
                string filepath = string.Empty;
                QuoteBL objQuoteBL = new QuoteBL();
                QuoteBO objQuoteBO = new QuoteBO();
                DataTable dt = new DataTable();

                html = "<!DOCTYPE html><html><head><style>table td{border:solid 1px #ddd;padding:5px;}</style></head><body>";
                objQuoteBO.Ref_Number = ref_number;
                objQuoteBO.Quotation_no = quote_no;
                objQuoteBO.Item_Number = item;
                dt = objQuoteBL.getQuoteFormatBL(objQuoteBO);

                html += Getheading(dt, quote_no);

                html += "</body></html>";
                filename = "Quote_" + Convert.ToString(quote_no.Replace('/', '_')) + ".pdf";
                filepath = ConfigurationManager.AppSettings["QuotePDF_Folder"].ToString();
                convertPDF(html, filepath, filename);
                file = String.Concat(filepath, filename);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return file;
        }
        private static void convertPDF(string html, string filepath, string filename)
        {
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                using (FileStream fs = new FileStream(Path.Combine(filepath, "test.htm"), FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                    {
                        w.WriteLine(html);
                    }
                }


                GeneratePdfFromHtml(filepath, filename, html);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        private static void GeneratePdfFromHtml(string filepath, string filename, string html)
        {
            string outputFilename = Path.Combine(filepath, filename);
            string inputFilename = Path.Combine(filepath, "test.htm");

            using (var input = new FileStream(inputFilename, FileMode.Open))
            using (var output = new FileStream(outputFilename, FileMode.Create))
            {
                CreatePdf(filepath, filename, input, output, html);
            }
        }

        private static void CreatePdf(string filepath, string filename, FileStream htmlInput, FileStream pdfOutput, string html)
        {
            string imageURL;
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                using (var document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 30, 30, 30, 30))
                {
                    var writer = PdfWriter.GetInstance(document, pdfOutput);
                    var worker = XMLWorkerHelper.GetInstance();
                    TextReader tr = new StreamReader(htmlInput);
                    document.Open();
                    worker.ParseXHtml(writer, document, htmlInput, null, Encoding.UTF8);
                    //worker.ParseXHtml(writer, document, new StringReader(html));
                    document.Close();
                }

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        //private static string Getheading(DataTable dt, string quote_no)
        //{
        //    CommonFunctions objCom = new CommonFunctions();
        //    string output = string.Empty;
        //    StringBuilder strHTMLBuilder = new StringBuilder();
        //    string imageURL = string.Empty;
        //    string Heading = string.Empty;
        //    string taegutec_add = string.Empty;
        //    string Customer_Name = string.Empty;
        //    string Customer_Address = string.Empty;
        //    string Customer_Number = string.Empty;
        //    string Quotation_No = string.Empty;
        //    string Date = string.Empty;
        //    string Due_On = string.Empty;
        //    string Tender_No = string.Empty;
        //    string Valid_For = string.Empty;
        //    try
        //    {
        //        Heading = Convert.ToString(ConfigurationManager.AppSettings["QuoteFormat_Heading"]);
        //        imageURL = Convert.ToString(ConfigurationManager.AppSettings["Logo"]);
        //        if (dt != null)
        //        {
        //            if (dt.Rows.Count > 0)
        //            {
        //                Customer_Name = Convert.ToString(dt.Rows[0]["Customer_Name"]);
        //                Customer_Address = Convert.ToString(dt.Rows[0]["Customer_Address"]);
        //                Customer_Number = Convert.ToString(dt.Rows[0]["Customer_Number"]);
        //                Quotation_No = Convert.ToString(dt.Rows[0]["Quotation_No"]);
        //                Date = Convert.ToString(dt.Rows[0]["Date"]);
        //                Due_On = Convert.ToString(dt.Rows[0]["Due_On"]);
        //                Tender_No = Convert.ToString(dt.Rows[0]["Tender_No"]);
        //                Valid_For = Convert.ToString(dt.Rows[0]["Valid_For"]);
        //                taegutec_add = Convert.ToString(dt.Rows[0]["Taegutec_Address"]);


        //                // strHTMLBuilder.Append("<table style='border: 1px solid darkgray; font-family: Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif; border-collapse:collapse;'>");

        //                strHTMLBuilder.Append("<table style='border: 1px solid darkgray; font-family: Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif; border-collapse:collapse;'>");
        //                strHTMLBuilder.Append("<tr style=' background-color: #5faae6c7;'>");
        //                strHTMLBuilder.Append("<td colspan='9' style='text-align:center; font-size:30px; font-weight:bold; color:black;'>");
        //                strHTMLBuilder.Append(Heading);
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("</tr>");
        //                strHTMLBuilder.Append("<tr>");
        //                strHTMLBuilder.Append("<td colspan='4'><img style='float:left;width: 80%;' src='");
        //                strHTMLBuilder.Append(imageURL);
        //                strHTMLBuilder.Append("'/>");
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("<td colspan='5' style='font-weight:bold; font-size: 15px;'>");
        //                strHTMLBuilder.Append(Customer_Name);
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("</tr>");
        //                strHTMLBuilder.Append("<tr>");
        //                strHTMLBuilder.Append("<td colspan='4' style='font-weight:bold; font-size: 10px;'>");
        //                strHTMLBuilder.Append(taegutec_add);
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("<td colspan='5' style='font-weight:bold; font-size: 10px;'>");
        //                strHTMLBuilder.Append(Customer_Address);
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("</tr>");
        //                strHTMLBuilder.Append("<tr >");
        //                strHTMLBuilder.Append("<td colspan='4' >");
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("<td colspan='5'  style='font-weight:bold; font-size: 10px;'>");
        //                strHTMLBuilder.Append("REMARKS:");
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("</tr>");
        //                strHTMLBuilder.Append("<tr >");
        //                strHTMLBuilder.Append("<td colspan='4' style='padding:0px'>");
        //                strHTMLBuilder.Append("<table width='100%'>");
        //                strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>CUSTOMER NO : ");
        //                strHTMLBuilder.Append(Customer_Number);
        //                strHTMLBuilder.Append("</td></tr>");
        //                strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>QUOTATION NO : ");
        //                strHTMLBuilder.Append(quote_no);
        //                strHTMLBuilder.Append("</td></tr>");
        //                strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>RFQ NO : ");
        //                strHTMLBuilder.Append(Quotation_No);
        //                strHTMLBuilder.Append("</td></tr>");
        //                strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>DUE ON : ");
        //                strHTMLBuilder.Append(Due_On);
        //                strHTMLBuilder.Append("</td></tr>");
        //                strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>VALID FOR : ");
        //                strHTMLBuilder.Append(Valid_For);
        //                strHTMLBuilder.Append("</td></tr>");
        //                strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>DATE : ");
        //                strHTMLBuilder.Append(Date);
        //                strHTMLBuilder.Append("</td></tr>");
        //                strHTMLBuilder.Append("</table>");
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("<td colspan='5'>");
        //                strHTMLBuilder.Append("</td>");
        //                strHTMLBuilder.Append("</tr>");

        //                strHTMLBuilder.Append("<tr>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>SL<br/>NO</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>STOCK<br/>CODE</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>ITEM DESCRIPTION</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>CATALOGUE NO</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>ITEM QTY</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>UNIT<br/>PRICE</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>LINE<br/>VALUE</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>DELIVERY<br/>DATE</td>");
        //                strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>END<br/>CUSTOMER</td>");
        //                strHTMLBuilder.Append("</tr>");
        //                for (int i = 0; i < dt.Rows.Count; i++)
        //                {
        //                    strHTMLBuilder.Append("<tr>");
        //                    strHTMLBuilder.Append("<td  style='font-size: 10px; text-align:center;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(i + 1));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px; text-align:center;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Stock_Code"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Catalogue_No"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item_Qty"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px; text-align:right;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Unit_Price"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px; text-align:right;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Line_Value"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Delivery_Date"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("<td style='font-size: 10px;'>");
        //                    strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["End_Customer"]));
        //                    strHTMLBuilder.Append("</td>");
        //                    strHTMLBuilder.Append("</tr>");
        //                }

        //                strHTMLBuilder.Append("</table>");
        //            }
        //        }

        //        output = strHTMLBuilder.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.LogError(ex);
        //    }
        //    return output;
        //}

        private static string Getheading(DataTable dt, string quote_no)
        {
            CommonFunctions objCom = new CommonFunctions();
            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            string imageURL = string.Empty;
            string Heading = string.Empty;
            string taegutec_add = string.Empty;
            string Customer_Name = string.Empty;
            string Customer_Address = string.Empty;
            string Customer_Number = string.Empty;
            string Quotation_No = string.Empty;
            string Date = string.Empty;
            string Due_On = string.Empty;
            string Tender_No = string.Empty;
            string Valid_For = string.Empty;
            string Remarks = string.Empty;
            try
            {
                Heading = Convert.ToString(ConfigurationManager.AppSettings["QuoteFormat_Heading"]);
                imageURL = Convert.ToString(ConfigurationManager.AppSettings["Logo"]);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        Customer_Name = Convert.ToString(dt.Rows[0]["Customer_Name"]);
                        Customer_Address = Convert.ToString(dt.Rows[0]["Customer_Address"]);
                        Customer_Number = Convert.ToString(dt.Rows[0]["Customer_Number"]);
                        Quotation_No = Convert.ToString(dt.Rows[0]["Quotation_No"]);
                        Date = Convert.ToString(dt.Rows[0]["Date"]);
                        Due_On = Convert.ToString(dt.Rows[0]["Due_On"]);
                        Tender_No = Convert.ToString(dt.Rows[0]["Tender_No"]);
                        Valid_For = Convert.ToString(dt.Rows[0]["Valid_For"]);
                        taegutec_add = Convert.ToString(dt.Rows[0]["Taegutec_Address"]);
                        Remarks = Convert.ToString(dt.Rows[0]["Remarks"]);

                        // strHTMLBuilder.Append("<table style='border: 1px solid darkgray; font-family: Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif; border-collapse:collapse;'>");

                        strHTMLBuilder.Append("<table style='border: 1px solid darkgray; font-family: Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif; border-collapse:collapse;'>");
                        strHTMLBuilder.Append("<tr style=' background-color: #5faae6c7;'>");
                        strHTMLBuilder.Append("<td colspan='9' style='text-align:center; font-size:30px; font-weight:bold; color:black;'>");
                        strHTMLBuilder.Append(Heading);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td colspan='4'><img style='float:left;width: 80%;' src='");
                        strHTMLBuilder.Append(imageURL);
                        strHTMLBuilder.Append("'/>");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5' style='font-weight:bold; font-size: 15px;'>");
                        strHTMLBuilder.Append(Customer_Name);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td colspan='4' style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append(taegutec_add);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5' style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append(Customer_Address);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr >");
                        strHTMLBuilder.Append("<td colspan='4' >");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5'  style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append("REMARKS:");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr >");
                        strHTMLBuilder.Append("<td colspan='4' style='padding:0px'>");
                        strHTMLBuilder.Append("<table width='100%'>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>CUSTOMER NO : ");
                        strHTMLBuilder.Append(Customer_Number);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>QUOTATION NO : ");
                        strHTMLBuilder.Append(quote_no);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>RFQ NO : ");
                        strHTMLBuilder.Append(Quotation_No);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>DUE ON : ");
                        strHTMLBuilder.Append(Due_On);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>VALID FOR : ");
                        strHTMLBuilder.Append(Valid_For);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>DATE : ");
                        strHTMLBuilder.Append(Date);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("</table>");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5'>");
                        strHTMLBuilder.Append(Remarks);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");

                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>SL<br/>NO</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>STOCK<br/>CODE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>ITEM DESCRIPTION</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>CATALOGUE NO</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>ITEM QTY</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>UNIT<br/>PRICE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>LINE<br/>VALUE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>DELIVERY<br/>DATE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>END<br/>CUSTOMER</td>");
                        strHTMLBuilder.Append("</tr>");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            strHTMLBuilder.Append("<tr>");
                            strHTMLBuilder.Append("<td  style='font-size: 10px; text-align:center;'>");
                            strHTMLBuilder.Append(Convert.ToString(i + 1));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px; text-align:center;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Stock_Code"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Catalogue_No"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item_Qty"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px; text-align:right;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Unit_Price"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px; text-align:right;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Line_Value"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Delivery_Date"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["End_Customer"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("</tr>");
                        }

                        strHTMLBuilder.Append("</table>");
                    }
                }

                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }
        #endregion


        protected void fileBM_Command(object sender, CommandEventArgs e)
        {
            if (Convert.ToString(e.CommandArgument) != string.Empty)
            {

                // Response.ContentType = "application/octet-stream";
                //string filePath = Server.MapPath("~/Quotes//" + Convert.ToString(e.CommandArgument));
                //if (File.Exists(filePath))
                //{
                //    Response.ContentType = "application/octet-stream";
                //    byte[] bts = System.IO.File.ReadAllBytes(filePath);
                //    MemoryStream ms = new MemoryStream(bts);
                //    Response.Clear();
                //    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + Path.GetFileName(filePath) + "\"");
                //    Response.TransmitFile(filePath);
                //    Response.End();
                //}
                try
                {
                    string filePath = ConfigurationManager.AppSettings["Escalation_Folder"].ToString() + Convert.ToString(e.CommandArgument);
                    if (File.Exists(filePath))
                    {
                        System.IO.FileInfo file = new System.IO.FileInfo(filePath);
                        if (file.Exists)
                        {
                            Response.ContentType = "application/octet-stream";
                            byte[] bts = System.IO.File.ReadAllBytes(filePath);
                            MemoryStream ms = new MemoryStream(bts);
                            Response.Clear();
                            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + Path.GetFileName(filePath) + "\"");
                            Response.TransmitFile(filePath);
                            Response.End();
                        }
                    }
                }
                catch (Exception ex)
                {
                    objCom.LogError(ex);
                }
              
            }
        }

      
    }
}