﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;


namespace TaegutecSalesBudget
{
    class splgrpvisibility
    {
        public string GetBudgetYear()
        {
            string B_BUDGET_YEAR = null;
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("B_BUDGET_YEAR", connection);
                command.CommandType = CommandType.StoredProcedure;
               B_BUDGET_YEAR=Convert.ToString(command.ExecuteScalar());

            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return B_BUDGET_YEAR;
        }


        public string Update_splgrpvisible(int year, string bb_flag = null, string SPC_flag = null, string gold_flag = null, string top_flag = null, string five_years_flag = null, string Ten_Years_flag = null,string Sfeed = null)
        {
            string message = null;
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
              
            try
            {


                connection.Open();
                SqlCommand command = new SqlCommand("updatesplgrpvisible", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter pyear = new SqlParameter("@year", SqlDbType.Int, 50);//1
                command.Parameters.Add(pyear).Value = year;
                SqlParameter pbb = new SqlParameter("@bb", SqlDbType.VarChar, 10);//1
                command.Parameters.Add(pbb).Value = bb_flag;
                SqlParameter pspc = new SqlParameter("@spc", SqlDbType.VarChar, 10);
                command.Parameters.Add(pspc).Value = SPC_flag;
                SqlParameter pgold = new SqlParameter("@gold", SqlDbType.VarChar, 10);
                command.Parameters.Add(pgold).Value = gold_flag;
                SqlParameter ptop = new SqlParameter("@top", SqlDbType.VarChar, 10);
                command.Parameters.Add(ptop).Value = top_flag;
                SqlParameter pfiveyears = new SqlParameter("@fiveyears", SqlDbType.VarChar, 10);
                command.Parameters.Add(pfiveyears).Value = five_years_flag;
                SqlParameter ptenyears = new SqlParameter("@tenyears", SqlDbType.VarChar, 10);
                command.Parameters.Add(ptenyears).Value = Ten_Years_flag;
                SqlParameter psfeed = new SqlParameter("@Sfeed", SqlDbType.VarChar, 10);
                command.Parameters.Add(psfeed).Value = Sfeed;
                command.ExecuteNonQuery();




            }
            catch (Exception ex)
            {
                message = "update failed";
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    message = "SUCCESS";
                    connection.Close();
                }

            }
            return message;
        }





        public DataTable splgrpvisible(int year)
        {
            DataTable dtsplgrpvisible = new DataTable();
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            try
            {


                connection.Open();
                SqlCommand command = new SqlCommand("splgrpvisible", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter parameters = new SqlParameter("@year", SqlDbType.Int, 50);//1
                command.Parameters.Add(parameters).Value = year;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtsplgrpvisible);

            }

            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtsplgrpvisible;
        }

    }
}
