﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="TaegutecSalesBudget.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2014.3.1316/styles/kendo.common.min.css" />
    <link rel="stylesheet" type="text/css" href="css/kendo.default.min.css" />
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2014.3.1316/styles/kendo.dataviz.min.css" />
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2014.3.1316/styles/kendo.dataviz.default.min.css" />

    <%--    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>--%>


    <link href="GridviewScroll.css" rel="stylesheet" />


    <%--   <script src="js/amcharts/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="js/amcharts/animated.js"></script>--%>
<%--    <script src="http://cdn.kendostatic.com/2014.3.1316/js/jquery.min.js"></script>
    <script src="http://cdn.kendostatic.com/2014.3.1316/js/kendo.all.min.js"></script>--%>
    <script src="js/kendostatic/jquery.min.js"></script>
    <script src="js/kendostatic/kendo.all.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    
    <script type="text/javascript" src="gridscroll.js"></script>
    <script type="text/javascript" src="js/app.js"></script>

    <link rel="stylesheet" type="text/css" href="https://dc-js.github.io/dc.js/css/dc.css" />
    <script src="https://dc-js.github.io/dc.js/js/d3.js"></script>
    <%--<script src="js/crossfilter.js"></script>--%>
    <script src="https://dc-js.github.io/dc.js/js/crossfilter.js"></script>
    <script src="https://dc-js.github.io/dc.js/js/dc.js"></script>
    <script src="https://rawgit.com/crossfilter/reductio/master/reductio.js"></script>
    <script src="https://npmcdn.com/universe@latest/universe.js"></script>



    <style type="text/css">
        .dc-legend
        {
            transform: translate(0px,200px);
        }
        div.dc-chart
        {
            float: none!important;
        }
        .amcharts-chart-div a
        {
            display: none!important;
        }

        #divCust
        {
            width: 100%;
            height: 300px;
            margin-left: 10px;
        }

        #divCP
        {
            width: 100%;
            height: 300px;
            margin-left: 10px;
        }

        #divTot
        {
            width: 100%;
            height: 300px;
            margin-left: 10px;
        }

        .gauge-container
        {
            text-align: center;
            /*margin: 0px auto;*/ margin: 2px 5px 0px 4px;
        }

        .gauge
        {
            width: 330px;
            height: 330px;
            margin: 0 auto 0;
        }

        td
        {
            height: 30px !important;
            padding-left: 10px;
            text-align: left;
        }

        .noclose .ui-dialog-titlebar-close
        {
            display: none;
        }

        .row
        {
            margin-left: 10px;
            margin-right: -15px;
        }

        .noTitleStuff .ui-dialog-titlebar
        {
            display: none;
        }

        .panelBack
        {
            background-color: #fff !Important;
        }

        #dropid label, #Div1 label, #Div2 label, #Div3 label, #Div4 label
        {
            top: -2px;
            position: relative;
            margin-left: 5px;
        }

        #chart_cust
        {
            height: 275px;
        }

        #se_charts
        {
            display: none;
            border: 1px solid #e1e1e1;
            padding: 10px;
            background: #fff;
            margin-bottom: 20px;
        }

        .panel
        {
            margin-bottom: 0px !important;
        }

        #MainContent_bm_chart
        {
            display: none;
            border: 1px solid #e1e1e1;
            padding: 10px;
            background: #fff;
            /*margin-bottom:20px;*/
        }

        #MainContent_ho_chart
        {
            display: none;
            border: 1px solid #e1e1e1;
            padding: 10px;
            background: #fff;
            margin-bottom: 20px;
        }

        #MainContent_divcnsldt
        {
            display: none;
        }

        #bargraphdiv, #bmbargraphdiv
        {
            border: 1px solid #e1e1e1;
            padding: 10px;
            background: #fff;
            margin-bottom: 20px;
        }

        .panel-group .panel
        {
            border-radius: 4px;
            margin-bottom: 0;
            overflow: hidden;
            margin-top: 20px;
        }

        .hyprlnk, .hpprlnk:hover
        {
            color: #0066FF;
            text-decoration: underline;
            float: right;
            font-weight: 600;
            font-size: 13px;
            margin-top: -21px;
            margin-right: 15px;
            cursor: pointer;
        }

        .gauge-label
        {
            color: #232BBD;
            float: left;
            font-weight: 600;
            font-size: 13px;
            margin-top: -21px;
            margin-left: 8px;
        }

        .bdgt-label
        {
            color: #232BBD;
            float: none;
            font-weight: 600;
            font-size: 13px;
        }

        .lnk_btn, .lnk_btn:hover
        {
            color: #232BBD;
            float: none;
            font-weight: 600;
            font-size: 13px;
            cursor: pointer;
        }

        .stsval
        {
            color: #0066FF;
            float: left;
            font-weight: 600;
            font-size: 13px;
            margin-top: -21px;
            margin-left: 80px;
        }

        #MainContent_grdviewAllValues td:last-child
        {
            text-align: right !important;
        }

        #MainContent_grdviewAllValues td
        {
            text-align: left;
            color: #000;
        }

        .panel-heading1
        {
            background: #333;
            color: #29AAe1 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading2
        {
            background: #999;
            color: #333 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading3
        {
            background: #333;
            color: #8ac340 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading4
        {
            background: #999;
            color: #91298E !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading5
        {
            background: #333;
            color: #F05A26 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading6
        {
            background: #999;
            color: #002238 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading7
        {
            background: #333;
            color: #fff !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }
    </style>
    <script type="text/javascript">
        //$(window).onload(function () {

        //    gridviewScrollTrigger();
        //});
        $(window).resize(function () {

            gridviewScrollTrigger();
        });
        function bindpiecharts(tmp1, chart_width, id) {
 
            if (tmp1.length > 0) {
                var tmp = jQuery.parseJSON(tmp1);
                console.log("familyview1 : " + tmp);
                console.log("len1 : " + tmp.length);

                chart = dc.pieChart("#" + id);
                var ndx = crossfilter(tmp),
                runDimension = ndx.dimension(function (d) { return d.Status + "(" + d.c_count + ")"; })
                speedSumGroup = runDimension.group().reduceSum(function (d) { return +(d.value); });

                chart
                    .width(400)
                    .height(300)
                    .slicesCap(10)
                    //.innerRadius(50)
                    //.externalLabels(25)
                    //.externalRadiusPadding(25)
                    .drawPaths(true)
                    .dimension(runDimension)
                    .group(speedSumGroup)
                    .legend(dc.legend());

                chart.on('pretransition', function (chart) {
                    chart.selectAll('.dc-legend-item text')
                        .text('')
                      .append('tspan')
                        .text(function (d) { return d.name; })
                      .append('tspan')
                        .attr('x', 225)
                        .attr('text-anchor', 'end')
                        .text(function (d) { return " : " + d.data; });
                });
                chart.render();
            }
        }

        function LoadCustPieCharts() {

            //am4core.useTheme(am4themes_animated);
            //// Themes end

            //var chart = am4core.create("divCust", am4charts.PieChart3D);
            //chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

            //chart.legend = new am4charts.Legend();

            //chart.data = [{ "Status": "Approved", "c_count": "0", "value": "" }, { "Status": "Under Review", "c_count": "1", "value": "4794460.00000" }, { "Status": "Waiting For HO Approval", "c_count": "6", "value": "21664875.00000" }, { "Status": "Drafted BY BM", "c_count": "8", "value": "12892172.00000" }, { "Status": "Draft", "c_count": "87", "value": "391227711.00000" }, { "Status": "Wating For BM Approval", "c_count": "103", "value": "581933110.00000" }, { "Status": "NOT Initiated", "c_count": "515", "value": "0.00000" }, { "Status": "TOTAL", "c_count": "720", "value": "1012512328.00000" }];

            //var series = chart.series.push(new am4charts.PieSeries3D());
            //series.dataFields.value = "value";
            //series.dataFields.category = "Status";

        
            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'Dashboard.aspx/LoadChartByCustomer',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    bindpiecharts(tmp, 700, "divCust");
                    ////am4core.useTheme(am4themes_material);
                    //am4core.useTheme(am4themes_animated);
                    //// Themes end

                    //var chart = am4core.create("divCust", am4charts.PieChart);
                    //chart.legend = new am4charts.Legend();
                    //chart.data = JSON.parse(tmp);
                    //var pieSeries = chart.series.push(new am4charts.PieSeries());
                    //pieSeries.dataFields.value = "c_count";
                    //pieSeries.dataFields.category = "Status";
                    //pieSeries.dataFields.labelText = "value";
                    //pieSeries.legendSettings.labelText = '{category}';
                    //pieSeries.legendSettings.valueText = "{labelText}";
                    //pieSeries.slices.template.stroke = am4core.color("#fff");
                    //pieSeries.slices.template.strokeWidth = 2;
                    //pieSeries.slices.template.strokeOpacity = 1;

                    //// This creates initial animation
                    //pieSeries.hiddenState.properties.opacity = 1;
                    //pieSeries.hiddenState.properties.endAngle = -90;
                    //pieSeries.hiddenState.properties.startAngle = -90;

                    ////var chart = am4core.create("divCust", am4charts.PieChart3D);
                    ////chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

                    ////chart.legend = new am4charts.Legend();

                    ////chart.data = JSON.parse(tmp);

                    ////var series = chart.series.push(new am4charts.PieSeries3D());
                    ////series.dataFields.value = "c_count";
                    ////series.dataFields.category = "Status";
                    ////series.dataFields.labelText = "value";
                    ////series.legendSettings.labelText = '{category}';
                    ////series.legendSettings.valueText = "{labelText}";
                },
                error: function (e) {
                    //console(e);
                }
            });

            //var tmp = null;
            //$.ajax({
            //    type: "POST",
            //    url: 'Dashboard.aspx/LoadChartByCustomer',
            //    data: "",
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    success: function (msg) {
            //        console.log("msg : " + msg);
            //        tmp = msg.d;
            //        console.log("tmp1 : " + tmp);
            //        //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
            //        var chart = AmCharts.makeChart("divCust", {
            //            "type": "pie",
            //            "theme": "light",
            //            //"titles": [{
            //            //    "text": "Customer"
            //            //}],
            //            "dataProvider": tmp,
            //            "valueField": "value",
            //            "titleField": "Status",
            //            "labelText": "[[title]]",
            //            "balloon": {
            //                "fixedPosition": true
            //            },
            //            "legends": {
            //                "position": "absolute",
            //                //"top": "30px",
            //                //"left": 600,
            //                "useGraphSettings": true
            //            }

            //        });
            //        chart.dataProvider = AmCharts.parseJSON(tmp);
            //        chart.validateData();
            //    },
            //    error: function (e) {
            //        //console(e);
            //    }
            //});
        }
        function LoadCPPieCharts() {

            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'Dashboard.aspx/LoadChartByCP',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    bindpiecharts(tmp, 700, "divCP");
                    //am4core.useTheme(am4themes_animated);
                    //// Themes end

                    //var chart = am4core.create("divCP", am4charts.PieChart3D);
                    //chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

                    //chart.legend = new am4charts.Legend();

                    //chart.data = JSON.parse(tmp);

                    //var series = chart.series.push(new am4charts.PieSeries3D());
                    //series.dataFields.value = "c_count";
                    //series.dataFields.category = "Status";
                    //series.dataFields.labelText = "value";
                    //series.legendSettings.labelText = '{category}';
                    //series.legendSettings.valueText = "{labelText}";
                },
                error: function (e) {
                    //console(e);
                }
            });

        }
        function LoadPieCharts() {
            
            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'Dashboard.aspx/LoadChartTotal',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    bindpiecharts(tmp, 1000, "divTot");
                    //am4core.useTheme(am4themes_animated);
                    //// Themes end

                    //var chart = am4core.create("divTot", am4charts.PieChart3D);
                    //chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

                    //chart.legend = new am4charts.Legend();

                    //chart.data = JSON.parse(tmp);

                    //var series = chart.series.push(new am4charts.PieSeries3D());
                    //series.dataFields.value = "c_count";
                    //series.dataFields.category = "Status";
                    //series.dataFields.labelText = "value";
                    //series.legendSettings.labelText = '{category}';
                    //series.legendSettings.valueText = "{labelText}";
                },
                error: function (e) {
                    //console(e);
                }
            });

        }
        $(document).ready(function () {
            
            bindPopup();
            LoadCustPieCharts();
            LoadCPPieCharts();
            LoadPieCharts();
            var se_cnt = $('#MainContent_hdn_se_statuscnt').val();
            var bm_cnt = $('#MainContent_hdn_bm_statuscnt').val();
            var ho_cnt = $('#MainContent_hdn_ho_statuscnt').val();
            var total_cnt = $('#MainContent_hdn_total_cnt').val() / 3;
            var se_total_cnt = $('#MainContent_hdn_se_cust_cnt').val() / 3;
            var range1 = total_cnt;
            var range2 = total_cnt + total_cnt;
            var range3 = total_cnt + total_cnt + total_cnt;

            //for se gauge
            var srange1 = se_total_cnt;
            var srange2 = se_total_cnt + se_total_cnt;
            var srange3 = se_total_cnt + se_total_cnt + se_total_cnt;
            createGauge("", se_cnt, srange1, srange2, srange3);
            createGauge1("", bm_cnt, range1, range2, range3);
            createGauge2("", ho_cnt, range1, range2, range3);


            $(document).bind("kendo:skinChange", function (e) {
                createGauge();
                createGauge1();
                createGauge2();
            });
            $('#gridtitle').click(function () {

                var attr = $('#MainContent_cnsldt_img').attr('src');
                //var imgsrc=images/button_plus.gif;
                $("#MainContent_divcnsldt").slideToggle();
                if (attr == "images/button_minus.gif") {

                    $("#MainContent_cnsldt_img").attr("src", "images/button_plus.gif");
                } else {
                    $(window).resize();
                    $("#MainContent_cnsldt_img").attr("src", "images/button_minus.gif");
                }

                jQuery(".ui-dialog.ui-widget").each(function () {

                    if (jQuery(this).css("display") == "block") {

                        jQuery(this).css("display", "none")
                    }
                });

            });

          
            function createGauge(labelPosition, se_cnt, range1, range2, range3) {

                $("#gauge").kendoRadialGauge({
                    // renderAs: "canvas",
                    pointer: [{
                        color: "#736F6E",
                        value: se_cnt
                    }],

                    scale: {
                        //majorUnit: 2,
                        //minorUnit: .25,
                        // minorUnit: 25,
                        startAngle: 0,
                        endAngle: 180,
                        //minorUnit: 5,

                        max: range3,
                        labels: {
                            position: "outside",

                        },

                        //majorTicks: {
                        //    size: 10
                        //},
                        ranges: [
                             {
                                 from: 0,
                                 to: range1,
                                 color: "#830300"
                             }, {
                                 from: range1,
                                 to: range2,
                                 color: "yellow"
                             }, {
                                 from: range2,
                                 to: range3,
                                 color: "#006400"
                             }
                        ],
                        rangeSize: 40,
                        rangeDistance: -10,
                    }
                });
            }
            function createGauge1(labelPosition, bm_cnt, range1, range2, range3) {
                $("#gauge1").kendoRadialGauge({
                 
                    pointer: [{
                        color: "#736F6E",
                        value: bm_cnt
                    }],

                    scale: {
                        //minorUnit: 5,
                        //startAngle: -30,
                        //endAngle: 210,
                        startAngle: 0,
                        endAngle: 180,


                        max: range3,
                        labels: {
                            position: "outside",

                        },
                        //majorTicks: {
                        //    size: 10
                        //},
                        ranges: [
                              {
                                  from: 0,
                                  to: range1,
                                  color: "#830300"
                              }, {
                                  from: range1,
                                  to: range2,
                                  color: "yellow"
                              }, {
                                  from: range2,
                                  to: range3,
                                  color: "#006400"
                              }
                        ],
                        rangeSize: 40,
                        rangeDistance: -10,
                    }
                });
            }
            function createGauge2(labelPosition, ho_cnt, range1, range2, range3) {
                $("#gauge2").kendoRadialGauge({

                    pointer: [{
                        color: "#736F6E",
                        value: ho_cnt
                    }],

                    scale: {
                        //minorUnit: 5,
                        //startAngle: -30,
                        //endAngle: 210,
                        startAngle: 0,
                        endAngle: 180,


                        max: range3,
                        labels: {
                            position: "outside",

                        },
                        //majorTicks: {
                        //    size: 10
                        //},
                        ranges: [
                              {
                                  from: 0,
                                  to: range1,
                                  color: "#830300"
                              }, {
                                  from: range1,
                                  to: range2,
                                  color: "yellow"
                              }, {
                                  from: range2,
                                  to: range3,
                                  color: "#006400"
                              }
                        ],
                        rangeSize: 40,
                        rangeDistance: -10,
                    }
                });
            }


            $('#MainContent_lnkbtnBulkapproval').bind("click", function (e) {
                dclg = $("#DivBranchApprove").dialog(
                            {
                                resizable: false,
                                draggable: true,
                                modal: false,
                                title: "BRANCH WISE APPROVAL",
                                width: "350",
                                height: "180",
                                open: function () {
                                    $(".ui-widget-overlay").css("display", 'none');
                                },
                                close: function () {
                                    $("#overlay").removeClass("ui-widget-overlay");
                                }
                            });
                //divPopUpBranchApproval = dclg;
                dclg.parent().appendTo(jQuery("form:first"));
                e.preventDefault();
            });


            $("#Div4 input[type='submit']").attr("disabled", true);
            $("#Div3 input[type='submit']").attr("disabled", true);
            $("#Div2 input[type='submit']").attr("disabled", true);
            $("#Div1 input[type='submit']").attr("disabled", true);
            $("#dropid input[type='submit']").attr("disabled", true);

            $("#Div4 INPUT[type='checkbox']").change(function () {

                var checkboxes = $("#Div4 input[type='checkbox']")
                var submitButt = $("#Div4 input[type='submit']");
                if ($(this).next('label').text() == "ALL") {
                    $("#Div4 INPUT[type='checkbox']").not(this).prop('checked', this.checked);
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
                }
                else
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
            });
            $(".chkho").click(function () {

                $("#MainContent_CheckBox4").prop('checked', ($(".chkho input[type='checkbox']:checked").length == $(".chkho input[type='checkbox']").length) ? true : false);
            });
            $("#Div3 INPUT[type='checkbox']").change(function () {

                var checkboxes = $("#Div3 input[type='checkbox']")
                var submitButt = $("#Div3 input[type='submit']");
                if ($(this).next('label').text() == "ALL") {
                    $("#Div3 INPUT[type='checkbox']").not(this).prop('checked', this.checked);
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
                }
                else
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
            });
            $(".chkbm").click(function () {

                $("#MainContent_CheckBox3").prop('checked', ($(".chkbm input[type='checkbox']:checked").length == $(".chkbm input[type='checkbox']").length) ? true : false);
            });
            $("#Div2 INPUT[type='checkbox']").change(function () {

                var checkboxes = $("#Div2 input[type='checkbox']")
                var submitButt = $("#Div2 input[type='submit']");
                if ($(this).next('label').text() == "ALL") {
                    $("#Div2 INPUT[type='checkbox']").not(this).prop('checked', this.checked);
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
                }
                else
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
            });
            $(".chkse").click(function () {

                $("#MainContent_CheckBox2").prop('checked', ($(".chkse input[type='checkbox']:checked").length == $(".chkse input[type='checkbox']").length) ? true : false);
            });
            $("#dropid INPUT[type='checkbox']").change(function () {

                var checkboxes = $(" #dropid input[type='checkbox']")
                var submitButt = $("#dropid input[type='submit']");
                console.log($(checkboxes[0]).next("label").text());
                if ($(this).next('label').text() == "ALL") {
                    $("#dropid INPUT[type='checkbox']").not(this).prop('checked', this.checked);
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
                }
                else
                    submitButt.attr("disabled", !checkboxes.is(":checked"));

            });
            $(".chkCT").click(function () {

                $("#MainContent_CheckBox1").prop('checked', ($(".chkCT input[type='checkbox']:checked").length == $(".chkCT input[type='checkbox']").length) ? true : false);
            });
            $("#Div1 INPUT[type='checkbox']").change(function () {

                var checkboxes = $(" #Div1 input[type='checkbox']")
                var submitButt = $("#Div1 input[type='submit']");
                if ($(this).next('label').text() == "ALL") {
                    $("#Div1 INPUT[type='checkbox']").not(this).prop('checked', this.checked);
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
                }
                else
                    submitButt.attr("disabled", !checkboxes.is(":checked"));
            });

            $("#MainContent_cbAllRegion").click(function () {
                
                disableChk();
                if ($('#MainContent_cbAllRegion').is(':checked')) {
                    $('input:checkbox[name$=cblRegion]').each(
                        function () {
                            $(this).attr('checked', 'checked');
                        });
                }
                else {
                    $('input:checkbox[name$=cblRegion]').each(
                        function () {
                            $(this).removeAttr('checked');
                        });
                }
                //$("#MainContent_cbAllRegion").prop('checked', ($("#MainContent_cblRegion input[type='checkbox']:checked").length == $("#MainContent_cblRegion input[type='checkbox']").length) ? true : false);
            });
            $('input:checkbox[name$=cblRegion]').click(function () {
                
                // if any of the checkbox is unchecked
                // check all checkbox should be cleared
                if (!$(this).is(':checked')) {
                    $('#MainContent_cbAllRegion').removeAttr('checked');
                }
                else {
                    // if all of the checkbox is checked
                    // check all checkbox should be checked
                    var count = $('input:checkbox[name$=cblRegion]').length
                    if (count == ($('input:checkbox[name$=cblRegion]:checked').length + 1)) {
                        $('#MainContent_cbAllRegion').attr('checked', 'checked');
                    }
                }
            });

            $('#MainContent_lnkbtnBulkapproval').bind("click", function (e) {
                dclg = $("#DivBranchApprove").dialog(
                            {
                                resizable: false,
                                draggable: true,
                                modal: false,
                                title: "BRANCH WISE APPROVAL",
                                width: "350",
                                height: "180",
                                open: function () {
                                    $(".ui-widget-overlay").css("display", 'none');
                                },
                                close: function () {
                                    $("#overlay").removeClass("ui-widget-overlay");
                                }
                            });
                //divPopUpBranchApproval = dclg;
                dclg.parent().appendTo(jQuery("form:first"));
                e.preventDefault();
            });
            
        });

        function disableChk() {
            if ($("#MainContent_hdnRole").val() == "BM" || $("#MainContent_hdnRole").val() == "SE") {
                $("#MainContent_cbAllRegion").removeAttr('checked');
            }
        }
        $(function () {
            $("[id*=lnkbtn_Customer_name]").bind("click", function () {
                var td = $("td", $(this).closest("tr"));
                var CustomerNum = $("[id*=hdn_customernumber]", td).val();
                $("#MainContent_hdn_CustomerNum").val(CustomerNum);
            });

        });

        var gridView1;
        function gridviewScrollTrigger() {
            
            if (gridView1 != undefined)
                return;
            gridView1 = $('#MainContent_grdviewAllValues').gridviewScroll({
                width: $(window).width() - 60,
                height: 500,
                railcolor: "#F0F0F0",
                barcolor: "#606060",
                barhovercolor: "#606060",
                bgcolor: "#F0F0F0",
                freezesize: 0,
                arrowsize: 30,
                varrowtopimg: "Images/arrowvt.png",
                varrowbottomimg: "Images/arrowvb.png",
                harrowleftimg: "Images/arrowhl.png",
                harrowrightimg: "Images/arrowhr.png",
                headerrowcount: 1,
                railsize: 16,
                barsize: 14,
                verticalbar: "auto",
                horizontalbar: "auto",
                wheelstep: 1,
          });
            jQuery(".ui-dialog.ui-widget").each(function () {
                if (jQuery(this).css("display") == "block")
                    jQuery(this).css("top", $('#MainContent_grdviewAllValues').offset().top)
            });
         bindPopup();

        }
        function modalht() {

            jQuery(".ui-dialog.ui-widget").each(function () {

                if (jQuery(this).css("display") == "block") {

                    jQuery(this).css("top", $('#MainContent_grdviewAllValues').offset().top)
                }
            });
        }

        var closedialog_panelHOStatus;
        var bindPopupBinded = false;
        function bindPopup() {
            if (bindPopupBinded) return;
            /*  Display or close pop up HO Status Start*/
  
            var dlg_panelHOStatus = $('#MainContent_panelHOStatus').dialog({

                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 160,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnHOStatus_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');

                    closedialog_panelHOStatus = 1;
                },
                focus: function () {
                    closedialog_panelHOStatus = 0;
                },
                close: function () {
                    $(this).dialog('close');
                    closedialog_panelHOStatus = 0;

                }
            });
            dlg_panelHOStatus.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnHOStatus_0').click(function () {
               


                if (closedialog_panelHOStatus == 1) {
                  
                    $('#MainContent_panelHOStatus').dialog('close');
                } else {
                    $('#MainContent_panelHOStatus').dialog('open');
                    closedialog_panelHOStatus = 1;
                }
                //gridviewScrollTrigger();
            });
            /*   Display or close pop up HO Status END*/


            /*  Display or close pop up BM Status Start*/
            var dlg_panelBMStatus = $('#MainContent_panelBMStatus').dialog({
             
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 200,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnBMStatus_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                    closedialog_panelBMStatus = 1;

                },
                focus: function () {
                    closedialog_panelBMStatus = 0;
                },
                close: function () {

                    $(this).dialog('close');
                    closedialog_panelBMStatus = 0;
                }
            });
            dlg_panelBMStatus.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnBMStatus_0').click(function () {
                if (closedialog_panelBMStatus == 1) {
                    $('#MainContent_panelBMStatus').dialog('close');
                } else {
                    $('#MainContent_panelBMStatus').dialog('open');
                    closedialog_panelBMStatus = 1;
                }
            });
            var closedialog_panelBMStatus;

            /*   Display or close pop up BM Status END*/


            /*  Display or close pop up SE Status Start*/
            var dlg_panelSEStatus = $('#MainContent_panelSEStatus').dialog({
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 200,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnSEStatus_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                    closedialog_panelSEStatus = 1;

                },
                focus: function () {
                    closedialog_panelSEStatus = 0;
                },
                close: function () {
                    $(this).dialog('close');
                    closedialog_panelSEStatus = 0;

                }
            });
            dlg_panelSEStatus.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnSEStatus_0').click(function () {

                if (closedialog_panelSEStatus == 1) {
                    $('#MainContent_panelSEStatus').dialog('close');
                } else {
                    $('#MainContent_panelSEStatus').dialog('open');
                    closedialog_panelSEStatus = 1;
                }
            });
            var closedialog_panelSEStatus;

            /*   Display or close pop up SE Status END*/

            /*  Display or close pop up customer REGION Start*/
            var dlg_panelRegion = $('#MainContent_panelRegion').dialog({
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 250,
                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnRegion_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                    closedialog_panelRegion = 1;

                },
                focus: function () {
                    closedialog_panelRegion = 0;
                },
                close: function () {
                    $(this).dialog('close');
                    closedialog_panelRegion = 0;

                }
            });
            dlg_panelRegion.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnRegion_0').click(function () {

                if (closedialog_panelRegion == 1) {
                    $('#MainContent_panelRegion').dialog('close');
                } else {
                    $('#MainContent_panelRegion').dialog('open');
                    closedialog_panelRegion = 1;
                }
            });
            var closedialog_panelRegion;

            /*  Display or close pop up customer REGION END*/

            /*   Display or close pop up customer type Start*/
            var dlg_panelCustomerType = $('#MainContent_panelCustomerType').dialog({
                autoOpen: false,
                dialogClass: 'noTitleStuff',
                minHeight: 100,
                width: 190,

                position: { my: "left top", at: "left bottom", of: $('#MainContent_grdviewAllValues_ibtnCtype_0').closest("td") },
                draggable: true,
                resizable: false,
                modal: false,
                closeText: 'Close',
                open: function () {
                    $(this).dialog('open');
                    closedialog = 1;
                    // $(document).bind('click', overlayclickclose);
                },
                focus: function () {
                    closedialog = 0;
                },
                close: function () {
                    $(this).dialog('close');
                    closedialog = 0;
                    //$(document).unbind('click');
                }
            });
            dlg_panelCustomerType.parent().appendTo(jQuery("form:first"));

            $('#MainContent_grdviewAllValues_ibtnCtype_0').click(function () {

                if (closedialog == 1) {
                    $('#MainContent_panelCustomerType').dialog('close');
                } else {
                    $('#MainContent_panelCustomerType').dialog('open');
                    closedialog = 1;
                }
            });
            var closedialog;

            /*   Display or close pop up customer type END*/

        }
        // Themes begin
       
        function collapse(container, img_id) {

            container = "#" + container;
            img_id = "#" + img_id;
            var attr = $(img_id).attr('src');
            $(container).slideToggle();
            if (attr == "images/button_minus.gif") {
                $(img_id).attr("src", "images/button_plus.gif");
                //gridviewScrollTrigger();
            } else {
                // gridviewScrollTrigger();
                $(img_id).attr("src", "images/button_minus.gif");
            }
            setTimeout(function () {
                jQuery(".ui-dialog.ui-widget.ui-widget-content").each(function () {
                    var ht = 0;
                    if (jQuery(this).css("display") == "block") {
                        $('#MainContent_panelHOStatus').dialog('close');
                        $('#MainContent_panelBMStatus').dialog('close');
                        $('#MainContent_panelCustomerType').dialog('close');
                        $('#MainContent_panelRegion').dialog('close');
                        $('#MainContent_panelSEStatus').dialog('close');

                    }
                });
            }, 10);

            
        }
        function test() {
            debugger;
            document.getElementById('MainContent_cbCTCustomer').checked = false;
            document.getElementById('MainContent_cbCTDistributor').checked = false;
            document.getElementById('MainContent_CheckBox1').checked = false;
            document.getElementById('MainContent_CheckBox2').checked = false;
            document.getElementById('MainContent_cb_se_NI').checked = false;
            document.getElementById('MainContent_cb_se_PS').checked = false;
            document.getElementById('MainContent_cb_se_Sub').checked = false;
            document.getElementById('MainContent_cb_se_NI').checked = false;
            document.getElementById('MainContent_CheckBox3').checked = false;
            document.getElementById('MainContent_cb_bm_NI').checked = false;
            document.getElementById('MainContent_cb_bm_PS').checked = false;
            document.getElementById('MainContent_cb_bm_Sub').checked = false;
            document.getElementById('MainContent_cb_bm_tr').checked = false;
            document.getElementById('MainContent_cb_bm_sr').checked = false;
            document.getElementById('MainContent_cb_bm_app').checked = false;
            document.getElementById('MainContent_CheckBox4').checked = false;
            document.getElementById('MainContent_cb_ho_tr').checked = false;
            document.getElementById('MainContent_cb_ho_app').checked = false;

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
    <div class="crumbs">
        <!-- Start : Breadcrumbs -->
        <ul id="breadcrumbs" class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a>Budget</a>
            </li>
            <li>Dashboard</li>
            <div>
                <ul>
                    <li class="title_bedcrum" style="list-style: none;">DASHBOARD</li>
                </ul>
            </div>
            <div>
                <ul style="float: right; list-style: none; margin-top: -4px; width: 247px; margin-right: -5px;" class="alert alert-danger fade in">
                    <li>
                        <span style="margin-right: 4px; vertical-align: text-bottom;">Val In '000</span>
                        <asp:RadioButton ID="rbtn_Thousand" OnCheckedChanged="rbtn_Thousand_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" Checked="True" />
                        <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">Val In Lakhs</span>
                        <asp:RadioButton ID="rbtn_Lakhs" OnCheckedChanged="rbtn_Lakhs_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" />
                    </li>
                </ul>
            </div>
        </ul>
    </div>
    <!-- End : Breadcrumbs -->
    <div class="row" style="margin-top: 5px;">

        <ul runat="server" id="cterDiv" visible="true" class="btn-info rbtn_panel" style="margin-left: 5px">
            <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>

                <asp:RadioButton ID="rdBtnTaegutec" onclick="test();" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                <asp:RadioButton ID="rdBtnDuraCab"  onclick="test();" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
            </li>
        </ul>

        <%--        <div style="float:right;padding-right: 10px; padding-left: 10px; width: 100%; margin-top:4px;height:20px;">--%>
        <asp:LinkButton runat="server" ID="lnkbtnBulkapproval" Text="BRANCH WISE APPROVAL" ForeColor="#0066FF"
            Font-Underline="True" Style="float: right; font-weight: 600; font-size: 13px; padding-right: 10px; padding-left: 10px; height: 20px; margin-top: -22px" Visible="true"></asp:LinkButton>
    </div>

    <div class="row" style="margin-top: 4px;">
        <!-- Inner Page Row 2 Starts Here -->

        <div class="col-md-4 col-sm-6">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">SALES ENGINEER</div>
                </div>
                <div class="portlet-body">

                    <div id="example" class="k-content">
                        <div id="gauge-container2" class="gauge-container" style="height: 220px">
                            <div id="gauge" class="gauge" style="width: 300px; height: 200px;"></div>
                        </div>
                        <asp:Label ID="lblsestatus" CssClass="gauge-label" runat="server"></asp:Label>
                        <%-- <asp:HyperLink ID="HyperLink_se" CssClass="hyprlnk" style="color: #0066FF;"  runat="server">View More..</asp:HyperLink>    --%>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-6">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">BRANCH MA3NAGER</div>
                </div>
                <div class="portlet-body col-md-12">
                    <div id="Div5" class="k-content">

                        <div id="gauge-container" class="gauge-container" style="height: 220px">
                            <div id="gauge1" class="gauge" style="width: 300px; height: 200px;"></div>
                        </div>
                        <asp:Label ID="lblbmstatus" CssClass="gauge-label" runat="server"></asp:Label>
                        <%--            <asp:HyperLink ID="HyperLink_bm" CssClass="hyprlnk" style="color: #0066FF;" runat="server">View More..</asp:HyperLink>--%>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-6">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption"><span style="text-align: center">HO</span></div>
                </div>
                <div class="portlet-body col-md-12">
                    <div id="Div6" class="k-content">

                        <div id="gauge-container1" class="gauge-container" style="height: 220px">
                            <div id="gauge2" class="gauge" style="width: 300px; height: 200px;"></div>
                        </div>
                        <asp:Label ID="lblhostatus" CssClass="gauge-label" runat="server"></asp:Label>
                        <%--             <asp:HyperLink ID="HyperLink_ho" style="color: #0066FF;" CssClass="hyprlnk" runat="server">View More..</asp:HyperLink>--%>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <div class="row" style="margin-top: 4px;">
        <!-- Inner Page Row 2 Starts Here -->

        <div class="col-md-4">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">CUSTOMER</div>
                </div>
                <div class="portlet-body">
                    <div id="divCust"></div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">CHANNEL PARTNER</div>
                </div>
                <div class="portlet-body col-md-12">
                    <div id="divCP"></div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">TOTAL</div>
                </div>
                <div class="portlet-body col-md-12">
                    <div id="divTot"></div>
                </div>

            </div>

        </div>

    </div>

    <%--  <div class="row panel panel-default ">
        <div id="se_collapsebtn" class=" panel-heading1 " onclick="collapse('se_charts','se_charts_image')">
        <h4 class="panel-title">
            <img id="se_charts_image" src="images/button_plus.gif" align="left">  &nbsp;&nbsp;&nbsp;&nbsp; SALES ENGINEER STATUS-COUNT
        </h4>
        </div>
        </div>
        
   <div class="row" id="se_charts" >
        
       <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CUSTOMERS</div>
         </div>
         <div class="portlet-body">
            <div id="Div7" class="k-content">
               <div id="Div8" class="gauge-container" style="width:200px;" >
                  <asp:Chart ID="chart_se_cust" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true" ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>    
                  </asp:Chart>
                     <asp:Label  ID="lbl_sec_bdgt_val" CssClass="hyprlnk" style="float:none;text-decoration:none;cursor:auto;" runat="server"  ></asp:Label>
                  
               </div>
                
            </div>
            
         </div>
      </div>
   </div>
      <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CHANNEL PARTNERS </div>
         </div>
         <div class="portlet-body">
            <div id="Div9" class="k-content">
               <div id="Div10" class="gauge-container" style="width:200px;" >
                  <asp:Chart ID="chart_se_dstrbtr" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true"  ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:Label ID="lbl_sed_bdgt_val" CssClass="hyprlnk" style="float:none;text-decoration:none;cursor:auto;" runat="server" ></asp:Label>   

               </div>
            </div>
         </div>
      </div>
   </div>
           <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">TOTALS</div>
         </div>
         <div class="portlet-body">
            <div id="Div11" class="k-content">
               <div id="Div12" class="gauge-container" style="width:200px;"  >
                  <asp:Chart ID="chart_se_total" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true" ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:Label ID="lbl_set_bdgt_val" CssClass="hyprlnk" style="float:none;text-decoration:none;cursor:auto;" runat="server" ></asp:Label>   
               </div>
            </div>
         </div>
      </div>
   </div>
            </div>
         <div style="margin-top:20px;" class="row panel panel-default ">
        <div id="bm_collapsebtn" class=" panel-heading2 " onclick="collapse('MainContent_bm_chart','MainContent_bm_charts_image')">
        <h4 class="panel-title">
            <img id="bm_charts_image" runat="server" src="images/button_plus.gif" align="left">  &nbsp;&nbsp;&nbsp;&nbsp; BRANCH MANAGER STATUS-COUNT
        </h4>
        </div>
        </div>
          <div class="row" id="bm_chart" runat="server">
   <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CUSTOMERS</div>
         </div>
         <div class="portlet-body">
            <div id="Div14" class="k-content">
               <div id="Div15" class="gauge-container" style="width:200px;">
                  <asp:Chart ID="chart_bm_cust" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true" ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:LinkButton ID="lbl_bmc_bdgt_val"  CssClass="hyprlnk" style="color: #0066FF;float:none;margin-right:0px;"  runat="server" OnClick="bm_bdgt_val_Click" ></asp:LinkButton>   

               </div>
            </div>
         </div>
      </div>
   </div>
      <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CHANNEL PARTNERS </div>
         </div>
         <div class="portlet-body">
            <div id="Div16" class="k-content">
               <div id="Div17" class="gauge-container" style="width:200px;" >
                  <asp:Chart ID="chart_bm_dstrbtr" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true"  ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:LinkButton ID="lbl_bmd_bdgt_val" CssClass="hyprlnk" style="color: #0066FF;float:none;margin-right:0px;" runat="server"  OnClick="bm_bdgt_val_Click" ></asp:LinkButton>   

               </div>
            </div>
         </div>
      </div>
   </div>
           <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">TOTALS</div>
         </div>
         <div class="portlet-body">
            <div id="Div18" class="k-content">
               <div id="Div19" class="gauge-container" style="width:200px;">
                  <asp:Chart ID="chart_bm_total" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true" ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:LinkButton ID="lbl_bmt_bdgt_val" CssClass="hyprlnk" style="color: #0066FF;float:none;margin-right:0px;" runat="server"  OnClick="bm_bdgt_val_Click" ></asp:LinkButton>   

               </div>
            </div>
         </div>
      </div>
   </div>
</div>
        <div id="divbmbargraph" runat="server" >
        <div style="margin-top:20px;" class="row panel panel-default ">
        <div id="Div32" class=" panel-heading2 "  onclick="collapse('bmbargraphdiv','MainContent_bmbar_graphs_image')">
        <h4 class="panel-title">
            <img id="bmbar_graphs_image" runat="server" src="images/button_plus.gif" align="left">  &nbsp;&nbsp;&nbsp;&nbsp;BRANCH MANAGER STATUS-VALUE
        </h4>
        </div>
        </div>
   <div class="row" id="bmbargraphdiv">
       <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CUSTOMERS</div>
         </div>
         <div class="portlet-body">
            <div id="Div33" class="k-content" >
               <div id="Div34" >
                  <asp:Chart ID="cbmbdgt_bargraph" EnableViewState="true" runat="server">

                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Column"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="SkyBlue"><AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
              <MajorGrid LineWidth="0"  /><LabelStyle Font="Verdana, 8.25pt" />
         </AxisX><AxisY>
              <MajorGrid LineWidth="0" />
         </AxisY></asp:ChartArea>
                     </ChartAreas>
                   
                  </asp:Chart>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
         <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CHANNEL PARTNERS </div>
         </div>
         <div class="portlet-body">
            <div id="Div35" class="k-content" >
               <div id="Div36">
                  <asp:Chart ID="dbmbdgt_bargraph" EnableViewState="true" runat="server">

                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Column"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="SkyBlue"><AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
              <MajorGrid LineWidth="0" /><LabelStyle Font="Verdana, 8.25pt" />
         </AxisX><AxisY>
              <MajorGrid LineWidth="0" />
         </AxisY></asp:ChartArea>
                     </ChartAreas>
                  </asp:Chart>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
      <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">TOTALS</div>
         </div>
         <div class="portlet-body">
            <div id="Div37" class="k-content" >
               <div id="Div38">
                  <asp:Chart ID="tbmbdgt_bargraph" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Column"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="SkyBlue"><AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
              <MajorGrid LineWidth="0" /><LabelStyle Font="Verdana, 8.25pt" />
         </AxisX><AxisY>
              <MajorGrid LineWidth="0" />
         </AxisY></asp:ChartArea>
                     </ChartAreas>
                  </asp:Chart>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
        </div>
         </div>
      <div style="margin-top:20px;" class="row panel panel-default ">
        <div id="ho_collapsebtn" class=" panel-heading3 " onclick="collapse('MainContent_ho_chart','MainContent_ho_charts_image')">
        <h4 class="panel-title">
            <img id="ho_charts_image" runat="server" src="images/button_plus.gif" align="left">  &nbsp;&nbsp;&nbsp;&nbsp;HO STATUS-COUNT
        </h4>
        </div>
        </div>
        <div class="row" id="ho_chart" runat="server">
   <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CUSTOMERS</div>
         </div>
         <div class="portlet-body">
            <div id="Div20" class="k-content">
               <div id="Div21" class="gauge-container" style="width:200px;">
                  <asp:Chart ID="chart_ho_cust" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true" ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:LinkButton ID="lbl_hoc_bdgt_val" CssClass="hyprlnk" style="color: #0066FF;float:none;margin-right:0px;" runat="server"  OnClick="ho_bdgt_val_Click" ></asp:LinkButton>   

               </div>
            </div>
         </div>
      </div>
   </div>
      <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CHANNEL PARTNERS </div>
         </div>
         <div class="portlet-body">
            <div id="Div22" class="k-content">
               <div id="Div23" class="gauge-container" style="width:200px;">
                  <asp:Chart ID="chart_ho_dstrbtr" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsValueShownAsLabel="true"  ChartType="Pie"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:LinkButton ID="lbl_hod_bdgt_val" CssClass="hyprlnk" style="color: #0066FF;float:none;margin-right:0px;" runat="server" OnClick="ho_bdgt_val_Click"  ></asp:LinkButton>   

               </div>
            </div>
         </div>
      </div>
   </div>
           <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">TOTALS</div>
         </div>
         <div class="portlet-body">
            <div id="Div24" class="k-content">
               <div id="Div25" class="gauge-container" style="width:200px;">
                  <asp:Chart ID="chart_ho_total" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Pie"></asp:Series>
                         
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                     </ChartAreas>
                     <Legends>
                        <asp:Legend></asp:Legend>
                     </Legends>
                  </asp:Chart>
                  <asp:LinkButton ID="lbl_hot_bdgt_val" CssClass="hyprlnk" style="color: #0066FF;float:none;margin-right:0px;" runat="server" OnClick="ho_bdgt_val_Click" ></asp:LinkButton>   

               </div>
            </div>
         </div>
      </div>
   </div>
</div>
        <div id="divbargraph" runat="server" >
        <div style="margin-top:20px;" class="row panel panel-default ">
        <div id="Div27" class=" panel-heading3 "  onclick="collapse('bargraphdiv','MainContent_bar_graphs_image')">
        <h4 class="panel-title">
            <img id="bar_graphs_image" runat="server" src="images/button_plus.gif" align="left">  &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lbl_bdgt_text" runat="server" Text="BUDGET DETAILS"></asp:Label>
        </h4>
        </div>
        </div>
   <div class="row" id="bargraphdiv">
       <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CUSTOMERS</div>
         </div>
         <div class="portlet-body">
            <div id="Div13" class="k-content" >
               <div id="Div26" >
                  <asp:Chart ID="cbdgt_bargraph" EnableViewState="true" runat="server">

                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Column"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="SkyBlue"><AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
              <MajorGrid LineWidth="0" /><LabelStyle Font="Verdana, 8.25pt" />
         </AxisX><AxisY>
              <MajorGrid LineWidth="0" />
         </AxisY></asp:ChartArea>
                     </ChartAreas>
                  </asp:Chart>
                 
               </div>
            </div>
         </div>
      </div>
   </div>

         <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">CHANNEL PARTNERS </div>
         </div>
         <div class="portlet-body">
            <div id="Div28" class="k-content" >
               <div id="Div29">
                  <asp:Chart ID="dbdgt_bargraph" EnableViewState="true" runat="server">

                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Column"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="SkyBlue"><AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
              <MajorGrid LineWidth="0" /><LabelStyle Font="Verdana, 8.25pt" />
         </AxisX><AxisY>
              <MajorGrid LineWidth="0" />
         </AxisY></asp:ChartArea>
                     </ChartAreas>
                  </asp:Chart>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
             <div class="col-md-4 col-sm-6">
      <div class="portlet box blue">
         <div class="portlet-title">
            <div class="caption">TOTALS</div>
         </div>
         <div class="portlet-body">
            <div id="Div30" class="k-content" >
               <div id="Div31">
                  <asp:Chart ID="tbdgt_bargraph" EnableViewState="true" runat="server">
                     <Series>
                        <asp:Series Name="Series1" IsVisibleInLegend="true" IsValueShownAsLabel="true" ChartType="Column"></asp:Series>
                     </Series>
                     <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="SkyBlue"><AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
              <MajorGrid LineWidth="0" /><LabelStyle Font="Verdana, 8.25pt" />
         </AxisX><AxisY>
              <MajorGrid LineWidth="0" />
         </AxisY></asp:ChartArea>
                     </ChartAreas>
                  </asp:Chart>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
        </div>
         </div>--%>

    <div style="margin-top: 20px;" class="row panel panel-default ">
        <div id="gridtitle" class=" panel-heading4 ">
            <h4 class="panel-title">
                <img id="cnsldt_img" runat="server" src="images/button_plus.gif" align="left">
                &nbsp;&nbsp;&nbsp;&nbsp; CONSOLIDATED STATUS
            </h4>
        </div>
    </div>
    <div id="divcnsldt" runat="server" style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
        <%-- margin-top: -25px;--%>
        <asp:HiddenField runat="server" ID="hdn_CustomerNum" />
        <asp:HiddenField ID="hdn_se_statuscnt" runat="server" />
        <asp:HiddenField ID="hdn_bm_statuscnt" runat="server" />
        <asp:HiddenField ID="hdn_ho_statuscnt" runat="server" />
        <asp:HiddenField ID="hdn_total_cnt" runat="server" />
        <asp:HiddenField ID="hdn_se_cust_cnt" runat="server" />
        <br />
        <asp:GridView ID="grdviewAllValues" runat="server" AutoGenerateColumns="False" ViewStateMode="Enabled" ShowHeader="False" Width="100%" Style="text-align: center;">
            <Columns>
                <asp:TemplateField HeaderText="Customer Number">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblcustomernumber" Text='<%# Eval("customer_number") %>'></asp:Label>
                        <asp:HiddenField runat="server" ID="hdn_customernumber" Value='<%# Eval("customer_number") %>' />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Customer Name">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lnkbtn_Customer_name" Text='<%# Eval("customer_name")%>' Visible='<%# Eval("flag").ToString() =="customer" %>' OnClick="lnkbtn_Vustomer_name_Click" Style="color: #2a6496; text-decoration: underline;"></asp:LinkButton>
                        <asp:Label runat="server" ID="lblcustomer_name" Text='<%# Eval("customer_name") %>' Visible='<%# Eval("flag").ToString() !="customer" %>'></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Customer Type">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lbl_customer_type" Text='<%# Eval("customer_type") %>'></asp:Label>
                        <asp:Image runat="server" ID="ibtnCtype" ImageUrl="~/images/arrow_down_popup.png" Visible='<%# Eval("flag").ToString() =="Heading" %>'
                            Style="padding-left: 10px;" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Customer Region">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lbl_Customer_region" Text='<%# Eval("Customer_region") %>'></asp:Label>
                        <asp:Image runat="server" ID="ibtnRegion" ImageUrl="~/images/arrow_down_popup.png" Visible='<%# Eval("flag").ToString() =="Heading" %>'
                            Style="padding-left: 10px;" />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Sales Engineer Status">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lbl_status_sales_engineer" Text='<%# Eval("status_sales_engineer") %>'></asp:Label>
                        <asp:Image runat="server" ID="ibtnSEStatus" ImageUrl="~/images/arrow_down_popup.png" Visible='<%# Eval("flag").ToString() =="Heading" %>' Style="padding-left: 10px;" />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Branch Manager Status">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lbl_status_branch_manager" Text='<%# Eval("status_branch_manager") %>'></asp:Label>
                        <asp:Image runat="server" ID="ibtnBMStatus" ImageUrl="~/images/arrow_down_popup.png" Visible='<%# Eval("flag").ToString() =="Heading" %>' Style="padding-left: 10px;" />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="HO Satus">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lbl_status_ho" Text='<%# Eval("status_ho") %>'></asp:Label>
                        <asp:Image runat="server" ID="ibtnHOStatus" ImageUrl="~/images/arrow_down_popup.png" Visible='<%# Eval("flag").ToString() =="Heading" %>' Style="padding-left: 10px;" />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Value">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lbl_estimate_value_next_year" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                        <asp:Label runat="server" ID="lblFlag" Text='<%# Eval("flag") %>' Style="display: none"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
    </div>


    <div class="col-md-2" id="footer" runat="server" style="height: 20px;">
        <asp:HiddenField runat="server" ID="hdnRole" />
    </div>


    <asp:Panel runat="server" ID="panelCustomerType" CssClass="panelBack" Style="display: none;">
        <div id="dropid">
            <asp:CheckBox runat="server" ID="CheckBox1" Text="ALL" /><br />
            <asp:CheckBox runat="server" ID="cbCTCustomer" CssClass="chkCT" Text="CUSTOMER" /><br />
            <asp:CheckBox runat="server" ID="cbCTDistributor" CssClass="chkCT" Text="CHANNEL PARTNER" /><br />
            <asp:Button runat="server" ID="btnCTOk" Text="Ok" OnClick="btnCTOk_Click" CssClass="btn green" /><br />
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="panelRegion" CssClass="panelBack" Style="display: none; width: 245px !important">
        <div id="Div1" style="width: 230px;">
            <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Style="max-height: 150px !important;">
                <asp:CheckBox runat="server" ID="cbAllRegion" Text="ALL" Style="margin-left: 10px;" /><br />
                <asp:CheckBoxList ID="cblRegion" runat="server" Width="90%">
                    <%--<asp:ListItem>BGL</asp:ListItem>
                                                    <asp:ListItem>NIC</asp:ListItem>
                                                    <asp:ListItem>PUD</asp:ListItem>
                                                    <asp:ListItem>PUR</asp:ListItem>
                                                    <asp:ListItem>MUM</asp:ListItem>
                                                    <asp:ListItem>GUR</asp:ListItem>--%>
                </asp:CheckBoxList>
            </asp:Panel>
            <asp:Button runat="server" ID="btnRegionOk" Text="Ok" OnClick="btnRegionOk_Click" Style="top: 0px" CssClass="btn green" /><br />
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="panelSEStatus" CssClass="panelBack" Style="display: none;">
        <div id="Div2" style="float: left;">
            <asp:CheckBox runat="server" ID="CheckBox2" Text="ALL" /><br />
            <asp:CheckBox runat="server" ID="cb_se_NI" CssClass="chkse" Text="Not Initiated" /><br />
            <asp:CheckBox runat="server" ID="cb_se_PS" CssClass="chkse" Text="Pending Submission" /><br />
            <asp:CheckBox runat="server" ID="cb_se_Sub" CssClass="chkse" Text="Submitted" /><br />
            <asp:Button runat="server" ID="btn_se_s_ok" Text="Ok" OnClick="btn_se_s_ok_Click" CssClass="btn green" /><br />
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="panelBMStatus" CssClass="panelBack" Style="display: none;">
        <div id="Div3">
            <asp:CheckBox runat="server" ID="CheckBox3" Text="ALL" /><br />
            <asp:CheckBox runat="server" ID="cb_bm_NI" CssClass="chkbm" Text="Not Initiated" /><br />
            <asp:CheckBox runat="server" ID="cb_bm_PS" CssClass="chkbm" Text="Pending Submission" /><br />
            <asp:CheckBox runat="server" ID="cb_bm_Sub" CssClass="chkbm" Text="Submitted" /><br />
            <asp:CheckBox runat="server" ID="cb_bm_tr" CssClass="chkbm" Text="To be Reviewed" /><br />
            <asp:CheckBox runat="server" ID="cb_bm_sr" CssClass="chkbm" Text="Sent for Review" /><br />
            <asp:CheckBox runat="server" ID="cb_bm_app" CssClass="chkbm" Text="Approved" /><br />
            <asp:Button runat="server" ID="btn_bm_s_ok" Text="Ok" OnClick="btn_bm_s_ok_Click" CssClass="btn green" /><br />
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="panelHOStatus" CssClass="panelBack" Style="display: none;">
        <div id="Div4">
            <asp:CheckBox runat="server" ID="CheckBox4" Text="ALL" /><br />
            <asp:CheckBox runat="server" ID="cb_ho_tr" CssClass="chkho" Text="To be Reviewed" /><br />
            <asp:CheckBox runat="server" ID="cb_ho_app" CssClass="chkho" Text="Approved" /><br />
            <%--            <asp:CheckBox runat="server" ID="ch_ho_null" Text="" /><br />--%>
            <asp:Button runat="server" ID="btn_ho_s_ok" Text="Ok" OnClick="btn_ho_s_ok_Click" CssClass="btn green" /><br />
        </div>
    </asp:Panel>
    <div id="DivBranchApprove" style="width: 100%; display: none; text-align: center !important; height: auto !important">
        <label class="col-md-4 ">BRANCH</label>
        <asp:DropDownList ID="ddlBranchList" runat="server" CssClass="form-control" ViewStateMode="Enabled" Width="68%"></asp:DropDownList>
        <br />
        <asp:Button runat="server" ID="btnBranchApproval" OnClientClick=" javascript:closePopUpApprove();" OnClick="btnBranchApproval_Click" CssClass="btn green" Style="width: 130px; font-weight: bolder; margin-left: 20px;" Text="APPROVE" />

    </div>




    <script type="text/javascript">
        function visibleLink() {
            document.getElementById("lnkbtnBulkapproval").style.visibility = "visible";
        }
        function disableLink() {
            document.getElementById("lnkbtnBulkapproval").style.visibility = "hidden";
        }
    </script>

       
</asp:Content>
