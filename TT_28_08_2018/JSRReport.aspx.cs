﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class JSRReport : System.Web.UI.Page
    {
        public static string cter;

        protected void Page_Load(object sender, EventArgs e)
        {
             if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

             if (!IsPostBack)
             {
                 cter = null;
                 string strUserId = Session["UserId"].ToString();
                 string roleId = Session["RoleId"].ToString();
                 //load product family details
                 
                 if (Session["RoleId"].ToString() == "HO" || Session["RoleId"].ToString() == "TM")
                 {
                     if (Session["RoleId"].ToString() == "HO")
                     {
                         if (Session["cter"] == null && roleId == "HO")
                         {
                             Session["cter"] = "TTA";
                             cter = "TTA";

                         }
                         if (Session["cter"].ToString() == "DUR")
                         {
                             rdBtnDuraCab.Checked = true;
                             rdBtnTaegutec.Checked = false;
                             cter = "DUR";
                         }
                         else
                         {
                             rdBtnTaegutec.Checked = true;
                             rdBtnDuraCab.Checked = false;
                             cter = "TTA";
                         }
                         cterDiv.Visible = true;
                     }

                 }
             }
        }
        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please click on FILTER to view results');triggerPostGridLodedActions();", true);
            if (rdBtnTaegutec.Checked)
            {
                Session["cter"] = "TTA";
                cter = "TTA";
            }
            if (rdBtnDuraCab.Checked)
            {
                Session["cter"] = "DUR";
                cter = "DUR";
            }
        }
    }
}