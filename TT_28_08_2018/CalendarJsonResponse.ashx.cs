﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using TaegutecSalesBudget.App_Code.BOL;

namespace TaegutecSalesBudget
{
    /// <summary>
    /// Summary description for CalendarJsonResponse
    /// </summary>
    public class CalendarJsonResponse : IHttpHandler, IRequiresSessionState 
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string userBranch =Convert.ToString( context.Session["BranchCode"]);

            string userID = Convert.ToString(context.Session["UserId"]);
            string cter=null;
            if(!string.IsNullOrEmpty(Convert.ToString(context.Session["cter"]))){
                cter = null;
            }
            string territoryEngineerID=null;
            if (Convert.ToString(context.Session["RoleId"])=="BM")
            {
                userID = null;
            }
            else if (Convert.ToString(context.Session["RoleId"]) == "HO")
            {
                userBranch = null;
                userID = null;
            }
            else if (Convert.ToString(context.Session["RoleId"]) == "TM")
            {
                territoryEngineerID = userID;
                userBranch = null;
                userID = null;
            }
            // FullCalendar 2.x
            DateTime start = Convert.ToDateTime(context.Request.QueryString["start"]);
            DateTime end = Convert.ToDateTime(context.Request.QueryString["end"]);
            string filterflag=(string)(HttpContext.Current.Items["FilterClick"]);
            if (filterflag=="yes")
            {
                userBranch = (string)(HttpContext.Current.Items["Branch"]);
                userID = (string)(HttpContext.Current.Items["salesEngId"]);
            }
            List<int> idList = new List<int>();
            List<ImproperCalendarEvent> tasksList = new List<ImproperCalendarEvent>();
           
            //Generate JSON serializable events
            foreach (CalendarEvent cevent in EventDAO.getEvents(start, end, userBranch, userID, territoryEngineerID, cter))
            {
                DateTime tempStart = Convert.ToDateTime(cevent.visit_date_start);
                DateTime tempend = Convert.ToDateTime(cevent.visit_date_end);

                tasksList.Add(new ImproperCalendarEvent
                {
                    id = cevent.id,
                    agenda = cevent.agenda,
                    customerNumber = cevent.customerNumber,
                    customer_short_name = cevent.customer_short_name,
                    customer_class = cevent.customer_class,
                    start = String.Format("{0:s}", tempStart),
                    end = String.Format("{0:s}", tempend),
                    remarks = cevent.remarks ,
                    project_id=cevent.project_id,
                    isSubmit=cevent.isSubmit,
                    salesEngineer=cevent.salesEngineer,
                    distributor_number=cevent.distributor_number,
                    visited_salesengineer_id = cevent.visited_salesengineer_id,
                    visited_salesengineer_name = cevent.visited_salesengineer_name
              
                }
                );
                idList.Add(cevent.id);
            }

            context.Session["idList"] = idList;

            //Serialize events to string
            System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string sJSON = oSerializer.Serialize(tasksList);

            //Write JSON to response object
            context.Response.Write(sJSON);
        }

        public bool IsReusable
        {
            get { return false; }
        }

       
    }
}