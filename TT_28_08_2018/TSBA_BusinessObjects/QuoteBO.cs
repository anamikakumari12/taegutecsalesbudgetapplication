﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSBA_BusinessObjects
{
    public class QuoteBO
    {

        public string Cust_Number { get; set; }
        public string Item_Number { get; set; }
        public int Err_code { get; set; }
        public string Err_msg { get; set; }

        public string Ref_Number { get; set; }

        public string EndDate { get; set; }

        public string StartDate { get; set; }
        public int QuoteID { get; set; }
        public string OrderType { get; set; }
        public string scheduleType { get; set; }
        public string quantity { get; set; }
        public string orderbyId { get; set; }
        public string orderbyName { get; set; }
        public string orderbyFlag { get; set; }
        public string NumberOfOrder { get; set; }


        public string SE_id { get; set; }
        public string role_id { get; set; }
        public string branch { get; set; }
        public string cter { get; set; }
        public string customer_class { get; set; }
        public string TM_id { get; set; }
        public string cust_type { get; set; }

        public string Cust_Name { get; set; }


        public string Reason { get; set; }

        public string ChangeBy { get; set; }

        public string MOQ { get; set; }

        public string Offer_Price { get; set; }

        public string Status { get; set; }

        public int RefID { get; set; }

        public string Quotation_no { get; set; }
        public string to { get; set; }
        public string cc { get; set; }
        public string subject { get; set; }
        public string message { get; set; }

        public int MultiOrderFlag { get; set; }

        public string OrderStartDate { get; set; }

        public string user_id { get; set; }

        public string RecommendedPrice { get; set; }

        public string POComment { get; set; }
        public int pdf_flag { get; set; }
    }

    public class ItemClass
    {
        public string item { get; set; }
        public string item_desc { get; set; }
        public string AP { get; set; }
        public string WHS { get; set; }
        public string LP { get; set; }
        public int stockCode { get; set; }
        public string Valid_from { get; set; }
        public string Valid_to { get; set; }
        public string quantity { get; set; }
    }

    public class searchCls
    {
        public string q { get; set; }
        public string page { get; set; }
        public string per_page { get; set; }
    }
    public class itemddl
    {
        public string id { get; set; }
        public string text { get; set; }
    }
    public class itemDetails
    {
        public string Ref_number { get; set; }
        public string Item_code { get; set; }
        public string Item_Desc { get; set; }
        public string WHS { get; set; }
        public string Order_type { get; set; }
        public string Order_freq { get; set; }
        public string Total_QTY { get; set; }
        public string QTY_perOrder { get; set; }
        public string List_Price { get; set; }
        public string Expected_price { get; set; }
        public string DC_rate { get; set; }
        public string Cust_number { get; set; }
        public string Cust_Name { get; set; }
        public string Cust_SP { get; set; }
        public string Comp_Name { get; set; }
        public string Comp_Desc { get; set; }
        public string Comp_SP { get; set; }
        public string CP_number { get; set; }
        public string CP_Name { get; set; }
        public string Status { get; set; }
        public string RequestedBy { get; set; }
        public string RequestedBy_Flag { get; set; }

    }

    public class Output
    {
        public int ErrorCode { get; set; }
        public string ErrorMsg { get; set; }
    }

    public class QuoteStatus
    {
        public string id { get; set; }
        public string item { get; set; }
        public string ref_no { get; set; }
        public string status { get; set; }
        public string comment { get; set; }
        public string MOQ { get; set; }
        public string offerPrice { get; set; }
        public string multiorder_flag { get; set; }
        public string RecPrice { get; set; }
        public string specialprice { get; set; }
        public string Order_Validity { get; set; }
        public string Approved_OrderQty { get; set; }
        public string Approved_OrderFreq { get; set; }
        public string file { get; set; }
        public string Internal_Remarks { get; set; }
    }

    public class QuoteStatusDB
    {
        public int ID { get; set; }
        public string RefNumber { get; set; }
        public string item { get; set; }
        public string flag { get; set; }
        public string changeby { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public string MOQ { get; set; }
        public string OfferPrice { get; set; }
        public int MultiOrderFlag { get; set; }
        public string RecommendedPrice { get; set; }
        //public DateTime expiry_date { get; set; }
        public string Order_Validity { get; set; }
        public string Approved_OrderQty { get; set; }
        public string Approved_OrderFreq { get; set; }
        public string Comp_name { get; set; }
        public string Comp_desc { get; set; }
        public string Comp_SP { get; set; }
        public string End_cust_name { get; set; }
        public string End_cust_num { get; set; }
        public string Escalation_file { get; set; }
        public string BM_Approval_file { get; set; }
        public string HO_Approval_file { get; set; }
        public string Internal_Remarks { get; set; }
    }

    public class SplQuoteBO
    {
        public string DropdwonValue { get; set; }
        public int DropdwonNumber { get; set; }
        public string CustomerType { get; set; }
        public string Distributor { get; set; }
        public string CustomerNumber { get; set; }
        public int Err_code { get; set; }
        public string Err_msg { get; set; }
        public string Ref_Number { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
        public string Value4 { get; set; }
        public string Value5 { get; set; }
        public string Value6 { get; set; }
        public string Value7 { get; set; }
        public string Value8 { get; set; }
        public string Value9 { get; set; }
        public string Value10 { get; set; }
    }

    public class SplItemDetails
    {
        public string Cust_type { get; set; }
        public string Cust_number { get; set; }
        public string CPCust_number { get; set; }
        public string Cat1 { get; set; }
        public string Cat2 { get; set; }
        public string Cat3 { get; set; }
        public string Cat4 { get; set; }
        public string Cat5 { get; set; }
        public string Cat6 { get; set; }
        public string Cat7 { get; set; }
        public string Cat8 { get; set; }
        public string Item_type { get; set; }
        public string Item_name { get; set; }
        public string ref_item { get; set; }
        public string Status { get; set; }
        public string RequestedBy { get; set; }
        public string RequestedBy_Flag { get; set; }

    }
}
