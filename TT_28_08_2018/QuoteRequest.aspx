﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteRequest.aspx.cs" Inherits="TaegutecSalesBudget.QuoteRequest" MasterPageFile="~/Site.Master" EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
    <link href="css/Tabs.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <style>
       
         #tblAPList {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #tblAPList td, #tblAPList th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #tblAPList tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #tblAPList tr:hover {
                background-color: #ddd;
            }

            #tblAPList th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #008a8a;
                color: white;
            }

        .disabled
        {
            background-color: #e4e4e4!important;
            border: 1px solid #aaa!important;
            border-radius: 4px!important;
            cursor: default!important;
            float: left!important;
            /* margin-right: 5px; */
            /* margin-top: 5px; */
            padding: 0 5px!important;
        }

        .btncontrol
        {
            padding-left: 5px;
            padding-top: 10px;
        }

        .btn.green
        {
            margin-top: 10px;
        }

        .control_dropdown
        {
            width: 180px;
            height: 30px;
            border-radius: 4px!important;
        }

        .label
        {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

        .control
        {
            padding-top: 2px;
        }

        .dataTables_wrapper
        {
            min-height: 250px;
            width:80%;
            margin-left:10%;
        }

        .order
        {
            padding-left: 10px;
            padding-right: 10px;
        }

        .divorder
        {
            border: 1px solid #316f8f;
            height: 90px;
            padding: 10px;
        }

        .ddl
        {
            background-color: #fff;
            border: 1px solid #aaa;
            box-sizing: border-box;
            cursor: pointer;
            display: block;
            height: 28px;
            border-radius: 4px!important;
        }

        .required:after
        {
            content: " *";
            color: red;
        }

        .select2-container--default .select2-selection--single
        {
            border-radius: 4px!important;
        }
        .loader_div
        {
            position: fixed;
            top: 0;
            bottom: 0%;
            left: 0;
            right: 0%;
            z-index: 99;
            opacity: 0.7;
            display: none;
            background: lightgrey url('../../../images/loader.gif') center center no-repeat;
        }
    </style>
    <script>
        $(document).ready(function () {
            //$(".ep").blur(function () {
                $('#MainContent_grdPriceRequest').on('blur', 'tbody tr td .ep', function () {
                debugger;
                var id = $(this).get(0).id;
                var data = sessionStorage.getItem("APValidity");
                data = JSON.parse(data);
                if (!(data.d.item == null || data.d.item == "" || data.d.item == undefined)) {
                    var message = "The agreement price for item " + data.d.item_desc + " with quantity " + data.d.quantity + " is already available with price " + data.d.AP + ". The price is valid till " + data.d.Valid_to + ". If you want to proceed with request, you should enter lesser requested price than agreement price. Do you want to continue?";
                    if (window.confirm(message)) {
                        // They clicked Yes
                    }
                    else {
                        var hdnItem_id = id.replace("txtTargetPrice", "hdnItem");
                        var item_id = id.replace("txtTargetPrice", "ddlitem");
                        var WHS_id = id.replace("txtTargetPrice", "txtWHS");
                        var LP_id = id.replace("txtTargetPrice", "txtDLP");
                        var AP_id = id.replace("txtTargetPrice", "txtAP");
                        $('#' + WHS_id).val("");
                        $('#' + LP_id).val("");
                        $('#' + AP_id).val("");
                        $('#' + hdnItem_id).val("");
                        $("#" + item_id).val("0");
                        $("#" + item_id).select2({ text: "--Select--", "minimumInputLength": 3, });
                        $('#' + id).val("");
                        $('#' + id.replace("txtTargetPrice", "txtDCRate")).val("");
                    }
                }
            });

            // $(".qty").focus(function () {
            $('#MainContent_grdPriceRequest').on('focusout', 'tbody tr td .qty', function () {

                console.log('out');
                var cust_number = $('#MainContent_ddlCustomers').val();
                var qty_id = $(this).get(0).id;
                if (qty_id.includes('txtTotQTY')) {
                    var qty_value = $(this).val();
                    var freq = $("#" + qty_id.replace('txtTotQTY', 'ddlOrder')).val();
                    var item_value = $("#" + qty_id.replace('txtTotQTY', 'hdnItem')).val();
                 /*   if (freq == "onetime") {*/
                        $.ajax({
                            url: 'QuoteRequest.aspx/LoadAgreementPrice',
                            method: 'post',
                            datatype: 'json',
                            data: '{"item":"' + item_value + '", "qty":"' + qty_value + '", "cust":"' + cust_number + '"}',
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                console.log(data);
                                console.log(JSON.stringify(data));
                                sessionStorage.setItem('APValidity', JSON.stringify(data));
                                $('#' + qty_id.replace('txtTotQTY', 'txtAP')).val(data.d.AP);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(xhr.responseText);
                            }
                        });

                   /* }*/
                }
                //else
                //{
                //    var qty_value = $(this).val();

                //    var item_value = $("#" + qty_id.replace('txtQTYPO', 'hdnItem')).val();
                //    $.ajax({
                //        url: 'QuoteRequest.aspx/LoadAgreementPrice',
                //        method: 'post',
                //        datatype: 'json',
                //        data: '{"item":"' + item_value + '", "qty":"' + qty_value + '", "cust":"' + cust_number + '"}',
                //        contentType: "application/json; charset=utf-8",
                //        success: function (data) {
                //            console.log(data);
                //            console.log(JSON.stringify(data));
                //            sessionStorage.setItem('APValidity', JSON.stringify(data));
                //            $('#' + qty_id.replace('txtQTYPO', 'txtAP')).val(data.d.AP);
                //        },
                //        error: function (xhr, ajaxOptions, thrownError) {
                //            alert(xhr.responseText);
                //        }
                //    });
                //}


            });

            $(".add-row").click(function (evt, obj) {

                add_row(evt, obj);
            });

            $('.ddl_item').on("change", function (e) {
                debugger;
                var item_value = $(this).val();
                var item_id = $(this).get(0).id;
                $("#" + item_id.replace('ddlitem', 'hdnItem')).val(item_value);
                $.ajax({
                    url: 'QuoteRequest.aspx/LoadItemDetails',
                    method: 'post',
                    datatype: 'json',
                    data: '{"item":"' + item_value + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        BindItemDetails(data, item_id);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });

            });

            $('.delbtn').on('click', function (evt) {
                debugger;
                if (Deletepopup(evt, this)) {
                    var $row = jQuery(this).closest('tr');
                    $row.remove();
                    if (parseInt($("[id*=grdPriceRequest] tr").length) <= 3) {
                        add_new_row(0);
                    }
                }
            });

            $('.btnSubmit').on('click', function (evt, obj) {
                jQuery(".loader_div").show();
                if (validateFields(evt, obj)) {
                    var totaltrCount = $("[id*=grdPriceRequest] tr").length;
                    var trCount = $("[id*=grdPriceRequest] td").closest("tr").length;
                    var param;
                    var paramList = [];
                    var values = "";
                    var item_id;
                    var i;
                    for (rowCount = 0; rowCount < trCount; rowCount++) {
                        item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(rowCount).id;

                        i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
                        param = {
                            //Ref_number: $('#MainContent_txtRef').val(),
                            //Item_code: $('#MainContent_grdPriceRequest_ddlitem_' + i).val(),
                            //Item_Desc: $('#MainContent_grdPriceRequest_ddlitem_' + i + '  option:selected').text(),
                            //WHS: $('#MainContent_grdPriceRequest_txtWHS_' + i).val(),
                            //Order_type: $('#MainContent_grdPriceRequest_ddlOrder_' + i).val(),
                            //Order_freq: $('#MainContent_grdPriceRequest_ddlFrequency_' + i).val(),
                            //Total_QTY: $('#MainContent_grdPriceRequest_txtTotQTY_' + i).val(),
                            //QTY_perOrder: $('#MainContent_grdPriceRequest_txtQTYPO_' + i).val(),
                            //List_Price: $('#MainContent_grdPriceRequest_txtDLP_' + i).val(),
                            //Expected_price: $('#MainContent_grdPriceRequest_txtTargetPrice_' + i).val(),
                            //DC_rate: $('#MainContent_grdPriceRequest_txtDCRate_' + i).val(),
                            //Cust_number:  $('#MainContent_ddlCustomers').val(),
                            //Comp_Name: $('#MainContent_grdPriceRequest_ddlCompanyName_' + i).val(),
                            //Comp_Desc: $('#MainContent_grdPriceRequest_txtDescription_' + i).val(),
                            //Comp_SP: $('#MainContent_grdPriceRequest_txtCompanySP_' + i).val()

                            Ref_number: $('#MainContent_txtRef').val(),
                            Item_code: $('#MainContent_grdPriceRequest_ddlitem_' + i).val(),
                            Item_Desc: $('#MainContent_grdPriceRequest_ddlitem_' + i + '  option:selected').text(),
                            WHS: $('#MainContent_grdPriceRequest_txtWHS_' + i).val(),
                            Order_type: '',
                            Order_freq: '',
                            Total_QTY: $('#MainContent_grdPriceRequest_txtTotQTY_' + i).val(),
                            QTY_perOrder: '',
                            List_Price: $('#MainContent_grdPriceRequest_txtDLP_' + i).val(),
                            Expected_price: '',
                            DC_rate: '',
                            Cust_number: $('#MainContent_ddlCustomers').val(),
                            Cust_SP: '',
                            Comp_Name: '',
                            Comp_Desc: '',
                            Comp_SP: ''
                        }
                        paramList.push(param);
                    }
                    var dataParam = {
                        obj: paramList
                    }
                    $.ajax({
                        url: 'QuoteRequest.aspx/GetItemDetailsForTable',
                        method: 'post',
                        datatype: 'json',
                        data: JSON.stringify(dataParam),
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            alert(data.d.ErrorMsg);
                            location.reload(true);
                            jQuery(".loader_div").hide();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.responseText);
                            jQuery(".loader_div").hide();
                        }
                    });
                }
                else {
                    alert("Please check error message.");
                    jQuery(".loader_div").hide();
                }


            });

            $('.lnk_item').on('click', function (evt) {

                var item_id = $(this).get(0).id.replace('lnk', 'lbl');
                var item_value = $('#' + item_id).text();

                $.ajax({
                    url: 'QuoteRequest.aspx/LoadDetailsFromlink',
                    method: 'post',
                    datatype: 'json',
                    data: '{"item":"' + item_value + '", "type":"F"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        LoadFrequentlyRecentlyItem(data);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            });

            $('.lnk_Ritem').on('click', function (evt) {

                var item_id = $(this).get(0).id.replace('lnk', 'lbl');
                var item_value = $('#' + item_id).text();

                $.ajax({
                    url: 'QuoteRequest.aspx/LoadDetailsFromlink',
                    method: 'post',
                    datatype: 'json',
                    data: '{"item":"' + item_value + '", "type":"R"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data);
                        LoadFrequentlyRecentlyItem(data);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            });
            LoadDropdowns();
            LoadTable();
        });

        function LoadAP(event, obj)
        {
            //var cust_number = $('#MainContent_ddlCustomers').val();
            //var qty_value = $('#' + obj.id).val();
            //var qty_id = obj.id;
            //var item_value = $("#" + qty_id.replace('txtTotQTY', 'hdnItem')).val();
            //$.ajax({
            //    url: 'QuoteRequest.aspx/LoadAgreementPrice',
            //    method: 'post',
            //    datatype: 'json',
            //    data: '{"item":"' + item_value + '", "qty":"' + qty_value + '", "cust":"' + cust_number + '"}',
            //    contentType: "application/json; charset=utf-8",
            //    success: function (data) {
            //        console.log(data);
            //        console.log(JSON.stringify(data));
            //        sessionStorage.setItem('APValidity', JSON.stringify(data));
            //        $('#' + qty_id.replace('txtTotQTY', 'txtAP')).val(data.d.AP);
            //    },
            //    error: function (xhr, ajaxOptions, thrownError) {
            //        alert(xhr.responseText);
            //    }
            //});
        }
        function ddl_item_change(e) {

            var item_value = e.value;
            var item_id = e.id;
            $("#" + item_id.replace('ddlitem', 'hdnItem')).val(item_value);
            $.ajax({
                url: 'QuoteRequest.aspx/LoadItemDetails',
                method: 'post',
                datatype: 'json',
                data: '{"item":"' + item_value + '"}',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    BindItemDetails(data, item_id);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });

        }

        function delete_row(evt, obj) {

            if (Deletepopup(evt, obj)) {
                var $row = jQuery(obj).closest('tr');
                $row.remove();
                if (parseInt($("[id*=grdPriceRequest] tr").length) <= 3) {
                    add_new_row(0);
                }
                //var $columns = $row.find('td');
                //$columns.addClass('row-highlight');
                //var values = "";
                //jQuery.each($columns, function (i, item) {
                //    values = values + 'td' + (i + 1) + ':' + item.innerHTML + '<br/>';
                //    alert(values);
                //});
                //console.log(values);
            }
        }
        function add_row(evt, obj) {

            if (validateFields(evt, obj)) {
                var totaltrCount = $("[id*=grdPriceRequest] tr").length;
                var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;

                var item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(rowCount - 1).id;

                var trCount = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
                trCount++;
                add_new_row(trCount);
               
            }
        }
        function add_new_row(trCount)
        {
           // markup = '<tr role="row" class="even">				<td>' +
           //           '         <input type="hidden" id="MainContent_grdPriceRequest_hdnItem_' + trCount + '">' +
           //          '          <input type="hidden" id="MainContent_grdPriceRequest_hdnStockCode_' + trCount + '">' +
           //         '           <select id="MainContent_grdPriceRequest_ddlitem_' + trCount + '" onchange="ddl_item_change(this);" class="ddl_item" style="width:200px!important;"></select>' +
           //        '        </td><td>' +
           //       '             <input type="text" readonly="readonly" id="MainContent_grdPriceRequest_txtWHS_' + trCount + '" class="ddl" style="width: 70px;">' +
           //      '          </td><td>' +
           //     '               <select id="MainContent_grdPriceRequest_ddlOrder_' + trCount + '" class="ddl" onchange="OrderChange(this);">' +
           //    '	<option value="onetime">One Time</option>' +
           //    '	<option value="schedule">Schedule</option>' +
           //    '</select>' +
           //                '</td><td>' +
           //                 '   <input type="text" id="MainContent_grdPriceRequest_txtTotQTY_' + trCount + '" class="ddl qty" onkeyup="LaodAP(event, this);" onkeypress="return isNumWithoutDecimalKey(event,this);" style="width: 80px;">' +
           //                '</td><td>' +
           //                 '             <a href="#mdAPList" rel="modal:open"> <asp:Image ID="MainContent_grdPriceRequest_viewAPList_' + trCount + '" onclick="ShowAPList(this);" src="images/info.jpg" Style="width: 20px;" /></a>' +
           // '          </td><td>' +

           //                '    <input type="text" id="MainContent_grdPriceRequest_txtQTYPO_' + trCount + '" class="ddl disabled" onkeypress="return isNumWithoutDecimalKey(event,this);" style="width: 80px;" disabled="">' +
           //                '</td><td>' +
           //                 '   <input type="text" readonly="readonly" id="MainContent_grdPriceRequest_txtDLP_' + trCount + '" class="ddl disabled" style="width: 80px;">' +
           //                '</td><td>' +
           //                 '   <input type="text" readonly="readonly" id="MainContent_grdPriceRequest_txtAP_' + trCount + '" class="ddl disabled" style="width: 80px;">' +
           //                '</td><td>' +
           //                 '   <input type="text" id="MainContent_grdPriceRequest_txtTargetPrice_' + trCount + '" class="ddl ep" onkeypress="return isNumberKey(event,this);" onkeyup="calDCrate(event,this);" style="width: 80px;">' +
           //                '    <input type="hidden" id="MainContent_grdPriceRequest_hdnAgreementPrice_' + trCount + '">' +
           //               '     <span id="MainContent_grdPriceRequest_lblPriceError_' + trCount + '" style="color: red;"></span>' +
           //              '  </td><td>' +
           //             '       <input type="text" id="MainContent_grdPriceRequest_txtDCRate_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" onkeyup="calExpectedPrice(event,this);" style="width: 80px;">' +
           //            '    </td><td>' +
              
           //                  '  <input type="image" id="MainContent_grdPriceRequest_imgbtnAdd_' + trCount + '" onclick="add_row();" class="add-row" src="images/add.png" onclick="return false;" style="width: 25px;">' +
           //                 '   <input type="image" id="MainContent_grdPriceRequest_imgbtnDel_' + trCount + '" class="delbtn" src="images/delete.png" onclick="delete_row(event,this);" onclick="return false;"  style="width: 25px;">' +
           //                '</td>' +
           //'</tr>";'
            markup = '<tr role="row" class="even">				<td>' +
                '         <input type="hidden" id="MainContent_grdPriceRequest_hdnItem_' + trCount + '">' +
                '          <input type="hidden" id="MainContent_grdPriceRequest_hdnStockCode_' + trCount + '">' +
                '           <select id="MainContent_grdPriceRequest_ddlitem_' + trCount + '" onchange="ddl_item_change(this);" class="ddl_item" style="width:200px!important;"></select>' +
                '        </td><td>' +
                '             <input type="text" readonly="readonly" id="MainContent_grdPriceRequest_txtWHS_' + trCount + '" class="ddl disabled" style="width:70px;">' +
                ' <label type="hidden"  id="MainContent_grdPriceRequest_lblMOQ_' + trCount + '">' +
                ' <label type="hidden"  id="MainContent_grdPriceRequest_lblIPACK_' + trCount + '">' +
                '          </td><td>' +
                //'             <label id="MainContent_grdPriceRequest_lblMOQ_' + trCount + '" class="ddl disabled" style="width: 70px;  text-align: right; padding: 5px!important;">' +
                //'          </td><td>' +
                //'               <select id="MainContent_grdPriceRequest_ddlOrder_' + trCount + '" class="ddl" onchange="OrderChange(this);">' +
                //'	<option value="onetime">One Time</option>' +
                //'	<option value="schedule">Schedule</option>' +
                //'</select>' +
                //'          </td><td>' +
                // '               <select id="MainContent_grdPriceRequest_ddlFrequency_' + trCount + '" class="ddl disabled" disabled="">' +
                //'	<option value="weekly">Weekly</option>' +
                //'	<option value="fortnightly">Fortnightly</option>' +
                //'	<option value="monthly">Monthly</option>' +
                //'</select>' +
                //            '</td><td>' +
                '   <input type="text" id="MainContent_grdPriceRequest_txtTotQTY_' + trCount + '" class="ddl qty" onkeyup="LaodAP(event, this);" onkeypress="return isNumWithoutDecimalKey(event,this);" style="width: 80px;">' +
                '</td><td>' +
                '  <a href="#mdAPList" rel="modal:open"> <input type="image" id="MainContent_grdPriceRequest_viewAPList_' + trCount + '" class="delbtn" src="images/info.jpg" onclick="ShowAPList(this);" onclick="return false;" style="width: 20px;"></a>' +
                '</td><td>' +

                //'    <input type="text" id="MainContent_grdPriceRequest_txtQTYPO_' + trCount + '" class="ddl disabled qty" onkeypress="return isNumWithoutDecimalKey(event,this);" style="width: 80px;" disabled="">' +
                //'</td><td>' +
                '   <input type="text" readonly="readonly" id="MainContent_grdPriceRequest_txtDLP_' + trCount + '" class="ddl disabled" style="width: 80px;">' +
                '</td><td>' +
                '   <input type="text" readonly="readonly" id="MainContent_grdPriceRequest_txtAP_' + trCount + '" class="ddl disabled" style="width: 80px;">' +
                '</td><td>' +
                //     '   <input type="text" id="MainContent_grdPriceRequest_txtTargetPrice_' + trCount + '" class="ddl ep" onkeypress="return isNumberKey(event,this);" onkeyup="calDCrate(event,this);" style="width: 80px;">' +
                //    '    <input type="hidden" id="MainContent_grdPriceRequest_hdnAgreementPrice_' + trCount + '">' +
                //   '     <span id="MainContent_grdPriceRequest_lblPriceError_' + trCount + '" style="color: red;"></span>' +
                //  '  </td><td>' +
                // '       <input type="text" id="MainContent_grdPriceRequest_txtDCRate_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" onkeyup="calExpectedPrice(event,this);" style="width: 80px;">' +
                //'    </td><td>' +
                //'         <select id="MainContent_grdPriceRequest_ddlCustName_' + trCount + '" onchange="ddl_cust_change(this);" class="ddl customer" style="width: 200px;" ></select>' +
                //'          <input type="hidden" id="MainContent_grdPriceRequest_hdnCustomers_' + trCount + '">' +
                //'           <span id="MainContent_grdPriceRequest_lblCustError_' + trCount + '" style="color: red;"></span>' +
                //'        </td><td>' +
                //'             <input type="text" id="MainContent_grdPriceRequest_txtCustSP_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" style="width: 80px;">' +
                //'          </td><td>' +
                // '               <select id="MainContent_grdPriceRequest_ddlCompanyName_' + trCount + '" class="ddl" style="width: 200px;">' +
                //'</select>' +
                //            '</td><td>' +
                //             '   <input type="text" id="MainContent_grdPriceRequest_txtDescription_' + trCount + '" class="ddl" style="width: 200px;">' +
                //            '</td><td>' +
                //             '   <input type="text" id="MainContent_grdPriceRequest_txtCompanySP_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" style="width: 80px;">' +
                //            '</td><td>' +
                '  <input type="image" id="MainContent_grdPriceRequest_imgbtnAdd_' + trCount + '" onclick="add_row();" class="add-row" src="images/add.png" onclick="return false;" style="width: 25px;">' +
                '  <input type="image" id="MainContent_grdPriceRequest_imgbtnCopy_' + trCount + '" onclick="copy_row(event,this);" class="copy-row" src="images/copy.jpg" onclick="return false;" style="width: 25px;">' +
                '   <input type="image" id="MainContent_grdPriceRequest_imgbtnDel_' + trCount + '" class="delbtn" src="images/delete.png" onclick="delete_row(event,this);" onclick="return false;"  style="width: 25px;">' +
                '</td>' +
                '</tr>";'
            tableBody = $("table tbody");
            tableBody.append(markup);
            //  LoadDropdowns();
            LoadDropdownForNextRow(trCount);
        }

        function LoadFrequentlyRecentlyItem(data) {

            var msg = data.d;
            var totaltrCount = $("[id*=grdPriceRequest] tr").length;
            var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;

            var item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(rowCount - 1).id;

            var trCount = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
            trCount++;
        //    markup = '<tr role="row" class="even">				<td>' +
        //           '         <input type="hidden" id="MainContent_grdPriceRequest_hdnItem_' + trCount + '" value="' + msg.Item_code + '" >' +
        //          '          <input type="hidden" id="MainContent_grdPriceRequest_hdnStockCode_' + trCount + '">' +
        //         '           <select id="MainContent_grdPriceRequest_ddlitem_' + trCount + '" onchange="ddl_item_change(this);" class="ddl_item" style="width:200px!important;"></select>' +
        //        '        </td><td>' +
        //       '             <input type="text" value="' + msg.WHS + '" readonly="readonly" id="MainContent_grdPriceRequest_txtWHS_' + trCount + '" class="ddl" style="width: 70px;">' +
        //      '          </td><td>' +
        //     '               <select id="MainContent_grdPriceRequest_ddlOrder_' + trCount + '" class="ddl" onchange="OrderChange(this);">' +
        //    '	<option value="onetime">One Time</option>' +
        //    '	<option value="schedule">Schedule</option>' +
        //    '</select>' +
        //      '          </td><td>' +
        //                 '   <input type="text" value="' + msg.Total_QTY + '" id="MainContent_grdPriceRequest_txtTotQTY_' + trCount + '" class="ddl" onkeypress="return isNumWithoutDecimalKey(event,this);" style="width: 80px;">' +
        //                '</td><td>' +
        //                '    <input type="text" value="' + msg.QTY_perOrder + '" id="MainContent_grdPriceRequest_txtQTYPO_' + trCount + '" class="ddl disabled" onkeypress="return isNumWithoutDecimalKey(event,this);" style="width: 80px;" disabled="">' +
        //                '</td><td>' +
        //                 '   <input type="text" value="' + msg.List_Price + '" readonly="readonly" id="MainContent_grdPriceRequest_txtDLP_' + trCount + '" class="ddl disabled" style="width: 80px;">' +
        //                '</td><td>' +
        //                 '   <input type="text" readonly="readonly" id="MainContent_grdPriceRequest_txtAP_' + trCount + '" class="ddl disabled" style="width: 80px;">' +
        //                '</td><td>' +
        //                 '   <input type="text" value="' + msg.Expected_price + '" id="MainContent_grdPriceRequest_txtTargetPrice_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" onkeyup="calDCrate(event,this);" style="width: 80px;">' +
        //                '    <input type="hidden" id="MainContent_grdPriceRequest_hdnAgreementPrice_' + trCount + '">' +
        //               '     <span id="MainContent_grdPriceRequest_lblPriceError_' + trCount + '" style="color: red;"></span>' +
        //              '  </td><td>' +
        //             '       <input type="text" value="' + msg.DC_rate + '" id="MainContent_grdPriceRequest_txtDCRate_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" onkeyup="calExpectedPrice(event,this);" style="width: 80px;">' +
        //            '    </td><td>' +
        //                  '  <input type="image" id="MainContent_grdPriceRequest_imgbtnAdd_' + trCount + '" onclick="add_row();" class="add-row" src="images/add.png" onclick="return false;" style="width: 25px;">' +
        //                 '   <input type="image" id="MainContent_grdPriceRequest_imgbtnDel_' + trCount + '" class="delbtn" src="images/delete.png" onclick="Deletepopup(event,this);" onclick="return false;" style="width: 25px;">' +
        //                '</td>' +
        //'</tr>";'
            markup = '<tr role="row" class="even">				<td>' +
                '         <input type="hidden" id="MainContent_grdPriceRequest_hdnItem_' + trCount + '" value="' + msg.Item_code + '" >' +
                '          <input type="hidden" id="MainContent_grdPriceRequest_hdnStockCode_' + trCount + '">' +
                '           <select id="MainContent_grdPriceRequest_ddlitem_' + trCount + '" onchange="ddl_item_change(this);" class="ddl_item" style="width:200px!important;"></select>' +
                '        </td><td>' +
                '             <input type="text" value="' + msg.WHS + '" readonly="readonly" id="MainContent_grdPriceRequest_txtWHS_' + trCount + '" class="ddl" style="width:70px;">' +
                ' <label  type="hidden"  id="MainContent_grdPriceRequest_lblMOQ_' + trCount + '" style="display:none;" class="ddl disabled">' +

                ' <label  type="hidden" id="MainContent_grdPriceRequest_lblIPACK_' + trCount + '" style="display:none;" class="ddl disabled"' +
                '          </td><td>' +
                //'             <label id="MainContent_grdPriceRequest_lblMOQ_' + trCount + '" class="ddl" style="width: 70px;  text-align: right; padding: 5px;">' +
                //'          </td><td>' +
                //'               <select id="MainContent_grdPriceRequest_ddlOrder_' + trCount + '" class="ddl" onchange="OrderChange(this);">' +
                //'	<option value="onetime">One Time</option>' +
                //'	<option value="schedule">Schedule</option>' +
                //'</select>' +
                //'          </td><td>' +
                // '               <select id="MainContent_grdPriceRequest_ddlFrequency_' + trCount + '" class="ddl disabled" disabled="">' +
                //'	<option value="weekly">Weekly</option>' +
                //'	<option value="fortnightly">Fortnightly</option>' +
                //'	<option value="monthly">Monthly</option>' +
                //'</select>' +
                //            '</td><td>' +
                '   <input type="text" value="' + msg.Total_QTY + '" id="MainContent_grdPriceRequest_txtTotQTY_' + trCount + '" class="ddl" onkeypress="return isNumWithoutDecimalKey(event,this);" style="width: 80px;">' +
                '</td><td>' +
                '  <a href="#mdAPList" rel="modal:open"> <input type="image" id="MainContent_grdPriceRequest_viewAPList_' + trCount + '" class="delbtn" src="images/info.jpg" onclick="ShowAPList(this);" onclick="return false;" style="width: 20px;"></a>' +
                '</td><td>' +

                //'    <input type="text" value="' + msg.QTY_perOrder + '" id="MainContent_grdPriceRequest_txtQTYPO_' + trCount + '" class="ddl disabled" onkeypress="return isNumWithoutDecimalKey(event,this);" style="width: 80px;" disabled="">' +
                //'</td><td>' +
                '   <input type="text" value="' + msg.List_Price + '" readonly="readonly" id="MainContent_grdPriceRequest_txtDLP_' + trCount + '" class="ddl disabled" style="width: 80px;">' +
                '</td><td>' +
                '   <input type="text" readonly="readonly" id="MainContent_grdPriceRequest_txtAP_' + trCount + '" class="ddl disabled" style="width: 80px;">' +
                '</td><td>' +
                //     '   <input type="text" value="' + msg.Expected_price + '" id="MainContent_grdPriceRequest_txtTargetPrice_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" onkeyup="calDCrate(event,this);" style="width: 80px;">' +
                //    '    <input type="hidden" id="MainContent_grdPriceRequest_hdnAgreementPrice_' + trCount + '">' +
                //   '     <span id="MainContent_grdPriceRequest_lblPriceError_' + trCount + '" style="color: red;"></span>' +
                //  '  </td><td>' +
                // '       <input type="text" value="' + msg.DC_rate + '" id="MainContent_grdPriceRequest_txtDCRate_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" onkeyup="calExpectedPrice(event,this);" style="width: 80px;">' +
                //'    </td><td>' +
                //'         <select id="MainContent_grdPriceRequest_ddlCustName_' + trCount + '" onchange="ddl_cust_change(this);" class="ddl customer" style="width: 200px;" ></select>' +
                //'          <input type="hidden" value="' + msg.Cust_number + '" id="MainContent_grdPriceRequest_hdnCustomers_' + trCount + '">' +
                //'           <span id="MainContent_grdPriceRequest_lblCustError_' + trCount + '" style="color: red;"></span>' +
                //'        </td><td>' +
                //'             <input type="text" value="' + msg.Cust_SP + '" id="MainContent_grdPriceRequest_txtCustSP_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" style="width: 80px;">' +
                //'          </td><td>' +
                // '               <select id="MainContent_grdPriceRequest_ddlCompanyName_' + trCount + '" class="ddl" style="width: 200px;">' +
                //'</select>' +
                //            '</td><td>' +
                //             '   <input type="text" id="MainContent_grdPriceRequest_txtDescription_' + trCount + '" class="ddl" style="width: 200px;">' +
                //            '</td><td>' +
                //             '   <input type="text" id="MainContent_grdPriceRequest_txtCompanySP_' + trCount + '" class="ddl" onkeypress="return isNumberKey(event,this);" style="width: 80px;">' +
                //            '</td><td>' +
                '  <input type="image" id="MainContent_grdPriceRequest_imgbtnAdd_' + trCount + '" onclick="add_row();" class="add-row" src="images/add.png" onclick="return false;" style="width: 25px;">' +
                '  <input type="image" id="MainContent_grdPriceRequest_imgbtnCopy_' + trCount + '" onclick="copy_row(event,this);" class="copy-row" src="images/copy.jpg" onclick="return false;" style="width: 25px;">' +
                '   <input type="image" id="MainContent_grdPriceRequest_imgbtnDel_' + trCount + '" class="delbtn" src="images/delete.png" onclick="Deletepopup(event,this);" onclick="return false;" style="width: 25px;">' +
                '</td>' +
                '</tr>";'
            tableBody = $("table tbody");
            tableBody.append(markup);
            //LoadDropdowns();
            $('#MainContent_grdPriceRequest_ddlOrder_' + trCount).val(msg.Order_type);

            LoadDropdownForNextRow(trCount);
        }
        function LoadDropdownForNextRow(i) {
            jQuery(".loader_div").show();
            var msg = sessionStorage.getItem("itemdata");
            BindItemSelect(JSON.parse(msg), i);
           
            //var ordertype = $("#MainContent_grdPriceRequest_ddlOrder_" + i).val();
            //if (ordertype == "schedule") {
            //    $("#MainContent_grdPriceRequest_txtQTYPO_" + i).removeClass('disabled');
            //    $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", false);
            //}
            //else {
            //    $("#MainContent_grdPriceRequest_txtQTYPO_" + i).addClass('disabled');
            //    $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", true);
            //}
            jQuery(".loader_div").hide();
        }

        function BindItemSelect(msg, i) {
            if (!$("#MainContent_grdPriceRequest_ddlitem_" + i).data('select2')) {
                $("#MainContent_grdPriceRequest_ddlitem_" + i).select2({
                    "width": "200px", "minimumInputLength": 3, data: msg, placeholder: "Select an item"
                });
                var str = $("#MainContent_grdPriceRequest_hdnItem_" + i).val();
                if (!IsnullOrEmpty(str)) {
                    $("#MainContent_grdPriceRequest_ddlitem_" + i).val(str);
                    $("#MainContent_grdPriceRequest_ddlitem_" + i).select2({ text: str, "minimumInputLength": 3, });
                }
                //var typ = $("#MainContent_grdPriceRequest_hdnType_" + i).val();
                //if (!IsnullOrEmpty(typ)) {
                //    $("#MainContent_grdPriceRequest_ddlOrder_" + i).val(typ);
                //    var qty_id = "MainContent_grdPriceRequest_txtQTYPO_" + i;
                //    if (typ == "schedule") {
                //        $('#' + qty_id).removeClass('disabled');
                //        $('#' + qty_id).prop("disabled", false);
                //    }
                //    else {
                //        $('#' + qty_id).addClass('disabled');
                //        $('#' + qty_id).prop("disabled", true);
                //        $('#' + qty_id).val("");
                //    }
                //}
            }
        }
        function BindItemDetails(data, id) {

            $('#' + id.replace('ddlitem', 'txtWHS')).val(data.d.WHS);
            $('#' + id.replace('ddlitem', 'txtDLP')).val(data.d.LP);
            $('#' + id.replace('ddlitem', 'hdnStockCode')).val(data.d.stockCode);
            $('#' + id.replace('ddlitem', 'lblMOQ')).val(data.d.quantity);
            $('#' + id.replace('ddlitem', 'lblIPACK')).val(data.d.IPACK);
            ShowMOQCondition(data.d.item, data.d.stockCode, id);
        }

        function BindItems(msg) {

            var totalRowCount = $("[id*=grdPriceRequest] tr").length;
            var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
            var item_id;
            var i;
            for (var trCount = 0; trCount < rowCount; trCount++) {
                item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(trCount).id;
                i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
                BindItemSelect(msg.d, i);
            }
            //for (var trCount = 0; trCount < rowCount; trCount++) {
            //    item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(trCount).id;
            //    i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
            //    if (!$("#MainContent_grdPriceRequest_ddlitem_" + i).data('select2')) {
            //        $("#MainContent_grdPriceRequest_ddlitem_" + i).select2({
            //            "width": "200px", "minimumInputLength": 3, data: msg.d, placeholder: "Select an item"
            //        });
            //        var str = $("#MainContent_grdPriceRequest_hdnItem_" + i).val();
            //        if (!IsnullOrEmpty(str)) {
            //            $("#MainContent_grdPriceRequest_ddlitem_" + i).val(str);
            //            $("#MainContent_grdPriceRequest_ddlitem_" + i).select2({ text: str, "minimumInputLength": 3, });
            //        }
            //    }
            //}

        }
        function ShowMOQCondition(item, stock, ddlidesc_id) {
            debugger;
            if (stock != "1" && stock != "6") {
                if (window.confirm(item + " may involve MOQ, do you want to proceed?")) {
                    // They clicked Yes
                }
                else {
                    //var item_id = ddlidesc_id.replace("ddlitem", "ddlitem");
                    var WHS_id = ddlidesc_id.replace("ddlitem", "txtWHS");
                    var LP_id = ddlidesc_id.replace("ddlitem", "txtDLP");
                    var AP_id = ddlidesc_id.replace("ddlitem", "txtAP");
                    $('#' + WHS_id).val("");
                    $('#' + LP_id).val("");
                    $('#' + AP_id).val("");
                    $("#" + ddlidesc_id).val("0");
                    $("#" + ddlidesc_id).select2({ text: "--Select--", "minimumInputLength": 3, });

                }
            }
        }

        function AddRowToGrid(data) {
            debugger;
            //$('#imgbtnAdd').click();

            if (data.d.length > 0) {
                var obj = JSON.parse(data.d);
                $('#MainContent_grdPriceRequest').DataTable().destroy();
                var head_content = $('#MainContent_grdPriceRequest tr:first').html();
                $('#MainContent_grdPriceRequest').prepend('<thead></thead>')
                $('#MainContent_grdPriceRequest thead').html('<tr>' + head_content + '</tr>');
                $('#MainContent_grdPriceRequest tbody tr:first').hide();
                $('#MainContent_grdPriceRequest').dataTable({
                    data: obj,
                    "columns": [
                    { "data": "ItemNumber" },
                    { "data": "ItemNumber" },
                    { "data": "WHS" },
                    { "data": "MQTY" },
                    { "data": "AQTY" },
                    { "data": "QTYPO" },
                    { "data": "Order" },
                    { "data": "DLP" },
                    { "data": "TargetPrice" },
                    { "data": "DCRate" },
                    { "data": "CustName" },
                    { "data": "CustSP" },
                    { "data": "CompanyName" },
                    { "data": "Description" },
                    { "data": "CompanySP" }
                    ]
                });

            }
        }

        function LoadPage() {
            debugger;
            LoadDropdowns();
            LoadTable();
        }

        function LoadTable() {
            if ($.fn.dataTable.isDataTable('#MainContent_grdPriceRequest')) {

            }
            else {
                var head_content = $('#MainContent_grdPriceRequest tr:first').html();
                $('#MainContent_grdPriceRequest').prepend('<thead></thead>')
                $('#MainContent_grdPriceRequest thead').html('<tr>' + head_content + '</tr>');
                $('#MainContent_grdPriceRequest tbody tr:first').hide();
                var table = $('#MainContent_grdPriceRequest').dataTable({
                    "ordering": false,
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    dom: 'lBfrtip',
                    //dom: 'Bfrtip',
                    buttons: [
                        //'copy', 'csv',
                        //'excel', 'pdf', 'print'
                    ],
                    //"scrollY": 200,
                    "scrollX": true
                });
            }
        }
        function LoadDropdowns() {
            debugger;
            jQuery(".loader_div").show();
            $.ajax({
                url: 'QuoteRequest.aspx/LoadItems',
                method: 'post',
                datatype: 'json',
                data: '',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    sessionStorage.setItem("itemdata", JSON.stringify(data.d));
                    BindItems(data);
                    jQuery(".loader_div").hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                    jQuery(".loader_div").hide();
                }
            });

            //$.ajax({
            //    url: 'QuoteRequest.aspx/LoadCompetitors',
            //    method: 'post',
            //    datatype: 'json',
            //    data: '',
            //    contentType: "application/json; charset=utf-8",
            //    success: function (msg) {
            //        LoadCompetitor(msg);
            //    },
            //    error: function (xhr, ajaxOptions, thrownError) {
            //        alert(xhr.responseText);
            //    }
            //});
            var totalRowCount = $("[id*=grdPriceRequest] tr").length;
            var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
            var item_id;
            var i;
            for (var trCount = 0; trCount < rowCount; trCount++) {
                item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(trCount).id;

                i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length);
                var ordertype = $("#MainContent_grdPriceRequest_ddlOrder_" + i).val();
                if (ordertype == "schedule") {
                    $("#MainContent_grdPriceRequest_ddlFrequency_" + i).removeClass('disabled');
                    $("#MainContent_grdPriceRequest_ddlFrequency_" + i).prop("disabled", false);
                    $("#MainContent_grdPriceRequest_txtQTYPO_" + i).removeClass('disabled');
                    $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", false);
                }
                else {
                    $("#MainContent_grdPriceRequest_ddlFrequency_" + i).addClass('disabled');
                    $("#MainContent_grdPriceRequest_ddlFrequency_" + i).prop("disabled", true);
                    $("#MainContent_grdPriceRequest_txtQTYPO_" + i).addClass('disabled');
                    $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", true);
                }
            }
            //var totalRowCount = $("[id*=grdPriceRequest] tr").length;
            //var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
            //for (var i = 0; i < rowCount; i++) {
            //    $("#MainContent_grdPriceRequest_ddlitem_" + i).select2({ "width": "110px" });
            //    $("#MainContent_grdPriceRequest_ddlitemdesc_" + i).select2({ "width": "200px" });

            //    var ordertype = $("#MainContent_grdPriceRequest_ddlOrder_" + i).val();
            //    if (ordertype == "schedule") {
            //        $("#MainContent_grdPriceRequest_ddlFrequency_" + i).removeClass('disabled');
            //        $("#MainContent_grdPriceRequest_ddlFrequency_" + i).prop("disabled", false);
            //        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).removeClass('disabled');
            //        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", false);
            //    }
            //    else {
            //        $("#MainContent_grdPriceRequest_ddlFrequency_" + i).addClass('disabled');
            //        $("#MainContent_grdPriceRequest_ddlFrequency_" + i).prop("disabled", true);
            //        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).addClass('disabled');
            //        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).prop("disabled", true);
            //    }
            //}
        }
        function ShowAPList(obj) {
            debugger;
            var cust_number = $('#MainContent_ddlCustomers').val();
            var id = obj.id;
            var item_value = $("#" + id.replace('viewAPList', 'hdnItem')).val();
            $.ajax({
                url: 'QuoteRequest.aspx/LoadAPList',
                method: 'post',
                datatype: 'json',
                data: '{"item":"' + item_value + '", "cust_number":"' + cust_number + '"}',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    debugger;
                    console.log(data);
                    console.log(JSON.stringify(data));
                    sessionStorage.setItem('APList', JSON.stringify(data));
                    LoadAPTable(data.d);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });
        }
        function LoadAPTable(data) {
            debugger;
            var markup = '<table id="tblAPList"><tr><th>Break Quantity</th><th>Agreement Price</th><th>Valid From (YYYYMMDD)</th><th>Valid To (YYYYMMDD)</th></tr>';
            for (var i = 0; i < data.length; i++)
                markup += '<tr><td>' +
                    data[i].quantity
                    + '</td><td>' +
                    data[i].AP + '</td><td>' +
                    data[i].Valid_from + '</td><td>' +
                    data[i].Valid_to + '</td></tr>';
            markup += '</table>'
            var tableBody = $("#mdAPList");
            tableBody.empty();
            tableBody.append(markup);
        }
        function LoadCompetitor(msg) {
            debugger;
            var totalRowCount = $("[id*=grdPriceRequest] tr").length;
            var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
            var item_id;
            var i;
            for (var trCount = 0; trCount < rowCount; trCount++) {
                item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(trCount).id;

                i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
                var str = $("#MainContent_grdPriceRequest_ddlCompanyName_" + i).val();
                if ($("#MainContent_grdPriceRequest_ddlCompanyName_" + i) != undefined)
                    $("#MainContent_grdPriceRequest_ddlCompanyName_" + i).select2({ data: msg.d });
                if (!IsnullOrEmpty(str)) {
                    $("#MainContent_grdPriceRequest_ddlCompanyName_" + i).val(str);
                    $("#MainContent_grdPriceRequest_ddlCompanyName_" + i).select2({ text: str });
                }
            }
        }

        document.addEventListener("DOMContentLoaded", function () {
            debugger;
            var tabs = document.querySelectorAll('.tabbed li');
            var switchers = document.querySelectorAll('.switcher-box a');
            var skinable = document.getElementById('skinable');

            for (var i = 0, len = tabs.length; i < len; i++) {
                tabs[i].addEventListener("click", function () {
                    if (this.classList.contains('active')) {
                        debugger;
                        return;
                    }
                    var parent = this.parentNode,
                        innerTabs = parent.querySelectorAll('li');

                    for (var index = 0, iLen = innerTabs.length; index < iLen; index++) {
                        innerTabs[index].classList.remove('active');
                    }

                    this.classList.add('active');
                });
            }

            for (var i = 0, len = switchers.length; i < len; i++) {
                switchers[i].addEventListener("click", function () {
                    if (this.classList.contains('active'))
                        return;

                    var parent = this.parentNode,
                        innerSwitchers = parent.querySelectorAll('a'),
                        skinName = this.getAttribute('skin');

                    for (var index = 0, iLen = innerSwitchers.length; index < iLen; index++) {
                        innerSwitchers[index].classList.remove('active');
                    }
                    this.classList.add('active');
                    skinable.className = 'tabbed round ' + skinName;
                });
            }
        });

        function tabchange(e) {
            debugger;
            if (e.id == "MainContent_freqList") {
                $('#MainContent_divFrequent').css("display", "block");
                $('#MainContent_divRecent').css("display", "none");
                $('#MainContent_divRecent').removeClass("active");
                $('#MainContent_divFrequent').addClass("active");
            }
            else {
                $('#MainContent_divFrequent').css("display", "none");
                $('#MainContent_divRecent').css("display", "block");
                $('#MainContent_divRecent').addClass("active");
                $('#MainContent_divFrequent').removeClass("active");
            }

        }

        function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;
                if (charCode == 46) return false;
            }
            else {
                if (value.length > 10) {
                    if (charCode == 46) return true;
                    else return false;
                }
            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function isNumWithoutDecimalKey(evt, obj) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;
                if (charCode == 46) return false;
            }
            else {
                if (value.length > 10) {
                    //if (charCode == 46) return true;
                    //else
                    return false;
                }
            }
            if (charCode == 46) return false;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function calculateYearlyQty(evt, obj) {
            var value = obj.value;
            if (value != undefined && value != '') {
                var mqty_id = obj.id;
                var aqty_id = mqty_id.replace("txtMQTY", "txtAQTY");
                var actual_id = "#" + aqty_id;
                $(actual_id).val(value * 12);
            }
        }
        function calculateMonthlyQty(evt, obj) {
            var value = obj.value;
            if (value != undefined && value != '') {
                var mqty_id = obj.id;
                var aqty_id = mqty_id.replace("txtAQTY", "txtMQTY");
                var actual_id = "#" + aqty_id;
                $(actual_id).val(value / 12);
            }
        }
        function calDCrate(evt, obj) {
            //var value = obj.value;
            //if (value != undefined && value != '') {
            //    var ep_id = obj.id;
            //    var lp_id = ep_id.replace("txtTargetPrice", "txtDLP");
            //    var dc_id = ep_id.replace("txtTargetPrice", "txtDCRate");
            //    var actual_lp_id = "#" + lp_id;
            //    var actual_dc_id = "#" + dc_id;
            //    var listprice = $(actual_lp_id).val();
            //    var rate = (listprice - value) * 100 / listprice;
            //    console.log(rate);
            //    $(actual_dc_id).val(parseFloat(rate).toFixed(2));
            //}
            var value = obj.value;
            var ep_id = obj.id;
            var lp_id = ep_id.replace("txtTargetPrice", "txtDLP");
            var dc_id = ep_id.replace("txtTargetPrice", "txtDCRate");
            if (value != undefined && value != '') {
                var actual_lp_id = "#" + lp_id;
                var actual_dc_id = "#" + dc_id;
                var listprice = $(actual_lp_id).val();
                var rate = (listprice - value) * 100 / listprice;
                $(actual_dc_id).val(parseFloat(rate).toFixed(2));

                $('#' + dc_id).attr("disabled", "true");
            }
            else {
                $('#' + dc_id).val('');
                $('#' + dc_id).removeAttr("disabled");
            }
        }

        function calExpectedPrice(evt, obj) {
            debugger;
            //var value = obj.value;
            //if (value != undefined && value != '') {
            //    var dc_id = obj.id;
            //    var lp_id = dc_id.replace("txtDCRate", "txtDLP");
            //    var ep_id = dc_id.replace("txtDCRate", "txtTargetPrice");
            //    var actual_lp_id = "#" + lp_id;
            //    var actual_ep_id = "#" + ep_id;
            //    var listprice = $(actual_lp_id).val();
            //    var targetprice = listprice * (1 - (value / 100));
            //    console.log(targetprice);
            //    $(actual_ep_id).val(parseFloat(targetprice).toFixed(2));
            //}
            var value = obj.value;
            var dc_id = obj.id;
            var lp_id = dc_id.replace("txtDCRate", "txtDLP");
            var ep_id = dc_id.replace("txtDCRate", "txtTargetPrice");
            if (value != undefined && value != '') {
                var actual_lp_id = "#" + lp_id;
                var actual_ep_id = "#" + ep_id;
                var listprice = $(actual_lp_id).val();
                var targetprice = listprice * (1 - (value / 100));
                $(actual_ep_id).val(parseFloat(targetprice).toFixed(2));
                $('#' + ep_id).attr("disabled", "true");
            }
            else {
                $('#' + ep_id).val('');
                $('#' + ep_id).removeAttr("disabled");
            }
        }
        function Deletepopup(evt, obj) {
            debugger;
            //var id = obj.id;
            //var item_id = id.replace("imgbtnDel", "ddlitem");
            //var item = $("#" + item_id + " option:selected").text().replace(/\s+/g, " ");
            //if (!confirm("Do you want to delete " + item + "?")) { return false; }
            //else
            //    return true;
            var id = obj.id;
            var item_id = id.replace("imgbtnDel", "ddlitem");
            var item = $("#" + item_id + " option:selected").text().replace(/\s+/g, " ");
            if (!confirm("Do you want to delete " + item + "?")) { return false; }
            else {
                //return true;
                $.ajax({
                    url: 'QuoteRequest.aspx/DeleteQuote',
                    method: 'post',
                    datatype: 'json',
                    data: '{"item":"' + item + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        var $row = jQuery(obj).closest('tr');
                        $row.remove();
                        return true;
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            }
        }

        function validateFields(evt, obj) {
            debugger;
            var table = $('#MainContent_grdPriceRequest');
            var errFlag = 0;
            var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
            var i;
            var item_id;
            for (var trCount = 0; trCount < rowCount; trCount++) {
                item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(trCount).id;

                i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)

                if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlitem_" + i).val()) || $("#MainContent_grdPriceRequest_ddlitem_" + i).val() == "0") {
                    $("#MainContent_grdPriceRequest_ddlitem_" + i).css("border", "1px solid red");

                    $('.select2-container--default .select2-selection--single').css({ "border": "1px solid red;!important" });
                    $("#MainContent_lblmessage").text("Item selection is mandatory");
                    errFlag++;
                }
                else {
                    $("#MainContent_lblmessage").text("");
                    $('.select2-container').css({ "border": "1px solid #aaa;!important" });
                    if (parseInt($("#MainContent_grdPriceRequest_lblMOQ_" + i).val()) > 0) {
                        $("#MainContent_lblmessage").text("");
                        //if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlitemdesc_" + i).val())) {
                        //    $("#MainContent_grdPriceRequest_ddlitemdesc_" + i).css("border", "1px solid red");
                        //    errFlag++;
                        //}
                        //else {
                        //    $("#MainContent_grdPriceRequest_ddlitemdesc_" + i).css("border", "");
                        //}
                        //if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtTotQTY_" + i).val())) {
                        //    $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "1px solid red");
                        //    errFlag++;
                        //}
                        //else {
                        //    $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "");
                        //}

                        //var ordertype = $("#MainContent_grdPriceRequest_ddlOrder_" + i).val();
                        //if (ordertype == "schedule") {
                        //    if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtQTYPO_" + i).val())) {
                        //        $("#MainContent_grdPriceRequest_txtQTYPO_" + i).css("border", "1px solid red");
                        //        errFlag++;
                        //    }
                        //    else {
                        //        if (parseInt($("#MainContent_grdPriceRequest_txtQTYPO_" + i).val()) > parseInt($("#MainContent_grdPriceRequest_txtTotQTY_" + i).val())) {
                        //            $("#MainContent_grdPriceRequest_txtQTYPO_" + i).css("border", "1px solid red");
                        //            errFlag++;
                        //        }
                        //        else {
                        //            if (parseInt($("#MainContent_grdPriceRequest_txtQTYPO_" + i).val()) < parseInt($("#MainContent_grdPriceRequest_lblMOQ_" + i).text())) {
                        //                $("#MainContent_grdPriceRequest_txtQTYPO_" + i).css("border", "1px solid red");
                        //                $("#MainContent_lblmessage").text("Requested quantity should be greater than minimum quantity to RFQ.");
                        //                errFlag++;
                        //            }
                        //            else {
                        //                $("#MainContent_lblmessage").text("");
                        //                $("#MainContent_grdPriceRequest_txtQTYPO_" + i).css("border", "");
                        //            }
                        //        }

                        //    }
                        //}
                        //else {
                        if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtTotQTY_" + i).val())) {
                            $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "1px solid red");
                            errFlag++;
                        }
                        else {

                            if (parseInt($("#MainContent_grdPriceRequest_txtTotQTY_" + i).val()) < parseInt($("#MainContent_grdPriceRequest_lblMOQ_" + i).val())) {
                                $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "1px solid red");
                                $("#MainContent_lblmessage").text("Requested quantity should be greater than " + $("#MainContent_grdPriceRequest_lblMOQ_" + i).val() + " to RFQ.");
                                errFlag++;
                            }
                            else {
                                if (parseInt($("#MainContent_grdPriceRequest_lblMOQ_" + i).val()) == 0) {
                                    $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "1px solid red");
                                    $("#MainContent_lblmessage").text("RFQ is not allowed for the selected item.");
                                    errFlag++;
                                }
                                else {
                                    if (parseInt($("#MainContent_grdPriceRequest_txtTotQTY_" + i).val()) % parseInt($("#MainContent_grdPriceRequest_lblIPACK_" + i).val()) == 0) {
                                        $("#MainContent_lblmessage").text("");
                                        $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "");
                                    }
                                    else {
                                        $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "1px solid red");
                                        $("#MainContent_lblmessage").text("Quantity per order should be multiple of " + $("#MainContent_grdPriceRequest_lblIPACK_" + i).val() + ".");
                                        errFlag++;
                                    }
                                }
                            }

                        }
                        /*   }*/

                        //if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtTargetPrice_" + i).val())) {
                        //    $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "1px solid red");
                        //    errFlag++;
                        //}
                        //else {
                        //    $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "");
                        //    var tp = $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).val();
                        //    var ap = $("#MainContent_grdPriceRequest_hdnAgreementPrice_" + i).val();
                        //    if (!IsnullOrEmpty(ap) && ap > 0) {
                        //        if (tp >= ap) {
                        //            $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "1px solid red");
                        //            errFlag++;
                        //            $("#MainContent_grdPriceRequest_lblPriceError_" + i).text("Expected price should be less than the agreement price.");

                        //        }
                        //        else {
                        //            $("#MainContent_grdPriceRequest_lblPriceError_" + i).text("");
                        //            $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "");
                        //        }
                        //    }
                        //    else {
                        //        $("#MainContent_grdPriceRequest_lblPriceError_" + i).text("");
                        //        $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "");
                        //    }
                        //}
                        //if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtDCRate_" + i).val())) {
                        //    $("#MainContent_grdPriceRequest_txtDCRate_" + i).css("border", "1px solid red");
                        //    errFlag++;
                        //}
                        //else {
                        //    $("#MainContent_grdPriceRequest_txtDCRate_" + i).css("border", "");
                        //}
                        //if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlCustName_" + i).val())) {
                        //    //$("#MainContent_grdPriceRequest_ddlCustName_" + i).css("border", "1px solid red");
                        //    //$("#MainContent_grdPriceRequest_lblCustError_" + i).text("Required");
                        //    //errFlag++;
                        //}
                        //else {
                        //    $("#MainContent_grdPriceRequest_ddlCustName_" + i).css("border", "");
                        //    var cust = $("#MainContent_grdPriceRequest_ddlCustName_" + i).val();
                        //    $("#MainContent_grdPriceRequest_lblCustError_" + i).text("");
                        //    $("#MainContent_grdPriceRequest_hdnCustomers_" + i).val(cust.toString());
                        //}
                    }
                    else {
                        $("#MainContent_lblmessage").text("RFQ is not allowed for the selected item.");
                        errFlag++;
                    }
                }

            }
            if (errFlag > 0) {
                return false;
            }
            else {
                return true;
            }
        }
        //function validateFields(evt, obj) {
        //    debugger;
        //    var table = $('#MainContent_grdPriceRequest');
        //    var errFlag = 0;
        //    var rowCount = $("[id*=grdPriceRequest] td").closest("tr").length;
        //    var i;
        //    var item_id;
        //    for (var trCount = 0; trCount < rowCount; trCount++) {
        //        item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(trCount).id;
        //        i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)

        //        if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlitem_" + i).val())) {
        //            $("#MainContent_grdPriceRequest_ddlitem_" + i).css("border", "1px solid red");

        //            $('.select2-container').css({ "border": "1px solid red;" });

        //            errFlag++;
        //        }
        //        else {
        //            $('.select2-container').css({ "border": "1px solid #aaa;" });
        //        }
        //        //if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlitemdesc_" + i).val())) {
        //        //    $("#MainContent_grdPriceRequest_ddlitemdesc_" + i).css("border", "1px solid red");
        //        //    errFlag++;
        //        //}
        //        //else {
        //        //    $("#MainContent_grdPriceRequest_ddlitemdesc_" + i).css("border", "");
        //        //}

        //        if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtTotQTY_" + i).val())) {
        //            $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "1px solid red");
        //            errFlag++;
        //        }
        //        else {
        //            $("#MainContent_grdPriceRequest_txtTotQTY_" + i).css("border", "");
        //        }

        //        var ordertype = $("#MainContent_grdPriceRequest_ddlOrder_" + i).val();
        //        if (ordertype == "schedule") {
        //            if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtQTYPO_" + i).val())) {
        //                $("#MainContent_grdPriceRequest_txtQTYPO_" + i).css("border", "1px solid red");
        //                errFlag++;
        //            }
        //            else {
        //                $("#MainContent_grdPriceRequest_txtQTYPO_" + i).css("border", "");
        //            }
        //        }
        //        if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtTargetPrice_" + i).val())) {
        //            $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "1px solid red");
        //            errFlag++;
        //        }
        //        else {
        //            $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "");
        //            var tp = $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).val();
        //            var ap = $("#MainContent_grdPriceRequest_hdnAgreementPrice_" + i).val();
        //            if (!IsnullOrEmpty(ap) && ap > 0) {
        //                if (tp >= ap) {
        //                    $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "1px solid red");
        //                    errFlag++;
        //                    $("#MainContent_grdPriceRequest_lblPriceError_" + i).text("Expected price should be equal to or less than the agreement price.");

        //                }
        //                else {
        //                    $("#MainContent_grdPriceRequest_lblPriceError_" + i).text("");
        //                    $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "");
        //                }
        //            }
        //            else {
        //                $("#MainContent_grdPriceRequest_lblPriceError_" + i).text("");
        //                $("#MainContent_grdPriceRequest_txtTargetPrice_" + i).css("border", "");
        //            }
        //        }
        //        if (IsnullOrEmpty($("#MainContent_grdPriceRequest_txtDCRate_" + i).val())) {
        //            $("#MainContent_grdPriceRequest_txtDCRate_" + i).css("border", "1px solid red");
        //            errFlag++;
        //        }
        //        else {
        //            $("#MainContent_grdPriceRequest_txtDCRate_" + i).css("border", "");
        //        }
        //        //if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlCustName_" + i).val())) {
        //        //    $("#MainContent_grdPriceRequest_ddlCustName_" + i).css("border", "1px solid red");
        //        //    errFlag++;
        //        //}
        //        //else {
        //        //    $("#MainContent_grdPriceRequest_ddlCustName_" + i).css("border", "");
        //        //}
        //        //if (IsnullOrEmpty($("#MainContent_grdPriceRequest_ddlCompanyName_" + i).val())) {
        //        //    $("#MainContent_grdPriceRequest_ddlCompanyName_" + i).css("border", "1px solid red");
        //        //    errFlag++;
        //        //}
        //        //else {
        //        //    $("#MainContent_grdPriceRequest_ddlCompanyName_" + i).css("border", "");
        //        //}
        //    }
        //    if (errFlag > 0) {
        //        return false;
        //    }
        //    else {
        //        return true;
        //    }
        //}
        function IsnullOrEmpty(val) {
            if (val != '' && val != undefined && val != '--Select--')
                return false;
            else
                return true;
        }

        function OrderChange(e) {

            var ordertype = e.options[e.selectedIndex].value;
            var id = e.id;
            var feq_id = id.replace("ddlOrder", "ddlFrequency");
            var qty_id = id.replace("ddlOrder", "txtQTYPO");
            if (ordertype == "schedule") {
                $('#' + feq_id).removeClass('disabled');
                $('#' + feq_id).prop("disabled", false);
                $('#' + qty_id).removeClass('disabled');
                $('#' + qty_id).prop("disabled", false);
            }
            else {
                $('#' + feq_id).addClass('disabled');
                $('#' + feq_id).prop("disabled", true);
                $('#' + qty_id).addClass('disabled');
                $('#' + qty_id).prop("disabled", true);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     
    <div class="crumbs">
        <!-- Start : Breadcrumbs -->
        <ul id="Ul1" class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a>Quote</a>
            </li>
            <li class="current">Request For Quote</li>
        </ul>
    </div>
    <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
    <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">
        <%--<asp:Panel runat="server" ID="pnlData">--%>
        <ContentTemplate>
            <div class="col-md-12 mn_margin">
                <div id="collapsebtn" class="row">
                    <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />
                </div>
                <div class="row filter_panel" id="reportdrpdwns" runat="server">
                    <div runat="server" id="cterDiv" visible="false">
                        <ul class="btn-info rbtn_panel">
                            <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>

                                <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                                <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                                <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            </li>
                        </ul>
                    </div>


                    <div class="col-md-12 nopadding">

                        <div class="col-md-2 control">
                            <label class="label ">Reference</label>
                            <asp:TextBox ID="txtRef" CssClass="control_dropdown" runat="server"></asp:TextBox>

                        </div>
                        <div class="col-md-2 control">
                            <label class="label ">Branch</label>
                            <asp:DropDownList ID="ddlBranch" runat="server" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                                <asp:ListItem>ALL</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="col-md-2 control">
                            <label class="label ">Sales Engineer</label>
                            <asp:DropDownList ID="ddlSE" runat="server" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlSE_SelectedIndexChanged">
                                <asp:ListItem>ALL</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="col-md-2 control">
                            <label class="label">Customer Class </label>
                            <asp:DropDownList ID="ddlCustomerClass" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCustomerClass_SelectedIndexChanged"
                                CssClass="control_dropdown">
                                <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                <asp:ListItem Value="KA">KEY FOCUSED CUSTOMER</asp:ListItem>
                                <asp:ListItem Value="KF">‘A’ CLASS CUSTOMER</asp:ListItem>
                                <asp:ListItem Value="OT"> OTHERS</asp:ListItem>
                            </asp:DropDownList>


                        </div>
                        <div class="col-md-2 control">
                            <label class="label">Customer</label>
                            <asp:DropDownList ID="ddlCustomers" runat="server" CssClass="control_dropdown">
                            </asp:DropDownList>

                        </div>
                        <div class="col-md-2 btncontrol">
                            <asp:Button runat="server" ID="btnFilter" CssClass="btn green" Text="Filter" OnClick="btnFilter_Click" />

                            <asp:Button runat="server" ID="Button1" CssClass="btn green btnSubmit" Text="Save" OnClientClick="return validateFields(event,this);" Style="float: right;" />
                        </div>
                    </div>



                </div>
                <%--<asp:Panel ID="panelref" runat="server" CssClass="filter_panel" Visible="false">
                    <div class="col-md-12 nopadding">
                        <div class="col-md-1">
                            <asp:Label ID="lblRef" runat="server" Text="Reference"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <div class="controls">
                                <asp:TextBox ID="txtRef1" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <asp:Button runat="server" ID="btnSave" CssClass="btnSubmit" Text="Save" OnClientClick="return validateFields(event,this);" OnClick="btnSave_Click" />
                        </div>
                    </div>
                </asp:Panel>--%>

                <div class="row" runat="server" style="margin-top: 1%">
                    <asp:Label runat="server" ID="lblmessage"></asp:Label>

                    <asp:GridView ID="grdPriceRequest" CssClass="display compact" runat="server" AutoGenerateColumns="false" Width="100%">

                        
                    <Columns>

                        <asp:TemplateField HeaderText="Item Code">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdnItem" Value='<%#Bind("ItemNumber") %>' />
                                <asp:HiddenField runat="server" ID="hdnStockCode" />
                                <asp:DropDownList CssClass="ddl_item" ID="ddlitem" runat="server" Style="width: 200px!important;"></asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderWHS" Text="WHS" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtWHS" ReadOnly="true" Text='<%#Bind("WHS") %>' CssClass="ddl disabled" runat="server" Width="70px"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="lblMOQ" Value='<%#Bind("MOQ") %>'  />
                                <asp:HiddenField runat="server" ID ="lblIPACK" />
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderAQTY" Text="QTY Per Order" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtTotQTY" Text='<%#Bind("Total_QTY") %>' onkeypress="return isNumWithoutDecimalKey(event,this);" CssClass="ddl qty" runat="server" Style="width: 80px;"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <a href="#mdAPList" rel="modal:open">
                                    <asp:Image ID="viewAPList" runat="server" onclick="ShowAPList(this);" src="images/info.jpg" Style="width: 20px;" /></a>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderDLP" Text="List Price" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtDLP" ReadOnly="true" Text='<%#Bind("DLP") %>' CssClass="ddl disabled" runat="server" Style="width: 80px;"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHeaderAP" Text="Agreement Price" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtAP" ReadOnly="true" CssClass="ddl disabled" runat="server" Style="width: 80px;"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label Text="Actions" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:ImageButton OnClientClick="return false;" CssClass="add-row" runat="server" Style="width: 25px;" ID="imgbtnAdd" ImageUrl="images/add.png" />
                                <asp:ImageButton OnClientClick="return false;" CssClass="copy-row" runat="server" Style="width: 25px;" ID="imgbtnCopy" ImageUrl="images/copy.jpg" />
                                <%--<asp:ImageButton OnClientClick="return validateFields(event,this);" CssClass="add-row" CommandName="AddItem" CommandArgument='<%# Eval("ItemNumber") %>' runat="server" Style="width: 25px;" ID="imgbtnAdd" ImageUrl="images/add-icon.jpeg" />--%>
                                <%--<asp:ImageButton CommandName="Delete" OnClientClick="return Deletepopup(event,this);" runat="server" Style="width: 25px;" ID="imgbtnDel" ImageUrl="images/delete.png" CssClass="delbtn" />--%>
                                <asp:ImageButton OnClientClick="return false;" runat="server" Style="width: 25px;" ID="imgbtnDel" ImageUrl="images/delete.png" CssClass="delbtn" />
                                </div>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                 <%--       <Columns>

                            <asp:TemplateField HeaderText="Item Code">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnItem" runat="server" />
                                     <asp:HiddenField runat="server" ID="hdnStockCode" />
                                    <asp:DropDownList ID="ddlitem" runat="server" CssClass="ddl_item" Style="width: 200px!important;"></asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderWHS" Text="WHS" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtWHS" ReadOnly="true" Text='<%#Bind("WHS") %>' CssClass="ddl" runat="server" Style="width: 70px;"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderOrder" Text="Order Type" runat="server" CssClass="required"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlOrder" runat="server" CssClass="ddl" onchange="OrderChange(this);">
                                        <asp:ListItem Text="One Time" Value="onetime"></asp:ListItem>
                                        <asp:ListItem Text="Schedule" Value="schedule"></asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderAQTY" Text="Total QTY" runat="server" CssClass="required"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtTotQTY" Text='<%#Bind("Total_QTY") %>' onkeypress="return isNumWithoutDecimalKey(event,this);" CssClass="ddl qty" runat="server" Style="width: 80px;"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField>
                            <ItemTemplate>
                                 <a href="#mdAPList" rel="modal:open">
                                    <asp:Image ID="viewAPList" runat="server" onclick="ShowAPList(this);" src="images/info.jpg" Style="width: 20px;" /></a>
                            
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderQTYPO" Text="QTY Per Order" runat="server" CssClass="required"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtQTYPO" Text='<%#Bind("QTYPO") %>' onkeypress="return isNumWithoutDecimalKey(event,this);" CssClass="ddl" runat="server" Style="width: 80px;"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderDLP" Text="List Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtDLP" ReadOnly="true" Text='<%#Bind("DLP") %>' CssClass="ddl disabled" runat="server" Style="width: 80px;"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderAP" Text="Agreement Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtAP" ReadOnly="true" CssClass="ddl disabled" runat="server" Style="width: 80px;"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderTargetPrice" Text="Expected Price" runat="server" CssClass="required"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtTargetPrice" Text='<%#Bind("TargetPrice") %>' onkeypress="return isNumberKey(event,this);" onkeyup="calDCrate(event,this);" CssClass="ddl ep" runat="server" Style="width: 80px;"></asp:TextBox>
                                    <asp:HiddenField ID="hdnAgreementPrice" runat="server" />
                                    <asp:Label ID="lblPriceError" runat="server" Style="color: red;" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderDCRate" Text="Discount Rate(%)" runat="server" CssClass="required"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtDCRate" Text='<%#Bind("DCRate") %>' onkeypress="return isNumberKey(event,this);" onkeyup="calExpectedPrice(event,this);" CssClass="ddl" runat="server" Style="width: 80px;"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                           <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label1" Text="Actions" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton OnClientClick="return false;" CssClass="add-row" runat="server" Style="width: 25px;" ID="imgbtnAdd" ImageUrl="images/add.png" />
                                <asp:ImageButton OnClientClick="return false;" runat="server" Style="width: 25px;" ID="imgbtnDel" ImageUrl="images/delete.png" CssClass="delbtn" />

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>--%>
                    </asp:GridView>
                    <div id="divOrders" runat="server" style="display: none;">
                        <div class="tabbed skin-turquoise round" id="skinable" style="margin-bottom: 10px;">
                            <ul>
                                <li id="freqList" runat="server" class="active" onclick="tabchange(this);">Frequently Ordered</li>
                                <li id="recentList" runat="server" onclick="tabchange(this);">Recently Ordered</li>
                            </ul>
                        </div>

                        <div class="col-md-12 nopad" id="divFrequent" runat="server" style="display: block">
                            <div class="col-md-1"></div>
                            <div class="col-md-2 order" id="divItem1" style="cursor: pointer;">
                                <asp:LinkButton ID="lnkItem1" runat="server" CssClass="lnk_item" OnClientClick="return false;">
                                    <div class="divorder">
                                        <div class="text-info mt-3">
                                            <h5>Item : <strong>
                                                <asp:Label ID="lblItem1" runat="server"></asp:Label></strong></h5>
                                        </div>
                                        <div class="text-info mt-2">
                                            <h4><strong>
                                                <asp:Label ID="lblDesc1" runat="server"></asp:Label></strong></h4>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div></div>
                            <div class="col-md-2 order" id="divItem2" style="cursor: pointer;">
                                <asp:LinkButton ID="lnkItem2" runat="server" CssClass="lnk_item" OnClientClick="return false;">
                                    <div class="divorder">
                                        <div class="text-info mt-3">
                                            <h5>Item : <strong>
                                                <asp:Label ID="lblItem2" runat="server"></asp:Label></strong></h5>
                                        </div>
                                        <div class="text-info mt-2">
                                            <h4><strong>
                                                <asp:Label ID="lblDesc2" runat="server"></asp:Label></h4>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div class="col-md-2 order" id="divItem3" style="cursor: pointer;">
                                <asp:LinkButton ID="lnkItem3" runat="server" CssClass="lnk_item" OnClientClick="return false;">
                                    <div class="divorder">
                                        <div class="text-info mt-3">
                                            <h5>Item : <strong>
                                                <asp:Label ID="lblItem3" runat="server"></asp:Label></strong></h5>
                                        </div>
                                        <div class="text-info mt-2">
                                            <h4><strong>
                                                <asp:Label ID="lblDesc3" runat="server"></asp:Label></strong></h4>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div class="col-md-2 order" id="divItem4" style="cursor: pointer;">
                                <asp:LinkButton ID="lnkItem4" runat="server" CssClass="lnk_item" OnClientClick="return false;">
                                    <div class="divorder">
                                        <div class="text-info mt-3">
                                            <h5>Item : <strong>
                                                <asp:Label ID="lblItem4" runat="server"></asp:Label></strong></h5>
                                        </div>
                                        <div class="text-info mt-2">
                                            <h4><strong>
                                                <asp:Label ID="lblDesc4" runat="server"></asp:Label></strong></h4>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div class="col-md-2 order" id="divItem5" style="cursor: pointer;">
                                <asp:LinkButton ID="lnkItem5" runat="server" CssClass="lnk_item" OnClientClick="return false;">
                                    <div class="divorder">
                                        <div class="text-info mt-3">
                                            <h5>Item : <strong>
                                                <asp:Label ID="lblItem5" runat="server"></asp:Label></strong></h5>
                                        </div>
                                        <div class="text-info mt-2">
                                            <h4><strong>
                                                <asp:Label ID="lblDesc5" runat="server"></asp:Label></strong></h4>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div class="col-md-1"></div>
                        </div>

                        <div class="col-md-12 nopad" id="divRecent" runat="server" style="display: none">
                            <div class="col-md-1"></div>
                            <div class="col-md-2 order" id="divOItem1" style="cursor: pointer;">
                                <asp:LinkButton ID="lnkOItem1" runat="server" CssClass="lnk_Ritem" OnClientClick="return false;">
                                    <div class="divorder">
                                        <div class="text-info mt-3">
                                            <h5>Item : <strong>
                                                <asp:Label ID="lblOItem1" runat="server"></asp:Label></strong></h5>
                                        </div>
                                        <div class="text-info mt-2">
                                            <h4><strong>
                                                <asp:Label ID="lblODesc1" runat="server"></asp:Label></strong></h4>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div></div>
                            <div class="col-md-2 order" id="divOItem2" style="cursor: pointer;">
                                <asp:LinkButton ID="lnkOItem2" runat="server" CssClass="lnk_Ritem" OnClientClick="return false;">
                                    <div class="divorder">
                                        <div class="text-info mt-3">
                                            <h5>Item : <strong>
                                                <asp:Label ID="lblOItem2" runat="server"></asp:Label></strong></h5>
                                        </div>
                                        <div class="text-info mt-2">
                                            <h4><strong>
                                                <asp:Label ID="lblODesc2" runat="server"></asp:Label></strong></h4>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div class="col-md-2 order" id="divOItem3" style="cursor: pointer;">
                                <asp:LinkButton ID="lnkOItem3" runat="server" CssClass="lnk_Ritem" OnClientClick="return false;">
                                    <div class="divorder">
                                        <div class="text-info mt-3">
                                            <h5>Item : <strong>
                                                <asp:Label ID="lblOItem3" runat="server"></asp:Label></strong></h5>
                                        </div>
                                        <div class="text-info mt-2">
                                            <h4><strong>
                                                <asp:Label ID="lblODesc3" runat="server"></asp:Label></strong></h4>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div class="col-md-2 order" id="divOItem4" style="cursor: pointer;">
                                <asp:LinkButton ID="lnkOItem4" runat="server" CssClass="lnk_Ritem" OnClientClick="return false;">
                                    <div class="divorder">
                                        <div class="text-info mt-3">
                                            <h5>Item : <strong>
                                                <asp:Label ID="lblOItem4" runat="server"></asp:Label></strong></h5>
                                        </div>
                                        <div class="text-info mt-2">
                                            <h4><strong>
                                                <asp:Label ID="lblODesc4" runat="server"></asp:Label></strong></h4>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div class="col-md-2 order" id="divOItem5" style="cursor: pointer;">
                                <asp:LinkButton ID="lnkOItem5" runat="server" CssClass="lnk_Ritem" OnClientClick="return false;">
                                    <div class="divorder">
                                        <div class="text-info mt-3">
                                            <h5>Item : <strong>
                                                <asp:Label ID="lblOItem5" runat="server"></asp:Label></strong></h5>
                                        </div>
                                        <div class="text-info mt-2">
                                            <h4><strong>
                                                <asp:Label ID="lblODesc5" runat="server"></asp:Label></strong></h4>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </div>


                </div>
            </div>
        </ContentTemplate>
        <%-- </asp:Panel>--%>
    </asp:UpdatePanel>
    <div id="loader_div" class="loader_div"></div>
    <%--<asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <div id="mdAPList" class="modal" style="border: solid 1px #008a8a;">
        </div>
</asp:Content>



