﻿using AjaxControlToolkit.HTMLEditor;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TSBA_BusinessObjects;

namespace TaegutecSalesBudget
{
    public partial class MDPEntryPage : System.Web.UI.Page
    {
        #region Global Declaration
        Budget objBudget = new Budget();
        MDP objMDP = new MDP();
        csFlag objFlag = new csFlag();

        static DataTable dtCutomerDetails = null;
        static List<string> tab1_collection = new List<string>();
        static List<string> tab2_collection = new List<string>();
        static List<string> tab3_collection = new List<string>();
        static List<string> tab4_collection = new List<string>();
        static List<string> tab5_collection = new List<string>();
        static List<string> tab6_collection = new List<string>();
        static List<string> tab7_collection = new List<string>();
        static List<string> tab8_collection = new List<string>();
        static List<string> tab9_collection = new List<string>();
        static List<string> tab10_collection = new List<string>();

        public static string cter;

        CommonFunctions objFunc = new CommonFunctions();
        #endregion

        #region Events
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Session["TDate1"] = Convert.ToString(tab1Target.Text);
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

                if (Convert.ToString(Session["Project"]) == "Existing")
                {
                    existing_project.Checked = true;
                    new_project.Checked = false;
                    new_project.Enabled = false;
                }
                else if (Convert.ToString(Session["Project"]) == "New")
                {

                    new_project.Checked = true;
                    existing_project.Checked = false;
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
                Response.Clear();
                if (!IsPostBack)
                {
                    if (Session["UserId"].ToString() != null)
                    {
                        objFlag = objMDP.setTSProject_Flag(Convert.ToString(Session["UserId"]));
                        if (String.IsNullOrEmpty(Convert.ToString(Session["TSProject"])))
                        {

                            if (Convert.ToString(objFlag.TSProject_Flag) == "Y")
                            {
                                rdbtsProject.Checked = true;

                            }
                            else
                            {
                                rdbNormalProject.Checked = true;
                            }
                        }
                        else
                        {
                            if (Convert.ToString(Session["TSProject"]) == "Y")
                            {
                                rdbtsProject.Checked = true;

                            }
                            else
                            {
                                rdbNormalProject.Checked = true;
                            }
                        }
                        if (Convert.ToString(objFlag.TSProject_Flag) == "Y" && Convert.ToString(objFlag.Marketing_Flag) == "Y")
                        {
                            rdbtsProject.Enabled = true;
                            rdbNormalProject.Enabled = true;
                        }
                        else
                        {
                            rdbtsProject.Enabled = false;
                            rdbNormalProject.Enabled = false;
                        }
                        string strUserId = Session["UserId"].ToString();
                        string roleId = Session["RoleId"].ToString();
                        string salesEngName = Session["UserName"].ToString();
                        string branchCode = Session["BranchCode"].ToString();
                        direct_customer.Checked = true;

                        if (roleId == "SE")
                        {
                            LoadCustomerDetails_SE(strUserId);
                            LoadDistributor_SE(strUserId);
                           
                        }
                        if (roleId == "BM")
                        {
                            LoadCustomerDetails_BM(strUserId);
                            LoadDistributor_BM(strUserId);
                            
                        }
                        if (roleId == "HO")
                        {
                            divCter.Visible = true;
                            if (Convert.ToString(objFlag.Marketing_Flag) == "Y")
                            {
                                if (Session["cter"] == null)
                                {
                                    Session["cter"] = "TTA";
                                    cter = "TTA";
                                }
                                rdBtnDuraCab.Enabled = true;
                                rdBtnTaegutec.Enabled = true;
                            }
                            else
                            {
                                if (Session["cter"] == null)
                                {
                                    Session["cter"] = "TTA";
                                    cter = "TTA";
                                }
                                rdBtnDuraCab.Enabled = false;
                                rdBtnTaegutec.Enabled = false;
                            }
                            if (Session["cter"].ToString() == "DUR")
                            {
                                rdBtnDuraCab.Checked = true;
                                rdBtnTaegutec.Checked = false;
                                cter = "DUR";
                            }
                            else
                            {
                                rdBtnTaegutec.Checked = true;
                                rdBtnDuraCab.Checked = false;
                                cter = "TTA";
                            }


                            LoadCustomerDetails_HO();
                            LoadAllDistributors();
                        }
                        if (roleId == "TM")
                        {
                            LoadDistributor_TM(strUserId);
                            LoadCustomerDetails_TM(strUserId);
                        }
                        LoadAllIndustries();
                        LoadAllProjectTypes();
                        LoadProductGroups();
                        LoadAllHOIndustry();
                        LoadAllMachineMake();
                        LoadAllMachineType();
                        TSProject_Default();
                        if (Session["Project"] == "Existing")
                        {
                            showApproveAndReject();
                        }
                    }
                    else
                    {
                        Response.Redirect("Login.aspx?Login");
                    }
                }
                else
                {
                    resetControls();
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strUserId = string.Empty;
            string CustomerNumber = "";
            string distributor = string.Empty;
            string submit1 = "", submit2 = "", submit3 = "", submit4 = "", submit5 = "", submit6 = "", submit7 = "", submit8 = "", submit9 = "", submit10 = "",comdate="";
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (Session["UserName"] == null || Session["RoleId"] == null)
                { Response.Redirect("Login.aspx?Login"); return; }
                strUserId = Session["UserId"].ToString();
                distributor = ddlDistributorList.SelectedItem.Value;
                if (direct_customer.Checked == true)
                {
                    CustomerNumber = ddlCustomerName.SelectedItem.Value;
                }
                else
                {
                    CustomerNumber = ddlDistributorList.SelectedItem.Value;
                }
                if (ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Please Select appropriate project title');", true);
                    existing_project_CheckedChanged(null, null);
                }
                else
                {
                    LoadReviewerbyCustomer(CustomerNumber);
                    LoadEscalatorbyCustomer(CustomerNumber);
                    string projectNumber = ddlTitle.SelectedItem.Value;
                    direct_customer.Enabled = false;
                    channel_partner.Enabled = false;
                    // new_project.Enabled = false;
                    new_project.Enabled = true;
                    // existing_project.Enabled = false;
                    existing_project.Enabled = true;

                    //FileUpload1.Enabled = true;
                    //tab1btnUpload.Enabled = true;
                    //for (int j = 1; j <= Convert.ToInt32(Stages.SelectedValue); j++)
                    //{
                    //
                    //    Label attachDoc = (Label)tab.FindControl("tab" + j + "doc");
                    //    attachDoc.Visible = true;
                    //    FileUpload fUpload = (FileUpload)tab.FindControl("FileUpload" + j);
                    //    fUpload.Visible = true;
                    //    Button btnUpload1 = (Button)tab.FindControl("tab" + j + "btnUpload");
                    //    btnUpload1.Visible = true;
                    //}

                    string submit = "";
                    string approved = "";
                    //anamika
                    string rejected = string.Empty;
                    string sentforApproval = string.Empty;
                    string status = string.Empty;
                    string Initiate = string.Empty;
                    string strUserRole = Convert.ToString(Session["RoleId"]);
                    string userid = Convert.ToString(Session["UserId"]);
                    //anamika end

                    tab1_collection = new List<string>();
                    tab2_collection = new List<string>();
                    tab3_collection = new List<string>();
                    tab4_collection = new List<string>();
                    tab5_collection = new List<string>();
                    tab6_collection = new List<string>();
                    tab7_collection = new List<string>();
                    tab8_collection = new List<string>();
                    tab9_collection = new List<string>();
                    tab10_collection = new List<string>();
                    DataTable dtMDPDetails = new DataTable();
                    DataTable dtMPD_StageDetails = new DataTable();
                    DataTable dtMDP_Remarks = new DataTable();
                    if (direct_customer.Checked == true)
                    {
                        CustomerNumber = ddlCustomerName.SelectedItem.Value;
                    }

                    else
                    {
                        CustomerNumber = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    

                    dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(CustomerNumber, projectNumber);

                    #region Load Upper Part Details
                    if (dtMDPDetails != null)
                    {
                        stagesvisibility(ddlTitle.SelectedValue);
                        ddlDistributorList.Enabled = false;
                        ddlTitle.Enabled = false;
                        ddlCustomerName.Enabled = false;
                        ddlCustomerNo.Enabled = false;
                        ddlCustomerforDistributor.Enabled = false;

                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtMDPDetails.Rows.Count; i++)
                            {
                                LoadOwnerbyProject(Convert.ToString(dtMDPDetails.Rows[0]["OwnerByProject"]), Convert.ToString(dtMDPDetails.Rows[0]["project_owner"]));
                                ddlTitle.SelectedValue = dtMDPDetails.Rows[i].ItemArray[5].ToString();
                                if (string.IsNullOrEmpty(Convert.ToString(dtMDPDetails.Rows[i].ItemArray[34])))
                                {
                                    ddlProjectType.SelectedValue = "-- SELECT --";
                                }
                                else
                                    ddlProjectType.SelectedValue = dtMDPDetails.Rows[i].ItemArray[34].ToString();
                                if (string.IsNullOrEmpty(Convert.ToString(dtMDPDetails.Rows[i].ItemArray[34])))
                                {
                                    ddlProductGroup.SelectedValue = "-- SELECT --";
                                }
                                else
                                    ddlProductGroup.SelectedValue = dtMDPDetails.Rows[i].ItemArray[44].ToString();
                                if (string.IsNullOrEmpty(Convert.ToString(dtMDPDetails.Rows[i].ItemArray[45])))
                                {
                                    ddlHOIndustry.SelectedValue = "-- SELECT --";
                                }
                                else
                                    ddlHOIndustry.SelectedValue = dtMDPDetails.Rows[i].ItemArray[45].ToString();
                                if (string.IsNullOrEmpty(Convert.ToString(dtMDPDetails.Rows[i].ItemArray[46])))
                                {
                                    ddlMachineMake.SelectedValue = "-- SELECT --";
                                }
                                else
                                    ddlMachineMake.SelectedValue = dtMDPDetails.Rows[i].ItemArray[46].ToString();
                                if (string.IsNullOrEmpty(Convert.ToString(dtMDPDetails.Rows[i].ItemArray[47])))
                                {
                                    ddlMachineType.SelectedValue = "-- SELECT --";
                                }
                                else
                                    ddlMachineType.SelectedValue = dtMDPDetails.Rows[i].ItemArray[47].ToString();
                                if (string.IsNullOrEmpty(Convert.ToString(dtMDPDetails.Rows[i].ItemArray[48])))
                                {
                                    txtMachNo.Text = "";
                                }
                                else
                                    txtMachNo.Text = dtMDPDetails.Rows[i].ItemArray[48].ToString();
                                ExistingProduct.Text = dtMDPDetails.Rows[i].ItemArray[7].ToString();
                                Component.Text = dtMDPDetails.Rows[i].ItemArray[8].ToString();
                                //CompetitionSpec.Text = dtMDPDetails.Rows[i].ItemArray[9].ToString();
                                txtTaegutecCutter.Text = Convert.ToString(dtMDPDetails.Rows[i]["TaegutecCutter"]);
                                txtTaegutecInsert.Text = Convert.ToString(dtMDPDetails.Rows[i]["TaegutecInsert"]);
                                txtCompCutter.Text = Convert.ToString(dtMDPDetails.Rows[i]["CompetitorCutter"]);
                                txtCompInsert.Text = Convert.ToString(dtMDPDetails.Rows[i]["CompetitorInsert"]);

                                string tsflag = dtMDPDetails.Rows[i].ItemArray[36].ToString();
                                if (tsflag == "Y")
                                {
                                    rdbtsProject.Checked = true;
                                }
                                else
                                {
                                    rdbNormalProject.Enabled = true;
                                }

                                Stages.SelectedValue = dtMDPDetails.Rows[i].ItemArray[10].ToString();
                                TargetDate.Text = dtMDPDetails.Rows[i].ItemArray[11].ToString();

                                if (distributor != "-- SELECT CHANNEL PARTNER --")
                                    ddlSalesEngName.SelectedValue = GetCustomerSalesEngineer(distributor);
                                else
                                    ddlSalesEngName.SelectedValue = GetCustomerSalesEngineer(CustomerNumber);
                                ddlReviewer.SelectedValue = dtMDPDetails.Rows[i].ItemArray[13].ToString();
                                ddlEscalate.SelectedValue = dtMDPDetails.Rows[i].ItemArray[14].ToString();
                                OverAllPotential.Text = dtMDPDetails.Rows[i].ItemArray[15].ToString();
                                Potential.Text = dtMDPDetails.Rows[i].ItemArray[16].ToString();
                                Business.Text = dtMDPDetails.Rows[i].ItemArray[17].ToString();
                                ddlIndustry.Text = dtMDPDetails.Rows[i].ItemArray[4].ToString();
                                approved = dtMDPDetails.Rows[i].ItemArray[27].ToString();
                                txtOrderTargetCY.Text = Convert.ToString(dtMDPDetails.Rows[i].ItemArray[37]);
                                txtOrderTargetNY.Text = Convert.ToString(dtMDPDetails.Rows[i].ItemArray[38]);
                                #region anamika
                                rejected = dtMDPDetails.Rows[i].ItemArray[28].ToString();
                                sentforApproval = dtMDPDetails.Rows[i].ItemArray[26].ToString();
                                status = dtMDPDetails.Rows[i].ItemArray[25].ToString();
                                Initiate= dtMDPDetails.Rows[i].ItemArray[49].ToString();

                                if (Initiate=="True")
                                {
                                    tab1Initiate.Visible = false;
                                    tab2Initiate.Visible = false;
                                    tab3Initiate.Visible = false;
                                    tab4Initiate.Visible = false;
                                    tab5Initiate.Visible = false;
                                    tab6Initiate.Visible = false;
                                    tab7Initiate.Visible = false;
                                    tab8Initiate.Visible = false;
                                    tab9Initiate.Visible = false;
                                    tab10Initiate.Visible = false;
                                }
                                if (sentforApproval == "True")
                                {
                                    DisableControls();
                                }
                                else
                                {
                                    if (approved == "True" && rejected == "False")
                                    {
                                        tab1Initiate.Visible = false;
                                        tab2Initiate.Visible = false;
                                        tab3Initiate.Visible = false;
                                        tab4Initiate.Visible = false;
                                        tab5Initiate.Visible = false;
                                        tab6Initiate.Visible = false;
                                        tab7Initiate.Visible = false;
                                        tab8Initiate.Visible = false;
                                        tab9Initiate.Visible = false;
                                        tab10Initiate.Visible = false;
                                        if (dtMDPDetails.Rows[i]["project_owner"].ToString() == strUserId)
                                        {
                                            //disable all controls
                                            DisableControls();
                                        }
                                        else if (dtMDPDetails.Rows[i].ItemArray[13].ToString() == strUserId)
                                        {
                                            //enable all controls
                                            EnableControls();
                                        }
                                        else if (dtMDPDetails.Rows[i].ItemArray[14].ToString() == strUserId)
                                        {

                                            //enable all controls
                                            EnableControls();

                                        }
                                        //else if (dtMDPDetails.Rows[i].ItemArray[32].ToString() == strUserId)
                                        //{
                                        //    //disable all controls
                                        //    EnableControls();
                                        //}    // added by anantha
                                        else
                                        {
                                            DisableControls();
                                        }
                                    }
                                    else if (approved == "False" && rejected == "True")
                                    {

                                        if (dtMDPDetails.Rows[i]["project_owner"].ToString() == strUserId)
                                        {
                                            //disable all controls
                                            EnableControls();
                                        }
                                        //else if (dtMDPDetails.Rows[i].ItemArray[13].ToString() == strUserId)
                                        //{
                                        //    //enable all controls
                                        //    EnableControls();
                                        //}
                                        //else if (dtMDPDetails.Rows[i].ItemArray[14].ToString() == strUserId)
                                        //{
                                        //    //enable all controls
                                        //    EnableControls();
                                        //}                                      
                                        else
                                        {
                                            DisableControls();
                                        }
                                    }
                                    //added by anamika
                                    else if ((approved == "False" || approved == "") && (rejected == "False" || rejected == "") && (sentforApproval == "False" || sentforApproval == "") && status == "")
                                    {
                                        EnableControls();
                                    }
                                    //else
                                    //{
                                    //    //enable all controls
                                    //    EnableControls();
                                    //}
                                }

                                #endregion
                                #endregion
                                string isoptional = "";
                                string hdnvalue = "";
                                string[] hdnarray;
                                List<stagedet> objstagelist = new List<stagedet>();
                                int count = 0;
                                #region Load Stage Details
                                for (int j = 1; j <= Convert.ToInt32(dtMDPDetails.Rows[i].ItemArray[10].ToString()); j++)
                                {
                                   
                                    dtMPD_StageDetails = objMDP.LoadMDP_Stage_Details_byCustomer_Number(CustomerNumber, j.ToString(), projectNumber);
                                    if (dtMPD_StageDetails.Rows.Count > 0)
                                    {
                                       
                                           TextBox txtGoal = (TextBox)tab.FindControl("tab" + j + "txtGoal");
                                        TextBox txtTaget = (TextBox)tab.FindControl("tab" + j + "Target");
                                        HiddenField target_flag = (HiddenField)tab.FindControl("tab" + j + "Target_flag");
                                        TextBox txtCompl = (TextBox)tab.FindControl("tab" + j + "Completion");
                                        TextBox txtNxtStageComp = (TextBox)tab.FindControl("tab" + (j) + "Completion");//YNR modified  from j+ 1 to j
                                        ImageButton btnSave = (ImageButton)tab.FindControl("tab" + j + "btnSave");
                                        ImageButton btnSaveApprove = (ImageButton)tab.FindControl("tab" + j + "btnSaveApprove");
                                        ImageButton btnInitiate = (ImageButton)tab.FindControl("tab" + j + "Initiate");
                                        ImageButton btnSubmit = (ImageButton)tab.FindControl("tab" + j + "btnSubmit");
                                        ImageButton AddRemarks = (ImageButton)tab.FindControl("tab" + j + "btnRemarks");
                                        ImageButton SaveRemarks = (ImageButton)tab.FindControl("tab" + j + "saveRemarks");
                                        CheckBox chkoptional = (CheckBox)tab.FindControl("tab" + j + "_chkbox");
                                        Label attachdoc = (Label)tab.FindControl("tab" + j + "doc");
                                        FileUpload fileUpload = (FileUpload)tab.FindControl("FileUpload" + j);

                                        Button btnUpload = (Button)tab.FindControl("tab" + j + "btnUpload");
                                        GridView fileList = (GridView)tab.FindControl("tab" + j + "_FileList");
                                        if (dtMDPDetails.Rows[i].ItemArray[36].ToString() == "Y")
                                        {
                                            if (j == 2)
                                            {
                                                TextBox txtProjectRef = (TextBox)tab.FindControl("tab2ProjectNumber");
                                                txtProjectRef.Text = Convert.ToString(dtMPD_StageDetails.Rows[0].ItemArray[7]);
                                            }
                                            if (j == 5 || j == 8)
                                            {
                                                TextBox txtOrdNum = (TextBox)tab.FindControl("tab" + j + "_Ord_Num");
                                                txtOrdNum.Text = Convert.ToString(dtMPD_StageDetails.Rows[0].ItemArray[8]);
                                                TextBox txtOrdVal = (TextBox)tab.FindControl("tab" + j + "_Ord_Val");
                                                txtOrdVal.Text = Convert.ToString(dtMPD_StageDetails.Rows[0].ItemArray[9]);
                                            }
                                        }
                                        DataTable dtFiles = new DataTable();
                                        dtFiles = objMDP.Get_StageWise_File_Names(CustomerNumber, j.ToString(), projectNumber);
                                        DataTable dtFileNames = new DataTable();
                                        dtFileNames.Columns.Add("file_name", typeof(string));
                                        dtFileNames.Columns.Add("file_number", typeof(string));
                                        if (dtFiles != null)
                                        {
                                            if (dtFiles.Rows.Count == 0)
                                            {
                                                fileList.Visible = false;
                                            }

                                            else
                                            {
                                                if (fileList.Visible == false)
                                                {
                                                    fileList.Visible = true;
                                                }
                                                for (int x = 0; x < dtFiles.Rows.Count; x++)
                                                {
                                                    string FileName = dtFiles.Rows[x].ItemArray[0].ToString();
                                                    string FileNumber = dtFiles.Rows[x].ItemArray[1].ToString();
                                                    dtFileNames.Rows.Add(FileName, FileNumber);
                                                }
                                                fileList.DataSource = dtFileNames;
                                                fileList.DataBind();
                                            }
                                        }

                                        foreach (GridViewRow row in fileList.Rows)
                                        {
                                            LinkButton lnkFull = row.FindControl("tab" + j + "_file_lbl") as LinkButton;
                                            if (lnkFull != null)
                                            {
                                                ScriptManager.GetCurrent(this).RegisterPostBackControl(lnkFull);
                                            }
                                        }
                                        
                                        if (dtMPD_StageDetails != null)
                                        {
                                            for (int k = 0; k < dtMPD_StageDetails.Rows.Count; k++)
                                            {
                                                stagedet objstage = new stagedet();
                                                txtGoal.Text = dtMPD_StageDetails.Rows[k].ItemArray[1].ToString();
                                                txtTaget.Text = dtMPD_StageDetails.Rows[k].ItemArray[2].ToString();
                                                if(dtMPD_StageDetails.Rows[k].ItemArray[3].ToString() !="01/01/1753")
                                                {
                                                    txtCompl.Text = dtMPD_StageDetails.Rows[k].ItemArray[3].ToString();
                                                }
                                                isoptional = dtMPD_StageDetails.Rows[k].ItemArray[10].ToString();
                                                objstage.Index = Convert.ToString(j);
                                                objstage.completionDate = dtMPD_StageDetails.Rows[k].ItemArray[3].ToString();
                                                objstage.approved = approved;
                                                objstage.enable = "False";
                                                objstage.checkboxflag = isoptional;
                                                objstagelist.Add(objstage);

                                                if (isoptional == "True")
                                                {
                                                    count++;
                                                    chkoptional.Checked = true;
                                                    txtTaget.Enabled = false;
                                                    txtCompl.Enabled = false;
                                                }
                                                else
                                                {
                                                    hdnvalue += Convert.ToString(j)+",";
                                                }
                                                if (sentforApproval == "True")
                                                {
                                                    btnInitiate.Visible = false;
                                                }

                                                comdate = dtMPD_StageDetails.Rows[k].ItemArray[3].ToString();
                                                submit = dtMPD_StageDetails.Rows[k].ItemArray[5].ToString();
                                                if (j == 1)
                                                {
                                                    submit1 = submit;
                                                }
                                                if (j == 2)
                                                {
                                                    submit2 = submit;
                                                }
                                                if (j == 3)
                                                {
                                                    submit3 = submit;
                                                }
                                                if (j == 4)
                                                {
                                                    submit4 = submit;
                                                }
                                                if (j == 5)
                                                {
                                                    submit5 = submit;
                                                }
                                                if (j == 6)
                                                {
                                                    submit6 = submit;
                                                }
                                                if (j == 7)
                                                {
                                                    submit7 = submit;
                                                }
                                                if (j == 8)
                                                {
                                                    submit8 = submit;
                                                }
                                                if (j == 9)
                                                {
                                                    submit9 = submit;
                                                }
                                                if (j == 10)
                                                {
                                                    submit10 = submit;
                                                }
                                                if (dtMDPDetails.Rows[i]["project_owner"].ToString() == strUserId)
                                                {
                                                    if (dtMDPDetails.Rows[i].ItemArray[26].ToString() == "True")
                                                    {
                                                        txtTaget.Enabled = false;
                                                        txtGoal.Enabled = false;
                                                        btnSave.Visible = true;
                                                        btnSaveApprove.Visible = true;
                                                        btnSubmit.Visible = false;
                                                        AddRemarks.Visible = false;
                                                        //SaveRemarks.Enabled = false;
                                                    }
                                                    else
                                                    {
                                                        if (dtMDPDetails.Rows[i].ItemArray[27].ToString() == "True")
                                                        {
                                                            txtTaget.Enabled = false;
                                                            txtGoal.Enabled = false;
                                                            btnSave.Visible = false;
                                                            btnSaveApprove.Visible = false;
                                                            //if (j == Convert.ToInt32(dtMDPDetails.Rows[i].ItemArray[10].ToString()))
                                                            // {
                                                            btnSubmit.Visible = true;
                                                            //}
                                                            AddRemarks.Visible = true;
                                                            //SaveRemarks.Enabled = true;
                                                        }
                                                        else if (dtMDPDetails.Rows[i].ItemArray[28].ToString() == "True")
                                                        {
                                                            txtTaget.Enabled = true;
                                                            txtGoal.Enabled = true;
                                                            btnSave.Visible = true;
                                                            btnSaveApprove.Visible = true;
                                                            btnSubmit.Visible = false;
                                                            AddRemarks.Visible = false;
                                                            //SaveRemarks.Enabled = false;
                                                        }
                                                        else
                                                        {
                                                            txtTaget.Enabled = true;
                                                            txtGoal.Enabled = true;
                                                            btnSave.Visible = true;
                                                            btnSaveApprove.Visible = true;
                                                            btnSubmit.Visible = false;
                                                            AddRemarks.Visible = false;
                                                            // SaveRemarks.Enabled = false;
                                                        }
                                                    }

                                                }
                                                else if (dtMDPDetails.Rows[i].ItemArray[12].ToString() == strUserId)
                                                {
                                                    string isInitiate = dtMDPDetails.Rows[i].ItemArray[43].ToString();
                                                    if (isInitiate == "True")
                                                    {
                                                        tab1_chkbox.Checked = false;
                                                        tab2_chkbox.Checked = false;
                                                        tab3_chkbox.Checked = false;
                                                        tab4_chkbox.Checked = false;
                                                        tab5_chkbox.Checked = false;
                                                        tab6_chkbox.Checked = false;
                                                        tab7_chkbox.Checked = false;
                                                        tab8_chkbox.Checked = false;
                                                        tab9_chkbox.Checked = false;
                                                        tab10_chkbox.Checked = false;
                                                        txtTaget.Enabled = true;
                                                    }

                                                    if (dtMDPDetails.Rows[i].ItemArray[27].ToString() == "True")
                                                    {

                                                        if (isoptional == "True")
                                                        {

                                                            txtTaget.Enabled = false;
                                                        }
                                                        else
                                                        {
                                                            txtTaget.Enabled = true;
                                                        }
                                                        txtGoal.Enabled = true;
                                                        btnSave.Visible = false;
                                                        btnSaveApprove.Visible = false;
                                                        //if (j == Convert.ToInt32(dtMDPDetails.Rows[i].ItemArray[10].ToString()))
                                                        //{
                                                        btnSubmit.Visible = true;
                                                        //}
                                                        AddRemarks.Visible = true;
                                                        // SaveRemarks.Enabled = true;
                                                    }
                                                    else
                                                    {
                                                        if (isoptional == "True")
                                                        {

                                                            txtTaget.Enabled = false;
                                                        }
                                                        else
                                                        {
                                                            txtTaget.Enabled = true;
                                                        }
                                                        txtGoal.Enabled = true;
                                                        btnSave.Visible = true;
                                                        btnSaveApprove.Visible = true;
                                                        btnSubmit.Visible = false;
                                                        AddRemarks.Visible = false;
                                                        // SaveRemarks.Enabled = false;
                                                    }

                                                }
                                                else if (dtMDPDetails.Rows[i].ItemArray[13].ToString() == strUserId)
                                                {
                                                    if (dtMDPDetails.Rows[i].ItemArray[27].ToString() == "True")
                                                    {
                                                        
                                                        if (isoptional == "True")
                                                        {
                                                          
                                                            txtTaget.Enabled = false;
                                                        }
                                                        else
                                                        {
                                                            txtTaget.Enabled = true;
                                                        }
                                                        txtGoal.Enabled = true;
                                                        btnSave.Visible = false;
                                                        btnSaveApprove.Visible = false;
                                                        //if (j == Convert.ToInt32(dtMDPDetails.Rows[i].ItemArray[10].ToString()))
                                                        //{
                                                        btnSubmit.Visible = true;
                                                        //}
                                                        AddRemarks.Visible = true;
                                                        // SaveRemarks.Enabled = true;
                                                    }
                                                    else
                                                    {
                                                        if (isoptional == "True")
                                                        {

                                                            txtTaget.Enabled = false;
                                                        }
                                                        else
                                                        {
                                                            txtTaget.Enabled = true;
                                                        }
                                                        txtGoal.Enabled = true;
                                                        btnSave.Visible = true;
                                                        btnSaveApprove.Visible = true;
                                                        btnSubmit.Visible = false;
                                                        AddRemarks.Visible = false;
                                                        // SaveRemarks.Enabled = false;
                                                    }
                                                }
                                                else if (dtMDPDetails.Rows[i].ItemArray[14].ToString() == strUserId)
                                                {
                                                    if (dtMDPDetails.Rows[i].ItemArray[27].ToString() == "True")
                                                    {
                                                        if (isoptional == "True")
                                                        {

                                                            txtTaget.Enabled = false;
                                                        }
                                                        else
                                                        {
                                                            txtTaget.Enabled = true;
                                                        }
                                                        txtGoal.Enabled = true;
                                                        btnSave.Visible = false;
                                                        btnSaveApprove.Visible = false;
                                                        //if (j == Convert.ToInt32(dtMDPDetails.Rows[i].ItemArray[10].ToString()))
                                                        //{
                                                        btnSubmit.Visible = true;
                                                        //}
                                                        AddRemarks.Visible = true;
                                                        // SaveRemarks.Visible = true;
                                                    }
                                                    else
                                                    {
                                                        if (isoptional == "True")
                                                        {

                                                            txtTaget.Enabled = false;
                                                        }
                                                        else
                                                        {
                                                            txtTaget.Enabled = true;
                                                        }
                                                        txtGoal.Enabled = true;
                                                        btnSave.Visible = true;
                                                        btnSaveApprove.Visible = true;
                                                        btnSubmit.Visible = false;
                                                        AddRemarks.Visible = false;
                                                        //SaveRemarks.Enabled = false;
                                                    }
                                                }
                                                else
                                                {
                                                    txtTaget.Enabled = false;
                                                    txtGoal.Enabled = false;
                                                    btnSave.Visible = false;
                                                    btnSaveApprove.Visible = false;
                                                    btnSubmit.Visible = false;
                                                    AddRemarks.Visible = true;
                                                    //SaveRemarks.Enabled = true;
                                                }
                                                if (submit == "True" && comdate!="01/01/1753")
                                                {

                                                    txtCompl.Enabled = false;
                                                    ddlEscalate.Enabled = false;
                                                    ddlReviewer.Enabled = false;
                                                    ddlProjectType.Enabled = false;
                                                    ddlProductGroup.Enabled = false;
                                                    //txtNxtStageComp.Enabled = true;
                                                    txtNxtStageComp.Enabled = false;//YNR modified true to false
                                                    ExistingProduct.Enabled = false;
                                                    txtTaegutecCutter.Enabled = false;
                                                    txtTaegutecInsert.Enabled = false;
                                                    txtCompCutter.Enabled = false;
                                                    txtCompInsert.Enabled = false;
                                                    Component.Enabled = false;
                                                    //CompetitionSpec.Enabled = false;
                                                    Stages.Enabled = false;
                                                    TargetDate.Enabled = false;
                                                    Target_flag.Value = "True";
                                                    OverAllPotential.Enabled = false;
                                                    Potential.Enabled = false;
                                                    Business.Enabled = false;
                                                    ddlSalesEngName.Enabled = false;
                                                    ddlCustomerName.Enabled = false;
                                                    ddlCustomerNo.Enabled = false;
                                                    ddlIndustry.Enabled = false;
                                                    ddlDistibutor.Enabled = false;
                                                    txtTaget.Enabled = false;
                                                    target_flag.Value = "True";
                                                    txtGoal.Enabled = false;
                                                    btnSubmit.Visible = false;
                                                    fileUpload.Enabled = false;
                                                    btnUpload.Enabled = false;
                                                    txtOrderTargetCY.Enabled = false;
                                                    txtOrderTargetNY.Enabled = false;
                                                    if (dtMDPDetails.Rows[i].ItemArray[10].ToString() == (j).ToString())
                                                    {
                                                        RadioButton success = (RadioButton)tab.FindControl("tab" + j + "_radio_success");
                                                        RadioButton failure = (RadioButton)tab.FindControl("tab" + j + "_radio_failure");
                                                        TextBox monthly_expected = (TextBox)tab.FindControl("tab" + j + "_monthly_expected");
                                                        TextBox comments = (TextBox)tab.FindControl("tab" + j + "_comments");
                                                        TextBox cust_ord = (TextBox)tab.FindControl("tab" + j + "_customer_order");
                                                        final_stage.Value = j.ToString();

                                                        if (dtMDPDetails.Rows[i].ItemArray[21].ToString() == "success")
                                                        {
                                                            success.Checked = true;
                                                            failure.Checked = false;
                                                            success.Enabled = false;
                                                            failure.Enabled = false;
                                                            monthly_expected.Text = dtMDPDetails.Rows[i].ItemArray[22].ToString();
                                                            monthly_expected.Enabled = false;
                                                            cust_ord.Text = dtMDPDetails.Rows[i].ItemArray[35].ToString();
                                                            cust_ord.Enabled = false;
                                                        }
                                                        else
                                                        {
                                                            success.Checked = false;
                                                            failure.Checked = true;
                                                            success.Enabled = false;
                                                            failure.Enabled = false;
                                                        }
                                                        comments.Text = dtMDPDetails.Rows[i].ItemArray[23].ToString();
                                                        comments.Enabled = false;
                                                    }
                                                }
                                                else
                                                {

                                                    //attachdoc.Visible=true;
                                                    fileUpload.Enabled = true;
                                                    btnUpload.Enabled = true;
                                                }
                                            }
                                        }
                                        if (j == Convert.ToInt32(dtMDPDetails.Rows[i].ItemArray[10].ToString()))
                                        {
                                            btnSave.Visible = false;
                                            if (dtMDPDetails.Rows[i].ItemArray[27].ToString() == "True")
                                            {
                                                btnSaveApprove.Visible = false;
                                            }
                                            else
                                            {
                                                btnSaveApprove.Visible = true;
                                            }
                                        }
                                        else
                                        {
                                            if (dtMDPDetails.Rows[i].ItemArray[27].ToString() == "True")
                                            {
                                                btnSave.Visible = false;
                                            }
                                            else
                                            {
                                                btnSave.Visible = true;
                                            }
                                            btnSaveApprove.Visible = false;
                                        }
                                    }
                                    #endregion

                                    #region Load Remarks
                                    dtMDP_Remarks = objMDP.LoadMDP_Remarks(CustomerNumber, j.ToString(), projectNumber);

                                    int idcount = 1;
                                    if (dtMDP_Remarks.Rows.Count > 0)
                                    {
                                        for (int l = 1; l <= dtMDP_Remarks.Rows.Count; l++)
                                        {

                                            /*If more than one then create textbox*/
                                            TextBox txtRemark = (TextBox)tab.FindControl("tab" + j + "txtRemarks_" + dtMDP_Remarks.Rows[l - 1].ItemArray[4].ToString());
                                            if (txtRemark != null)
                                            {

                                                if (dtMDP_Remarks != null)
                                                {
                                                    for (int m = l - 1; m < dtMDP_Remarks.Rows.Count; m++)
                                                    {

                                                        if (isoptional == "True")
                                                        {

                                                            txtRemark.Enabled = false;
                                                        }
                                                        txtRemark.Text = dtMDP_Remarks.Rows[l - 1].ItemArray[1].ToString();
                                                        //anamika
                                                        if (dtMDPDetails.Rows[i]["project_owner"].ToString() == strUserId)
                                                        {
                                                            if (dtMDPDetails.Rows[i].ItemArray[26].ToString() == "True")
                                                            {
                                                                txtRemark.Enabled = false;
                                                            }
                                                            else
                                                            {

                                                                if (dtMDPDetails.Rows[i].ItemArray[27].ToString() == "True")
                                                                {
                                                                    txtRemark.Enabled = false;
                                                                }
                                                                else if (dtMDPDetails.Rows[i].ItemArray[28].ToString() == "True")
                                                                {
                                                                    txtRemark.Enabled = true;
                                                                }
                                                                else
                                                                {
                                                                    txtRemark.Enabled = true;
                                                                }
                                                            }

                                                        }
                                                        if (dtMDPDetails.Rows[i].ItemArray[12].ToString() == strUserId)
                                                        {
                                                            string isInitiate = dtMDPDetails.Rows[i].ItemArray[43].ToString();
                                                            if (isInitiate == "True")
                                                            {
                                                                txtRemark.Enabled = true;
                                                            }
                                                           
                                                        }

                                                        else
                                                        {
                                                            txtRemark.Enabled = false;
                                                        }

                                                        //txtRemark.Enabled = false;
                                                        if (Convert.ToInt32(dtMDP_Remarks.Rows[l - 1].ItemArray[4].ToString()) > 1)
                                                        {
                                                            if (j == 1)
                                                            {
                                                                tab1_collection.Add(txtRemark.ID);
                                                            }
                                                            if (j == 2)
                                                            {
                                                                tab2_collection.Add(txtRemark.ID);
                                                            }
                                                            if (j == 3)
                                                            {
                                                                tab3_collection.Add(txtRemark.ID);
                                                            }
                                                            if (j == 4)
                                                            {
                                                                tab4_collection.Add(txtRemark.ID);
                                                            }
                                                            if (j == 5)
                                                            {
                                                                tab5_collection.Add(txtRemark.ID);
                                                            }
                                                            if (j == 6)
                                                            {
                                                                tab6_collection.Add(txtRemark.ID);
                                                            }
                                                            if (j == 7)
                                                            {
                                                                tab7_collection.Add(txtRemark.ID);
                                                            }
                                                            if (j == 8)
                                                            {
                                                                tab8_collection.Add(txtRemark.ID);
                                                            }
                                                            if (j == 9)
                                                            {
                                                                tab9_collection.Add(txtRemark.ID);
                                                            }
                                                            if (j == 10)
                                                            {
                                                                tab10_collection.Add(txtRemark.ID);
                                                            }
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                            else
                                            {

                                                HtmlContainerControl cntrl = (HtmlContainerControl)tab.FindControl("tab" + j + "Remarks");
                                                TextBox txtRemark1 = new TextBox();
                                                txtRemark1.ID = "tab" + j + "txtRemarks_" + (l);
                                                txtRemark1.TextMode = TextBoxMode.MultiLine;
                                                txtRemark1.Rows = 2;
                                                txtRemark1.Columns = 100;
                                                txtRemark1.CssClass = "form-control  stageinput addtext";
                                                txtRemark1.Enabled = false;
                                                cntrl.Controls.Add(txtRemark1);

                                                HtmlGenericControl para = new HtmlGenericControl("p");
                                                cntrl.Controls.Add(para);
                                                if (j == 1)
                                                {
                                                    tab1_collection.Add(txtRemark1.ID);
                                                }
                                                if (j == 2)
                                                {
                                                    tab2_collection.Add(txtRemark1.ID);
                                                }
                                                if (j == 3)
                                                {
                                                    tab3_collection.Add(txtRemark1.ID);
                                                }
                                                if (j == 4)
                                                {
                                                    tab4_collection.Add(txtRemark1.ID);
                                                }
                                                if (j == 5)
                                                {
                                                    tab5_collection.Add(txtRemark1.ID);
                                                }
                                                if (j == 6)
                                                {
                                                    tab6_collection.Add(txtRemark1.ID);
                                                }
                                                if (j == 7)
                                                {
                                                    tab7_collection.Add(txtRemark1.ID);
                                                }
                                                if (j == 8)
                                                {
                                                    tab8_collection.Add(txtRemark1.ID);
                                                }
                                                if (j == 9)
                                                {
                                                    tab9_collection.Add(txtRemark1.ID);
                                                }
                                                if (j == 10)
                                                {
                                                    tab10_collection.Add(txtRemark1.ID);
                                                }
                                                if (dtMDP_Remarks != null)
                                                {
                                                    for (int m = l - 1; m < dtMDP_Remarks.Rows.Count; m++)
                                                    {

                                                        txtRemark1.Text = dtMDP_Remarks.Rows[m].ItemArray[1].ToString();
                                                        if (submit == "True")
                                                        {
                                                            txtRemark1.Enabled = false;
                                                        }
                                                        break;

                                                    }
                                                }

                                            }
                                        }
                                    } 

                                }
                                if (hdnvalue.Length > 0)
                                {
                                    hdnvalue = hdnvalue.Remove(hdnvalue.Length - 1);
                                }
                                hdnarray = hdnvalue.Split(',');
                                Hdnfinalsatge.Value = hdnarray[hdnarray.Length-1];
                                #endregion

                                tab10_TextBoxIdCollection = tab10_collection;
                                tab1_TextBoxIdCollection = tab1_collection;
                                tab2_TextBoxIdCollection = tab2_collection;
                                tab3_TextBoxIdCollection = tab3_collection;
                                tab4_TextBoxIdCollection = tab4_collection;
                                tab5_TextBoxIdCollection = tab5_collection;
                                tab6_TextBoxIdCollection = tab6_collection;
                                tab7_TextBoxIdCollection = tab7_collection;
                                tab8_TextBoxIdCollection = tab8_collection;
                                tab9_TextBoxIdCollection = tab9_collection;
                                tab10_TextBoxIdCollection = tab10_collection;

                                string check = "False";
                              
                                for(int m=0;m<objstagelist.Count;m++)
                                {
                                    for(int n=0;n<=m;n++)
                                    {
                                        if (objstagelist[n].checkboxflag == "False" && approved == "True" && objstagelist[n].completionDate != "")
                                        {
                                            if (check == "False")
                                            {
                                                objstagelist[n].enable = "False";
                                                check = "False";
                                                
                                            }
                                        }
                                        else if (objstagelist[n].enable == "False" && check == "False" && approved == "True" && objstagelist[n].checkboxflag == "False" && objstagelist[n].completionDate == "")
                                        {

                                            objstagelist[n].enable = "True";
                                            check = "True";
                                           
                                        }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
                                        else if (objstagelist[n].enable == "True")
                                        {
                                            check = "True";
                                        }
                                    }
                                    check = "False";
                                }
                                
                                for(int l=0;l<objstagelist.Count;l++)
                                {
                                    if(l==0) 
                                    {
                                        tab1Completion.Enabled = Convert.ToBoolean(objstagelist[l].enable);
                                        tab1Completion_flag.Value = Convert.ToString(objstagelist[l].enable);
                                    }

                                    else if(l == 1)
                                    {
                                        tab2Completion.Enabled = Convert.ToBoolean(objstagelist[l].enable);
                                        tab2Completion_flag.Value = Convert.ToString(objstagelist[l].enable);
                                    }
                                    else if (l == 2)
                                    {
                                        tab3Completion.Enabled = Convert.ToBoolean(objstagelist[l].enable);
                                        tab3Completion_flag.Value = Convert.ToString(objstagelist[l].enable);
                                    }
                                    else if (l == 3)
                                    {
                                        tab4Completion.Enabled = Convert.ToBoolean(objstagelist[l].enable);
                                        tab4Completion_flag.Value = Convert.ToString(objstagelist[l].enable);
                                    }
                                    else if (l == 4)
                                    {
                                        tab5Completion.Enabled = Convert.ToBoolean(objstagelist[l].enable);
                                        tab5Completion_flag.Value = Convert.ToString(objstagelist[l].enable);
                                    }
                                    else if (l == 5)
                                    {
                                        tab6Completion.Enabled = Convert.ToBoolean(objstagelist[l].enable);
                                        tab6Completion_flag.Value = Convert.ToString(objstagelist[l].enable);
                                    }
                                    else if (l == 6)
                                    {
                                        tab7Completion.Enabled = Convert.ToBoolean(objstagelist[l].enable);
                                        tab7Completion_flag.Value = Convert.ToString(objstagelist[l].enable);
                                    }
                                    else if (l == 7)
                                    {
                                        tab8Completion.Enabled = Convert.ToBoolean(objstagelist[l].enable);
                                        tab8Completion_flag.Value = Convert.ToString(objstagelist[l].enable);
                                    }
                                    else if (l == 8)
                                    {
                                        tab9Completion.Enabled = Convert.ToBoolean(objstagelist[l].enable);
                                        tab9Completion_flag.Value = Convert.ToString(objstagelist[l].enable);
                                    }
                                    else if (l == 9)
                                    {
                                        tab10Completion.Enabled = Convert.ToBoolean(objstagelist[l].enable);
                                        tab10Completion_flag.Value = Convert.ToString(objstagelist[l].enable);
                                    }
                                    
                                }
                               
                                //TextBox finaltarget = (TextBox)tab.FindControl("tab" + dtMDPDetails.Rows[i].ItemArray[10].ToString() + "Target");
                                //if (finaltarget.Text != "" && submit1 != "True" && approved == "True")
                                //{
                                //    tab1Completion.Enabled = true;
                                //    tab1Completion_flag.Value = "True";
                                //}

                                //if (tab1Completion.Text != "" && submit2 != "True" && approved == "True")
                                //{
                                //    tab2Completion.Enabled = true;
                                //    tab2Completion_flag.Value = "True";
                                //}

                                //if (tab2Completion.Text != "" && submit3 != "True" && approved == "True")
                                //{
                                //    tab3Completion.Enabled = true;
                                //    tab3Completion_flag.Value = "True";
                                //}

                                //if (tab3Completion.Text != "" && submit4 != "True" && approved == "True")
                                //{
                                //    tab4Completion.Enabled = true;
                                //    tab4Completion_flag.Value = "True";
                                //}

                                //if (tab4Completion.Text != "" && submit5 != "True" && approved == "True")
                                //{
                                //    tab5Completion.Enabled = true;
                                //    tab5Completion_flag.Value = "True";
                                //}

                                //if (tab5Completion.Text != "" && submit6 != "True" && approved == "True")
                                //{
                                //    tab6Completion.Enabled = true;
                                //    tab6Completion_flag.Value = "True";
                                //}

                                //if (tab6Completion.Text != "" && submit7 != "True" && approved == "True")
                                //{
                                //    tab7Completion.Enabled = true;
                                //    tab7Completion_flag.Value = "True";
                                //}

                                //if (tab7Completion.Text != "" && submit8 != "True" && approved == "True")
                                //{
                                //    tab8Completion.Enabled = true;
                                //    tab8Completion_flag.Value = "True";
                                //}

                                //if (tab8Completion.Text != "" && submit9 != "True" && approved == "True")
                                //{
                                //    tab9Completion.Enabled = true;
                                //    tab9Completion_flag.Value = "True";
                                //}

                                //if (tab9Completion.Text != "" && submit10 != "True" && approved == "True")
                                //{
                                //    tab10Completion.Enabled = true;
                                //    tab10Completion_flag.Value = "True";
                                //}
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);

                        }
                        else
                        {
                            LoadOwnerbyProject(Convert.ToString(Session["UserName"]), Convert.ToString(Session["UserId"]));
                            if (distributor != "-- SELECT CHANNEL PARTNER --")
                                ddlSalesEngName.SelectedValue = GetCustomerSalesEngineer(distributor);
                            else
                                ddlSalesEngName.SelectedValue = GetCustomerSalesEngineer(CustomerNumber);

                            LoadDefault();
                            LoadDefaultTab();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
                        }
                    }
                    tab1Target_date.Value = Convert.ToString(tab1Target.Text);
                    tab2Target_date.Value = Convert.ToString(tab2Target.Text);
                    tab3Target_date.Value = Convert.ToString(tab3Target.Text);
                    tab4Target_date.Value = Convert.ToString(tab4Target.Text);
                    tab5Target_date.Value = Convert.ToString(tab5Target.Text);
                    tab6Target_date.Value = Convert.ToString(tab6Target.Text);
                    tab7Target_date.Value = Convert.ToString(tab7Target.Text);
                    tab8Target_date.Value = Convert.ToString(tab8Target.Text);
                    tab9Target_date.Value = Convert.ToString(tab9Target.Text);
                    tab10Target_date.Value = Convert.ToString(tab10Target.Text);
                    //added by anamika
                    Target_Date.Value = Convert.ToString(TargetDate.Text);
                }

                TSProject_Changes();
                //if (existing_project.Checked == true)
                //{

                //    int finalstage = 0;
                //    finalstage = Convert.ToInt32(Stages.SelectedValue);
                //    for (int j = 1; j <= finalstage; j++)
                //    {

                //        ImageButton btnRemarks = (ImageButton)tab.FindControl("tab" + j + "btnRemarks");
                //        btnRemarks.Visible = true;
                //    }

                //    ImageButton btnSaveApprove = (ImageButton)tab.FindControl("tab" + finalstage + "btnSaveApprove");
                //    btnSaveApprove.Visible = false;

                //    //Add here button//

                //}

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        public class stagedet
        {
            public string Index { get; set; }
            public string checkboxflag { get; set; }
            public string approved { get; set; }
            public string completionDate { get; set; }
            public string enable { get; set; }
        }

        private void LoadOwnerbyProject(string owner, string owner_id)
        {
            txtOwner.Text = owner;
            hdnOwner.Value = owner_id;
        }

        #region Download Stage Files
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void downloadStage_Files(object sender, EventArgs e)
        {
            int file_number;
            string file_name;
            string strFileName;
            string path;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                LinkButton lb = (LinkButton)sender;
                file_number = Convert.ToInt32(lb.CommandArgument.ToString());
                file_name = objMDP.Get_File_Names_byNumber(file_number);
                strFileName = file_name;// lnkDownload.Text;
                path = Server.MapPath("~/Uploads//" + strFileName);
                if (File.Exists(path))
                {
                    byte[] bts = System.IO.File.ReadAllBytes(path);
                    MemoryStream ms = new MemoryStream(bts);
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + strFileName + "\"");
                    Response.TransmitFile(path);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        #endregion

        #region File Upload
        protected void tab1_UploadMultipleFiles(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (FileUpload1.PostedFiles[0].FileName != "")
                {
                    foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
                    {
                        string fileName = Path.GetFileName(postedFile.FileName);
                        FileInfo fi = new FileInfo(fileName);
                        string extension = fi.Extension;
                        string filepart = Path.GetFileNameWithoutExtension(fi.Name);
                        fileName = filepart + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH_mm_ss") + extension;

                        postedFile.SaveAs(Server.MapPath("~/Uploads/") + fileName);

                        if (direct_customer.Checked == true)
                        {
                            objMDP.file_customer_num = ddlCustomerNo.SelectedItem.Value;
                        }
                        else
                        {
                            objMDP.file_customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                            objMDP.file_distributor = ddlDistributorList.SelectedItem.Value;
                        }
                        if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
                        {
                            objMDP.file_project_num = objMDP.new_project_number;
                        }
                        else
                        {
                            objMDP.file_project_num = ddlTitle.SelectedItem.Value;
                        }
                        objMDP.file_Stage_Number = "1";
                        objMDP.file_name = fileName;
                        string Message = objMDP.save_file_details(objMDP);
                    }
                    tab1Completion.Enabled = true;
                    ddlTitle_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('files have been uploaded successfully.');", true);  // Added by Anantha
                    // tab1lblSuccess.Text = string.Format("{0} files have been uploaded successfully.", FileUpload1.PostedFiles.Count);  //commented by Anantha
                    //tab1lblSuccess.Text = "";
                }
                else
                {
                    tab1lblSuccess.Text = "Please Select a file.";

                }
                stagesvisibility(objMDP.file_project_num);
                tab_number.Value = "1";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void tab2_UploadMultipleFiles(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (FileUpload2.PostedFiles[0].FileName != "")
                {
                    foreach (HttpPostedFile postedFile in FileUpload2.PostedFiles)
                    {
                        string fileName = Path.GetFileName(postedFile.FileName);
                        FileInfo fi = new FileInfo(fileName);
                        string extension = fi.Extension;
                        string filepart = Path.GetFileNameWithoutExtension(fi.Name);
                        fileName = filepart + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH_mm_ss") + extension;
                        postedFile.SaveAs(Server.MapPath("~/Uploads/") + fileName);
                        if (direct_customer.Checked == true)
                        {
                            objMDP.file_customer_num = ddlCustomerNo.SelectedItem.Value;
                        }
                        else
                        {
                            objMDP.file_customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                            objMDP.file_distributor = ddlDistributorList.SelectedItem.Value;
                        }
                        if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
                        {
                            objMDP.file_project_num = objMDP.new_project_number;
                        }
                        else
                        {
                            objMDP.file_project_num = ddlTitle.SelectedItem.Value;
                        }
                        objMDP.file_name = fileName;
                        objMDP.file_Stage_Number = "2";
                        string Message = objMDP.save_file_details(objMDP);
                    }
                    ddlTitle_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('files have been uploaded successfully.');", true);
                    // tab2lblSuccess.Text = string.Format("{0} files have been uploaded successfully.", FileUpload2.PostedFiles.Count);
                    //tab2lblSuccess.Text = "";
                }
                else
                {
                    tab2lblSuccess.Text = "Please Select a file";
                }
                stagesvisibility(objMDP.file_project_num);
                tab_number.Value = "2";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void tab3_UploadMultipleFiles(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (FileUpload3.PostedFiles[0].FileName != "")
                {
                    foreach (HttpPostedFile postedFile in FileUpload3.PostedFiles)
                    {
                        string fileName = Path.GetFileName(postedFile.FileName);
                        FileInfo fi = new FileInfo(fileName);
                        string extension = fi.Extension;
                        string filepart = Path.GetFileNameWithoutExtension(fi.Name);
                        fileName = filepart + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH_mm_ss") + extension;
                        postedFile.SaveAs(Server.MapPath("~/Uploads/") + fileName);
                        if (direct_customer.Checked == true)
                        {
                            objMDP.file_customer_num = ddlCustomerNo.SelectedItem.Value;
                        }
                        else
                        {
                            objMDP.file_customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                            objMDP.file_distributor = ddlDistributorList.SelectedItem.Value;
                        }
                        if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
                        {
                            objMDP.file_project_num = objMDP.new_project_number;
                        }
                        else
                        {
                            objMDP.file_project_num = ddlTitle.SelectedItem.Value;
                        }
                        objMDP.file_Stage_Number = "3";
                        objMDP.file_name = fileName;
                        string Message = objMDP.save_file_details(objMDP);
                    }
                    ddlTitle_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('files have been uploaded successfully.');", true);
                    // tab3lblSuccess.Text = string.Format("{0} files have been uploaded successfully.", FileUpload3.PostedFiles.Count);
                    //tab3lblSuccess.Text = "";
                }
                else
                {
                    tab3lblSuccess.Text = "Please Select a file";
                }
                stagesvisibility(objMDP.file_project_num);
                tab_number.Value = "3";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void tab4_UploadMultipleFiles(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (FileUpload4.PostedFiles[0].FileName != "")
                {
                    foreach (HttpPostedFile postedFile in FileUpload4.PostedFiles)
                    {
                        string fileName = Path.GetFileName(postedFile.FileName);
                        FileInfo fi = new FileInfo(fileName);
                        string extension = fi.Extension;
                        string filepart = Path.GetFileNameWithoutExtension(fi.Name);
                        fileName = filepart + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH_mm_ss") + extension;
                        postedFile.SaveAs(Server.MapPath("~/Uploads/") + fileName);
                        if (direct_customer.Checked == true)
                        {
                            objMDP.file_customer_num = ddlCustomerNo.SelectedItem.Value;
                        }
                        else
                        {
                            objMDP.file_customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                            objMDP.file_distributor = ddlDistributorList.SelectedItem.Value;
                        }
                        if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
                        {
                            objMDP.file_project_num = objMDP.new_project_number;
                        }
                        else
                        {
                            objMDP.file_project_num = ddlTitle.SelectedItem.Value;
                        }
                        objMDP.file_Stage_Number = "4";
                        objMDP.file_name = fileName;
                        string Message = objMDP.save_file_details(objMDP);
                    }
                    ddlTitle_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('files have been uploaded successfully.');", true);
                    //  tab4lblSuccess.Text = string.Format("{0} files have been uploaded successfully.", FileUpload4.PostedFiles.Count);
                    //tab4lblSuccess.Text = "";
                }
                else
                {
                    tab4lblSuccess.Text = "Please select a file";
                }
                stagesvisibility(objMDP.file_project_num);
                tab_number.Value = "5";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void tab5_UploadMultipleFiles(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (FileUpload5.PostedFiles[0].FileName != "")
                {
                    foreach (HttpPostedFile postedFile in FileUpload5.PostedFiles)
                    {
                        string fileName = Path.GetFileName(postedFile.FileName);
                        FileInfo fi = new FileInfo(fileName);
                        string extension = fi.Extension;
                        string filepart = Path.GetFileNameWithoutExtension(fi.Name);
                        fileName = filepart + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH_mm_ss") + extension;
                        postedFile.SaveAs(Server.MapPath("~/Uploads/") + fileName);
                        if (direct_customer.Checked == true)
                        {
                            objMDP.file_customer_num = ddlCustomerNo.SelectedItem.Value;
                        }
                        else
                        {
                            objMDP.file_customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                            objMDP.file_distributor = ddlDistributorList.SelectedItem.Value;
                        }
                        if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
                        {
                            objMDP.file_project_num = objMDP.new_project_number;
                        }
                        else
                        {
                            objMDP.file_project_num = ddlTitle.SelectedItem.Value;
                        }
                        objMDP.file_Stage_Number = "5";
                        objMDP.file_name = fileName;
                        string Message = objMDP.save_file_details(objMDP);
                    }
                    ddlTitle_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('files have been uploaded successfully.');", true);
                    //  tab5lblSuccess.Text = string.Format("{0} files have been uploaded successfully.", FileUpload5.PostedFiles.Count);
                    // tab5lblSuccess.Text = "";
                }
                else
                {
                    tab5lblSuccess.Text = "Please select a file.";
                }
                stagesvisibility(objMDP.file_project_num);
                tab_number.Value = "5";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void tab6_UploadMultipleFiles(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (FileUpload6.PostedFiles[0].FileName != "")
                {
                    foreach (HttpPostedFile postedFile in FileUpload6.PostedFiles)
                    {
                        string fileName = Path.GetFileName(postedFile.FileName);
                        FileInfo fi = new FileInfo(fileName);
                        string extension = fi.Extension;
                        string filepart = Path.GetFileNameWithoutExtension(fi.Name);
                        fileName = filepart + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH_mm_ss") + extension;
                        postedFile.SaveAs(Server.MapPath("~/Uploads/") + fileName);
                        if (direct_customer.Checked == true)
                        {
                            objMDP.file_customer_num = ddlCustomerNo.SelectedItem.Value;
                        }
                        else
                        {
                            objMDP.file_customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                            objMDP.file_distributor = ddlDistributorList.SelectedItem.Value;
                        }
                        if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
                        {
                            objMDP.file_project_num = objMDP.new_project_number;
                        }
                        else
                        {
                            objMDP.file_project_num = ddlTitle.SelectedItem.Value;
                        }
                        objMDP.file_Stage_Number = "6";
                        objMDP.file_name = fileName;
                        string Message = objMDP.save_file_details(objMDP);
                    }
                    ddlTitle_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('files have been uploaded successfully.');", true);
                    //   tab6lblSuccess.Text = string.Format("{0} files have been uploaded successfully.", FileUpload6.PostedFiles.Count);
                    //tab6lblSuccess.Text = "";
                }
                else
                {
                    tab6lblSuccess.Text = "Please select a file.";
                }
                stagesvisibility(objMDP.file_project_num);
                tab_number.Value = "6";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void tab7_UploadMultipleFiles(object sender, EventArgs e)
        {
            string fileName;
            FileInfo fi;
            string extension;
            string filepart;
            string Message;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (FileUpload7.PostedFiles[0].FileName != "")
                {
                    foreach (HttpPostedFile postedFile in FileUpload7.PostedFiles)
                    {
                        fileName = Path.GetFileName(postedFile.FileName);
                        fi = new FileInfo(fileName);
                        extension = fi.Extension;
                        filepart = Path.GetFileNameWithoutExtension(fi.Name);
                        fileName = filepart + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH_mm_ss") + extension;
                        postedFile.SaveAs(Server.MapPath("~/Uploads/") + fileName);
                        if (direct_customer.Checked == true)
                        {
                            objMDP.file_customer_num = ddlCustomerNo.SelectedItem.Value;
                        }
                        else
                        {
                            objMDP.file_customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                            objMDP.file_distributor = ddlDistributorList.SelectedItem.Value;
                        }
                        if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
                        {
                            objMDP.file_project_num = objMDP.new_project_number;
                        }
                        else
                        {
                            objMDP.file_project_num = ddlTitle.SelectedItem.Value;
                        }
                        objMDP.file_Stage_Number = "7";
                        objMDP.file_name = fileName;
                        Message = objMDP.save_file_details(objMDP);
                    }
                    ddlTitle_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('files have been uploaded successfully.');", true);
                    //   tab7lblSuccess.Text = string.Format("{0} files have been uploaded successfully.", FileUpload7.PostedFiles.Count);
                    //tab7lblSuccess.Text = "";
                }
                else
                {
                    tab7lblSuccess.Text = "Please select a file.";
                }
                stagesvisibility(objMDP.file_project_num);
                tab_number.Value = "7";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void tab8_UploadMultipleFiles(object sender, EventArgs e)
        {
            string fileName;
            FileInfo fi;
            string extension;
            string filepart;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (FileUpload8.PostedFiles[0].FileName != "")
                {
                    foreach (HttpPostedFile postedFile in FileUpload8.PostedFiles)
                    {
                        fileName = Path.GetFileName(postedFile.FileName);
                        fi = new FileInfo(fileName);
                        extension = fi.Extension;
                        filepart = Path.GetFileNameWithoutExtension(fi.Name);
                        fileName = filepart + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH_mm_ss") + extension;
                        postedFile.SaveAs(Server.MapPath("~/Uploads/") + fileName);
                        if (direct_customer.Checked == true)
                        {
                            objMDP.file_customer_num = ddlCustomerNo.SelectedItem.Value;
                        }
                        else
                        {
                            objMDP.file_customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                            objMDP.file_distributor = ddlDistributorList.SelectedItem.Value;
                        }
                        if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
                        {
                            objMDP.file_project_num = objMDP.new_project_number;
                        }
                        else
                        {
                            objMDP.file_project_num = ddlTitle.SelectedItem.Value;
                        }
                        objMDP.file_Stage_Number = "8";
                        objMDP.file_name = fileName;
                        string Message = objMDP.save_file_details(objMDP);
                    }
                    ddlTitle_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('files have been uploaded successfully.');", true);
                    //  tab8lblSuccess.Text = string.Format("{0} files have been uploaded successfully.", FileUpload8.PostedFiles.Count);
                    //tab8lblSuccess.Text = "";
                }
                else
                {
                    tab8lblSuccess.Text = "Please select a file.";

                }
                stagesvisibility(objMDP.file_project_num);
                tab_number.Value = "8";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void tab9_UploadMultipleFiles(object sender, EventArgs e)
        {
            string fileName;
            FileInfo fi;
            string extension;
            string filepart;
            string Message;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (FileUpload9.PostedFiles[0].FileName != "")
                {
                    foreach (HttpPostedFile postedFile in FileUpload9.PostedFiles)
                    {
                        fileName = Path.GetFileName(postedFile.FileName);
                        fi = new FileInfo(fileName);
                        extension = fi.Extension;
                        filepart = Path.GetFileNameWithoutExtension(fi.Name);
                        fileName = filepart + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH_mm_ss") + extension;
                        postedFile.SaveAs(Server.MapPath("~/Uploads/") + fileName);
                        if (direct_customer.Checked == true)
                        {
                            objMDP.file_customer_num = ddlCustomerNo.SelectedItem.Value;
                        }
                        else
                        {
                            objMDP.file_customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                            objMDP.file_distributor = ddlDistributorList.SelectedItem.Value;
                        }
                        if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
                        {
                            objMDP.file_project_num = objMDP.new_project_number;
                        }
                        else
                        {
                            objMDP.file_project_num = ddlTitle.SelectedItem.Value;
                        }
                        objMDP.file_Stage_Number = "9";
                        objMDP.file_name = fileName;
                        Message = objMDP.save_file_details(objMDP);
                    }
                    ddlTitle_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('files have been uploaded successfully.');", true);
                    //  tab9lblSuccess.Text = string.Format("{0} files have been uploaded successfully.", FileUpload9.PostedFiles.Count);
                    // tab9lblSuccess.Text = "";
                }
                else
                {
                    tab9lblSuccess.Text = "Please select a file.";
                }
                stagesvisibility(objMDP.file_project_num);
                tab_number.Value = "9";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void tab10_UploadMultipleFiles(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (FileUpload10.PostedFiles[0].FileName != "")
                {
                    foreach (HttpPostedFile postedFile in FileUpload10.PostedFiles)
                    {
                        string fileName = Path.GetFileName(postedFile.FileName);
                        FileInfo fi = new FileInfo(fileName);
                        string extension = fi.Extension;
                        string filepart = Path.GetFileNameWithoutExtension(fi.Name);
                        fileName = filepart + "_" + DateTime.Now.ToString("dd-MM-yyyy_HH_mm_ss") + extension;
                        postedFile.SaveAs(Server.MapPath("~/Uploads/") + fileName);
                        if (direct_customer.Checked == true)
                        {
                            objMDP.file_customer_num = ddlCustomerNo.SelectedItem.Value;
                        }
                        else
                        {
                            objMDP.file_customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                            objMDP.file_distributor = ddlDistributorList.SelectedItem.Value;
                        }




                        if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
                        {
                            objMDP.file_project_num = objMDP.new_project_number;

                        }
                        else
                        {
                            objMDP.file_project_num = ddlTitle.SelectedItem.Value;

                        }
                        objMDP.file_Stage_Number = "10";
                        objMDP.file_name = fileName;
                        string Message = objMDP.save_file_details(objMDP);
                    }
                    ddlTitle_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('files have been uploaded successfully.');", true);
                    // tab10lblSuccess.Text = string.Format("{0} files have been uploaded successfully.", FileUpload10.PostedFiles.Count);
                    // tab10lblSuccess.Text = "";
                }
                else
                {
                    tab10lblSuccess.Text = "Please select a file.";
                }
                stagesvisibility(objMDP.file_project_num);
                tab_number.Value = "10";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        #endregion

        /// <summary>
        /// To load the corresponding customer number and type when customer name is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string roleId = Session["RoleId"].ToString();
                if (Session["UserName"] == null || Session["RoleId"] == null)
                { Response.Redirect("Login.aspx?Login"); return; }
                string strUserId = Session["UserId"].ToString();
                string CustomerNumber = ddlCustomerName.SelectedItem.Value;
                //string projectNumber = ddlTitle.SelectedItem.Value;
                string submit = "";
                txtCustomerClass.Text = "";

                if (CustomerNumber == "-- SELECT CUSTOMER --")
                {
                    Enable_NewCustomer(null, null);
                    return;
                }
                ddlCustomerNo.Text = CustomerNumber;

                if (dtCutomerDetails != null)
                {
                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        if (dtCutomerDetails.Rows[i].Field<string>("customer_number").ToString() == CustomerNumber)
                        {
                            string customerClass = dtCutomerDetails.Rows[i].Field<string>("customer_class");
                            if (!string.IsNullOrEmpty(customerClass))
                            {
                                if (customerClass.Contains("KA"))
                                {
                                    customerClass = "KEY FOCUSED CUSTOMER";
                                }
                                else if (customerClass.Contains("KF"))
                                {
                                    customerClass = "‘A’ CLASS CUSTOMER";
                                }
                                else if (customerClass.Contains("OT"))
                                {
                                    customerClass = "OTHERS";
                                }

                                txtCustomerClass.Text = customerClass;
                            }
                            if (dtCutomerDetails.Rows[i].ItemArray[3].ToString() == "D")
                            {
                                ddlDistibutor.Text = "Channel Partner";
                            }
                            else
                            {
                                ddlDistibutor.Text = "Direct";
                            }
                            break;
                        }
                    }
                }
                if (existing_project.Checked == true)
                {
                    DataTable dtProjectTitles = new DataTable();
                    dtProjectTitles = objMDP.LoadProjectTitles(CustomerNumber);
                    if (dtProjectTitles != null)
                    {
                        DataTable dtDeatils = new DataTable();
                        dtDeatils.Columns.Add("project_number", typeof(string));
                        dtDeatils.Columns.Add("project_title", typeof(string));
                        for (int i = 0; i < dtProjectTitles.Rows.Count; i++)
                        {
                            dtDeatils.Rows.Add(dtProjectTitles.Rows[i].ItemArray[0].ToString(), dtProjectTitles.Rows[i].ItemArray[1].ToString());
                        }
                        ddlTitle.Items.Clear();
                        ddlTitle.DataSource = dtDeatils;
                        ddlTitle.DataTextField = "project_title";
                        ddlTitle.DataValueField = "project_number";
                        try
                        {
                            ddlTitle.DataBind();
                        }
                        catch (Exception ex)
                        {
                            objFunc.LogError(ex);
                        }
                        if (dtProjectTitles.Rows.Count == 0)
                        {
                            ddlTitle.Items.Insert(0, "-- NO PROJECT --");
                        }
                        else
                        {
                            ddlTitle.Items.Insert(0, "-- SELECT PROJECT --");
                        }
                    }

                    if (ddlCustomerName.SelectedValue == "-- SELECT CUSTOMER --" && ddlCustomerforDistributor.SelectedValue == "-- SELECT CUSTOMER --")
                    {
                        ddlTitle.Enabled = false;
                    }
                    else
                    {
                        ddlTitle.Enabled = true;
                    }
                }
                string industry_code = objMDP.getCustomer_Industry(CustomerNumber);
                ddlIndustry.SelectedValue = industry_code;
                ddlIndustry.Enabled = false;
                //ddlDistributorList.Enabled = false;
                if (ddlDistributorList.Items.Count > 0)
                    if (ddlDistributorList.Items[0].Text == "-- SELECT CHANNEL PARTNER --")
                        ddlDistributorList.SelectedValue = "-- SELECT CHANNEL PARTNER --";
                    else
                    {
                        ddlDistributorList.Items.Insert(0, "-- SELECT CHANNEL PARTNER --");
                        ddlDistributorList.SelectedValue = "-- SELECT CHANNEL PARTNER --";
                    }
                else
                {
                    ddlDistributorList.Items.Insert(0, "-- SELECT CHANNEL PARTNER --");
                    ddlDistributorList.SelectedValue = "-- SELECT CHANNEL PARTNER --";
                }
                existing_project.Enabled = true;
                new_project.Enabled = true;
                LoadAllProjectTypes();
                LoadProductGroups();
                LoadAllHOIndustry();
                LoadAllMachineMake();
                LoadAllMachineType();
                //anamika
                //existing_project.Checked = true;
                if (roleId == "SE")
                {
                    LoadOwnerbyProject(Convert.ToString(Session["UserName"]), Convert.ToString(Session["UserId"]));
                    LoadOwnerbyCustomer(CustomerNumber);
                    LoadReviewerbyCustomer(CustomerNumber);
                    LoadEscalatorbyCustomer(CustomerNumber);
                    ddlSalesEngName.SelectedValue = GetCustomerSalesEngineer(CustomerNumber);
                    ddlSalesEngName.Enabled = false;
                }
                if (roleId == "BM")
                {
                    LoadOwnerbyProject(Convert.ToString(Session["UserName"]), Convert.ToString(Session["UserId"]));
                    LoadOwnerbyCustomer(CustomerNumber);
                    LoadReviewerbyCustomer(CustomerNumber);
                    LoadEscalatorbyCustomer(CustomerNumber);
                    ddlSalesEngName.SelectedValue = GetCustomerSalesEngineer(CustomerNumber);
                    ddlSalesEngName.Enabled = false;
                }


                if (roleId == "TM")
                {
                    LoadOwnerbyProject(Convert.ToString(Session["UserName"]), Convert.ToString(Session["UserId"]));
                    LoadOwnerbyCustomer(CustomerNumber);
                    LoadReviewerbyCustomer(CustomerNumber);
                    LoadEscalatorbyCustomer(CustomerNumber);
                    ddlSalesEngName.SelectedValue = GetCustomerSalesEngineer(CustomerNumber);
                    ddlSalesEngName.Enabled = false;
                }

                if (roleId == "HO")
                {
                    LoadOwnerbyProject(Convert.ToString(Session["UserName"]), Convert.ToString(Session["UserId"]));
                    LoadOwnerbyCustomer(CustomerNumber);
                    LoadReviewerbyCustomer(CustomerNumber);
                    LoadEscalatorbyCustomer(CustomerNumber);
                    ddlSalesEngName.SelectedValue = GetCustomerSalesEngineer(CustomerNumber);
                    ddlSalesEngName.Enabled = false;
                }

                TSProject_Changes();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (Session["UserName"] == null || Session["RoleId"] == null)
                { Response.Redirect("Login.aspx?Login"); return; }
                string strUserId = Session["UserId"].ToString();
                string CustomerNumber = ddlCustomerName.SelectedItem.Value;
                //string projectNumber = ddlTitle.SelectedItem.Value;
                string submit = "";
                txtCustomerClass.Text = "";
                if (CustomerNumber == "-- SELECT CUSTOMER --")
                {

                    Enable_NewCustomer(null, null);
                    return;
                }

                ddlCustomerNo.Text = CustomerNumber;

                if (dtCutomerDetails != null)
                {
                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        if (dtCutomerDetails.Rows[i].ItemArray[1].ToString() == CustomerNumber)
                        {
                            string customerClass = dtCutomerDetails.Rows[i].Field<string>("customer_class");
                            if (!string.IsNullOrEmpty(customerClass))
                            {
                                if (customerClass.Contains("KA"))
                                {
                                    customerClass = "KEY FOCUSED CUSTOMER";
                                }
                                else if (customerClass.Contains("KF"))
                                {
                                    customerClass = "‘A’ CLASS CUSTOMER";
                                }
                                else if (customerClass.Contains("OT"))
                                {
                                    customerClass = "OTHERS";
                                }

                                txtCustomerClass.Text = customerClass;
                            }
                            if (dtCutomerDetails.Rows[i].ItemArray[3].ToString() == "D")
                            {
                                ddlDistibutor.Text = "Channel Partner";
                            }
                            else
                            {
                                ddlDistibutor.Text = "Customer";
                            }
                            break;
                        }
                    }
                }

                DataTable dtProjectTitles = new DataTable();
                dtProjectTitles = objMDP.LoadProjectTitles(CustomerNumber);
                if (dtProjectTitles != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("project_number", typeof(string));
                    dtDeatils.Columns.Add("project_title", typeof(string));
                    for (int i = 0; i < dtProjectTitles.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtProjectTitles.Rows[i].ItemArray[0].ToString(), dtProjectTitles.Rows[i].ItemArray[1].ToString());
                    }
                    ddlTitle.DataSource = dtDeatils;
                    ddlTitle.DataTextField = "project_title";
                    ddlTitle.DataValueField = "project_number";
                    ddlTitle.DataBind();
                    ddlTitle.Items.Insert(0, "-- SELECT PROJECT --");
                }

                string industry_code = objMDP.getCustomer_Industry(CustomerNumber);
                ddlIndustry.SelectedValue = industry_code;
                ddlIndustry.Enabled = false;
                existing_project.Enabled = true;
                new_project.Enabled = true;
                existing_project.Checked = true;

                TSProject_Changes();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// To load the corresponding customer name and type when customer number is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCustomerNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (Session["UserName"] == null || Session["RoleId"] == null)
                { Response.Redirect("Login.aspx?Login"); return; }
                string strUserId = Session["UserId"].ToString();
                string submit = "";
                txtCustomerClass.Text = "";
                string CustomerNumber = ddlCustomerNo.SelectedItem.Value;
                if (CustomerNumber == "-- SELECT CUSTOMER NUMBER --")
                {

                    Enable_NewCustomer(null, null);
                    return;
                }

                ddlCustomerName.SelectedValue = CustomerNumber;
                if (dtCutomerDetails != null)
                {
                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        if (dtCutomerDetails.Rows[i].ItemArray[1].ToString() == CustomerNumber)
                        {
                            string customerClass = dtCutomerDetails.Rows[i].Field<string>("customer_class");
                            if (!string.IsNullOrEmpty(customerClass))
                            {
                                if (customerClass.Contains("KA"))
                                {
                                    customerClass = "KEY FOCUSED CUSTOMER";
                                }
                                else if (customerClass.Contains("KF"))
                                {
                                    customerClass = "‘A’ CLASS CUSTOMER";
                                }
                                else if (customerClass.Contains("OT"))
                                {
                                    customerClass = "OTHERS";
                                }
                                txtCustomerClass.Text = customerClass;
                            }
                            if (dtCutomerDetails.Rows[i].ItemArray[3].ToString() == "D")
                            {
                                ddlDistibutor.Text = "Channel Partner";
                            }
                            else
                            {
                                ddlDistibutor.Text = "Direct";
                            }
                            break;
                        }
                    }
                }

                DataTable dtProjectTitles = new DataTable();
                dtProjectTitles = objMDP.LoadProjectTitles(CustomerNumber);
                if (dtProjectTitles != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("project_number", typeof(string));
                    dtDeatils.Columns.Add("project_title", typeof(string));


                    for (int i = 0; i < dtProjectTitles.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtProjectTitles.Rows[i].ItemArray[0].ToString(), dtProjectTitles.Rows[i].ItemArray[1].ToString());
                    }
                    ddlTitle.DataSource = dtDeatils;
                    ddlTitle.DataTextField = "project_title";
                    ddlTitle.DataValueField = "project_number";
                    ddlTitle.DataBind();
                    if (dtProjectTitles.Rows.Count == 0)
                    {
                        ddlTitle.Items.Insert(0, "-- NO PROJECT --");

                    }
                    else
                    {
                        ddlTitle.Items.Insert(0, "-- SELECT PROJECT --");
                    }
                }
                string industry_code = objMDP.getCustomer_Industry(CustomerNumber);
                ddlIndustry.SelectedValue = industry_code;
                ddlIndustry.Enabled = false;
                existing_project.Enabled = true;
                new_project.Enabled = true;
                TSProject_Changes();
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void ddlDistributorName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string roleId = Session["RoleId"].ToString();
                DataTable dtnewCustomer = new DataTable();
                //txtdistCustomerName.Enabled = true;
                string distributor = ddlDistributorList.SelectedItem.Value;
                if (distributor != "-- SELECT CHANNEL PARTNER --")
                {
                    objMDP.distributor = distributor;
                    dtnewCustomer = objMDP.LoadCustomers();
                    ddlCustomerforDistributor.Enabled = true;
                }

                if (dtnewCustomer != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_name", typeof(string));


                    for (int i = 0; i < dtnewCustomer.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtnewCustomer.Rows[i].ItemArray[0].ToString(), dtnewCustomer.Rows[i].ItemArray[1].ToString());//+ "(" + dtnewCustomer.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlCustomerforDistributor.DataSource = dtDeatils;
                    ddlCustomerforDistributor.DataTextField = "customer_name";
                    ddlCustomerforDistributor.DataValueField = "customer_number";
                    ddlCustomerforDistributor.DataBind();

                    if (dtnewCustomer.Rows.Count == 0)
                    {
                        ddlCustomerforDistributor.Items.Insert(0, "-- NO CUSTOMER --");
                    }
                    else
                    {
                        ddlCustomerforDistributor.Items.Insert(0, "-- SELECT CUSTOMER --");
                    }
                }


                if (dtnewCustomer.Rows.Count == 0)
                {
                    DataTable dtTitleDetails = new DataTable();
                    ddlTitle.DataSource = dtTitleDetails;
                    ddlTitle.DataTextField = "project_title";
                    ddlTitle.DataValueField = "project_number";
                    ddlTitle.DataBind();
                    ddlTitle.Items.Insert(0, "-- SELECT PROJECT --");
                }


                ddlDistibutor.Text = "Channel Partner";
                ddlIndustry.Enabled = true;
                if (distributor != "-- SELECT CHANNEL PARTNER --")
                {
                    string industry_code = objMDP.getCustomer_Industry(distributor);
                    ddlIndustry.SelectedValue = industry_code;
                }
                else
                {
                    ddlIndustry.SelectedValue = "-- SELECT --";

                }
                ddlIndustry.Enabled = false;
                ddlCustomerName.SelectedValue = "-- SELECT CUSTOMER --";
                // ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;

                LoadOwnerbyProject(Convert.ToString(Session["UserName"]), Convert.ToString(Session["UserId"]));
                if (roleId == "SE")
                {
                    LoadOwnerbyCustomer(distributor);
                    LoadReviewerbyCustomer(distributor);
                    LoadEscalatorbyCustomer(distributor);
                    if (distributor != "-- SELECT CHANNEL PARTNER --")
                    {
                        ddlSalesEngName.SelectedValue = GetCustomerSalesEngineer(distributor);
                    }
                    else
                    {
                        ddlSalesEngName.SelectedValue = "-- SELECT --";
                    }
                    ddlSalesEngName.Enabled = false;
                }
                if (roleId == "BM")
                {
                    LoadOwnerbyCustomer(distributor);
                    LoadReviewerbyCustomer(distributor);
                    LoadEscalatorbyCustomer(distributor);
                    if (distributor != "-- SELECT CHANNEL PARTNER --")
                    {
                        ddlSalesEngName.SelectedValue = GetCustomerSalesEngineer(distributor);
                    }
                    else
                    {
                        ddlSalesEngName.SelectedValue = "-- SELECT --";
                    }
                    ddlSalesEngName.Enabled = false;
                }


                if (roleId == "TM")
                {
                    LoadOwnerbyCustomer(distributor);
                    LoadReviewerbyCustomer(distributor);
                    LoadEscalatorbyCustomer(distributor);
                    if (distributor != "-- SELECT CHANNEL PARTNER --")
                    {
                        ddlSalesEngName.SelectedValue = GetCustomerSalesEngineer(distributor);
                    }
                    else
                    {
                        ddlSalesEngName.SelectedValue = "-- SELECT --";
                    }
                    ddlSalesEngName.Enabled = false;
                }

                if (roleId == "HO")
                {
                    LoadOwnerbyCustomer(distributor);
                    LoadReviewerbyCustomer(distributor);
                    LoadEscalatorbyCustomer(distributor);
                    if (distributor != "-- SELECT CHANNEL PARTNER --")
                    {
                        ddlSalesEngName.SelectedValue = GetCustomerSalesEngineer(distributor);
                    }
                    else
                    {
                        ddlSalesEngName.SelectedValue = "-- SELECT --";
                    }
                    ddlSalesEngName.Enabled = false;
                }
                TSProject_Changes();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void ddlCustomerforDistributor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string customer = ddlCustomerforDistributor.SelectedItem.Value;
                if (existing_project.Checked == true)
                {

                    // existing_project_CheckedChanged(null,null);
                }
                DataTable dtProjectTitles = new DataTable();
                dtProjectTitles = objMDP.LoadProjectTitles(customer);

                string customerClass = objMDP.GetCustomerClass_from_CRP(customer);
                if (!string.IsNullOrEmpty(customerClass))
                {
                    if (customerClass.Contains("KA"))
                    {
                        customerClass = "KEY FOCUSED CUSTOMER";
                    }
                    else if (customerClass.Contains("KF"))
                    {
                        customerClass = "‘A’ CLASS CUSTOMER";
                    }
                    else if (customerClass.Contains("OT"))
                    {
                        customerClass = "OTHERS";
                    }
                }
                txtCustomerClass.Text = customerClass;
                if (dtProjectTitles != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("project_number", typeof(string));
                    dtDeatils.Columns.Add("project_title", typeof(string));


                    for (int i = 0; i < dtProjectTitles.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtProjectTitles.Rows[i].ItemArray[0].ToString(), dtProjectTitles.Rows[i].ItemArray[1].ToString());
                    }
                    ddlTitle.DataSource = dtDeatils;
                    ddlTitle.DataTextField = "project_title";
                    ddlTitle.DataValueField = "project_number";
                    ddlTitle.DataBind();
                    if (dtProjectTitles.Rows.Count == 0)
                    {
                        ddlTitle.Items.Insert(0, "-- NO PROJECT --");

                    }
                    else
                    {
                        ddlTitle.Items.Insert(0, "-- SELECT PROJECT --");
                    }
                }
                TSProject_Changes();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        #region Adding New Remarks
        protected void tab1AddOneMoreRemark(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddlTitle_SelectedIndexChanged(null, null);
                // int count = tab1Remarks.Controls.Count;
                string CustomerNumber = "";

                if (direct_customer.Checked == true)
                {
                    CustomerNumber = ddlCustomerName.SelectedItem.Value;
                }
                else
                {
                    CustomerNumber = ddlCustomerforDistributor.SelectedItem.Value;
                }
                string projectNumber = ddlTitle.SelectedItem.Value;
                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(CustomerNumber, "1", projectNumber);
                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                if (tab1Remarks.FindControl("tab1txtRemarks_" + TextBox_Count) == null)
                {

                    TextBox textbox = new TextBox();
                    textbox.ID = "tab1txtRemarks_" + TextBox_Count;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab1Remarks.Controls.Add(textbox);
                    tab1_collection.Add(textbox.ID);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab1Remarks.Controls.Add(para);
                }

                //tab1btnSave.Enabled = true;
                tab1btnRemarks.Visible = false;
                tab1saveRemarks.Visible = true;

                ViewState["Generated"] = "true";
                tab1_TextBoxIdCollection = tab1_collection;
                tab_number.Value = "1";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab2AddOneMoreRemark(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddlTitle_SelectedIndexChanged(null, null);
                string CustomerNumber = "";

                if (direct_customer.Checked == true)
                {
                    CustomerNumber = ddlCustomerName.SelectedItem.Value;
                }
                else
                {
                    CustomerNumber = ddlCustomerforDistributor.SelectedItem.Value;
                }
                string projectNumber = ddlTitle.SelectedItem.Value;
                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(CustomerNumber, "2", projectNumber);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }
                if (tab2Remarks.FindControl("tab2txtRemarks_" + TextBox_Count) == null)
                {
                    TextBox textbox = new TextBox();
                    textbox.ID = "tab2txtRemarks_" + TextBox_Count;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab2Remarks.Controls.Add(textbox);
                    tab2_collection.Add(textbox.ID);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab2Remarks.Controls.Add(para);
                }

                //tab1btnSave.Enabled = true;
                tab2btnRemarks.Visible = false;
                tab2saveRemarks.Visible = true;

                ViewState["Generated"] = "true";
                tab2_TextBoxIdCollection = tab2_collection;
                tab_number.Value = "2";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab3AddOneMoreRemark(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddlTitle_SelectedIndexChanged(null, null);
                string CustomerNumber = "";

                if (direct_customer.Checked == true)
                {
                    CustomerNumber = ddlCustomerName.SelectedItem.Value;
                }
                else
                {
                    CustomerNumber = ddlCustomerforDistributor.SelectedItem.Value;
                }
                string projectNumber = ddlTitle.SelectedItem.Value;
                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(CustomerNumber, "3", projectNumber);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }
                if (tab3Remarks.FindControl("tab3txtRemarks_" + TextBox_Count) == null)
                {
                    TextBox textbox = new TextBox();
                    textbox.ID = "tab3txtRemarks_" + TextBox_Count;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab3Remarks.Controls.Add(textbox);
                    tab3_collection.Add(textbox.ID);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab3Remarks.Controls.Add(para);
                }

                //tab1btnSave.Enabled = true;
                tab3btnRemarks.Visible = false;
                tab3saveRemarks.Visible = true;

                ViewState["Generated"] = "true";
                tab3_TextBoxIdCollection = tab3_collection;
                tab_number.Value = "3";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab4AddOneMoreRemark(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddlTitle_SelectedIndexChanged(null, null);
                string CustomerNumber = "";

                if (direct_customer.Checked == true)
                {
                    CustomerNumber = ddlCustomerName.SelectedItem.Value;
                }
                else
                {
                    CustomerNumber = ddlCustomerforDistributor.SelectedItem.Value;
                }
                string projectNumber = ddlTitle.SelectedItem.Value;
                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(CustomerNumber, "4", projectNumber);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }
                if (tab4Remarks.FindControl("tab4txtRemarks_" + TextBox_Count) == null)
                {
                    TextBox textbox = new TextBox();
                    textbox.ID = "tab4txtRemarks_" + TextBox_Count;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab4Remarks.Controls.Add(textbox);
                    tab4_collection.Add(textbox.ID);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab4Remarks.Controls.Add(para);
                }
                tab4btnRemarks.Visible = false;
                tab4saveRemarks.Visible = true;
                ViewState["Generated"] = "true";
                tab4_TextBoxIdCollection = tab4_collection;
                tab_number.Value = "4";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab5AddOneMoreRemark(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddlTitle_SelectedIndexChanged(null, null);
                string CustomerNumber = "";

                if (direct_customer.Checked == true)
                {
                    CustomerNumber = ddlCustomerName.SelectedItem.Value;
                }
                else
                {
                    CustomerNumber = ddlCustomerforDistributor.SelectedItem.Value;
                }
                string projectNumber = ddlTitle.SelectedItem.Value;
                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(CustomerNumber, "5", projectNumber);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }
                if (tab5Remarks.FindControl("tab5txtRemarks_" + TextBox_Count) == null)
                {
                    TextBox textbox = new TextBox();
                    textbox.ID = "tab5txtRemarks_" + TextBox_Count;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab5Remarks.Controls.Add(textbox);
                    tab5_collection.Add(textbox.ID);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab5Remarks.Controls.Add(para);

                }

                tab5btnRemarks.Visible = false;
                tab5saveRemarks.Visible = true;
                ViewState["Generated"] = "true";

                tab5_TextBoxIdCollection = tab5_collection;
                tab_number.Value = "5";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab6AddOneMoreRemark(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddlTitle_SelectedIndexChanged(null, null);
                string CustomerNumber = "";

                if (direct_customer.Checked == true)
                {
                    CustomerNumber = ddlCustomerName.SelectedItem.Value;
                }
                else
                {
                    CustomerNumber = ddlCustomerforDistributor.SelectedItem.Value;
                }
                string projectNumber = ddlTitle.SelectedItem.Value;
                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(CustomerNumber, "6", projectNumber);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }
                if (tab6Remarks.FindControl("tab6txtRemarks_" + TextBox_Count) == null)
                {
                    TextBox textbox = new TextBox();
                    textbox.ID = "tab6txtRemarks_" + TextBox_Count;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab6Remarks.Controls.Add(textbox);
                    tab6_collection.Add(textbox.ID);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab6Remarks.Controls.Add(para);

                }
                tab6btnRemarks.Visible = false;
                tab6saveRemarks.Visible = true;
                ViewState["Generated"] = "true";



                tab6_TextBoxIdCollection = tab6_collection;
                tab_number.Value = "6";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab7AddOneMoreRemark(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddlTitle_SelectedIndexChanged(null, null);
                string CustomerNumber = "";

                if (direct_customer.Checked == true)
                {
                    CustomerNumber = ddlCustomerName.SelectedItem.Value;
                }
                else
                {
                    CustomerNumber = ddlCustomerforDistributor.SelectedItem.Value;
                }
                string projectNumber = ddlTitle.SelectedItem.Value;
                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(CustomerNumber, "7", projectNumber);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }
                if (tab7Remarks.FindControl("tab7txtRemarks_" + TextBox_Count) == null)
                {
                    TextBox textbox = new TextBox();
                    textbox.ID = "tab7txtRemarks_" + TextBox_Count;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab7Remarks.Controls.Add(textbox);
                    tab7_collection.Add(textbox.ID);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab7Remarks.Controls.Add(para);

                }
                tab7btnRemarks.Visible = false;
                tab7saveRemarks.Visible = true;
                ViewState["Generated"] = "true";



                tab7_TextBoxIdCollection = tab7_collection;
                tab_number.Value = "7";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab8AddOneMoreRemark(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddlTitle_SelectedIndexChanged(null, null);
                string CustomerNumber = "";

                if (direct_customer.Checked == true)
                {
                    CustomerNumber = ddlCustomerName.SelectedItem.Value;
                }
                else
                {
                    CustomerNumber = ddlCustomerforDistributor.SelectedItem.Value;
                }
                string projectNumber = ddlTitle.SelectedItem.Value;
                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(CustomerNumber, "8", projectNumber);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }
                if (tab8Remarks.FindControl("tab8txtRemarks_" + TextBox_Count) == null)
                {
                    TextBox textbox = new TextBox();
                    textbox.ID = "tab8txtRemarks_" + TextBox_Count;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab8Remarks.Controls.Add(textbox);
                    tab8_collection.Add(textbox.ID);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab8Remarks.Controls.Add(para);
                }
                tab8btnRemarks.Visible = false;
                tab8saveRemarks.Visible = true;
                ViewState["Generated"] = "true";


                tab8_TextBoxIdCollection = tab8_collection;
                tab_number.Value = "8";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab9AddOneMoreRemark(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddlTitle_SelectedIndexChanged(null, null);
                string CustomerNumber = "";

                if (direct_customer.Checked == true)
                {
                    CustomerNumber = ddlCustomerName.SelectedItem.Value;
                }
                else
                {
                    CustomerNumber = ddlCustomerforDistributor.SelectedItem.Value;
                }
                string projectNumber = ddlTitle.SelectedItem.Value;
                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(CustomerNumber, "9", projectNumber);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }
                if (tab9Remarks.FindControl("tab9txtRemarks_" + TextBox_Count) == null)
                {
                    TextBox textbox = new TextBox();
                    textbox.ID = "tab9txtRemarks_" + TextBox_Count;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab9Remarks.Controls.Add(textbox);
                    tab9_collection.Add(textbox.ID);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab9Remarks.Controls.Add(para);

                }
                tab9btnRemarks.Visible = false;
                tab9saveRemarks.Visible = true;
                ViewState["Generated"] = "true";
                tab9_TextBoxIdCollection = tab9_collection;
                tab_number.Value = "9";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab10AddOneMoreRemark(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddlTitle_SelectedIndexChanged(null, null);
                string CustomerNumber = "";

                if (direct_customer.Checked == true)
                {
                    CustomerNumber = ddlCustomerName.SelectedItem.Value;
                }
                else
                {
                    CustomerNumber = ddlCustomerforDistributor.SelectedItem.Value;
                }
                string projectNumber = ddlTitle.SelectedItem.Value;
                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(CustomerNumber, "10", projectNumber);
                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }
                if (tab10Remarks.FindControl("tab10txtRemarks_" + TextBox_Count) == null)
                {
                    TextBox textbox = new TextBox();
                    textbox.ID = "tab10txtRemarks_" + TextBox_Count;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab10Remarks.Controls.Add(textbox);
                    tab10_collection.Add(textbox.ID);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab10Remarks.Controls.Add(para);
                }
                tab10btnRemarks.Visible = false;
                tab10saveRemarks.Visible = true;
                ViewState["Generated"] = "true";
                tab10_TextBoxIdCollection = tab10_collection;
                tab_number.Value = "10";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        private List<string> tab1_TextBoxIdCollection
        {
            get
            {
                var collection = ViewState["TextBoxIdCollection1"] as List<string>;
                return collection ?? new List<string>();
            }
            set { ViewState["TextBoxIdCollection1"] = value; }
        }
        private List<string> tab2_TextBoxIdCollection
        {
            get
            {
                var collection = ViewState["TextBoxIdCollection2"] as List<string>;
                return collection ?? new List<string>();
            }
            set { ViewState["TextBoxIdCollection2"] = value; }
        }
        private List<string> tab3_TextBoxIdCollection
        {
            get
            {
                var collection = ViewState["TextBoxIdCollection3"] as List<string>;
                return collection ?? new List<string>();
            }
            set { ViewState["TextBoxIdCollection3"] = value; }
        }
        private List<string> tab4_TextBoxIdCollection
        {
            get
            {
                var collection = ViewState["TextBoxIdCollection4"] as List<string>;
                return collection ?? new List<string>();
            }
            set { ViewState["TextBoxIdCollection4"] = value; }
        }
        private List<string> tab5_TextBoxIdCollection
        {
            get
            {
                var collection = ViewState["TextBoxIdCollection5"] as List<string>;
                return collection ?? new List<string>();
            }
            set { ViewState["TextBoxIdCollection5"] = value; }
        }
        private List<string> tab6_TextBoxIdCollection
        {
            get
            {
                var collection = ViewState["TextBoxIdCollection6"] as List<string>;
                return collection ?? new List<string>();
            }
            set { ViewState["TextBoxIdCollection6"] = value; }
        }
        private List<string> tab7_TextBoxIdCollection
        {
            get
            {
                var collection = ViewState["TextBoxIdCollection7"] as List<string>;
                return collection ?? new List<string>();
            }
            set { ViewState["TextBoxIdCollection7"] = value; }
        }
        private List<string> tab8_TextBoxIdCollection
        {
            get
            {
                var collection = ViewState["TextBoxIdCollection8"] as List<string>;
                return collection ?? new List<string>();
            }
            set { ViewState["TextBoxIdCollection8"] = value; }
        }
        private List<string> tab9_TextBoxIdCollection
        {
            get
            {
                var collection = ViewState["TextBoxIdCollection9"] as List<string>;
                return collection ?? new List<string>();
            }
            set { ViewState["TextBoxIdCollection9"] = value; }
        }
        private List<string> tab10_TextBoxIdCollection
        {
            get
            {
                var collection = ViewState["TextBoxIdCollection10"] as List<string>;
                return collection ?? new List<string>();
            }
            set { ViewState["TextBoxIdCollection10"] = value; }
        }
        #endregion

        #region Initiate All Stages
        protected void tab1Initiate_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            try
            {
                string branchCode = "";
                tab1Initiate.Visible = false;
                tab2Initiate.Visible = false;
                tab3Initiate.Visible = false;
                tab4Initiate.Visible = false;
                tab5Initiate.Visible = false;
                tab6Initiate.Visible = false;
                tab7Initiate.Visible = false;
                tab8Initiate.Visible = false;
                tab9Initiate.Visible = false;
                tab10Initiate.Visible = false;
                //string customer_nameonly = "";
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }

                #region New Project

                if (new_project.Checked)
                //&& (ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --" || ddlTitle.SelectedItem.Value == "-- NO PROJECT --"))
                {
                    //string latest_project_number = "";
                    int max_projectnum;
                    if (direct_customer.Checked == true)
                    {
                        max_projectnum = Convert.ToInt32(objMDP.getLatest_project(ddlCustomerNo.SelectedItem.Value));
                    }
                    else
                    {
                        max_projectnum = Convert.ToInt32(objMDP.getLatest_project(ddlCustomerforDistributor.SelectedItem.Value));
                    }

                    objMDP.new_project_number = objMDP.customer_num + "_" + max_projectnum;

                }

                #endregion
                if (new_project.Checked)
                //&& (ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --" || ddlTitle.SelectedItem.Value == "-- NO PROJECT --"))
                {
                    objMDP.project_number = objMDP.new_project_number;
                    objMDP.project_title = txtTitle.Text;
                }
                else //if (ddlTitle.SelectedItem.Value != "-- SELECT PROJECT --" && ddlTitle.SelectedItem.Value != "-- NO PROJECT --") // Edited by aswini , 09/01/2016
                {
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                }


                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "1", objMDP.project_number);
                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }
                for (int i = 1; i <= TextBox_Count; i++)
                {
                    objMDP.Industry = ddlIndustry.SelectedItem.Value;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    if (Business.Text == "")
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    }
                    if (Potential.Text == "")
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    }
                    objMDP.Goal = tab1txtGoal.Text;
                    objMDP.Stage_Target_Date = tab1Target.Text;
                    objMDP.Completion_Date = tab1Completion.Text;
                    objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 1;
                    objMDP.IsSubmitted = "0";
                    objMDP.Date_Created = DateTime.Now.ToString("dd/MM/yyyy HH:mm tt");
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab1_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            objMDP.IsInitiated = "1";
                        }
                        else
                        {
                            objMDP.IsInitiated = "1";
                        }
                    }

                    objMDP.BranchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);
                    try
                    {
                        if (tab1Remarks.FindControl("tab1txtRemarks_" + i.ToString()) != null)
                        {
                            TextBox textbox = (TextBox)tab1Remarks.FindControl("tab1txtRemarks_" + i.ToString());
                            objMDP.Remarks = textbox.Text;
                            objMDP.Remark_Number = i;
                            textbox.Enabled = false;
                            if (rdbtsProject.Checked)
                            {
                                objMDP.ts_project_flag = "Y";
                                if (txtOrderTargetCY.Text == "")
                                {
                                    objMDP.OrderTarget_CY = Convert.ToDecimal(0.0);
                                }
                                else
                                {
                                    objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                                }
                                if (txtOrderTargetNY.Text == "")
                                {
                                    objMDP.OrderTarget_NY = Convert.ToDecimal(0.0);
                                }
                                else
                                {
                                    objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                                }


                            }
                            string Messege = objMDP.insert_mdpinfo(objMDP);
                        }
                    }
                    catch (Exception ex)
                    {

                    }


                    string customerName = ddlCustomerName.SelectedItem.Text;
                    if (channel_partner.Checked == true)
                    {
                        customerName = ddlCustomerforDistributor.SelectedItem.Text;
                    }
                    tab_number.Value = "1";

                    SendmailAtInitiateProject(objMDP.project_title, objMDP.project_number, customerName, Convert.ToString(ddlSalesEngName.SelectedValue), objMDP.project_owner, objMDP.Reviewer,objMDP.EscalatedTo);


                }
                existing_project.Checked = true;
                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.Enabled = false;

                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);


                }


                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                txtTitle.Enabled = false;

                if (ddlTitle.Items.FindByValue(objMDP.project_number.ToString().Trim()) != null)
                {
                    ddlTitle.SelectedValue = objMDP.project_number.ToString().Trim();
                }
                ddlTitle.Enabled = false;

                FileUpload1.Enabled = true;
                tab1btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Initiated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Oops Something went wrong.. try again');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
        }

        protected void tab2Initiate_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                tab1Initiate.Visible = false;
                tab2Initiate.Visible = false;
                tab3Initiate.Visible = false;
                tab4Initiate.Visible = false;
                tab5Initiate.Visible = false;
                tab6Initiate.Visible = false;
                tab7Initiate.Visible = false;
                tab8Initiate.Visible = false;
                tab9Initiate.Visible = false;
                tab10Initiate.Visible = false;
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value); ;
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;
                objMDP.project_title = ddlTitle.SelectedItem.Text;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "2", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    if (Business.Text == "")
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    }
                    if (Potential.Text == "")
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    }
                    objMDP.Goal = tab2txtGoal.Text;
                    objMDP.Stage_Target_Date = tab2Target.Text;
                    objMDP.Completion_Date = tab2Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 2;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab2_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            objMDP.IsInitiated = "1";
                        }
                        else
                        {
                            objMDP.IsInitiated = "1";
                        }
                    }

                    if (tab2Remarks.FindControl("tab2txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab2Remarks.FindControl("tab2txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            if (txtOrderTargetCY.Text == "")
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            }
                            if (txtOrderTargetNY.Text == "")
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                            }
                            objMDP.project_ref = Convert.ToString(tab2ProjectNumber.Text);
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }

                   
                    string customerName = ddlCustomerName.SelectedItem.Text;
                    if (channel_partner.Checked == true)
                    {
                        customerName = ddlCustomerforDistributor.SelectedItem.Text;
                    }

                    tab_number.Value = "2";
                    SendmailAtInitiateProject(objMDP.project_title, objMDP.project_number, customerName, Convert.ToString(ddlSalesEngName.SelectedValue), objMDP.project_owner, objMDP.Reviewer, objMDP.EscalatedTo);

                }
                existing_project.Checked = true;
                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.Enabled = false;

                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);


                }


                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                txtTitle.Enabled = false;

                if (ddlTitle.Items.FindByValue(objMDP.project_number.ToString().Trim()) != null)
                {
                    ddlTitle.SelectedValue = objMDP.project_number.ToString().Trim();
                }
                ddlTitle.Enabled = false;
                //tab2btnSave.Enabled = false;
                FileUpload2.Enabled = true;
                tab2btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Initiated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab3Initiate_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                tab1Initiate.Visible = false;
                tab2Initiate.Visible = false;
                tab3Initiate.Visible = false;
                tab4Initiate.Visible = false;
                tab5Initiate.Visible = false;
                tab6Initiate.Visible = false;
                tab7Initiate.Visible = false;
                tab8Initiate.Visible = false;
                tab9Initiate.Visible = false;
                tab10Initiate.Visible = false;
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value); ;
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;
                objMDP.project_title = ddlTitle.SelectedItem.Text;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "3", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    if (Business.Text == "")
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    }
                    if (Potential.Text == "")
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    }
                    objMDP.Goal = tab3txtGoal.Text;
                    objMDP.Stage_Target_Date = tab3Target.Text;
                    objMDP.Completion_Date = tab3Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 3;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab3_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            objMDP.IsInitiated = "1";
                        }
                        else
                        {
                            objMDP.IsInitiated = "1";
                        }
                    }

                    if (tab3Remarks.FindControl("tab3txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab3Remarks.FindControl("tab3txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            if (txtOrderTargetCY.Text == "")
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            }
                            if (txtOrderTargetNY.Text == "")
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                            }
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }


                    string customerName = ddlCustomerName.SelectedItem.Text;
                    if (channel_partner.Checked == true)
                    {
                        customerName = ddlCustomerforDistributor.SelectedItem.Text;
                    }

                    tab_number.Value = "3";
                    SendmailAtInitiateProject(objMDP.project_title, objMDP.project_number, customerName, Convert.ToString(ddlSalesEngName.SelectedValue), objMDP.project_owner, objMDP.Reviewer, objMDP.EscalatedTo);

                }
                existing_project.Checked = true;
                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.Enabled = false;

                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);


                }


                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                txtTitle.Enabled = false;

                if (ddlTitle.Items.FindByValue(objMDP.project_number.ToString().Trim()) != null)
                {
                    ddlTitle.SelectedValue = objMDP.project_number.ToString().Trim();
                }
                ddlTitle.Enabled = false;
                FileUpload3.Enabled = true;
                tab3btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Initiated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab4Initiate_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                tab1Initiate.Visible = false;
                tab2Initiate.Visible = false;
                tab3Initiate.Visible = false;
                tab4Initiate.Visible = false;
                tab5Initiate.Visible = false;
                tab6Initiate.Visible = false;
                tab7Initiate.Visible = false;
                tab8Initiate.Visible = false;
                tab9Initiate.Visible = false;
                tab10Initiate.Visible = false;
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value); ;
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;
                objMDP.project_title = ddlTitle.SelectedItem.Text;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "4", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    if (Business.Text == "")
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    }
                    if (Potential.Text == "")
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    }
                    objMDP.Goal = tab4txtGoal.Text;
                    objMDP.Stage_Target_Date = tab4Target.Text;
                    objMDP.Completion_Date = tab4Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 4;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab4_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            objMDP.IsInitiated = "1";
                        }
                        else
                        {
                            objMDP.IsInitiated = "1";
                        }
                    }

                    if (tab4Remarks.FindControl("tab4txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab4Remarks.FindControl("tab4txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            if (txtOrderTargetCY.Text == "")
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            }
                            if (txtOrderTargetNY.Text == "")
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                            }
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }


                    string customerName = ddlCustomerName.SelectedItem.Text;
                    if (channel_partner.Checked == true)
                    {
                        customerName = ddlCustomerforDistributor.SelectedItem.Text;
                    }

                    tab_number.Value = "4";
                    SendmailAtInitiateProject(objMDP.project_title, objMDP.project_number, customerName, Convert.ToString(ddlSalesEngName.SelectedValue), objMDP.project_owner, objMDP.Reviewer, objMDP.EscalatedTo);

                }
                existing_project.Checked = true;
                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.Enabled = false;

                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);


                }


                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                txtTitle.Enabled = false;

                if (ddlTitle.Items.FindByValue(objMDP.project_number.ToString().Trim()) != null)
                {
                    ddlTitle.SelectedValue = objMDP.project_number.ToString().Trim();
                }
                ddlTitle.Enabled = false;
                FileUpload4.Enabled = true;
                tab4btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Initiated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab5Initiate_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                tab1Initiate.Visible = false;
                tab2Initiate.Visible = false;
                tab3Initiate.Visible = false;
                tab4Initiate.Visible = false;
                tab5Initiate.Visible = false;
                tab6Initiate.Visible = false;
                tab7Initiate.Visible = false;
                tab8Initiate.Visible = false;
                tab9Initiate.Visible = false;
                tab10Initiate.Visible = false;
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value); ;
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;
                objMDP.project_title = ddlTitle.SelectedItem.Text;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "5", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    if (Business.Text == "")
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    }
                    if (Potential.Text == "")
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    }
                    objMDP.Goal = tab5txtGoal.Text;
                    objMDP.Stage_Target_Date = tab5Target.Text;
                    objMDP.Completion_Date = tab5Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 5;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                   
                    if (tab5_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            objMDP.IsInitiated = "1";
                        }
                        else
                        {
                            objMDP.IsInitiated = "1";
                        }
                    }

                    if (tab5Remarks.FindControl("tab5txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab5Remarks.FindControl("tab5txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            if (txtOrderTargetCY.Text == "")
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            }
                            if (txtOrderTargetNY.Text == "")
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                            }
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }


                    string customerName = ddlCustomerName.SelectedItem.Text;
                    if (channel_partner.Checked == true)
                    {
                        customerName = ddlCustomerforDistributor.SelectedItem.Text;
                    }

                    tab_number.Value = "5";
                    SendmailAtInitiateProject(objMDP.project_title, objMDP.project_number, customerName, Convert.ToString(ddlSalesEngName.SelectedValue), objMDP.project_owner, objMDP.Reviewer, objMDP.EscalatedTo);

                }
                existing_project.Checked = true;
                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.Enabled = false;

                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);


                }


                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                txtTitle.Enabled = false;

                if (ddlTitle.Items.FindByValue(objMDP.project_number.ToString().Trim()) != null)
                {
                    ddlTitle.SelectedValue = objMDP.project_number.ToString().Trim();
                }
                ddlTitle.Enabled = false;
                //tab2btnSave.Enabled = false;
                FileUpload5.Enabled = true;
                tab5btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Initiated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab6Initiate_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                tab1Initiate.Visible = false;
                tab2Initiate.Visible = false;
                tab3Initiate.Visible = false;
                tab4Initiate.Visible = false;
                tab5Initiate.Visible = false;
                tab6Initiate.Visible = false;
                tab7Initiate.Visible = false;
                tab8Initiate.Visible = false;
                tab9Initiate.Visible = false;
                tab10Initiate.Visible = false;
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value); ;
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;
                objMDP.project_title = ddlTitle.SelectedItem.Text;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "6", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    if (Business.Text == "")
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    }
                    if (Potential.Text == "")
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    }
                    objMDP.Goal = tab6txtGoal.Text;
                    objMDP.Stage_Target_Date = tab6Target.Text;
                    objMDP.Completion_Date = tab6Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 6;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab6_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            objMDP.IsInitiated = "1";
                        }
                        else
                        {
                            objMDP.IsInitiated = "1";
                        }
                    }

                    if (tab6Remarks.FindControl("tab6txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab6Remarks.FindControl("tab6txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            if (txtOrderTargetCY.Text == "")
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            }
                            if (txtOrderTargetNY.Text == "")
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                            }
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }


                    string customerName = ddlCustomerName.SelectedItem.Text;
                    if (channel_partner.Checked == true)
                    {
                        customerName = ddlCustomerforDistributor.SelectedItem.Text;
                    }

                    tab_number.Value = "6";
                    SendmailAtInitiateProject(objMDP.project_title, objMDP.project_number, customerName, Convert.ToString(ddlSalesEngName.SelectedValue), objMDP.project_owner, objMDP.Reviewer, objMDP.EscalatedTo);

                }
                existing_project.Checked = true;
                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.Enabled = false;

                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);


                }


                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                txtTitle.Enabled = false;

                if (ddlTitle.Items.FindByValue(objMDP.project_number.ToString().Trim()) != null)
                {
                    ddlTitle.SelectedValue = objMDP.project_number.ToString().Trim();
                }
                ddlTitle.Enabled = false;
                FileUpload6.Enabled = true;
                tab6btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Initiated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab7Initiate_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                tab1Initiate.Visible = false;
                tab2Initiate.Visible = false;
                tab3Initiate.Visible = false;
                tab4Initiate.Visible = false;
                tab5Initiate.Visible = false;
                tab6Initiate.Visible = false;
                tab7Initiate.Visible = false;
                tab8Initiate.Visible = false;
                tab9Initiate.Visible = false;
                tab10Initiate.Visible = false;
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value); ;
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;
                objMDP.project_title = ddlTitle.SelectedItem.Text;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "7", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    if (Business.Text == "")
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    }
                    if (Potential.Text == "")
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    }
                    objMDP.Goal = tab7txtGoal.Text;
                    objMDP.Stage_Target_Date = tab7Target.Text;
                    objMDP.Completion_Date = tab7Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 7;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab7_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            objMDP.IsInitiated = "1";
                        }
                        else
                        {
                            objMDP.IsInitiated = "1";
                        }
                    }

                    if (tab7Remarks.FindControl("tab7txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab7Remarks.FindControl("tab7txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            if (txtOrderTargetCY.Text == "")
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            }
                            if (txtOrderTargetNY.Text == "")
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                            }
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }


                    string customerName = ddlCustomerName.SelectedItem.Text;
                    if (channel_partner.Checked == true)
                    {
                        customerName = ddlCustomerforDistributor.SelectedItem.Text;
                    }

                    tab_number.Value = "7";
                    SendmailAtInitiateProject(objMDP.project_title, objMDP.project_number, customerName, Convert.ToString(ddlSalesEngName.SelectedValue), objMDP.project_owner, objMDP.Reviewer, objMDP.EscalatedTo);

                }
                existing_project.Checked = true;
                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.Enabled = false;

                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);


                }


                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                txtTitle.Enabled = false;

                if (ddlTitle.Items.FindByValue(objMDP.project_number.ToString().Trim()) != null)
                {
                    ddlTitle.SelectedValue = objMDP.project_number.ToString().Trim();
                }
                ddlTitle.Enabled = false;
                FileUpload7.Enabled = true;
                tab7btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Initiated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab8Initiate_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                tab1Initiate.Visible = false;
                tab2Initiate.Visible = false;
                tab3Initiate.Visible = false;
                tab4Initiate.Visible = false;
                tab5Initiate.Visible = false;
                tab6Initiate.Visible = false;
                tab7Initiate.Visible = false;
                tab8Initiate.Visible = false;
                tab9Initiate.Visible = false;
                tab10Initiate.Visible = false;
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value); ;
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;
                objMDP.project_title = ddlTitle.SelectedItem.Text;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "8", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    if (Business.Text == "")
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    }
                    if (Potential.Text == "")
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    }
                    objMDP.Goal = tab8txtGoal.Text;
                    objMDP.Stage_Target_Date = tab8Target.Text;
                    objMDP.Completion_Date = tab8Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 8;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab8_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            objMDP.IsInitiated = "1";
                        }
                        else
                        {
                            objMDP.IsInitiated = "1";
                        }
                    }

                    if (tab8Remarks.FindControl("tab8txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab8Remarks.FindControl("tab8txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            if (txtOrderTargetCY.Text == "")
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            }
                            if (txtOrderTargetNY.Text == "")
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                            }
                            objMDP.project_ref = Convert.ToString(tab2ProjectNumber.Text);
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }


                    string customerName = ddlCustomerName.SelectedItem.Text;
                    if (channel_partner.Checked == true)
                    {
                        customerName = ddlCustomerforDistributor.SelectedItem.Text;
                    }

                    tab_number.Value = "8";
                    SendmailAtInitiateProject(objMDP.project_title, objMDP.project_number, customerName, Convert.ToString(ddlSalesEngName.SelectedValue), objMDP.project_owner, objMDP.Reviewer, objMDP.EscalatedTo);

                }
                existing_project.Checked = true;
                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.Enabled = false;

                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);


                }


                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                txtTitle.Enabled = false;

                if (ddlTitle.Items.FindByValue(objMDP.project_number.ToString().Trim()) != null)
                {
                    ddlTitle.SelectedValue = objMDP.project_number.ToString().Trim();
                }
                ddlTitle.Enabled = false;
                //tab2btnSave.Enabled = false;
                FileUpload8.Enabled = true;
                tab8btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Initiated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab9Initiate_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                tab1Initiate.Visible = false;
                tab2Initiate.Visible = false;
                tab3Initiate.Visible = false;
                tab4Initiate.Visible = false;
                tab5Initiate.Visible = false;
                tab6Initiate.Visible = false;
                tab7Initiate.Visible = false;
                tab8Initiate.Visible = false;
                tab9Initiate.Visible = false;
                tab10Initiate.Visible = false;
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value); ;
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;
                objMDP.project_title = ddlTitle.SelectedItem.Text;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "9", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    if (Business.Text == "")
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    }
                    if (Potential.Text == "")
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    }
                    objMDP.Goal = tab9txtGoal.Text;
                    objMDP.Stage_Target_Date = tab9Target.Text;
                    objMDP.Completion_Date = tab9Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 9;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab9_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            objMDP.IsInitiated = "1";
                        }
                        else
                        {
                            objMDP.IsInitiated = "1";
                        }
                    }

                    if (tab9Remarks.FindControl("tab9txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab9Remarks.FindControl("tab9txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            if (txtOrderTargetCY.Text == "")
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            }
                            if (txtOrderTargetNY.Text == "")
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                            }
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }


                    string customerName = ddlCustomerName.SelectedItem.Text;
                    if (channel_partner.Checked == true)
                    {
                        customerName = ddlCustomerforDistributor.SelectedItem.Text;
                    }

                    tab_number.Value = "9";
                    SendmailAtInitiateProject(objMDP.project_title, objMDP.project_number, customerName, Convert.ToString(ddlSalesEngName.SelectedValue), objMDP.project_owner, objMDP.Reviewer, objMDP.EscalatedTo);

                }
                existing_project.Checked = true;
                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.Enabled = false;

                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);


                }


                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                txtTitle.Enabled = false;

                if (ddlTitle.Items.FindByValue(objMDP.project_number.ToString().Trim()) != null)
                {
                    ddlTitle.SelectedValue = objMDP.project_number.ToString().Trim();
                }
                ddlTitle.Enabled = false;
                FileUpload9.Enabled = true;
                tab9btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Initiated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab10Initiate_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                tab1Initiate.Visible = false;
                tab2Initiate.Visible = false;
                tab3Initiate.Visible = false;
                tab4Initiate.Visible = false;
                tab5Initiate.Visible = false;
                tab6Initiate.Visible = false;
                tab7Initiate.Visible = false;
                tab8Initiate.Visible = false;
                tab9Initiate.Visible = false;
                tab10Initiate.Visible = false;
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value); ;
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;
                objMDP.project_title = ddlTitle.SelectedItem.Text;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "10", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    if (Business.Text == "")
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    }
                    if (Potential.Text == "")
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(0.0);
                    }
                    else
                    {
                        objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    }
                    objMDP.Goal = tab10txtGoal.Text;
                    objMDP.Stage_Target_Date = tab10Target.Text;
                    objMDP.Completion_Date = tab10Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 10;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                        objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                        objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                        objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab10_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            objMDP.IsInitiated = "1";
                        }
                        else
                        {
                            objMDP.IsInitiated = "1";
                        }
                    }

                    if (tab10Remarks.FindControl("tab10txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab10Remarks.FindControl("tab10txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            if (txtOrderTargetCY.Text == "")
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            }
                            if (txtOrderTargetNY.Text == "")
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(0.0);
                            }
                            else
                            {
                                objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                            }
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }


                    string customerName = ddlCustomerName.SelectedItem.Text;
                    if (channel_partner.Checked == true)
                    {
                        customerName = ddlCustomerforDistributor.SelectedItem.Text;
                    }

                    tab_number.Value = "10";
                    SendmailAtInitiateProject(objMDP.project_title, objMDP.project_number, customerName, Convert.ToString(ddlSalesEngName.SelectedValue), objMDP.project_owner, objMDP.Reviewer, objMDP.EscalatedTo);

                }
                existing_project.Checked = true;
                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.Enabled = false;

                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);


                }


                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                txtTitle.Enabled = false;

                if (ddlTitle.Items.FindByValue(objMDP.project_number.ToString().Trim()) != null)
                {
                    ddlTitle.SelectedValue = objMDP.project_number.ToString().Trim();
                }
                ddlTitle.Enabled = false;
                FileUpload10.Enabled = true;
                tab10btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Initiated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        #endregion

        #region Save All Stages
        /// <summary>
        /// Onclick Method for Stage 1 Save Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void tab1btnSave_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            //Label attachDoc = (Label)tab.FindControl("tab1doc");
            ////   Label tab1Doc = (Label)FindControl("tab" + j + "doc");
            //attachDoc.Visible = true;
            //FileUpload fUpload = (FileUpload)tab.FindControl("FileUpload1");
            //fUpload.Visible = true;
            //Button btnUpload = (Button)tab.FindControl("tab1btnUpload");
            //btnUpload.Visible = true;
            //if (existing_project.Checked == true)
            //{
            //    tab1doc.Visible = false;
            //    FileUpload1.Visible = false;
            //    tab1btnUpload.Visible = false;
            //}

            try
            {
                string branchCode = "";
                //string customer_nameonly = "";
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }

                #region New Project

                if (new_project.Checked)
                //&& (ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --" || ddlTitle.SelectedItem.Value == "-- NO PROJECT --"))
                {
                    //string latest_project_number = "";
                    int max_projectnum;
                    if (direct_customer.Checked == true)
                    {
                        max_projectnum = Convert.ToInt32(objMDP.getLatest_project(ddlCustomerNo.SelectedItem.Value));
                    }
                    else
                    {
                        max_projectnum = Convert.ToInt32(objMDP.getLatest_project(ddlCustomerforDistributor.SelectedItem.Value));
                    }

                    objMDP.new_project_number = objMDP.customer_num + "_" + max_projectnum;

                    //if (latest_project_number == "")
                    //{
                    //    objMDP.new_project_number = objMDP.customer_num + "_" + "1";
                    //}
                    //else
                    //{
                    //    if (latest_project_number.Split('_').Length == 2)//Edited by aswini , 09/01/2016
                    //    {

                    //        objMDP.new_project_number = objMDP.customer_num + "_" + (Convert.ToInt32(latest_project_number.Split('_')[1]) + 1).ToString();
                    //    }
                    //    else { objMDP.new_project_number = objMDP.customer_num + "_" + "1"; }
                    //}

                }

                #endregion
                if (new_project.Checked)
                //&& (ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --" || ddlTitle.SelectedItem.Value == "-- NO PROJECT --"))
                {
                    objMDP.project_number = objMDP.new_project_number;
                    objMDP.project_title = txtTitle.Text;
                }
                else //if (ddlTitle.SelectedItem.Value != "-- SELECT PROJECT --" && ddlTitle.SelectedItem.Value != "-- NO PROJECT --") // Edited by aswini , 09/01/2016
                {
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                }


                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "1", objMDP.project_number);
                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }
                for (int i = 1; i <= TextBox_Count; i++)
                {
                    objMDP.Industry = ddlIndustry.SelectedItem.Value;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab1txtGoal.Text;
                    objMDP.Stage_Target_Date = tab1Target.Text;
                    objMDP.Completion_Date = tab1Completion.Text;
                    objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 1;
                    objMDP.IsSubmitted = "0";
                    objMDP.Date_Created = DateTime.Now.ToString("dd/MM/yyyy HH:mm tt");
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab1_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                        
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            if (dtMDPDetails.Rows[0].ItemArray[27].ToString() != "True")
                            {
                                if (objMDP.NoOfStages == 1)
                                {
                                    objMDP.IsSentForApproval = "1";
                                    objMDP.IsApproved = "0";
                                    objMDP.IsRejected = "0";
                                }
                            }
                        }
                        else
                        {
                            if (objMDP.NoOfStages == 1)
                            {
                                objMDP.IsSentForApproval = "1";
                                objMDP.IsApproved = "0";
                                objMDP.IsRejected = "0";
                            }
                        }
                    }

                    objMDP.BranchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);
                    try
                    {
                        if (tab1Remarks.FindControl("tab1txtRemarks_" + i.ToString()) != null)
                        {
                            TextBox textbox = (TextBox)tab1Remarks.FindControl("tab1txtRemarks_" + i.ToString());
                            objMDP.Remarks = textbox.Text;
                            objMDP.Remark_Number = i;
                            textbox.Enabled = false;
                            if (rdbtsProject.Checked)
                            {
                                objMDP.ts_project_flag = "Y";
                                objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                                objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                            }
                            string Messege = objMDP.insert_mdpinfo(objMDP);
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                    string custNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        custNum = ddlDistributorList.SelectedItem.Value;
                    }
                    string distNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        distNum = ddlCustomerforDistributor.SelectedItem.Value;
                    }

                    tab_number.Value = "1";
                    if (objMDP.NoOfStages == 1)
                    {
                        string customerName = ddlCustomerName.SelectedItem.Text;
                        if (channel_partner.Checked == true)
                        {
                            customerName = ddlCustomerforDistributor.SelectedItem.Text;
                        }
                        if (rdbtsProject.Checked == true)
                        {
                             SendmailToSalesEngineer(objMDP.project_title, objMDP.project_number,customerName, custNum, objMDP.Potential_Lakhs.ToString(), Convert.ToString(ddlSalesEngName.SelectedValue));
                    }
                       
                            SendmailToOwner(objMDP.project_owner, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());
                            SendmailToReviewer(objMDP.Reviewer, objMDP.project_title, objMDP.project_number, customerName, custNum, distNum, objMDP.Potential_Lakhs.ToString());
                            SendmailToEscalateTo(objMDP.EscalatedTo, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());

                     

                    }

                }
                existing_project.Checked = true;
                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.Enabled = false;
                   
                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);


                }


                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                txtTitle.Enabled = false;

                if (ddlTitle.Items.FindByValue(objMDP.project_number.ToString().Trim()) != null)
                {
                    ddlTitle.SelectedValue = objMDP.project_number.ToString().Trim();
                }
                ddlTitle.Enabled = false;
               

                ddlTitle_SelectedIndexChanged(null, null);
                FileUpload1.Enabled = true;
                tab1btnUpload.Enabled = true;
                direct_customer.Enabled = false;
                channel_partner.Enabled = false;
                new_project.Enabled = false;
                existing_project.Enabled = false;


                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Successfully Updated, Now you can upload documents for this project.');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
                existing_project.Checked = true;
                new_project.Checked = false;
                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.Enabled = false;
                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);
                }
                ddlTitle.SelectedValue = objMDP.project_number;
                ddlTitle.Enabled = false;
                ddlTitle_SelectedIndexChanged(null, null);

                tab1saveRemarks.Visible = false;

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Oops Something went wrong.. try again');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
        }

        //protected void tab1btnSave_Click(object sender, EventArgs e)
        //{
        //    string branchCode = "";

        //    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
        //    branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

        //    if (direct_customer.Checked == true)
        //    {
        //        objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
        //        objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
        //    }
        //    else
        //    {
        //        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
        //        objMDP.distributor = ddlDistributorList.SelectedItem.Value;
        //        objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
        //    }


        //    #region New Project

        //    if (new_project.Checked && (ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --" || ddlTitle.SelectedItem.Value == "-- NO PROJECT --"))
        //    {
        //        string latest_project_number = "";

        //        if (direct_customer.Checked == true)
        //        {
        //            latest_project_number = objMDP.getLatest_project(ddlCustomerNo.SelectedItem.Value);
        //        }
        //        else
        //        {
        //            latest_project_number = objMDP.getLatest_project(ddlCustomerforDistributor.SelectedItem.Value);
        //        }

        //        if (latest_project_number == "")
        //        {
        //            objMDP.new_project_number = objMDP.customer_num + "_" + "1";
        //        }
        //        else
        //        {

        //            objMDP.new_project_number = objMDP.customer_num + "_" + (Convert.ToInt32(latest_project_number.Split('_')[1]) + 1).ToString();
        //        }

        //    }
        //    #endregion
        //    if (new_project.Checked && (ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --" || ddlTitle.SelectedItem.Value == "-- NO PROJECT --"))
        //    {
        //        objMDP.project_number = objMDP.new_project_number;
        //        objMDP.project_title = txtTitle.Text;
        //    }
        //    else
        //    {
        //        objMDP.project_number = ddlTitle.SelectedItem.Value;
        //        objMDP.project_title = ddlTitle.SelectedItem.Text;
        //    }


        //    DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "1", objMDP.project_number);


        //    int TextBox_Count = 0;
        //    if (dtMDP_Remarks != null)
        //    {
        //        TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
        //    }
        //    else
        //    {
        //        TextBox_Count = 2;
        //    }


        //    for (int i = 1; i <= TextBox_Count; i++)
        //    {





        //        objMDP.Industry = ddlIndustry.SelectedItem.Value;
        //        objMDP.Existing_Product = ExistingProduct.Text;
        //        objMDP.Component = Component.Text;
        //        objMDP.Competition = CompetitionSpec.Text;
        //        objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
        //        objMDP.Target_Date = TargetDate.Text;
        //        objMDP.OverAll_Potential = Convert.ToDecimal(OverAllPotential.Text);
        //        objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
        //        objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
        //        objMDP.Goal = tab1txtGoal.Text;
        //        objMDP.Stage_Target_Date = tab1Target.Text;
        //        objMDP.Completion_Date = tab1Completion.Text;
        //        objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
        //        objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
        //        objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
        //        objMDP.Stage_Number = 1;
        //        objMDP.IsSubmitted = "0";
        //        objMDP.Date_Created = DateTime.Now.ToString("dd/MM/yyyy HH:mm tt");
        //        objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
        //        DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
        //        if (dtMDPDetails != null)
        //        {
        //            if (dtMDPDetails.Rows.Count > 0)
        //            {
        //                if (dtMDPDetails.Rows[0].ItemArray[27].ToString() != "True")
        //                {
        //                    if (objMDP.NoOfStages == 1)
        //                    {
        //                        objMDP.IsSentForApproval = "1";
        //                        objMDP.IsApproved = "0";
        //                        objMDP.IsRejected = "0";
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (objMDP.NoOfStages == 1)
        //                {
        //                    objMDP.IsSentForApproval = "1";
        //                    objMDP.IsApproved = "0";
        //                    objMDP.IsRejected = "0";
        //                }

        //            }
        //        }



        //        objMDP.BranchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);
        //        try
        //        {
        //            if (tab1Remarks.FindControl("tab1txtRemarks_" + i.ToString()) != null)
        //            {
        //                TextBox textbox = (TextBox)tab1Remarks.FindControl("tab1txtRemarks_" + i.ToString());
        //                objMDP.Remarks = textbox.Text;
        //                objMDP.Remark_Number = i;
        //                textbox.Enabled = false;
        //                string Messege = objMDP.insert_mdpinfo(objMDP);
        //            }
        //        }
        //        catch (Exception ex)
        //        {

        //        }

        //        tab_number.Value = "1";
        //        if (objMDP.NoOfStages == 1)
        //        {
        //            string customerName=ddlCustomerName.SelectedItem.Text;
        //            if(channel_partner.Checked==true){
        //                customerName = ddlCustomerforDistributor.SelectedItem.Text;
        //            }
        //            SendmailToOwner(objMDP.Sales_Eng_Id, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());
        //            SendmailToReviewer(objMDP.Reviewer, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());

        //        }

        //    }



        //    if (channel_partner.Checked == true)
        //    {
        //        ddlDistributorList.SelectedValue = objMDP.distributor;
        //        ddlDistributorName_SelectedIndexChanged(null, null);
        //        ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
        //        ddlCustomerforDistributor_SelectedIndexChanged(null, null);
        //        ddlCustomerforDistributor.Enabled = false;
        //    }
        //    else
        //    {
        //        ddlCustomerName.SelectedValue = objMDP.customer_num;
        //        ddlCustomerName_SelectedIndexChanged(null, null);


        //    }


        //    ddlCustomerName.Enabled = false;
        //    ddlCustomerNo.Enabled = false;
        //    txtTitle.Enabled = false;
        //    ddlTitle.SelectedValue = objMDP.project_number;
        //    ddlTitle.Enabled = false;
        //    ddlTitle_SelectedIndexChanged(null, null);
        //    //tab1btnSave.Enabled = false;
        //    FileUpload1.Enabled = true;
        //    tab1btnUpload.Enabled = true;
        //    direct_customer.Enabled = false;
        //    channel_partner.Enabled = false;
        //    new_project.Enabled = false;
        //    existing_project.Enabled = false;
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Updated, Now you can upload documents for this project.');", true);
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
        //}

        /// <summary>
        /// Onclick Method for Stage 2 Save Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab2btnSave_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value); ;
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;
                objMDP.project_title = ddlTitle.SelectedItem.Text;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "2", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab2txtGoal.Text;
                    objMDP.Stage_Target_Date = tab2Target.Text;
                    objMDP.Completion_Date = tab2Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 2;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab2_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                        
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            if (dtMDPDetails.Rows[0].ItemArray[27].ToString() != "True")
                            {
                                if (objMDP.NoOfStages == 2)
                                {
                                    objMDP.IsSentForApproval = "1";
                                    objMDP.IsApproved = "0";
                                    objMDP.IsRejected = "0";
                                }
                            }
                        }
                        else
                        {
                            if (objMDP.NoOfStages == 2)
                            {
                                objMDP.IsSentForApproval = "1";
                                objMDP.IsApproved = "0";
                                objMDP.IsRejected = "0";
                            }

                        }
                    }

                    if (tab2Remarks.FindControl("tab2txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab2Remarks.FindControl("tab2txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                            objMDP.project_ref = Convert.ToString(tab2ProjectNumber.Text);
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }

                    string custNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        custNum = ddlDistributorList.SelectedItem.Value;
                    }
                    string distNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        distNum = ddlCustomerforDistributor.SelectedItem.Value;
                    }

                    tab_number.Value = "2";
                    if (objMDP.NoOfStages == 2)
                    {
                        string customerName = ddlCustomerName.SelectedItem.Text;
                        if (channel_partner.Checked == true)
                        {
                            customerName = ddlCustomerforDistributor.SelectedItem.Text;
                        }
                        if (rdbtsProject.Checked == true)
                        {
                            SendmailToSalesEngineer(objMDP.project_title, objMDP.project_number, customerName, custNum, objMDP.Potential_Lakhs.ToString(), Convert.ToString(ddlSalesEngName.SelectedValue));
                        }

                        SendmailToOwner(objMDP.project_owner, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());
                        SendmailToReviewer(objMDP.Reviewer, objMDP.project_title, objMDP.project_number, customerName, custNum, distNum, objMDP.Potential_Lakhs.ToString());
                        SendmailToEscalateTo(objMDP.EscalatedTo, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());


                    }
                }
                //tab2btnSave.Enabled = false;
                FileUpload2.Enabled = true;
                tab2btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Updated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        /// <summary>
        /// Onclick Method for Stage 3 Save Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab3btnSave_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "3", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //  objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab3txtGoal.Text;
                    objMDP.Stage_Target_Date = tab3Target.Text;
                    objMDP.Completion_Date = tab3Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 3;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                   
                    if (tab3_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                        
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            if (dtMDPDetails.Rows[0].ItemArray[27].ToString() != "True")
                            {
                                if (objMDP.NoOfStages == 3)
                                {
                                    objMDP.IsSentForApproval = "1";
                                    objMDP.IsApproved = "0";
                                    objMDP.IsRejected = "0";
                                }
                            }
                        }
                        else
                        {
                            if (objMDP.NoOfStages == 3)
                            {
                                objMDP.IsSentForApproval = "1";
                                objMDP.IsApproved = "0";
                                objMDP.IsRejected = "0";
                            }

                        }
                    }


                    if (tab3Remarks.FindControl("tab3txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab3Remarks.FindControl("tab3txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }

                    string custNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        custNum = ddlDistributorList.SelectedItem.Value;
                    }
                    string distNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        distNum = ddlCustomerforDistributor.SelectedItem.Value;
                    }

                    tab_number.Value = "3";
                    if (objMDP.NoOfStages == 3)
                    {
                        string customerName = ddlCustomerName.SelectedItem.Text;
                        if (channel_partner.Checked == true)
                        {
                            customerName = ddlCustomerforDistributor.SelectedItem.Text;
                        }
                        if (rdbtsProject.Checked == true)
                        {
                            SendmailToSalesEngineer(objMDP.project_title, objMDP.project_number, customerName, custNum, objMDP.Potential_Lakhs.ToString(), Convert.ToString(ddlSalesEngName.SelectedValue));
                        }

                        SendmailToOwner(objMDP.project_owner, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());
                        SendmailToReviewer(objMDP.Reviewer, objMDP.project_title, objMDP.project_number, customerName, custNum, distNum, objMDP.Potential_Lakhs.ToString());
                        SendmailToEscalateTo(objMDP.EscalatedTo, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());


                    }
                }
                //tab3btnSave.Enabled = false;
                FileUpload3.Enabled = true;
                tab3btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Updated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 4 Save Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab4btnSave_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "4", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab4txtGoal.Text;
                    objMDP.Stage_Target_Date = tab4Target.Text;
                    objMDP.Completion_Date = tab4Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 4;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                   
                    if (tab4_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                        
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            if (dtMDPDetails.Rows[0].ItemArray[27].ToString() != "True")
                            {
                                if (objMDP.NoOfStages == 4)
                                {
                                    objMDP.IsSentForApproval = "1";
                                    objMDP.IsApproved = "0";
                                    objMDP.IsRejected = "0";
                                }
                            }
                        }
                        else
                        {
                            if (objMDP.NoOfStages == 4)
                            {
                                objMDP.IsSentForApproval = "1";
                                objMDP.IsApproved = "0";
                                objMDP.IsRejected = "0";
                            }

                        }
                    }


                    if (tab4Remarks.FindControl("tab4txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab4Remarks.FindControl("tab4txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }

                    string custNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        custNum = ddlDistributorList.SelectedItem.Value;
                    }
                    string distNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        distNum = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    tab_number.Value = "4";
                    if (objMDP.NoOfStages == 4)
                    {
                        string customerName = ddlCustomerName.SelectedItem.Text;
                        if (channel_partner.Checked == true)
                        {
                            customerName = ddlCustomerforDistributor.SelectedItem.Text;
                        }
                        if (rdbtsProject.Checked == true)
                        {
                            SendmailToSalesEngineer(objMDP.project_title, objMDP.project_number, customerName, custNum, objMDP.Potential_Lakhs.ToString(), Convert.ToString(ddlSalesEngName.SelectedValue));
                        }

                        SendmailToOwner(objMDP.project_owner, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());
                        SendmailToReviewer(objMDP.Reviewer, objMDP.project_title, objMDP.project_number, customerName, custNum, distNum, objMDP.Potential_Lakhs.ToString());
                        SendmailToEscalateTo(objMDP.EscalatedTo, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());


                    }

                }
                // tab4btnSave.Enabled = false;
                FileUpload4.Enabled = true;
                tab4btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Updated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 5 Save Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab5btnSave_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "5", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }
                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //  objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab5txtGoal.Text;
                    objMDP.Stage_Target_Date = tab5Target.Text;
                    objMDP.Completion_Date = tab5Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 5;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    

                    if (tab5_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                        
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            if (dtMDPDetails.Rows[0].ItemArray[27].ToString() != "True")
                            {
                                if (objMDP.NoOfStages == 5)
                                {
                                    objMDP.IsSentForApproval = "1";
                                    objMDP.IsApproved = "0";
                                    objMDP.IsRejected = "0";
                                }
                            }
                        }
                        else
                        {
                            if (objMDP.NoOfStages == 5)
                            {
                                objMDP.IsSentForApproval = "1";
                                objMDP.IsApproved = "0";
                                objMDP.IsRejected = "0";
                            }

                        }
                    }


                    if (tab5Remarks.FindControl("tab5txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab5Remarks.FindControl("tab5txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }

                    string custNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        custNum = ddlDistributorList.SelectedItem.Value;
                    }
                    string distNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        distNum = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    tab_number.Value = "5";
                    if (objMDP.NoOfStages == 5)
                    {
                        string customerName = ddlCustomerName.SelectedItem.Text;
                        if (channel_partner.Checked == true)
                        {
                            customerName = ddlCustomerforDistributor.SelectedItem.Text;
                        }
                        if (rdbtsProject.Checked == true)
                        {
                            SendmailToSalesEngineer(objMDP.project_title, objMDP.project_number, customerName, custNum, objMDP.Potential_Lakhs.ToString(), Convert.ToString(ddlSalesEngName.SelectedValue));
                        }

                        SendmailToOwner(objMDP.project_owner, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());
                        SendmailToReviewer(objMDP.Reviewer, objMDP.project_title, objMDP.project_number, customerName, custNum,distNum, objMDP.Potential_Lakhs.ToString());
                        SendmailToEscalateTo(objMDP.EscalatedTo, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());


                    }
                }
                //tab5btnSave.Enabled = false;
                FileUpload5.Enabled = true;
                tab5btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Updated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        /// <summary>
        /// Onclick Method for Stage 6 Save Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab6btnSave_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "6", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {
                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab6txtGoal.Text;
                    objMDP.Stage_Target_Date = tab6Target.Text;
                    objMDP.Completion_Date = tab6Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 6;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                   
                    if (tab6_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                       
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            if (dtMDPDetails.Rows[0].ItemArray[27].ToString() != "True")
                            {
                                if (objMDP.NoOfStages == 6)
                                {
                                    objMDP.IsSentForApproval = "1";
                                    objMDP.IsApproved = "0";
                                    objMDP.IsRejected = "0";
                                }
                            }
                        }
                        else
                        {
                            if (objMDP.NoOfStages == 6)
                            {
                                objMDP.IsSentForApproval = "1";
                                objMDP.IsApproved = "0";
                                objMDP.IsRejected = "0";
                            }

                        }
                    }


                    if (tab6Remarks.FindControl("tab6txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab6Remarks.FindControl("tab6txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }

                    string custNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        custNum = ddlDistributorList.SelectedItem.Value;
                    }
                    string distNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        distNum = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    tab_number.Value = "6";
                    if (objMDP.NoOfStages == 6)
                    {
                        string customerName = ddlCustomerName.SelectedItem.Text;
                        if (channel_partner.Checked == true)
                        {
                            customerName = ddlCustomerforDistributor.SelectedItem.Text;
                        }
                        if (rdbtsProject.Checked == true)
                        {
                            SendmailToSalesEngineer(objMDP.project_title, objMDP.project_number, customerName,custNum, objMDP.Potential_Lakhs.ToString(), Convert.ToString(ddlSalesEngName.SelectedValue));
                        }

                        SendmailToOwner(objMDP.project_owner, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());
                        SendmailToReviewer(objMDP.Reviewer, objMDP.project_title, objMDP.project_number, customerName,custNum,distNum, objMDP.Potential_Lakhs.ToString());
                        SendmailToEscalateTo(objMDP.EscalatedTo, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());


                    }

                }
                // tab6btnSave.Enabled = false;
                FileUpload6.Enabled = true;
                tab6btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Updated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        /// <summary>
        /// Onclick Method for Stage 7 Save Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab7btnSave_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);
                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "7", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    // objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab7txtGoal.Text;
                    objMDP.Stage_Target_Date = tab7Target.Text;
                    objMDP.Completion_Date = tab7Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 7;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                   
                    if (tab7_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                       
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            if (dtMDPDetails.Rows[0].ItemArray[27].ToString() != "True")
                            {
                                if (objMDP.NoOfStages == 7)
                                {
                                    objMDP.IsSentForApproval = "1";
                                    objMDP.IsApproved = "0";
                                    objMDP.IsRejected = "0";
                                }
                            }
                        }
                        else
                        {
                            if (objMDP.NoOfStages == 7)
                            {
                                objMDP.IsSentForApproval = "1";
                                objMDP.IsApproved = "0";
                                objMDP.IsRejected = "0";
                            }

                        }
                    }


                    if (tab7Remarks.FindControl("tab7txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab7Remarks.FindControl("tab7txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }


                    string custNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        custNum = ddlDistributorList.SelectedItem.Value;
                    }
                    string distNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        distNum = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    tab_number.Value = "7";
                    if (objMDP.NoOfStages == 7)
                    {
                        string customerName = ddlCustomerName.SelectedItem.Text;
                        if (channel_partner.Checked == true)
                        {
                            customerName = ddlCustomerforDistributor.SelectedItem.Text;
                        }
                        if (rdbtsProject.Checked == true)
                        {
                            SendmailToSalesEngineer(objMDP.project_title, objMDP.project_number, customerName, custNum, objMDP.Potential_Lakhs.ToString(), Convert.ToString(ddlSalesEngName.SelectedValue));
                        }

                        SendmailToOwner(objMDP.project_owner, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());
                        SendmailToReviewer(objMDP.Reviewer, objMDP.project_title, objMDP.project_number, customerName,custNum, distNum, objMDP.Potential_Lakhs.ToString());
                        SendmailToEscalateTo(objMDP.EscalatedTo, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());


                    }
                }
                //tab7btnSave.Enabled = false;
                FileUpload7.Enabled = true;
                tab7btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Updated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 8 Save Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab8btnSave_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "8", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //     objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab8txtGoal.Text;
                    objMDP.Stage_Target_Date = tab8Target.Text;
                    objMDP.Completion_Date = tab8Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 8;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab8_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                        
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            if (dtMDPDetails.Rows[0].ItemArray[27].ToString() != "True")
                            {
                                if (objMDP.NoOfStages == 8)
                                {
                                    objMDP.IsSentForApproval = "1";
                                    objMDP.IsApproved = "0";
                                    objMDP.IsRejected = "0";
                                }
                            }
                        }
                        else
                        {
                            if (objMDP.NoOfStages == 8)
                            {
                                objMDP.IsSentForApproval = "1";
                                objMDP.IsApproved = "0";
                                objMDP.IsRejected = "0";
                            }

                        }
                    }


                    if (tab1Remarks.FindControl("tab8txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab8Remarks.FindControl("tab8txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }
                    string custNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        custNum = ddlDistributorList.SelectedItem.Value;
                    }
                    string distNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        distNum = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    tab_number.Value = "8";
                    if (objMDP.NoOfStages == 8)
                    {
                        string customerName = ddlCustomerName.SelectedItem.Text;
                        if (channel_partner.Checked == true)
                        {
                            customerName = ddlCustomerforDistributor.SelectedItem.Text;
                        }
                        if (rdbtsProject.Checked == true)
                        {
                            SendmailToSalesEngineer(objMDP.project_title, objMDP.project_number, customerName,custNum, objMDP.Potential_Lakhs.ToString(), Convert.ToString(ddlSalesEngName.SelectedValue));
                        }

                        SendmailToOwner(objMDP.project_owner, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());
                        SendmailToReviewer(objMDP.Reviewer, objMDP.project_title, objMDP.project_number, customerName,custNum,distNum, objMDP.Potential_Lakhs.ToString());
                        SendmailToEscalateTo(objMDP.EscalatedTo, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());


                    }
                }
                // tab8btnSave.Enabled = false;
                FileUpload8.Enabled = true;
                tab8btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Updated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 9 Save Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab9btnSave_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "9", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //       objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab9txtGoal.Text;
                    objMDP.Stage_Target_Date = tab9Target.Text;
                    objMDP.Completion_Date = tab9Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 9;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                   
                    if (tab9_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                        
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            if (dtMDPDetails.Rows[0].ItemArray[27].ToString() != "True")
                            {
                                if (objMDP.NoOfStages == 9)
                                {
                                    objMDP.IsSentForApproval = "1";
                                    objMDP.IsApproved = "0";
                                    objMDP.IsRejected = "0";
                                }
                            }
                        }
                        else
                        {
                            if (objMDP.NoOfStages == 9)
                            {
                                objMDP.IsSentForApproval = "1";
                                objMDP.IsApproved = "0";
                                objMDP.IsRejected = "0";
                            }

                        }
                    }


                    if (tab9Remarks.FindControl("tab9txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab9Remarks.FindControl("tab9txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }

                    string custNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        custNum = ddlDistributorList.SelectedItem.Value;
                    }
                    string distNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        distNum = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    tab_number.Value = "9";
                    if (objMDP.NoOfStages == 9)
                    {
                        string customerName = ddlCustomerName.SelectedItem.Text;
                        if (channel_partner.Checked == true)
                        {
                            customerName = ddlCustomerforDistributor.SelectedItem.Text;
                        }
                        if (rdbtsProject.Checked == true)
                        {
                            SendmailToSalesEngineer(objMDP.project_title, objMDP.project_number, customerName, custNum, objMDP.Potential_Lakhs.ToString(), Convert.ToString(ddlSalesEngName.SelectedValue));
                        }

                        SendmailToOwner(objMDP.project_owner, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());
                        SendmailToReviewer(objMDP.Reviewer, objMDP.project_title, objMDP.project_number, customerName, custNum, distNum, objMDP.Potential_Lakhs.ToString());
                        SendmailToEscalateTo(objMDP.EscalatedTo, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());


                    }

                }
                // tab9btnSave.Enabled = false;
                FileUpload9.Enabled = true;
                tab9btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Updated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 10 Save Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab10btnSave_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "10", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //  objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab10txtGoal.Text;
                    objMDP.Stage_Target_Date = tab10Target.Text;
                    objMDP.Completion_Date = tab10Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 10;
                    objMDP.IsSubmitted = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                   

                    if (tab10_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                       
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(objMDP.customer_num, objMDP.project_number);
                    if (dtMDPDetails != null)
                    {
                        if (dtMDPDetails.Rows.Count > 0)
                        {
                            if (dtMDPDetails.Rows[0].ItemArray[27].ToString() != "True")
                            {
                                if (objMDP.NoOfStages == 10)
                                {
                                    objMDP.IsSentForApproval = "1";
                                    objMDP.IsApproved = "0";
                                    objMDP.IsRejected = "0";
                                }
                            }
                        }
                        else
                        {
                            if (objMDP.NoOfStages == 10)
                            {
                                objMDP.IsSentForApproval = "1";
                                objMDP.IsApproved = "0";
                                objMDP.IsRejected = "0";
                            }

                        }
                    }
                    if (tab1Remarks.FindControl("tab10txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab10Remarks.FindControl("tab10txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                        if (rdbtsProject.Checked)
                        {
                            objMDP.ts_project_flag = "Y";
                            objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                            objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                        }
                        string Messege = objMDP.insert_mdpinfo(objMDP);
                    }


                    tab_number.Value = "10";
                    string custNum = ddlCustomerName.SelectedItem.Value;
                    if(channel_partner.Checked == true)
                    {
                        custNum = ddlDistributorList.SelectedItem.Value;
                    }
                    string distNum = ddlCustomerName.SelectedItem.Value;
                    if (channel_partner.Checked == true)
                    {
                        distNum = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    if (objMDP.NoOfStages == 10)
                    {
                        string customerName = ddlCustomerName.SelectedItem.Text;
                        if (channel_partner.Checked == true)
                        {
                            customerName = ddlCustomerforDistributor.SelectedItem.Text;
                        }
                        if (rdbtsProject.Checked == true)
                        {
                            SendmailToSalesEngineer(objMDP.project_title, objMDP.project_number, customerName, custNum, objMDP.Potential_Lakhs.ToString(), Convert.ToString(ddlSalesEngName.SelectedValue));
                        }

                        SendmailToOwner(objMDP.project_owner, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());
                        SendmailToReviewer(objMDP.Reviewer, objMDP.project_title, objMDP.project_number, customerName, custNum, distNum, objMDP.Potential_Lakhs.ToString());
                        SendmailToEscalateTo(objMDP.EscalatedTo, objMDP.project_title, objMDP.project_number, customerName, objMDP.Potential_Lakhs.ToString());


                    }

                }
                //tab10btnSave.Enabled = false;
                FileUpload10.Enabled = true;
                tab10btnUpload.Enabled = true;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Sucessfully Updated');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        #endregion

        #region Submit All Stages
        /// <summary>
        /// Onclick Method for Stage 1 Submit Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab1btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);

                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "1", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab1txtGoal.Text;
                    objMDP.Stage_Target_Date = tab1Target.Text;
                    objMDP.Completion_Date = tab1Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 1;
                    objMDP.IsSubmitted = "1";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.IsSentForApproval = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab1_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                      
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    if (tab1Remarks.FindControl("tab1txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab1Remarks.FindControl("tab1txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                    }

                    if (objMDP.NoOfStages == 1)
                    {
                        if (tab1_radio_success.Checked == true)
                        {
                            objMDP.project_status = "success";
                            objMDP.monthly_expected = Convert.ToDecimal(tab1_monthly_expected.Text);
                            objMDP.customer_order = Convert.ToDecimal(tab1_customer_order.Text);
                        }
                        else
                        {
                            objMDP.project_status = "failure";
                            objMDP.monthly_expected = 0;
                        }

                        objMDP.comments = tab1_comments.Text;

                    }
                    if (rdbtsProject.Checked)
                    {
                        objMDP.ts_project_flag = "Y";
                        objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                        objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                    }
                    string Messege = objMDP.insert_mdpinfo(objMDP);
                    tab_number.Value = "1";
                    if (tab2_chkbox.Checked == false)
                    {
                        tab2Completion.Enabled = true;
                        tab2Completion_flag.Value = "True";
                    }
                    ExistingProduct.Enabled = false;
                    txtTaegutecCutter.Enabled = false;
                    txtTaegutecInsert.Enabled = false;
                    txtCompCutter.Enabled = false;
                    txtCompInsert.Enabled = false;
                    ddlProjectType.Enabled = false;
                    ddlProductGroup.Enabled = false;
                    Component.Enabled = false;
                    //   CompetitionSpec.Enabled = false;
                    Stages.Enabled = false;
                    TargetDate.Enabled = false;
                    Target_flag.Value = "True";
                    OverAllPotential.Enabled = false;
                    Potential.Enabled = false;
                    Business.Enabled = false;
                    ddlSalesEngName.Enabled = false;
                    tab1txtGoal.Enabled = false;
                    tab1Target.Enabled = false;
                    tab1Target_flag.Value = "True";
                    tab1Completion.Enabled = false;
                    ddlReviewer.Enabled = false;
                    ddlEscalate.Enabled = false;
                    ddlIndustry.Enabled = false;
                    ddlDistibutor.Enabled = false;
                    FileUpload1.Enabled = false;
                    tab1btnUpload.Enabled = false;
                    txtOrderTargetCY.Enabled = false;
                    txtOrderTargetNY.Enabled = false;
                }
                ddlTitle_SelectedIndexChanged(null, null);
                tab1btnSubmit.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "close1", "alert('Sucessfully Submitted');", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        /// <summary>
        /// Onclick Method for Stage 2 Submit Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab2btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "2", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab2txtGoal.Text;
                    objMDP.Stage_Target_Date = tab2Target.Text;
                    objMDP.Completion_Date = tab2Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 2;
                    objMDP.IsSubmitted = "1";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.IsSentForApproval = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    

                    if (tab2_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                       
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    if (tab2Remarks.FindControl("tab2txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab2Remarks.FindControl("tab2txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                    }
                    if (objMDP.NoOfStages == 2)
                    {
                        if (tab2_radio_success.Checked == true)
                        {
                            objMDP.project_status = "success";
                            objMDP.monthly_expected = Convert.ToDecimal(tab2_monthly_expected.Text);
                            objMDP.customer_order = Convert.ToDecimal(tab2_customer_order.Text);
                        }
                        else
                        {
                            objMDP.project_status = "failure";
                            objMDP.monthly_expected = 0;
                        }

                        objMDP.comments = tab2_comments.Text;
                    }
                    if (rdbtsProject.Checked)
                    {
                        objMDP.ts_project_flag = "Y";
                        objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                        objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                        objMDP.project_ref = Convert.ToString(tab2ProjectNumber.Text);
                    }
                    string Messege = objMDP.insert_mdpinfo(objMDP);
                    tab_number.Value = "2";
                    if (tab3_chkbox.Checked == false)
                    {
                        tab3Completion.Enabled = true;
                        tab3Completion_flag.Value = "True";
                    }
                    ExistingProduct.Enabled = false;
                    txtTaegutecCutter.Enabled = false;
                    txtTaegutecInsert.Enabled = false;
                    txtCompCutter.Enabled = false;
                    txtCompInsert.Enabled = false;
                    ddlProjectType.Enabled = false;
                    ddlProductGroup.Enabled = false;
                    Component.Enabled = false;
                    //     CompetitionSpec.Enabled = false;
                    Stages.Enabled = false;
                    TargetDate.Enabled = false;
                    Target_flag.Value = "True";
                    OverAllPotential.Enabled = false;
                    Potential.Enabled = false;
                    Business.Enabled = false;
                    ddlSalesEngName.Enabled = false;
                    tab2txtGoal.Enabled = false;
                    tab2Target.Enabled = false;
                    tab2ProjectNumber.Enabled = false;
                    tab2Target_flag.Value = "True";
                    tab2Completion.Enabled = false;
                    ddlReviewer.Enabled = false;
                    ddlEscalate.Enabled = false;
                    ddlIndustry.Enabled = false;
                    ddlDistibutor.Enabled = false;
                    txtOrderTargetCY.Enabled = false;
                    txtOrderTargetNY.Enabled = false;
                }
                ddlTitle_SelectedIndexChanged(null, null);
                tab2btnSubmit.Visible = false;
                FileUpload2.Enabled = false;
                tab2btnUpload.Enabled = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "close2", "alert('Sucessfully Submitted');", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 3 Submit Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab3btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "3", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //     objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab3txtGoal.Text;
                    objMDP.Stage_Target_Date = tab3Target.Text;
                    objMDP.Completion_Date = tab3Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 3;
                    objMDP.IsSubmitted = "1";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.IsSentForApproval = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    

                    if (tab3_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                        
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    if (tab3Remarks.FindControl("tab3txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab3Remarks.FindControl("tab3txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                    }
                    if (objMDP.NoOfStages == 3)
                    {
                        if (tab3_radio_success.Checked == true)
                        {
                            objMDP.project_status = "success";
                            objMDP.monthly_expected = Convert.ToDecimal(tab3_monthly_expected.Text);
                            objMDP.customer_order = Convert.ToDecimal(tab3_customer_order.Text);
                        }
                        else
                        {
                            objMDP.project_status = "failure";
                            objMDP.monthly_expected = 0;
                        }

                        objMDP.comments = tab3_comments.Text;

                    }
                    if (rdbtsProject.Checked)
                    {
                        objMDP.ts_project_flag = "Y";
                        objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                        objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                    }
                    string Messege = objMDP.insert_mdpinfo(objMDP);
                    tab_number.Value = "3";
                    if (tab4_chkbox.Checked == false)
                    {
                        tab4Completion.Enabled = true;
                        tab4Completion_flag.Value = "True";
                    }
                    ExistingProduct.Enabled = false;
                    txtTaegutecCutter.Enabled = false;
                    txtTaegutecInsert.Enabled = false;
                    txtCompCutter.Enabled = false;
                    txtCompInsert.Enabled = false;
                    ddlProjectType.Enabled = false;
                    ddlProductGroup.Enabled = false;
                    Component.Enabled = false;
                    //   CompetitionSpec.Enabled = false;
                    Stages.Enabled = false;
                    TargetDate.Enabled = false;
                    Target_flag.Value = "True";
                    OverAllPotential.Enabled = false;
                    Potential.Enabled = false;
                    Business.Enabled = false;
                    ddlSalesEngName.Enabled = false;
                    tab3txtGoal.Enabled = false;
                    tab3Target.Enabled = false;
                    tab3Target_flag.Value = "True";
                    tab3Completion.Enabled = false;
                    ddlReviewer.Enabled = false;
                    ddlEscalate.Enabled = false;
                    ddlIndustry.Enabled = false;
                    ddlDistibutor.Enabled = false;
                    txtOrderTargetCY.Enabled = false;
                    txtOrderTargetNY.Enabled = false;
                }
                tab3btnSubmit.Visible = false;
                FileUpload3.Enabled = false;
                tab3btnUpload.Enabled = false;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "close3", "alert('Sucessfully Submitted');", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 4 Submit Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab4btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "4", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab4txtGoal.Text;
                    objMDP.Stage_Target_Date = tab4Target.Text;
                    objMDP.Completion_Date = tab4Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 4;
                    objMDP.IsSubmitted = "1";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.IsSentForApproval = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                   

                    if (tab4_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                        
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    if (tab4Remarks.FindControl("tab4txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab4Remarks.FindControl("tab4txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                    }
                    if (objMDP.NoOfStages == 4)
                    {
                        if (tab4_radio_success.Checked == true)
                        {
                            objMDP.project_status = "success";
                            objMDP.monthly_expected = Convert.ToDecimal(tab4_monthly_expected.Text);
                            objMDP.customer_order = Convert.ToDecimal(tab4_customer_order.Text);
                        }
                        else
                        {
                            objMDP.project_status = "failure";
                            objMDP.monthly_expected = 0;
                        }

                        objMDP.comments = tab4_comments.Text;

                    }
                    if (rdbtsProject.Checked)
                    {
                        objMDP.ts_project_flag = "Y";
                        objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                        objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                    }
                    string Messege = objMDP.insert_mdpinfo(objMDP);
                    tab_number.Value = "4";
                    if (tab5_chkbox.Checked == false)
                    { 
                    tab5Completion.Enabled = true;
                    tab5Completion_flag.Value = "True";
                }
                    ExistingProduct.Enabled = false;
                    txtTaegutecCutter.Enabled = false;
                    txtTaegutecInsert.Enabled = false;
                    txtCompCutter.Enabled = false;
                    txtCompInsert.Enabled = false;
                    ddlProjectType.Enabled = false;
                    ddlProductGroup.Enabled = false;
                    Component.Enabled = false;
                    //   CompetitionSpec.Enabled = false;
                    Stages.Enabled = false;
                    TargetDate.Enabled = false;
                    Target_flag.Value = "True";
                    OverAllPotential.Enabled = false;
                    Potential.Enabled = false;
                    Business.Enabled = false;
                    ddlSalesEngName.Enabled = false;
                    tab4txtGoal.Enabled = false;
                    tab4Target.Enabled = false;
                    tab4Target_flag.Value = "True";
                    tab4Completion.Enabled = false;
                    ddlReviewer.Enabled = false;
                    ddlEscalate.Enabled = false;
                    ddlIndustry.Enabled = false;
                    ddlDistibutor.Enabled = false;
                    txtOrderTargetCY.Enabled = false;
                    txtOrderTargetNY.Enabled = false;
                }
                tab4btnSubmit.Visible = false;
                FileUpload4.Enabled = false;
                tab4btnUpload.Enabled = false;
                ddlTitle_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "close4", "alert('Sucessfully Submitted');", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 5 Submit Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab5btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "5", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //   objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab5txtGoal.Text;
                    objMDP.Stage_Target_Date = tab5Target.Text;
                    objMDP.Completion_Date = tab5Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 5;
                    objMDP.IsSubmitted = "1";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.IsSentForApproval = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab5_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                     
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    if (tab5Remarks.FindControl("tab5txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab5Remarks.FindControl("tab5txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                    }
                    if (objMDP.NoOfStages == 5)
                    {
                        if (tab5_radio_success.Checked == true)
                        {
                            objMDP.project_status = "success";
                            objMDP.monthly_expected = Convert.ToDecimal(tab5_monthly_expected.Text);
                            objMDP.customer_order = Convert.ToDecimal(tab5_customer_order.Text);
                        }
                        else
                        {
                            objMDP.project_status = "failure";
                            objMDP.monthly_expected = 0;
                        }

                        objMDP.comments = tab5_comments.Text;

                    }
                    if (rdbtsProject.Checked)
                    {
                        objMDP.ts_project_flag = "Y";
                        objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                        objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                        objMDP.OrderNumber = Convert.ToString(tab5_Ord_Num.Text);
                        objMDP.OrderValue = Convert.ToDecimal(tab5_Ord_Val.Text);
                    }
                    string Messege = objMDP.insert_mdpinfo(objMDP);
                    tab_number.Value = "5";
                    if (tab6_chkbox.Checked == false)
                    {
                        tab6Completion.Enabled = true;
                        tab6Completion_flag.Value = "True";
                    }
                    ExistingProduct.Enabled = false;
                    txtTaegutecCutter.Enabled = false;
                    txtTaegutecInsert.Enabled = false;
                    txtCompCutter.Enabled = false;
                    txtCompInsert.Enabled = false;
                    ddlProjectType.Enabled = false;
                    ddlProductGroup.Enabled = false;
                    Component.Enabled = false;
                    //    CompetitionSpec.Enabled = false;
                    Stages.Enabled = false;
                    TargetDate.Enabled = false;
                    Target_flag.Value = "True";
                    OverAllPotential.Enabled = false;
                    Potential.Enabled = false;
                    Business.Enabled = false;
                    ddlSalesEngName.Enabled = false;
                    tab5txtGoal.Enabled = false;
                    tab5Target.Enabled = false;
                    tab5Target_flag.Value = "True";
                    tab5Completion.Enabled = false;
                    ddlReviewer.Enabled = false;
                    ddlEscalate.Enabled = false;
                    ddlIndustry.Enabled = false;
                    ddlDistibutor.Enabled = false;
                    txtOrderTargetCY.Enabled = false;
                    txtOrderTargetNY.Enabled = false;
                }
                ddlTitle_SelectedIndexChanged(null, null);
                tab5btnSubmit.Visible = false;
                FileUpload5.Enabled = false;
                tab5btnUpload.Enabled = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "close5", "alert('Sucessfully Submitted');", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 6 Submit Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab6btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "6", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //    objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab6txtGoal.Text;
                    objMDP.Stage_Target_Date = tab6Target.Text;
                    objMDP.Completion_Date = tab6Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 6;
                    objMDP.IsSubmitted = "1";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.IsSentForApproval = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab6_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                       
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    if (tab6Remarks.FindControl("tab6txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab6Remarks.FindControl("tab6txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                    }
                    if (objMDP.NoOfStages == 6)
                    {
                        if (tab6_radio_success.Checked == true)
                        {
                            objMDP.project_status = "success";
                            objMDP.monthly_expected = Convert.ToDecimal(tab6_monthly_expected.Text);
                            objMDP.customer_order = Convert.ToDecimal(tab6_customer_order.Text);
                        }
                        else
                        {
                            objMDP.project_status = "failure";
                            objMDP.monthly_expected = 0;
                        }

                        objMDP.comments = tab6_comments.Text;

                    }
                    if (rdbtsProject.Checked)
                    {
                        objMDP.ts_project_flag = "Y";
                        objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                        objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                    }
                    string Messege = objMDP.insert_mdpinfo(objMDP);
                    tab_number.Value = "6";
                    if (tab7_chkbox.Checked == false)
                    {
                        tab7Completion.Enabled = true;
                        tab7Completion_flag.Value = "True";
                    }
                    ExistingProduct.Enabled = false;
                    txtTaegutecCutter.Enabled = false;
                    txtTaegutecInsert.Enabled = false;
                    txtCompCutter.Enabled = false;
                    txtCompInsert.Enabled = false;
                    ddlProjectType.Enabled = false;
                    ddlProductGroup.Enabled = false;
                    Component.Enabled = false;
                    //   CompetitionSpec.Enabled = false;
                    Stages.Enabled = false;
                    TargetDate.Enabled = false;
                    Target_flag.Value = "True";
                    OverAllPotential.Enabled = false;
                    Potential.Enabled = false;
                    Business.Enabled = false;
                    ddlSalesEngName.Enabled = false;
                    tab6txtGoal.Enabled = false;
                    tab6Target.Enabled = false;
                    tab6Target_flag.Value = "True";
                    tab6Completion.Enabled = false;
                    ddlReviewer.Enabled = false;
                    ddlEscalate.Enabled = false;
                    ddlIndustry.Enabled = false;
                    ddlDistibutor.Enabled = false;
                    txtOrderTargetCY.Enabled = false;
                    txtOrderTargetNY.Enabled = false;
                }
                ddlTitle_SelectedIndexChanged(null, null);
                tab6btnSubmit.Visible = false;
                FileUpload6.Enabled = false;
                tab6btnUpload.Enabled = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "close6", "alert('Sucessfully Submitted');", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 7 Submit Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab7btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "7", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //    objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab7txtGoal.Text;
                    objMDP.Stage_Target_Date = tab7Target.Text;
                    objMDP.Completion_Date = tab7Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 7;
                    objMDP.IsSubmitted = "1";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.IsSentForApproval = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab7_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                      
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    if (tab7Remarks.FindControl("tab7txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab7Remarks.FindControl("tab7txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                    }
                    if (objMDP.NoOfStages == 7)
                    {
                        if (tab7_radio_success.Checked == true)
                        {
                            objMDP.project_status = "success";
                            objMDP.monthly_expected = Convert.ToDecimal(tab7_monthly_expected.Text);
                            objMDP.customer_order = Convert.ToDecimal(tab7_customer_order.Text);
                        }
                        else
                        {
                            objMDP.project_status = "failure";
                            objMDP.monthly_expected = 0;
                        }

                        objMDP.comments = tab7_comments.Text;

                    }
                    if (rdbtsProject.Checked)
                    {
                        objMDP.ts_project_flag = "Y";
                        objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                        objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                    }
                    string Messege = objMDP.insert_mdpinfo(objMDP);
                    tab_number.Value = "7";
                    if (tab8_chkbox.Checked == false)
                    {
                        tab8Completion.Enabled = true;
                        tab8Completion_flag.Value = "True";
                    }
                    ExistingProduct.Enabled = false;
                    txtTaegutecCutter.Enabled = false;
                    txtTaegutecInsert.Enabled = false;
                    txtCompCutter.Enabled = false;
                    txtCompInsert.Enabled = false;
                    ddlProjectType.Enabled = false;
                    ddlProductGroup.Enabled = false;
                    Component.Enabled = false;
                    //    CompetitionSpec.Enabled = false;
                    Stages.Enabled = false;
                    TargetDate.Enabled = false;
                    Target_flag.Value = "True";
                    OverAllPotential.Enabled = false;
                    Potential.Enabled = false;
                    Business.Enabled = false;
                    ddlSalesEngName.Enabled = false;
                    tab7txtGoal.Enabled = false;
                    tab7Target.Enabled = false;
                    tab7Target_flag.Value = "True";
                    tab7Completion.Enabled = false;
                    ddlReviewer.Enabled = false;
                    ddlEscalate.Enabled = false;
                    ddlIndustry.Enabled = false;
                    ddlDistibutor.Enabled = false;
                    txtOrderTargetCY.Enabled = false;
                    txtOrderTargetNY.Enabled = false;
                }
                ddlTitle_SelectedIndexChanged(null, null);
                tab7btnSubmit.Visible = false;
                FileUpload7.Enabled = false;
                tab7btnUpload.Enabled = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "close7", "alert('Sucessfully Submitted');", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 8 Submit Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab8btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "8", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //     objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab8txtGoal.Text;
                    objMDP.Stage_Target_Date = tab8Target.Text;
                    objMDP.Completion_Date = tab8Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 8;
                    objMDP.IsSubmitted = "1";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.IsSentForApproval = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                   
                    if (tab8_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                        
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    if (tab8Remarks.FindControl("tab8txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab8Remarks.FindControl("tab8txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                    }
                    if (objMDP.NoOfStages == 8)
                    {
                        if (tab8_radio_success.Checked == true)
                        {
                            objMDP.project_status = "success";
                            objMDP.monthly_expected = Convert.ToDecimal(tab8_monthly_expected.Text);
                            objMDP.customer_order = Convert.ToDecimal(tab8_customer_order.Text);
                        }
                        else
                        {
                            objMDP.project_status = "failure";
                            objMDP.monthly_expected = 0;
                        }

                        objMDP.comments = tab8_comments.Text;

                    }
                    if (rdbtsProject.Checked)
                    {
                        objMDP.ts_project_flag = "Y";
                        objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                        objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                        objMDP.OrderNumber = Convert.ToString(tab8_Ord_Num.Text);
                        objMDP.OrderValue = Convert.ToDecimal(tab8_Ord_Val.Text);
                    }
                    string Messege = objMDP.insert_mdpinfo(objMDP);
                    tab_number.Value = "8";
                    if (tab9_chkbox.Checked == false)
                    {
                        tab9Completion.Enabled = true;
                        tab9Completion_flag.Value = "True";
                    }
                    ExistingProduct.Enabled = false;
                    txtTaegutecCutter.Enabled = false;
                    txtTaegutecInsert.Enabled = false;
                    txtCompCutter.Enabled = false;
                    txtCompInsert.Enabled = false;
                    ddlProjectType.Enabled = false;
                    ddlProductGroup.Enabled = false;
                    Component.Enabled = false;
                    //      CompetitionSpec.Enabled = false;
                    Stages.Enabled = false;
                    TargetDate.Enabled = false;
                    Target_flag.Value = "True";
                    OverAllPotential.Enabled = false;
                    Potential.Enabled = false;
                    Business.Enabled = false;
                    ddlSalesEngName.Enabled = false;
                    tab8txtGoal.Enabled = false;
                    tab8Target.Enabled = false;
                    tab8Target_flag.Value = "True";
                    tab8Completion.Enabled = false;
                    ddlReviewer.Enabled = false;
                    ddlEscalate.Enabled = false;
                    ddlIndustry.Enabled = false;
                    ddlDistibutor.Enabled = false;
                    txtOrderTargetCY.Enabled = false;
                    txtOrderTargetNY.Enabled = false;
                }
                ddlTitle_SelectedIndexChanged(null, null);
                tab8btnSubmit.Visible = false;
                FileUpload8.Enabled = false;
                tab8btnUpload.Enabled = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "close8", "alert('Sucessfully Submitted');", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 9 Submit Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab9btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "9", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    // objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab9txtGoal.Text;
                    objMDP.Stage_Target_Date = tab9Target.Text;
                    objMDP.Completion_Date = tab9Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 9;
                    objMDP.IsSubmitted = "1";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.IsSentForApproval = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                   
                    if (tab9_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                      
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    if (tab9Remarks.FindControl("tab9txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab9Remarks.FindControl("tab9txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                    }
                    if (objMDP.NoOfStages == 9)
                    {
                        if (tab9_radio_success.Checked == true)
                        {
                            objMDP.project_status = "success";
                            objMDP.monthly_expected = Convert.ToDecimal(tab9_monthly_expected.Text);
                            objMDP.customer_order = Convert.ToDecimal(tab9_customer_order.Text);
                        }
                        else
                        {
                            objMDP.project_status = "failure";
                            objMDP.monthly_expected = 0;
                        }

                        objMDP.comments = tab9_comments.Text;

                    }
                    if (rdbtsProject.Checked)
                    {
                        objMDP.ts_project_flag = "Y";
                        objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                        objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                    }
                    string Messege = objMDP.insert_mdpinfo(objMDP);
                    tab_number.Value = "9";
                    if (tab10_chkbox.Checked == false)
                    {
                        tab10Completion.Enabled = true;
                        tab10Completion_flag.Value = "True";
                    }
                    ExistingProduct.Enabled = false;
                    txtTaegutecCutter.Enabled = false;
                    txtTaegutecInsert.Enabled = false;
                    txtCompCutter.Enabled = false;
                    txtCompInsert.Enabled = false;
                    ddlProjectType.Enabled = false;
                    ddlProductGroup.Enabled = false;
                    Component.Enabled = false;
                    //    CompetitionSpec.Enabled = false;
                    Stages.Enabled = false;
                    TargetDate.Enabled = false;
                    Target_flag.Value = "True";
                    OverAllPotential.Enabled = false;
                    Potential.Enabled = false;
                    Business.Enabled = false;
                    ddlSalesEngName.Enabled = false;
                    tab9txtGoal.Enabled = false;
                    tab9Target.Enabled = false;
                    tab9Target_flag.Value = "True";
                    tab9Completion.Enabled = false;
                    ddlReviewer.Enabled = false;
                    ddlEscalate.Enabled = false;
                    ddlIndustry.Enabled = false;
                    ddlDistibutor.Enabled = false;
                    txtOrderTargetCY.Enabled = false;
                    txtOrderTargetNY.Enabled = false;
                }
                ddlTitle_SelectedIndexChanged(null, null);
                tab9btnSubmit.Visible = false;
                FileUpload9.Enabled = false;
                tab9btnUpload.Enabled = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "close9", "alert('Sucessfully Submitted');", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Onclick Method for Stage 10 Submit Button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tab10btnSubmit_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                    //objMDP.customer_name = ddlCustomerName.SelectedItem.Text;
                    objMDP.customer_name = objMDP.getcustomer_name(ddlCustomerNo.SelectedItem.Value);
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                    objMDP.customer_name = ddlCustomerforDistributor.SelectedItem.Text;
                }
                //objMDP.Industry_Id = ddlIndustry.SelectedItem.Value;
                objMDP.project_number = ddlTitle.SelectedItem.Value;

                DataTable dtMDP_Remarks = objMDP.LoadMDP_Remarks(objMDP.customer_num, "10", objMDP.project_number);


                int TextBox_Count = 0;
                if (dtMDP_Remarks != null)
                {
                    TextBox_Count = dtMDP_Remarks.Rows.Count + 1;
                }
                else
                {
                    TextBox_Count = 2;
                }

                for (int i = 1; i <= TextBox_Count; i++)
                {

                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                    objMDP.Industry = ddlIndustry.SelectedItem.Value; ;
                    objMDP.Existing_Product = ExistingProduct.Text;
                    objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                    objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                    objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                    objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                    objMDP.Component = Component.Text;
                    //       objMDP.Competition = CompetitionSpec.Text;
                    objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                    objMDP.Target_Date = TargetDate.Text;
                    objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                    objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                    objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                    objMDP.Goal = tab10txtGoal.Text;
                    objMDP.Stage_Target_Date = tab10Target.Text;
                    objMDP.Completion_Date = tab10Completion.Text;
                    objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                    objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                    objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                    objMDP.project_type = ddlProjectType.SelectedItem.Value;
                    objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                    objMDP.Stage_Number = 10;
                    objMDP.IsSubmitted = "1";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.IsSentForApproval = "0";
                    objMDP.BranchCode = branchCode;
                    objMDP.customer_class = getCustomerClassCode(txtCustomerClass.Text);
                    objMDP.HOIndustry = Convert.ToString(ddlHOIndustry.SelectedValue);
                    objMDP.MachineMake = Convert.ToString(ddlMachineMake.SelectedValue);
                    objMDP.MachineType = Convert.ToString(ddlMachineType.SelectedValue);
                    objMDP.MachineNo = Convert.ToString(txtMachNo.Text);
                    
                    if (tab10_chkbox.Checked == true)
                    {
                        objMDP.IsOptional = "1";
                        objMDP.IsSubmitted = "1";
                        objMDP.Completion_Date = "01/01/1753";
                        
                    }
                    else
                    {
                        objMDP.IsOptional = "0";
                    }
                    if (tab10Remarks.FindControl("tab10txtRemarks_" + i) != null)
                    {
                        TextBox textbox = (TextBox)tab10Remarks.FindControl("tab10txtRemarks_" + i);
                        objMDP.Remarks = textbox.Text;
                        objMDP.Remark_Number = i;
                        textbox.Enabled = false;
                    }
                    if (objMDP.NoOfStages == 10)
                    {
                        if (tab10_radio_success.Checked == true)
                        {
                            objMDP.project_status = "success";
                            objMDP.monthly_expected = Convert.ToDecimal(tab10_monthly_expected.Text);
                            objMDP.customer_order = Convert.ToDecimal(tab10_customer_order.Text);
                        }
                        else
                        {
                            objMDP.project_status = "failure";
                            objMDP.monthly_expected = 0;
                        }

                        objMDP.comments = tab10_comments.Text;

                    }
                    if (rdbtsProject.Checked)
                    {
                        objMDP.ts_project_flag = "Y";
                        objMDP.OrderTarget_CY = Convert.ToDecimal(txtOrderTargetCY.Text);
                        objMDP.OrderTarget_NY = Convert.ToDecimal(txtOrderTargetNY.Text);
                    }
                    string Messege = objMDP.insert_mdpinfo(objMDP);
                    tab_number.Value = "10";
                    ExistingProduct.Enabled = false;
                    txtTaegutecCutter.Enabled = false;
                    txtTaegutecInsert.Enabled = false;
                    txtCompCutter.Enabled = false;
                    txtCompInsert.Enabled = false;
                    ddlProjectType.Enabled = false;
                    ddlProductGroup.Enabled = false;
                    Component.Enabled = false;
                    //  CompetitionSpec.Enabled = false;
                    Stages.Enabled = false;
                    TargetDate.Enabled = false;
                    Target_flag.Value = "True";
                    OverAllPotential.Enabled = false;
                    Potential.Enabled = false;
                    Business.Enabled = false;
                    ddlSalesEngName.Enabled = false;
                    tab10txtGoal.Enabled = false;
                    tab10Target.Enabled = false;
                    tab10Target_flag.Value = "True";
                    tab10Completion.Enabled = false;
                    ddlReviewer.Enabled = false;
                    ddlEscalate.Enabled = false;
                    ddlIndustry.Enabled = false;
                    ddlDistibutor.Enabled = false;
                    txtOrderTargetCY.Enabled = false;
                    txtOrderTargetNY.Enabled = false;
                }
                ddlTitle_SelectedIndexChanged(null, null);
                tab10btnSubmit.Visible = false;
                FileUpload10.Enabled = false;
                tab10btnUpload.Enabled = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "close10", "alert('Sucessfully Submitted');", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Enable_NewCustomer(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddlCustomerNo.Enabled = true;
                ddlCustomerName.Enabled = true;
                ddlCustomerName.SelectedValue = "-- SELECT CUSTOMER --";
                ddlCustomerNo.SelectedValue = "-- SELECT CUSTOMER NUMBER --";
                ddlDistibutor.SelectedValue = "--SELECT--";
                ddlDistibutor.Enabled = true;
                ddlDistributorList.Enabled = true;

                LoadDefault();
                LoadDefaultTab();

                new_project.Checked = true;
                existing_project.Checked = false;

                tab1btnSubmit.Visible = false;
                tab2btnSubmit.Visible = false;
                tab3btnSubmit.Visible = false;
                tab4btnSubmit.Visible = false;
                tab5btnSubmit.Visible = false;
                tab6btnSubmit.Visible = false;
                tab7btnSubmit.Visible = false;
                tab8btnSubmit.Visible = false;
                tab9btnSubmit.Visible = false;
                tab10btnSubmit.Visible = false;
                // tab1btnSave.Enabled = true;
                tab1btnSave.Visible = true;
                //tab2btnSave.Enabled = true;
                tab2btnSave.Visible = true;
                // tab3btnSave.Enabled = true;
                tab3btnSave.Visible = true;
                // tab4btnSave.Enabled = true;
                tab4btnSave.Visible = true;
                // tab5btnSave.Enabled = true;
                tab5btnSave.Visible = true;
                //tab6btnSave.Enabled = true;
                tab6btnSave.Visible = true;
                // tab7btnSave.Enabled = true;
                tab7btnSave.Visible = true;
                //tab8btnSave.Enabled = true;
                tab8btnSave.Visible = true;
                // tab9btnSave.Enabled = true;
                tab9btnSave.Visible = true;
                // tab10btnSave.Enabled = true;
                tab10btnSave.Visible = true;
                tab1btnSaveApprove.Visible = false;
                tab2btnSaveApprove.Visible = false;
                tab3btnSaveApprove.Visible = false;
                tab4btnSaveApprove.Visible = false;
                tab5btnSaveApprove.Visible = false;
                tab6btnSaveApprove.Visible = false;
                tab7btnSaveApprove.Visible = false;
                tab8btnSaveApprove.Visible = false;
                tab9btnSaveApprove.Visible = false;
                tab10btnSaveApprove.Visible = false;

                ddlCustomerforDistributor.Enabled = true;

                ddlIndustry.Enabled = false;
                DataTable dtDeatils = new DataTable();
                ddlCustomerforDistributor.DataSource = dtDeatils;
                ddlCustomerforDistributor.DataTextField = "customer_name";
                ddlCustomerforDistributor.DataValueField = "customer_number";
                ddlCustomerforDistributor.DataBind();
                ddlCustomerforDistributor.Items.Insert(0, "-- SELECT CUSTOMER --");
                direct_customer.Checked = true;
                direct_customer.Enabled = true;
                channel_partner.Enabled = true;
                ddlDistributorList.SelectedValue = "-- SELECT CHANNEL PARTNER --";

                tab1_lblApprove.Visible = false;
                tab1_txtApprove.Visible = false;
                tab1_btnApprove.Visible = false;
                tab1_btnReject.Visible = false;

                tab2_lblApprove.Visible = false;
                tab2_txtApprove.Visible = false;
                tab2_btnApprove.Visible = false;
                tab2_btnReject.Visible = false;

                tab3_lblApprove.Visible = false;
                tab3_txtApprove.Visible = false;
                tab3_btnApprove.Visible = false;
                tab3_btnReject.Visible = false;

                tab4_lblApprove.Visible = false;
                tab4_txtApprove.Visible = false;
                tab4_btnApprove.Visible = false;
                tab4_btnReject.Visible = false;

                tab5_lblApprove.Visible = false;
                tab5_txtApprove.Visible = false;
                tab5_btnApprove.Visible = false;
                tab5_btnReject.Visible = false;

                tab6_lblApprove.Visible = false;
                tab6_txtApprove.Visible = false;
                tab6_btnApprove.Visible = false;
                tab6_btnReject.Visible = false;

                tab7_lblApprove.Visible = false;
                tab7_txtApprove.Visible = false;
                tab7_btnApprove.Visible = false;
                tab7_btnReject.Visible = false;

                tab8_lblApprove.Visible = false;
                tab8_txtApprove.Visible = false;
                tab8_btnApprove.Visible = false;
                tab8_btnReject.Visible = false;

                tab9_lblApprove.Visible = false;
                tab9_txtApprove.Visible = false;
                tab9_btnApprove.Visible = false;
                tab9_btnReject.Visible = false;

                tab10_lblApprove.Visible = false;
                tab10_txtApprove.Visible = false;
                tab10_btnApprove.Visible = false;
                tab10_btnReject.Visible = false;

                tab1_FileList.Visible = false;
                tab2_FileList.Visible = false;
                tab3_FileList.Visible = false;
                tab4_FileList.Visible = false;
                tab5_FileList.Visible = false;
                tab6_FileList.Visible = false;
                tab7_FileList.Visible = false;
                tab8_FileList.Visible = false;
                tab9_FileList.Visible = false;
                tab10_FileList.Visible = false;

                tab1lblSuccess.Visible = false;
                tab2lblSuccess.Visible = false;
                tab3lblSuccess.Visible = false;
                tab4lblSuccess.Visible = false;
                tab5lblSuccess.Visible = false;
                tab6lblSuccess.Visible = false;
                tab7lblSuccess.Visible = false;
                tab8lblSuccess.Visible = false;
                tab9lblSuccess.Visible = false;
                tab10lblSuccess.Visible = false;

                FileUpload1.Enabled = false;
                tab1btnUpload.Enabled = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

        }

        protected void btnFreeze_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string branchCode = "";
            try
            {
                objMDP.project_owner = Convert.ToString(hdnOwner.Value);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                branchCode = objMDP.LoadBranchCodeby_Owner(ddlSalesEngName.SelectedItem.Value);

                if (direct_customer.Checked == true)
                {
                    objMDP.customer_num = ddlCustomerNo.SelectedItem.Value;
                }
                else
                {
                    objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    objMDP.distributor = ddlDistributorList.SelectedItem.Value;
                }

                objMDP.Industry = ddlIndustry.SelectedItem.Value;
                objMDP.Existing_Product = ExistingProduct.Text;
                objMDP.taegutec_cutter = Convert.ToString(txtTaegutecCutter.Text);
                objMDP.taegutec_insert = Convert.ToString(txtTaegutecInsert.Text);
                objMDP.comp_cutter = Convert.ToString(txtCompCutter.Text);
                objMDP.comp_insert = Convert.ToString(txtCompInsert.Text);
                objMDP.Component = Component.Text;
                //  objMDP.Competition = CompetitionSpec.Text;
                objMDP.NoOfStages = Convert.ToInt32(Stages.SelectedItem.Value);
                objMDP.Target_Date = TargetDate.Text;
                objMDP.OverAll_Potential = String.IsNullOrEmpty(Convert.ToString(OverAllPotential.Text)) ? Convert.ToDecimal(0.0) : Convert.ToDecimal(OverAllPotential.Text);
                objMDP.Business_Expected = Convert.ToDecimal(Business.Text);
                objMDP.Potential_Lakhs = Convert.ToDecimal(Potential.Text);
                objMDP.Sales_Eng_Id = ddlSalesEngName.SelectedItem.Value;
                objMDP.Reviewer = ddlReviewer.SelectedItem.Value;
                objMDP.EscalatedTo = ddlEscalate.SelectedItem.Value;
                objMDP.BranchCode = branchCode;
                objMDP.project_type = ddlProjectType.SelectedItem.Value;
                objMDP.product_group = Convert.ToString(ddlProductGroup.SelectedValue);
                objMDP.Date_Created = DateTime.Now.ToString("dd/MM/yyyy HH:mm tt");

                if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
                {
                    string latest_project_number = "";

                    if (direct_customer.Checked == true)
                    {
                        latest_project_number = objMDP.getLatest_project(ddlCustomerNo.SelectedItem.Value);
                    }
                    else
                    {
                        latest_project_number = objMDP.getLatest_project(ddlCustomerforDistributor.SelectedItem.Value);
                    }

                    if (latest_project_number == "")
                    {
                        objMDP.new_project_number = "1";
                    }
                    else
                    {
                        objMDP.new_project_number = (Convert.ToInt32(latest_project_number) + 1).ToString();
                    }
                    objMDP.project_number = objMDP.new_project_number;
                    objMDP.project_title = txtTitle.Text;
                }
                else
                {
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.project_title = ddlTitle.SelectedItem.Text;
                }

                string Messege2 = objMDP.freezemdp_info(objMDP);
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();

                if (channel_partner.Checked == true)
                {
                    ddlDistributorList.SelectedValue = objMDP.distributor;
                    ddlDistributorName_SelectedIndexChanged(null, null);
                    ddlCustomerforDistributor.SelectedValue = objMDP.customer_num;
                    ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlCustomerName.SelectedValue = objMDP.customer_num;
                    ddlCustomerName_SelectedIndexChanged(null, null);
                }

                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                ddlIndustry.Enabled = false;
                ddlDistibutor.Enabled = false;
                ExistingProduct.Enabled = false;
                txtTaegutecCutter.Enabled = false;
                txtTaegutecInsert.Enabled = false;
                txtCompCutter.Enabled = false;
                txtCompInsert.Enabled = false;
                ddlProjectType.Enabled = false;
                ddlProductGroup.Enabled = false;
                Component.Enabled = false;
                //  CompetitionSpec.Enabled = false;
                Stages.Enabled = false;
                TargetDate.Enabled = false;
                OverAllPotential.Enabled = false;
                Business.Enabled = false;
                Potential.Enabled = false;
                ddlSalesEngName.Enabled = false;
                ddlReviewer.Enabled = false;
                ddlEscalate.Enabled = false;
                //btnFreeze.Enabled = false;
                ddlTitle.Enabled = false;
                txtTitle.Enabled = false;
                ddlDistributorList.Enabled = false;
                ddlCustomerName.Enabled = false;
                ddlCustomerforDistributor.Enabled = false;
                txtOrderTargetCY.Enabled = false;
                txtOrderTargetNY.Enabled = false;

                ddlTitle.SelectedValue = objMDP.project_number;

                ddlTitle_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        #region Approve Project

        protected void tab1btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.Approval_Remarks = tab1_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab1_txtApprove.Enabled = false;
                    tab1_btnApprove.Enabled = false;
                    tab1_btnReject.Enabled = false;
                    SendmailAtApproval(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num,Convert.ToString(ddlSalesEngName.SelectedValue));
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Successfully Approved'); location.href ='PendingTasks.aspx?Approve';", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);

                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab2btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.Approval_Remarks = tab2_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab2_txtApprove.Enabled = false;
                    tab2_btnApprove.Enabled = false;
                    tab2_btnReject.Enabled = false;
                    SendmailAtApproval(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, Convert.ToString(ddlSalesEngName.SelectedValue));
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Successfully Approved'); location.href ='PendingTasks.aspx?Approve';", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab3btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.Approval_Remarks = tab3_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab3_txtApprove.Enabled = false;
                    tab3_btnApprove.Enabled = false;
                    tab3_btnReject.Enabled = false;
                    SendmailAtApproval(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, Convert.ToString(ddlSalesEngName.SelectedValue));
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Successfully Approved'); location.href ='PendingTasks.aspx?Approve';", true);
                }

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab4btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.Approval_Remarks = tab4_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab4_txtApprove.Enabled = false;
                    tab4_btnApprove.Enabled = false;
                    tab4_btnReject.Enabled = false;
                    SendmailAtApproval(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, Convert.ToString(ddlSalesEngName.SelectedValue));
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Successfully Approved'); location.href ='PendingTasks.aspx?Approve';", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab5btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.Approval_Remarks = tab5_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab5_txtApprove.Enabled = false;
                    tab5_btnApprove.Enabled = false;
                    tab5_btnReject.Enabled = false;
                    SendmailAtApproval(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, Convert.ToString(ddlSalesEngName.SelectedValue));
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Successfully Approved'); location.href ='PendingTasks.aspx?Approve';", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab6btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.Approval_Remarks = tab6_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab6_txtApprove.Enabled = false;
                    tab6_btnApprove.Enabled = false;
                    tab6_btnReject.Enabled = false;
                    SendmailAtApproval(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, Convert.ToString(ddlSalesEngName.SelectedValue));
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Successfully Approved'); location.href ='PendingTasks.aspx?Approve';", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab7btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.Approval_Remarks = tab7_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab7_txtApprove.Enabled = false;
                    tab7_btnApprove.Enabled = false;
                    tab7_btnReject.Enabled = false;
                    SendmailAtApproval(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, Convert.ToString(ddlSalesEngName.SelectedValue));
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Successfully Approved'); location.href ='PendingTasks.aspx?Approve';", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab8btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.Approval_Remarks = tab8_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab8_txtApprove.Enabled = false;
                    tab8_btnApprove.Enabled = false;
                    tab8_btnReject.Enabled = false;
                    SendmailAtApproval(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, Convert.ToString(ddlSalesEngName.SelectedValue));
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Successfully Approved'); location.href ='PendingTasks.aspx?Approve';", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab9btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.Approval_Remarks = tab9_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab9_txtApprove.Enabled = false;
                    tab9_btnApprove.Enabled = false;
                    tab9_btnReject.Enabled = false;
                    SendmailAtApproval(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, Convert.ToString(ddlSalesEngName.SelectedValue));
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Successfully Approved'); location.href ='PendingTasks.aspx?Approve';", true);
                    // ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab10btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "1";
                    objMDP.IsRejected = "0";
                    objMDP.Approval_Remarks = tab10_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab10_txtApprove.Enabled = false;
                    tab10_btnApprove.Enabled = false;
                    tab10_btnReject.Enabled = false;
                    SendmailAtApproval(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, Convert.ToString(ddlSalesEngName.SelectedValue));
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Successfully Approved'); location.href ='PendingTasks.aspx?Approve';", true);
                    // ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        #endregion

        #region Reject Project

        protected void tab1btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "0";
                    objMDP.IsRejected = "1";
                    objMDP.Approval_Remarks = tab1_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab1_txtApprove.Enabled = false;
                    tab1_btnApprove.Enabled = false;
                    tab1_btnReject.Enabled = false;
                    SendmailAtRejection(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, objMDP.customer_name, objMDP.Approval_Remarks);
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Rejected'); location.href ='PendingTasks.aspx?Approve';", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab2btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "0";
                    objMDP.IsRejected = "1";
                    objMDP.Approval_Remarks = tab2_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab2_txtApprove.Enabled = false;
                    tab2_btnApprove.Enabled = false;
                    tab2_btnReject.Enabled = false;
                    SendmailAtRejection(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, objMDP.customer_name, objMDP.Approval_Remarks);
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Rejected'); location.href ='PendingTasks.aspx?Approve';", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab3btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "0";
                    objMDP.IsRejected = "1";
                    objMDP.Approval_Remarks = tab3_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab3_txtApprove.Enabled = false;
                    tab3_btnApprove.Enabled = false;
                    tab3_btnReject.Enabled = false;
                    SendmailAtRejection(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, objMDP.customer_name, objMDP.Approval_Remarks);
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Rejected'); location.href ='PendingTasks.aspx?Approve';", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab4btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "0";
                    objMDP.IsRejected = "1";
                    objMDP.Approval_Remarks = tab4_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab4_txtApprove.Enabled = false;
                    tab4_btnApprove.Enabled = false;
                    tab4_btnReject.Enabled = false;
                    SendmailAtRejection(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, objMDP.customer_name, objMDP.Approval_Remarks);
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Rejected'); location.href ='PendingTasks.aspx?Approve';", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab5btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "0";
                    objMDP.IsRejected = "1";
                    objMDP.Approval_Remarks = tab5_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab5_txtApprove.Enabled = false;
                    tab5_btnApprove.Enabled = false;
                    tab5_btnReject.Enabled = false;
                    SendmailAtRejection(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, objMDP.customer_name, objMDP.Approval_Remarks);
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Rejected'); location.href ='PendingTasks.aspx?Approve';", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab6btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "0";
                    objMDP.IsRejected = "1";
                    objMDP.Approval_Remarks = tab6_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab6_txtApprove.Enabled = false;
                    tab6_btnApprove.Enabled = false;
                    tab6_btnReject.Enabled = false;
                    SendmailAtRejection(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, objMDP.customer_name, objMDP.Approval_Remarks);
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Rejected'); location.href ='PendingTasks.aspx?Approve';", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab7btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "0";
                    objMDP.IsRejected = "1";
                    objMDP.Approval_Remarks = tab7_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab7_txtApprove.Enabled = false;
                    tab7_btnApprove.Enabled = false;
                    tab7_btnReject.Enabled = false;
                    SendmailAtRejection(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, objMDP.customer_name, objMDP.Approval_Remarks);
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Rejected'); location.href ='PendingTasks.aspx?Approve';", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab8btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "0";
                    objMDP.IsRejected = "1";
                    objMDP.Approval_Remarks = tab8_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab8_txtApprove.Enabled = false;
                    tab8_btnApprove.Enabled = false;
                    tab8_btnReject.Enabled = false;
                    SendmailAtRejection(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, objMDP.customer_name, objMDP.Approval_Remarks);
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Rejected'); location.href ='PendingTasks.aspx?Approve';", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab9btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "0";
                    objMDP.IsRejected = "1";
                    objMDP.Approval_Remarks = tab9_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab9_txtApprove.Enabled = false;
                    tab9_btnApprove.Enabled = false;
                    tab9_btnReject.Enabled = false;
                    SendmailAtRejection(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, objMDP.customer_name, objMDP.Approval_Remarks);
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Rejected'); location.href ='PendingTasks.aspx?Approve';", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void tab10btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx"); return;
                }
                else
                {
                    if (direct_customer.Checked == true)
                    {
                        objMDP.customer_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        objMDP.customer_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    objMDP.project_number = ddlTitle.SelectedItem.Value;
                    objMDP.IsSentForApproval = "0";
                    objMDP.IsApproved = "0";
                    objMDP.IsRejected = "1";
                    objMDP.Approval_Remarks = tab10_txtApprove.Text;
                    string Message = objMDP.approve_Project(objMDP);
                    tab10_txtApprove.Enabled = false;
                    tab10_btnApprove.Enabled = false;
                    tab10_btnReject.Enabled = false;
                    //anamika
                    SendmailAtRejection(Convert.ToString(hdnOwner.Value), Convert.ToString(ddlReviewer.SelectedValue), Convert.ToString(ddlEscalate.SelectedValue), Convert.ToString(ddlTitle.SelectedItem.Text), objMDP.project_number, objMDP.customer_num, objMDP.customer_name, objMDP.Approval_Remarks);
                    Enable_NewCustomer(null, null);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "alert('Rejected'); location.href ='PendingTasks.aspx?Approve';", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        #endregion

        /// <summary>
        /// Author : Anamika
        /// Date : 
        /// Description :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void existing_project_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DisableControls();
                //Label attachDoc = (Label)tab.FindControl("tab1doc");
                //// Label tab1Doc = (Label)FindControl("tab" + j + "doc");
                //attachDoc.Visible = true;
                //FileUpload fUpload = (FileUpload)tab.FindControl("FileUpload1");
                //fUpload.Visible = true;
                //ImageButton btnUpload = (ImageButton)tab.FindControl("tab1btnUpload");
                //btnUpload.Visible = true;

                tab1btnSave.Enabled = true;
                if (ddlCustomerName.SelectedValue == "-- SELECT CUSTOMER --" && ddlCustomerforDistributor.SelectedValue == "-- SELECT CUSTOMER --")
                {
                    ddlTitle.Enabled = false;
                    ddlCustomerforDistributor.Enabled = true;
                    ddlCustomerName.Enabled = true;
                    ddlDistributorList.Enabled = true;
                }
                else
                {
                    string customer = string.Empty;
                    if (direct_customer.Checked)
                        customer = Convert.ToString(ddlCustomerName.SelectedValue);
                    else
                        customer = Convert.ToString(ddlCustomerforDistributor.SelectedValue);
                    ddlTitle.Enabled = true;
                    DataTable dtProjectTitles = new DataTable();
                    dtProjectTitles = objMDP.LoadProjectTitles(customer);
                    if (dtProjectTitles != null)
                    {
                        DataTable dtDeatils = new DataTable();
                        dtDeatils.Columns.Add("project_number", typeof(string));
                        dtDeatils.Columns.Add("project_title", typeof(string));
                        for (int i = 0; i < dtProjectTitles.Rows.Count; i++)
                        {
                            dtDeatils.Rows.Add(dtProjectTitles.Rows[i].ItemArray[0].ToString(), dtProjectTitles.Rows[i].ItemArray[1].ToString());
                        }
                        ddlTitle.DataSource = dtDeatils;
                        ddlTitle.DataTextField = "project_title";
                        ddlTitle.DataValueField = "project_number";
                        ddlTitle.DataBind();

                        if (dtProjectTitles.Rows.Count == 0)
                        {
                            ddlTitle.Items.Insert(0, "-- NO PROJECT --");
                        }
                        else
                        {
                            ddlTitle.Items.Insert(0, "-- SELECT PROJECT --");
                        }
                    }
                    // ddlCustomerName_SelectedIndexChanged(null, null);
                }
                if (ddlTitle.SelectedValue == "-- SELECT PROJECT --" || ddlTitle.SelectedValue == "-- NO PROJECT --" || ddlTitle.SelectedValue == "")
                {
                    ddlDistributorList.Enabled = true;
                    ddlCustomerName.Enabled = true;
                    ddlCustomerforDistributor.Enabled = true;

                }
                if (ddlTitle.SelectedValue != "-- SELECT PROJECT --" && ddlTitle.SelectedValue != "-- NO PROJECT --" && ddlTitle.SelectedValue != "")
                {
                    if (existing_project.Checked == true)
                    {
                        int finalstage = 0;
                        finalstage = Convert.ToInt32(Stages.SelectedValue);
                        for (int j = 1; j < finalstage; j++)
                        {
                            ImageButton btnRemarks = (ImageButton)tab.FindControl("tab" + j + "btnRemarks");
                            btnRemarks.Visible = true;
                        }
                    }
                }

                TSProject_Changes();
                //else
                //{
                //  ddlCustomerName_SelectedIndexChanged(null, null);
                //}


                //ExistingProduct.Enabled = false;
                //ddlProjectType.Enabled = false;
                //Component.Enabled = false;
                //CompetitionSpec.Enabled = false;
                //Stages.Enabled = false;
                //Target_flag.Value = "false";
                //OverAllPotential.Enabled = false;
                //Business.Enabled = false;
                //Potential.Enabled = false;
                //ddlSalesEngName.Enabled = false;
                //ddlReviewer.Enabled = false;
                //ddlEscalate.Enabled = false;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "disablecontrols();", true);
                ////TargetDate.Enabled = false;
                //ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
                //if (ddlTitle.SelectedValue == "-- SELECT PROJECT --" || ddlTitle.SelectedValue == "-- NO PROJECT --")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "disable();", true);
                //}
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : 
        /// Description :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void new_project_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (existing_project.Checked != true)
                {

                    int finalstage = 0;
                    finalstage = Convert.ToInt32(Stages.SelectedValue);
                    for (int j = 1; j <= finalstage; j++)
                    {

                        ImageButton btnRemarks = (ImageButton)tab.FindControl("tab" + j + "btnRemarks");
                        btnRemarks.Visible = false;
                    }
                }
                EnableControls();
               
                ddlCustomerNo.Enabled = true;
                ddlCustomerName.Enabled = true;
                ddlCustomerName.SelectedValue = "-- SELECT CUSTOMER --";
                ddlCustomerNo.SelectedValue = "-- SELECT CUSTOMER NUMBER --";
                
                clear();
                TSProject_Changes();
                if (ddlTitle.Items.Count > 0)
                    ddlTitle.SelectedValue = "-- SELECT PROJECT --";
                ddlDistibutor.SelectedValue = "--SELECT--";
                ddlDistibutor.Enabled = true;
                ddlDistributorList.Enabled = true;
                //ExistingProduct.Enabled = true;
                //ddlProjectType.Enabled = true;
                //Component.Enabled = true;
                //CompetitionSpec.Enabled = true;
                //Stages.Enabled = true;
                ////TargetDate.Enabled = true;
                //Target_flag.Value = "True";
                //OverAllPotential.Enabled = true;
                //Business.Enabled = true;
                //Potential.Enabled = true;
                //ddlReviewer.Enabled = true;
                //ddlEscalate.Enabled = true;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "enablecontrols();", true);
                //ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
                //if (ddlTitle.SelectedValue != "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "disable();", true);
                //}
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

        }

        /// <summary>
        /// Author : Anamika
        /// Date : 
        /// Description :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Stages_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int finalstage = 0;
                finalstage = Convert.ToInt32(Stages.SelectedValue);
                for (int j = 1; j < finalstage; j++)
                {
                    ImageButton btnSave = (ImageButton)tab.FindControl("tab" + j + "btnSave");
                    btnSave.Visible = true;
                    ImageButton btnSaveApprove = (ImageButton)tab.FindControl("tab" + j + "btnSaveApprove");
                    btnSaveApprove.Visible = false;
                }
                ImageButton btnSave1 = (ImageButton)tab.FindControl("tab" + finalstage + "btnSave");
                btnSave1.Visible = false;
                ImageButton btnSaveApprove1 = (ImageButton)tab.FindControl("tab" + finalstage + "btnSaveApprove");
                btnSaveApprove1.Visible = true;
                if (new_project.Checked == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "disable();", true);
                }
                else
                {
                    stagesvisibility(ddlTitle.SelectedValue);
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        //try
        //{
        //    int finalstage = 0;
        //    finalstage = Convert.ToInt32(Stages.SelectedValue);
        //    for (int j = 1; j < finalstage; j++)
        //    {

        //        Label attachDoc = (Label)tab.FindControl("tab" + j + "doc");
        //        Label tab1Doc = (Label)FindControl("tab" + j + "doc");
        //        attachDoc.Visible = true;
        //        FileUpload fUpload = (FileUpload)tab.FindControl("FileUpload" + j);
        //        fUpload.Visible = true;
        //        ImageButton btnUpload = (ImageButton)tab.FindControl("tab" + j + "btnUpload");
        //        btnUpload.Visible = true;

        //        ImageButton btnSave = (ImageButton)tab.FindControl("tab" + j + "btnSave");
        //        btnSave.Visible = true;
        //        ImageButton btnSaveApprove = (ImageButton)tab.FindControl("tab" + j + "btnSaveApprove");
        //        btnSaveApprove.Visible = false;     
        //    }
        //        ImageButton btnSave1 = (ImageButton)tab.FindControl("tab" + finalstage + "btnSave");
        //        btnSave1.Visible = false;
        //        ImageButton btnSaveApprove1 = (ImageButton)tab.FindControl("tab" + finalstage + "btnSaveApprove");
        //        btnSaveApprove1.Visible = true;
        //        //ImageButton btnRemarks = (ImageButton)tab.FindControl("tab" + finalstage + "btnRemarks");
        //        //      btnRemarks.Visible = false; 


        //    if (new_project.Checked == true)
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "disable();", true);
        //    }
        //    else
        //    {
        //        stagesvisibility(ddlTitle.SelectedValue);
        //    }
        //    ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
        //}
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //}

        protected void tab1Target_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (new_project.Checked == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "disable();", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        #region delete stage files  Anantha
        protected void DeleteFile_tab1(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            LinkButton lb = (LinkButton)sender;
            int file_number = Convert.ToInt32(lb.CommandArgument.ToString());
            string file_name = objMDP.Get_File_Names_byNumber(file_number);

            string strFileName = file_name;// lnkDownload.Text;
            string path = Server.MapPath("~/Uploads//" + strFileName);
            File.Delete(path);
            if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
            {
                objMDP.file_project_num = objMDP.new_project_number;
            }
            else
            {
                objMDP.file_project_num = ddlTitle.SelectedItem.Value;
            }
            objMDP.file_Stage_Number = "1";
            objMDP.file_name = file_name;
            string Message = objMDP.Delete_file_details(objMDP);
            ddlTitle_SelectedIndexChanged(null, null);
            if (Message == "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Successfully Deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Something Went Wrong.');", true);
            }
            tab_number.Value = "1";
            //ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            tab1_FileList.DataBind();

        }
        protected void DeleteFile_tab2(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            LinkButton lb = (LinkButton)sender;
            int file_number = Convert.ToInt32(lb.CommandArgument.ToString());
            string file_name = objMDP.Get_File_Names_byNumber(file_number);
            string strFileName = file_name;// lnkDownload.Text;
            string path = Server.MapPath("~/Uploads//" + strFileName);
            File.Delete(path);
            if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
            {
                objMDP.file_project_num = objMDP.new_project_number;
            }
            else
            {
                objMDP.file_project_num = ddlTitle.SelectedItem.Value;
            }
            objMDP.file_Stage_Number = "2";
            objMDP.file_name = file_name;
            string Message = objMDP.Delete_file_details(objMDP);
            ddlTitle_SelectedIndexChanged(null, null);
            if (Message == "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Successfully Deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Something Went Wrong.');", true);
            }
            tab_number.Value = "2";
            ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            tab2_FileList.DataBind();
        }
        protected void DeleteFile_tab3(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            LinkButton lb = (LinkButton)sender;
            int file_number = Convert.ToInt32(lb.CommandArgument.ToString());
            string file_name = objMDP.Get_File_Names_byNumber(file_number);
            string strFileName = file_name;// lnkDownload.Text;
            string path = Server.MapPath("~/Uploads//" + strFileName);
            File.Delete(path);
            if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
            {
                objMDP.file_project_num = objMDP.new_project_number;
            }
            else
            {
                objMDP.file_project_num = ddlTitle.SelectedItem.Value;
            }
            objMDP.file_Stage_Number = "3";
            objMDP.file_name = file_name;
            string Message = objMDP.Delete_file_details(objMDP);
            ddlTitle_SelectedIndexChanged(null, null);
            if (Message == "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Successfully Deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Something Went Wrong.');", true);
            }
            tab_number.Value = "3";
            ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            tab3_FileList.DataBind();
        }
        protected void DeleteFile_tab4(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            LinkButton lb = (LinkButton)sender;
            int file_number = Convert.ToInt32(lb.CommandArgument.ToString());
            string file_name = objMDP.Get_File_Names_byNumber(file_number);
            string strFileName = file_name;// lnkDownload.Text;
            string path = Server.MapPath("~/Uploads//" + strFileName);
            File.Delete(path);
            if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
            {
                objMDP.file_project_num = objMDP.new_project_number;
            }
            else
            {
                objMDP.file_project_num = ddlTitle.SelectedItem.Value;
            }
            objMDP.file_Stage_Number = "4";
            objMDP.file_name = file_name;
            string Message = objMDP.Delete_file_details(objMDP);
            ddlTitle_SelectedIndexChanged(null, null);
            if (Message == "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Successfully Deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Something Went Wrong.');", true);
            }
            tab_number.Value = "4";
            ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            tab4_FileList.DataBind();
        }
        protected void DeleteFile_tab5(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            LinkButton lb = (LinkButton)sender;
            int file_number = Convert.ToInt32(lb.CommandArgument.ToString());
            string file_name = objMDP.Get_File_Names_byNumber(file_number);
            string strFileName = file_name;// lnkDownload.Text;
            string path = Server.MapPath("~/Uploads//" + strFileName);
            File.Delete(path);
            if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
            {
                objMDP.file_project_num = objMDP.new_project_number;
            }
            else
            {
                objMDP.file_project_num = ddlTitle.SelectedItem.Value;
            }
            objMDP.file_Stage_Number = "5";
            objMDP.file_name = file_name;
            string Message = objMDP.Delete_file_details(objMDP);
            ddlTitle_SelectedIndexChanged(null, null);
            if (Message == "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Successfully Deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Something Went Wrong.');", true);
            }
            tab_number.Value = "5";
            ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            tab5_FileList.DataBind();
        }
        protected void DeleteFile_tab6(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            LinkButton lb = (LinkButton)sender;
            int file_number = Convert.ToInt32(lb.CommandArgument.ToString());
            string file_name = objMDP.Get_File_Names_byNumber(file_number);
            string strFileName = file_name;// lnkDownload.Text;
            string path = Server.MapPath("~/Uploads//" + strFileName);
            File.Delete(path);
            if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
            {
                objMDP.file_project_num = objMDP.new_project_number;
            }
            else
            {
                objMDP.file_project_num = ddlTitle.SelectedItem.Value;
            }
            objMDP.file_Stage_Number = "6";
            objMDP.file_name = file_name;
            string Message = objMDP.Delete_file_details(objMDP);
            if (Message == "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Successfully Deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Something Went Wrong.');", true);
            }
            tab_number.Value = "6";
            ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            tab6_FileList.DataBind();
        }
        protected void DeleteFile_tab7(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            LinkButton lb = (LinkButton)sender;
            int file_number = Convert.ToInt32(lb.CommandArgument.ToString());
            string file_name = objMDP.Get_File_Names_byNumber(file_number);
            string strFileName = file_name;// lnkDownload.Text;
            string path = Server.MapPath("~/Uploads//" + strFileName);
            File.Delete(path);
            if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
            {
                objMDP.file_project_num = objMDP.new_project_number;
            }
            else
            {
                objMDP.file_project_num = ddlTitle.SelectedItem.Value;
            }
            objMDP.file_Stage_Number = "7";
            objMDP.file_name = file_name;
            string Message = objMDP.Delete_file_details(objMDP);
            if (Message == "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Successfully Deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Something Went Wrong.');", true);
            }
            tab_number.Value = "7";
            ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            tab7_FileList.DataBind();
        }
        protected void DeleteFile_tab8(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            LinkButton lb = (LinkButton)sender;
            int file_number = Convert.ToInt32(lb.CommandArgument.ToString());
            string file_name = objMDP.Get_File_Names_byNumber(file_number);
            string strFileName = file_name;// lnkDownload.Text;
            string path = Server.MapPath("~/Uploads//" + strFileName);
            File.Delete(path);
            if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
            {
                objMDP.file_project_num = objMDP.new_project_number;
            }
            else
            {
                objMDP.file_project_num = ddlTitle.SelectedItem.Value;
            }
            objMDP.file_Stage_Number = "7";
            objMDP.file_name = file_name;
            string Message = objMDP.Delete_file_details(objMDP);
            if (Message == "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Successfully Deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Something Went Wrong.');", true);
            }
            tab_number.Value = "8";
            ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            tab8_FileList.DataBind();
        }
        protected void DeleteFile_tab9(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            LinkButton lb = (LinkButton)sender;
            int file_number = Convert.ToInt32(lb.CommandArgument.ToString());
            string file_name = objMDP.Get_File_Names_byNumber(file_number);
            string strFileName = file_name;// lnkDownload.Text;
            string path = Server.MapPath("~/Uploads//" + strFileName);
            File.Delete(path);
            if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
            {
                objMDP.file_project_num = objMDP.new_project_number;
            }
            else
            {
                objMDP.file_project_num = ddlTitle.SelectedItem.Value;
            }
            objMDP.file_Stage_Number = "9";
            objMDP.file_name = file_name;
            string Message = objMDP.Delete_file_details(objMDP);
            if (Message == "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Successfully Deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Something Went Wrong.');", true);
            }
            tab_number.Value = "9";
            ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            tab9_FileList.DataBind();
        }
        protected void DeleteFile_tab10(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            LinkButton lb = (LinkButton)sender;
            int file_number = Convert.ToInt32(lb.CommandArgument.ToString());
            string file_name = objMDP.Get_File_Names_byNumber(file_number);
            string strFileName = file_name;// lnkDownload.Text;
            string path = Server.MapPath("~/Uploads//" + strFileName);
            File.Delete(path);
            if (new_project.Checked && ddlTitle.SelectedItem.Value == "-- SELECT PROJECT --")
            {
                objMDP.file_project_num = objMDP.new_project_number;
            }
            else
            {
                objMDP.file_project_num = ddlTitle.SelectedItem.Value;
            }
            objMDP.file_Stage_Number = "10";
            objMDP.file_name = file_name;
            string Message = objMDP.Delete_file_details(objMDP);
            if (Message == "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Successfully Deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('Something Went Wrong.');", true);
            }
            tab_number.Value = "10";
            ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            tab10_FileList.DataBind();
        }
        #endregion

        #endregion

        #region Methods
        #region Download Stage Files
        private void RegisterDownloadPostBackControl()
        {

        }
        #endregion



        protected void showApproveAndReject()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                string salesEngName = Session["UserName"].ToString();
                string branchCode = Session["BranchCode"].ToString();

                ddlIndustry.Enabled = false;
                if (Session["Customer_Number"] != null && Session["Project_Number"] != null)
                {
                    if (Session["Distributor_Number"].ToString() != "")
                    {
                        ddlDistributorList.SelectedValue = Session["Distributor_Number"].ToString();
                        ddlDistributorName_SelectedIndexChanged(null, null);
                        ddlCustomerforDistributor.SelectedValue = Session["Customer_Number"].ToString();
                        ddlCustomerforDistributor_SelectedIndexChanged(null, null);
                        direct_customer.Checked = false;
                        channel_partner.Checked = true;
                    }
                    else
                    {
                        ddlCustomerName.SelectedValue = Session["Customer_Number"].ToString();
                        ddlCustomerName_SelectedIndexChanged(null, null);
                    }
                    ddlTitle.SelectedValue = Session["Project_Number"].ToString();
                    ddlTitle_SelectedIndexChanged(null, null);
                    //#region Showing Approve and Reject
                    string cust_num = "";
                    if (direct_customer.Checked == true)
                    {
                        cust_num = ddlCustomerName.SelectedItem.Value;
                    }
                    else
                    {
                        cust_num = ddlCustomerforDistributor.SelectedItem.Value;
                    }
                    string proj_num = ddlTitle.SelectedValue;
                    DataTable dtMDPDetails = objMDP.LoadMDP_Details_byCustomer_Number(cust_num, proj_num);
                    string sentforApproval = "";
                    string IsApproved = "";
                    string IsRejected = "";
                    string remarks = "";
                    string reviewer = ddlReviewer.SelectedItem.Value;

                    string ts_flag = "";

                    string tab_no = Stages.SelectedItem.Value;
                    if (dtMDPDetails.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtMDPDetails.Rows.Count; i++)
                        {
                            sentforApproval = dtMDPDetails.Rows[i].ItemArray[26].ToString();
                            IsApproved = dtMDPDetails.Rows[i].ItemArray[27].ToString();
                            IsRejected = dtMDPDetails.Rows[i].ItemArray[28].ToString();
                            remarks = dtMDPDetails.Rows[i].ItemArray[29].ToString();
                            ts_flag = Convert.ToString(dtMDPDetails.Rows[i].ItemArray[36]);
                        }
                    }
                    if (ts_flag == "Y")
                    {
                        rdbtsProject.Checked = true;
                        tab1txtGoal.Enabled = false;
                        tab2txtGoal.Enabled = false;
                        tab3txtGoal.Enabled = false;
                        tab4txtGoal.Enabled = false;
                        tab5txtGoal.Enabled = false;
                        tab6txtGoal.Enabled = false;
                        tab7txtGoal.Enabled = false;
                        tab8txtGoal.Enabled = false;
                        divCY.Visible = true;
                        divNY.Visible = true;
                        divCustPot.Visible = false;
                    }
                    else
                    {
                        rdbNormalProject.Checked = true;
                        divCY.Visible = false;
                        divNY.Visible = false;
                        divCustPot.Visible = true;
                    }
                    if (sentforApproval == "True" && IsApproved == "False" && strUserId == reviewer)
                    {
                        TextBox txtApp_Remarks = (TextBox)tab.FindControl("tab" + tab_no + "_txtApprove");
                        ImageButton btnApprove = (ImageButton)tab.FindControl("tab" + tab_no + "_btnApprove");
                        ImageButton btnReject = (ImageButton)tab.FindControl("tab" + tab_no + "_btnReject");
                        Label lblApprove = (Label)tab.FindControl("tab" + tab_no + "_lblApprove");
                        txtApp_Remarks.Visible = true;
                        btnApprove.Visible = true;
                        btnReject.Visible = true;
                        lblApprove.Visible = true;
                        txtApp_Remarks.Text = remarks;
                    }
                    //#endregion
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

        }
        protected void resetControls()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                tab_number.Value = "";
                Target_flag.Value = "";
                tab1Target_flag.Value = "";
                tab2Target_flag.Value = "";
                tab3Target_flag.Value = "";
                tab4Target_flag.Value = "";
                tab5Target_flag.Value = "";
                tab6Target_flag.Value = "";
                tab7Target_flag.Value = "";
                tab8Target_flag.Value = "";
                tab9Target_flag.Value = "";
                tab10Target_flag.Value = "";

                tab1Completion_flag.Value = "";
                tab2Completion_flag.Value = "";
                tab3Completion_flag.Value = "";
                tab4Completion_flag.Value = "";
                tab5Completion_flag.Value = "";
                tab6Completion_flag.Value = "";
                tab7Completion_flag.Value = "";
                tab8Completion_flag.Value = "";
                tab9Completion_flag.Value = "";
                tab10Completion_flag.Value = "";
                #region NewRemarks

                foreach (string textboxId in tab1_TextBoxIdCollection)
                {

                    var textbox = new TextBox();
                    textbox.ID = textboxId;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab1Remarks.Controls.Add(textbox);

                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab1Remarks.Controls.Add(para);

                }
                foreach (string textboxId in tab2_TextBoxIdCollection)
                {
                    var textbox = new TextBox();
                    textbox.ID = textboxId;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab2Remarks.Controls.Add(textbox);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab2Remarks.Controls.Add(para);
                }

                foreach (string textboxId in tab3_TextBoxIdCollection)
                {
                    var textbox = new TextBox();
                    textbox.ID = textboxId;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab3Remarks.Controls.Add(textbox);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab3Remarks.Controls.Add(para);
                }

                foreach (string textboxId in tab4_TextBoxIdCollection)
                {
                    var textbox = new TextBox();
                    textbox.ID = textboxId;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab4Remarks.Controls.Add(textbox);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab4Remarks.Controls.Add(para);
                }

                foreach (string textboxId in tab5_TextBoxIdCollection)
                {
                    var textbox = new TextBox();
                    textbox.ID = textboxId;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab5Remarks.Controls.Add(textbox);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab5Remarks.Controls.Add(para);
                }

                foreach (string textboxId in tab6_TextBoxIdCollection)
                {
                    var textbox = new TextBox();
                    textbox.ID = textboxId;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab6Remarks.Controls.Add(textbox);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab6Remarks.Controls.Add(para);
                }

                foreach (string textboxId in tab7_TextBoxIdCollection)
                {
                    var textbox = new TextBox();
                    textbox.ID = textboxId;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab7Remarks.Controls.Add(textbox);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab7Remarks.Controls.Add(para);

                }

                foreach (string textboxId in tab8_TextBoxIdCollection)
                {
                    var textbox = new TextBox();
                    textbox.ID = textboxId;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab8Remarks.Controls.Add(textbox);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab8Remarks.Controls.Add(para);
                }

                foreach (string textboxId in tab9_TextBoxIdCollection)
                {
                    var textbox = new TextBox();
                    textbox.ID = textboxId;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab9Remarks.Controls.Add(textbox);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab9Remarks.Controls.Add(para);
                }

                foreach (string textboxId in tab10_TextBoxIdCollection)
                {
                    var textbox = new TextBox();
                    textbox.ID = textboxId;
                    textbox.TextMode = TextBoxMode.MultiLine;
                    textbox.Rows = 2;
                    textbox.Columns = 100;
                    textbox.CssClass = "form-control stageinput addtext";
                    tab10Remarks.Controls.Add(textbox);
                    HtmlGenericControl para = new HtmlGenericControl("p");
                    tab10Remarks.Controls.Add(para);
                }
                #endregion
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

        }

        #region Methods to populate Customer and Distributor Dropdowns
        /// <summary>
        /// Method to Load Customer Name and Number in the corresponding Drop Down Lists
        /// </summary>
        /// <param name="strUserId"></param>
        /// <param name="roleId"></param>
        protected void LoadCustomerDetails_SE(string strUserId)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                dtCutomerDetails = new DataTable();
                // dtCutomerDetails = objMDP.LoadCustomerDetails_SE(strUserId);
                dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, "SE", null, null, "C");
                if (dtCutomerDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_name", typeof(string));


                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        // dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                        dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                    }
                    ddlCustomerName.DataSource = dtDeatils;
                    ddlCustomerName.DataTextField = "customer_name";
                    ddlCustomerName.DataValueField = "customer_number";
                    ddlCustomerName.DataBind();
                    ddlCustomerName.Items.Insert(0, "-- SELECT CUSTOMER --");

                    ddlCustomerNo.DataSource = dtCutomerDetails;
                    ddlCustomerNo.DataTextField = "customer_number";
                    ddlCustomerNo.DataBind();
                    ddlCustomerNo.Items.Insert(0, "-- SELECT CUSTOMER NUMBER --");

                }

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

        }

        protected void LoadCustomerDetails_BM(string strUserId)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                dtCutomerDetails = new DataTable();
                // dtCutomerDetails = objMDP.LoadCustomerDetails_BM(branchCode);
                dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, "BM", null, null, "C");
                if (dtCutomerDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_name", typeof(string));


                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                        dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[0].ToString(), dtCutomerDetails.Rows[i].ItemArray[1].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlCustomerName.DataSource = dtDeatils;
                    ddlCustomerName.DataTextField = "customer_name";
                    ddlCustomerName.DataValueField = "customer_number";
                    ddlCustomerName.DataBind();
                    ddlCustomerName.Items.Insert(0, "-- SELECT CUSTOMER --");

                    ddlCustomerNo.DataSource = dtCutomerDetails;
                    ddlCustomerNo.DataTextField = "customer_number";
                    ddlCustomerNo.DataBind();
                    ddlCustomerNo.Items.Insert(0, "-- SELECT CUSTOMER NUMBER --");

                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadDistributor_SE(string strUserId)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                //dtCutomerDetails = new DataTable();
                //dtCutomerDetails = objMDP.LoadDistributor_SE(strUserId);
                DataTable dtDistributor = new DataTable();
                dtDistributor = objBudget.LoadCustomerDetails(strUserId, "SE", null, null, "D");
                if (dtDistributor != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_name", typeof(string));


                    for (int i = 0; i < dtDistributor.Rows.Count; i++)
                    {
                        //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[0].ToString(), dtCutomerDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[0].ToString() + ")");
                        dtDeatils.Rows.Add(dtDistributor.Rows[i].ItemArray[1].ToString(), dtDistributor.Rows[i].ItemArray[2].ToString() + "(" + dtDistributor.Rows[i].ItemArray[1].ToString() + ")");
                    }
                    ddlDistributorList.DataSource = dtDeatils;
                    ddlDistributorList.DataTextField = "customer_name";
                    ddlDistributorList.DataValueField = "customer_number";
                    ddlDistributorList.DataBind();
                    ddlDistributorList.Items.Insert(0, "-- SELECT CHANNEL PARTNER --");



                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }


        }

        protected void LoadDistributor_BM(string strUserId)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                //dtCutomerDetails = new DataTable();
                //dtCutomerDetails = objMDP.LoadDistributor_BM(branchCode);
                DataTable dtDistributor = new DataTable();

                dtDistributor = objBudget.LoadCustomerDetails(strUserId, "BM", null, cter, "D");
                if (dtDistributor != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_name", typeof(string));


                    for (int i = 0; i < dtDistributor.Rows.Count; i++)
                    {
                        //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[0].ToString(), dtCutomerDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[0].ToString() + ")");
                        dtDeatils.Rows.Add(dtDistributor.Rows[i].ItemArray[0].ToString(), dtDistributor.Rows[i].ItemArray[1].ToString() + "(" + dtDistributor.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlDistributorList.DataSource = dtDeatils;
                    ddlDistributorList.DataTextField = "customer_name";
                    ddlDistributorList.DataValueField = "customer_number";
                    ddlDistributorList.DataBind();
                    ddlDistributorList.Items.Insert(0, "-- SELECT CHANNEL PARTNER --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadCustomerDetails_TM(string strUserId)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                dtCutomerDetails = new DataTable();
                // dtCutomerDetails = objMDP.LoadCustomerDetails_TM(strUserId);
                dtCutomerDetails = objBudget.LoadCustomerDetails("ALL", "TM", strUserId, null, "C");
                if (dtCutomerDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_name", typeof(string));


                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                        dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");

                    }
                    ddlCustomerName.DataSource = dtDeatils;
                    ddlCustomerName.DataTextField = "customer_name";
                    ddlCustomerName.DataValueField = "customer_number";
                    ddlCustomerName.DataBind();
                    ddlCustomerName.Items.Insert(0, "-- SELECT CUSTOMER --");

                    ddlCustomerNo.DataSource = dtCutomerDetails;
                    ddlCustomerNo.DataTextField = "customer_number";
                    ddlCustomerNo.DataBind();
                    ddlCustomerNo.Items.Insert(0, "-- SELECT CUSTOMER NUMBER --");

                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void LoadDistributor_TM(string strUserId)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                //dtCutomerDetails = new DataTable();
                //dtCutomerDetails = objMDP.LoadDistributor_TM(strUserId);
                DataTable dtDistributor = new DataTable();
                dtDistributor = objBudget.LoadCustomerDetails("ALL", "TM", strUserId, null, "D");
                if (dtDistributor != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_name", typeof(string));


                    for (int i = 0; i < dtDistributor.Rows.Count; i++)
                    {
                        //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[0].ToString(), dtCutomerDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[0].ToString() + ")");
                        dtDeatils.Rows.Add(dtDistributor.Rows[i].ItemArray[1].ToString(), dtDistributor.Rows[i].ItemArray[2].ToString() + " " + "(" + dtDistributor.Rows[i].ItemArray[1].ToString() + ")");
                    }
                    ddlDistributorList.DataSource = dtDeatils;
                    ddlDistributorList.DataTextField = "customer_name";
                    ddlDistributorList.DataValueField = "customer_number";
                    ddlDistributorList.DataBind();
                    ddlDistributorList.Items.Insert(0, "-- SELECT CHANNEL PARTNER --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadCustomerDetails_HO()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                dtCutomerDetails = new DataTable();
                //dtCutomerDetails = objMDP.LoadCustomerDetails_HO();
                dtCutomerDetails = objBudget.LoadCustomerDetails(null, "HO", null, cter, "C");
                if (dtCutomerDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_name", typeof(string));
                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                    }
                    ddlCustomerName.DataSource = dtDeatils;
                    ddlCustomerName.DataTextField = "customer_name";
                    ddlCustomerName.DataValueField = "customer_number";
                    ddlCustomerName.DataBind();
                    ddlCustomerName.Items.Insert(0, "-- SELECT CUSTOMER --");

                    ddlCustomerNo.DataSource = dtCutomerDetails;
                    ddlCustomerNo.DataTextField = "customer_number";
                    ddlCustomerNo.DataBind();
                    ddlCustomerNo.Items.Insert(0, "-- SELECT CUSTOMER NUMBER --");

                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllDistributors()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtDistributor = new DataTable();
                //  dtDistributor = objMDP.LoadAllDistributors();
                dtDistributor = objBudget.LoadCustomerDetails(null, "HO", null, cter, "D");
                if (dtDistributor != null)
                {
                    DataTable dtDst = new DataTable();
                    //dtDeatils.Columns.Add("distributor_number", typeof(string));
                    //dtDeatils.Columns.Add("distributor_name", typeof(string));
                    dtDst.Columns.Add("customer_number", typeof(string));
                    dtDst.Columns.Add("customer_name", typeof(string));

                    for (int i = 0; i < dtDistributor.Rows.Count; i++)
                    {
                        // dtDeatils.Rows.Add(dtDistributor.Rows[i].ItemArray[0].ToString(), dtDistributor.Rows[i].ItemArray[1].ToString() + " " + "(" + dtDistributor.Rows[i].ItemArray[0].ToString() + ")");
                        dtDst.Rows.Add(dtDistributor.Rows[i].ItemArray[1].ToString(), dtDistributor.Rows[i].ItemArray[2].ToString() + " " + "(" + dtDistributor.Rows[i].ItemArray[1].ToString() + ")");

                    }
                    ddlDistributorList.DataSource = dtDst;
                    //ddlDistributorList.DataTextField = "distributor_name";
                    //ddlDistributorList.DataValueField = "distributor_number";
                    ddlDistributorList.DataTextField = "customer_name";
                    ddlDistributorList.DataValueField = "customer_number";
                    ddlDistributorList.DataBind();
                    ddlDistributorList.Items.Insert(0, "-- SELECT CHANNEL PARTNER --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        #endregion

        protected void LoadAllIndustries()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtIndustry = new DataTable();
                dtIndustry = objMDP.GetAllIndustriesEntry();
                if (dtIndustry != null)
                {
                    DataTable dtDeatils1 = new DataTable();
                    dtDeatils1.Columns.Add("IndustryId", typeof(string));
                    dtDeatils1.Columns.Add("IndustryName", typeof(string));
                    for (int i = 0; i < dtIndustry.Rows.Count; i++)
                    {
                        dtDeatils1.Rows.Add(dtIndustry.Rows[i].ItemArray[0].ToString(), dtIndustry.Rows[i].ItemArray[1].ToString() + " " + "(" + dtIndustry.Rows[i].ItemArray[2].ToString() + ")");
                    }
                    ddlIndustry.DataSource = dtDeatils1;
                    ddlIndustry.DataTextField = "IndustryName";
                    ddlIndustry.DataValueField = "IndustryId";
                    ddlIndustry.DataBind();
                    ddlIndustry.Items.Insert(0, "-- SELECT --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        #region Methods to Load Owner,Reviewer and Escalated to fropdowns
        /// <summary>
        /// Method to load all the HO to the escalate to dropdown
        /// </summary>
        protected void LoadAllHOs()
        {
            DataTable dtHoDetails;
            DataTable dtDeatils;
            try
            {
                dtHoDetails = new DataTable();
                objMDP.userid = Convert.ToString(Session["UserId"]);
                objMDP.ts_project_flag = rdbtsProject.Checked ? "Y" : "N";
                dtHoDetails = objMDP.GetAllHO(objMDP);
                if (dtHoDetails != null)
                {
                    dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("HO_number", typeof(string));
                    dtDeatils.Columns.Add("HO_name", typeof(string));


                    for (int i = 0; i < dtHoDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtHoDetails.Rows[i].ItemArray[0].ToString(), dtHoDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtHoDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlEscalate.DataSource = dtDeatils;
                    ddlEscalate.DataTextField = "HO_name";
                    ddlEscalate.DataValueField = "HO_number";
                    ddlEscalate.DataBind();
                    ddlEscalate.Items.Insert(0, "-- SELECT --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        protected void LoadAllReviewer_HO()
        {
            DataTable dtREDetails;
            DataTable dtDeatils;
            try
            {
                dtREDetails = new DataTable();
                objMDP.userid = Convert.ToString(Session["UserId"]);
                objMDP.ts_project_flag = rdbtsProject.Checked ? "Y" : "N";
                dtREDetails = objMDP.GetAllHO(objMDP);
                if (dtREDetails != null)
                {
                    dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("RE_number", typeof(string));
                    dtDeatils.Columns.Add("RE_name", typeof(string));


                    for (int i = 0; i < dtREDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtREDetails.Rows[i].ItemArray[0].ToString(), dtREDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtREDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlReviewer.DataSource = dtDeatils;
                    ddlReviewer.DataTextField = "RE_name";
                    ddlReviewer.DataValueField = "RE_number";
                    ddlReviewer.DataBind();
                    ddlReviewer.Items.Insert(0, "-- SELECT --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        protected void LoadAllSEs_Ho()
        {
            DataTable dtSEDetails;
            DataTable dtDeatils;
            try
            {
                dtSEDetails = new DataTable();
                objMDP.userid = Convert.ToString(Session["UserId"]);
                objMDP.ts_project_flag = rdbtsProject.Checked ? "Y" : "N";
                dtSEDetails = objMDP.GetAllHO(objMDP);
                if (dtSEDetails != null)
                {
                    dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("SE_number", typeof(string));
                    dtDeatils.Columns.Add("SE_name", typeof(string));


                    for (int i = 0; i < dtSEDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtSEDetails.Rows[i].ItemArray[0].ToString(), dtSEDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtSEDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlSalesEngName.DataSource = dtDeatils;
                    ddlSalesEngName.DataTextField = "SE_name";
                    ddlSalesEngName.DataValueField = "SE_number";
                    ddlSalesEngName.DataBind();
                    ddlSalesEngName.Items.Insert(0, "-- SELECT --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllSEs()
        {
            DataTable dtSEDetails;
            DataTable dtDeatils;
            try
            {
                dtSEDetails = new DataTable();
                dtSEDetails = objMDP.GetAllSE();
                if (dtSEDetails != null)
                {
                    dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("SE_number", typeof(string));
                    dtDeatils.Columns.Add("SE_name", typeof(string));


                    for (int i = 0; i < dtSEDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtSEDetails.Rows[i].ItemArray[0].ToString(), dtSEDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtSEDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlSalesEngName.DataSource = dtDeatils;
                    ddlSalesEngName.DataTextField = "SE_name";
                    ddlSalesEngName.DataValueField = "SE_number";
                    ddlSalesEngName.DataBind();
                    ddlSalesEngName.Items.Insert(0, "-- SELECT --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

        }

        protected void LoadAllReviewer()
        {
            DataTable dtREDetails;
            DataTable dtDeatils;
            try
            {
                dtREDetails = new DataTable();
                if (dtREDetails != null)
                {
                    dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("RE_number", typeof(string));
                    dtDeatils.Columns.Add("RE_name", typeof(string));
                    for (int i = 0; i < dtREDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtREDetails.Rows[i].ItemArray[0].ToString(), dtREDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtREDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlReviewer.DataSource = dtDeatils;
                    ddlReviewer.DataTextField = "RE_name";
                    ddlReviewer.DataValueField = "RE_number";
                    ddlReviewer.DataBind();
                    ddlReviewer.Items.Insert(0, "-- SELECT --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

        }

        protected void LoadAllHOIndustry()
        {
            DataTable dtHoDetails;
            try
            {
                dtHoDetails = new DataTable();
                dtHoDetails = objMDP.GetAllHOIndustry();
                if (dtHoDetails != null)
                {
                    ddlHOIndustry.DataSource = dtHoDetails;
                    ddlHOIndustry.DataTextField = "IndustryDetail";
                    ddlHOIndustry.DataValueField = "ID";
                    ddlHOIndustry.DataBind();
                    ddlHOIndustry.Items.Insert(0, "-- SELECT --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllMachineMake()
        {
            DataTable dtHoDetails;
            try
            {
                dtHoDetails = new DataTable();
                dtHoDetails = objMDP.GetAllMachineMake();
                if (dtHoDetails != null)
                {
                    ddlMachineMake.DataSource = dtHoDetails;
                    ddlMachineMake.DataTextField = "MachineMake";
                    ddlMachineMake.DataValueField = "ID";
                    ddlMachineMake.DataBind();
                    ddlMachineMake.Items.Insert(0, "-- SELECT --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllMachineType()
        {
            DataTable dtHoDetails;
            try
            {
                dtHoDetails = new DataTable();
                dtHoDetails = objMDP.GetAllMachineType();
                if (dtHoDetails != null)
                {
                    ddlMachineType.DataSource = dtHoDetails;
                    ddlMachineType.DataTextField = "MachineType";
                    ddlMachineType.DataValueField = "ID";
                    ddlMachineType.DataBind();
                    ddlMachineType.Items.Insert(0, "-- SELECT --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        protected void LoadOwnerbyCustomer(string CustomerNumber)
        {
            DataTable dtSEDetails;
            DataTable dtDeatils;
            try
            {
                dtSEDetails = new DataTable();
                string branchcode = Session["BranchCode"].ToString();
                dtSEDetails = objMDP.LoadOwnerbyCustomer(CustomerNumber);
                if (dtSEDetails != null)
                {
                    dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("SE_number", typeof(string));
                    dtDeatils.Columns.Add("SE_name", typeof(string));


                    for (int i = 0; i < dtSEDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtSEDetails.Rows[i].ItemArray[0].ToString(), dtSEDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtSEDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlSalesEngName.DataSource = dtDeatils;
                    ddlSalesEngName.DataTextField = "SE_name";
                    ddlSalesEngName.DataValueField = "SE_number";
                    ddlSalesEngName.DataBind();
                    ddlSalesEngName.Items.Insert(0, "-- SELECT --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadReviewerbyCustomer(string CustomerNumber)
        {
            DataTable dtREDetails;
            DataTable dtDeatils;
            try
            {
                dtREDetails = new DataTable();
                string project = string.Empty;

                if (new_project.Checked)
                {
                    dtREDetails = objMDP.LoadReviewerbyCustomer(CustomerNumber, rdbNormalProject.Checked ? "Y" : "N");
                }
                else
                {
                    project = ddlTitle.SelectedValue.ToString();
                    dtREDetails = objMDP.LoadReviewerbyCustomer_SE(CustomerNumber, project);
                }

                if (dtREDetails != null)
                {
                    dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("RE_number", typeof(string));
                    dtDeatils.Columns.Add("RE_name", typeof(string));


                    for (int i = 0; i < dtREDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtREDetails.Rows[i].ItemArray[0].ToString(), dtREDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtREDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlReviewer.DataSource = dtDeatils;
                    ddlReviewer.DataTextField = "RE_name";
                    ddlReviewer.DataValueField = "RE_number";
                    ddlReviewer.DataBind();
                    ddlReviewer.Items.Insert(0, "-- SELECT --");
                    if (rdbtsProject.Checked)
                    {
                        objMDP.Owner = Convert.ToString(Session["UserId"]);
                        string reviewer = objMDP.getReviewer_TSProject(objMDP);
                        ddlReviewer.SelectedValue = reviewer;
                        ddlReviewer.Enabled = true;
                    }
                    else
                    {
                        ddlReviewer.Enabled = true;
                    }

                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadEscalatorbyCustomer(string CustomerNumber)
        {
            DataTable dtHoDetails;
            DataTable dtDeatils;
            try
            {
                dtHoDetails = new DataTable();
                //anamika
                string project = string.Empty;
                if (new_project.Checked)
                {
                    dtHoDetails = objMDP.LoadEscalatorbyCustomer(CustomerNumber, rdbNormalProject.Checked ? "Y" : "N");
                }
                else
                {
                    project = ddlTitle.SelectedValue.ToString();
                    dtHoDetails = objMDP.LoadEscalatorbyCustomer_SE(CustomerNumber, project);
                }
                if (dtHoDetails != null)
                {
                    dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("HO_number", typeof(string));
                    dtDeatils.Columns.Add("HO_name", typeof(string));


                    for (int i = 0; i < dtHoDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtHoDetails.Rows[i].ItemArray[0].ToString(), dtHoDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtHoDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlEscalate.DataSource = dtDeatils;
                    ddlEscalate.DataTextField = "HO_name";
                    ddlEscalate.DataValueField = "HO_number";
                    ddlEscalate.DataBind();
                    ddlEscalate.Items.Insert(0, "-- SELECT --");
                    if (rdbtsProject.Checked)
                    {
                        objMDP.Owner = Convert.ToString(Session["UserId"]);
                        string escalateTo = objMDP.getEscalateTo_TSProject(objMDP);
                        ddlEscalate.SelectedValue = escalateTo;
                        ddlEscalate.Enabled = true;
                    }
                    else
                    {
                        ddlEscalate.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected string GetCustomerSalesEngineer(string CustomerNumber)
        {
            string engineeer_id = objMDP.GetCustomerSalesEngineer(CustomerNumber);
            return engineeer_id;


        }


        #endregion

        /// <summary>
        /// Method to load the default state of Main page controls.
        /// </summary>
        public void LoadDefault()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                string salesEngName = Session["UserName"].ToString();
                string branchCode = Session["BranchCode"].ToString();
                ExistingProduct.Text = "";

                txtTaegutecCutter.Text = "";
                txtTaegutecInsert.Text = "";
                txtCompCutter.Text = "";
                txtCompInsert.Text = "";
                Component.Text = "";
                //  CompetitionSpec.Text = "";
                TSProject_Default();
                TargetDate.Text = "";
                Target_Date.Value = "";
                OverAllPotential.Text = "";
                Potential.Text = "";
                Business.Text = "";
                txtCustomerClass.Text = "";

                txtOrderTargetCY.Text = "";
                txtOrderTargetNY.Text = "";
                if (roleId == "SE")
                {
                    ddlSalesEngName.SelectedValue = strUserId;
                    ddlReviewer.SelectedValue = objMDP.LoadBranchManagerDetails(branchCode);
                }
                else
                {
                    //anamika
                    //ddlSalesEngName.SelectedValue = "-- SELECT --";
                    ddlReviewer.SelectedValue = "-- SELECT --";
                }
                ExistingProduct.Enabled = true;
                txtTaegutecCutter.Enabled = true;
                txtTaegutecInsert.Enabled = true;
                txtCompCutter.Enabled = true;
                txtCompInsert.Enabled = true;
                Component.Enabled = true;
                //CompetitionSpec.Enabled = true;
                Stages.Enabled = true;
                //TargetDate.Enabled = true;
                OverAllPotential.Enabled = true;
                Potential.Enabled = true;
                Business.Enabled = true;
                ddlProjectType.Enabled = true;    //anantha
                //anamika
                ddlProductGroup.Enabled = true;
                ddlSalesEngName.Enabled = false;
                ddlIndustry.Enabled = true;
                ddlEscalate.Enabled = true;
                ddlReviewer.Enabled = true;
                txtOrderTargetCY.Enabled = true;
                txtOrderTargetNY.Enabled = true;

                ddlEscalate.SelectedValue = "-- SELECT --";
                ddlIndustry.SelectedValue = "-- SELECT --";
                ddlProjectType.SelectedValue = "-- SELECT --";
                ddlProductGroup.SelectedValue = "-- SELECT --";
                existing_project.Checked = true;
                existing_project.Enabled = true;
                new_project.Enabled = true;
                txtTitle.Text = "";
                ddlTitle.Enabled = true;
                txtTitle.Enabled = true;
                //anamika
                //existing_project.Checked = true;
                new_project.Checked = false;
                DataTable dtDeatils = new DataTable();
                dtDeatils.Columns.Add("project_number", typeof(string));
                dtDeatils.Columns.Add("project_title", typeof(string));
                ddlTitle.DataSource = dtDeatils;
                ddlTitle.DataTextField = "project_title";
                ddlTitle.DataValueField = "project_number";
                ddlTitle.DataBind();
                ddlTitle.Items.Insert(0, "-- SELECT PROJECT --");
                ddlTitle.SelectedValue = "-- SELECT PROJECT --";
                //btnFreeze.Enabled = true;
                ddlIndustry.Enabled = true;
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Method to load the default state of All tab controls.
        /// </summary>
        public void LoadDefaultTab()
        {
            string strUserId = string.Empty;
            string roleId = string.Empty;
            string salesEngName = string.Empty;
            string branchCode = string.Empty;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                strUserId = Session["UserId"].ToString();
                roleId = Session["RoleId"].ToString();
                salesEngName = Session["UserName"].ToString();
                branchCode = Session["BranchCode"].ToString();

                tab1txtGoal.Text = "";
                tab1txtRemarks_1.Text = "";
                tab1Target.Text = "";
                tab1Completion.Text = "";
                tab1txtGoal.Enabled = true;
                tab1txtRemarks_1.Enabled = true;
                tab1Target.Enabled = true;


                tab2txtGoal.Text = "";
                tab2txtRemarks_1.Text = "";
                tab2Target.Text = "";
                tab2Completion.Text = "";
                tab2txtGoal.Enabled = true;
                tab2txtRemarks_1.Enabled = true;
                tab2Target.Enabled = true;
                tab2ProjectNumber.Enabled = true;
                tab2ProjectNumber.Text = "";

                tab3txtGoal.Text = "";
                tab3txtRemarks_1.Text = "";
                tab3Target.Text = "";
                tab3Completion.Text = "";
                tab3txtGoal.Enabled = true;
                tab3txtRemarks_1.Enabled = true;
                tab3Target.Enabled = true;

                tab4txtGoal.Text = "";
                tab4txtRemarks_1.Text = "";
                tab4Target.Text = "";
                tab4Completion.Text = "";
                tab4txtGoal.Enabled = true;
                tab4txtRemarks_1.Enabled = true;
                tab4Target.Enabled = true;

                tab5txtGoal.Text = "";
                tab5txtRemarks_1.Text = "";
                tab5Target.Text = "";
                tab5Completion.Text = "";
                tab5txtGoal.Enabled = true;
                tab5txtRemarks_1.Enabled = true;
                tab5Target.Enabled = true;

                tab6txtGoal.Text = "";
                tab6txtRemarks_1.Text = "";
                tab6Target.Text = "";
                tab6Completion.Text = "";
                tab6txtGoal.Enabled = true;
                tab6txtRemarks_1.Enabled = true;
                tab6Target.Enabled = true;

                tab7txtGoal.Text = "";
                tab7txtRemarks_1.Text = "";
                tab7Target.Text = "";
                tab7Completion.Text = "";
                tab7txtGoal.Enabled = true;
                tab7txtRemarks_1.Enabled = true;
                tab7Target.Enabled = true;

                tab8txtGoal.Text = "";
                tab8txtRemarks_1.Text = "";
                tab8Target.Text = "";
                tab8Completion.Text = "";
                tab8txtGoal.Enabled = true;
                tab8txtRemarks_1.Enabled = true;
                tab8Target.Enabled = true;

                tab9txtGoal.Text = "";
                tab9txtRemarks_1.Text = "";
                tab9Target.Text = "";
                tab9Completion.Text = "";
                tab9txtGoal.Enabled = true;
                tab9txtRemarks_1.Enabled = true;
                tab9Target.Enabled = true;

                tab10txtGoal.Text = "";
                tab10txtRemarks_1.Text = "";
                tab10Target.Text = "";
                tab10Completion.Text = "";
                tab10txtGoal.Enabled = true;
                tab10txtRemarks_1.Enabled = true;
                tab10Target.Enabled = true;

                for (int i = 1; i <= 10; i++)
                {
                    for (int j = 2; j < j + 1; j++)
                    {
                        TextBox txtRemark = (TextBox)tab.FindControl("tab" + i + "txtRemarks_" + j);
                        HtmlContainerControl cntrl = (HtmlContainerControl)tab.FindControl("tab" + i + "Remarks");
                        if (txtRemark != null)
                        {
                            cntrl.Controls.Remove(txtRemark);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        #region Send Mail To Owner And Reviewer And EscalatedTo
        public void SendmailToOwner(string assigned_Engineer_Id, string project_title, string project_number, string customer_number, string potential)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            EmailDetails email;
            try
            {


                DestinationEmail = objMDP.GetEmail_Id(assigned_Engineer_Id);
                email = new EmailDetails();

                email.toMailId = DestinationEmail; //Destination Recipient e-mail address.
                email.ccMailId = "anamika@knstek.com; teja@knstek.com";

                email.subject = "Sales-Budget & Performance Monitoring Project Creation";//Subject for your request
                email.body = "A project is created with following details <br/><br/>Project Name: " + project_title + "(" + project_number + ")" + "<br/><br/>Customer Name: " + customer_number + "<br/><br/> Project Potential:" + potential + "<br/><br/>" + "From" + "<br/>" + "Sales-Budget & Performance Monitoring";
                SendGridMail(email).Wait();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                objFunc.LogError(ex);
            }
            finally
            {
                email = new EmailDetails();
            }
        }

        public void SendmailToSalesEngineer(string project_title, string project_number, string customerName, string customer_number, string potential, string salesengId)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            string DestinationEmail1 = string.Empty;
            EmailDetails email;
            SmtpClient smtpc;
            try
            {


                DestinationEmail = objMDP.GetEmail_Id(salesengId);
                DestinationEmail1 = objMDP.GetBMEmail_Id(customer_number);
                objFunc.LogMessage("CustomerNum:" + customer_number);
                objFunc.LogMessage(DestinationEmail);
                objFunc.LogMessage(DestinationEmail1);
                email = new EmailDetails();

                email.toMailId = DestinationEmail; //Destination Recipient e-mail address.
                email.ccMailId = DestinationEmail1 + "; anamika@knstek.com; teja@knstek.com";
                email.subject = "Sales-Budget & Performance Monitoring Project Creation";//Subject for your request
                email.body = "A project is created with following details <br/><br/>Project Name: " + project_title + "(" + project_number + ")" + "<br/><br/>Customer Name: " + customerName + "<br/><br/> Project Potential:" + potential + "<br/><br/>" + "From" + "<br/>" + "Sales-Budget & Performance Monitoring";
                SendGridMail(email).Wait();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                objFunc.LogError(ex);
            }
            finally
            {
                email = new EmailDetails();
            }
        }

        public void SendmailToReviewer(string assigned_Engineer_Id, string project_title, string project_number, string customer_name, string customer_num, string distNum, string potential)
        {
            LoginAuthentication logobj = new LoginAuthentication();
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;

            var reader = ConfigurationManager.AppSettings["MailApprovalLink"].ToString();
            string htmlstr = reader;
            string reviewerid = logobj.Encrypt(assigned_Engineer_Id);
            string custno = logobj.Encrypt(distNum);
            string projectno = logobj.Encrypt(project_number);
            string URl = reader + "?id=" + reviewerid + "&ProjectNo=" + projectno + "&CustomerNo=" + custno + "";


            EmailDetails email;
            try
            {
                DestinationEmail = objMDP.GetEmail_Id(assigned_Engineer_Id);
                email = new EmailDetails();

                email.toMailId = DestinationEmail; //Destination Recipient e-mail address.
                email.ccMailId = "anamika@knstek.com; teja@knstek.com";
                email.subject = "Sales-Budget & Performance Monitoring Project Creation";//Subject for your request
               email.body = "A project is created with following details <br/><br/>Project Name: " + project_title + "(" + project_number + ")" + "<br/><br/>Customer Name: " + customer_name + "<br/><br/> Project Potential:" + potential + "<br/><br/> Please <a href=" + URl + ">Click Here</a> to approve the project.<br/><br/>" + "From" + "<br/>" + "Sales-Budget & Performance Monitoring";
                SendGridMail(email).Wait();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                objFunc.LogError(ex);
            }
            finally
            {
                email = new EmailDetails();
            }
        }

        public void SendmailToEscalateTo(string assigned_Engineer_Id, string project_title, string project_number, string customer_number, string potential)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            EmailDetails email;
            try
            {
                DestinationEmail = objMDP.GetEmail_Id(assigned_Engineer_Id);
                email = new EmailDetails();

                email.toMailId = DestinationEmail; //Destination Recipient e-mail address.
                email.ccMailId = "anamika@knstek.com; teja@knstek.com";
                email.subject = "Sales-Budget & Performance Monitoring Project Creation";//Subject for your request
                email.body = "A project is created with following details <br/><br/>Project Name: " + project_title + "(" + project_number + ")" + "<br/><br/>Customer Name: " + customer_number + "<br/><br/> Project Potential:" + potential + "<br/><br/> Please approve the project." + "<br/><br/>" + "From" + "<br/>" + "Sales-Budget & Performance Monitoring";
                SendGridMail(email).Wait();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                objFunc.LogError(ex);
            }
            finally
            {
                email = new EmailDetails();
            }
        }

        public void SendmailAtApproval(string project_owner, string reviewer, string escalteTo, string project_title, string project_number, string customer_number, string salesengId)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            EmailDetails email;
            try
            {
                email = new EmailDetails();

                if (rdbtsProject.Checked == true)
                {
                    email.toMailId = objMDP.GetEmail_Id(project_owner); //Destination Recipient e-mail address.
                    email.ccMailId = objMDP.GetEmail_Id(reviewer) + ";" + objMDP.GetEmail_Id(escalteTo) + ";" + objMDP.GetBMEmail_Id(customer_number) + ";" + objMDP.GetEmail_Id(salesengId) + ";anamika@knstek.com; teja@knstek.com";
                    objFunc.LogMessage("techSupport");
                }
                else
                {
                    email.toMailId = objMDP.GetEmail_Id(project_owner); //Destination Recipient e-mail address.
                    email.ccMailId = objMDP.GetEmail_Id(reviewer) + ";" + objMDP.GetEmail_Id(escalteTo) + ";" + objMDP.GetBMEmail_Id(customer_number) + ";" + objMDP.GetEmail_Id(salesengId) + ";anamika@knstek.com; teja@knstek.com";
                    objFunc.LogMessage("marketing");
                }
                email.subject = "Sales-Budget & Performance Monitoring Project Creation";//Subject for your request
                email.body = "A project is <b>approved</b> with following details <br/><br/>Project Name: " + project_title + "(" + project_number + ")" + "<br/><br/>Customer Name: " + customer_number + "<br/><br/> " + "<br/><br/>" + "From" + "<br/>" + "Sales-Budget & Performance Monitoring";
                SendGridMail(email).Wait();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                objFunc.LogError(ex);
            }
            finally
            {
                email = new EmailDetails();
            }
        }

        public void SendmailAtRejection(string project_owner, string reviewer, string escalteTo, string project_title, string project_number, string customer_number, string customer_Name, string remarks)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            EmailDetails email;
            try
            {
                email = new EmailDetails();
                email.toMailId = objMDP.GetEmail_Id(project_owner); //Destination Recipient e-mail address.
                email.ccMailId = objMDP.GetEmail_Id(reviewer) + ";" + objMDP.GetEmail_Id(escalteTo);
                email.subject = "Sales-Budget & Performance Monitoring Project Creation";//Subject for your request
                //email.Body = "A project is <b>rejected</b> with following details <br/><br/>Rejection Remarks: " + remarks + "< br/><br/>Project Name: " + project_title + "(" + project_number + ")" + "<br/><br/>Customer Name: " + customer_number + "<br/><br/>" + "<br/><br/>" + "From" + "<br/>" + "Sales-Budget & Performance Monitoring";
                email.body = "A project is <b>rejected</b> with following details <br/><br/>Rejection Remarks: " + remarks + "<br/><br/>Project Name: " + project_title + "(" + project_number + ")" + "<br/><br/>Customer Name: " + customer_Name + "(" + customer_number + ")" + "<br/><br/>" + "<br/><br/>" + "From" + "<br/>" + "Sales-Budget & Performance Monitoring";
                SendGridMail(email).Wait();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                objFunc.LogError(ex);
            }
            finally
            {
                email = new EmailDetails();
            }
        }


        public void SendmailAtInitiateProject(string project_title, string project_number, string customerName, string salesengId, string project_owner, string reviewer, string escalteTo)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            string DestinationEmail1 = string.Empty;
            EmailDetails email;
            try
            {


                DestinationEmail = objMDP.GetEmail_Id(salesengId);
                DestinationEmail1 = objMDP.GetEmail_Id(salesengId);
                objFunc.LogMessage(DestinationEmail);
                objFunc.LogMessage(salesengId);
                objFunc.LogMessage(project_owner);
                objFunc.LogMessage(reviewer);
                objFunc.LogMessage(escalteTo);
                objFunc.LogMessage(objMDP.GetEmail_Id(project_owner));
                //objFunc.LogMessage(objMDP.GetEmail_Id(reviewer));
                //objFunc.LogMessage(objMDP.GetEmail_Id(escalteTo));
                email = new EmailDetails();

                //email.toMailId = DestinationEmail; //Destination Recipient e-mail address.
                //email.ccMailId =  " anamika@knstek.com; teja@knstek.com; monika@knstek.com";
                //email.ccMailId = DestinationEmail1+";anamika@knstek.com; teja@knstek.com;monika@knstek.com";

                if (reviewer != "-- SELECT --" && escalteTo != "-- SELECT --") 
                {
                    email.toMailId = DestinationEmail; //Destination Recipient e-mail address.
                    email.ccMailId = objMDP.GetEmail_Id(project_owner) + ";" + objMDP.GetEmail_Id(reviewer) + ";" + objMDP.GetEmail_Id(escalteTo) + "; anamika@knstek.com; teja@knstek.com;monika@knstek.com";
                }
                else if (reviewer == "-- SELECT --" && escalteTo != "-- SELECT --")
                {
                    email.toMailId = DestinationEmail; //Destination Recipient e-mail address.
                    email.ccMailId = objMDP.GetEmail_Id(project_owner) + ";" + objMDP.GetEmail_Id(escalteTo) + "; anamika@knstek.com; teja@knstek.com; monika@knstek.com";
                }
               else if (reviewer != "-- SELECT --" && escalteTo == "-- SELECT --")
                {
                    email.toMailId = DestinationEmail; //Destination Recipient e-mail address.
                    email.ccMailId = objMDP.GetEmail_Id(project_owner) + ";" + objMDP.GetEmail_Id(reviewer) + "; anamika@knstek.com; teja@knstek.com; monika@knstek.com";
                }
                else
                {
                    objFunc.LogMessage(DestinationEmail);
                    email.toMailId = DestinationEmail; //Destination Recipient e-mail address.
                    email.ccMailId = objMDP.GetEmail_Id(project_owner) + "; anamika@knstek.com; teja@knstek.com; monika@knstek.com";
                    objFunc.LogMessage(email.toMailId);
                    objFunc.LogMessage(email.ccMailId);
                }
                email.subject = "Sales-Budget & Performance Monitoring Project Initiated";//Subject for your request
                email.body = "A project is initiated with following details, Please Login to the Application to Create Project <br/><br/>Project Name: " + project_title + "(" + project_number + ")" + "<br/><br/>Customer Name: " + customerName + "<br/><br/>" + "From" + "<br/>" + "Sales-Budget & Performance Monitoring";
                SendGridMail(email).Wait();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                objFunc.LogError(ex);
            }
            finally
            {
                email = new EmailDetails();
            }
        }
        #endregion




        public static async Task SendGridMail(EmailDetails email)
        {
            try
            {
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"].ToString());

                var sendGridMessage = new SendGridMessage()
                {
                    From = new EmailAddress("ttilalerts@taegutec-india.com", "TT Alerts"),
                    Subject = email.subject,
                    HtmlContent = email.body
                };
                //sendGridMessage.AddTo(new EmailAddress(email.toMailId));
                List<EmailAddress> objList = new List<EmailAddress>();
                if (!string.IsNullOrEmpty(email.toMailId))
                {
                    if (email.toMailId.Contains(";"))
                    {
                        List<string> names = email.toMailId.Split(';').ToList<string>();
                        for (int i = 0; i < names.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                                //email.CC.Add(names[i]);
                                objList.Add(new EmailAddress(names[i]));
                        }
                    }
                    else
                    {
                        objList.Add(new EmailAddress(email.toMailId));
                    }
                    sendGridMessage.AddTos(objList);
                }
                objList = new List<EmailAddress>();
                if (!string.IsNullOrEmpty(email.ccMailId))
                {
                    if (email.ccMailId.Contains(";"))
                    {
                        List<string> names = email.ccMailId.Split(';').ToList<string>();
                        for (int i = 0; i < names.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                                //email.CC.Add(names[i]);
                                objList.Add(new EmailAddress(names[i]));
                        }
                    }
                    else
                    {
                        objList.Add(new EmailAddress(email.ccMailId));
                    }
                    sendGridMessage.AddCcs(objList);
                }
                if (!string.IsNullOrEmpty(email.attachment))
                {
                    //byte[] byteData = Encoding.ASCII.GetBytes(File.ReadAllText(email.attachment));
                    byte[] fileContent = null;
                    System.IO.FileStream fs = new System.IO.FileStream(email.attachment, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(fs);
                    long byteLength = new System.IO.FileInfo(email.attachment).Length;
                    fileContent = binaryReader.ReadBytes((Int32)byteLength);
                    fs.Close();
                    fs.Dispose();
                    binaryReader.Close();

                    sendGridMessage.AddAttachment(new SendGrid.Helpers.Mail.Attachment()
                    {
                        Content = Convert.ToBase64String(fileContent),
                        Filename = Path.GetFileName(email.attachment),
                        Type = "application/pdf",
                        Disposition = "attachment"
                    });
                }
                var response = await sendGridClient.SendEmailAsync(sendGridMessage).ConfigureAwait(false);

            }
            catch (Exception ex)
            {
                //StaticErrorLog(ex);
            }
        }


        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (Session["RoleId"].ToString() == "HO")
                {
                    if (rdBtnTaegutec.Checked)
                    {
                        Session["cter"] = "TTA";
                        cter = "TTA";

                    }
                    if (rdBtnDuraCab.Checked)
                    {
                        Session["cter"] = "DUR";
                        cter = "DUR";
                    }
                    LoadCustomerDetails_HO();
                    LoadAllDistributors();
                    if (Session["Customer_Number"] != null && Session["Project_Number"] != null)
                    {
                        Enable_NewCustomer(null, null);
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
                }

                clear();
                TSProject_Changes();
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

        }
        protected string getCustomerClassCode(string fullClassName)
        {
            string code = "";
            try
            {
                if (!string.IsNullOrEmpty(fullClassName))
                {
                    if (fullClassName.Contains("KEY FOCUSED CUSTOMER"))
                    {
                        code = "KA";
                    }
                    else if (fullClassName.Contains("‘A’ CLASS CUSTOMER"))
                    {
                        code = "KF";
                    }
                    else if (fullClassName.Contains("OTHERS"))
                    {
                        code = "OT";
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return code;
        }

        /// <summary>
        /// Author : Anamika
        /// Date : 
        /// Description :
        /// </summary>
        protected void EnableControls()
        {
            try
            {
                ddlDistributorList.Enabled = true;
                ddlCustomerName.Enabled = true;
                ddlCustomerNo.Enabled = true;
                ddlDistibutor.Enabled = true;
                ExistingProduct.Enabled = true;
                txtTaegutecCutter.Enabled = true;
                txtTaegutecInsert.Enabled = true;
                txtCompCutter.Enabled = true;
                txtCompInsert.Enabled = true;
                ddlProjectType.Enabled = true;
                ddlProductGroup.Enabled = true;
                Component.Enabled = true;
                //  CompetitionSpec.Enabled = true;
           
                //Target_flag.Value = "True";
                //TargetDate.Enabled = true;
                OverAllPotential.Enabled = true;
                Business.Enabled = true;
                Potential.Enabled = true;
                if (rdbtsProject.Checked)
                {
                    ddlReviewer.Enabled = true;
                    ddlEscalate.Enabled = true;
                    Stages.Enabled = false;
                }
                else
                {
                    ddlReviewer.Enabled = true;
                    ddlEscalate.Enabled = true;
                    Stages.Enabled = true;
                }
                ddlTitle.Enabled = true;
                txtTitle.Enabled = true;
                txtOrderTargetCY.Enabled = true;
                txtOrderTargetNY.Enabled = true;
                ddlCustomerforDistributor.Enabled = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "enablecontrols();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : 
        /// Description :
        /// </summary>
        protected void DisableControls()
        {
            try
            {
                ddlDistributorList.Enabled = false;
                ddlProjectType.Enabled = false;
                ddlProductGroup.Enabled = false;
                ddlCustomerName.Enabled = false;
                ddlCustomerNo.Enabled = false;
                ddlIndustry.Enabled = false;
                ddlDistibutor.Enabled = false;
                ExistingProduct.Enabled = false;
                txtTaegutecCutter.Enabled = false;
                txtTaegutecInsert.Enabled = false;
                txtCompCutter.Enabled = false;
                txtCompInsert.Enabled = false;
                Component.Enabled = false;
                //  CompetitionSpec.Enabled = false;
                Stages.Enabled = false;
                TargetDate.Enabled = false;
                //Target_flag.Value = "False";
                OverAllPotential.Enabled = false;
                Business.Enabled = false;
                Potential.Enabled = false;
                ddlSalesEngName.Enabled = false;
                ddlReviewer.Enabled = false;
                ddlEscalate.Enabled = false;
                //btnFreeze.Enabled = false;
                ddlTitle.Enabled = false;
                txtTitle.Enabled = false;
                txtOrderTargetCY.Enabled = false;
                txtOrderTargetNY.Enabled = false;
                ddlCustomerforDistributor.Enabled = false;
                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "disablecontrols();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "javascript", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {

                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : 
        /// Description :
        /// </summary>
        protected void clear()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddlCustomerNo.Enabled = true;
                ddlCustomerName.Enabled = true;
                ddlDistibutor.SelectedValue = "--SELECT--";
                ddlDistibutor.Enabled = true;
                ddlDistributorList.Enabled = true;
                LoadDefault();
                LoadDefaultTab();
                new_project.Checked = true;
                existing_project.Checked = true;
                tab1btnSubmit.Visible = false;
                tab2btnSubmit.Visible = false;
                tab3btnSubmit.Visible = false;
                tab4btnSubmit.Visible = false;
                tab5btnSubmit.Visible = false;
                tab6btnSubmit.Visible = false;
                tab7btnSubmit.Visible = false;
                tab8btnSubmit.Visible = false;
                tab9btnSubmit.Visible = false;
                tab10btnSubmit.Visible = false;
                tab1btnSave.Enabled = true;
                tab1btnSave.Visible = true;
                tab2btnSave.Enabled = true;
                tab2btnSave.Visible = true;
                tab3btnSave.Enabled = true;
                tab3btnSave.Visible = true;
                tab4btnSave.Enabled = true;
                tab4btnSave.Visible = true;
                tab5btnSave.Enabled = true;
                tab5btnSave.Visible = true;
                tab6btnSave.Enabled = true;
                tab6btnSave.Visible = true;
                tab7btnSave.Enabled = true;
                tab7btnSave.Visible = true;
                tab8btnSave.Enabled = true;
                tab8btnSave.Visible = true;
                tab9btnSave.Enabled = true;
                tab9btnSave.Visible = true;
                tab10btnSave.Enabled = true;
                tab10btnSave.Visible = true;
                tab1btnSaveApprove.Visible = false;
                tab2btnSaveApprove.Visible = false;
                tab3btnSaveApprove.Visible = false;
                tab4btnSaveApprove.Visible = false;
                tab5btnSaveApprove.Visible = false;
                tab6btnSaveApprove.Visible = false;
                tab7btnSaveApprove.Visible = false;
                tab8btnSaveApprove.Visible = false;
                tab9btnSaveApprove.Visible = false;
                tab10btnSaveApprove.Visible = false;
                ddlCustomerforDistributor.Enabled = true;
                ddlIndustry.Enabled = false;
                direct_customer.Checked = true;
                direct_customer.Enabled = true;
                channel_partner.Enabled = true;
                ddlDistributorList.SelectedValue = "-- SELECT CHANNEL PARTNER --";

                tab1_lblApprove.Visible = false;
                tab1_txtApprove.Visible = false;
                tab1_btnApprove.Visible = false;
                tab1_btnReject.Visible = false;

                tab2_lblApprove.Visible = false;
                tab2_txtApprove.Visible = false;
                tab2_btnApprove.Visible = false;
                tab2_btnReject.Visible = false;

                tab3_lblApprove.Visible = false;
                tab3_txtApprove.Visible = false;
                tab3_btnApprove.Visible = false;
                tab3_btnReject.Visible = false;

                tab4_lblApprove.Visible = false;
                tab4_txtApprove.Visible = false;
                tab4_btnApprove.Visible = false;
                tab4_btnReject.Visible = false;

                tab5_lblApprove.Visible = false;
                tab5_txtApprove.Visible = false;
                tab5_btnApprove.Visible = false;
                tab5_btnReject.Visible = false;

                tab6_lblApprove.Visible = false;
                tab6_txtApprove.Visible = false;
                tab6_btnApprove.Visible = false;
                tab6_btnReject.Visible = false;

                tab7_lblApprove.Visible = false;
                tab7_txtApprove.Visible = false;
                tab7_btnApprove.Visible = false;
                tab7_btnReject.Visible = false;

                tab8_lblApprove.Visible = false;
                tab8_txtApprove.Visible = false;
                tab8_btnApprove.Visible = false;
                tab8_btnReject.Visible = false;

                tab9_lblApprove.Visible = false;
                tab9_txtApprove.Visible = false;
                tab9_btnApprove.Visible = false;
                tab9_btnReject.Visible = false;

                tab10_lblApprove.Visible = false;
                tab10_txtApprove.Visible = false;
                tab10_btnApprove.Visible = false;
                tab10_btnReject.Visible = false;

                tab1_FileList.Visible = false;
                tab2_FileList.Visible = false;
                tab3_FileList.Visible = false;
                tab4_FileList.Visible = false;
                tab5_FileList.Visible = false;
                tab6_FileList.Visible = false;
                tab7_FileList.Visible = false;
                tab8_FileList.Visible = false;
                tab9_FileList.Visible = false;
                tab10_FileList.Visible = false;

                tab1lblSuccess.Visible = false;
                tab2lblSuccess.Visible = false;
                tab3lblSuccess.Visible = false;
                tab4lblSuccess.Visible = false;
                tab5lblSuccess.Visible = false;
                tab6lblSuccess.Visible = false;
                tab7lblSuccess.Visible = false;
                tab8lblSuccess.Visible = false;
                tab9lblSuccess.Visible = false;
                tab10lblSuccess.Visible = false;
                //tab1doc.Visible = false;
                FileUpload1.Enabled = false;
                tab1btnUpload.Enabled = false;

                ddlCustomerName.SelectedValue = "-- SELECT CUSTOMER --";
                ddlSalesEngName.SelectedValue = "-- SELECT --";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Oct 21,2016
        /// Desc
        /// </summary>
        /// <param name="file_project_num"></param>
        private void stagesvisibility(string file_project_num)
        {
            DataTable dtstage = new DataTable();
            try
            {
                if (string.IsNullOrEmpty(file_project_num))
                {
                    dtstage = objMDP.getCurrentStage(ddlTitle.SelectedValue);
                }
                else if (file_project_num == "-- SELECT PROJECT --" || file_project_num == "-- NO PROJECT --")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "disable();", true);
                }
                else
                {
                    dtstage = objMDP.getCurrentStage(file_project_num);
                }
                if (dtstage.Rows.Count > 0)
                {
                    int stage = 0;
                    stage = Convert.ToInt32(dtstage.Rows[0]["CurrentStage"]);
                    if (stage == 1)
                    {
                        DisableGoal(stage);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "enabletab2();", true);
                    }
                    else if (stage == 2)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "enabletab3();", true);
                    else if (stage == 3)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "enabletab4();", true);
                    else if (stage == 4)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "enabletab5();", true);
                    else if (stage == 5)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "enabletab6();", true);
                    else if (stage == 6)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "enabletab7();", true);
                    else if (stage == 7)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "enabletab8();", true);
                    else if (stage == 8)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "enabletab9();", true);
                    else if (stage == 9)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "enabletab10();", true);
                    else if (stage == -1)
                    { }
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "disable();", true);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        private void DisableGoal(int stage)
        {
            if (rdbtsProject.Checked)
            {
                if (stage == 1)
                    tab1txtGoal.Enabled = false;
            }
        }


        /// <summary>
        /// Anantha load all project types
        /// </summary>
        protected void LoadAllProjectTypes()
        {
            DataTable dtREDetails;
            DataTable dtDeatils;
            try
            {
                dtREDetails = new DataTable();
                dtREDetails = objMDP.GetAllProjectType();
                if (dtREDetails != null)
                {
                    dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("Project_Type_Desc", typeof(string));

                    for (int i = 0; i < dtREDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtREDetails.Rows[i].ItemArray[0].ToString());
                    }
                    ddlProjectType.DataSource = dtDeatils;
                    ddlProjectType.DataTextField = "Project_Type_Desc";
                    ddlProjectType.DataBind();
                    ddlProjectType.Items.Insert(0, "-- SELECT --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Date : 
        /// </summary>
        protected void LoadProductGroups()
        {
            DataTable dtProdGrp;
            try
            {
                dtProdGrp = new DataTable();
                dtProdGrp = objMDP.GetProductGroups();
                if (dtProdGrp != null)
                {

                    ddlProductGroup.DataSource = dtProdGrp;
                    ddlProductGroup.DataTextField = "item_family_name";
                    ddlProductGroup.DataValueField = "item_family_id";
                    ddlProductGroup.DataBind();
                    ddlProductGroup.Items.Insert(0, "-- SELECT --");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        #endregion

        protected void rdbtsProject_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                clear();
                TSProject_Default();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "PageReloadScripts();", true);


            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        private void TSProject_Changes()
        {
            if (rdbtsProject.Checked)
            {
                Stages.SelectedValue = "8";
                Stages.Enabled = false;
                tab1txtGoal.Text = "Enquiry";
                tab2txtGoal.Text = "Tech. proposal submission";
                tab3txtGoal.Text = "Technical discussion";
                tab4txtGoal.Text = "Commercial Quote";
                tab5txtGoal.Text = "Trial Order";
                tab6txtGoal.Text = "Execution";
                tab7txtGoal.Text = "1st  Order";
                tab8txtGoal.Text = "1st Repeat Order";
                tab1txtGoal.Enabled = false;
                tab2txtGoal.Enabled = false;
                tab3txtGoal.Enabled = false;
                tab4txtGoal.Enabled = false;
                tab5txtGoal.Enabled = false;
                tab6txtGoal.Enabled = false;
                tab7txtGoal.Enabled = false;
                tab8txtGoal.Enabled = false;

                divCY.Visible = true;
                divNY.Visible = true;
                divCustPot.Visible = false;
                ddlSalesEngName.Enabled = false;
                ddlReviewer.Enabled = true;
                ddlEscalate.Enabled = true;
            }
            else
            {

                divCY.Visible = false;
                divNY.Visible = false;
                divCustPot.Visible = true;
            }
        }

        private void TSProject_Default()
        {
            if (rdbtsProject.Checked)
            {
                Stages.SelectedValue = "8";
                Stages.Enabled = false;
                tab1txtGoal.Text = "Enquiry";
                tab2txtGoal.Text = "Tech. proposal submission";
                tab3txtGoal.Text = "Technical discussion";
                tab4txtGoal.Text = "Commercial Quote";
                tab5txtGoal.Text = "Trial Order";
                tab6txtGoal.Text = "Execution";
                tab7txtGoal.Text = "1st  Order";
                tab8txtGoal.Text = "1st Repeat Order";
                tab1txtGoal.Enabled = false;
                tab2txtGoal.Enabled = false;
                tab3txtGoal.Enabled = false;
                tab4txtGoal.Enabled = false;
                tab5txtGoal.Enabled = false;
                tab6txtGoal.Enabled = false;
                tab7txtGoal.Enabled = false;
                tab8txtGoal.Enabled = false;
                divCY.Visible = true;
                divNY.Visible = true;
                divCustPot.Visible = false;
                ddlSalesEngName.Enabled = false;
                ddlReviewer.Enabled = true;
                ddlEscalate.Enabled = true;
            }
            else
            {
                Stages.SelectedValue = "4";
                Stages.Enabled = true;
                tab1txtGoal.Text = "";
                tab2txtGoal.Text = "";
                tab3txtGoal.Text = "";
                tab4txtGoal.Text = "";
                tab5txtGoal.Text = "";
                tab6txtGoal.Text = "";
                tab7txtGoal.Text = "";
                tab8txtGoal.Text = "";
                tab1txtGoal.Enabled = true;
                tab2txtGoal.Enabled = true;
                tab3txtGoal.Enabled = true;
                tab4txtGoal.Enabled = true;
                tab5txtGoal.Enabled = true;
                tab6txtGoal.Enabled = true;
                tab7txtGoal.Enabled = true;
                tab8txtGoal.Enabled = true;
                divCY.Visible = false;
                divNY.Visible = false;
                divCustPot.Visible = true;
            }
        }
    }
}