﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AllowDevices.aspx.cs" MasterPageFile="~/Admin.Master" Inherits="TaegutecSalesBudget.AllowDevices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript">


        function submit() {


            bindGridView();
            //$('#form1').submit();
        }



        function bindGridView() {
            debugger;
            var head_content = $('#MainContent_grdallowdevices tr:first').html();
            $('#MainContent_grdallowdevices').prepend('<thead></thead>')
            $('#MainContent_grdallowdevices thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdallowdevices tbody tr:first').hide();
            $('#MainContent_grdallowdevices').DataTable(
               {
                   "info": false,
               });
        }


        $(document).ready(function () {
           
            });
      


        function edit() {
            $("#MainContent_txtusernamename").attr("readonly", true);
            $("#MainContent_txtmaild").attr("readonly", true);
            $("#MainContent_txtroleid").attr("readonly", true);
            validatedefaultdevice1();
            validatedefaultdevice2();
            validaterequestdevice();
            validatetxt();
            debugger;
            dclg = $("#Edituser").dialog(
              {
                  resizable: false,
                  draggable: true,
                  modal: true,
                  title: "User Device Info",
                  width: "450",
                  height: "250",

                  open: function () {
                      //  $(".ui-widget-overlay").css("display", 'none');
                      $(this).dialog('open');

                  },

                  close: function () {
                      $(this).dialog('close');
                      //  $("#overlay").removeClass("ui-widget-overlay");
                      //    return true;
                  }

              });

            dclg.parent().appendTo(jQuery("form:first"));

            //  $('MainContent_grdallowdevices_imgeditAction_0').bind("onclick", function (e) {

        }

        function validatetxt() {
            debugger;
            var nodaysformat = /^\d*$/
           
            if ($('#MainContent_txtrequestdevice').val().length > 0) {
                if (nodaysformat.test($('#MainContent_txtimeperiod').val()))
            {
                    $('#MainContent_lblmsgtimeperiods').hide();
                    $('#MainContent_lbltimeperiods').hide();
                    $('#MainContent_lblchckreq').hide();
                    $('#MainContent_lblradiobuttonapprove').hide()
                }
                else {
                    $('#MainContent_lblmsgtimeperiods').show();
                }
                rdbtnApproveChange();
            }
            else {
                $('#MainContent_lblchckreq').show();
            }
            if ($('#MainContent_txtimeperiod').val().length == 0 || $('#MainContent_txtimeperiod').val()=="0") {
                $('#MainContent_lblchckreq').hide();
            }
        }
        function ValidateIPaddres() {
            var flag = 0;
            var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

            if (ipformat.test($('#MainContent_defaultdevice1').val()) && $('#MainContent_defaultdevice1').val().length > 0 || $('#MainContent_defaultdevice1').val().length == 0) {
              
            }
            else {
               
                flag++;
            }
            
            if (ipformat.test($('#MainContent_defaultdevice2').val()) && $('#MainContent_defaultdevice2').val().length > 0 || $('#MainContent_defaultdevice2').val().length == 0) {

                $('#MainContent_lbldefaultdevice2').hide();
            }
            else {
                $('#MainContent_lbldefaultdevice2').show();
                flag++;
            }
            if (ipformat.test($('#MainContent_txtrequestdevice').val()) && $('#MainContent_txtrequestdevice').val().length > 0 || $('#MainContent_txtrequestdevice').val().length == 0) {
                $('#MainContent_lblrequestdevice').hide();
            }
            else {

                $('#MainContent_lblrequestdevice').show();
                flag++;
            }
            if ($('#MainContent_defaultdevice1').val() == $('#MainContent_defaultdevice2').val() || $('#MainContent_defaultdevice1').val() == $('#MainContent_txtrequestdevice').val() || $('#MainContent_defaultdevice2').val() == $('#MainContent_txtrequestdevice').val()) {
                $('#MainContent_lbldupdefault1').show();
                flag++;
            }
            else {
                $('#MainContent_lbldupdefault1').hide();
            }
            if ($('#MainContent_txtimeperiod').val() =="0") {

                $('#MainContent_lblradiobuttonapprove').show()
                flag++
            }
        

            if (flag > 0) {
                return false;
            }
          
            else {
                if ($('#MainContent_rdbtnapprove').prop('checked')) {
                    if ($('#MainContent_txtimeperiod').val() > "0") {
                     
                        $('#MainContent_lblradiobuttonapprove').hide()
                        return true;
                    }
                    else {
                        $('#MainContent_lblradiobuttonapprove').show()
                        return false;
                    }
                }
                else {
                    return true;
                }

            }
            //validatedefaultdevice1();
            //validatedefaultdevice2();
            //validaterequestdevice();
            //validatetxt();
           // debugger;
            //if ($('#MainContent_lbldefaultdevice1').is(':visible') || $('#MainContent_lbldefaultdevice2').is(':visible') || $('#MainContent_lblmsgtimeperiods').is(':visible') || $('#MainContent_lbltimeperiods').is(':visible') || $('#MainContent_lblrequestdevice').is(':visible') || $('#MainContent_lblchckreq').is(':visible')) {
               
            //    return false;
            //}
            //else {
            //    return true;
            //}
        }
        function validatedefaultdevice1() {


           

            debugger;
            var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

            if ($('#MainContent_defaultdevice1').val() == $('#MainContent_defaultdevice2').val() || $('#MainContent_defaultdevice1').val() == $('#MainContent_txtrequestdevice').val() || $('#MainContent_defaultdevice2').val() == $('#MainContent_txtrequestdevice').val()) {
                $('#MainContent_lbldupreq').show();
            }
            else {
                $('#MainContent_lbldupreq').hide();
            }


            if (ipformat.test($('#MainContent_defaultdevice1').val()) && $('#MainContent_defaultdevice1').val().length > 0 || $('#MainContent_defaultdevice1').val().length == 0)
{
                $('#MainContent_lbldefaultdevice1').hide();
            }
            else {
                $('#MainContent_lbldefaultdevice1').show();
            }
            if ($('#MainContent_defaultdevice1').val() == $('#MainContent_defaultdevice2').val()) {
                $('#MainContent_ lbldupdefault1').show();
            }
        }
        function validatedefaultdevice2() {
            debugger;
            var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
            if ($('#MainContent_defaultdevice1').val() == $('#MainContent_defaultdevice2').val() || $('#MainContent_defaultdevice1').val() == $('#MainContent_txtrequestdevice').val() || $('#MainContent_defaultdevice2').val() == $('#MainContent_txtrequestdevice').val()) {
                $('#MainContent_lbldupreq').show();
            }
            else {
                $('#MainContent_lbldupreq').hide();
            }

            if (ipformat.test($('#MainContent_defaultdevice2').val()) && $('#MainContent_defaultdevice2').val().length > 0 || $('#MainContent_defaultdevice2').val().length == 0)
            {

                $('#MainContent_lbldefaultdevice2').hide();
            }
            else {
                $('#MainContent_lbldefaultdevice2').show();
            }
        }
        function validaterequestdevice()
        {
            debugger;

            var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

            if ($('#MainContent_defaultdevice1').val() == $('#MainContent_defaultdevice2').val() || $('#MainContent_defaultdevice1').val() == $('#MainContent_txtrequestdevice').val() || $('#MainContent_defaultdevice2').val() == $('#MainContent_txtrequestdevice').val()) {
                $('#MainContent_lbldupreq').show();
            }
            else {
                $('#MainContent_lbldupreq').hide();
            }

            if (ipformat.test($('#MainContent_txtrequestdevice').val()) && $('#MainContent_txtrequestdevice').val().length > 0 || $('#MainContent_txtrequestdevice').val().length == 0)
            {
                $('#MainContent_lblrequestdevice').hide();
            }
            else {
                $('#MainContent_lblrequestdevice').show();
            }
        }
        function rdbtnApproveChange() {
            var nodaysformat = /^\d*$/
            debugger;
            if ($('#MainContent_rdbtnapprove').prop('checked')) {
                if ($('#MainContent_txtrequestdevice').val().length > 0) {
                    if ($('#MainContent_txtimeperiod').val().length > 0) {
                        $('#MainContent_lbltimeperiods').hide()


                        if(nodaysformat.test($('#MainContent_txtimeperiod').val()))
                            {
                       
                            $('#MainContent_lblmsgtimeperiods').hide();
                            // $("#Edituser").dialog('close')
                            //  return true;
                        }
                        else {
                            $('#MainContent_lblmsgtimeperiods').show()
                            //  return false;
                        }
                    }
                    else {
                        $('#MainContent_lbltimeperiods').show();
                        $('#MainContent_lblmsgtimeperiods').show()
                        //  return false;
                    }
                }
                else {
                    if ($('#MainContent_txtimeperiod').val() == "0") {
                        $('#MainContent_lbltimeperiods').hide();
                        $('#MainContent_lblmsgtimeperiods').hide();
                        $('#MainContent_lblchckreq').hide();
                    }
                    else {
                        $('#MainContent_lblchckreq').show();
                    }
                }
            }
           
        }
        function rdbtnCancelChange() {
            if ($('#MainContent_rdbtncancel').prop('checked')) {
                $('#MainContent_txtimeperiod').val("0");
                $('#MainContent_lbltimeperiods').hide()
                $('#MainContent_lblmsgtimeperiods').hide()
                $('#MainContent_lblchckreq').hide();
            }
        }
    </script>


    <style>
        .HeadergridAll {
            background: #ebeef5;
            color: #fff;
            font-weight: 600;
            text-align: center;
            border-color: #ebeef5;
            font-size: 12px;
        }

        .distance {
            padding-top: 10px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="Ul2" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Admin</a>
                    </li>

                    <li>Allow Devices </li>
                    <div>
                        <ul>
                            <li class="title_bedcrum" style="list-style: none;">ALLOW DEVICES</li>
                        </ul>
                    </div>
                </ul>
            </div>
            <%--  <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" style="margin-left: 46%;" />
            </div>--%>

            <div>                
                <asp:GridView ID="grdallowdevices" CssClass="display compact" Width="80%" Style="text-align: center;" runat="server" AutoGenerateColumns="false" OnRowCommand="EditUser_Click">
                    <Columns>
                        <asp:TemplateField HeaderText="USER NAME" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label ID="lblCPNumber" runat="server" Text='<%# Eval("EngineerName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MAIL ID" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label ID="lblmailid" runat="server" Text='<%# Eval("EmailId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ROLE" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label ID="lblrole" runat="server" Text='<%# Eval("RoleId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DEFAULT DEVICE1" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label ID="lbldd1" runat="server" Text='<%# Eval("defaultDevice1") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DEFAULT DEVICE2" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label ID="lbldd2" runat="server" Text='<%# Eval("defaultDevice2") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="REQUEST" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label ID="txtrequest" runat="server" Text='<%# Eval("RequestedDevice") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EDIT" HeaderStyle-CssClass="HeadergridAll">
                            <ItemTemplate>
                                <asp:ImageButton runat="server" ToolTip="Edit" Width="20px" CssClass="distance" ImageUrl="~/images/edit_small.png" ID="imgeditAction" CommandArgument='<%# Bind("EmailId") %>' />
                                <%--<asp:ImageButton runat="server" ToolTip="Delete" Width="20px" CssClass="distance" ImageUrl="~/images/delete_icon.png" ID="imgdelAction"  />--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="STATUS" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label ID="txtstatus" runat="server" Text='<%# Eval("statusofdevice") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

            <div id="Edituser" style="width: 100%; display: none; text-align: center !important; height: auto !important">
                <div class="form-horizontal row-border1" id="Div2">

                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            User Name 

                        </label>

                        <div class="col-md-7">
                            <asp:TextBox runat="server" ID="txtusernamename"  CssClass="form-control required" ></asp:TextBox>
                            <asp:Label ID="lbltxtusername" runat="server" ForeColor="Red" Style="float: right" name="edit"></asp:Label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Email Id
                                          
                        </label>

                        <div class="col-md-7">
                            <asp:TextBox runat="server" MaxLength="50" ID="txtmaild" CssClass="form-control required email" placeholder="Username@taegutec-india.com"></asp:TextBox>

                            <%--<asp:Image ID="Editmailexists" ImageUrl="~/images/x.png" runat="server" Style="display: none" ImageAlign="Right" />
                            <asp:Image ID="Editmailnotexits" ImageUrl="~/images/tick.png" runat="server" Style="display: none" ImageAlign="Right" />--%>
                            <asp:Label ID="lbltxteditempmail" runat="server" ForeColor="Red" Style="float: right" name="edit"></asp:Label>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Role Id   
                        </label>
                        <div class="col-md-7">
                            <asp:TextBox runat="server" ID="txtroleid" MaxLength="12" CssClass="form-control required email"></asp:TextBox>
                            <asp:Label ID="lbltxteditroleid" runat="server" ForeColor="Red" Style="float: right" name="edit"></asp:Label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Default Device1

                        </label>

                        <div class="col-md-7">
                            <asp:TextBox runat="server" ID="defaultdevice1" CssClass="form-control required" name="txtdvc1" onkeyup="validatedefaultdevice1()"></asp:TextBox>
                                <asp:Label ID="lbldefaultdevice1" Text="please enter valid ip address in correct format" runat="server" style="display:none;color:red" name="edit"></asp:Label>
                           <%-- <asp:Label ID="lbldupdefault1" Text="there is already a device with same ip " runat="server" style="display:none;color:red" name="edit"></asp:Label>--%>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Default Device2

                        </label>

                        <div class="col-md-7">
                            <asp:TextBox runat="server" ID="defaultdevice2" CssClass="form-control required" onkeyup="validatedefaultdevice2()"></asp:TextBox>
                          <asp:Label ID="lbldefaultdevice2" Text="please enter valid ip address in correct format" runat="server" style="display:none;color:red" name="edit"></asp:Label>
                           <%-- <asp:Label ID="lbldupdefault2" Text="there is already a device with same ip " runat="server" style="display:none;color:red" name="edit"></asp:Label>--%>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Request Device

                        </label>

                        <div class="col-md-7">
                            <asp:TextBox runat="server" ID="txtrequestdevice" CssClass="form-control required" onkeyup="validaterequestdevice()"></asp:TextBox>
                             <asp:Label ID="lblrequestdevice" Text="please enter valid ip address in correct format" runat="server" style="display:none;color:red" name="edit"></asp:Label>
                          <asp:Label ID="lbldupreq" Text="Please check their may be a device with same ip in Default device1 or default device2 or requested device " runat="server" style="display:none;color:red" name="edit"></asp:Label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-6 control-label">
                            Status

                        </label>

                        <div class="col-md-3">
                            Approve
                            <asp:RadioButton ID="rdbtnapprove" runat="server" GroupName="approval" onchange="rdbtnApproveChange()"/>
                           
                        </div>

                        <div class="col-md-3">
                            cancel
                            <asp:RadioButton ID="rdbtncancel" runat="server" GroupName="approval"  onchange="rdbtnCancelChange()"/>
                        </div>

                    </div>


                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Time Period In Days

                        </label>

                        <div class="col-md-7">
                            <asp:TextBox runat="server" ID="txtimeperiod" CssClass="form-control required" onkeyup="return validatetxt()"></asp:TextBox>                        
                             <%--<asp:Label ID="lbltimeperiods" Text="Please enter the time period in Days" runat="server" style="display:none;color:red" name="edit"></asp:Label>--%>
                             <asp:Label ID="lblmsgtimeperiods" Text="Please enter the time period in numbers" runat="server" style="display:none;color:red" name="edit"></asp:Label>
                              <asp:Label ID="lblchckreq" Text="There must be some requested device for entering time period" runat="server" style="display:none;color:red" name="edit"></asp:Label>  
                            <asp:Label ID="lblradiobuttonapprove" Text="please enter time period gretar than 0" runat="server" style="display:none;color:red" name="edit"></asp:Label>                        
                        </div>
                    </div>

                    <div class="form-actions" style="margin-bottom: -23px !important">
                        <asp:Button runat="server" ID="Cancel" Text="Cancel" CssClass="btn blue pull-right" OnClientClick="Cancel();" />
                        <asp:Button runat="server" ID="EditAdd" Text="Save" CssClass="btn blue pull-right" OnClick="EditAdd_Click" OnClientClick="return ValidateIPaddres();" />
                        
                    </div>
                     
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
