﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        #region Global Declaration
        CommonFunctions comFun = new CommonFunctions();
        List<string> cssListFamilyHead = new List<string>();
        Budget objBudget = new Budget();
        List<string> cssList = new List<string>();
        List<string> lsType = new List<string>();
        AdminConfiguration objConfig = new AdminConfiguration();
        public static int BudgetYear;
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] != null)
            {
                BudgetYear = objConfig.getBudgetYear();

                if (!IsPostBack)
                {
                    string strUserId = Session["UserId"].ToString();
                    string roleId = Session["RoleId"].ToString();
                    lsType.Add("I");
                    lsType.Add("P");
                    lsType.Add("S");
                    lsType.Add("T");
                    lsType.Add("X");
                    lsType.Add("");
                    LoadCSS();
                    LoadCustomerDetails(strUserId, roleId);
                    LoadInitGrid();
                }
            }
            else
            { Response.Redirect("Login.aspx?Login"); }
        }

        protected void grdviewAllValues_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblBMFlag = e.Row.FindControl("lbl_BM_flag") as Label;
                var txtQty = e.Row.FindControl("txt_ActualQuantity_sales_qty_year_P") as TextBox;
                var cbQty = e.Row.FindControl("checkbox_ActualQuantity_sales_qty_year_P") as CheckBox;
                var txtvalue = e.Row.FindControl("txt_PricePerUnit_sales_value_year_P") as TextBox;

                if (lblBMFlag != null)
                    if (lblBMFlag.Text == "APPROVE")
                    {
                        if (txtQty != null && cbQty != null)
                        {
                            txtQty.Enabled = false; cbQty.Enabled = false;
                        }

                        if (txtvalue != null)
                        {
                            txtvalue.Enabled = false;
                        }
                    }
                    else
                    {

                        if (txtQty != null && cbQty != null)
                        {
                            if (cbQty.Checked) { txtQty.BackColor = System.Drawing.Color.Red; txtQty.Font.Bold = true; txtQty.ForeColor = System.Drawing.Color.White; } //txtQty.Enabled = false;
                        }
                    }

            }

        }

        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserName"] == null || Session["RoleId"] == null)
            { Response.Redirect("Login.aspx?Login"); return; }

            string CustomerNumber = ddlCustomerList.SelectedItem.Value;
            if (CustomerNumber == "SELECT CUSTOMER")
            {
                lbl_MSG.Text = "";
                grdviewAllValues.DataSource = null;
                grdviewAllValues.DataBind();
                ddlCustomerNumber.Text = "";
                panelButtons.Visible = false;
                return;
            }
            LoadCSS();
            ddlCustomerNumber.Text = CustomerNumber;
            //DataTable dtMain = loadBudget(CustomerNumber);
            DataTable dtMain = comFun.LoadBudget(CustomerNumber);
            dtMain = comFun.GrandTotalforEntry(dtMain);
            dtMain = comFun.AddInsertPerTools(dtMain);
            ViewState["CurrentTableBudget"] = dtMain;
            //Main Grid
            lbl_MSG.Text = "";
            lblResult.Text = "";
            grdviewAllValues.DataSource = null;
            grdviewAllValues.DataBind();
            if (dtMain != null)
            {
                grdviewAllValues.DataSource = dtMain;
                grdviewAllValues.DataBind();

                bindgridColor();
                bindgridCssforValueAdding();

                panelButtons.Visible = true;
                //get customer budget status (Save/approve/review)
                string role = Session["RoleId"].ToString();
                string status = objBudget.getCustomerBudgetStatus(CustomerNumber, role);
                hdnCustomerBudgetStatus.Value = status;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                if (status == "APPROVE")
                {
                    string cust = ddlCustomerList.SelectedItem.Text;
                    lbl_MSG.Text = "Selected Customer : " + cust + " was Approved  ";
                }
                else if (status == "REVIEW")
                {
                    string cust = ddlCustomerList.SelectedItem.Text;
                    lbl_MSG.Text = "Selected Customer : " + cust + " is under Review ";
                }
            }
            else
            {
                grdviewAllValues.DataSource = null;
                grdviewAllValues.DataBind();
                lblResult.Text = "Refresh the page and try again";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string ProductName = "";
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx?Login"); return; }
            string strUserRole = Session["RoleId"].ToString();
            //objBudget.Review_flag = "SAVE";
            objBudget.salesengineer_id = Session["UserId"].ToString();
            objBudget.customer_number = ddlCustomerList.SelectedItem.Value.ToString();
            objBudget.budget_year = objConfig.getBudgetYear_B();

            if (strUserRole == "BM")
            {
                objBudget.status_branch_manager = "SAVE";
            }
            else if (strUserRole == "HO")
            {
                objBudget.status_ho = "SAVE";
            }
            if (grdviewAllValues.Rows.Count != 0)
            {
                string errorMsg = "";
                int Count = 0;
                int budgetId;
                foreach (GridViewRow row in grdviewAllValues.Rows)
                {
                    var lblitemCode = row.FindControl("lblItemCode") as Label;
                    var txtQuantityP = row.FindControl("txt_ActualQuantity_sales_qty_year_P") as TextBox;
                    var txtValueP = row.FindControl("lbl_ActualValue_sales_value_year_P_New") as Label;

                    var lblBudget = row.FindControl("lbl_BudgetId") as Label;
                    var lblFlag_Item = row.FindControl("lbl_SumFlag") as Label;

                    if (lblitemCode.Text != "")
                    {
                        if (lblFlag_Item.Text == "products" && (txtQuantityP.Text != "" && txtQuantityP.Text != "0"))
                        {
                            decimal valueB = 0;
                            if (txtQuantityP.Text != "") { objBudget.estimated_qty = txtQuantityP.Text.ToString() == "" ? 0 : Convert.ToInt32(txtQuantityP.Text.ToString()); }
                            var txtEstimatedRate = row.FindControl("txt_PricePerUnit_sales_value_year_P") as TextBox;
                            if (txtEstimatedRate.Text != "")
                            {
                                objBudget.estimated_rate = txtEstimatedRate.Text.ToString() == "" ? 0 : Convert.ToInt32(Convert.ToDecimal(txtEstimatedRate.Text.ToString()));
                                valueB = (objBudget.estimated_qty * objBudget.estimated_rate);
                            }
                            var reviewQuantity = row.FindControl("checkbox_ActualQuantity_sales_qty_year_P") as CheckBox;
                            var reviewValue = row.FindControl("checkbox_ActualValue_sales_value_year_P") as CheckBox;

                            string reviewQ = "";
                            if (reviewQuantity != null) { reviewQ = reviewQuantity.Checked == true ? "Q" : ""; }
                            string reviewV = "";
                            if (reviewValue != null) { reviewV = reviewValue.Checked == true ? "V" : ""; }
                            objBudget.Review_flag = reviewQ + reviewV;
                            objBudget.item_code = lblitemCode.Text.ToString();
                            objBudget.estimated_value = valueB;
                            if (objBudget.budget_id == 0)
                            {
                                objBudget.budget_id = lblBudget.Text.ToString() == "" ? 0 : Convert.ToInt32(lblBudget.Text.ToString());
                            }
                            errorMsg = objBudget.insertProBudget(objBudget);
                            Count++;

                        }
                        else if (lblFlag_Item.Text == "products" && (txtQuantityP.Text == "" || txtQuantityP.Text == "0"))
                        {
                            string budId = lblBudget.Text;
                            string itemcode = lblitemCode.Text;
                            if (budId != "" && !Regex.IsMatch(budId, @"^[a-zA-Z]+$"))
                            {
                                int id = Convert.ToInt32(budId);
                                delete_BudgetNumber(id, itemcode);
                                Count++;
                            }


                        }
                    }

                }
                if (Count != 0)
                {

                    lblResult.Text = "Saved successfully";//errorMsg;
                    budgetId = objBudget.budget_id;
                }
                else { lblResult.Text = "All Products of quantity and values are zero/null. We didn’t updated Customer " + ddlCustomerList.SelectedItem.Text + Convert.ToString(System.DateTime.Now.Year + 1) + "  Budget "; }

            }
            //string CustomerNumber = ddlCustomerList.SelectedItem.Value;       
            //string role = Session["RoleId"].ToString();
            //string status = objBudget.getCustomerBudgetStatus(CustomerNumber, role);
            //hdnCustomerBudgetStatus.Value = status;
            //if (status == "APPROVE")
            //{
            //    string cust = ddlCustomerList.SelectedItem.Text;
            //    lbl_MSG.Text = "Selected Customer : " + cust + "(" + CustomerNumber + ")" + " submitted for higher Level approval ";
            //}
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "DivPopUpOpen()", true);
        }

        protected void btnReview_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx?Login"); return; }
            string strUserRole = Session["RoleId"].ToString();

            objBudget.salesengineer_id = Session["UserId"].ToString();
            objBudget.customer_number = ddlCustomerList.SelectedItem.Value.ToString();
            objBudget.budget_year = objConfig.getBudgetYear_B();

            if (strUserRole == "BM")
            {
                objBudget.status_sales_engineer = "REVIEW";
                objBudget.status_branch_manager = "REVIEW";
            }
            else if (strUserRole == "HO")
            {
                objBudget.status_ho = "REVIEW";
            }

            if (grdviewAllValues.Rows.Count != 0)
            {
                string errorMsg = "";
                int Count = 0;
                int budgetId;
                foreach (GridViewRow row in grdviewAllValues.Rows)
                {
                    var lblitemCode = row.FindControl("lblItemCode") as Label;
                    var txtQuantityP = row.FindControl("txt_ActualQuantity_sales_qty_year_P") as TextBox;
                    var txtValueP = row.FindControl("lbl_ActualValue_sales_value_year_P_New") as Label;

                    var lblBudget = row.FindControl("lbl_BudgetId") as Label;
                    var lblFlag_Item = row.FindControl("lbl_SumFlag") as Label;

                    if (lblitemCode.Text != "")
                    {
                        if (lblFlag_Item.Text == "products" && (txtQuantityP.Text != "" && txtQuantityP.Text != "0"))
                        {
                            decimal valueB = 0;
                            if (txtQuantityP.Text != "") { objBudget.estimated_qty = txtQuantityP.Text.ToString() == "" ? 0 : Convert.ToInt32(txtQuantityP.Text.ToString()); }
                            var txtEstimatedRate = row.FindControl("txt_PricePerUnit_sales_value_year_P") as TextBox;
                            if (txtEstimatedRate.Text != "")
                            {
                                objBudget.estimated_rate = txtEstimatedRate.Text.ToString() == "" ? 0 : Convert.ToInt32(Convert.ToDecimal(txtEstimatedRate.Text.ToString()));
                                valueB = (objBudget.estimated_qty * objBudget.estimated_rate);
                            }
                            var reviewQuantity = row.FindControl("checkbox_ActualQuantity_sales_qty_year_P") as CheckBox;
                            var reviewValue = row.FindControl("checkbox_ActualValue_sales_value_year_P") as CheckBox;

                            string reviewQ = "";
                            if (reviewQuantity != null) { reviewQ = reviewQuantity.Checked == true ? "Q" : ""; }
                            string reviewV = "";
                            if (reviewValue != null) { reviewV = reviewValue.Checked == true ? "V" : ""; }
                            objBudget.Review_flag = reviewQ + reviewV;
                            objBudget.item_code = lblitemCode.Text.ToString();
                            objBudget.estimated_value = valueB;
                            if (objBudget.budget_id == 0)
                            {
                                objBudget.budget_id = lblBudget.Text.ToString() == "" ? 0 : Convert.ToInt32(lblBudget.Text.ToString());
                            }
                            errorMsg = objBudget.insertProBudget(objBudget);
                            Count++;

                        }
                        else if (lblFlag_Item.Text == "products" && (txtQuantityP.Text == "" || txtQuantityP.Text == "0"))
                        {
                            string budId = lblBudget.Text;
                            string itemcode = lblitemCode.Text;
                            if (budId != "" && !Regex.IsMatch(budId, @"^[a-zA-Z]+$"))
                            {
                                int id = Convert.ToInt32(budId);
                                delete_BudgetNumber(id, itemcode);
                                Count++;
                            }


                        }
                    }

                }
                if (Count != 0)
                {
                    lblResult.Text = "Review sent successfully ";//errorMsg;
                    budgetId = objBudget.budget_id;
                }
                else { lblResult.Text = "All Products of quantity and values are zero/null. We didn’t updated Customer " + ddlCustomerList.SelectedItem.Text + Convert.ToString(System.DateTime.Now.Year + 1) + "  Budget "; }

            }

            string CustomerNumber = ddlCustomerList.SelectedItem.Value;

            string role = Session["RoleId"].ToString();
            string status = objBudget.getCustomerBudgetStatus(CustomerNumber, role);
            hdnCustomerBudgetStatus.Value = status;
            if (status == "APPROVE")
            {
                string cust = ddlCustomerList.SelectedItem.Text;
                lbl_MSG.Text = "Selected Customer : " + cust + " was Approved  ";
            }
            else if (status == "REVIEW")
            {
                string cust = ddlCustomerList.SelectedItem.Text;
                lbl_MSG.Text = "Selected Customer : " + cust + " is under Review ";
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "DivPopUpOpen()", true);



        }

        protected void btnSubmitforHigherApproval_Click(object sender, EventArgs e)
        {

            if (Session["UserName"] == null) { Response.Redirect("Login.aspx?Login"); return; }
            string strUserRole = Session["RoleId"].ToString();
            objBudget.Review_flag = "";
            objBudget.salesengineer_id = Session["UserId"].ToString();
            objBudget.customer_number = ddlCustomerList.SelectedItem.Value.ToString();
            objBudget.budget_year = objConfig.getBudgetYear_B();

            if (strUserRole == "BM" || strUserRole=="TM")
            {
                objBudget.status_sales_engineer = "APPROVE";
                objBudget.status_branch_manager = "APPROVE";
            }
            if (grdviewAllValues.Rows.Count != 0)
            {
                string errorMsg = "";
                int Count = 0;
                int budgetId;
                foreach (GridViewRow row in grdviewAllValues.Rows)
                {
                    var lblitemCode = row.FindControl("lblItemCode") as Label;
                    var txtQuantityP = row.FindControl("txt_ActualQuantity_sales_qty_year_P") as TextBox;
                    var txtValueP = row.FindControl("lbl_ActualValue_sales_value_year_P_New") as Label;

                    var lblBudget = row.FindControl("lbl_BudgetId") as Label;
                    var lblFlag_Item = row.FindControl("lbl_SumFlag") as Label;

                    if (lblitemCode.Text != "")
                    {
                        if (lblFlag_Item.Text == "products" && (txtQuantityP.Text != "" && txtQuantityP.Text != "0"))
                        {
                            decimal valueB = 0;
                            if (txtQuantityP.Text != "") { objBudget.estimated_qty = txtQuantityP.Text.ToString() == "" ? 0 : Convert.ToInt32(txtQuantityP.Text.ToString()); }
                            var txtEstimatedRate = row.FindControl("txt_PricePerUnit_sales_value_year_P") as TextBox;
                            if (txtEstimatedRate.Text != "")
                            {
                                objBudget.estimated_rate = txtEstimatedRate.Text.ToString() == "" ? 0 : Convert.ToInt32(Convert.ToDecimal(txtEstimatedRate.Text.ToString()));
                                valueB = (objBudget.estimated_qty * objBudget.estimated_rate);
                            }
                            var reviewQuantity = row.FindControl("checkbox_ActualQuantity_sales_qty_year_P") as CheckBox;
                            var reviewValue = row.FindControl("checkbox_ActualValue_sales_value_year_P") as CheckBox;

                            string reviewQ = "";
                            if (reviewQuantity != null) { reviewQ = reviewQuantity.Checked == true ? "Q" : ""; }
                            string reviewV = "";
                            if (reviewValue != null) { reviewV = reviewValue.Checked == true ? "V" : ""; }
                            objBudget.Review_flag = reviewQ + reviewV;
                            objBudget.item_code = lblitemCode.Text.ToString();
                            objBudget.estimated_value = valueB;
                            if (objBudget.budget_id == 0)
                            {
                                objBudget.budget_id = lblBudget.Text.ToString() == "" ? 0 : Convert.ToInt32(lblBudget.Text.ToString());
                            }
                            errorMsg = objBudget.insertProBudget(objBudget);
                            Count++;

                        }
                        else if (lblFlag_Item.Text == "products" && (txtQuantityP.Text == "" || txtQuantityP.Text == "0"))
                        {
                            string budId = lblBudget.Text;
                            string itemcode = lblitemCode.Text;
                            if (budId != "" && !Regex.IsMatch(budId, @"^[a-zA-Z]+$"))
                            {
                                int id = Convert.ToInt32(budId);
                                delete_BudgetNumber(id, itemcode);
                                Count++;
                            }

                        }
                    }

                }
                if (Count != 0)
                {

                    lblResult.Text = "Thanks for submitting Budget";
                    lbl_MSG.Text = "";

                }
                else { lblResult.Text = "All Products of quantity and values are zero/null. We didn’t submitted, Customer " + ddlCustomerList.SelectedItem.Text + Convert.ToString(System.DateTime.Now.Year + 1) + "  Budget "; }

            }
            string CustomerNumber = ddlCustomerList.SelectedItem.Value;

            string role = Session["RoleId"].ToString();
            string status = objBudget.getCustomerBudgetStatus(CustomerNumber, role);
            hdnCustomerBudgetStatus.Value = status;
            if (status == "APPROVE")
            {
                string cust = ddlCustomerList.SelectedItem.Text;
                lbl_MSG.Text = "Selected Customer : " + cust + " was Approved  ";
            }
            else if (status == "REVIEW")
            {
                string cust = ddlCustomerList.SelectedItem.Text;
                lbl_MSG.Text = "Selected Customer : " + cust + " is under Review ";
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "DivPopUpOpen();", true);


        }



        protected void btnLogoff_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] != null)
            {

                Session["UserName"] = null;
                Session["LoginMailId"] = null;
                Session["RoleId"] = null;
                Session["UserId"] = null;
                Session.RemoveAll();
                Response.Redirect("Login.aspx");
            }
        }

        protected void btnNewCustomer_Click(object sender, EventArgs e)
        {
            lbl_MSG.Text = "";
            ddlCustomerList.SelectedIndex = 0;
            ddlCustomerNumber.SelectedIndex = 0;
            grdviewAllValues.DataSource = null;
            grdviewAllValues.DataBind();
            panelButtons.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            LoadGrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }

        protected void delete_BudgetNumber(int BudgetId, string itemcode)
        {
            string result = objBudget.DeleteBudgetNumbers(BudgetId, itemcode);
        }

        
        #endregion

        #region Methods
        protected void LoadInitGrid()
        {
            if (Session["UserName"] == null || Session["RoleId"] == null)
            { Response.Redirect("Login.aspx?Login"); return; }
            if (Session["CustomerNumber"] != null)
            {
                LoadCSS();
                string CustomerNumber = Session["CustomerNumber"].ToString();
                ddlCustomerNumber.Text = CustomerNumber;
                //DataTable dtMain = loadBudget(CustomerNumber);
                DataTable dtMain = comFun.LoadBudget(CustomerNumber);
                dtMain = comFun.GrandTotalforEntry(dtMain);
                dtMain = comFun.AddInsertPerTools(dtMain);
                ViewState["CurrentTableBudget"] = dtMain;
                //Main Grid
                lbl_MSG.Text = "";
                lblResult.Text = "";
                grdviewAllValues.DataSource = null;
                grdviewAllValues.DataBind();
                ddlCustomerList.SelectedValue = CustomerNumber;
                ddlCustomerNumber.Text = CustomerNumber;
                if (dtMain != null)
                {
                    grdviewAllValues.DataSource = dtMain;
                    grdviewAllValues.DataBind();

                    bindgridColor();
                    bindgridCssforValueAdding();

                    panelButtons.Visible = true;
                    //get customer budget status (Save/approve/review)
                    string role = Session["RoleId"].ToString();
                    string status = objBudget.getCustomerBudgetStatus(CustomerNumber, role);
                    hdnCustomerBudgetStatus.Value = status;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                    if (status == "APPROVE")
                    {
                        string cust = ddlCustomerList.SelectedItem.Text;
                        lbl_MSG.Text = "Selected Customer : " + cust + " was Approved  ";
                    }
                    else if (status == "REVIEW")
                    {
                        string cust = ddlCustomerList.SelectedItem.Text;
                        lbl_MSG.Text = "Selected Customer : " + cust + " is under Review ";
                    }
                }


                Session["CustomerNumber"] = null;
            }

        }

        protected void LoadCustomerDetails(string strUserId, string roleId)
        {
            DataTable dtCutomerDetails = new DataTable();
            if (roleId == "TM")
                dtCutomerDetails = objBudget.LoadCustomerDetails("ALL", roleId, strUserId,null,null);
            else
                dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, roleId);
            if (dtCutomerDetails != null)
            {
                DataTable dtDeatils = new DataTable();
                dtDeatils.Columns.Add("customer_number", typeof(string));
                dtDeatils.Columns.Add("customer_name", typeof(string));


                for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                {
                    if (roleId=="TM")
                        dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                    else
                        dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[0].ToString(), dtCutomerDetails.Rows[i].ItemArray[1].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[0].ToString() + ")");
                }
                ddlCustomerList.DataSource = dtDeatils;
                ddlCustomerList.DataTextField = "customer_name";
                ddlCustomerList.DataValueField = "customer_number";
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, "-- SELECT CUSTOMER --");

                ddlCustomerNumber.DataSource = dtCutomerDetails;
                ddlCustomerNumber.DataTextField = "customer_number";
                ddlCustomerNumber.DataValueField = "customer_number";
                ddlCustomerNumber.DataBind();
                ddlCustomerNumber.Items.Insert(0, "-- SELECT CUSTOMER NUMBER --");
            }



        }

        protected void LoadCSS()
        {
            cssList.Add("color_3");
            cssList.Add("color_4");
            cssList.Add("color_3");
            //cssList.Add("color_5");
            //cssList.Add("color_4");
            //cssList.Add("color_2");
            //cssList.Add("greendark");

            cssListFamilyHead.Add("heading1");
            cssListFamilyHead.Add("heading2");
            cssListFamilyHead.Add("heading3");
            cssListFamilyHead.Add("heading4");
            cssListFamilyHead.Add("heading5");
            cssListFamilyHead.Add("heading6");
            cssListFamilyHead.Add("heading7");

        }
        protected string GetCSS(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssList.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssList.ElementAt(2); }
            //else if (cIndex.Contains("3"))
            //{ return cssList.ElementAt(3); }
            //else if (cIndex.Contains("4"))
            //{ return cssList.ElementAt(4); }
            //else if (cIndex.Contains("5"))
            //{ return cssList.ElementAt(5); }
            else { return cssList.ElementAt(2); }

        }
        protected string GetCssFam(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssListFamilyHead.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssListFamilyHead.ElementAt(2); }
            else if (cIndex.Contains("3"))
            { return cssListFamilyHead.ElementAt(3); }
            else if (cIndex.Contains("4"))
            { return cssListFamilyHead.ElementAt(4); }
            else if (cIndex.Contains("5"))
            { return cssListFamilyHead.ElementAt(5); }
            else if (cIndex.Contains("6"))
            { return cssListFamilyHead.ElementAt(6); }
            else { return cssListFamilyHead.ElementAt(0); }
        }

        protected void bindgridColor()
        {
            if (grdviewAllValues.Rows.Count != 0)
            {
                int colorIndex = 0;
                int rowIndex = 1, subRowIndex = 0, productTypeIndex = 1; int color = 0, parent_row_index = 0;
                bool currentRowIsLast = false;
                foreach (GridViewRow row in grdviewAllValues.Rows)
                {

                    var check = row.FindControl("lblSumFlag") as Label;
                    string txt = check.Text;



                    var lbl_QuantityVariance_sales_qty_year_1 = row.FindControl("lbl_QuantityVariance_sales_qty_year_1") as Label;
                    lbl_QuantityVariance_sales_qty_year_1.Attributes.Add("title", "sales_qty_year" + (BudgetYear - 1) + "- sales_qty_year" + (BudgetYear - 2));

                    var lbl_QuantityVariance_sales_qty_year_0 = row.FindControl("lbl_QuantityVariance_sales_qty_year_0") as Label;
                    lbl_QuantityVariance_sales_qty_year_0.Attributes.Add("title", "sales_qty_year" + (BudgetYear) + "- sales_qty_year" + (BudgetYear - 1));

                    var lbl_QuantityPercentage_sales_qty_year_1 = row.FindControl("lbl_QuantityPercentage_sales_qty_year_1") as Label;
                    lbl_QuantityPercentage_sales_qty_year_1.Attributes.Add("title", "(sales_qty_year_" + (BudgetYear - 1) + "/sales_qty_year_" + (BudgetYear - 2) + ")*100");

                    var lbl_QuantityPercentage_sales_qty_year_0 = row.FindControl("lbl_QuantityPercentage_sales_qty_year_0") as Label;
                    lbl_QuantityPercentage_sales_qty_year_0.Attributes.Add("title", "(sales_qty_year_" + (BudgetYear) + "/sales_qty_year_" + (BudgetYear - 1) + ")*100");


                    var lbl_ValuePercentage_sales_value_year_1 = row.FindControl("lbl_ValuePercentage_sales_value_year_1") as Label;
                    lbl_ValuePercentage_sales_value_year_1.Attributes.Add("title", "(sales_value_year" + (BudgetYear - 1) + "/sales_value_year" + (BudgetYear - 2) + ")*100");


                    var lbl_ValuePercentage_sales_value_year_0 = row.FindControl("lbl_ValuePercentage_sales_value_year_0") as Label;
                    lbl_ValuePercentage_sales_value_year_0.Attributes.Add("title", "(sales_value_year" + (BudgetYear) + "/sales_value_year" + (BudgetYear - 1) + ")*100");


                    var lbl_priceschange_sales_value_year_1 = row.FindControl("lbl_priceschange_sales_value_year_1") as Label;
                    lbl_priceschange_sales_value_year_1.Attributes.Add("title", "(PricePerUnit_sales_value_year" + (BudgetYear - 1) + "/PricePerUnit_sales_value_year" + (BudgetYear - 2) + ")*100");


                    var lbl_priceschange_sales_value_year_2 = row.FindControl("lbl_priceschange_sales_value_year_2") as Label;
                    lbl_priceschange_sales_value_year_2.Attributes.Add("title", "(PricePerUnit_sales_value_year" + (BudgetYear - 2) + "/PricePerUnit_sales_value_year" + (BudgetYear - 3) + ")*100");

                    var lbl_priceschange_sales_value_year_0 = row.FindControl("lbl_priceschange_sales_value_year_0") as Label;
                    lbl_priceschange_sales_value_year_0.Attributes.Add("title", "(PricePerUnit_sales_value_year" + (BudgetYear) + "/PricePerUnit_sales_value_year" + (BudgetYear - 1) + ")*100");



                    if (txt == "typeSum")
                    {
                        row.CssClass = "product_row_hide subTotalRowGrid subrowindex subrowindex_" + subRowIndex + " ";
                        var txtQuantityP = row.FindControl("txt_ActualQuantity_sales_qty_year_P") as TextBox;
                        txtQuantityP.CssClass += " product_type_sub_total product_type_sub_total_" + productTypeIndex + " product_type_sub_total_row_" + subRowIndex;
                        txtQuantityP.Attributes["data-product_type_sub_total_index"] = subRowIndex.ToString();
                        txtQuantityP.Attributes["data-product_type"] = productTypeIndex.ToString();

                        var lblnextYearValue = row.FindControl("lbl_ActualValue_sales_value_year_P_New") as Label;
                        lblnextYearValue.CssClass += "lblnextYearValue_Sum_" + productTypeIndex + " type_sum_lblnextYearValue_" + subRowIndex;

                        productTypeIndex++;
                    }
                    else if (txt == "SubFamilySum")
                    {
                        row.CssClass = "product_row_hide greendarkSubFamSum subrowindex subrowindex_" + subRowIndex;
                        var txtQuantityP = row.FindControl("txt_ActualQuantity_sales_qty_year_P") as TextBox;
                        txtQuantityP.CssClass += " product_type_SubFamilySum product_type_SubFamilySum_" + subRowIndex;
                        txtQuantityP.Attributes["data-product_type_subfamily_total_index"] = subRowIndex.ToString();

                        var lblnextYearValue = row.FindControl("lbl_ActualValue_sales_value_year_P_New") as Label;
                        lblnextYearValue.CssClass += "subfamily_sum_lblnextYearValue_" + subRowIndex;

                    }
                    else if (txt == "SubFamilyHeading")
                    {
                        row.CssClass = "product_row_hide greendark row_index row_" + rowIndex + " parent_row_index_" + parent_row_index;

                        var image = row.FindControl("Image1") as Image;
                        image.CssClass = "row_index_image";

                        row.Attributes["data-index"] = rowIndex.ToString();
                        row.Attributes["style"] = "background: #808080;";
                        rowIndex++;
                        subRowIndex++;
                    }
                    else if (txt == "products")
                    {
                        color++;
                        if (color == 1) { row.CssClass = "color_Product1 "; }
                        else if (color == 2)
                        {
                            row.CssClass = "color_Product2 ";
                            color = 0;
                        }

                        row.CssClass += "product_row_hide product_type_" + productTypeIndex + " subrowindex subrowindex_" + subRowIndex;
                        var txtQuantityP = row.FindControl("txt_ActualQuantity_sales_qty_year_P") as TextBox;
                        txtQuantityP.CssClass += " product_type product_type_" + productTypeIndex;
                        txtQuantityP.Attributes["data-product_type"] = productTypeIndex.ToString();

                        var lblnextYearValue = row.FindControl("lbl_ActualValue_sales_value_year_P_New") as Label;
                        lblnextYearValue.CssClass += "lblnextYearValue_" + productTypeIndex;
                    }
                    else if (txt == "FamilyHeading")
                    {
                        row.CssClass = "parent_row_index";
                        row.Attributes["data-parent_row_index"] = "" + (++parent_row_index);

                        if (colorIndex <= 6)
                        {
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                            }
                            //row.CssClass = GetCssFam(colorIndex); 
                            colorIndex++;
                        }
                        else
                        {
                            colorIndex = 0;
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                            }
                        }



                    }
                    else if (txt == "FamilySum")
                    {
                        row.CssClass = "product_row_hide TotalRowGrid" + " parent_row_index_" + parent_row_index;
                        currentRowIsLast = true;

                    }
                    else if (txt == "MainSum")
                    {
                        row.CssClass = "MainTotal";
                    }

                    else if (txt == "")
                    {
                        row.CssClass = "empty_row";
                        if (!currentRowIsLast)
                        {
                            row.CssClass += " product_row_hide ";
                        }
                        currentRowIsLast = false;
                        for (int i = 0; i < row.Cells.Count; i++)
                        {
                            row.Cells[i].CssClass = "HidingHeading";// = row.Cells[1].Text + row.Cells[i].Text;
                            //row.Cells[i].BackColor = "HidingHeading";
                            //row.Cells[i].BorderColor = System.Drawing.Color.White;// = row.Cells[1].Text + row.Cells[i].Text;
                            //row.Cells[i].BackColor = System.Drawing.Color.White;
                        }
                    }
                    else if (txt == "HidingHeading")
                    {

                        for (int i = 0; i < row.Cells.Count; i++)
                        { row.Cells[i].CssClass = "greendark MainHeader"; }
                    }

                }
            }




        }

        protected void bindgridCssforValueAdding()
        {
            if (grdviewAllValues.Rows.Count != 0)
            {

                int rowIndex = 1, subRowIndex = 0, productTypeIndex = 1, parent_row_index = 0;
                foreach (GridViewRow row in grdviewAllValues.Rows)
                {
                    var txtValueP = row.FindControl("txt_PricePerUnit_sales_value_year_P") as TextBox;
                    var check = row.FindControl("lblSumFlag") as Label;
                    string txt = check.Text;
                    if (txt == "typeSum")
                    {

                        txtValueP.CssClass += " value_product_type_sub_total value_product_type_sub_total_" + productTypeIndex + " value_product_type_sub_total_row_" + subRowIndex;
                        txtValueP.Attributes["data-value_product_type_sub_total_index"] = subRowIndex.ToString();
                        productTypeIndex++;
                    }
                    else if (txt == "SubFamilySum")
                    {
                        txtValueP.CssClass += " value_product_type_SubFamilySum_" + subRowIndex;
                    }
                    else if (txt == "SubFamilyHeading")
                    {

                        rowIndex++;
                        subRowIndex++;
                    }
                    else if (txt == "products")
                    {
                        txtValueP.CssClass += " value_product_type value_product_type_" + productTypeIndex;
                        txtValueP.Attributes["data-product_type"] = productTypeIndex.ToString();

                    }

                }

            }


        }

        protected DataTable loadBudget(string CustomerNumber)
        {
            // DataTable dtBudget = new DataTable();
            //LogFile("items load Method: LoadBudgetbySubFamily(): ", "Start", "", "");
            List<DataTable> LoadBudgetbySubFamily = objBudget.LoadBudgetbySubFamily_old_withoutType(CustomerNumber);
            ///LogFile("items load Method: LoadBudgetbySubFamily(): ", "End", "", "-----------------------------------------------------");
            DataTable dtSubFamily = new DataTable();

            dtSubFamily.Columns.Add("gold_flag", typeof(string)); //1
            dtSubFamily.Columns.Add("top_flag", typeof(string)); //2
            dtSubFamily.Columns.Add("five_years_flag", typeof(string)); //3
            dtSubFamily.Columns.Add("bb_flag", typeof(string));//4
            dtSubFamily.Columns.Add("SPC_flag", typeof(string));//5

            dtSubFamily.Columns.Add("item_id", typeof(string)); //6
            dtSubFamily.Columns.Add("item_family_id", typeof(string));//7
            dtSubFamily.Columns.Add("item_family_name", typeof(string)); //8
            dtSubFamily.Columns.Add("item_sub_family_id", typeof(string));//9
            dtSubFamily.Columns.Add("item_sub_family_name", typeof(string));//10

            dtSubFamily.Columns.Add("item_group_code", typeof(string));//11
            dtSubFamily.Columns.Add("insert_or_tool_flag", typeof(string));//12
            dtSubFamily.Columns.Add("item_code", typeof(string));//13
            dtSubFamily.Columns.Add("item_short_name", typeof(string));//14
            dtSubFamily.Columns.Add("item_description", typeof(string));//15
            dtSubFamily.Columns.Add("customer_number", typeof(string));//16

            dtSubFamily.Columns.Add("sales_qty_year_2", typeof(string));//17
            dtSubFamily.Columns.Add("sales_qty_year_1", typeof(string));//18
            dtSubFamily.Columns.Add("sales_qty_year_0", typeof(string));//19
            dtSubFamily.Columns.Add("estimate_qty_next_year", typeof(string));//20

            dtSubFamily.Columns.Add("sales_value_year_2", typeof(string));//21
            dtSubFamily.Columns.Add("sales_value_year_1", typeof(string));//22
            dtSubFamily.Columns.Add("sales_value_year_0", typeof(string));//23
            dtSubFamily.Columns.Add("estimate_value_next_year", typeof(string));//24

            dtSubFamily.Columns.Add("review_flag", typeof(string)); //25
            dtSubFamily.Columns.Add("salesengineer_id", typeof(string));//26
            dtSubFamily.Columns.Add("status_sales_engineer", typeof(string));//27
            dtSubFamily.Columns.Add("status_branch_manager", typeof(string));//28
            dtSubFamily.Columns.Add("status_ho", typeof(string));//29

            dtSubFamily.Columns.Add("sumFlag", typeof(string)); //30

            dtSubFamily.Columns.Add("budget_id", typeof(string)); //31
            dtSubFamily.Columns.Add("estimate_rate_next_year", typeof(string));//32




            foreach (DataTable dtTemp in LoadBudgetbySubFamily)
            {
                int flag = 0;
                decimal sales_qty_year_2 = 0, sales_qty_year_1 = 0, sales_qty_year_0 = 0, estimate_qty_next_year = 0,
                    sales_value_year_2 = 0, sales_value_year_1 = 0, sales_value_year_0 = 0, estimate_value_next_year = 0, estimate_rate_next_year = 0;
                string FamilyId = "", FamilyName = "", subFamId = "", SubFamilyName = "", strType = "";
                for (int i = 0; i < dtTemp.Rows.Count; i++)
                {
                    flag++;
                    strType = dtTemp.Rows[i].ItemArray[11].ToString();//insert_or_tool_flag                            
                    FamilyId = dtTemp.Rows[i].ItemArray[6].ToString();
                    FamilyName = dtTemp.Rows[i].ItemArray[7].ToString();
                    subFamId = dtTemp.Rows[i].ItemArray[8].ToString();
                    SubFamilyName = dtTemp.Rows[i].ItemArray[9].ToString();

                    //Quantity addition per Insert or Tool
                    sales_qty_year_2 = sales_qty_year_2 + Convert.ToDecimal(dtTemp.Rows[i].ItemArray[16].ToString() != "" ? dtTemp.Rows[i].ItemArray[16].ToString() : "0");
                    sales_qty_year_1 = sales_qty_year_1 + Convert.ToDecimal(dtTemp.Rows[i].ItemArray[17].ToString() != "" ? dtTemp.Rows[i].ItemArray[17].ToString() : "0");
                    sales_qty_year_0 = sales_qty_year_0 + Convert.ToDecimal(dtTemp.Rows[i].ItemArray[18].ToString() != "" ? dtTemp.Rows[i].ItemArray[18].ToString() : "0");
                    estimate_qty_next_year = estimate_qty_next_year + Convert.ToDecimal(dtTemp.Rows[i].ItemArray[19].ToString() != "" ? dtTemp.Rows[i].ItemArray[19].ToString() : "0");

                    //Value addition per Insert or Tool
                    sales_value_year_2 = sales_value_year_2 + Convert.ToDecimal(dtTemp.Rows[i].ItemArray[20].ToString() != "" ? dtTemp.Rows[i].ItemArray[20].ToString() : "0");
                    sales_value_year_1 = sales_value_year_1 + Convert.ToDecimal(dtTemp.Rows[i].ItemArray[21].ToString() != "" ? dtTemp.Rows[i].ItemArray[21].ToString() : "0");
                    sales_value_year_0 = sales_value_year_0 + Convert.ToDecimal(dtTemp.Rows[i].ItemArray[22].ToString() != "" ? dtTemp.Rows[i].ItemArray[22].ToString() : "0");
                    estimate_value_next_year = estimate_value_next_year + Convert.ToDecimal(dtTemp.Rows[i].ItemArray[23].ToString() != "" ? dtTemp.Rows[i].ItemArray[23].ToString() : "0");
                    //estimate_rate_next_year
                    ///estimate_rate_next_year = estimate_rate_next_year + Convert.ToDecimal(dtTemp.Rows[i].ItemArray[30].ToString() != "" ? dtTemp.Rows[i].ItemArray[30].ToString() : "0");
                    ///Changed by aswini 
                    ///
                    //estimate_rate_next_year = estimate_value_next_year / estimate_qty_next_year; 
                    //estimate_rate_next_year + Convert.ToDecimal(dtTemp.Rows[i].ItemArray[30].ToString() != "" ? dtTemp.Rows[i].ItemArray[30].ToString() : "0");

                    dtSubFamily.Rows.Add(
                            dtTemp.Rows[i].ItemArray[0].ToString(),//gold_flag
                            dtTemp.Rows[i].ItemArray[1].ToString(),//top_flag
                            dtTemp.Rows[i].ItemArray[2].ToString(),//five_years_flag
                            dtTemp.Rows[i].ItemArray[3].ToString(),//bb_flag
                            dtTemp.Rows[i].ItemArray[4].ToString(),//SPC_flag

                            dtTemp.Rows[i].ItemArray[5].ToString(),//item_id
                            dtTemp.Rows[i].ItemArray[6].ToString(),//item_family_id
                            dtTemp.Rows[i].ItemArray[7].ToString(),//item_family_name

                            dtTemp.Rows[i].ItemArray[8].ToString(), //item_sub_family_id
                            dtTemp.Rows[i].ItemArray[9].ToString(), //item_sub_family_name

                            dtTemp.Rows[i].ItemArray[10].ToString(),//item_group_code
                            dtTemp.Rows[i].ItemArray[11].ToString(),//insert_or_tool_flag
                            dtTemp.Rows[i].ItemArray[12].ToString(),//item_code
                            dtTemp.Rows[i].ItemArray[13].ToString(),//item_short_name
                            dtTemp.Rows[i].ItemArray[14].ToString(), //item_description
                            dtTemp.Rows[i].ItemArray[15].ToString(), //customer_number

                            dtTemp.Rows[i].ItemArray[16].ToString(),//sales_qty_year_2
                            dtTemp.Rows[i].ItemArray[17].ToString(), // sales_qty_year_1
                            dtTemp.Rows[i].ItemArray[18].ToString(),// sales_qty_year_0
                            dtTemp.Rows[i].ItemArray[19].ToString(), //estimate_qty_next_year

                            dtTemp.Rows[i].ItemArray[20].ToString(),//sales_value_year_2,
                            dtTemp.Rows[i].ItemArray[21].ToString(),//sales_value_year_1,
                            dtTemp.Rows[i].ItemArray[22].ToString(),//sales_value_year_0,
                            dtTemp.Rows[i].ItemArray[23].ToString(),//estimate_value_next_year,

                            dtTemp.Rows[i].ItemArray[24].ToString(),//[review_flag],
                            dtTemp.Rows[i].ItemArray[25].ToString(),//[salesengineer_id],
                            dtTemp.Rows[i].ItemArray[26].ToString(),//[status_sales_engineer],
                            dtTemp.Rows[i].ItemArray[27].ToString(),//[status_branch_manager],
                            dtTemp.Rows[i].ItemArray[28].ToString(),//[status_ho],

                            "products",// column "sumFlag"
                             dtTemp.Rows[i].ItemArray[29].ToString(), // budget Id
                             dtTemp.Rows[i].ItemArray[30].ToString() // estimate_rate_next_year 
                            );
                }
                if (strType == "I") { strType = "INSERTS"; }
                else if (strType == "T") { strType = "TOOLS"; }
                else { strType = "OTHERS"; }//pending add type toal Name
                if (flag == dtTemp.Rows.Count && dtTemp.Rows.Count != 0)
                {
                    dtSubFamily.Rows.Add("",//gold_flag
                                         "",//top_flag
                                        "",//five_years_flag
                                        "",//bb_flag
                                        "",//SPC_flag

                                        "",//item_id
                                        FamilyId,//item_family_id
                                        FamilyName,
                                        subFamId, //item_sub_family_id
                                        SubFamilyName,

                                        "",//item_group_code
                                        "Total",//insert_or_tool_flag
                                        "",//item_code
                                        "",//item_short_name
                                        "TOTAL " + SubFamilyName + " " + strType, //item_description //SubFamily Name give here
                                        "", //customer_number

                                         sales_qty_year_2,
                                         sales_qty_year_1,
                                         sales_qty_year_0,
                                         estimate_qty_next_year,

                                         sales_value_year_2,
                                         sales_value_year_1,
                                         sales_value_year_0,
                                         estimate_value_next_year,

                                         "", "", "", "", "",
                                         "typeSum", "",
                                         estimate_qty_next_year != 0 ? estimate_value_next_year / estimate_qty_next_year : 0
                                         ); //pending to add values here >>>>> type addition values
                }

            }

            DataTable dtMainSub = new DataTable();
            dtMainSub = dtSubFamily.Clone();
            DataTable SubFamIds = new DataTable();
            SubFamIds = objBudget.LoadSubFamilyId();

            for (int j = 0; j < SubFamIds.Rows.Count; j++)
            {
                int flag = 0;
                decimal sales_qty_year_2 = 0, sales_qty_year_1 = 0, sales_qty_year_0 = 0, estimate_qty_next_year = 0,
                     sales_value_year_2 = 0, sales_value_year_1 = 0, sales_value_year_0 = 0, estimate_value_next_year = 0, estimate_rate_next_year = 0;
                string FamilyId = "", FamilyName = "", subFamId = "", SubFamilyName = "";
                int count = 0;
                flag++;
                for (int i = 0; i < dtSubFamily.Rows.Count; i++)
                {
                    if (SubFamIds.Rows[j].ItemArray[0].ToString() == dtSubFamily.Rows[i].ItemArray[8].ToString())
                    {
                        if (count == 0)
                        {
                            count++;
                            //adding Here Header of the SubFamily

                            dtMainSub.Rows.Add(
                            "GOLD",//gold_flag
                            "TOP",//top_flag
                            "5yrs",//five_years_flag
                            "BB",//bb_flag
                            "SPC",//SPC_flag
                            "",//item_id
                            dtSubFamily.Rows[i].ItemArray[6].ToString(),//item_family_id,\
                            dtSubFamily.Rows[i].ItemArray[7].ToString(),//item_family_Name,\

                            dtSubFamily.Rows[i].ItemArray[8].ToString(), //item_sub_family_id
                            dtSubFamily.Rows[i].ItemArray[9].ToString(),//item_sub_family_name

                            "",//item_group_code
                            "SubFamHeading",//insert_or_tool_flag
                            "",//item_code
                            "",//item_short_name
                            dtSubFamily.Rows[i].ItemArray[9].ToString(), //item_description SubFamily name Insert Here
                            dtSubFamily.Rows[i].ItemArray[13].ToString(), //customer_number


                            Convert.ToString(System.DateTime.Now.Year - 2),//sales_qty_year_2
                            Convert.ToString(System.DateTime.Now.Year - 1), // sales_qty_year_1
                            Convert.ToString(System.DateTime.Now.Year) + "P",// sales_qty_year_0
                            Convert.ToString(System.DateTime.Now.Year + 1) + "B", //NextYear

                            Convert.ToString(System.DateTime.Now.Year - 2),//sales_value_year_2,
                            Convert.ToString(System.DateTime.Now.Year - 1),//sales_value_year_1,
                            Convert.ToString(System.DateTime.Now.Year) + "P",//sales_value_year_0,
                            Convert.ToString(System.DateTime.Now.Year + 1) + "B",//sales_qty_year_p

                            "", "", "", "", "",

                            "SubFamilyHeading",// column "sumFlag"
                            "", ""
                            );
                        }
                        if (dtSubFamily.Rows[i].ItemArray[29].ToString() == "products")
                        {

                            FamilyId = dtSubFamily.Rows[i].ItemArray[6].ToString();
                            FamilyName = dtSubFamily.Rows[i].ItemArray[7].ToString();
                            subFamId = dtSubFamily.Rows[i].ItemArray[8].ToString();
                            SubFamilyName = dtSubFamily.Rows[i].ItemArray[9].ToString();

                            //Quantity addition Sub Family Tool
                            sales_qty_year_2 = sales_qty_year_2 + Convert.ToDecimal(dtSubFamily.Rows[i].ItemArray[16].ToString() != "" ? dtSubFamily.Rows[i].ItemArray[16].ToString() : "0");
                            sales_qty_year_1 = sales_qty_year_1 + Convert.ToDecimal(dtSubFamily.Rows[i].ItemArray[17].ToString() != "" ? dtSubFamily.Rows[i].ItemArray[17].ToString() : "0");
                            sales_qty_year_0 = sales_qty_year_0 + Convert.ToDecimal(dtSubFamily.Rows[i].ItemArray[18].ToString() != "" ? dtSubFamily.Rows[i].ItemArray[18].ToString() : "0");
                            estimate_qty_next_year = estimate_qty_next_year + Convert.ToDecimal(dtSubFamily.Rows[i].ItemArray[19].ToString() != "" ? dtSubFamily.Rows[i].ItemArray[19].ToString() : "0");

                            //Value addition Sub Family Tool
                            sales_value_year_2 = sales_value_year_2 + Convert.ToDecimal(dtSubFamily.Rows[i].ItemArray[20].ToString() != "" ? dtSubFamily.Rows[i].ItemArray[20].ToString() : "0");
                            sales_value_year_1 = sales_value_year_1 + Convert.ToDecimal(dtSubFamily.Rows[i].ItemArray[21].ToString() != "" ? dtSubFamily.Rows[i].ItemArray[21].ToString() : "0");
                            sales_value_year_0 = sales_value_year_0 + Convert.ToDecimal(dtSubFamily.Rows[i].ItemArray[22].ToString() != "" ? dtSubFamily.Rows[i].ItemArray[22].ToString() : "0");
                            estimate_value_next_year = estimate_value_next_year + Convert.ToDecimal(dtSubFamily.Rows[i].ItemArray[23].ToString() != "" ? dtSubFamily.Rows[i].ItemArray[23].ToString() : "0");

                            //Changed by aswini 
                            //estimate_rate_next_year = estimate_rate_next_year + Convert.ToDecimal(dtSubFamily.Rows[i].ItemArray[31].ToString() != "" ? dtSubFamily.Rows[i].ItemArray[31].ToString() : "0");

                            //estimate_rate_next_year = estimate_qty_next_year != 0 ? estimate_value_next_year / estimate_qty_next_year : 0;

                        }


                        dtMainSub.Rows.Add(
                                dtSubFamily.Rows[i].ItemArray[0].ToString(),//gold_flag
                                dtSubFamily.Rows[i].ItemArray[1].ToString(),//top_flag
                                dtSubFamily.Rows[i].ItemArray[2].ToString(),//five_years_flag
                                dtSubFamily.Rows[i].ItemArray[3].ToString(),//bb_flag
                                dtSubFamily.Rows[i].ItemArray[4].ToString(),//SPC_flag
                                dtSubFamily.Rows[i].ItemArray[5].ToString(),
                                dtSubFamily.Rows[i].ItemArray[6].ToString(),
                                dtSubFamily.Rows[i].ItemArray[7].ToString(),
                                dtSubFamily.Rows[i].ItemArray[8].ToString(),
                                dtSubFamily.Rows[i].ItemArray[9].ToString(),
                                dtSubFamily.Rows[i].ItemArray[10].ToString(),
                                dtSubFamily.Rows[i].ItemArray[11].ToString(),
                                dtSubFamily.Rows[i].ItemArray[12].ToString(),
                                dtSubFamily.Rows[i].ItemArray[13].ToString(),
                                dtSubFamily.Rows[i].ItemArray[14].ToString(),
                                dtSubFamily.Rows[i].ItemArray[15].ToString(),
                                dtSubFamily.Rows[i].ItemArray[16].ToString(),
                                dtSubFamily.Rows[i].ItemArray[17].ToString(),
                                dtSubFamily.Rows[i].ItemArray[18].ToString(),
                                dtSubFamily.Rows[i].ItemArray[19].ToString(),
                                dtSubFamily.Rows[i].ItemArray[20].ToString(),
                                dtSubFamily.Rows[i].ItemArray[21].ToString(),
                                dtSubFamily.Rows[i].ItemArray[22].ToString(),
                                dtSubFamily.Rows[i].ItemArray[23].ToString(),
                                dtSubFamily.Rows[i].ItemArray[24].ToString(),
                                dtSubFamily.Rows[i].ItemArray[25].ToString(),
                                dtSubFamily.Rows[i].ItemArray[26].ToString(),
                                dtSubFamily.Rows[i].ItemArray[27].ToString(),
                                dtSubFamily.Rows[i].ItemArray[28].ToString(),
                                dtSubFamily.Rows[i].ItemArray[29].ToString(),
                                dtSubFamily.Rows[i].ItemArray[30].ToString(),
                                dtSubFamily.Rows[i].ItemArray[31].ToString()

                                );
                    }
                }

                dtMainSub.Rows.Add("",//gold_flag
                                    "",//top_flag
                                    "",//five_years_flag
                                    "",//bb_flag
                                    "",//SPC_flag

                                    "",//item_id
                                    FamilyId,//item_family_id
                                    FamilyName,
                                    subFamId, //item_sub_family_id
                                    SubFamilyName,

                                    "",//item_group_code
                                    "Total",//insert_or_tool_flag
                                    "",//item_code
                                    "",//item_short_name
                                     " TOTAL " + SubFamilyName, //item_description //SubFamily Name give here
                                    "", //customer_number

                                    sales_qty_year_2,
                                    sales_qty_year_1,
                                    sales_qty_year_0,
                                    estimate_qty_next_year,

                                    sales_value_year_2,
                                    sales_value_year_1,
                                    sales_value_year_0,
                                    estimate_value_next_year,

                                    "", "", "", "", "",

                                    "SubFamilySum", "", estimate_qty_next_year != 0 ? estimate_value_next_year / estimate_qty_next_year : 0);
                dtMainSub.Rows.Add("");

            }

            DataTable dtFamily = new DataTable();
            dtFamily = objBudget.LoadFamilyId();

            DataTable dtMain = new DataTable();
            dtMain = dtMainSub.Clone();
            //family total and Main Toatal is pending 


            for (int i = 0; i < dtFamily.Rows.Count; i++)
            {
                string FamilyId = ""; string FamilyName = "";
                int flag = 0;
                decimal sales_qty_year_2 = 0, sales_qty_year_1 = 0, sales_qty_year_0 = 0, estimate_qty_next_year = 0,
                    sales_value_year_2 = 0, sales_value_year_1 = 0, sales_value_year_0 = 0, estimate_value_next_year = 0, estimate_rate_next_year = 0;
                int count = 0;
                for (int j = 0; j < dtMainSub.Rows.Count; j++)
                {
                    if (dtFamily.Rows[i].ItemArray[0].ToString() == dtMainSub.Rows[j].ItemArray[6].ToString())
                    {
                        if (count == 0)
                        {
                            count++;
                            //adding Here Header of the SubFamily

                            dtMain.Rows.Add(
                                            "",//gold_flag
                                            "",//top_flag
                                            "",//five_years_flag
                                            "",//bb_flag
                                            "",//SPC_flag

                                            "",//item_id
                                            dtMainSub.Rows[j].ItemArray[6].ToString(),//item_family_id,
                                            dtMainSub.Rows[j].ItemArray[7].ToString(),//item_family_Name,
                                            "", "",

                                            "",//item_group_code
                                            "FamilyHeading",//insert_or_tool_flag
                                            "",//item_code
                                            "",//item_short_name
                                            "", //item_description SubFamily name Insert Here
                                            dtMainSub.Rows[j].ItemArray[13].ToString(), //customer_number


                                            "",//sales_qty_year_2
                                            "", // sales_qty_year_1
                                            "",// sales_qty_year_0
                                            "", //NextYear

                                            "",//sales_value_year_2,
                                            "",//sales_value_year_1,
                                            "",//sales_value_year_0,
                                            "",//sales_qty_year_p

                                            "", "", "", "", "",

                                            "FamilyHeading",// column "sumFlag"
                                             "", "");
                        }
                        if (dtMainSub.Rows[j].ItemArray[29].ToString() == "products")
                        {
                            flag++;
                            FamilyId = dtMainSub.Rows[j].ItemArray[6].ToString();
                            FamilyName = dtMainSub.Rows[j].ItemArray[7].ToString();

                            //Quantity addition Family Total
                            sales_qty_year_2 = sales_qty_year_2 + Convert.ToDecimal(dtMainSub.Rows[j].ItemArray[16].ToString() != "" ? dtMainSub.Rows[j].ItemArray[16].ToString() : "0");
                            sales_qty_year_1 = sales_qty_year_1 + Convert.ToDecimal(dtMainSub.Rows[j].ItemArray[17].ToString() != "" ? dtMainSub.Rows[j].ItemArray[17].ToString() : "0");
                            sales_qty_year_0 = sales_qty_year_0 + Convert.ToDecimal(dtMainSub.Rows[j].ItemArray[18].ToString() != "" ? dtMainSub.Rows[j].ItemArray[18].ToString() : "0");
                            estimate_qty_next_year = estimate_qty_next_year + Convert.ToDecimal(dtMainSub.Rows[j].ItemArray[19].ToString() != "" ? dtMainSub.Rows[j].ItemArray[19].ToString() : "0");

                            //Value addition Family Tool
                            sales_value_year_2 = sales_value_year_2 + Convert.ToDecimal(dtMainSub.Rows[j].ItemArray[20].ToString() != "" ? dtMainSub.Rows[j].ItemArray[20].ToString() : "0");
                            sales_value_year_1 = sales_value_year_1 + Convert.ToDecimal(dtMainSub.Rows[j].ItemArray[21].ToString() != "" ? dtMainSub.Rows[j].ItemArray[21].ToString() : "0");
                            sales_value_year_0 = sales_value_year_0 + Convert.ToDecimal(dtMainSub.Rows[j].ItemArray[22].ToString() != "" ? dtMainSub.Rows[j].ItemArray[22].ToString() : "0");
                            estimate_value_next_year = estimate_value_next_year + Convert.ToDecimal(dtMainSub.Rows[j].ItemArray[23].ToString() != "" ? dtMainSub.Rows[j].ItemArray[23].ToString() : "0");
                            //estimate_rate_next_year = estimate_rate_next_year + Convert.ToDecimal(dtMainSub.Rows[j].ItemArray[31].ToString() != "" ? dtMainSub.Rows[j].ItemArray[31].ToString() : "0");
                        }
                        dtMain.Rows.Add(
                               dtMainSub.Rows[j].ItemArray[0].ToString(),//gold_flag
                               dtMainSub.Rows[j].ItemArray[1].ToString(),//top_flag
                               dtMainSub.Rows[j].ItemArray[2].ToString(),//five_years_flag
                               dtMainSub.Rows[j].ItemArray[3].ToString(),//bb_flag
                               dtMainSub.Rows[j].ItemArray[4].ToString(),//SPC_flag
                               dtMainSub.Rows[j].ItemArray[5].ToString(),
                               dtMainSub.Rows[j].ItemArray[6].ToString(),
                               dtMainSub.Rows[j].ItemArray[7].ToString(),
                               dtMainSub.Rows[j].ItemArray[8].ToString(),
                               dtMainSub.Rows[j].ItemArray[9].ToString(),
                               dtMainSub.Rows[j].ItemArray[10].ToString(),
                               dtMainSub.Rows[j].ItemArray[11].ToString(),
                               dtMainSub.Rows[j].ItemArray[12].ToString(),
                               dtMainSub.Rows[j].ItemArray[13].ToString(),
                               dtMainSub.Rows[j].ItemArray[14].ToString(),
                               dtMainSub.Rows[j].ItemArray[15].ToString(),
                               dtMainSub.Rows[j].ItemArray[16].ToString(),
                               dtMainSub.Rows[j].ItemArray[17].ToString(),
                               dtMainSub.Rows[j].ItemArray[18].ToString(),
                               dtMainSub.Rows[j].ItemArray[19].ToString(),
                               dtMainSub.Rows[j].ItemArray[20].ToString(),
                               dtMainSub.Rows[j].ItemArray[21].ToString(),
                               dtMainSub.Rows[j].ItemArray[22].ToString(),
                               dtMainSub.Rows[j].ItemArray[23].ToString(),
                               dtMainSub.Rows[j].ItemArray[24].ToString(),
                               dtMainSub.Rows[j].ItemArray[25].ToString(),
                               dtMainSub.Rows[j].ItemArray[26].ToString(),
                               dtMainSub.Rows[j].ItemArray[27].ToString(),
                               dtMainSub.Rows[j].ItemArray[28].ToString(),
                               dtMainSub.Rows[j].ItemArray[29].ToString(),
                               dtMainSub.Rows[j].ItemArray[30].ToString(),
                               dtMainSub.Rows[j].ItemArray[31].ToString()
                               );
                    }


                    int k = dtMainSub.Rows.Count;
                    if (k - 2 > j)
                    {
                        if (dtMainSub.Rows[j].ItemArray[29].ToString() == "" && (dtFamily.Rows[i].ItemArray[0].ToString() == dtMainSub.Rows[j + 1].ItemArray[6].ToString()))
                        {
                            dtMain.Rows.Add("");
                        }
                    }

                }
                dtMain.Rows.Add("",//gold_flag
                                    "",//top_flag
                                    "",//five_years_flag
                                    "",//bb_flag
                                    "",//SPC_flag

                                    "",//item_id
                                    FamilyId,//item_family_id
                                    FamilyName,
                                    "", //item_sub_family_id
                                    "",

                                    "",//item_group_code
                                    "Total",//insert_or_tool_flag
                                    "",//item_code
                                    "",//item_short_name
                                    " TOTAL " + FamilyName + " FAMILY", //item_description //Family Name give here
                                    "", //customer_number

                                    sales_qty_year_2,
                                    sales_qty_year_1,
                                    sales_qty_year_0,
                                    estimate_qty_next_year,

                                    sales_value_year_2,
                                    sales_value_year_1,
                                    sales_value_year_0,
                                    estimate_value_next_year,

                                    "", "", "", "", "",

                                    "FamilySum", "", estimate_qty_next_year != 0 ? estimate_value_next_year / estimate_qty_next_year : 0);
                //dtMain.Rows.Add("");
            }
            DataTable dtGrandMain = new DataTable();
            dtGrandMain = dtMain.Clone();
            decimal total_sales_qty_year_2 = 0, total_sales_qty_year_1 = 0, total_sales_qty_year_0 = 0, total_estimate_qty_next_year = 0,
               total_sales_value_year_2 = 0, total_sales_value_year_1 = 0, total_sales_value_year_0 = 0, total_estimate_value_next_year = 0, total_estimate_rate_next_year = 0;

            for (int j = 0; j < dtMain.Rows.Count; j++)
            {
                if (dtMain.Rows[j].ItemArray[29].ToString() == "FamilySum")
                {
                    //Quantity addition Main Total
                    total_sales_qty_year_2 = total_sales_qty_year_2 + Convert.ToDecimal(dtMain.Rows[j].ItemArray[16].ToString() != "" ? dtMain.Rows[j].ItemArray[16].ToString() : "0");
                    total_sales_qty_year_1 = total_sales_qty_year_1 + Convert.ToDecimal(dtMain.Rows[j].ItemArray[17].ToString() != "" ? dtMain.Rows[j].ItemArray[17].ToString() : "0");
                    total_sales_qty_year_0 = total_sales_qty_year_0 + Convert.ToDecimal(dtMain.Rows[j].ItemArray[18].ToString() != "" ? dtMain.Rows[j].ItemArray[18].ToString() : "0");
                    total_estimate_qty_next_year = total_estimate_qty_next_year + Convert.ToDecimal(dtMain.Rows[j].ItemArray[19].ToString() != "" ? dtMain.Rows[j].ItemArray[19].ToString() : "0");

                    //Value addition Main Tool
                    total_sales_value_year_2 = total_sales_value_year_2 + Convert.ToDecimal(dtMain.Rows[j].ItemArray[20].ToString() != "" ? dtMain.Rows[j].ItemArray[20].ToString() : "0");
                    total_sales_value_year_1 = total_sales_value_year_1 + Convert.ToDecimal(dtMain.Rows[j].ItemArray[21].ToString() != "" ? dtMain.Rows[j].ItemArray[21].ToString() : "0");
                    total_sales_value_year_0 = total_sales_value_year_0 + Convert.ToDecimal(dtMain.Rows[j].ItemArray[22].ToString() != "" ? dtMain.Rows[j].ItemArray[22].ToString() : "0");
                    total_estimate_value_next_year = total_estimate_value_next_year + Convert.ToDecimal(dtMain.Rows[j].ItemArray[23].ToString() != "" ? dtMain.Rows[j].ItemArray[23].ToString() : "0");
                    total_estimate_rate_next_year = total_estimate_rate_next_year + Convert.ToDecimal(dtMain.Rows[j].ItemArray[31].ToString() != "" ? dtMain.Rows[j].ItemArray[31].ToString() : "0");
                }
                dtGrandMain.Rows.Add(
                               dtMain.Rows[j].ItemArray[0].ToString(),//gold_flag
                               dtMain.Rows[j].ItemArray[1].ToString(),//top_flag
                               dtMain.Rows[j].ItemArray[2].ToString(),//five_years_flag
                               dtMain.Rows[j].ItemArray[3].ToString(),//bb_flag
                               dtMain.Rows[j].ItemArray[4].ToString(),//SPC_flag
                               dtMain.Rows[j].ItemArray[5].ToString(),
                               dtMain.Rows[j].ItemArray[6].ToString(),
                               dtMain.Rows[j].ItemArray[7].ToString(),
                               dtMain.Rows[j].ItemArray[8].ToString(),
                               dtMain.Rows[j].ItemArray[9].ToString(),
                               dtMain.Rows[j].ItemArray[10].ToString(),
                               dtMain.Rows[j].ItemArray[11].ToString(),
                               dtMain.Rows[j].ItemArray[12].ToString(),
                               dtMain.Rows[j].ItemArray[13].ToString(),
                               dtMain.Rows[j].ItemArray[14].ToString(),
                               dtMain.Rows[j].ItemArray[15].ToString(),
                               dtMain.Rows[j].ItemArray[16].ToString(),
                               dtMain.Rows[j].ItemArray[17].ToString(),
                               dtMain.Rows[j].ItemArray[18].ToString(),
                               dtMain.Rows[j].ItemArray[19].ToString(),
                               dtMain.Rows[j].ItemArray[20].ToString(),
                               dtMain.Rows[j].ItemArray[21].ToString(),
                               dtMain.Rows[j].ItemArray[22].ToString(),
                               dtMain.Rows[j].ItemArray[23].ToString(),
                               dtMain.Rows[j].ItemArray[24].ToString(),
                               dtMain.Rows[j].ItemArray[25].ToString(),
                               dtMain.Rows[j].ItemArray[26].ToString(),
                               dtMain.Rows[j].ItemArray[27].ToString(),
                               dtMain.Rows[j].ItemArray[28].ToString(),
                               dtMain.Rows[j].ItemArray[29].ToString(),
                               dtMain.Rows[j].ItemArray[30].ToString(),
                               dtMain.Rows[j].ItemArray[31].ToString()
                               );
            }
            dtGrandMain.Rows.Add("",//gold_flag
                        "",//top_flag
                        "",//five_years_flag
                        "",//bb_flag
                        "",//SPC_flag

                        "",//item_id
                        "",//item_family_id
                        "",
                        "", //item_sub_family_id
                        "",

                        "",//item_group_code
                        "MainTotal",//insert_or_tool_flag
                        "",//item_code
                        "",//item_short_name
                        "CUTTING TOOLS TOTAL", //item_description //SubFamily Name give here
                        "", //customer_number

                        total_sales_qty_year_2,
                        total_sales_qty_year_1,
                        total_sales_qty_year_0,
                        total_estimate_qty_next_year,

                        total_sales_value_year_2,
                        total_sales_value_year_1,
                        total_sales_value_year_0,
                        total_estimate_value_next_year,

                        "", "", "", "", "",
                        "MainSum", "", total_estimate_qty_next_year != 0 ? total_estimate_value_next_year / total_estimate_qty_next_year : 0);
            dtMain.Rows.Add("");


            //adding variance, percentage and price changes 
            DataTable Dtbudget = new DataTable();

            Dtbudget.Columns.Add("gold_flag", typeof(string));
            Dtbudget.Columns.Add("top_flag", typeof(string));
            Dtbudget.Columns.Add("five_years_flag", typeof(string));
            Dtbudget.Columns.Add("bb_flag", typeof(string));
            Dtbudget.Columns.Add("SPC_flag", typeof(string));

            Dtbudget.Columns.Add("item_id", typeof(string));
            Dtbudget.Columns.Add("item_family_id", typeof(string));
            Dtbudget.Columns.Add("item_family_name", typeof(string));
            Dtbudget.Columns.Add("item_sub_family_id", typeof(string));
            Dtbudget.Columns.Add("item_sub_family_name", typeof(string));

            Dtbudget.Columns.Add("item_group_code", typeof(string));
            Dtbudget.Columns.Add("insert_or_tool_flag", typeof(string));
            Dtbudget.Columns.Add("item_code", typeof(string));
            Dtbudget.Columns.Add("item_short_name", typeof(string));
            Dtbudget.Columns.Add("item_description", typeof(string));
            Dtbudget.Columns.Add("customer_number", typeof(string));

            Dtbudget.Columns.Add("sales_qty_year_2", typeof(string));
            Dtbudget.Columns.Add("sales_qty_year_1", typeof(string));
            Dtbudget.Columns.Add("sales_qty_year_0", typeof(string));
            Dtbudget.Columns.Add("estimate_qty_next_year", typeof(string));

            Dtbudget.Columns.Add("sales_value_year_2", typeof(string));
            Dtbudget.Columns.Add("sales_value_year_1", typeof(string));
            Dtbudget.Columns.Add("sales_value_year_0", typeof(string));
            Dtbudget.Columns.Add("estimate_value_next_year", typeof(string));

            Dtbudget.Columns.Add("review_flag", typeof(string));
            Dtbudget.Columns.Add("salesengineer_id", typeof(string));
            Dtbudget.Columns.Add("status_sales_engineer", typeof(string));
            Dtbudget.Columns.Add("status_branch_manager", typeof(string));
            Dtbudget.Columns.Add("status_ho", typeof(string));

            Dtbudget.Columns.Add("sumFlag", typeof(string));

            //quality variance
            Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_2", typeof(string));
            Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_1", typeof(string));
            Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_0", typeof(string));

            //Quality Percentage
            Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_2", typeof(string));
            Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_1", typeof(string));
            Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_0", typeof(string));

            //Value Percentage
            Dtbudget.Columns.Add("ValuePercentage_sales_value_year_2", typeof(string));
            Dtbudget.Columns.Add("ValuePercentage_sales_value_year_1", typeof(string));
            Dtbudget.Columns.Add("ValuePercentage_sales_value_year_0", typeof(string));

            //price per unit
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_2", typeof(string));
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_1", typeof(string));
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_0", typeof(string));
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_P", typeof(string));

            //price changes per year
            Dtbudget.Columns.Add("priceschange_sales_value_year_2", typeof(string));
            Dtbudget.Columns.Add("priceschange_sales_value_year_1", typeof(string));
            Dtbudget.Columns.Add("priceschange_sales_value_year_0", typeof(string));

            Dtbudget.Columns.Add("budget_id", typeof(string));
            Dtbudget.Columns.Add("estimate_rate_next_year", typeof(string));


            //int headcount = 0;
            int ActualYear = objConfig.getActualYear_B();
            int ActualMonth = objConfig.getActualMonth_B();
            int BudgetYear = objConfig.getBudgetYear_B();
            string Year = "";
            if (ActualMonth == 12) { Year = ActualYear + ""; }
            else { Year = ActualYear + "P"; }
            dtGrandMain = objBudget.RemoveEmptyRows(dtGrandMain);
            for (int j = 0; j < dtGrandMain.Rows.Count; j++)
            {
                if (dtGrandMain.Rows[j].ItemArray[29].ToString() == "products" || dtGrandMain.Rows[j].ItemArray[29].ToString() == "typeSum" ||
                    dtGrandMain.Rows[j].ItemArray[29].ToString() == "SubFamilySum" || dtGrandMain.Rows[j].ItemArray[29].ToString() == "FamilySum" ||
                    dtGrandMain.Rows[j].ItemArray[29].ToString() == "MainSum")
                {
                    decimal sales_qty_year_2 = 0, sales_qty_year_1 = 0, sales_qty_year_0 = 0, estimate_qty_next_year = 0,
                        sales_value_year_2 = 0, sales_value_year_1 = 0, sales_value_year_0 = 0, estimate_value_next_year = 0, estimate_rate_next_year = 0;

                    estimate_rate_next_year = dtGrandMain.Rows[j].ItemArray[31].ToString() == "" ? 0 : Convert.ToDecimal(dtGrandMain.Rows[j].ItemArray[31].ToString());
                    sales_qty_year_2 = dtGrandMain.Rows[j].ItemArray[16].ToString() == "" ? 0 : Convert.ToDecimal(dtGrandMain.Rows[j].ItemArray[16].ToString());
                    sales_qty_year_1 = dtGrandMain.Rows[j].ItemArray[17].ToString() == "" ? 0 : Convert.ToDecimal(dtGrandMain.Rows[j].ItemArray[17].ToString());
                    sales_qty_year_0 = dtGrandMain.Rows[j].ItemArray[18].ToString() == "" ? 0 : Convert.ToDecimal(dtGrandMain.Rows[j].ItemArray[18].ToString());
                    estimate_qty_next_year = dtGrandMain.Rows[j].ItemArray[19].ToString() == "" ? 0 : Convert.ToDecimal(dtGrandMain.Rows[j].ItemArray[19].ToString());

                    sales_value_year_2 = dtGrandMain.Rows[j].ItemArray[20].ToString() == "" ? 0 : Convert.ToDecimal(dtGrandMain.Rows[j].ItemArray[20].ToString());
                    sales_value_year_1 = dtGrandMain.Rows[j].ItemArray[21].ToString() == "" ? 0 : Convert.ToDecimal(dtGrandMain.Rows[j].ItemArray[21].ToString());
                    sales_value_year_0 = dtGrandMain.Rows[j].ItemArray[22].ToString() == "" ? 0 : Convert.ToDecimal(dtGrandMain.Rows[j].ItemArray[22].ToString());
                    estimate_value_next_year = dtGrandMain.Rows[j].ItemArray[23].ToString() == "" ? 0 : Convert.ToDecimal(dtGrandMain.Rows[j].ItemArray[23].ToString());


                    decimal pvalue2 = (sales_value_year_2 <= 0 ? 0 : (decimal)sales_value_year_1 / sales_value_year_2 - 1) * 100;
                    decimal pvalue1 = (sales_value_year_1 <= 0 ? 0 : (decimal)sales_value_year_0 / sales_value_year_1 - 1) * 100;
                    decimal pvalue0 = (sales_value_year_0 <= 0 ? 0 : (decimal)estimate_value_next_year / sales_value_year_0 - 1) * 100;

                    decimal pqvalue2 = (sales_qty_year_2 == 0 ? 0 : (decimal)sales_value_year_2 * 1000 / sales_qty_year_2);
                    decimal pqvalue1 = (sales_qty_year_1 == 0 ? 0 : (decimal)sales_value_year_1 * 1000 / sales_qty_year_1);
                    decimal pqvalue0 = (sales_qty_year_0 == 0 ? 0 : (decimal)sales_value_year_0 * 1000 / sales_qty_year_0);
                    decimal Nextpqvalue = (estimate_qty_next_year == 0 ? 0 : (decimal)estimate_value_next_year * 1000 / estimate_qty_next_year);
                    decimal rateNextYear = dtGrandMain.Rows[j].ItemArray[31].ToString() != null && dtGrandMain.Rows[j].ItemArray[31].ToString() != "" ? Convert.ToDecimal(dtGrandMain.Rows[j].ItemArray[31].ToString()) : 0;

                    // rateNextYear = Math.Round((decimal)rateNextYear);
                    Dtbudget.Rows.Add(
                               dtGrandMain.Rows[j].ItemArray[0].ToString(),//gold_flag
                               dtGrandMain.Rows[j].ItemArray[1].ToString(),//top_flag
                               dtGrandMain.Rows[j].ItemArray[2].ToString(),//five_years_flag
                               dtGrandMain.Rows[j].ItemArray[3].ToString(),//bb_flag
                               dtGrandMain.Rows[j].ItemArray[4].ToString(),//SPC_flag

                               dtGrandMain.Rows[j].ItemArray[5].ToString(),
                               dtGrandMain.Rows[j].ItemArray[6].ToString(),
                               dtGrandMain.Rows[j].ItemArray[7].ToString(),
                               dtGrandMain.Rows[j].ItemArray[8].ToString(),
                               dtGrandMain.Rows[j].ItemArray[9].ToString(),

                               dtGrandMain.Rows[j].ItemArray[10].ToString(),
                               dtGrandMain.Rows[j].ItemArray[11].ToString(),
                               dtGrandMain.Rows[j].ItemArray[12].ToString(),
                               dtGrandMain.Rows[j].ItemArray[13].ToString(),
                               dtGrandMain.Rows[j].ItemArray[14].ToString(),
                               dtGrandMain.Rows[j].ItemArray[15].ToString(),

                               dtGrandMain.Rows[j].ItemArray[16].ToString(),
                               dtGrandMain.Rows[j].ItemArray[17].ToString(),
                               dtGrandMain.Rows[j].ItemArray[18].ToString(),
                               dtGrandMain.Rows[j].ItemArray[19].ToString(),

                               //Values Original

                               Math.Round((decimal)sales_value_year_2 / 1000, 0),
                               Math.Round((decimal)sales_value_year_1 / 1000, 0),
                               Math.Round((decimal)sales_value_year_0 / 1000, 0),
                               Math.Round((decimal)estimate_value_next_year / 1000, 0),
                        //dtGrandMain.Rows[j].ItemArray[20].ToString(),
                        //dtGrandMain.Rows[j].ItemArray[21].ToString(),
                        //dtGrandMain.Rows[j].ItemArray[22].ToString(),
                        //dtGrandMain.Rows[j].ItemArray[23].ToString(),

                               dtGrandMain.Rows[j].ItemArray[24].ToString(),
                               dtGrandMain.Rows[j].ItemArray[25].ToString(),
                               dtGrandMain.Rows[j].ItemArray[26].ToString(),
                               dtGrandMain.Rows[j].ItemArray[27].ToString(),
                               dtGrandMain.Rows[j].ItemArray[28].ToString(),

                               dtGrandMain.Rows[j].ItemArray[29].ToString(),//sum flag

                               //Quality Variance and Percentage
                               sales_qty_year_1 - sales_qty_year_2,
                               sales_qty_year_0 - sales_qty_year_1,
                               estimate_qty_next_year - sales_qty_year_0,

                               (sales_qty_year_2 <= 0 ? 0 : Math.Round((decimal)((decimal)sales_qty_year_1 / sales_qty_year_2 - 1) * 100, 0)) + "%", //sales_qty_year_1 / sales_qty_year_2 - 1, 
                               (sales_qty_year_1 <= 0 ? 0 : Math.Round((decimal)((decimal)sales_qty_year_0 / sales_qty_year_1 - 1) * 100, 0)) + "%",
                               (sales_qty_year_0 <= 0 ? 0 : Math.Round((decimal)((decimal)estimate_qty_next_year / sales_qty_year_0 - 1) * 100, 0)) + "%",


                               //Value Percentage
                               Math.Round(pvalue2).ToString() + "%",
                               Math.Round(pvalue1).ToString() + "%",
                               Math.Round(pvalue0).ToString() + "%",

                               //value per Quantity
                               Math.Round(pqvalue2 / 1000, 0),
                               Math.Round(pqvalue1 / 1000, 0),
                               Math.Round(pqvalue0 / 1000, 0),
                               Math.Round(Nextpqvalue / 1000, 0),
                        //pqvalue2.ToString(),
                        //pqvalue1.ToString(),
                        //pqvalue0.ToString(),
                        //Nextpqvalue.ToString(),

                               //Year changes percentage 
                               (pqvalue2 <= 0 ? 0 : Math.Round(((decimal)(pqvalue1 / pqvalue2 - 1) * 100), 0)) + "%",
                               (pqvalue1 <= 0 ? 0 : Math.Round(((decimal)(pqvalue0 / pqvalue1 - 1) * 100), 0)) + "%",
                               (pqvalue0 <= 0 ? 0 : Math.Round(((decimal)(estimate_rate_next_year / (pqvalue0 / 1000) - 1) * 100), 0)) + "%",

                               dtGrandMain.Rows[j].ItemArray[30].ToString(),//budget_id
                               Math.Round(rateNextYear)//dtGrandMain.Rows[j].ItemArray[31].ToString()
                               );

                }
                else if (dtGrandMain.Rows[j].ItemArray[29].ToString() == "SubFamilyHeading")
                {
                    Dtbudget.Rows.Add(
                              "",//gold_flag
                              "", "", "", "",

                              dtGrandMain.Rows[j].ItemArray[5].ToString(),
                              dtGrandMain.Rows[j].ItemArray[6].ToString(),
                              dtGrandMain.Rows[j].ItemArray[7].ToString(),
                              dtGrandMain.Rows[j].ItemArray[8].ToString(),
                              dtGrandMain.Rows[j].ItemArray[9].ToString(),

                              dtGrandMain.Rows[j].ItemArray[10].ToString(),
                              dtGrandMain.Rows[j].ItemArray[11].ToString(),
                              dtGrandMain.Rows[j].ItemArray[12].ToString(),
                              dtGrandMain.Rows[j].ItemArray[13].ToString(),
                              dtGrandMain.Rows[j].ItemArray[14].ToString(),
                              dtGrandMain.Rows[j].ItemArray[15].ToString(),

                              "", "", "", "",

                              "", "", "", "",

                              dtGrandMain.Rows[j].ItemArray[24].ToString(),
                              dtGrandMain.Rows[j].ItemArray[25].ToString(),
                              dtGrandMain.Rows[j].ItemArray[26].ToString(),
                              dtGrandMain.Rows[j].ItemArray[27].ToString(),
                              dtGrandMain.Rows[j].ItemArray[28].ToString(),

                              dtGrandMain.Rows[j].ItemArray[29].ToString(),//sum flag

                              //Quality
                              "", "", "",

                              "", "", "",


                              //Value
                              "", "", "",

                              "", "", "", "",


                              "", "", "",
                                dtGrandMain.Rows[j].ItemArray[30].ToString(),//budget_id
                                dtGrandMain.Rows[j].ItemArray[31].ToString()
                              );



                }
                else if (dtGrandMain.Rows[j].ItemArray[29].ToString() == "FamilyHeading")
                {
                    if (j == 0)
                    {
                        Dtbudget.Rows.Add(
                           "GOLD",//gold_flag
                           "TOP",//top_flag
                           "5yrs",//five_years_flag
                           "BB",//bb_flag
                           "SPC",//SPC_flag

                           "00",
                           "01",
                           "Family/ SubGroup/ Application",
                           "45",
                           "XXXXXXXXXXXXXXXXX",

                           "FF",
                           "Tools",
                           "  ",
                           "XXxXXXXX XXX",
                           "",
                           dtGrandMain.Rows[j].ItemArray[15].ToString(),

                           Convert.ToString(ActualYear - 2) + " Pieces",
                           Convert.ToString(ActualYear - 1) + " Pieces",
                           Year + " Pieces",
                           Convert.ToString(BudgetYear) + "B Pieces",

                           Convert.ToString(ActualYear - 2) + " Rupee(000')",
                           Convert.ToString(ActualYear - 1) + " Rupee(000')",
                           Year + " Rupee(000')",
                           Convert.ToString(BudgetYear) + "B Rupee(000')",

                           dtGrandMain.Rows[j].ItemArray[24].ToString(),
                           dtGrandMain.Rows[j].ItemArray[25].ToString(),
                           dtGrandMain.Rows[j].ItemArray[26].ToString(),
                           dtGrandMain.Rows[j].ItemArray[27].ToString(),
                           dtGrandMain.Rows[j].ItemArray[28].ToString(),

                           "HidingHeading",//dtGrandMain.Rows[j].ItemArray[29].ToString(),//sum flag

                          //Quality
                              Convert.ToString(ActualYear - 1) + "-" + Convert.ToString(ActualYear - 2) + " Incr.PCS",
                              Convert.ToString(Year) + "-" + Convert.ToString(ActualYear - 1) + " Incr.PCS",
                               Convert.ToString(BudgetYear) + "B-" + Convert.ToString(Year) + " Incr.PCS",

                              Convert.ToString(ActualYear - 1) + "/" + Convert.ToString(ActualYear - 2) + " Incr.Pcs(%)",
                              Convert.ToString(Year) + "/" + Convert.ToString(ActualYear - 1) + " Incr.Pcs(%)",
                               Convert.ToString(BudgetYear) + "B/" + Convert.ToString(Year) + " Incr.Pcs(%)",


                              //Value
                              Convert.ToString(ActualYear - 1) + "/" + Convert.ToString(ActualYear - 2) + " Incr.Rupee(%)",
                              Convert.ToString(Year) + "/" + Convert.ToString(ActualYear - 1) + " Incr.Rupee(%)",
                               Convert.ToString(BudgetYear) + "B/" + Convert.ToString(Year) + " Incr.Rupee(%)",

                              Convert.ToString(ActualYear - 2) + " RupeePerPcs",
                              Convert.ToString(ActualYear - 1) + " RupeePerPcs",
                              Convert.ToString(Year) + " RupeePerPcs",
                              Convert.ToString(BudgetYear) + "B" + " RupeePerPcs",



                              Convert.ToString(ActualYear - 1) + "/" + Convert.ToString(ActualYear - 2) + " PriceChange(%)",
                              Convert.ToString(Year) + "/" + Convert.ToString(ActualYear - 1) + " PriceChange(%)",
                               Convert.ToString(BudgetYear) + "B/" + Convert.ToString(Year) + " PriceChange(%)",
                                dtGrandMain.Rows[j].ItemArray[30].ToString(),
                                dtGrandMain.Rows[j].ItemArray[31].ToString()//budget_id
                              );

                    }
                    Dtbudget.Rows.Add(
                              dtGrandMain.Rows[j].ItemArray[0].ToString(),//gold_flag
                              dtGrandMain.Rows[j].ItemArray[1].ToString(),//top_flag
                              dtGrandMain.Rows[j].ItemArray[2].ToString(),//five_years_flag
                              dtGrandMain.Rows[j].ItemArray[3].ToString(),//bb_flag
                              dtGrandMain.Rows[j].ItemArray[4].ToString(),//SPC_flag

                              dtGrandMain.Rows[j].ItemArray[5].ToString(),
                              dtGrandMain.Rows[j].ItemArray[6].ToString(),
                              dtGrandMain.Rows[j].ItemArray[7].ToString(),
                              dtGrandMain.Rows[j].ItemArray[8].ToString(),
                              dtGrandMain.Rows[j].ItemArray[9].ToString(),

                              dtGrandMain.Rows[j].ItemArray[10].ToString(),
                              dtGrandMain.Rows[j].ItemArray[11].ToString(),
                              dtGrandMain.Rows[j].ItemArray[12].ToString(),
                              dtGrandMain.Rows[j].ItemArray[13].ToString(),
                              dtGrandMain.Rows[j].ItemArray[14].ToString(),
                              dtGrandMain.Rows[j].ItemArray[15].ToString(),

                              dtGrandMain.Rows[j].ItemArray[16].ToString(),
                              dtGrandMain.Rows[j].ItemArray[17].ToString(),
                              dtGrandMain.Rows[j].ItemArray[18].ToString(),
                              dtGrandMain.Rows[j].ItemArray[19].ToString(),

                              dtGrandMain.Rows[j].ItemArray[20].ToString(),
                              dtGrandMain.Rows[j].ItemArray[21].ToString(),
                              dtGrandMain.Rows[j].ItemArray[22].ToString(),
                              dtGrandMain.Rows[j].ItemArray[23].ToString(),

                              dtGrandMain.Rows[j].ItemArray[24].ToString(),
                              dtGrandMain.Rows[j].ItemArray[25].ToString(),
                              dtGrandMain.Rows[j].ItemArray[26].ToString(),
                              dtGrandMain.Rows[j].ItemArray[27].ToString(),
                              dtGrandMain.Rows[j].ItemArray[28].ToString(),

                              dtGrandMain.Rows[j].ItemArray[29].ToString(),//sum flag
                              dtGrandMain.Rows[j].ItemArray[30].ToString(),
                              dtGrandMain.Rows[j].ItemArray[31].ToString()
                              );

                }
                else if (dtGrandMain.Rows[j].ItemArray[29].ToString() == "")
                {
                    Dtbudget.Rows.Add("");
                }

            }


            //grid displaying is pending.. for check box and status of values text boxes 
            return Dtbudget;
        }


        public void LogFile(string use1, string count, string names, string use)
        {
            StreamWriter log;

            if (!File.Exists("logfileBudget.txt"))
            {
                log = new StreamWriter(("C:\\TSBA_Log\\logfileBudget.txt"), true);
            }
            else
            {
                log = File.AppendText(("C:\\TSBA_Log\\logfileBudget.txt"));
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(use1);
            log.WriteLine(count);
            log.WriteLine(names);
            log.WriteLine(use + "\n");
            // Close the stream:
            log.Close();

        }

        protected void LoadGrid()
        {
            LoadCSS();
            string CustomerNumber = ddlCustomerList.SelectedItem.Value;
            ddlCustomerNumber.Text = CustomerNumber;
            //DataTable dtMain = loadBudget(CustomerNumber);
            DataTable dtMain = comFun.LoadBudget(CustomerNumber);
            dtMain = comFun.GrandTotalforEntry(dtMain);
            dtMain = comFun.AddInsertPerTools(dtMain);
            grdviewAllValues.DataSource = dtMain;
            grdviewAllValues.DataBind();
            bindgridColor();
            bindgridCssforValueAdding();
            panelButtons.Visible = true;
        }


        /// This Method takes the byte stream as parameter 
        /// and return a compressed bytestream.
        /// For compression it uses GZipStream
        private byte[] Compress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(ms, CompressionMode.Compress, true);
            zs.Write(b, 0, b.Length);
            zs.Close();
            return ms.ToArray();
        }

        /// This method takes the compressed byte stream as parameter
        /// and return a decompressed bytestream.

        private byte[] Decompress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(new MemoryStream(b),
                                           CompressionMode.Decompress, true);
            byte[] buffer = new byte[4096];
            int size;
            while (true)
            {
                size = zs.Read(buffer, 0, buffer.Length);
                if (size > 0)
                    ms.Write(buffer, 0, size);
                else break;
            }
            zs.Close();
            return ms.ToArray();
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            pageStatePersister1.Load();
            String vState = pageStatePersister1.ViewState.ToString();
            byte[] pBytes = System.Convert.FromBase64String(vState);
            pBytes = Decompress(pBytes);
            LosFormatter mFormat = new LosFormatter();
            Object ViewState = mFormat.Deserialize(System.Convert.ToBase64String(pBytes));
            return new Pair(pageStatePersister1.ControlState, ViewState);
        }

        protected override void SavePageStateToPersistenceMedium(Object pViewState)
        {
            Pair pair1;
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            Object ViewState;
            if (pViewState is Pair)
            {
                pair1 = ((Pair)pViewState);
                pageStatePersister1.ControlState = pair1.First;
                ViewState = pair1.Second;
            }
            else
            {
                ViewState = pViewState;
            }
            LosFormatter mFormat = new LosFormatter();
            StringWriter mWriter = new StringWriter();
            mFormat.Serialize(mWriter, ViewState);
            String mViewStateStr = mWriter.ToString();
            byte[] pBytes = System.Convert.FromBase64String(mViewStateStr);
            pBytes = Compress(pBytes);
            String vStateStr = System.Convert.ToBase64String(pBytes);
            pageStatePersister1.ViewState = vStateStr;
            pageStatePersister1.Save();
        }
        #endregion
    }
}