﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using TSBA_BusinessAccessLayer;
using TSBA_BusinessObjects;

namespace TaegutecSalesBudget
{
    public partial class UpdateQuoteStatus : System.Web.UI.Page
    {
            #region GlobalDeclaration
            CommonFunctions objCom = new CommonFunctions();
        public static string cter;
        DataTable dtCutomerDetails;
        Review objRSum = new Review();
        Reports objReports = new Reports();
        QuoteBL objQuoteBL = new QuoteBL();
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    LoadAllDropDownAtFirst();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                    BindGrid();
                
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            BindGrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void grdPriceSummary_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName.Equals("Open"))
            //{
            //    QuoteBL objquoteBL = new QuoteBL();
            //    DataTable dtQuote = new DataTable();
            //    QuoteBO objQuoteBO = new QuoteBO();
            //    objQuoteBO.Ref_Number = Convert.ToString(e.CommandArgument);
            //    dtQuote = objquoteBL.GetQuoteDetailsBL(objQuoteBO);
            //    if (dtQuote.Rows.Count > 0)
            //    {
            //        grdDetailedPriceSummary.DataSource = dtQuote;
            //    }
            //    else
            //    {
            //        grdDetailedPriceSummary.DataSource = null;
            //    }
            //    grdDetailedPriceSummary.DataBind();
            // }
        }

        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (rdBtnTaegutec.Checked)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                }
                if (rdBtnDuraCab.Checked)
                {
                    Session["cter"] = "DUR";
                    cter = "DUR";
                }
                LoadBranches();
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        /// <summary>
        /// To load the sales engineer for the selected branch only
        /// Modified By : Anamika
        /// Date : Oct 19, 2016
        /// Desc : LoadAllDropDown is called.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                //hdnsearch.Value = "";
                DataTable dtSalesEngDetails = new DataTable();
                string branchcode = ddlBranch.SelectedItem.Value;
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                if (branchcode == "ALL")
                {
                    if (roleId == "TM")
                        dtSalesEngDetails = objReports.LoadUserInfo(null, "ALL", null, strUserId);
                    else
                        dtSalesEngDetails = objReports.LoadUserInfo(null, "ALL");

                }

                else
                {
                    if (roleId == "TM")
                        dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode, branchcode, strUserId);
                    else
                        dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);

                }
                if (dtSalesEngDetails != null)
                {
                    ddlSE.DataSource = dtSalesEngDetails;
                    ddlSE.DataTextField = "EngineerName";
                    ddlSE.DataValueField = "EngineerId";
                    ddlSE.DataBind();
                    ddlSE.Items.Insert(0, "ALL");
                }
                Session["ddlBranch"] = ddlBranch.SelectedItem.Value;
                ddlSE_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                ////LoadAllDropDown();
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void ddlCustomerClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                string branchCode = Session["BranchCode"].ToString();
                LoadCustomers(strUserId, roleId, branchCode);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                Session["ddlCustomers"] = ddlCustomers.SelectedItem.Value;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void ddlSE_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                string branchCode = Session["BranchCode"].ToString();
                LoadCustomers(strUserId, roleId, branchCode);
                //grdPriceSummary.DataSource = null;
                //grdPriceSummary.DataBind();
                Session["ddlSE"] = ddlSE.SelectedItem.Value;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Load All the branches
        /// </summary>
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

                string roleId = Convert.ToString(Session["RoleId"]);
                string userId = Convert.ToString(Session["UserId"]);
                string branchcode = Convert.ToString(Session["BranchCode"]);
                Review objRSum = new Review();
                objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
                objRSum.roleId = roleId;
                objRSum.flag = "Branch";
                objRSum.cter = Convert.ToString(Session["cter"]);
                DataTable dtData = new DataTable();
                dtData = objRSum.getFilterAreaValue(objRSum);
                if (dtData != null)
                {
                    if (dtData.Rows.Count != 0)
                    {
                        ddlBranch.DataSource = dtData;
                        ddlBranch.DataTextField = "BranchDesc";
                        ddlBranch.DataValueField = "BranchCode";
                        ddlBranch.DataBind();
                        ddlBranch.Items.Insert(0, "ALL");
                    }
                    if (roleId == "BM" || roleId == "SE")
                    {
                        string branchdesc = Session["BranchDesc"].ToString();
                        dtData.Columns.Add("BranchCode");
                        dtData.Columns.Add("BranchDesc");
                        dtData.Rows.Add(branchcode, branchdesc);
                        ddlBranch.DataSource = dtData;
                        ddlBranch.DataTextField = "BranchDesc";
                        ddlBranch.DataValueField = "BranchCode";
                        ddlBranch.DataBind();
                    }
                }
            }

            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        /// <summary>
        /// Author : Anamika
        /// Date : Oct 20, 2016
        /// Desc : Load all the dropdowns at first
        /// </summary>
        private void LoadAllDropDownAtFirst()
        {
            try
            {
                cter = null;

                if (Session["UserId"].ToString() != null)
                {
                    string strUserId = Session["UserId"].ToString();
                    string roleId = Session["RoleId"].ToString();
                    string salesEngName = Session["UserName"].ToString();
                    string branchCode = Session["BranchCode"].ToString();
                    LoadBranches();
                    LoadCustomers(strUserId, roleId, branchCode);
                    LoadAllSEs();
                    if (roleId == "SE")
                    {

                        //LoadAllReviewer_SE(strUserId);
                        //LoadAllEscalator_BM(strUserId);
                        //LoadCustomerDetails_SE(strUserId);
                        ddlBranch.SelectedValue = branchCode;
                        ddlSE.SelectedValue = strUserId;
                        ddlSE.Enabled = false;
                        ddlBranch.Enabled = false;
                        //LoadAllSEs();
                        //save.Visible = false;
                        //export.Visible = false;
                        //back.Visible = false;

                    }
                    if (roleId == "BM")
                    {

                        //LoadAllReviewer_BM(strUserId);
                        //LoadAllEscalator_BM(strUserId);
                        //LoadAllSE_BM();
                        //LoadCustomerDetails_BM(branchCode);
                        ddlBranch.SelectedValue = branchCode;
                        ddlBranch.Enabled = false;
                        //save.Visible = false;
                        //export.Visible = false;
                        //back.Visible = false;
                    }
                    if (roleId == "HO")
                    {
                        if (Session["cter"] == null && roleId == "HO")
                        {
                            Session["cter"] = "TTA";
                            cter = "TTA";

                        }
                        if (Session["cter"].ToString() == "DUR")
                        {
                            rdBtnDuraCab.Checked = true;
                            rdBtnTaegutec.Checked = false;
                            cter = "DUR";
                        }
                        else
                        {
                            rdBtnTaegutec.Checked = true;
                            rdBtnDuraCab.Checked = false;
                            cter = "TTA";
                        }
                        cterDiv.Visible = true;

                        //LoadCustomerDetails_HO();
                        //LoadAllSEs();
                        //LoadAllReviewers();
                        //LoadAllEscalator();
                        //save.Visible = false;
                        //export.Visible = false;
                        //back.Visible = false;

                    }

                    if (roleId == "TM")
                    {
                        //LoadBranches_TM(strUserId);
                        //LoadAllSE_TM(strUserId);
                        //LoadAllReviewer_TM(strUserId);
                        //LoadAllEscalator_TM(strUserId);
                        //LoadCustomerDetails_TM(strUserId);
                        //save.Visible = false;
                        //export.Visible = false;
                        //back.Visible = false;
                    }

                }
                else { Response.Redirect("Login.aspx?Login"); }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void LoadAllSEs()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtSEDetails = new DataTable();
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Session["BranchCode"].ToString();
                objRSum.BranchCode = roleId == "TM" && ddlBranch.SelectedItem.Value == "ALL" ? userId : ddlBranch.SelectedItem.Value;
                objRSum.roleId = roleId;
                objRSum.flag = "SalesEngineer";
                objRSum.cter = cter;
                dtSEDetails = objRSum.getFilterAreaValue(objRSum);
                if (dtSEDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("SE_number", typeof(string));
                    dtDeatils.Columns.Add("SE_name", typeof(string));

                    for (int i = 0; i < dtSEDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtSEDetails.Rows[i].ItemArray[0].ToString(), dtSEDetails.Rows[i].ItemArray[1].ToString());
                    }
                    ddlSE.DataSource = dtDeatils;
                    ddlSE.DataTextField = "SE_name";
                    ddlSE.DataValueField = "SE_number";
                    ddlSE.DataBind();
                    ddlSE.Items.Insert(0, "ALL");
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }

            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        private void LoadCustomers(string strUserId, string roleId, string branchCode)
        {
            try
            {
                MDP objMDP = new MDP();
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                QuoteBO objQuoteBO = new QuoteBO();
                dtCutomerDetails = new DataTable();
                string salesEngineerId = string.Empty;
                string ddCustomerClass = Convert.ToString(ddlCustomerClass.SelectedItem.Value) == "ALL" ? null : ddlCustomerClass.SelectedItem.Value;
                if (roleId == "SE")
                {
                    objQuoteBO.branch = null;
                    objQuoteBO.SE_id = strUserId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;

                    // dtCutomerDetails = objMDP.LoadCustomerDetailstype(strUserId, roleId, null, null, cter, ddCustomerClass);
                }
                else if (roleId == "BM")
                {
                    salesEngineerId = ddlSE.SelectedItem.Value == "ALL" ? null : ddlSE.SelectedItem.Value;
                    objQuoteBO.branch = branchCode;
                    objQuoteBO.SE_id = salesEngineerId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    //dtCutomerDetails = objMDP.LoadCustomerDetailstype(salesEngineerId, "BM", null, branchCode, cter, ddCustomerClass);
                }
                else if (roleId == "TM")
                {
                    string branch = ddlBranch.SelectedItem.Value == "ALL" ? null : ddlBranch.SelectedItem.Value;
                    salesEngineerId = ddlSE.SelectedItem.Value == "ALL" ? null : ddlSE.SelectedItem.Value;
                    objQuoteBO.branch = branch;
                    objQuoteBO.SE_id = salesEngineerId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    objQuoteBO.TM_id = strUserId;
                    //dtCutomerDetails = objMDP.LoadCustomerDetailstype(null, "TM", null, branch, cter, ddCustomerClass, strUserId);
                }
                else
                {
                    string branch = ddlBranch.SelectedItem.Value;
                    salesEngineerId = ddlSE.SelectedItem.Value == "ALL" ? null : ddlSE.SelectedItem.Value;
                    objQuoteBO.branch = branch;
                    objQuoteBO.SE_id = salesEngineerId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    //dtCutomerDetails = objMDP.LoadCustomerDetailstype(null, "HO", null, branch, cter, ddCustomerClass);

                }
                dtCutomerDetails = objQuoteBL.GetCustomerListBL(objQuoteBO);
                if (dtCutomerDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customernumber", typeof(string));
                    dtDeatils.Columns.Add("customername", typeof(string));
                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(Convert.ToString(dtCutomerDetails.Rows[i]["customer_number"]), Convert.ToString(dtCutomerDetails.Rows[i]["customer_short_name"]) + "(" + Convert.ToString(dtCutomerDetails.Rows[i]["customer_number"]) + ")");
                    }
                    //Session["dtCustForQuote"] = dtDeatils;
                    ddlCustomers.DataSource = dtDeatils;
                    ddlCustomers.DataTextField = "customername";
                    ddlCustomers.DataValueField = "customernumber";
                    ddlCustomers.DataBind();
                    ddlCustomers.Items.Insert(0, "ALL");
                }
                else
                {
                    ddlCustomers.DataSource = null;
                    ddlCustomers.DataBind();
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }
        private void BindGrid()
        {
            DataTable dtoutput = new DataTable();
            try
            {
                string start_date = string.Empty;
                string end_date = string.Empty;
                string selectedDate = txtDateRange.Text.Trim();
                if (selectedDate.Contains("/"))
                {
                    string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                    start_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                    end_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                }
                else
                {
                    end_date = DateTime.Now.ToString("MM/dd/yyyy").Replace("-", "/");
                    start_date = DateTime.Now.AddDays(-30).ToString("MM/dd/yyyy").Replace("-", "/");
                }

                if (Convert.ToString(Session["ddlSE"]) != "")
                {
                    ddlSE.SelectedValue = Convert.ToString(Session["ddlSE"]);
                }
                if (Convert.ToString(Session["ddlBranch"]) != "")
                {
                    ddlBranch.SelectedValue = Convert.ToString(Session["ddlBranch"]);
                }
                if (Convert.ToString(Session["ddlCustomers"]) != "")
                {
                    ddlCustomers.SelectedValue = Convert.ToString(Session["ddlCustomers"]);
                }
                QuoteBO objQuoteBO = new QuoteBO();
                objQuoteBO.StartDate = start_date;
                objQuoteBO.EndDate = end_date;
                objQuoteBO.Cust_Number = Convert.ToString(ddlCustomers.SelectedItem.Value) == "ALL" ? null : Convert.ToString(ddlCustomers.SelectedItem.Value);
                objQuoteBO.branch = Convert.ToString(ddlBranch.SelectedItem.Value) == "ALL" ? null : Convert.ToString(ddlBranch.SelectedItem.Value);
                objQuoteBO.SE_id = Convert.ToString(ddlSE.SelectedItem.Value) == "ALL" ? null : Convert.ToString(ddlSE.SelectedItem.Value);
                string roleId = Convert.ToString(Session["RoleId"]);
                string strUserId = Convert.ToString(Session["UserId"]);
                string ddCustomerClass = Convert.ToString(ddlCustomerClass.SelectedItem.Value) == "ALL" ? null : ddlCustomerClass.SelectedItem.Value;
                string salesEngineerId = string.Empty;
                if (roleId == "SE")
                {
                    //objQuoteBO.branch = null;
                    objQuoteBO.branch = Convert.ToString(Session["BranchCode"]);
                    objQuoteBO.SE_id = strUserId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;

                    // dtCutomerDetails = objMDP.LoadCustomerDetailstype(strUserId, roleId, null, null, cter, ddCustomerClass);
                }
                else if (roleId == "BM")
                {
                    salesEngineerId = ddlSE.SelectedItem.Value == "ALL" ? null : ddlSE.SelectedItem.Value;
                    objQuoteBO.branch = Convert.ToString(Session["BranchCode"]);
                    objQuoteBO.SE_id = salesEngineerId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    //dtCutomerDetails = objMDP.LoadCustomerDetailstype(salesEngineerId, "BM", null, branchCode, cter, ddCustomerClass);
                }
                else if (roleId == "TM")
                {
                    string branch = ddlBranch.SelectedItem.Value == "ALL" ? null : ddlBranch.SelectedItem.Value;
                    salesEngineerId = ddlSE.SelectedItem.Value == "ALL" ? null : ddlSE.SelectedItem.Value;
                    objQuoteBO.branch = branch;
                    objQuoteBO.SE_id = salesEngineerId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    objQuoteBO.TM_id = strUserId;
                    //dtCutomerDetails = objMDP.LoadCustomerDetailstype(null, "TM", null, branch, cter, ddCustomerClass, strUserId);
                }
                else
                {
                    string branch = ddlBranch.SelectedItem.Value == "ALL" ? null : ddlBranch.SelectedItem.Value;
                    salesEngineerId = ddlSE.SelectedItem.Value == "ALL" ? null : ddlSE.SelectedItem.Value;
                    objQuoteBO.branch = branch;
                    objQuoteBO.SE_id = salesEngineerId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    //dtCutomerDetails = objMDP.LoadCustomerDetailstype(null, "HO", null, branch, cter, ddCustomerClass);

                }
                objQuoteBO.user_id = strUserId;
               LoadDetailQuote(objQuoteBO);

               
                //dtoutput = objQuoteBL.GetQuoteSummaryBL(objQuoteBO);
                //if (dtoutput != null)
                //{
                //    if (dtoutput.Rows.Count > 0)
                //    {
                //        grdPriceSummary.DataSource = dtoutput;
                //        grdPriceSummary.DataBind();
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                //        LoadDetailQuote(objQuoteBO);
                //    }
                //    else
                //    {
                //        grdPriceSummary.DataSource = null;
                //        grdPriceSummary.DataBind();
                //    }

                //}
                //else
                //{
                //    grdPriceSummary.DataSource = null;
                //    grdPriceSummary.DataBind();
                //} 
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        private void LoadDetailQuote(QuoteBO objQuoteBO)
        {
            try
            {
                QuoteBL objquoteBL = new QuoteBL();
                objQuoteBO.Cust_Number = Convert.ToString(ddlCustomers.SelectedValue);
                DataTable dtQuote = new DataTable();
                dtQuote = objquoteBL.GetPAQuoteDetailsBL(objQuoteBO);
                Session["dtQuote"] = dtQuote;
                if (dtQuote != null)
                {
                    if (dtQuote.Rows.Count > 0)
                    {
                        grdItemSummary.DataSource = dtQuote;
                        grdItemSummary.DataBind();
                        

                    }
                    else
                    {
                        grdItemSummary.DataSource = null;
                        grdItemSummary.DataBind();
                    }

                }
                else
                {
                    grdItemSummary.DataSource = null;
                    grdItemSummary.DataBind();
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        [WebMethod]
        public static string SubmitStatus(int ID, string status, string ocNumber, int ocflag, string comment)
        {
            string output = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            UpdateQuoteStatus objPrice = new UpdateQuoteStatus();
            DateTime changedDate1 = DateTime.Now;
            string changedDate = Convert.ToString(changedDate1);
            string loggedby = HttpContext.Current.Session["UserId"].ToString();
            try
            {

                QuoteBL objBL = new QuoteBL();
                string result = objBL.SubmitStatusBL(ID, status, ocNumber, ocflag, comment, changedDate, loggedby);

                //output = "{\"msg\":\"" + result + "\"}";
                output = result;
            }
            catch (Exception ex)
            {
                output = "{\"code\":\"105\",\"msg\":\"" + ex.Message + "\"}";
                CommonFunctions.LogErrorStatic(ex);
            }
            return output;
        }
        #endregion


    }
}