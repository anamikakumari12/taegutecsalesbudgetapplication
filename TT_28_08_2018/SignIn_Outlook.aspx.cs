﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.Owin.Host.SystemWeb;
using System.Security.Principal;
using System.Security.Claims;
using System.Data;

namespace TaegutecSalesBudget
{
    public partial class SignIn_Outlook : System.Web.UI.Page
    {
        CommonFunctions objCom = new CommonFunctions();
        protected void Page_Load(object sender, EventArgs e)
        {
            Configurations obj = new Configurations();
            try
            {

                if (!Request.IsAuthenticated)
                {
                    //objCom.LogMessage(AuthenticationProperties.RedirectUri);
                    Request.GetOwinContext().Authentication.Challenge(
                         new AuthenticationProperties { RedirectUri = System.Configuration.ConfigurationManager.AppSettings["EndPoint"] },
                         OpenIdConnectAuthenticationDefaults.AuthenticationType);

                }
                else
                {
                    var userClaims = User.Identity as System.Security.Claims.ClaimsIdentity;
                    string email_id = userClaims?.FindFirst("preferred_username")?.Value;

                    objCom.LogMessage(email_id);
                    LoginAuthentication authObj = new LoginAuthentication();
                    authObj.LoginMailID = email_id;
                    DataTable dt = authObj.AuthenticateUserEmailId(authObj);
                    if (dt.Rows.Count > 0)
                    {
                        authObj.MailPassword = Convert.ToString(dt.Rows[0]["Password"]);
                        string ErrorMessage = authObj.authLogin(authObj);
                        Session["Quote_ref"] = authObj.Quote_flag;
                        Session["PhoneNumber"] = authObj.PhoneNumber;

                        String UserGuid = System.Guid.NewGuid().ToString();
                        Session["UserGuid"] = UserGuid;
                        //Session["IP"] = ipAddress;
                        Session["UserName"] = authObj.UserName;
                        Session["LoginMailId"] = authObj.LoginMailID;
                        Session["RoleId"] = authObj.RoleId;
                        Session["UserId"] = authObj.EngineerId;
                        Session["BranchCode"] = authObj.BranchCode;
                        Session["BranchDesc"] = authObj.BranchDesc;
                        Session["Territory"] = authObj.Territory;
                        Session["EngineerId"] = authObj.EngineerId;
                        Session["Password"] = authObj.MailPassword;
                        if (Session["RoleId"] != null)
                        {
                            if (Session["RoleId"].ToString() == "Admin") { Session["AdminauthObj"] = Session["authObj"]; Response.Redirect("AdminProfile.aspx?Profile"); }
                            //else if (Convert.ToString(Session["RoleId"]) == "HO" && Convert.ToString(Session["Quote_ref"]) == "1")
                            //{
                            //    Response.Redirect("PendingQuotes.aspx?Quote", false);
                            //    Context.ApplicationInstance.CompleteRequest();
                            //}
                            else
                            {
                                Otp objOtP = new Otp();
                                objOtP.SetCompanyCode(Convert.ToString(Session["UserId"]));
                                Response.Redirect("ReportDashboard.aspx?RD");
                            }
                        }
                    }
                    else
                    {
                        Response.Redirect("Login.aspx?error=1");
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
    }
}