﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProjectsDashboard.aspx.cs" Inherits="TaegutecSalesBudget.ProjectsDashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true"></asp:ScriptManager>
   
   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>

          <script src="js/amcharts/amcharts.js"></script>
          <script src="js/amcharts/funnel.js"></script>
          <script src="js/amcharts/gauge.js"></script>
          <script src="js/amcharts/radar.js"></script>
          <script src="js/amcharts/serial.js"></script>
          <script src="js/amcharts/pie.js"></script>

          <script src="js/amcharts/plugins/dataloader/dataloader.min.js"></script>
          <script src="js/amcharts/plugins/exports/export.min.js"></script>
          <link href="js/amcharts/plugins/exports/export.css" rel="stylesheet" />


          <script src="js/amcharts/plugins/exports/libs/themes/light.js"></script>


    <link rel="stylesheet" type="text/css" href="https://dc-js.github.io/dc.js/css/dc.css" />
    <script src="https://dc-js.github.io/dc.js/js/d3.js"></script>
    <script src="https://dc-js.github.io/dc.js/js/crossfilter.js"></script>
    <script src="https://dc-js.github.io/dc.js/js/dc.js"></script>
    <script src="https://rawgit.com/crossfilter/reductio/master/reductio.js"></script>
    <script src="https://npmcdn.com/universe@latest/universe.js"></script>
    <script src="js/charts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.38/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.38/vfs_fonts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/table-to-json@0.13.0/lib/jquery.tabletojson.min.js"></script>

   <link href="GridviewScroll.css" rel="stylesheet" />
   <script type="text/javascript" src="gridscroll.js"></script>
    <%--<link rel="stylesheet" href="http://cdn.kendostatic.com/2014.3.1316/styles/kendo.common.min.css" />--%>
          <link href="css/kendostatic/kendo.common.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/kendo.default.min.css" />
    <%--<link rel="stylesheet" href="http://cdn.kendostatic.com/2014.3.1316/styles/kendo.dataviz.min.css" />
    <link rel="stylesheet" href="http://cdn.kendostatic.com/2014.3.1316/styles/kendo.dataviz.default.min.css" />--%>
          <link href="css/kendostatic/kendo.dataviz.default.min.css" rel="stylesheet" />
          <link href="css/kendostatic/kendo.dataviz.min.css" rel="stylesheet" />
    
   <div class="crumbs" >
      <!-- Start : Breadcrumbs -->
      <ul id="breadcrumbs" class="breadcrumb">
         <li>
            <i class="fa fa-home"></i>
            <a>Project</a>
         </li>
         <li class="current">Dashboard</li>
         
      </ul>
   </div>
           <div id="collapsebtn" class="row">
                                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;"/> 
                                                               
          </div>
      <div id="divSelections">
     <div id="divCter" style="margin-left:5px" runat="server" visible="false">
                  <ul  class="btn-info rbtn_panel">
                      <li ><span style="margin-right:4px;vertical-align:text-bottom;  ">TAEGUTEC</span>
                       
                        <asp:RadioButton ID="rdBtnTaegutec"  AutoPostBack="true"  Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged"   GroupName="byCmpnyCodeInradiobtn" runat="server" />
                         <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">DURACARB</span>
                        <asp:RadioButton ID="rdBtnDuraCab"  AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged"   GroupName="byCmpnyCodeInradiobtn" runat="server" />
                     </li>
                  </ul>
         
    </div>
          <div runat="server" class="row filter_panel" style="padding:5px">
                  <div class="col-md-2 control" runat="server" id="div5">
                         <div id="divBranch" runat="server" class="col-md-4">
                    <label class="label">BRANCH</label><br />
                    <asp:DropDownList ID="ddlBranchList" runat="server" CssClass="control_dropdown" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged"></asp:DropDownList>
                </div>
                      </div>
               <div id="div6" runat="server"  class="col-md-2">
                          <div id="divSalesEngnr" runat="server" class="col-md-4">
                <label class="label">SALES ENGINEER</label><br />
                       <asp:DropDownList ID="ddlSalesEngineerList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSalesEngineerList_SelectedIndexChanged"
                        CssClass="control_dropdown">
                                <asp:ListItem>--SELECT SALES ENGINEER--</asp:ListItem>
                           </asp:DropDownList>

                   </div>
                   </div>
               <div  class="col-md-3">
             
                  <label class="label">CUSTOMER CLASS </label>
                      <asp:DropDownList ID="ddlCustomerClass" runat="server" CssClass="control_dropdown" >
                           <asp:ListItem Value="ALL">ALL</asp:ListItem>
                        <asp:ListItem Value="KA">KEY FOCUSED CUSTOMER</asp:ListItem>
                        <asp:ListItem Value="KF">‘A’ CLASS CUSTOMER</asp:ListItem>
                        <asp:ListItem Value="OT"> OTHERS</asp:ListItem>
                     </asp:DropDownList>
                   </div>
              <div class="col-md-3">
                    <div>
                       <br />
                     <asp:Button ID="filter" runat="server" Text="FILTER" OnClick="filter_Click" CssClass="btn green"/>
                  </div>
                   </div>
          </div>
         
         
          </div>

   <div>
       <br />
      <asp:Label runat="server" id="vallbl" style="float:right;margin-top:-15px;margin-right:10px" Text="Val In Lakhs" ></asp:Label></div>
          

      
 
 
   <style type="text/css">
      @media (min-width: 768px) and (max-width:900px) {
      th, td {
      font-size: 9px;
      }
      }
      .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td 
      {
      border-top: 1px solid #ddd;
      line-height: 1.42857;
      padding: 2px !important;
      }
       td {
           height:0px !important;
           padding:0px !important;
       }
      .square {
	   width:10px;
	    height:10px;
	   margin-left: 10px;
    }
       .valalign  {
           text-align:right;
       }
      #MainContent_lblprjcttotal    {
           text-align:right;
       }
      #MainContent_lblcmpltdval  {
           text-align:right;
       }
       .th {
            text-align:center;
       }
       .gauge-container {
                 
				 margin: 0px auto;
         }

        .gauge {
                    width: 330px;
                    height: 330px;
                    margin: 0 auto 0;
                }
         .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }
         
        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px!important;
            position: relative;
        }

    .portlet.box > .portlet-body {
    background-color: #fff;
    padding: 0px !important;
    }
      .portlet > .portlet-title > .caption {
      display: inherit !important;
      float: none !important;
      font-size: 18px !important;
      margin: 0 0 0px !important;
      padding: 0 !important;
      text-align: center !important;
      }
    #header_fixed {
    display:none;
    position:fixed;
    top:50px;
    background-color:#006780;
    margin-left:15px;
    color:#fff;
    }
      th,td {
      border: 1px solid #ddd;
      }
       th {
           background:#006780;
           color:#fff;
           text-align:center;
       }
          #div1 {
            width: 100%;
            height: 300px;
            background-image: url('images/GraphBackground.png');
            background-repeat: no-repeat;
            background-size: cover;
        }
       .progresslbl {
           text-align:center;
           padding-left:185px;
           font-size:14px;
           font-weight:600;
       }
      #MainContent_gridtrgtcmpltnprjcts tr:nth-child(odd) {
    background: rgba(227,227,227,1);
}
            #MainContent_gridtrgtcmpltnprjcts tr:nth-child(even) {
    background: #f1f1f1;
}
  
            #MainContent_GridPdngPrjcts tr:nth-child(odd) {
    background: rgba(227,227,227,1);
}
            #MainContent_GridPdngPrjcts tr:nth-child(even) {
    background: #f1f1f1;
}
                  #MainContent_GridViewOverdue tr:nth-child(odd) {
    background: rgba(227,227,227,1);
}
                   #MainContent_GridViewOverdue tr:nth-child(even) {
    background:#f1f1f1;
}

       .shadow {
-webkit-box-shadow: 1px 2px 3px 2px rgba(227,227,227,1);
-moz-box-shadow: 1px 2px 3px 2px rgba(227,227,227,1);
box-shadow: 1px 2px 3px 2px rgba(227,227,227,1);
       }
      /*th {
      padding:3px;
      text-align:center;
      }
      td {
      padding:3px;
      }*/
      .HeadergridAll {
      text-align: center;
      background:#006780; color:#fff;
      }
      #MainContent_trgtdcmpltnprjcts th{
      padding:14px;
      }
      #MainContent_trgtdcmpltnprjcts  tr:last-child, #MainContent_stgcmpltnstatus  tr:last-child, #MainContent_GridOverdue  tr:last-child,
      #MainContent_stgcmpltnstatusFreeze tr:last-child,#MainContent_GridOverdueFreeze tr:last-child  {
      /*font-size: 14px;*/
      background-color: #fdf8e4;
      color: #666666;
      }
      .portlet.box > .portlet-body {
      border-radius:0px !important;
      }
       #MainContent_Table1 td {
           border:1px solid #999;
       }
       .caption1 { border-bottom: 1px solid #ccc;}
       #dt_cnsldt_industry_val_chartdiv {
           width: 100%;
           height: 350px;
           font-size: 11px;
           float:left;
       }
       #ChartConsolidated {
           width: 100%;
           height: 350px;
           font-size: 11px;
           float:left;
       }
       #dt_projecttype_potential_chartdiv {
            width: 100%;
           height: 350px;
           font-size: 11px;
           float:left;
           fit-position:initial;
       }
     
       #ProjectsValueCompleted_chartdiv{
              width: 100%;
           height: 300px;
           font-size: 11px;
            float:left;
       }
        #dt_inustry_bsns_val_chartdiv{
              width: 100%;
           height: 400px;
           font-size: 11px;
           float:left;
       }
       #dt_pndng_prjcts_chartdiv {
               width: 100%;
           height: 200px;
           font-size: 11px;
            float:left;
       }
    #dt_dtOverduePrjcts_chartdiv,#dt_trgtd_cmpltn_prjcts_chartdiv{
             width: 100%;
           height: 200px;
           font-size: 11px;
            float:left;
       }
    
   
           .amcharts-chart-div a {
            display: none !important;
        }
               .legend-title {
  font-family: Verdana;
  font-weight: bold;
  margin-left:450px;



}
	
       /*.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
        border-top: 1px solid #ddd;
        line-height: 1.42857;
        padding:2px !important; 
        }*/
   </style>

<%--    <script src="http://cdn.kendostatic.com/2014.3.1316/js/jquery.min.js"></script>
    <script src="http://cdn.kendostatic.com/2014.3.1316/js/kendo.all.min.js"></script>--%>
          <script src="js/kendostatic/jquery.min.js"></script>
    <script src="js/kendostatic/kendo.all.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        loadScript();
        debugger;
        $('#product_image').unbind('click').bind('click', function (e) {
            var attr = $('#product_image').attr('src');
            $("#divSelections").slideToggle();
            if (attr == "images/up_arrow.png") {
                $("#product_image").attr("src", "images/down_arrow.png");
            } else {
                $("#product_image").attr("src", "images/up_arrow.png");
            }
        });
    });
    function LoadChartConsolidated() {
        console.log("LoadChartConsolidated guage");
        var tmp = null;
        var valu = "dt_cnsldt_industry_val";
        $.ajax({
            type: "POST",
            //url: 'ReportDashboard.aspx/LoadChartConsolidated',
            url: "ProjectsDashboard.aspx/BarGraphValues",
            data: "{'s':'" + valu + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                console.log("msg : " + msg);
                tmp = msg.d;
                console.log("tmp1 : " + tmp);
                tmp = AmCharts.parseJSON(tmp);
                console.log("tmp2 : " + tmp);
                var d = new Date();
                var n = d.getFullYear();
                var chart = AmCharts.makeChart("ChartConsolidated", {
                    "theme": "light",
                    "type": "gauge",
                    
                    "axes": [{
                        "axisAlpha": 0,
                        "tickAlpha": 0,
                        "labelsEnabled": false,
                        "startValue": 0,
                        "endValue": 100,
                        "startAngle": 0,
                        "endAngle": 270,
                        "bands": tmp
                        
                    }],
                        "allLabels": [{
                            "text": "Customer Potential",
                            "x": "49%",
                            "y": "10%",
                            "size": 12,
                            "bold": true,
                            "color": "#84b761",
                            "align": "right"
                        }, {
                            "text": "Project Potential",
                            "x": "49%",
                            "y": "19%",
                            "size": 12,
                            "bold": true,
                            "color": "#fdd400",
                            "align": "right"
                        }, {
                            "text": "Business Expected",
                            "x": "49%",
                            "y": "26%",
                            "size": 12,
                            "bold": true,
                            "color": "#cc4748",
                            "align": "right"
                        }, {
                            "text": "Monthly Expected",
                            "x": "49%",
                            "y": "35%",
                            "size": 12,
                            "bold": true,
                            "color": "#67b7dc",
                            "align": "right"
                        },
                        {
                            "x": "50%",
                            "align": "middle",
                            "y": "95%",
                            "bold": true,
                            "size": 12,
                            "text": "CONSOLIDATED VIEW"
                        }
                        ],
                  
                        "dataProvider": tmp,
                  
                        "export": {
                            "enabled": false
                        }
                    }
                    );
                
                chart.dataProvider = AmCharts.parseJSON(tmp);
                chart.validateData();
            },
            error: function (e) {
                //console(e);
            }
        });
    }

    function BarGraph() {
        //debugger;
        //var value = "dt_cnsldt_industry_val";
        //$.ajax({
           
        //    type: "POST",
        //    url: "ProjectsDashboard.aspx/BarGraphValues",
        //    data: "{'s':'" + value + "'}",
        //    contentType: "Application/json; charset=utf-8",
        //    dataType: "json",
        //    success: function (msg) {
        //        console.log(msg.d)
        //        var tmp = msg.d;
        //        var chart = AmCharts.makeChart("dt_cnsldt_industry_val_chartdiv", {
        //            "type": "serial",
        //            "theme": "light",
        //            "dataProvider":tmp,
        //            "categoryField": "flag",
                   
        //            "chartCursor": {
        //                "categoryBalloonEnabled": false,
        //                "cursorAlpha": 0,
        //                "zoomable": false
        //            },
        //            "categoryAxis": {
        //                "gridPosition": "start",
        //                "axisAlpha": 0.8,
        //                "gridAlpha": 0,
        //                "labelRotation":60,
                      

        //            },
        //            "allLabels": [{
        //                "x": "50%",
        //                "align": "middle",
        //                "y": "95%",
        //                "bold": true,
        //                "size": 12,
        //                "text": "CONSOLIDATED VIEW"
        //            }],
        //            "valueAxes": [{
        //                "position": "left",
        //                "axisAlpha": 0.8,
        //                "gridAlpha": 0,
        //            }],
        //            "graphs": [{
        //                "colorField": "Color",
        //                "balloonText": "[[category]]: <b>[[value]]</b>",
        //                "showAllValueLabels": true,
        //                "labelPosition": "inside",
        //                "fillAlphas": 0.8,
        //                "lineAlpha": 0.2,
        //                "type": "column",
        //                "valueField": "value",
        //                "fixedColumnWidth": 15

        //            }

        //            ],
        //        });
        //       chart.dataProvider = AmCharts.parseJSON(tmp);
        //        chart.validateData();
        //    },
        //    error: function (e) {
        //        alert("NO")
        //    }
        //});
    }
    function projecttypepotential() {
        debugger;
        var value = "dt_projecttype_potential";
        $.ajax({

            type: "POST",
            url: "ProjectsDashboard.aspx/BarGraphValues",
            data: "{'s':'" + value + "'}",
            contentType: "Application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                console.log(msg.d)
                var tmp = msg.d;
                var chart = AmCharts.makeChart("dt_projecttype_potential_chartdiv", {
                    "type": "serial",
                    "theme": "light",
                    "dataProvider": tmp,
                    "categoryField": "PType",
                   
                  
                    "legend": {
                        "horizontalGap": 15,
                        "maxColumns": 1,
                        "position": "top",
                        "useGraphSettings": true,
                        "markerSize": 10,
                        "marginTop": 20,
                        "align": "right",
                        "valueAlign":"left"
                    },
                    "chartCursor": {
                        "categoryBalloonEnabled": false,
                        "cursorAlpha": 0,
                        "zoomable": false
                    },
                    "categoryAxis": {
                        
                        "gridPosition": "start",
                        "axisAlpha": 0.8,
                        "gridAlpha": 0,
                        "title":"PROJECT TYPE VIEW",
                    
                        "autoWrap": true
                        

                    },
                    "valueAxes": [{
                        "position": "left",
                        "axisAlpha": 0.8,
                        "gridAlpha": 0,
                    }],
                    "graphs": [{
                        "balloonText": "[[title]]: <b>[[value]]</b>",
                        "fillAlphas": 0.8,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "valueField": "Project_Potential_All",
                        "title": "Potential for all projects"

                    },
                    {
                        "balloonText": "[[title]]: <b>[[value]]</b>",
                        "fillAlphas": 0.8,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "valueField": "Project_Potential_Closed",
                        "title": "Potential for closed projects"
                    }],
                    "listeners": [{
                        "event": "drawn",
                        "method": addLegendLabel
                    }]

                });
                function addLegendLabel(e) {
                    var title = document.createElement("div");
                    title.innerHTML = "";
                    title.className = "legend-title";
                    e.chart.legendDiv.appendChild(title)
                }
                chart.dataProvider = AmCharts.parseJSON(tmp);
                chart.validateData();
            },
            error: function (e) {
                alert("NO")
            }
        });
    }
          
        function inustrybsnsval() {
            debugger;
            var value = "dt_inustry_bsns_val";
            $.ajax({

                type: "POST",
                url: "ProjectsDashboard.aspx/BarGraphValues",
                data: "{'s':'" + value + "'}",
                contentType: "Application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log(msg.d)
                    var tmp = msg.d;
                    var chart = AmCharts.makeChart("dt_inustry_bsns_val_chartdiv", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider":tmp,
                        "categoryField": "IndustryName",
                       
                        "legend": {
                            "horizontalGap":5,
                            "maxColumns": 2,
                            "position": "top",
                            "useGraphSettings": true,
                            "markerSize": 10,
                            "align":"right",
                            "marginTop": 20
                            
                        },
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0.8,
                            "gridAlpha": 0,
                            "labelRotation": 30,
                            "title":"INDUSTRY VIEW"
                        },
                        "valueAxes": [{
                            "position": "left",
                            "axisAlpha": 0.8,
                            "gridAlpha": 0,
                        }],
                        "graphs": [{
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "type": "column",
                            "valueField": "Ovrl_ptnl",
                            "title":"Customer Potential"
                        },
                        {
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "type": "column",
                            "valueField": "Potential",
                            "title": "Project Potential"
                        },
                         
                        {
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "type": "column",
                            "valueField": "Bsns_Exptd",
                            "title":"Business Expected"
                        },
                        {
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "fillAlphas": 0.8,
                            "lineAlpha": 0.2,
                            "type": "column",
                            "valueField": "ytd",
                            "title": "Monthly Business Expected"
                        }],
                        "listeners": [{
                            "event": "drawn",
                            "method": addLegendLabel
                        }]
                    });
                    function addLegendLabel(e) {
                        var title = document.createElement("div");
                        title.innerHTML = "";
                        title.className = "legend-title";
                        e.chart.legendDiv.appendChild(title)
                    }
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    alert("NO")
                }
            });
          
    }
        function pndngprjcts() {
            var value = "dt_pndng_prjcts";
            $.ajax({

                type: "POST",
                url: "ProjectsDashboard.aspx/BarGraphValues",
                data: "{'s':'" + value + "'}",
                contentType: "Application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log(msg.d)
                    var tmp = msg.d;
                    var chart = AmCharts.makeChart("dt_pndng_prjcts_chartdiv", {
                      
                        "autoMargins": false,
                        "marginLeft": 0,
                        "marginRight": 0,
                        "marginBottom": 0,
                        "allLabels": [{
                            "x": "50%",
                            "align": "middle",
                            "y": "89%",
                            "bold": true,
                            "size": 12,
                            "text": "STAGE-WISE STATUS OF PENDING PROJECTS"
                        }],
                        "depth3D": 5,
                        "type": "pie",
                        "theme": "light",
                        "dataProvider": tmp,
                        "valueField": "Count",
                        "titleField": "Target"
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                }
            });
        }
        function OverduePrjcts() {
            var value = "dtOverduePrjcts";
            $.ajax({

                type: "POST",
                url: "ProjectsDashboard.aspx/BarGraphValues",
                data: "{'s':'" + value + "'}",
                contentType: "Application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log(msg.d)
                    var tmp = msg.d;
                    var chart = AmCharts.makeChart("dt_dtOverduePrjcts_chartdiv", {
                        "type": "pie",
                        "theme": "light",
                        "depth3D": 5,
                        "dataProvider": tmp,
                        "valueField": "Count",
                        "titleField": "Target",
                        "autoMargins": false,
                        "marginLeft": 0,
                        "marginRight": 0,
                        "marginBottom": 0,
                        "allLabels": [{
                            "x": "50%",
                            "align": "middle",
                            "y": "89%",
                            "bold": true,
                            "size": 12,
                            "text": "STAGE-WISE OVERDUE VALUE OF PROJECTS"
                        }],

                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                }
            });
        }
        function trgtdcmpltnprjcts() {
            var value = "dt_trgtd_cmpltn_prjcts";
            $.ajax({

                type: "POST",
                url: "ProjectsDashboard.aspx/BarGraphValues",
                data: "{'s':'" + value + "'}",
                contentType: "Application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log(msg.d)
                    var tmp = msg.d;
                    var chart = AmCharts.makeChart("dt_trgtd_cmpltn_prjcts_chartdiv", {
                        "type": "pie",
                        "theme": "light",
                        "dataProvider": tmp,
                        "depth3D": 5,
                        "valueField": "Count",
                        "titleField": "Target",
                        "autoMargins": false,
                        "marginLeft": 0,
                        "marginRight": 0,
                        "marginBottom": 0,
                        "allLabels": [{
                            "x": "50%",
                            "align": "middle",
                            "y": "89%",
                            "bold": true,
                            "size": 12,
                            "text": "POTENTIAL VALUE BY TARGETTED DAYS"
                    }],
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                }
            });
        }
        function ProjectsValueCompleted() {
            var value = "ProjectsValueCompleted";
            var tmp = document.getElementById("MainContent_hdnfieldprojectval").value;
            tmp = tmp * 100;
            console.log("Result"+tmp);
            $.ajax({
               
                type: "POST",
                url: "ProjectsDashboard.aspx/BarGraphValues",
                data: "{'s':'" + value + "'}",
                contentType: "Application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log(msg.d)
                  
                    console.log(tmp);
                    var chart = AmCharts.makeChart("ProjectsValueCompleted_chartdiv", {
                        "type": "gauge",
                        "arrows": [
                          {
                              "value": tmp
                          }
                        ],
                        "allLabels": [{
                            "x": "50%",
                            "align": "middle",
                            "y": "89%",
                            "bold": true,
                            "size": 12,
                            "text": "PROJECTS VALUE COMPLETED"
                        }],
                       
                       
                        "axes": [
                          {
                              "bottomText": tmp+"%",
                              "endValue": 100,
                              "valueInterval": 10,
                              "bands": [
                                {
                                    "color":"#ea3838",
                                    "endValue": 40,
                                    "startValue": 0
                                },
                                {
                                    "color": "#ffac29",
                                    "endValue": 60,
                                    "startValue": 40
                                },
                                {
                                    "color": "#00CC00",
                                    "endValue": 100,
                                    "startValue": 60,
                                    "innerRadius": "95%"
                                }
                              ]
                          }
                        ]
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                }
             
            });
        
        }
        function loadScript() {
            LoadChartConsolidated()
            projecttypepotential();
            inustrybsnsval();
            pndngprjcts();
            OverduePrjcts();
            trgtdcmpltnprjcts();
            ProjectsValueCompleted();
        var pval = $('#MainContent_hdnfieldprojectval').val();
        $("#gauge").kendoRadialGauge({
            pointer: [{
                // Current value
                color: "#736F6E",
                value: pval,
            }],
            scale: {
                // Start and End angle of the Gauge
                startAngle: 0,
                endAngle: 180,



                // Configure major and minor unit
                //minorUnit: 0.2,
                //majorUnit: 0.2,
                // Make major ticks same size than minor ticks
                //majorTicks: {
                //    size: 10
                //},

                // Define min and max (0% - 100%)
                min: 0,
                max: 1,

                // Labels outside the range and number as percentage with no decimals
                labels: {
                    position: "outside",
                    format: "p0"
                },

                // Color ranges
                ranges: [
                    {
                        from: 0,
                        to: 0.33,
                        color: "#830300"
                    },
                    {
                        from: 0.33,
                        to: 0.66,
                        color: "yellow"
                    },
                    {
                        from: 0.66,
                        to: 1.00,
                        color: "green"
                    }
                ],
                rangeSize: 40,
                rangeDistance: -10,
            }
        });
        $('#product_image').unbind('click').bind('click', function (e) {
            var attr = $('#product_image').attr('src');
            $("#divSelections").slideToggle();
            if (attr == "images/up_arrow.png") {
                $("#product_image").attr("src", "images/down_arrow.png");
            } else {
                $("#product_image").attr("src", "images/up_arrow.png");
            }
        });
    }
</script>
      
         <asp:HiddenField ID="hdnfieldprojectval" runat="server" />


           </ContentTemplate>
     </asp:UpdatePanel>

     <div class="portlet box grey" style="margin-bottom:0px">
         <div class="portlet-title" style="background-color:#006780;">
            <div class="caption">PROJECT PERFORMANCE</div>
                    </div>
         </div>
        
               <div class="portlet-body shadow" style="margin-top:0px">
                     <div  class="col-md-3">

                          <div id="ProjectsValueCompleted_chartdiv"  class="portlet-body shadow"></div> 
                           <div class="clearfix"></div>
                         <div class="portlet-body" >
                             <asp:UpdatePanel runat="server">
                                 <ContentTemplate>
                         <asp:Table ID="Table1" runat="server" CellPadding="3" class="table"  align="Center" rules="rows" border="1"
                    GridLines="horizontal" HorizontalAlign="Center" style="margin-top:10px">
                    <asp:TableRow>
                        <asp:TableCell style="background: #006780;color: #fff; font-weight:bold;">Total Project Potential Value</asp:TableCell>
                        <asp:TableCell  ID="lblprjcttotal">2</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell style="background: #006780;color: #fff; font-weight:bold;">Completed Project Potential Value</asp:TableCell>
                        <asp:TableCell ID="lblcmpltdval">4</asp:TableCell>
                    </asp:TableRow>
                    </asp:Table>
                                     </ContentTemplate>
                                 <Triggers>
         <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
         <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
          <asp:AsyncPostBackTrigger ControlID="ddlBranchList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="ddlSalesEngineerList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="filter" EventName="Click" />
                                     </Triggers>
                             </asp:UpdatePanel>
                          </div>
                         </div>
                   <div class="col-md-9" style="">
                         <div id="dt_inustry_bsns_val_chartdiv"></div>
                   </div>
         </div>
           
              
           <div class="clearfix"></div>
  <div class="portlet-body">
              
               
      <div class="portlet-body shadow">
<div class="col-md-3">

                <div id="ChartConsolidated" class="portlet-body shadow"></div>
   <input type="hidden" id="loadChart" runat="server" />
                 
                </div>
            <div class="portlet-body" >
                   <div class="col-md-9">
                          <div id="dt_projecttype_potential_chartdiv"></div>
                   </div>
                </div>
         </div>
               </div>
   


    
     <div class="clearfix"></div>

    <div class="container" style="margin-top:10px">
           <div class="row">
                 <div class="portlet box grey" style="margin-bottom:0px">
         <div class="portlet-title" style="background-color:#006780;">
            <div class="caption">PROGRESS</div>
         </div>
                     </div>
                     
        <div class="portlet-body shadow">
               <div class="porlet-body shadow">
                     <div  class="col-md-4">
                       <div class="portlet-body shadow">   
                  <div id="dt_pndng_prjcts_chartdiv" class="portlet-body " ></div>

                           <div class="clearfix"></div>
                         <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                          
     <asp:GridView ID="GridPdngPrjcts" runat="server" AutoGenerateColumns="false" Style="margin-left:55px;margin-top:10px">
                           <columns>
                                <asp:TemplateField HeaderText=" " ItemStyle-Width="30">
                                 <ItemTemplate>
                                 <asp:Label ID="Image1" runat="server" Height = "10" CssClass="square" style="background:#399BFF;"  Visible = '<%# Eval("Target").ToString() == "Stage 1"? true : false %>' />
                                 <asp:Label ID="Label2" runat="server" Height = "10" CssClass="square" style="background:#fdd400;"  Visible = '<%# Eval("Target").ToString() == "Stage 2" ? true : false %>' />
                                 <asp:Label ID="Label3" runat="server" Height = "10" CssClass="square" style="background:#84b761 ;"  Visible = '<%# Eval("Target").ToString() == "Stage 3" ? true : false %>' />
                                 <asp:Label ID="Label4" runat="server" Height = "10" CssClass="square" style="background:#cc4748 ;"  Visible = '<%# Eval("Target").ToString() == "Stage 4" ? true : false %>' />
                                 <asp:Label ID="Label5" runat="server" Height = "10" CssClass="square" style="background:#CB513A ;"  Visible = '<%# Eval("Target").ToString() == "Stage 5" ? true : false %>' />
                                 <asp:Label ID="Label6" runat="server" Height = "10" CssClass="square" style="background:#C7BAA7 ;"  Visible = '<%# Eval("Target").ToString() == "Stage 6" ? true : false %>' />
                                 <asp:Label ID="Label7" runat="server" Height = "10" CssClass="square" style="background:#FAA43A ;"  Visible = '<%# Eval("Target").ToString() == "Stage 7" ? true : false %>' />
                                 <asp:Label ID="Label8" runat="server" Height = "10" CssClass="square" style="background:#5DA5DA ;"  Visible = '<%# Eval("Target").ToString() == "Stage 8" ? true : false %>' />
                                 <asp:Label ID="Label9" runat="server" Height = "10" CssClass="square" style="background:#4D4D4D ;"  Visible = '<%# Eval("Target").ToString() == "Stage 9" ? true : false %>' />
                                 <asp:Label ID="Label10" runat="server" Height = "10" CssClass="square" style="background:#FF00FF;"  Visible = '<%# Eval("Target").ToString() == "Stage 10" ? true : false %>' />

                                 </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="Stage" ItemStyle-Width="90">
                                 <ItemTemplate>
                                    <asp:Label  ID="lbl1"  runat="server" Text='<%# Eval("Target") %>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="Project Potential"  ItemStyle-Width="100" ItemStyle-CssClass="valalign">
                                 <ItemTemplate>
                                    <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("Count") %>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="%" ItemStyle-Width="70" ItemStyle-CssClass="valalign">
                                 <ItemTemplate>
                                    <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("Value") %>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateField>
                           </columns>
                        </asp:GridView>


</ContentTemplate>
                                 <Triggers>
         <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
         <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
          <asp:AsyncPostBackTrigger ControlID="ddlBranchList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="ddlSalesEngineerList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="filter" EventName="Click" />
                             </Triggers>
                             </asp:UpdatePanel>
                            <br /> 
                       
              </div>
                              
                  </div>	
                         </div>
            </div> 
                           <div class="portlet-body shadow">
                <div class="col-md-4">
    <div id="dt_dtOverduePrjcts_chartdiv" class="portlet-body "></div>
             
                 <div class="portlet-body shadow">
          
                     <div class="clearfix"></div>
                    <div class="portlet-body">
                     <asp:UpdatePanel runat="server">
                         <ContentTemplate>
                         <asp:GridView ID="GridViewOverdue" runat="server" AutoGenerateColumns="false" Style="margin-left:50px;margin-top:10px">
                           <columns>
                               <asp:TemplateField HeaderText=" " ItemStyle-Width="30">
                                 <ItemTemplate>
                                 <asp:Label ID="Image1" runat="server" Height = "10" CssClass="square" style="background:#399BFF;"  Visible = '<%# Eval("Target").ToString() == "Stage 1" ? true : false %>' />
                                 <asp:Label ID="Label2" runat="server" Height = "10" CssClass="square" style="background:#fdd400;"  Visible = '<%# Eval("Target").ToString() == "Stage 2" ? true : false %>' />
                                 <asp:Label ID="Label3" runat="server" Height = "10" CssClass="square" style="background:#84b761 ;"  Visible = '<%# Eval("Target").ToString() == "Stage 3" ? true : false %>' />
                                 <asp:Label ID="Label4" runat="server" Height = "10" CssClass="square" style="background:#cc4748 ;"  Visible = '<%# Eval("Target").ToString() == "Stage 4" ? true : false %>' />
                                 <asp:Label ID="Label5" runat="server" Height = "10" CssClass="square" style="background:#cd82ad;"  Visible = '<%# Eval("Target").ToString() == "Stage 5" ? true : false %>' />
                                 <asp:Label ID="Label6" runat="server" Height = "10" CssClass="square" style="background:#C7BAA7 ;"  Visible = '<%# Eval("Target").ToString() == "Stage 6" ? true : false %>' />
                                 <asp:Label ID="Label7" runat="server" Height = "10" CssClass="square" style="background:#FAA43A ;"  Visible = '<%# Eval("Target").ToString() == "Stage 7" ? true : false %>' />
                                 <asp:Label ID="Label8" runat="server" Height = "10" CssClass="square" style="background:#5DA5DA ;"  Visible = '<%# Eval("Target").ToString() == "Stage 8" ? true : false %>' />
                                 <asp:Label ID="Label9" runat="server" Height = "10" CssClass="square" style="background:#4D4D4D ;"  Visible = '<%# Eval("Target").ToString() == "Stage 9" ? true : false %>' />
                                 <asp:Label ID="Label10" runat="server" Height = "10" CssClass="square" style="background:#FF00FF;"  Visible = '<%# Eval("Target").ToString() == "Stage 10" ? true : false %>' />

                                 </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="Stage" ItemStyle-Width="90">
                                 <ItemTemplate>
                                    <asp:Label  ID="lbl1" style="text-align:right" runat="server" Text='<%# Eval("Target") %>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="Project Potential"  ItemStyle-Width="100" ItemStyle-CssClass="valalign">
                                 <ItemTemplate>
                                    <asp:Label  ID="lbl1" style="text-align:right" runat="server" Text='<%# Eval("Count") %>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="%" ItemStyle-Width="70" ItemStyle-CssClass="valalign">
                                 <ItemTemplate>
                                    <asp:Label  ID="lbl1" style="text-align:right" runat="server" Text='<%# Eval("Value") %>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateField>
                           </columns>
                        </asp:GridView>
                             </ContentTemplate>
                          <Triggers>
         <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
         <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
          <asp:AsyncPostBackTrigger ControlID="ddlBranchList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="ddlSalesEngineerList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="filter" EventName="Click" />

      </Triggers>
                              </asp:UpdatePanel>
       
                  </div>
                    </div>
                       </div>
              
                     <div class="portlet-body shadow">
                <div class="col-md-4">
    <div id="dt_trgtd_cmpltn_prjcts_chartdiv" class="portlet-body" > </div>
                    <div>
                           <div class="portlet-body shadow">
                               <div class="clearfix"></div>
                               <asp:UpdatePanel runat="server">
                                   <ContentTemplate>                  
                                          <asp:GridView ID="gridtrgtcmpltnprjcts" runat="server" AutoGenerateColumns="false" Style="margin-top:10px;margin-left:55px" >

                                                <columns>
                             <asp:TemplateField HeaderText=" " ItemStyle-Width="30">
                                 <ItemTemplate>
                                     
                                 <asp:Label ID="Image1" runat="server" Height = "10" CssClass="square" style="background:#399BFF;"  Visible = '<%# Eval("Target").ToString() == " <30 Days"? true : false %>' />
                                 <asp:Label ID="Label2" runat="server" Height = "10" CssClass="square" style="background:#fdd400;"  Visible = '<%# Eval("Target").ToString() == "31-60" ? true : false %>' />
                                 <asp:Label ID="Label3" runat="server" Height = "10" CssClass="square" style="background:#84b761 ;"  Visible = '<%# Eval("Target").ToString() == "61-90" ? true : false %>' />
                                 <asp:Label ID="Label4" runat="server" Height = "10" CssClass="square" style="background:#cc4748 ;"  Visible = '<%# Eval("Target").ToString() == "91-180" ? true : false %>' />
                                 <asp:Label ID="Label5" runat="server" Height = "10" CssClass="square" style="background:#cd82ad ;"  Visible = '<%# Eval("Target").ToString() == ">180" ? true : false %>' />
                                
                                 </ItemTemplate>
                              </asp:TemplateField>
                           <asp:TemplateField HeaderText="Target" ItemStyle-Width="90"  ControlStyle-CssClass="hdrtxt"  >
                              <ItemTemplate >
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("Target") %>' ></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Project Potential" ItemStyle-Width="100" ItemStyle-CssClass="valalign">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("Count") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="%" ItemStyle-Width="70" ItemStyle-CssClass="valalign">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("Value") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                        </columns>
                     </asp:GridView>
                                       </ContentTemplate>
                                   <Triggers>
                                       <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
         <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
          <asp:AsyncPostBackTrigger ControlID="ddlBranchList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="ddlSalesEngineerList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="filter" EventName="Click" />
                                   </Triggers>
                               </asp:UpdatePanel>   
                               
                               </div>
                    </div>
                    </div>
                  </div>

               </div>

                   </div>
         
          </div>
     <%-- </ContentTemplate>
         <Triggers>
         <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
         <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
          <asp:AsyncPostBackTrigger ControlID="ddlBranchList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="ddlSalesEngineerList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="filter" EventName="Click" />

      </Triggers>
     </asp:UpdatePanel>--%>
                       <div class="clearfix"></div>
   <div class="col-md-12" style="margin:20px">
      <div class="col-md-5"></div>
      <div class="col-md-2">
         <asp:HyperLink ID="HyperLink1" NavigateUrl="~/MDPEntry.aspx?MDPEntry" style="  width: 150px; margin-left: 15px; border-radius: 5px !important;" class="btn green"  runat="server">NEXT</asp:HyperLink>

      </div>
       
      <div class="col-md-5"></div>
   </div> 
          <asp:UpdateProgress id="updateProgress" runat="server">
      <ProgressTemplate>
         <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color:#fff" >Please wait</span>
         </div>
      </ProgressTemplate>
   </asp:UpdateProgress>


       
</asp:Content>  