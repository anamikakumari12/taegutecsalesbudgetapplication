﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteStatusDetails.aspx.cs" Inherits="TaegutecSalesBudget.QuoteStatusDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/respond.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
     <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" /> 
    <link rel="stylesheet" type="text/css" href="fonts/fsquere/style.css" />
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="fonts/open-sans/open-sans.css" />    
    <link rel="stylesheet" type="text/css" href="css/footable.core.css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/style_default.css" rel="stylesheet" type="text/css" />
    <title></title>
    <style>
        .title
		{
			margin: 5px 0;
			width: 100%;
			font-size: large;
			font-weight: bold;
			padding: 10px;
			display: block;
			text-align: center;
            color:white;
		}
        .subtitle
		{
			margin: 5px 0;
			width: 100%;
			font-size: large;
			font-weight: bold;
			display: block;
			text-align: center;
            color:white;
		}
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            debugger;
            var head_content = $('#grdStatusLog tr:first').html();
            $('#grdStatusLog').prepend('<thead></thead>')
            $('#grdStatusLog thead').html('<tr>' + head_content + '</tr>');
            $('#grdStatusLog tbody tr:first').hide();
            var table = $('#grdStatusLog').dataTable({
                "order": [[1, 'desc']],
                "paging": false,
                "searching": false,
                "bInfo": false,
                "bSort": false
            });
        });
        </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style="background: #006780;">
			<asp:Label ID="lblTitle" CssClass="title" runat="server"></asp:Label>
            <asp:Label ID="lblSubTitle" CssClass="subtitle" runat="server"></asp:Label>
		</div>
    <asp:Label ID="lblmsg" ForeColor="Red" runat="server"></asp:Label>
         <asp:GridView ID="grdStatusLog" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <%--<asp:TemplateField HeaderText="Reference Number">
                            <ItemTemplate>
                                <asp:Label ID="lnkRef" CssClass="link" Text='<%# Bind("Ref_Number")%>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item Code">
                            <ItemTemplate>
                                <asp:Label ID="lblItem" Text='<%# Bind("Item_code")%>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                         <asp:TemplateField HeaderText="Status Updated By">
                            <ItemTemplate>
                                <asp:Label ID="lblItem" Text='<%# Bind("StatusChangeBy_Name")%>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Status Update Comment">
                            <ItemTemplate>
                                <asp:Label ID="lblItem" Text='<%# Bind("StatusChange_Comment")%>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Status update date">
                            <ItemTemplate>
                                <asp:Label ID="lblItem" Text='<%# Bind("StatusChangeDate")%>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" Text='<%# Bind("Status")%>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
    </div>
    </form>
</body>
</html>

