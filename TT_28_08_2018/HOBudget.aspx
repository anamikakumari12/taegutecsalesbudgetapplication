﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HOBudget.aspx.cs" Inherits="TaegutecSalesBudget.WebForm4" Async="true" ViewStateMode="Disabled" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="GridviewScroll.css" rel="stylesheet" />
    <script type="text/javascript" src="gridscroll.js"></script>
    <script type="text/javascript" src="js/app.js"></script>

    <style type="text/css">
        td
        {
            height: 45px !important;
        }
        /*body{
        overflow:hidden !important;
        }*/
        .noclose .ui-dialog-titlebar-close
        {
            display: none;
        }

        .ui-dialog .ui-dialog-titlebar
        {
            padding-left: 45px;
            text-align: center !important;
        }
        /*.col-md-4 {
         width: 33.3333% !important;
        }*/

        #MainContent_grdviewAllValues_Image1_0
        {
            display: none;
        }

        /*#MainContent_grdviewAllValues td
        {
            text-align: right;
        }*/

        #MainContent_rbtnlistComp td
        {
            height: 0px !important;
            padding: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="crumbs">
        <!-- Start : Breadcrumbs -->
        <ul id="breadcrumbs" class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a>Budget</a>
            </li>
            <li class="current">Entry</li>



        </ul>
    </div>

    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360">
    </asp:ScriptManager>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hdnCustomerBudgetStatus" />
            <div id="collapsedropdwns" class="row" style="cursor: pointer">
                <img id="Imgcollapsedropdwns" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />

            </div>

            <div id="MainContent_reportdrpdwns" class="row filter_panel ">
                <div id="divCter" runat="server">
                    <ul class="btn-info rbtn_panel">
                        <li>
                            <asp:RadioButtonList ID="rbtnlistComp" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtnlistComp_SelectedIndexChanged" EnableViewState="true" ViewStateMode="Enabled" RepeatDirection="Horizontal" class="btn-info rbtn_panel">
                                <asp:ListItem Text="TAEGUTEC" Value="TTA" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="DURACARB" Value="DUR"></asp:ListItem>
                            </asp:RadioButtonList>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label">CUSTOMER NAME </label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlCustomerList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged"
                                CssClass="form-control" Width="230px" ViewStateMode="Enabled">
                                <asp:ListItem>SELECT CUSTOMER</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label">CUSTOMER NUMBER </label>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlCustomerNumber" runat="server" AutoPostBack="True" CssClass="form-control" Width="230px" ViewStateMode="Enabled">
                                <asp:ListItem>--SELECT CUSTOMER NUMBER--</asp:ListItem>
                            </asp:DropDownList>
                            <%--<asp:TextBox runat="server" ID="txtCustomerNumber" class="form-control-input"  Width="230px" ViewStateMode="Enabled" CssClass="form-control"></asp:TextBox>--%>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="form-group">
                        <label class="col-md-4 control-label ">BUDGET INCREASE(%)</label>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtBudgetInc" runat="server" class="form-control-input" Width="230px" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

            <div style="float: left; width: 100% !Important; margin-top: 13px; margin-left: 5px;">
                <div class="col-md-6 ">
                    <asp:Label runat="server" ID="lbl_MSG" Style="font-weight: 800"></asp:Label>
                </div>
                <div class="col-md-6 " style="padding-right: 17px;">

                    <asp:Panel runat="server" ID="panelButtons" Visible="false">
                        <asp:Button runat="server" ID="btnReview" Text="REVIEW" OnClick="btnReview_Click" CssClass="btn green" Style="margin-left: 20px; float: right;" />
                        <asp:Button runat="server" ID="btnSave" Text="SAVE " OnClick="btnSave_Click" CssClass="btn green" Style="margin-left: 20px; float: right;" />
                        <asp:Button runat="server" ID="btnSubmitforHigherApproval" Text="APPROVE" OnClick="btnSubmitforHigherApproval_Click" CssClass="btn green" Style="margin-left: 20px; float: right;" />

                    </asp:Panel>
                </div>
            </div>


            <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
                <%--     CssClass="table table-bordered table-hover responsive" --%>
                <asp:GridView ID="grdviewAllValues" runat="server" ViewStateMode="Enabled" AutoGenerateColumns="False" OnRowDataBound="grdviewAllValues_RowDataBound" ShowHeader="false" Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/button_plus.gif" Visible='<%# (Eval("sumFlag").ToString() == "SubFamilyHeading") || (Eval("sumFlag").ToString() == "FamilyHeading") || (Eval("sumFlag").ToString() == "HidingHeading") %>' ImageAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="GOLD">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblgold" Text='<%#( Eval("gold_flag").ToString()=="" ?"" :"GOLD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                     <%--   <asp:TemplateField HeaderText="TOP">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbltop" Text='<%# (Eval("top_flag").ToString()=="" ?"" :"TOP") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                       <%-- <asp:TemplateField HeaderText="5yrs">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl5yrs" Text='<%# (Eval("five_years_flag").ToString()=="" ?"" :"5yrs") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="BB">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblbb" Text='<%# (Eval("bb_flag").ToString()=="" ?"" :"BB") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    <%--    <asp:TemplateField HeaderText="SPC">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblspc" Text='<%#( Eval("SPC_flag").ToString()=="" ?"" :"SPC") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="10yrs">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl10y" Text='<%#( Eval("ten_years_flag").ToString()=="" ?"" :"10yrs") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SFEED">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblsfeed" Text='<%#( Eval("SFEED_flag").ToString()=="" ?"" :"SFEED") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="1" ItemStyle-Width="35px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ins" Text="Ins." Visible='<%# (Eval("insert_or_tool_flag").ToString() =="I" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="lbl_tool" Text="P" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="P" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label1" Text="S" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="S" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label2" Text="Tools" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="T" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label3" Text="X" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="X" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label4" Text="" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label5" Text='<%# (Eval("item_sub_family_id").ToString()) %>' Visible='<%# (Eval("insert_or_tool_flag").ToString() =="SubFamHeading") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblproductcode" Text='<%# Eval("item_code".ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="T_CLAMP_PARTING_OFF ffffffffffffffffffffffff" ItemStyle-Width="240px"><%--HeaderStyle-CssClass="greendark">--%>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblFmilyName" Text='<%# (Eval("item_family_name").ToString()) %>' Visible='<%#Eval("sumFlag").ToString() == "FamilyHeading" || Eval("sumFlag").ToString() == "HidingHeading"  %>' CssClass="productLabel"></asp:Label>
                                <asp:Label runat="server" ID="lblProductName" Text='<%# (Eval("item_description").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblSumFlag" runat="server" Text='<%# Eval("sumFlag").ToString()%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="color_3" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_item_code" Text='<%# (Eval("item_code").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--Actual Quality--%>
                         <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="60px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualQuantity_sales_qty_year_2" Text='<%# (Eval("sales_qty_year_2").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="60px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualQuantity_sales_qty_year_1" Text='<%# (Eval("sales_qty_year_1").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="60px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualQuantity_sales_qty_year_0" Text='<%# (Eval("sales_qty_year_0").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_ActualQuantity_sales_qty_year_0" Value='<%# (Eval("sales_qty_year_1").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txt_ActualQuantity_sales_qty_year_P"  MaxLength="6" Text='<%# Eval("estimate_qty_next_year") %>'
                                    Visible='<%# Eval("sumFlag").ToString() != "SubFamilyHeading" && Eval("sumFlag").ToString() != "FamilyHeading" &&Eval("sumFlag").ToString() != "HidingHeading" && Eval("sumFlag").ToString() != ""%>' Width="90px"
                                    Enabled='<%# Eval("sumFlag").ToString() !="typeSum" &&  Eval("sumFlag").ToString() !="SubFamilySum"  &&  Eval("sumFlag").ToString() !="FamilySum" &&  Eval("sumFlag").ToString() !="MainSum" %>'
                                    onkeypress="return isNumberKey(event)"></asp:TextBox>
                                <asp:Label runat="server" ID="lbl_ActualQuantity_sales_qty_year_P" Text='<%# Eval("estimate_qty_next_year") %>' Visible='<%# Eval("sumFlag").ToString() == "SubFamilyHeading" || (Eval("sumFlag").ToString() == "HidingHeading") %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="checkbox_ActualQuantity_sales_qty_year_P" Visible='<%# Eval("sumFlag").ToString() != "SubFamilyHeading" && Eval("sumFlag").ToString() != "FamilyHeading"  && Eval("sumFlag").ToString() != "HidingHeading" && Eval("sumFlag").ToString() != "" && Eval("sumFlag").ToString() != "typeSum" && Eval("sumFlag").ToString() != "SubFamilySum" && Eval("sumFlag").ToString() != "FamilySum"%>'
                                    Checked='<%# Eval("review_flag").ToString() == "Q" || Eval("review_flag").ToString() == "QV"%>' Enabled="true" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <%-- Inserts per tool --%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_insertpertool_year_1" Text='<%# (Eval("insertpertool_year_1").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_insertpertool_year_1" Value='<%# (Eval("insertpertool_year_1").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                       <%-- <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_insertpertool_year_0" Text='<%# (Eval("insertpertool_year_0").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_insertpertool_year_0" Value='<%# (Eval("insertpertool_year_0").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_insertpertool_next_year" Text='<%# (Eval("insertpertool_next_year").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_insertpertool_next_year" Value='<%# (Eval("insertpertool_next_year").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--Variance of Quality--%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_QuantityVariance_sales_qty_year_1" Text='<%# Eval("QuantityVariance_sales_qty_year_1")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_QuantityVariance_sales_qty_year_0" Text='<%# Eval("QuantityVariance_sales_qty_year_0").ToString() %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--Percentage of Quantiity--%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_QuantityPercentage_sales_qty_year_1" Text='<%# Eval("QuantityPercentage_sales_qty_year_1") %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_QuantityPercentage_sales_qty_year_0" Text='<%# Eval("QuantityPercentage_sales_qty_year_0").ToString() %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- Actual Value--%>
                         <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_2" Text='<%# (Eval("sales_value_year_2").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_1" Text='<%# (Eval("sales_value_year_1").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_0" Text='<%# (Eval("sales_value_year_0").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_ActualValue_sales_value_year_0" Value='<%# (Eval("sales_value_year_1").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_P_New" Text='<%# Eval("estimate_value_next_year") %>'
                                    Visible='<%# Eval("sumFlag").ToString() != "SubFamilyHeading" && Eval("sumFlag").ToString() != "FamilyHeading" && Eval("sumFlag").ToString() != "" && Eval("sumFlag").ToString() != "HidingHeading" %>' Width="90px"></asp:Label>
                                <asp:TextBox runat="server" ID="txt_ActualValue_sales_value_year_P" CssClass="form-control valuebud" MaxLength="28" Text='<%# Eval("estimate_value_next_year") %>' Style="display: none"
                                    Enabled='<%# Eval("sumFlag").ToString() !="typeSum" &&  Eval("sumFlag").ToString() !="SubFamilySum"  &&  Eval("sumFlag").ToString() !="FamilySum" &&  Eval("sumFlag").ToString() !="MainSum" %>'
                                    onkeypress="return isNumberKey(event)"></asp:TextBox>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_P" Text='<%# (Eval("estimate_value_next_year").ToString()) %>' Visible='<%# Eval("sumFlag").ToString() == "SubFamilyHeading" || (Eval("sumFlag").ToString() == "HidingHeading") %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- Percentage Value--%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ValuePercentage_sales_value_year_1" Text='<%# Eval("ValuePercentage_sales_value_year_1")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ValuePercentage_sales_value_year_0" Text='<%# Eval("ValuePercentage_sales_value_year_0")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- Price per Unit --%>
                         <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_PricePerUnit_sales_value_year_2" Text='<%#Eval("PricePerUnit_sales_value_year_2") %>'>  </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_PricePerUnit_sales_value_year_1" Text='<%#Eval("PricePerUnit_sales_value_year_1") %>'>  </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_PricePerUnit_sales_value_year_0" Text='<%#Eval("PricePerUnit_sales_value_year_0") %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_PricePerUnit_sales_value_year_0" Value='<%#Eval("PricePerUnit_sales_value_year_1") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txt_PricePerUnit_sales_value_year_P" CssClass="form-control valuebud" MaxLength="28" Text='<%# Eval("estimate_rate_next_year") %>'
                                    Visible='<%# Eval("sumFlag").ToString() != "SubFamilyHeading" && Eval("sumFlag").ToString() != "FamilyHeading" && Eval("sumFlag").ToString() != "" && Eval("sumFlag").ToString() != "HidingHeading" %>' Width="90px"
                                    Enabled='<%# Eval("sumFlag").ToString() !="typeSum" &&  Eval("sumFlag").ToString() !="SubFamilySum"  &&  Eval("sumFlag").ToString() !="FamilySum" &&  Eval("sumFlag").ToString() !="MainSum" && Eval("SFEED_flag").ToString()=="" %>'
                                    onkeypress="return isNumberKey(event)"></asp:TextBox>
                                <asp:Label runat="server" ID="lbl_PricePerUnit_sales_value_year_P" Text='<%#Eval("PricePerUnit_sales_value_year_P") %>' Visible='<%# Eval("sumFlag").ToString() == "SubFamilyHeading" || (Eval("sumFlag").ToString() == "HidingHeading") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- Price Changes --%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" Visible="false">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_priceschange_sales_value_year_2" Text='<%#Eval("priceschange_sales_value_year_2") %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_priceschange_sales_value_year_1" Text='<%#Eval("priceschange_sales_value_year_1") %>'>  </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_priceschange_sales_value_year_0" Text='<%#Eval("priceschange_sales_value_year_0") %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_priceschange_sales_value_year_0" Value='<%#Eval("priceschange_sales_value_year_1") %>' />
                                <%--Hidden values--%>
                                <asp:Label runat="server" ID="lbl_Family_Id" Text='<%# Eval("item_family_id").ToString() %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lbl_Sub_Family_Id" Text='<%# Eval("item_sub_family_id").ToString() %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lbl_customer_number" Text='<%# Eval("customer_number").ToString() %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lblItemCode" Text='<%# (Eval("item_code").ToString()) %>' Style="display: none"> </asp:Label>
                                <asp:Label runat="server" ID="lbl_SumFlag" Text='<%# Eval("sumFlag").ToString()%>' Style="display: none" />
                                <asp:Label runat="server" ID="lbl_BudgetId" Text='<%# Eval("budget_id").ToString()%>' Style="display: none" />
                                <asp:HiddenField runat="server" ID="hdn_SumFlag" Value='<%# Eval("sumFlag").ToString()%>' />
                                <asp:Label runat="server" ID="lbl_ReviewFlag" Text='<%# Eval("review_flag").ToString()%>' Style="display: none" />
                                <asp:Label runat="server" ID="lbl_BM_flag" Text='<%# Eval("status_ho").ToString()%>' Style="display: none" />
                                <asp:HiddenField runat="server" ID="hdn_BM_flag" Value='<%# Eval("status_ho").ToString()%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>


            <div id="dvResultPopUp" style="width: 100%; display: none; text-align: center !important;">
                <br />

                <asp:Label runat="server" ID="lblResult" Style="font-weight: 800;"></asp:Label>

                <br />
                <br />
                <div>
                    <asp:Button ID="btnLogoff" runat="server" Text=" Log Out " OnClientClick=" javascript:closePopUpLogoff();" CssClass="btn green" Style="width: 130px; font-weight: bolder; margin-left: 20px; float: right;" OnClick="btnLogoff_Click" />

                    <asp:Button ID="btnNewCustomer" runat="server" Text="New Customer" OnClientClick=" javascript:closePopUpNewCust();" CssClass="btn green" Style="width: 130px; font-weight: bolder; margin-left: 20px; float: right;" OnClick="btnNewCustomer_Click" />

                    <asp:Button ID="btnContinue" runat="server" Text="Continue" OnClientClick=" javascript:closePopUpContinue();" CssClass="btn green" Style="width: 130px; font-weight: bolder; margin-left: 20px; float: right;" OnClick="btnContinue_Click" />
                </div>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlCustomerList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnReview" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSubmitforHigherApproval" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnContinue" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnNewCustomer" EventName="Click" />
            <asp:PostBackTrigger ControlID="btnLogoff" />
            <asp:AsyncPostBackTrigger ControlID="rbtnlistComp" EventName="SelectedIndexChanged" />
        </Triggers>



    </asp:UpdatePanel>



    <%--  <a style="display: inline;" class="scrollup" href="javascript:void(0);">Scroll</a>--%>


    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>



    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            if (args.get_error() != undefined) {
                args.set_errorHandled(true);
            }
        }

        $(document).ready(function () {
            debugger;
            triggerPostGridLodedActions();
            $(window).resize(function () {
                triggerPostGridLodedActions();
            });
            $('#MainContent_ddlCustomerList').change(function () {
                var ddlslectedText = $("#MainContent_ddlCustomerList option:selected").val();
                $("#MainContent_ddlCustomerNumber").val(ddlslectedText);
            });
            $('#MainContent_ddlCustomerNumber').change(function () {

                var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
                $("#MainContent_ddlCustomerList").val(ddlslectedText);
            });
            $('#Imgcollapsedropdwns').unbind('click').bind('click', function (e) {
                var attr = $('#Imgcollapsedropdwns').attr('src');
                $("#MainContent_reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#Imgcollapsedropdwns").attr("src", "images/down_arrow.png");
                } else {
                    $("#Imgcollapsedropdwns").attr("src", "images/up_arrow.png");
                }
            });
        });
        var divPopUp;
        function DivPopUpOpen() {
            dclg = $("#dvResultPopUp").dialog(
                       {
                           resizable: false,
                           draggable: true,
                           modal: true,
                           title: "Select to Proceed",
                           width: "500",
                           //height: "150",
                           closeOnEscape: false,
                           //beforeClose: function (event, ui) { return false; },
                           dialogClass: "noclose",

                       });
            divPopUp = dclg;
            dclg.parent().appendTo(jQuery("form:first"));
        }

        function closePopUpLogoff() {
            divPopUp.dialog('close');
            //
            // document.getElementById('<%= btnLogoff.ClientID %>').click();


        }
        function closePopUpNewCust() {

            divPopUp.dialog('close');
            triggerPostGridLodedActions();
            //document.getElementById('<%= btnNewCustomer.ClientID %>').click();

        }
        function closePopUpContinue() {

            divPopUp.dialog('close');
            triggerPostGridLodedActions();
            //document.getElementById('<%= btnContinue.ClientID %>').click();


        }

        function triggerPostGridLodedActions() {
            debugger;
            $('#Imgcollapsedropdwns').unbind('click').bind('click', function (e) {
                var attr = $('#Imgcollapsedropdwns').attr('src');
                $("#MainContent_reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#Imgcollapsedropdwns").attr("src", "images/down_arrow.png");
                } else {
                    $("#Imgcollapsedropdwns").attr("src", "images/up_arrow.png");
                }
            });
            //$("#MainContent_ddlCustomerList").searchable();
            //$("#MainContent_ddlCustomerNumber").searchable();

            //$('#MainContent_ddlCustomerNumber').change(function () {

            //    var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
            //    $("#MainContent_ddlCustomerList").val(ddlslectedText);
            //});
            $('#MainContent_ddlCustomerList').change(function () {
                var ddlslectedText = $("#MainContent_ddlCustomerList option:selected").val();
                $("#MainContent_ddlCustomerNumber").val(ddlslectedText);
            });
            $('#MainContent_ddlCustomerNumber').change(function () {

                var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
                $("#MainContent_ddlCustomerList").val(ddlslectedText);
            });
            gridviewScroll();
            bindToogleForRowIndex();
            bindToogleForParentRowIndex();

            $(function () {

                $("[id*=checkbox_ActualQuantity_sales_qty_year_P]").bind("click", function () {

                    //Find and reference the GridView.
                    var grid = $(this).closest("table");

                    //Find and reference the Header CheckBox.
                    var chkHeader = $("[id*=chkHeader]", grid);

                    //If the CheckBox is Checked then enable the TextBoxes in thr Row.
                    if (!$(this).is(":checked")) {
                        var td = $("td", $(this).closest("tr"));
                        //td.css({ "background-color": "#FFF" });
                        $("[id*=txt_ActualQuantity_sales_qty_year_P]", td).css({ "background-color": "#FFF", "color": "#555", "font-weight": "normal" });

                    } else {
                        var td = $("td", $(this).closest("tr"));
                        //td.css({ "background-color": "#D8EBF2" });
                        $("[id*=txt_ActualQuantity_sales_qty_year_P]", td).css({ "background-color": "#FF0000", "font-weight": "bolder", "color": "#FFF" });
                    }

                });
                /**
                * added by thangaraj.
                */
                function product_type_sub_total_change(element) {

                    var product_type = jQuery(element).data("product_type");
                    var product_type_value_sub_total = 0;
                    jQuery(".lblnextYearValue_" + product_type).each(function () {
                        product_type_value_sub_total = parseInt(product_type_value_sub_total) + parseInt(jQuery(this).html());
                    });
                    return product_type_value_sub_total;
                }

                /**
                *
                */
                function product_type_sub_family_total_change(element) {
                    debugger;
                    var product_type = jQuery(element).data("product_type_subfamily_total_index");
                    var product_type_value_sub_total = 0;
                    jQuery(".type_sum_lblnextYearValue_" + product_type).each(function () {
                        product_type_value_sub_total = parseInt(product_type_value_sub_total) + parseInt(jQuery(this).html());
                    });
                    jQuery(".subfamily_sum_lblnextYearValue_" + product_type).html(product_type_value_sub_total);
                    return product_type_value_sub_total;
                }

                /**
                *
                */
                $("[id*=txt_ActualQuantity_sales_qty_year_P]").bind("change", function () {
                    var product_type_value_sub_total = 0;
                    if (jQuery(this).hasClass("product_type_sub_total")) {
                        product_type_value_sub_total = product_type_sub_total_change(this);
                    }
                    else if (jQuery(this).hasClass("product_type_SubFamilySum")) {
                        product_type_value_sub_total = product_type_sub_family_total_change(this);
                    }


                    //Find and reference the GridView.
                    var grid = $(this).closest("table");

                    var td = $("td", $(this).closest("tr"));
                    var BudQuantity = $("[id*=txt_ActualQuantity_sales_qty_year_P]", td).val(); //2015 Next Year
                    var ProQuantity = $("[id*=hdn_ActualQuantity_sales_qty_year_0]", td).val(); //2014 Current Year
                    var BudRate = $("[id*=txt_PricePerUnit_sales_value_year_P]", td).val(); //2015
                    var resultVariance = 0;
                    var ValueNextYear = 0;
                    if (ProQuantity != undefined && !isNaN(ProQuantity)) {
                        resultVariance = ProQuantity == "" ? parseInt(BudQuantity) : parseInt(BudQuantity) - parseInt(ProQuantity);
                    }
                    var resultPercentage = (ProQuantity == undefined || ProQuantity == "") ? 0 : (parseInt(BudQuantity) / parseInt(ProQuantity) - 1) * 100;
                    resultPercentage = Math.round(resultPercentage);

                    ///value for Next Year Calculation
                    var ValueNextYear = (BudQuantity == undefined || BudQuantity == "") ? 0 : BudQuantity * (BudRate != undefined && BudRate != "" ? parseInt(BudRate) : 0);
                    ValueNextYear = ValueNextYear != 0 ? ValueNextYear / 1000 : 0;
                    ValueNextYear = Math.round(ValueNextYear);

                    $("[id*=lbl_QuantityVariance_sales_qty_year_0]", td).html(resultVariance);
                    $("[id*=lbl_QuantityPercentage_sales_qty_year_0]", td).html(resultPercentage + "%");
                    if (jQuery(this).hasClass("product_type_sub_total") || jQuery(this).hasClass("product_type_SubFamilySum")) {
                        ValueNextYear = product_type_value_sub_total;
                        var txt_PricePerUnit_sales_value_year_P = Math.round((ValueNextYear * 1000) / BudQuantity);
                        $("[id*=txt_PricePerUnit_sales_value_year_P]", td).val(txt_PricePerUnit_sales_value_year_P);
                        BudRate = txt_PricePerUnit_sales_value_year_P
                    }
                    $("[id*=lbl_ActualValue_sales_value_year_P_New]", td).html(ValueNextYear);


                    //MainContent_grdviewAllValues_lbl_ValuePercentage_sales_value_year_0_

                    //$("[id*=hdn_ActualValue_sales_value_year_0]", td).val();
                    var current_year_value_inc = $("[id*=hdn_ActualValue_sales_value_year_0]", td).val();
                    var result_inc_next = 0;
                    if (current_year_value_inc != undefined && !isNaN(current_year_value_inc))
                    { result_inc_next = current_year_value_inc != 0 ? (ValueNextYear / current_year_value_inc - 1) * 100 : 0; }

                    result_inc_next = Math.round(result_inc_next);
                    $("[id*=lbl_ValuePercentage_sales_value_year_0]", td).html(result_inc_next + "%");


                    var resultPriceChange = 0;
                    var resultPriceperUnitCurrentYear = $("[id*=hdn_PricePerUnit_sales_value_year_0]", td).val();
                    if (BudRate != undefined && BudRate != "") {
                        resultPriceChange = resultPriceperUnitCurrentYear == 0 ? 0 : (BudRate / resultPriceperUnitCurrentYear - 1) * 100;
                    }
                    resultPriceChange = Math.round(resultPriceChange);
                    $("[id*=lbl_priceschange_sales_value_year_0]", td).html(resultPriceChange + "%");
                    //anamika
                    var gridViewCtl = null;
                    var curSelRow = null;
                    var curRowIdx = -1;
                    var branchcode = null;
                    var result;
                    var currentrow = this.parentNode.parentNode;
                    var rowIndex = currentrow.rowIndex - 1;
                    if (rowIndex != undefined && rowIndex != "" && !isNaN(rowIndex)) {
                        branchcode = currentrow.cells[7].innerText;
                    }
                    else {
                        branchcode = $("[id*=lbl_item_code]", td).text();
                    }
                    if (branchcode != "") {
                        var sales_qty_year = $("[id*=txt_ActualQuantity_sales_qty_year_P]", td).val();

                        if (sales_qty_year == undefined || sales_qty_year == "") {
                            sales_qty_year = "0";
                        }
                        gridViewCtl = document.getElementById('MainContent_grdviewAllValues');
                        var id = 0;
                        for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                            var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                            if (branchcode == Branchcode_1) {
                                id = i;
                                break;
                            }
                        }

                        //AA=AA/AH
                        if (branchcode == "AA") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "AH") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        gridViewCtl.rows[id].cells[15].innerText = result;

                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "AH") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "AA") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);

                                    }
                                    break;
                                }

                            }
                        }

                        //AC=AC/AJ
                        if (branchcode == "AC") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "AJ") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        gridViewCtl.rows[id].cells[15].innerText = result;

                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "AJ") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "AC") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);

                                    }
                                    break;
                                }

                            }
                        }
                        //s1=(s1+s2)/(s3+s4)
                        if (branchcode == "S1") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {

                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "S2") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "S3") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "S4") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year_2 == 0 && sales_qty_year_3 == 0) {
                                    result = "inifinity";
                                    $("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    break;
                                }

                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)));
                                    //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    gridViewCtl.rows[id].cells[15].innerText = result;
                                    break;
                                }



                            }
                        }
                        if (branchcode == "S2") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "S1") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "S3") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "S4") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "S3") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "S2") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "S1") {
                                    j = i;
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "S4") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year_2) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_3)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "S4") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "S2") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "S3") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "S1") {
                                    j = i;
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year_3) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        }
                        //B0=B0/(B6+B7)
                        if (branchcode == "B0") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "B6") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B7") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "") {
                                    result = Math.round((parseInt(sales_qty_year)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_1)));
                                    //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    gridViewCtl.rows[id].cells[15].innerText = result;
                                    break;

                                }



                            }
                        }
                        if (branchcode == "B6") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "B0") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B7") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "") {
                                    result = Math.round((parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        } if (branchcode == "B7") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "B6") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B0") {
                                    j = i;
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "") {
                                    result = Math.round((parseInt(sales_qty_year_2)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_1)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        }
                        //B3=(B1+B2+B3)/(B8+B9)
                        if (branchcode == "B3") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {

                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "B2") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B1") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B8") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B9") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2)) / (parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)));
                                    //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    gridViewCtl.rows[id].cells[15].innerText = result;
                                    break;
                                }



                            }
                        }
                        if (branchcode == "B2") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "B1") {

                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B3") {
                                    j = i;
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B8") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B9") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2)) / (parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "B1") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "B3") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B2") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B8") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B9") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2)) / (parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "B8") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "B2") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B3") {
                                    j = i;
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B1") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B9") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "") {
                                    result = Math.round((parseInt(sales_qty_year_3) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_4)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        }
                        if (branchcode == "B9") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "B2") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B3") {
                                    j = i;
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B8") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "B1") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "") {
                                    result = Math.round((parseInt(sales_qty_year_4) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_3)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        }
                        //FA=FA/FV
                        if (branchcode == "FA") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FV") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;

                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "FV") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FA") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);

                                    }
                                    break;
                                }

                            }
                        }
                        //FG=FG/FM
                        if (branchcode == "FG") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FM") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "FM") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FG") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);

                                    }
                                    break;
                                }

                            }
                        }

                        //FH=FH/FN
                        if (branchcode == "FH") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FN") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "FN") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FH") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                    }
                                    break;
                                }

                            }
                        }
                        //CB=(CB+CC+CK)/(CO+CQ)
                        if (branchcode == "CB") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {

                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "CC") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CK") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CO") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CQ") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2)) / (parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)));
                                    //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    gridViewCtl.rows[id].cells[15].innerText = result;
                                    break;
                                }



                            }
                        }
                        if (branchcode == "CC") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "CB") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CK") {

                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CO") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CQ") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2)) / (parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "CK") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "CB") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CC") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CO") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CQ") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2)) / (parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "CO") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "CB") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CC") {

                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CK") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CQ") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "") {
                                    result = Math.round((parseInt(sales_qty_year_3) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_4)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        }
                        if (branchcode == "CQ") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "CB") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CC") {

                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CK") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CO") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "") {
                                    result = Math.round((parseInt(sales_qty_year_3) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_4)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        }
                        //CA=CA/(CM+CN)
                        if (branchcode == "CA") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "CM") {
                                    debugger
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CN") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "") {
                                    result = Math.round((parseInt(sales_qty_year)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_1)));
                                    //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    gridViewCtl.rows[id].cells[15].innerText = result;
                                    break;

                                }



                            }
                        }
                        if (branchcode == "CM") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "CA") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CN") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "") {
                                    result = Math.round((parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        } if (branchcode == "CN") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "CM") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "CA") {
                                    j = i;
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "") {
                                    result = Math.round((sales_qty_year_2) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_1)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        }
                        //CE=CE/CR
                        if (branchcode == "CE") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "CR") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        // $("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "CR") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "CE") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    }
                                    break;
                                }

                            }
                        }
                        //CF=CF/CS
                        if (branchcode == "CF") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "CS") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(sales_qty_year / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "CS") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "CF") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                    }
                                    break;
                                }

                            }
                        }
                        //GA=GA/GJ
                        if (branchcode == "GA") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "GJ") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "GJ") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "GA") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);

                                    }
                                    break;
                                }

                            }
                        }

                        //GE=GE/GU
                        if (branchcode == "GE") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "GU") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "GU") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "GE") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    }
                                    break;
                                }

                            }
                        }

                        //GP=GP/(G0+G4+G3)
                        if (branchcode == "GP") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {

                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "G0") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "G3") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "G4") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_1)));
                                    //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    gridViewCtl.rows[id].cells[15].innerText = result;
                                    break;
                                }



                            }
                        }
                        if (branchcode == "G0") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "GP") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "G3") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "G4") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "G3") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "GP") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "G0") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "G4") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "G4") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "GP") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "G3") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "G0") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }

                        //GF=GF/G9
                        if (branchcode == "GF") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "G9") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "G9") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "GF") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    }
                                    break;
                                }

                            }
                        }

                        //IA=(IA+IG)/(IR+IW)
                        if (branchcode == "IA") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {

                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "IG") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IR") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IW") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)));
                                    //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    gridViewCtl.rows[id].cells[15].innerText = result;
                                    break;
                                }



                            }
                        }
                        if (branchcode == "IG") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "IA") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IR") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IW") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "IR") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "IG") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IA") {
                                    j = i;
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IW") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year_2) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_3)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "IW") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "IG") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IR") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IA") {
                                    j = i;
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year_3) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        }

                        //IB=(IB+IJ)/(IS+IX)
                        if (branchcode == "IB") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {

                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "IJ") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IS") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IX") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)));
                                    //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    gridViewCtl.rows[id].cells[15].innerText = result;
                                    break;
                                }



                            }
                        }
                        if (branchcode == "IJ") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "IB") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IS") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IX") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "IS") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "IJ") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IB") {
                                    j = i;
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IX") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year_2) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_3)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "IX") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "IJ") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IS") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "IB") {
                                    j = i;
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year_3) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        }

                        //KH=KH/KK
                        if (branchcode == "KH") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "KK") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        // $("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "KK") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "KH") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;

                                    }
                                    break;
                                }

                            }
                        }
                        //K9=K9/K8
                        if (branchcode == "K9") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "K8") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;

                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "K8") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "K9") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;

                                    }
                                    break;
                                }

                            }
                        }
                        //KD=KD/KN
                        if (branchcode == "KD") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "KN") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "KN") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "KD") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //LA=LA/(LP+LQ+LR)
                        if (branchcode == "LA") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {

                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "LP") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "LQ") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "LR") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_1)));
                                    //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    gridViewCtl.rows[id].cells[15].innerText = result;
                                    break;
                                }



                            }
                        }
                        if (branchcode == "LP") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "LA") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "LQ") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "LR") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "LQ") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "LA") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "LP") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "LR") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "LR") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "LA") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "LP") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "LQ") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "") {
                                    result = Math.round((parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }

                        //LL=LL/L8
                        if (branchcode == "LL") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "L8") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "L8") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "LL") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //MX=MX/M8
                        if (branchcode == "MX") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "M8") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "M8") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "MX") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //MY=MY/M9
                        if (branchcode == "MY") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "M9") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "M9") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "MY") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }


                        //LD=LD/(LY+LZ)
                        if (branchcode == "LD") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "LY") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "LZ") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "") {
                                    result = Math.round((parseInt(sales_qty_year)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year_1)));
                                    //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    gridViewCtl.rows[id].cells[15].innerText = result;
                                    break;

                                }



                            }
                        }
                        if (branchcode == "LY") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "LD") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "LZ") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "") {
                                    result = Math.round((parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        } if (branchcode == "LZ") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "LY") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "LD") {
                                    j = i;
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "") {
                                    result = Math.round((parseInt(sales_qty_year_2)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_1)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        }
                        //LM=LM/L9
                        if (branchcode == "LM") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "L9") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "L9") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "LM") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //LE=LE/L3
                        if (branchcode == "LE") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "L3") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "L3") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "LE") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //MD=MD/MK
                        if (branchcode == "MD") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "MK") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "MK") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "MD") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //MG=MG/MP
                        if (branchcode == "MG") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "MP") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "MP") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "MG") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //P2=P2/P6
                        if (branchcode == "P2") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "P6") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "P6") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "P2") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        //R1=R1/R6
                        if (branchcode == "R1") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "R6") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "R6") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "R1") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //RX=(RX+RY)/RZ
                        if (branchcode == "RX") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "RY") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "RZ") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year_2)));
                                    //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                    gridViewCtl.rows[id].cells[15].innerText = result;
                                    break;

                                }



                            }
                        }
                        if (branchcode == "RY") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "RX") {
                                    j = i;
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "RZ") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "") {
                                    result = Math.round((parseInt(sales_qty_year_1) + parseInt(sales_qty_year)) / (parseInt(sales_qty_year_2)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        } if (branchcode == "RZ") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var j;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "RY") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "RX") {
                                    j = i;
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "") {
                                    result = Math.round((parseInt(sales_qty_year_2) + parseInt(sales_qty_year_1)) / (parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }

                                }



                            }
                        }

                        //SA=SA/SJ
                        if (branchcode == "SA") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "SJ") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "SJ") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "SA") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //R2=R2/R7
                        if (branchcode == "R2") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "R7") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "R7") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "R2") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //R3=R3/R8
                        if (branchcode == "R3") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "R8") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "R8") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "R3") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        //ST=ST/SX
                        if (branchcode == "ST") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "SX") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "SX") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "ST") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //SS=SS/SW
                        if (branchcode == "SS") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "SW") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "SW") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "SS") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //SE=SE/SQ
                        if (branchcode == "SE") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "SQ") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "SQ") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "SE") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //RW=RW/RV
                        if (branchcode == "RW") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "RV") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        // $("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "RV") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "RW") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //RP=RP/RQ
                        if (branchcode == "RP") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "RQ") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        gridViewCtl.rows[id].cells[15].innerText = result;
                                        break;
                                    }
                                    break;
                                }

                            }
                        }
                        if (branchcode == "RQ") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "RP") {
                                    if (sales_qty_year_1 == undefined || sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                    if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                        result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                        gridViewCtl.rows[i].cells[15].innerText = result;
                                        //$("[id*=lbl_insertpertool_next_year]", td).html(result);
                                        break;
                                    }
                                    break;
                                }

                            }
                        }

                        //TOTAL T-CLAMP GROOVE INSERTS=(BA+BC+BD+BE+BG+BF)/(BK+BL+BP+BS)
                        if (branchcode == "BA") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL T-CLAMP GROOVE INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "BC") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BD") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BG") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BF") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BK") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BL") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BP") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BS") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5)) / (parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "BC") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL T-CLAMP GROOVE INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "BA") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BD") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BG") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BF") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BK") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BL") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BP") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BS") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5)) / (parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "BD") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL T-CLAMP GROOVE INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "BC") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BA") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BG") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BF") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BK") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BL") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BP") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BS") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5)) / (parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "BE") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL T-CLAMP GROOVE INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "BC") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BD") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BA") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BG") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BF") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BK") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BL") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BP") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BS") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5)) / (parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "BG") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL T-CLAMP GROOVE INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "BC") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BD") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BA") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BF") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BK") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BL") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BP") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BS") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5)) / (parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "BF") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL T-CLAMP GROOVE INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "BC") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BD") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BG") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BA") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BK") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BL") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BP") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BS") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5)) / (parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "BK") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL T-CLAMP GROOVE INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "BC") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BD") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BG") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BF") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BA") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BL") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BP") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BS") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year_6) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "BL") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL T-CLAMP GROOVE INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "BC") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BD") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BG") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BF") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BK") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BA") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BP") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BS") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year_7) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5)) / (parseInt(sales_qty_year_6) + parseInt(sales_qty_year) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "BP") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL T-CLAMP GROOVE INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "BC") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BD") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BG") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BF") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BK") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BL") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BA") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BS") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year_8) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5)) / (parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }
                        if (branchcode == "BS") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL T-CLAMP GROOVE INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "BC") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BD") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BG") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BF") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BK") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BL") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BP") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "BA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year_9) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5)) / (parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }



                            }
                        }

                        //TOTAL TAEGUTHREAD INSERTS=(E0+E1+OL+OK+E3)/(E4+ON_OM+OO+OP)
                        if (branchcode == "E0") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTHREAD INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "E1") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OL") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OK") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E3") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E4") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ON") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OM") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OO") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OP") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)) / (parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "E1") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTHREAD INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "E0") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OL") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OK") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E3") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E4") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ON") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OM") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OO") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OP") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)) / (parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "OL") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTHREAD INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "E1") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E0") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OK") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E3") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E4") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ON") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OM") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OO") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OP") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)) / (parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "OK") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTHREAD INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "E1") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OL") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E0") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E3") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E4") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ON") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OM") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OO") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OP") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)) / (parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "E3") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTHREAD INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "E1") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OL") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OK") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E0") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E4") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ON") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OM") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OO") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OP") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)) / (parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "E4") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTHREAD INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "E1") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OL") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OK") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E3") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E0") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ON") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OM") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OO") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OP") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year_5) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "ON") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTHREAD INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "E1") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OL") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OK") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E3") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E4") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E0") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OM") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OO") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OP") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year_6) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)) / (parseInt(sales_qty_year_5) + parseInt(sales_qty_year) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "OM") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTHREAD INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "E1") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OL") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OK") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E3") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E4") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ON") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E0") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OO") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OP") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year_7) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)) / (parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "OO") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTHREAD INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "E1") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OL") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OK") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E3") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E4") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ON") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OM") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E0") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OP") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year_8) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)) / (parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year) + parseInt(sales_qty_year_9)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "OP") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTHREAD INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "E1") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OL") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OK") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E3") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E4") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ON") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OM") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "OO") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E0") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "") {
                                    result = Math.round((parseInt(sales_qty_year_9) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4)) / (parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        //TOTAL TOP-CAP/T-CAP INSERTS=EN/E_
                        if (branchcode == "EN") {
                            for (var i = id; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TOP-CAP/T-CAP INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "E_") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                    result = Math.round(parseInt(sales_qty_year) / parseInt(sales_qty_year_1));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }
                        if (branchcode == "E_") {
                            for (var i = id - 1; i > 3 ; i--) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TOP-CAP/T-CAP INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "EN") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "") {
                                    result = Math.round(parseInt(sales_qty_year_1) / parseInt(sales_qty_year));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        //TOTAL ISO MILL INSERTS=(KA+KH+K9+KE)/(KL+KK+K8+KO)
                        if (branchcode == "KA") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL ISO MILL INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "KH") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K9") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KL") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KK") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K8") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KO") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }

                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)) / (parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }
                        if (branchcode == "KH") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL ISO MILL INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "KA") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K9") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KL") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KK") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K8") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KO") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }

                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)) / (parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }
                        if (branchcode == "K9") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL ISO MILL INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "KH") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KA") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KL") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KK") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K8") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KO") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }

                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)) / (parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }
                        if (branchcode == "KE") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL ISO MILL INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "KH") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K9") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KA") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KL") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KK") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K8") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KO") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }

                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)) / (parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }
                        if (branchcode == "KL") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL ISO MILL INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "KH") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K9") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KA") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KK") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K8") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KO") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }

                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "") {
                                    result = Math.round((parseInt(sales_qty_year_4) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }
                        if (branchcode == "KK") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL ISO MILL INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "KH") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K9") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KL") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KA") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K8") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KO") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }

                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "") {
                                    result = Math.round((parseInt(sales_qty_year_5) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)) / (parseInt(sales_qty_year_4) + parseInt(sales_qty_year) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }
                        if (branchcode == "K8") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL ISO MILL INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "KH") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K9") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KL") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KK") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KA") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KO") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }

                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "") {
                                    result = Math.round((parseInt(sales_qty_year_6) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)) / (parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year) + parseInt(sales_qty_year_7)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }
                        if (branchcode == "KO") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL ISO MILL INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "KH") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K9") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KE") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KL") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KK") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "K8") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "KA") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }

                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "") {
                                    result = Math.round((parseInt(sales_qty_year_7) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3)) / (parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }
                        //TOTAL TAEGUTURN INSERTS=(EA+FE+FB+FF+EC+ED+EE+EF+EG+FA+FG+FH+EJ+EL)/(EP+EQ+ER+E8+FV+FM+FN+EY+EX+EV)
                        if (branchcode == "EA") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "FE") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }
                        if (branchcode == "FB") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "FF") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "EC") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "ED") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "EE") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "EF") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "EG") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "FA") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "FG") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "FH") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "EJ") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "EL") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "EP") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year_14) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        break;
                                    }
                                }
                            }
                        }

                        if (branchcode == "EQ") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year_15) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        return;
                                    }
                                }
                            }
                        }
                        if (branchcode == "ER") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year_16) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        return;
                                    }
                                }
                            }
                        } if (branchcode == "E8") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year_17) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        return;
                                    }
                                }
                            }
                        } if (branchcode == "FV") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year_18) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        return;
                                    }
                                }
                            }
                        }

                        if (branchcode == "FM") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year_19) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        return;
                                    }
                                }
                            }
                        }

                        if (branchcode == "FN") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year_20) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        return;
                                    }
                                }
                            }
                        }

                        if (branchcode == "EY") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year_21) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        return;
                                    }
                                }
                            }
                        }
                        if (branchcode == "EX") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EV") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year_22) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year) + parseInt(sales_qty_year_23)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;
                                        return;
                                    }
                                }
                            }
                        }
                        if (branchcode == "EV") {
                            for (var i = 3; i < gridViewCtl.rows.length - 1; i++) {
                                var item = gridViewCtl.rows[i].cells[8].innerText;
                                var j;
                                if (item == "TOTAL TAEGUTURN INSERTS") {
                                    j = i;
                                }
                                var Branchcode_1 = gridViewCtl.rows[i].cells[7].innerText;
                                if (Branchcode_1 == "FE") {
                                    var sales_qty_year_1 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_1 == "") {
                                        sales_qty_year_1 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FB") {
                                    var sales_qty_year_2 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_2 == "") {
                                        sales_qty_year_2 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FF") {
                                    var sales_qty_year_3 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_3 == "") {
                                        sales_qty_year_3 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EC") {
                                    var sales_qty_year_4 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_4 == "") {
                                        sales_qty_year_4 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ED") {
                                    var sales_qty_year_5 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_5 == "") {
                                        sales_qty_year_5 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EE") {
                                    var sales_qty_year_6 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_6 == "") {
                                        sales_qty_year_6 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EF") {
                                    var sales_qty_year_7 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_7 == "") {
                                        sales_qty_year_7 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EG") {
                                    var sales_qty_year_8 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_8 == "") {
                                        sales_qty_year_8 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FA") {
                                    var sales_qty_year_9 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_9 == "") {
                                        sales_qty_year_9 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FG") {
                                    var sales_qty_year_10 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_10 == "") {
                                        sales_qty_year_10 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FH") {
                                    var sales_qty_year_11 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_11 == "") {
                                        sales_qty_year_11 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EJ") {
                                    var sales_qty_year_12 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_12 == "") {
                                        sales_qty_year_12 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EL") {
                                    var sales_qty_year_13 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_13 == "") {
                                        sales_qty_year_13 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EP") {
                                    var sales_qty_year_14 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_14 == "") {
                                        sales_qty_year_14 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EQ") {
                                    var sales_qty_year_15 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_15 == "") {
                                        sales_qty_year_15 = "0";
                                    }
                                }
                                if (Branchcode_1 == "ER") {
                                    var sales_qty_year_16 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_16 == "") {
                                        sales_qty_year_16 = "0";
                                    }
                                }
                                if (Branchcode_1 == "E8") {
                                    var sales_qty_year_17 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_17 == "") {
                                        sales_qty_year_17 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FV") {
                                    var sales_qty_year_18 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_18 == "") {
                                        sales_qty_year_18 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FM") {
                                    var sales_qty_year_19 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_19 == "") {
                                        sales_qty_year_19 = "0";
                                    }
                                }
                                if (Branchcode_1 == "FN") {
                                    var sales_qty_year_20 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_20 == "") {
                                        sales_qty_year_20 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EY") {
                                    var sales_qty_year_21 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_21 == "") {
                                        sales_qty_year_21 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EX") {
                                    var sales_qty_year_22 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_22 == "") {
                                        sales_qty_year_22 = "0";
                                    }
                                }
                                if (Branchcode_1 == "EA") {
                                    var sales_qty_year_23 = gridViewCtl.rows[i].cells[11].childNodes[1].value;
                                    if (sales_qty_year_23 == "") {
                                        sales_qty_year_23 = "0";
                                    }
                                }
                                if (sales_qty_year != undefined && sales_qty_year != "" && sales_qty_year_1 != undefined && sales_qty_year_1 != "" && sales_qty_year_2 != undefined && sales_qty_year_2 != "" && sales_qty_year_3 != undefined && sales_qty_year_3 != "" && sales_qty_year_4 != undefined && sales_qty_year_4 != "" && sales_qty_year_5 != undefined && sales_qty_year_5 != "" && sales_qty_year_6 != undefined && sales_qty_year_6 != "" && sales_qty_year_7 != undefined && sales_qty_year_7 != "" && sales_qty_year_8 != undefined && sales_qty_year_8 != "" && sales_qty_year_9 != undefined && sales_qty_year_9 != "" && sales_qty_year_10 != undefined && sales_qty_year_10 != "" && sales_qty_year_11 != undefined && sales_qty_year_11 != "" && sales_qty_year_12 != undefined && sales_qty_year_12 != "" && sales_qty_year_13 != undefined && sales_qty_year_13 != "" && sales_qty_year_14 != undefined && sales_qty_year_14 != "" && sales_qty_year_15 != undefined && sales_qty_year_15 != "" && sales_qty_year_16 != undefined && sales_qty_year_16 != "" && sales_qty_year_17 != undefined && sales_qty_year_17 != "" && sales_qty_year_18 != undefined && sales_qty_year_18 != "" && sales_qty_year_19 != undefined && sales_qty_year_19 != "" && sales_qty_year_20 != undefined && sales_qty_year_20 != "" && sales_qty_year_21 != undefined && sales_qty_year_21 != "" && sales_qty_year_22 != undefined && sales_qty_year_22 != "" && sales_qty_year_23 != undefined && sales_qty_year_23 != "") {
                                    result = Math.round((parseInt(sales_qty_year_23) + parseInt(sales_qty_year_1) + parseInt(sales_qty_year_2) + parseInt(sales_qty_year_3) + parseInt(sales_qty_year_4) + parseInt(sales_qty_year_5) + parseInt(sales_qty_year_6) + parseInt(sales_qty_year_7) + parseInt(sales_qty_year_8) + parseInt(sales_qty_year_9) + parseInt(sales_qty_year_10) + parseInt(sales_qty_year_11) + parseInt(sales_qty_year_12) + parseInt(sales_qty_year_13)) / (parseInt(sales_qty_year_14) + parseInt(sales_qty_year_15) + parseInt(sales_qty_year_16) + parseInt(sales_qty_year_17) + parseInt(sales_qty_year_18) + parseInt(sales_qty_year_19) + parseInt(sales_qty_year_20) + parseInt(sales_qty_year_21) + parseInt(sales_qty_year_22) + parseInt(sales_qty_year)));
                                    if (j != undefined && j != "") {
                                        gridViewCtl.rows[j].cells[15].innerText = result;

                                    }
                                    break;
                                }
                            }
                        }
                    }
                    //end
                    trigger_grand_total_manupulation(false);

                });

                /**
                * by aswini
                */
                $("[id*=txt_PricePerUnit_sales_value_year_P]").bind("change", function () {


                    //Find and reference the GridView.
                    var grid = $(this).closest("table");

                    var td = $("td", $(this).closest("tr"));
                    //td.css({ "background-color": "#D8EBF2" });

                    var BudRate = $("[id*=txt_PricePerUnit_sales_value_year_P]", td).val(); //2015 Next Year
                    var ProValue = $("[id*=hdn_ActualValue_sales_value_year_0]", td).val(); //2014 Current Year
                    var BudQuantity = $("[id*=txt_ActualQuantity_sales_qty_year_P]", td).val(); //2015 Next Year

                    ////Thangu Code Start
                    ///value for Next Year Calculation
                    var ValueNextYear = (BudQuantity == undefined || BudQuantity == "") ? 0 : BudQuantity * (BudRate != undefined && BudRate != "" ? parseInt(BudRate) : 0);
                    ValueNextYear = ValueNextYear != 0 ? ValueNextYear / 1000 : 0;
                    ValueNextYear = Math.round(ValueNextYear);
                    $("[id*=lbl_ActualValue_sales_value_year_P_New]", td).html(ValueNextYear);

                    var current_year_value_inc = $("[id*=hdn_ActualValue_sales_value_year_0]", td).val();
                    var result_inc_next = 0;
                    if (current_year_value_inc != undefined && !isNaN(current_year_value_inc))
                    { result_inc_next = current_year_value_inc != 0 ? (ValueNextYear / current_year_value_inc - 1) * 100 : 0; }

                    result_inc_next = Math.round(result_inc_next);
                    $("[id*=lbl_ValuePercentage_sales_value_year_0]", td).html(result_inc_next + "%");


                    var resultPriceChange = 0;
                    var resultPriceperUnitCurrentYear = $("[id*=hdn_PricePerUnit_sales_value_year_0]", td).val();
                    if (BudRate != undefined && BudRate != "") {
                        resultPriceChange = resultPriceperUnitCurrentYear == 0 ? 0 : (BudRate / resultPriceperUnitCurrentYear - 1) * 100;
                    }
                    resultPriceChange = Math.round(resultPriceChange);
                    $("[id*=lbl_priceschange_sales_value_year_0]", td).html(resultPriceChange + "%");
                    $("[id*=txt_ActualQuantity_sales_qty_year_P]", td).change();

                    trigger_grand_total_manupulation(true);


                    //Grand Total


                });

            });

            function trigger_grand_total_manupulation(flag) {

                //Grand Total
                var rowsCount = $('#MainContent_grdviewAllValues tr').length;
                var GrandTotal = 0;
                var FamilyTotal = 0;
                var GrandTotalValNext = 0;
                var FamilyTotalValNext = 0;
                for (var i = 0; i < rowsCount; i++) {

                    var next_V = $('#MainContent_grdviewAllValues_lbl_ActualValue_sales_value_year_P_New_' + i).html();
                    if (next_V != undefined && !isNaN(next_V)) {
                        var SumFlag = $('#MainContent_grdviewAllValues_hdn_SumFlag_' + i).val();
                        if (SumFlag != undefined && SumFlag == "products") {
                            GrandTotalValNext = GrandTotalValNext + (next_V == "" ? 0 : parseInt(next_V));
                            FamilyTotalValNext = FamilyTotalValNext + (next_V == "" ? 0 : parseInt(next_V));
                        }
                        if (SumFlag != undefined && SumFlag == "FamilySum") {
                            $('#MainContent_grdviewAllValues_lbl_ActualValue_sales_value_year_P_New_' + i).html(FamilyTotalValNext);
                            var next_Q = $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i).val();
                            var temp = next_Q != 0 && next_Q != undefined ? (FamilyTotalValNext * 1000) / next_Q : 0;
                            temp = Math.round(temp);
                            $('#MainContent_grdviewAllValues_txt_PricePerUnit_sales_value_year_P_' + i).val(temp);

                            FamilyTotalValNext = 0;
                        }
                    }

                    var Qty_P = $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i).val(); //2015 Next Year
                    if (Qty_P == undefined) { }
                    else
                    {
                        var SumFlag = $('#MainContent_grdviewAllValues_hdn_SumFlag_' + i).val();
                        if (SumFlag != undefined && SumFlag == "products") {
                            GrandTotal = GrandTotal + (Qty_P == "" ? 0 : parseInt(Qty_P));
                            FamilyTotal = FamilyTotal + (Qty_P == "" ? 0 : parseInt(Qty_P));
                        }
                        if (SumFlag != undefined && SumFlag == "FamilySum") {
                            var temp = $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i).val();
                            if (temp != FamilyTotal || flag)
                                $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i).val(FamilyTotal).change();
                            FamilyTotal = 0;
                        }
                    }


                }
                var txtId = rowsCount - 1;
                $('#MainContent_grdviewAllValues_lbl_ActualValue_sales_value_year_P_New_' + txtId).html(GrandTotalValNext);
                var temp2 = $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + txtId).val();
                if (temp2 != GrandTotal || flag) {

                    //var next_Q1 = $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + txtId).val();
                    var tempRate = GrandTotal != 0 && GrandTotal != undefined ? (GrandTotalValNext * 1000) / GrandTotal : 0;
                    tempRate = Math.round(tempRate);
                    $('#MainContent_grdviewAllValues_txt_PricePerUnit_sales_value_year_P_' + txtId).val(tempRate);
                    $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + txtId).val(GrandTotal).change();

                }
            }

            $("#MainContent_btnSubmitforHigherApproval").click(function (e) {

                var status = $("#MainContent_hdnCustomerBudgetStatus").val();
                if (status == "APPROVE") {
                    alert("Customer Budget already approved. you can not Edit"); e.preventDefault(); return false;
                }
                return true;
            });
            $("#MainContent_btnReview").click(function (e) {

                var status = $("#MainContent_hdnCustomerBudgetStatus").val();
                if (status == "APPROVE") {
                    alert("Customer Budget already approved. you can not Edit"); e.preventDefault(); return false;
                }
                return true;
            });
            $("#MainContent_btnSave").click(function (e) {

                var status = $("#MainContent_hdnCustomerBudgetStatus").val();
                if (status == "APPROVE") {
                    alert("Customer Budget already approved. you can not Edit"); e.preventDefault(); return false;
                }
                return true;
            });


            //Quantity changes Start
            jQuery(".product_type").each(function () {
                jQuery(this).change(function () {
                    var currentIndex = jQuery(this).data("product_type");
                    var grandTotal = 0;
                    jQuery(".product_type_" + currentIndex).each(function () {
                        var val = jQuery(this).val();
                        if (val != "") {
                            grandTotal += parseInt(val);
                        }
                    });
                    jQuery(".product_type_sub_total_" + currentIndex).val(grandTotal).change();
                });
            });

            jQuery(".product_type_sub_total").each(function () {
                jQuery(this).change(function () {
                    var currentIndex = jQuery(this).data("product_type_sub_total_index");
                    var grandTotal = 0;
                    jQuery(".product_type_sub_total_row_" + currentIndex).each(function () {
                        var val = jQuery(this).val();
                        if (val != "") {
                            grandTotal += parseInt(val);
                        }
                    });
                    jQuery(".product_type_SubFamilySum_" + currentIndex).val(grandTotal).change();
                });
            });
            //Quantity Changes End

            //setTimeout(function () {
            //    $("#MainContent_lbl_MSG").hide('blind', {}, 500)
            //}, 100000);

            $("[id*=txtBudgetInc]").bind("change", function () {
                debugger;
                var incBudget = $("[id*=txtBudgetInc]").val();
                var rowsCount = $('#MainContent_grdviewAllValues tr').length;
                var gridViewCtl = document.getElementById('MainContent_grdviewAllValues');
                if (incBudget == "") {
                    for (var i = 0; i < rowsCount; i++) {
                        $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i).val("");
                    }
                    return;
                }

                for (var i = 0; i < rowsCount; i++) {
                    var result = 0;
                    var qty_P = $('#MainContent_grdviewAllValues_hdn_ActualQuantity_sales_qty_year_0_' + i).val(); //2015 Next Year qty                   
                    if (!isNaN(qty_P) && (qty_P != "") && (qty_P != 0) && (qty_P != null)) {

                        result = (1 + (incBudget / 100)) * qty_P;
                        result = Math.round(result);
                        if (i == 1 || i == rowsCount-1) {
                            $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i + '_Copy').attr('disabled', false);
                            $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i + '_Copy').val(result);
                            $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i + '_Copy').attr('disabled', true);
                        }
                        $('#MainContent_grdviewAllValues_txt_ActualQuantity_sales_qty_year_P_' + i).val(result).change();
                    }
                    
                }

                //for (var i = 0; i < rowsCount; i++) { }


                trigger_grand_total_manupulation(false);

            });

            var td = $("#MainContent_grdviewAllValuesCopy").find("tr:first").find("td:first");
            $(td).find('div').attr("style", "min-width:25px");
            td = $("#MainContent_grdviewAllValuesCopyFreeze").find("tr:first").find("td:first");
            $(td).find('div').attr("style", "min-width:25px");
        }

        function alertforRquired() { alert('Quantity/Value should not be Zero or Empty '); }

        function gridviewScroll() {

            gridView1 = $('#MainContent_grdviewAllValues').gridviewScroll({
                width: $(window).width() - 70,
                height: findminofscreenheight($(window).height(), 460),
                railcolor: "#F0F0F0",
                barcolor: "#606060",
                barhovercolor: "#606060",
                bgcolor: "#F0F0F0",
                freezesize: 9,
                arrowsize: 30,
                varrowtopimg: "Images/arrowvt.png",
                varrowbottomimg: "Images/arrowvb.png",
                harrowleftimg: "Images/arrowhl.png",
                harrowrightimg: "Images/arrowhr.png",
                headerrowcount: 2,
                railsize: 16,
                barsize: 14,
                verticalbar: "auto",
                horizontalbar: "auto",
                wheelstep: 1,
            });
        }
        function findminofscreenheight(a, b) {
            a = a - $("#MainContent_ddlCustomerList").offset().top;
            return a < b ? a : b;
        }

        $('#MainContent_grdView_T_Clamp_Partingoff tr:last').addClass("last_row");


        //jQuery(".parent_row_index td div img").click();
        function bindToogleForRowIndex() {
            jQuery(".row_index_image").click(function () {

                var currentRowElement = jQuery(this).closest(".row_index");
                var currentIndex = jQuery(currentRowElement).data("index");
                var minimised = false;
                jQuery(".subrowindex_" + currentIndex).each(function () {
                    if (jQuery(this).css("display") != "none") {
                        jQuery(this).slideUp();
                        minimised = true;
                    }
                    else {
                        jQuery(this).slideDown();
                        minimised = false;
                    }
                });
                if (minimised) {
                    jQuery(this).attr("src", "images/button_plus.gif");
                    jQuery(currentRowElement).data("toogle_status", "MINIMISED");
                }
                else {
                    jQuery(this).attr("src", "images/button_minus.gif");
                    jQuery(currentRowElement).data("toogle_status", "MAXIMISED");
                }
            });

            //jQuery(".row_index_image").click();
        }


        function bindToogleForParentRowIndex() {
            jQuery(".parent_row_index td div img").click(function () {

                var currentIndex = jQuery(this).closest(".parent_row_index").data("parent_row_index");
                var minimised = false;
                jQuery(".parent_row_index_" + currentIndex).each(function () {
                    if (jQuery(this).data("toogle_status") == "MAXIMISED")
                        jQuery(this).find("td div img").click();
                    var nextElement = jQuery(this).prev();
                    if (jQuery(this).css("display") != "none") {
                        jQuery(this).slideUp();
                        if (jQuery(nextElement).hasClass("empty_row"))
                            jQuery(nextElement).slideUp();
                    }
                    else {
                        jQuery(this).slideDown();
                        if (jQuery(nextElement).hasClass("empty_row"))
                            jQuery(nextElement).slideDown();
                        minimised = true;
                    }
                });
                if (minimised)
                    jQuery(this).attr("src", "images/button_minus.gif");
                else
                    jQuery(this).attr("src", "images/button_plus.gif");
            });
            //jQuery(".parent_row_index td div img").click();
        }

        //toogle row hide and show

        jQuery(".row_index").click(function () {
            debugger;
            var currentIndex = jQuery(this).data("index");

            jQuery(".subrowindex_" + currentIndex).each(function () {
                if (jQuery(this).css("display") != "none")
                    jQuery(this).slideUp();
                else
                    jQuery(this).slideDown();
            });
        });


        function isNumberKey(e) {

            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }

            return true;
        }



    </script>




</asp:Content>
