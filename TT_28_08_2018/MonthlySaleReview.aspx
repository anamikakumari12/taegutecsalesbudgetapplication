﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlySaleReview.aspx.cs" Inherits="TaegutecSalesBudget.MonthlySaleReview" MasterPageFile="~/Site.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="GridviewScroll.css" rel="stylesheet" />
    <script type="text/javascript" src="gridscroll.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   <%-- <script src="Scripts/powerbi.js"></script>--%>
    <script src="Scripts/powerbi.js"></script>
<%--    <form id="form1" runat="server">
        <asp:DropDownList ID="ddlReport" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Page_Load" />
   <br />--%>
	<div class="row filter_panel" id="reportdrpdwns" runat="server" style="padding:2px;">
                <div class="col-md-8" runat="server" id="cterDiv" visible="false" style="padding:8px;">
                    <ul id="divCter" runat="server" class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>
                            <asp:RadioButton ID="rdBtnTaegutec" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" AutoPostBack="true" onChange="hideTable();"/>
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab"  OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" AutoPostBack="true" onChange="hideTable();"/>
                        </li>
                    </ul>
                </div>
		<div class="col-md-4 ">
                    <asp:Button ID="clear" runat="server" Text="Clear" CssClass="btn green"  Style="top: -5px !important;     float: right;" OnClick="clear_Click"/>
                </div>
		</div>

<div id="embedDiv" style="height: 750px; width: 100%;" />
            <script>
        debugger;
	// Read embed token
	var embedToken = "<% =this.embedToken %>";

	// Read embed URL
	var embedUrl = "<% = this.embedUrl %>";

	// Read report Id
	var reportId = "<% = this.reportId %>";

	// Get models (models contains enums)
	var models = window['powerbi-client'].models;

	// Embed configuration is used to describe what and how to embed
	// This object is used when calling powerbi.embed
	// It can also includes settings and options such as filters
	var config = {
		type: 'report',
		tokenType: models.TokenType.Embed,
		accessToken: embedToken,
		embedUrl: embedUrl,
		id: reportId,

		settings: {
			filterPaneEnabled: false,
			navContentPaneEnabled: true
		}
	};

        // Embed the report within the div element
	var embedDiv=document.getElementById("embedDiv");
	var report = powerbi.embed(embedDiv, config);
            </script>
   </asp:Content>

