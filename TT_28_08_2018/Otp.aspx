﻿<%@ Page Language="C#" ValidateRequest="false" AutoEventWireup="true" CodeBehind="Otp.aspx.cs" Inherits="TaegutecSalesBudget.Otp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Login |  Sales-Budget & Performance Monitoring</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" />
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="fonts/fsquere/style.css" />
    <link rel='stylesheet' type='text/css' href="fonts/open-sans/open-sans.css">
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href="#">
</head>

<body class="login" style="background-color: #54364a; background-size: cover; background-image: url(images/b.jpg);">

    <div class="logo">
        <!-- BEGIN LOGO -->
        <img src="images/logo.png" alt="logo" />
    </div>
    <!-- END LOGO -->

    <div class="content">
        <!-- BEGIN LOGIN -->
        <form id="Form1" runat="server">
            <%--  --%>
            <p>Please Enter Otp that you received on your mobile or Email.</p>
            <div class="control-group">
                <div class="controls">
                    <div class="input-icon left">
                       <%-- <i class="fa fa-envelope-o"></i>--%>
                        <asp:TextBox ID="txtotp" class="form-control" placeholder="Otp" name="otp" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <%--  <button type="button" id="back-btn" class="btn">
                    <i class="fs-arrow-left"></i> Back
                </button>--%>
                <%-- <asp:Button ID="btnBack" runat="server" class="btn" Text=" Back" OnClick="btnBack_Click" Width="80px"  />--%> 

                <%--<button type="submit" class="btn green pull-right">
                    <i class="fs-checkmark-2"></i> Submit
                </button>--%>
           <asp:Button ID="btnotp" runat="server" Text="Submit" OnClick="BtnOtp_Click" Width="80px" Height="38px" Style="background: linear-gradient(to bottom,#0582b7 0,#0478af 29%,#025c98 85%,#035995 100%); color: #fff;" OnClientClick=" return  ValidateOtp(); "/>
             
             <br />
                        <asp:Label ID="lblOtpFailure" runat="server" ForeColor="Red"></asp:Label>
            </div>
             <asp:LinkButton ID="resendotp" runat="server" OnClick="ResendOtp_Click" Text="Resend OTP" OnClientClick= "Clear();" ></asp:LinkButton>
         
        </form>
    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->
    <div class="copyright">
        copyright &copy; 2014  Design & Developed by <a href="#">KNS TECHNOLOGIES PVT Ltd</a>... Sales-Budget & Performance Monitoring.   
    </div>
    <!-- END COPYRIGHT -->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/respond.min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.js"></script>

    <%-- <script type="text/javascript" src="Scripts/login.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
          
        });
        function ValidateOtp() {
            debugger;
            //SpecialCharchk();
            if (document.getElementById('txtotp').value == "") {
                alert("Please provide your OTP");
                return false;
            }
            else if (isNaN(document.getElementById('txtotp').value)) {
                alert("Please provide valid OTP");
                return false;
            }
            else
                return true;
        }

        function Clear() {
            debugger;
            document.getElementById('txtotp').value = '';
        }

        //function SpecialCharchk() {
        //    debugger;
        //    var str = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
        //    if ("!@#$%^&*()+=-[]\\\';,./{}|\":<>?".test(str) == false) {
        //             alert("Your Otp has special characters. \nThese are not allowed.\n Please remove them and try again.");
        //    }
        //}

        
  
    </script>
</body>
</html>
