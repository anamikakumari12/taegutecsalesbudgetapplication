﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TSBA_BusinessAccessLayer;

namespace TaegutecSalesBudget
{
    public partial class ListPriceDownload : System.Web.UI.Page
    {
        #region Global Declaration

        Budget objBudget = new Budget();
        Reports objReports = new Reports();
        Review objRSum = new Review();
        CommonFunctions objCom = new CommonFunctions();
        public static int gridLoadedStatus;
        public static string Sbranchlist;
        public static string cter;
        public static decimal byValueIn;
        public static int tegutecheckedchanged;
        public static string roleid = "";
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (!IsPostBack)
                {
                    Session["gridstatus"] = "notempty";
                    cter = null;
                    byValueIn = 100000;
                    gridLoadedStatus = 0;
                    string strUserId = Session["UserId"].ToString();
                    roleid = strUserId;
                    string roleId = Session["RoleId"].ToString();
                    //load product family details
                    if (Session["RoleId"].ToString() == "HO" || Session["RoleId"].ToString() == "TM")
                    {
                        if (Session["RoleId"].ToString() == "HO")
                        {
                            if (Session["cter"] == null && roleId == "HO")
                            {
                                Session["cter"] = "TTA";
                                cter = "TTA";

                            }

                            if (Session["cter"].ToString() == "DUR")
                            {
                                rdBtnDuraCab.Checked = true;
                                rdBtnTaegutec.Checked = false;
                                cter = "DUR";
                            }
                            else
                            {
                                rdBtnTaegutec.Checked = true;
                                rdBtnDuraCab.Checked = false;
                                cter = "TTA";
                            }
                            cterDiv.Visible = true;
                        }

                        LoadBranches();

                        BranchList_SelectedIndexChanged(null, null);
                        SalesEngList_SelectedIndexChanged(null, null);
                        ddlcustomertype_SelectedIndexChanged(null, null);

                    }

                    else if (Session["RoleId"].ToString() == "BM")
                    {
                        string username = Session["UserName"].ToString();
                        string branchcode = Session["BranchCode"].ToString();
                        string branchDec = Session["BranchDesc"].ToString();
                        Session["SelectedBranchList"] = "'" + branchcode + "'";

                        //bind branch
                        BranchList.Items.Insert(0, branchcode);
                        BranchList.Visible = false;
                        BranchText.Visible = true;
                        BranchText.Value = branchDec;
                        BranchText.Disabled = true;

                        // sales engineers loading
                        DataTable dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);
                        if (dtSalesEngDetails != null)
                        {
                            SalesEngList.DataSource = dtSalesEngDetails;
                            SalesEngList.DataTextField = "EngineerName";
                            SalesEngList.DataValueField = "EngineerId";
                            SalesEngList.DataBind();
                        
                        }

                        SalesEngList_SelectedIndexChanged(null, null);


                    }
                    else if (Session["RoleId"].ToString() == "SE")
                    {

                        string username = Session["UserName"].ToString();
                        string branchcode = Session["BranchCode"].ToString();
                        string branchDec = Session["BranchDesc"].ToString();
                        Session["SelectedBranchList"] = "'" + branchcode + "'";
                        Session["SelectedSalesEngineers"] = "'" + strUserId + "'";

                        //bind branch
                        BranchList.Items.Insert(0, branchcode);
                        BranchList.Visible = false;
                        BranchText.Visible = true;
                        BranchText.Value = branchDec;
                        BranchText.Disabled = true;

                        //bind Sales engineer
                        SalesEngList.Items.Insert(0, strUserId);
                        SalesEngList.Visible = false;
                        SaleText.Visible = true;
                        SaleText.Value = username;
                        SaleText.Disabled = true;

                        // customers loading
                        DataTable dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, "SE"); ;
                        if (dtCutomerDetails != null)
                        {
                            DataTable dtDeatils = new DataTable();
                            dtDeatils.Columns.Add("customer_number", typeof(string));
                            dtDeatils.Columns.Add("customer_name", typeof(string));
                            for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                            {
                                dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                            }
                            CustNameList.DataSource = dtDeatils;
                            CustNameList.DataTextField = "customer_name";
                            CustNameList.DataValueField = "customer_number";
                            CustNameList.DataBind();
                           
                            //CustNumList.DataSource = dtCutomerDetails;
                            //CustNumList.DataTextField = "customer_number";
                            //CustNumList.DataValueField = "customer_number";
                            //CustNumList.DataBind();
                            

                            CustNameList_SelectedIndexChanged(null, null);
                            //CustNumList_SelectedIndexChanged(null, null);
                        }

                    }


                }
                txtsearch.Attributes.Add("onkeypress", "return controlEnter('" + searchbtn.UniqueID + "', event)");
                txtsearch1.Attributes.Add("onkeypress", "return controlEnter('" + searchbtn1.UniqueID + "', event)");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadSecondGrid();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author :Monika M S
        /// Created date :2021-06-01
        /// Description :  click on branches dropdownlist, se should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BranchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

                string branchlist = BranchList.SelectedValue;
               
                    Session["SelectedBranchList"] = branchlist;
                
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                objRSum.BranchCode = branchcode;
                objRSum.roleId = roleId;
                objRSum.flag = "SalesEngineer";
                objRSum.cter = cter;
                DataTable dtData = objRSum.getFilterAreaValue(objRSum);

                if (dtData.Rows.Count != 0)
                {
                    SalesEngList.DataSource = dtData;
                    SalesEngList.DataTextField = "EngineerName";
                    SalesEngList.DataValueField = "EngineerId";
                    SalesEngList.DataBind();
                }
                else
                {
                    SalesEngList.DataSource = dtData;
                    SalesEngList.DataTextField = "EngineerName";
                    SalesEngList.DataValueField = "EngineerId";
                    SalesEngList.DataBind();
                }

                grdListPrice.DataSource = null;
                grdListPrice.DataBind();

                grdListPrice1.DataSource = null;
                grdListPrice1.DataBind();

                SalesEngList_SelectedIndexChanged(null, null);

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        /// <summary>
        /// Author :Monika M S
        /// Created date :2021-06-01
        /// Description : click on se Dropdownlist, customers should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void SalesEngList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SalesengList = null;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

               
                SalesengList = SalesEngList.SelectedValue;

                Session["SelectedSalesEngineers"] = SalesengList;
                
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                objRSum.BranchCode = branchcode;
                objRSum.salesengineer_id = SalesengList;


                objRSum.customer_type = ddlcustomertype.SelectedItem.Value;
                objRSum.roleId = roleId;
                objRSum.flag = "CustomerType";
                objRSum.cter = cter;
                DataTable dtData = objRSum.getFilterAreaValue(objRSum);

                if (dtData.Rows.Count != 0)
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                   
                }
                else
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                    
                }
                //if (dtData.Rows.Count != 0)
                //{
                //    CustNumList.DataSource = dtData;
                //    CustNumList.DataTextField = "customer_number";
                //    CustNumList.DataValueField = "customer_number";
                //    CustNumList.DataBind();
                //}
                //else
                //{
                //    CustNumList.DataSource = dtData;
                //    CustNumList.DataTextField = "customer_number";
                //    CustNumList.DataValueField = "customer_number";
                //    CustNumList.DataBind();
                //}


                grdListPrice.DataSource = null;
                grdListPrice.DataBind();

                grdListPrice1.DataSource = null;
                grdListPrice1.DataBind();
                CustNameList_SelectedIndexChanged(null, null);
                //CustNumList_SelectedIndexChanged(null, null);

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author :Monika M S
        /// Created date :2021-06-01
        /// Description : click on cust_name checkbox, cust_no should be checked accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CustNameList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                
                string CustomerNamelist = CustNameList.SelectedValue;
                    Session["SelectedCustomerNames"] = CustomerNamelist;

                //string CustomerNumlist = CustNumList.SelectedValue;

                //    Session["SelectedCustomerNumbers"] = CustomerNumlist;

                grdListPrice.DataSource = null;
                grdListPrice.DataBind();

                grdListPrice1.DataSource = null;
                grdListPrice1.DataBind();
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }


            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        /// <summary>
        /// Author :Monika M S
        /// Created date :2021-06-01
        /// Description : click on cust_no checkbox, cust_name should be checked accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void CustNumList_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //    try
        //    {
               
        //        string CustomerNumlist = CustNumList.SelectedValue;


               
        //            Session["SelectedCustomerNumbers"] = CustomerNumlist;
               
        //        string CustomerNamelist = CustNameList.SelectedValue;
                
        //            Session["SelectedCustomerNames"] = CustomerNamelist;

                

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.LogError(ex);
        //    }


        //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //}

        /// <summary>
        /// Author :Monika M S
        /// Created date :2021-06-01
        /// Description :click on customer type, customers should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlcustomertype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
           
            try
            {
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                string SalesengList = Convert.ToString(Session["SelectedSalesEngineers"]);
                objRSum.BranchCode = (roleId == "TM" && branchcode == "ALL") ? userId : branchcode;
                objRSum.salesengineer_id = SalesengList;
                objRSum.customer_type = ddlcustomertype.SelectedItem.Value;
                objRSum.roleId = roleId;
                objRSum.flag = "CustomerType";
                Session["ddlcustomertype"] = ddlcustomertype.SelectedItem.Value;
                DataTable dtData = objRSum.getFilterAreaValue(objRSum);

                if (dtData.Rows.Count != 0)
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                    // ChkCustName.Items.Insert(0, "ALL");
                }
                else
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                    // ChkCustName.Items.Insert(0, "NO CUSTOMER");
                }
                //if (dtData.Rows.Count != 0)
                //{
                //    CustNumList.DataSource = dtData;
                //    CustNumList.DataTextField = "customer_number";
                //    CustNumList.DataValueField = "customer_number";
                //    CustNumList.DataBind();
                //}
                //else
                //{
                //    CustNumList.DataSource = dtData;
                //    CustNumList.DataTextField = "customer_number";
                //    CustNumList.DataValueField = "customer_number";
                //    CustNumList.DataBind();
                //}

                grdListPrice.DataSource = null;
                grdListPrice.DataBind();

                grdListPrice1.DataSource = null;
                grdListPrice1.DataBind();
                CustNameList_SelectedIndexChanged(null, null);
                //CustNumList_SelectedIndexChanged(null, null);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please click on FILTER to view results');", true);
           
            if (rdBtnTaegutec.Checked)
            {
                Session["cter"] = "TTA";
                cter = "TTA";
            }
            if (rdBtnDuraCab.Checked)
            {
                Session["cter"] = "DUR";
                cter = "DUR";
            }

            LoadBranches();
            BranchList_SelectedIndexChanged(null, null);
            SalesEngList_SelectedIndexChanged(null, null);

        }

        /// <summary>
        /// Author  : Monika M S
        /// Date    : 2021-06-01
        /// Desc    :  Load Branches into DropDown 
        /// </summary>
        protected void LoadBranches()
        {
            try
            {

                string name_desc = "", name_code = "";
                int count = 0;
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Session["BranchCode"].ToString();
                DataTable dtData = new DataTable();
                objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
                objRSum.roleId = roleId;
                objRSum.flag = "Branch";
                objRSum.cter = cter;
                dtData = objRSum.getFilterAreaValue(objRSum);
                BranchList.DataSource = dtData;
                BranchList.DataTextField = "BranchDesc";
                BranchList.DataValueField = "BranchCode";
                BranchList.DataBind();
                Session["DtBranchList"] = dtData;
                //foreach (System.Web.UI.WebControls.ListItem val in BranchList.Items)
                //{
                //    val.Selected = true;

                //    if (val.Selected)
                //    {
                //        count++;
                //        name_desc += val.Text + " , ";
                //        name_code += val.Value + "','";
                //    }
                //}

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                
            }

            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }


        protected void reports_Click(object sender, EventArgs e)
        {
            Session["checkedrows"] = null;
            txtsearch.Text = "";
            txtsearch1.Text = "";
            DataTable dtBind = new DataTable();
            try
            {
                Loadgrid();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadFirstGrid();", true);
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }

        protected void Loadgrid()
        {
            ListPriceDownloadBL objfilebl = new ListPriceDownloadBL();
            DataTable dtDistributors = new DataTable();
            DataTable NewdtDistributors = new DataTable();
            DataTable NewdtDistributors1 = new DataTable();
            DataTable NewdtDistributors2 = new DataTable();

            if (Convert.ToString(Session["customerName"]) != "")
            {
                CustNameList.SelectedValue = Convert.ToString(Session["customerName"]);
            }



            NewdtDistributors1.Columns.AddRange(new DataColumn[6] { new DataColumn("Group"), new DataColumn("Item_code"), new DataColumn("Item_Desc"), new DataColumn("ProductFamily"), new DataColumn("StockCode"), new DataColumn("Price") });
            dtDistributors = objfilebl.getListPriceDetailsBL();
            Session.Add("DataTable", dtDistributors);
            NewdtDistributors = Session["checkedrows"] as DataTable;
            ///NewdtDistributors = dtDistributors.AsEnumerable().Take(20).Skip(0).CopyToDataTable();
            if (dtDistributors.Rows.Count > 0)
            {
                if (NewdtDistributors != null)
                {
                    foreach (DataRow row in NewdtDistributors.Rows)
                    {
                        DataRow[] foundrow = dtDistributors.Select("Item_code = '" + row.ItemArray[1] + "'");
                        if (foundrow.Length != 0)
                        {
                            dtDistributors.Rows.Remove(foundrow[0]);
                        }
                    }
                }
                var searchitem = txtsearch.Text;
                if (searchitem != "")
                {
                    foreach (DataRow row in dtDistributors.Rows)
                    {
                        //add your own columns to be searched here
                        if (row.Field<string>("Group").Contains(searchitem) || row.Field<string>("Item_code").Contains(searchitem) || row.Field<string>("Item_Desc").Contains(searchitem) || row.Field<string>("ProductFamily").Contains(searchitem) || row.Field<string>("StockCode").Contains(searchitem) || row.Field<decimal>("Price").ToString().Contains(searchitem))
                        {
                            //when found copy the row to the cloned table
                            NewdtDistributors1.Rows.Add(row.ItemArray);
                        }
                    }
                    grdListPrice.DataSource = NewdtDistributors1;
                    grdListPrice.DataBind();
                    if (NewdtDistributors1.Rows.Count != 0)
                    {
                        Session["gridstatus"] = "notempty";
                    }
                    else
                    {
                        Session["gridstatus"] = "empty";
                    }
                }
                else
                {
                    grdListPrice.DataSource = dtDistributors;
                    grdListPrice.DataBind();
                }
                var searchitem1 = txtsearch1.Text;
                if (searchitem1 != "")
                {
                    foreach (DataRow row in NewdtDistributors.Rows)
                    {
                        string desc = row.Field<string>("Item_Desc");
                        string productFamily = row.Field<string>("ProductFamily");
                        //add your own columns to be searched here
                        if (row.Field<string>("Group").Contains(searchitem1) || row.Field<string>("Item_code").Contains(searchitem1) || (desc.ToUpper()).Contains(searchitem1.ToUpper()) || (productFamily.ToUpper()).Contains(searchitem1.ToUpper()) || row.Field<string>("StockCode").Contains(searchitem1) || row.Field<string>("Price").ToString().Contains(searchitem1))
                        {
                            //when found copy the row to the cloned table
                            NewdtDistributors2.Rows.Add(row.ItemArray);
                        }
                    }

                   
                    grdListPrice1.DataSource = NewdtDistributors2;
                    grdListPrice1.DataBind();
                    if (NewdtDistributors2.Rows.Count != 0)
                    {
                        Session["gridstatus"] = "notempty";
                    }
                    else
                    {
                        Session["gridstatus"] = "empty";
                    }
                }
                else
                {
                    grdListPrice1.DataSource = NewdtDistributors;
                    grdListPrice1.DataBind();
                }
            }
            else
            {
                grdListPrice.DataSource = null;
                grdListPrice.DataBind();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadSecondGrid();", true);
        }

        protected void chkbox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable Tissues = Session["checkedrows"] as DataTable;
                if (Tissues == null)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[6] { new DataColumn("Group"), new DataColumn("Item_code"), new DataColumn("Item_Desc"), new DataColumn("ProductFamily"), new DataColumn("StockCode"), new DataColumn("Price") });

                    CheckBox chkall = (grdListPrice.HeaderRow.FindControl("chk_All1") as CheckBox);
                    if (chkall.Checked)
                    {
                        chkall.Checked = false;
                    }
                    GridViewRow row = (sender as CheckBox).Parent.Parent as GridViewRow;
                    CheckBox chk = (row.Cells[0].FindControl("chk_multi") as CheckBox);

                    if (row != null && chk != null)
                    {
                        if (chk.Checked)
                        {
                            string itemcode = (row.Cells[1].FindControl("lblitem") as Label).Text;
                            string itemdesc = (row.Cells[2].FindControl("lblitemDesc") as Label).Text;
                            string price = (row.Cells[3].FindControl("lblLP") as Label).Text;
                            string grp = (row.Cells[1].FindControl("lblgrp") as Label).Text;
                            string pf = (row.Cells[2].FindControl("lblpf") as Label).Text;
                            string sc = (row.Cells[3].FindControl("lblsc") as Label).Text;
                            dt.Rows.Add(grp, itemcode, itemdesc, pf, sc, price);
                            
                            Session.Add("checkedrows", dt);
                            row.Visible = false;
                        }

                    }

                    grdListPrice1.DataSource = dt;
                    grdListPrice1.DataBind();

                }
                else
                {
                    DataTable dt = Session["checkedrows"] as DataTable;
                    GridViewRow row = (sender as CheckBox).Parent.Parent as GridViewRow;
                    CheckBox chk = (row.Cells[0].FindControl("chk_multi") as CheckBox);
                    CheckBox chkall = (grdListPrice.HeaderRow.FindControl("chk_All1") as CheckBox);
                    if (chkall.Checked)
                    {
                        chkall.Checked = false;
                    }
                    if (row != null && chk != null)
                    {
                        if (chk.Checked)
                        {
                            string itemcode = (row.Cells[1].FindControl("lblitem") as Label).Text;
                            string itemdesc = (row.Cells[2].FindControl("lblitemDesc") as Label).Text;
                            string price = (row.Cells[3].FindControl("lblLP") as Label).Text;
                            string grp = (row.Cells[1].FindControl("lblgrp") as Label).Text;
                            string pf = (row.Cells[2].FindControl("lblpf") as Label).Text;
                            string sc = (row.Cells[3].FindControl("lblsc") as Label).Text;

                            DataRow[] foundrow = dt.Select("Item_code = '" + itemcode + "'");
                            if (foundrow.Length != 0)
                            {

                            }
                            else
                            {
                                dt.Rows.Add(grp, itemcode, itemdesc, pf, sc, price);
                            }
                            
                            Session.Add("checkedrows", dt);
                            row.Visible = false;
                        }

                    }


                    grdListPrice1.DataSource = dt;
                    grdListPrice1.DataBind();

                }
                Loadgrid();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadSecondGrid();", true);
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }

        protected void chkboxUnSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //ListPriceDownloadBL objfilebl = new ListPriceDownloadBL();

                //DataTable dtDistributors = objfilebl.getListPriceDetailsBL();
                //foreach (GridViewRow row in grdListPrice1.Rows)
                //{
                //    CheckBox chk1 = (row.Cells[0].FindControl("chk1") as CheckBox);
                //    CheckBox chkall = (grdListPrice1.HeaderRow.FindControl("chk_All") as CheckBox);

                //    if (chkall.Checked)
                //    {
                //        chk1.Checked = true;
                //    }
                //    if (chk1.Checked)
                //    {
                //        chkall.Checked = false;
                //        chk1.Checked = false;
                //    }
                //    if (row != null && chk1 != null)
                //    {
                //        if (chk1.Checked == false)
                //        {
                //            string itemcode = (row.Cells[1].FindControl("lblitem") as Label).Text;
                //            foreach (GridViewRow datarow in grdListPrice.Rows)
                //            {
                //                if (datarow.RowType == DataControlRowType.DataRow)
                //                {
                //                    string itemcode1 = (datarow.Cells[1].FindControl("lblitem") as Label).Text;
                //                    CheckBox chk2 = (datarow.Cells[0].FindControl("chk_multi") as CheckBox);
                //                    if (itemcode == itemcode1)
                //                    {
                //                        chk2.Checked = false;
                //                        datarow.Visible = true;
                //                        row.Visible = false;
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                int pageindex = Convert.ToInt32(Session["pageIndex"]);
                grdListPrice.PageIndex = pageindex;
                Session["checkedrows"] = null;
                Loadgrid();
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
         
            //Response.Redirect("ListPriceDownload.aspx?ListPrice");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadSecondGrid();", true);
        }

        protected void btnpdf_Click(object sender, EventArgs e)
        {
            DataTable dt = Session["checkedrows"] as DataTable;
            DataTable result = dt.AsEnumerable()
                     .GroupBy(x => x.Field<string>("Item_code"))
                     .Select(x => x.First()).CopyToDataTable();
            try
            {
                string customerName = CustNameList.SelectedItem.Text;
                //download pdf as html
                string html = string.Empty;
                string html1 = string.Empty;
                string filename = string.Empty;
                
                string year = DateTime.Now.Year.ToString();
                html = "<html ><head></head><body>";
                html1 = "<html ><head></head><body>";
                string Imagepath = ConfigurationManager.AppSettings["ImagePath"].ToString();
                StringBuilder strHTMLBuilder = new StringBuilder();
                StringBuilder strHTMLBuilder1 = new StringBuilder();

                strHTMLBuilder1.Append("<style type='text/css'>.heading{border-bottom:solid black 4px}</style>");

                strHTMLBuilder1.Append("<table cellspacing='0' style='border: 1px solid white; border-collapse:collapse; width: 100%; background-color: #696969; '>");

                strHTMLBuilder1.Append("<tr height='40px'>");
                strHTMLBuilder1.Append("<td align='left' style='width: 40%;'><img src='" + Imagepath + "' style='width: 70%;background-color: #fff;' /></td>");
                strHTMLBuilder1.Append("<td style='width: 60%;text-align:left; font-size:17px; font-weight:bold; color: white;'>");
                strHTMLBuilder1.Append("Product Program " + year + "");
                strHTMLBuilder1.Append("</td>");
                strHTMLBuilder1.Append("</tr>");
                strHTMLBuilder1.Append("</table>");

                strHTMLBuilder1.Append("<table cellspacing='3' style='width:100%;'>");

                strHTMLBuilder1.Append("<tr>");
                strHTMLBuilder1.Append("<td style='width:25%; text-align:center; font-size:13.5px;font-weight:bold; color: black;'>");
                strHTMLBuilder1.Append("<u>General Terms & Conditions</u>");
                strHTMLBuilder1.Append("</td>");
                strHTMLBuilder1.Append("</tr>");
                strHTMLBuilder1.Append("</table>");

                strHTMLBuilder1.Append("<table cellspacing='5' style='border-bottom: solid black 4px; width: 100%;'>");
                strHTMLBuilder1.Append("<tr>");
                strHTMLBuilder1.Append("<td>");
                strHTMLBuilder1.Append("<span style='font-weight:bold;font-size:20px'>Price</span>");
                strHTMLBuilder1.Append("</td>");
                strHTMLBuilder1.Append("</tr>");
                strHTMLBuilder1.Append("</table>");

                strHTMLBuilder1.Append("<table cellspacing='4' style='width: 100%;'>");
                strHTMLBuilder1.Append("<tr>");
                strHTMLBuilder1.Append("<td style='font-size:12px'>");

                strHTMLBuilder1.Append("<p>");
                strHTMLBuilder1.Append("<b>This supersedes all price communicated earlier for these items and is effective from 1ST April 2021.</b> <br/>" +
                                        "1.The prices indicated are unit prices. <br/>" +
                                        "2.The prices indicated are, for nearest to the destination inclusive of packing and insurance. <br/>" +
                                        "3.GST and other levies of Central and State Governments will be charged extras applicable at the time of dispatch. <br/>" +
                                        "4.Prices ruling at the time of delivery will apply. <br/>" +
                                        "5.Specifications and prices are subject to change without prior notice. <br/>");
                strHTMLBuilder1.Append("</p>");
                strHTMLBuilder1.Append("</td>");
                strHTMLBuilder1.Append("</tr>");
                strHTMLBuilder1.Append("</table>");

                strHTMLBuilder1.Append("<table cellspacing='5' style='width:100%;'>");
                strHTMLBuilder1.Append("<tr>");
                strHTMLBuilder1.Append("<td class='heading' style='width:50%;'><span style='font-weight:bold;font-size:20px'>Warranty</span></td>");
                strHTMLBuilder1.Append("<td class='heading' style='width:50%;'><span style='font-weight:bold;font-size:20px'>Jurisdiction</span></td>");
                strHTMLBuilder1.Append("</tr>");
                strHTMLBuilder1.Append("<tr>");
                strHTMLBuilder1.Append("<td style='width:50%;font-size:12px'>");
                strHTMLBuilder1.Append("<p>");
                strHTMLBuilder1.Append("TaeguTec India warrants its products to be free from <br/> any material defects and workmanship for a period <br/> of one year from the date of original sales (first sale <br/> from the company).");
                strHTMLBuilder1.Append("</p>");
                strHTMLBuilder1.Append("</td>");
                strHTMLBuilder1.Append("<td style='width:50%;font-size:12px'>");
                strHTMLBuilder1.Append("<p>");
                strHTMLBuilder1.Append("All disputes or actions arising in connection with <br/> this purchase contract shall be resolved and <br/> governed in accordance with local applicable laws <br/> subject to Bangalore jurisdiction.");
                strHTMLBuilder1.Append("</p>");
                strHTMLBuilder1.Append("</td>");
                strHTMLBuilder1.Append("</tr>");
                strHTMLBuilder1.Append("</table>");

                strHTMLBuilder1.Append("<table cellspacing='5' style='width:100%;'>");
                strHTMLBuilder1.Append("<tr>");
                strHTMLBuilder1.Append("<td class='heading' style='width:50%;'><span style='font-weight:bold;font-size:20px'>Delivery</span></td>");
                strHTMLBuilder1.Append("<td class='heading' style='width:50%;'><span style='font-weight:bold;font-size:20px'>Conditions of Delivery</span></td>");
                strHTMLBuilder1.Append("</tr>");
                strHTMLBuilder1.Append("<tr>");
                strHTMLBuilder1.Append("<td style='width:50%;font-size:12px'>");
                strHTMLBuilder1.Append("<p>");
                strHTMLBuilder1.Append("Stock code (SC) is indicated against each item. However, <br/> this is a dynamic situation. Based on the movement in <br/> the market, the stock code may change without prior <br/> notice. <br/>");
                strHTMLBuilder1.Append("</p>");
                strHTMLBuilder1.Append("<p>");
                strHTMLBuilder1.Append("<b>General guidelines on delivery are: </b><br/>");
                strHTMLBuilder1.Append("•Stock Code 1: lf not in stock, delivery within 4 <br/> weeks. <br/> •Stock Code 3/8: Generally, not stackable all <br/> the time. They are made to order. Delivery <br/>" +
                    "will be 6-8 weeks. In some cases, there may <br/> be MOQ applicable. Kindly check prior to <br/> ordering. <br/> •Stock Code 6: If not available in stock, delivery <br/>within 8 weeks.");

                strHTMLBuilder1.Append("</p>");
                strHTMLBuilder1.Append("</td>");
                strHTMLBuilder1.Append("<td style='width:50%;font-size:12px'>");
                strHTMLBuilder1.Append("<p>");
                strHTMLBuilder1.Append("Delivery time Is defined on receipt of order at our <br/> end. Conditions of delivery are contingent upon any <br/> causes beyond TaeguTec’s control including but not <br/> limited to acts of god, war, civil riots, local or foreign." +
                    "government regulations, strikes, equipment breakdown <br/> or transportation disruption or availability of raw <br/> material. TaeguTec’s obligation hereunder is subject to <br/> cancellation and / or rescission by TaeguTec at our <br/>option. Any risk of loss or destruction of products or" +
                    "injury shall pass to buyer upon delivery to transporter <br/> or courier.");
                strHTMLBuilder1.Append("</p>");
                strHTMLBuilder1.Append("</td>");
                strHTMLBuilder1.Append("</tr>");
                strHTMLBuilder1.Append("</table>");

                strHTMLBuilder1.Append("<table cellspacing='5' style='width:100%;'>");
                strHTMLBuilder1.Append("<tr>");
                strHTMLBuilder1.Append("<td class='heading' style='width:50%;'><span style='font-weight:bold;font-size:20px'>Safety</span></td>");
                strHTMLBuilder1.Append("<td class='heading' style='width:50%;'><span style='font-weight:bold;font-size:20px'>Liability</span></td>");
                strHTMLBuilder1.Append("</tr>");
                strHTMLBuilder1.Append("<tr>");
                strHTMLBuilder1.Append("<td style='width:50%;font-size:12px'>");
                strHTMLBuilder1.Append("<p>");
                strHTMLBuilder1.Append("Purchaser will familiarize itself with all the  <br/> information and precautions mentioned in the <br/> safety and health communication. Purchaser will <br/>" +
                    "instruct all its employees, agents and customers of  <br/> the precautions and safe use practices needed in <br/> handling, storage, transportation, usage and disposal <br/> of products. <br/>" +
                    "Purchaser will comply with applicable safety and <br/> environmental laws and initiate action needed to avoid <br/> danger to humans, property and environment.");

                strHTMLBuilder1.Append("</p>");
                strHTMLBuilder1.Append("</td>");
                strHTMLBuilder1.Append("<td style='width:50%;font-size:12px'>");
                strHTMLBuilder1.Append("<p>");
                strHTMLBuilder1.Append("The purchaser accepts that no remedy (including, <br/> Without limitation, special, incidental, or consequential  <br/> damage for lost sale, profits, injury to person or <br/> property or any other incidental or consequential loss)" +
                    "<br/> shall be available whether arising under contract,  <br/> negligence, warranty or any other liability. Purchaser <br/> agrees to indemnify TaeguTec from any liability or  <br/>obligation incurred by or assessed against TaeguTec to or <br/>" +
                    "by any persons injured directly or indirectly in <br/> connection with any of the products, documentation, or <br/> materials supplied by TaeguTec.");
                strHTMLBuilder1.Append("</p>");
                strHTMLBuilder1.Append("</td>");
                strHTMLBuilder1.Append("</tr>");
                strHTMLBuilder1.Append("</table>");
                html1 += strHTMLBuilder1.ToString();

                //strHTMLBuilder.Append("<table style='width: 100%;'>");

                //strHTMLBuilder.Append("<tr>");
                //strHTMLBuilder.Append("<td align='left'><img src='" + Imagepath + "' style='width: 80%;'/></td>");
                //strHTMLBuilder.Append("<td align='right'>");
                //strHTMLBuilder.Append("Product Program " + year + "");
                //strHTMLBuilder.Append("</td>");
                //strHTMLBuilder.Append("</tr>");
                //strHTMLBuilder.Append("</table>");

                strHTMLBuilder.Append("<table style='width: 100%;'>");

                strHTMLBuilder.Append("<tr>");
                //strHTMLBuilder.Append("<td align:'right' style='font-weight:bold;font-size:12px'>");
                //strHTMLBuilder.Append("Customer Of Distributor:");
                //strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td align='center' style='font-weight:bold;font-size:16px'>");
                strHTMLBuilder.Append("" + customerName + "");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                strHTMLBuilder.Append("<br/>");
                strHTMLBuilder.Append("<br/>");
                //strHTMLBuilder.Append("<table cellspacing='0' style='border: 1px solid white; border-collapse:collapse; width: 100%; background-color: #C0C0C0; '>");
                strHTMLBuilder.Append("<table cellspacing='0' style='border: 1px solid white; border-collapse:collapse; width: 100%;'>");

                strHTMLBuilder.Append("<tr height='35px' style='background-color:#C0C0C0; color: black;'>");
                strHTMLBuilder.Append("<td style='width:7%; text-align:center; font-size:14px; font-weight:bold; color: black;border: 1px solid black;'>");
                strHTMLBuilder.Append("Sl No");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='width:13%; text-align:center; font-size:14px; font-weight:bold; color: black;border: 1px solid black;'>");
                strHTMLBuilder.Append("Code");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='width:35%; text-align:center; font-size:14px; font-weight:bold; color: black;border: 1px solid black;'>");
                strHTMLBuilder.Append("Item Description");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='width:45%; text-align:center; font-size:14px; font-weight:bold; color: black;border: 1px solid black;'>");
                strHTMLBuilder.Append("Product Family");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='width:7%; text-align:center; font-size:14px; font-weight:bold; color: black;border: 1px solid black;'>");
                strHTMLBuilder.Append("SC");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='width:13%; text-align:center; font-size:14px; font-weight:bold; color: black;border: 1px solid black;'>");
                strHTMLBuilder.Append("Price /Unit(Rs)");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                int slno = 0;
                for (int i = 0; i < result.Rows.Count; i++)
                {
                    slno++;
                    strHTMLBuilder.Append("<tr height='28px' style='background-color:#F5F5F5;'>");
                    strHTMLBuilder.Append("<td style='width:7%; text-align:center; font-size:12px; color: black;border: 1px solid black;'>");
                    strHTMLBuilder.Append(slno);
                    strHTMLBuilder.Append("</td>");

                    strHTMLBuilder.Append("<td style='width:13%; text-align:center; font-size:12px; color: black;border: 1px solid black;'>");
                    strHTMLBuilder.Append(Convert.ToString(result.Rows[i]["Item_code"]));
                    strHTMLBuilder.Append("</td>");

                    strHTMLBuilder.Append("<td style='width:35%; text-align:center; font-size:12px; color: black;border: 1px solid black;'>");
                    strHTMLBuilder.Append(Convert.ToString(result.Rows[i]["Item_Desc"]));
                    strHTMLBuilder.Append("</td>");

                    strHTMLBuilder.Append("<td style='width:45%; text-align:center; font-size:12px; color: black;border: 1px solid black;'>");
                    string pf = Convert.ToString(result.Rows[i]["ProductFamily"]);
                    if (pf.Contains('&'))
                    {
                        pf = pf.Replace("&", "&amp;");
                        strHTMLBuilder.Append(pf);
                        strHTMLBuilder.Append("</td>");
                    }
                    else
                    {
                        strHTMLBuilder.Append(Convert.ToString(result.Rows[i]["ProductFamily"]));
                        strHTMLBuilder.Append("</td>");
                    }
                    strHTMLBuilder.Append("<td style='width:7%; text-align:center; font-size:12px; color: black;border: 1px solid black;'>");
                    strHTMLBuilder.Append(Convert.ToString(result.Rows[i]["StockCode"]));
                    strHTMLBuilder.Append("</td>");

                    strHTMLBuilder.Append("<td style='width:13%; text-align:right; font-size:12px; color: black;border: 1px solid black;'>");
                    strHTMLBuilder.Append(Convert.ToString(result.Rows[i]["Price"]));
                    strHTMLBuilder.Append("</td>");
                    strHTMLBuilder.Append("</tr>");

                }
                strHTMLBuilder.Append("</table>");

                strHTMLBuilder.Append("<table cellspacing='5' style='width:100%;'>");
                strHTMLBuilder.Append("<tr>");
                strHTMLBuilder.Append("<td style='width:50%;font-size:12px'>");
                strHTMLBuilder.Append("<p>");
                strHTMLBuilder.Append("SC:Stock Code <br/> SC 1:Genearally Stocked | SC 6 : New Items <br/> Item Description, Stock Codes and Prices are subjects to change. <br/> For detailed product specifications,please refer to our catalogue.");
                strHTMLBuilder.Append("</p>");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                //strHTMLBuilder.Append("<p style ='font-weight:bold;font-size:12px;color:black'> SC:Stock Code</p>");
                //strHTMLBuilder.Append("<p style ='font-weight:bold;font-size:12px;color:black'> SC 1:Genearally Stocked | SC 6 : New Items</p>");
                //strHTMLBuilder.Append("<p style ='font-weight:bold;font-size:12px;color:black'> Item Description, Stock Codes and Prices are subjects to change.</p>");
                //strHTMLBuilder.Append("<p style ='font-weight:bold;font-size:12px;color:black'> For detailed product specifications,please refer to our catalogue.</p>");

                html += strHTMLBuilder.ToString();
                string date = DateTime.Now.Date.ToString("yyyy-MM-dd");
                filename = "ListPrice_"+date+".pdf";
                string htmlfile1 = "htmlpage1.txt";
                string htmlfile2 = "htmlpage2.txt";
                string filepath = ConfigurationManager.AppSettings["ListPriePDF_Folder"].ToString();
                string filename1 = GetNextFileName(filepath, filename);
                string htmlfilename = GetNextFileName1(filepath, htmlfile1);
                string htmlfilename1 = GetNextFileName2(filepath, htmlfile2);
                string path = filepath + filename1;
                html += "</body></html>";
                html1 += "</body></html>";
                var document = new Document();
                convertPDF(html, html1, filepath, filename1, htmlfilename, htmlfilename1);

            }

            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }

        }
        private void convertPDF(string html, string html1, string filepath, string filename, string htmlfilename1, string htmlfilename2)
        {
            try
            {
                XMLWorkerHelper worker = XMLWorkerHelper.GetInstance();
                using (var stream = new MemoryStream())
                {
                    using (var document = new Document(PageSize.A4, 10f, 10f, 40f, 40f))
                    {
                        //document.SetMargins(10f, 10f, 40f, 30f);
                        PdfWriter writer = PdfWriter.GetInstance(document, stream);
                        document.Open();
                        document.Add(new Chunk(""));
                        using (var stringReader = new StringReader(html1))
                        {
                            XMLWorkerHelper.GetInstance().ParseXHtml(
                                writer, document, stringReader
                            );

                        }
                        document.NewPage();

                        using (var stringReader = new StringReader(html))
                        {
                            XMLWorkerHelper.GetInstance().ParseXHtml(
                                writer, document, stringReader
                            );
                        }

                        document.Close();
                        File.WriteAllBytes(string.Concat(filepath, filename), stream.ToArray());
                        File.WriteAllText(filepath + htmlfilename1, html);
                        File.WriteAllText(filepath + htmlfilename2, html1);


                        string filenamewithPath = filepath + filename;
                        OnEndPage(filenamewithPath);
                        HttpResponse response = HttpContext.Current.Response;
                        DownloadFile(response, filenamewithPath);
                       
                    }

                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }

        public static void DownloadFile(HttpResponse response, string fileRelativePath)
        {
            try
            {
                string contentType = "";
                //Get the physical path to the file.
                string FilePath = fileRelativePath;

                string fileExt = Path.GetExtension(fileRelativePath).Split('.')[1].ToLower();

                if (fileExt == "pdf")
                {
                    //Set the appropriate ContentType.
                    contentType = "Application/pdf";
                }

                //Set the appropriate ContentType.
                response.ContentType = contentType;
                response.AppendHeader("content-disposition", "attachment; filename=" + (new FileInfo(fileRelativePath)).Name);

                //Write the file directly to the HTTP content output stream.
                response.WriteFile(FilePath);
                response.End();
            }
            catch
            {
                //To Do
            }
        }


        public void OnEndPage(string filenamewithPath)
        {
            string year = DateTime.Now.Year.ToString();
            var filePath = filenamewithPath;
            byte[] bytes = System.IO.File.ReadAllBytes(filePath);
            var pdfReader = new PdfReader(bytes);
            using (Stream output = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (PdfStamper pdfStamper = new PdfStamper(pdfReader, output))
                {
                    for (int pageIndex = 1; pageIndex <= pdfReader.NumberOfPages; pageIndex++)
                    {
                        pdfStamper.FormFlattening = false;
                        Rectangle pageRectangle = pdfReader.GetPageSizeWithRotation(pageIndex);
                        PdfContentByte pdfData = pdfStamper.GetOverContent(pageIndex);
                        pdfData.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 10);
                        PdfGState graphicsState = new PdfGState
                        {
                            FillOpacity = 1.0F
                        };
                        pdfData.SetGState(graphicsState);
                        //pdfData.BeginText();
                        // select the font properties
                        BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        pdfData.SetColorFill(BaseColor.BLACK);
                        //pdfData.SetFontAndSize(bf, 8);
                        pdfData.SetFontAndSize(bf, 12);

                        if (pageIndex == 1)
                        {

                            string text2 = pageIndex + " " + "of" + " " + pdfReader.NumberOfPages;
                            pdfData.ShowTextAligned(1, text2, 290, 10, 0);
                        }
                        else
                        {
                            string Imagepath = ConfigurationManager.AppSettings["ImagePath1"].ToString();
                            var header = iTextSharp.text.Image.GetInstance(Path.Combine(HttpRuntime.AppDomainAppPath + Imagepath));
                            header.ScalePercent(3f);
                            header.SetAbsolutePosition(10, 805);
                            pdfData.AddImage(header);
                            string text1 = "Product Program " + year + "";
                            pdfData.ShowTextAligned(1, text1, 515, 815, 0);
                            string text2 = pageIndex + " " + "of" + " " + pdfReader.NumberOfPages;
                            pdfData.ShowTextAligned(1, text2, 290, 10, 0);




                            //pdfStamper.FormFlattening = false;
                            //Rectangle pageRectangle = pdfReader.GetPageSizeWithRotation(pageIndex);
                            //PdfContentByte pdfData = pdfStamper.GetOverContent(pageIndex);
                            //pdfData.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), 10);
                            //PdfGState graphicsState = new PdfGState
                            //{
                            //    FillOpacity = 1.0F
                            //};
                            //pdfData.SetGState(graphicsState);
                            ////pdfData.BeginText();
                            //// select the font properties
                            //BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                            //pdfData.SetColorFill(BaseColor.BLACK);
                            ////pdfData.SetFontAndSize(bf, 8);
                            //pdfData.SetFontAndSize(bf, 12);
                            //// write the text in the pdf content
                            ////pdfData.BeginText();
                            ////string text = "Faisal Pathan";
                            ////// put the alignment and coordinates here
                            ////pdfData.ShowTextAligned(1, text, 50, 815, 0);
                            //var header = iTextSharp.text.Image.GetInstance(Path.Combine(HttpRuntime.AppDomainAppPath + "images\\Final.png"));
                            //header.ScalePercent(3f);
                            //header.SetAbsolutePosition(10, 805);
                            //pdfData.AddImage(header);
                            ////pdfData.EndText();
                            ////pdfData.BeginText();
                            ////string text1 = "faisalmpathan@gmail.com";
                            //string text1 = "Product Program " + year + "";
                            //pdfData.ShowTextAligned(1, text1, 515, 815, 0);
                            //string text2 = pageIndex + " " + "of" + " " + pdfReader.NumberOfPages;
                            //pdfData.ShowTextAligned(1, text2, 290, 10, 0);

                            //pdfData.EndText();
                        }
                    }

                }
                output.Close();
                output.Dispose();
            }
        }

        private string GetNextFileName(string pathname, string fileName)
        {
         
            string extension = Path.GetExtension(fileName);
            string pathName = Path.GetDirectoryName(pathname);
            string onlyfilename = Path.GetFileNameWithoutExtension(fileName);

            int fCount = Directory.GetFiles(pathname, fileName, SearchOption.AllDirectories).Length;
            int fCount1 = Directory.GetFiles(pathname, fileName+"(*", SearchOption.AllDirectories).Length;
            if (fCount == 0 && fCount1 == 0)
            {
                fileName = fileName;
            }
            else if (fCount == 1 && fCount1 >= 0)
            {
                fileName = string.Format("{0}({1}){2}", onlyfilename, fCount1 + 1, extension);
            }

            return fileName;
        }

        private string GetNextFileName2(string pathname, string fileName)
        {

            string extension = Path.GetExtension(fileName);
            string pathName = Path.GetDirectoryName(pathname);
            string onlyfilename = Path.GetFileNameWithoutExtension(fileName);

            int fCount = Directory.GetFiles(pathname, fileName, SearchOption.AllDirectories).Length;
            int fCount1 = Directory.GetFiles(pathname, fileName + "(*", SearchOption.AllDirectories).Length;
            if (fCount == 0 && fCount1 == 0)
            {
                fileName = fileName;
            }
            else if (fCount == 1 && fCount1 >= 0)
            {
                fileName = string.Format("{0}({1}){2}", onlyfilename, fCount1 + 1, extension);
            }

            return fileName;
        }

        private string GetNextFileName1(string pathname, string fileName)
        {

            string extension = Path.GetExtension(fileName);
            string pathName = Path.GetDirectoryName(pathname);
            string onlyfilename = Path.GetFileNameWithoutExtension(fileName);

            int fCount = Directory.GetFiles(pathname, fileName, SearchOption.AllDirectories).Length;
            int fCount1 = Directory.GetFiles(pathname, fileName + "(*", SearchOption.AllDirectories).Length;
            if (fCount == 0 && fCount1 == 0)
            {
                fileName = fileName;
            }
            else if (fCount == 1 && fCount1 >= 0)
            {
                fileName = string.Format("{0}({1}){2}", onlyfilename, fCount1 + 1, extension);
            }

            return fileName;
        }

        protected void btnclear_Click(object sender, EventArgs e)
        {
            Session["checkedrows"] = null;
            Loadgrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadSecondGrid();", true);
            ///Response.Redirect("ListPriceDownload.aspx?ListPrice");
        }

        protected void chkboxSelectAll_CheckedChanged1(object sender, EventArgs e)
        {
            try
            {
                ListPriceDownloadBL objfilebl = new ListPriceDownloadBL();

                DataTable dtDistributors = objfilebl.getListPriceDetailsBL();
                DataTable Tissues = Session["checkedrows"] as DataTable;
                GridViewRow row = (sender as CheckBox).Parent.Parent as GridViewRow;
                CheckBox chk = (row.Cells[0].FindControl("chk1") as CheckBox);
                if (row != null && chk != null)
                {
                    if (chk.Checked == false)
                    {
                        string itemcode = (row.Cells[1].FindControl("lblitem") as Label).Text;
                        //foreach (GridViewRow datarow in grdListPrice.Rows)
                        //if (datarow.RowType == DataControlRowType.DataRow)
                        //{
                        //    string itemcode1 = (datarow.Cells[1].FindControl("lblitem") as Label).Text;
                        //    CheckBox chk1 = (datarow.Cells[0].FindControl("chk_multi") as CheckBox);
                        //    if (itemcode == itemcode1)
                        //    {
                        //        chk1.Checked = false;
                        //        datarow.Visible = true;
                        //        row.Visible = false;
                        //    }
                        //}
                        //row.Visible = false;
                        DataRow[] foundrow = Tissues.Select("Item_code = '" + itemcode + "'");
                        if (foundrow.Length != 0)
                        {
                            Tissues.Rows.Remove(foundrow[0]);
                        }



                    }
                }
                Loadgrid();
                int pageindex = Convert.ToInt32(Session["pageIndex"]);
                //grdListPrice.DataSource = dtDistributors;
                grdListPrice.PageIndex = pageindex;
                grdListPrice.DataBind();
                //grdListPrice1.DataSource = Tissues;
                //grdListPrice1.DataBind();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadSecondGrid();", true);
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }
       
        protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable Tissues = Session["checkedrows"] as DataTable;
                if (Tissues == null)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[6] { new DataColumn("Group"), new DataColumn("Item_code"), new DataColumn("Item_Desc"), new DataColumn("ProductFamily"), new DataColumn("StockCode"), new DataColumn("Price") });

                    foreach (GridViewRow row in grdListPrice.Rows)
                    {
                        CheckBox chk1 = (row.Cells[0].FindControl("chk_multi") as CheckBox);
                        CheckBox chkall = (grdListPrice.HeaderRow.FindControl("chk_All1") as CheckBox);

                        if (chkall.Checked)
                        {
                            chk1.Checked = true;
                        }
                        if (chk1.Checked)
                        {
                            chkall.Checked = false;
                            chk1.Checked = false;
                        }
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            string itemcode = (row.Cells[1].FindControl("lblitem") as Label).Text;
                            string itemdesc = (row.Cells[2].FindControl("lblitemDesc") as Label).Text;
                            string price = (row.Cells[3].FindControl("lblLP") as Label).Text;
                            string grp = (row.Cells[1].FindControl("lblgrp") as Label).Text;
                            string pf = (row.Cells[2].FindControl("lblpf") as Label).Text;
                            string sc = (row.Cells[3].FindControl("lblsc") as Label).Text;
                            dt.Rows.Add(grp, itemcode, itemdesc, pf, sc, price);
                            Session.Add("checkedrows", dt);
                            row.Visible = false;
                        }
                    }
                    grdListPrice1.DataSource = dt;
                    grdListPrice1.DataBind();
                }
                else
                {
                    DataTable dt = Session["checkedrows"] as DataTable;
                    foreach (GridViewRow row in grdListPrice.Rows)
                    {
                        CheckBox chk1 = (row.Cells[0].FindControl("chk_multi") as CheckBox);
                        CheckBox chkall = (grdListPrice.HeaderRow.FindControl("chk_All1") as CheckBox);

                        if (chkall.Checked)
                        {
                            chk1.Checked = true;
                        }
                        if (chk1.Checked)
                        {
                            chkall.Checked = false;
                            chk1.Checked = false;
                        }
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            string itemcode = (row.Cells[1].FindControl("lblitem") as Label).Text;
                            string itemdesc = (row.Cells[2].FindControl("lblitemDesc") as Label).Text;
                            string price = (row.Cells[3].FindControl("lblLP") as Label).Text;
                            string grp = (row.Cells[1].FindControl("lblgrp") as Label).Text;
                            string pf = (row.Cells[2].FindControl("lblpf") as Label).Text;
                            string sc = (row.Cells[3].FindControl("lblsc") as Label).Text;
                            dt.Rows.Add(grp, itemcode, itemdesc, pf, sc, price);
                            Session.Add("checkedrows", dt);
                            row.Visible = false;
                        }
                    }
                    grdListPrice1.DataSource = dt;
                    grdListPrice1.DataBind();
                }
                Loadgrid();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadSecondGrid();", true);
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }

        protected void grdListPrice_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            grdListPrice.PageIndex = e.NewPageIndex;
            this.Loadgrid();
            Session["pageIndex"] = e.NewPageIndex;

            //int index = e.NewPageIndex;
            //index = index * 10;
            //DataTable NewdtDistributors = new DataTable();
            //DataTable dt = Session["DataTable"] as DataTable;
            //NewdtDistributors = dt.AsEnumerable().Skip(index).Take(10).CopyToDataTable();
            //grdListPrice.DataSource = NewdtDistributors;
            //grdListPrice.DataBind();
        }


        protected void ddlselect_SelectedIndexChanged(object sender, EventArgs e)
        {
            int pagesize = Convert.ToInt32(ddlselect.SelectedValue);
            DataTable dt = Session["DataTable"] as DataTable;
            grdListPrice.DataSource = dt;
            grdListPrice.PageSize = pagesize;
            grdListPrice.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadSecondGrid();", true);
        }

        protected void searchbtn_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DataTable dt = Session["DataTable"] as DataTable;
                DataTable NewdtDistributors = new DataTable();
                NewdtDistributors.Columns.AddRange(new DataColumn[6] { new DataColumn("Group"), new DataColumn("Item_code"), new DataColumn("Item_Desc"), new DataColumn("ProductFamily"), new DataColumn("StockCode"), new DataColumn("Price") });
                var searchitem = txtsearch.Text;
                Session["searchItem"] = searchitem;
                foreach (DataRow row in dt.Rows)
                {
                    string desc = row.Field<string>("Item_Desc");
                    string productFamily = row.Field<string>("ProductFamily");
                    //add your own columns to be searched here
                    if (row.Field<string>("Group").Contains(searchitem) || row.Field<string>("Item_code").Contains(searchitem) || (desc.ToUpper()).Contains(searchitem.ToUpper()) || (productFamily.ToUpper()).Contains(searchitem.ToUpper()) || row.Field<string>("StockCode").Contains(searchitem) || row.Field<decimal>("Price").ToString().Contains(searchitem))
                    {
                        //when found copy the row to the cloned table
                        NewdtDistributors.Rows.Add(row.ItemArray);
                    }
                }

                if (NewdtDistributors.Rows.Count != 0)
                {
                    Session["gridstatus"] = "notempty";
                }
                else
                {
                    Session["gridstatus"] = "empty";
                }
                grdListPrice.DataSource = NewdtDistributors;
                grdListPrice.DataBind();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadSecondGrid();", true);
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }

        protected void searchbtn1_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DataTable dt = Session["checkedrows"] as DataTable;
                DataTable NewdtDistributors = new DataTable();
                NewdtDistributors.Columns.AddRange(new DataColumn[6] { new DataColumn("Group"), new DataColumn("Item_code"), new DataColumn("Item_Desc"), new DataColumn("ProductFamily"), new DataColumn("StockCode"), new DataColumn("Price") });
                var searchitem = txtsearch1.Text;

                foreach (DataRow row in dt.Rows)
                {
                    string desc = row.Field<string>("Item_Desc");
                    string productFamily = row.Field<string>("ProductFamily");
                    //add your own columns to be searched here
                    if (row.Field<string>("Group").Contains(searchitem) || row.Field<string>("Item_code").Contains(searchitem) || (desc.ToUpper()).Contains(searchitem.ToUpper()) || (productFamily.ToUpper()).Contains(searchitem.ToUpper()) || row.Field<string>("StockCode").Contains(searchitem) || row.Field<string>("Price").ToString().Contains(searchitem))
                    {
                        //when found copy the row to the cloned table
                        NewdtDistributors.Rows.Add(row.ItemArray);
                    }
                }

                if (NewdtDistributors.Rows.Count != 0)
                {
                    Session["gridstatus"] = "notempty";
                }
                else
                {
                    Session["gridstatus"] = "empty";
                }
                grdListPrice1.DataSource = NewdtDistributors;
                grdListPrice1.DataBind();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadSecondGrid();", true);
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }
    }
}