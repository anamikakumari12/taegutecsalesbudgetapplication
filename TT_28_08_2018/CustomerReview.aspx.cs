﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.PowerBI.Api;
using Microsoft.PowerBI.Api.Models;

namespace TaegutecSalesBudget
{
    public partial class CustomerReview : System.Web.UI.Page
    {
        public string embedToken;
        public string embedUrl;
        public Guid reportId;
        CommonFunctions objCom = new CommonFunctions();
        public static string cter;
        public string report_id = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserName"] != null)
                {
                    try
                    {
                        cter = null;
                       
                        if (Convert.ToBoolean(Session["CompanyCode"]))
                        {
                            if (Session["cter"] == null)
                            {
                                Session["cter"] = "TTA";
                                cter = "TTA";

                            }
                            if (Session["cter"].ToString() == "DUR")
                            {
                                rdBtnDuraCab.Checked = true;
                                rdBtnTaegutec.Checked = false;
                                cter = "DUR";
                            }
                            else
                            {
                                rdBtnTaegutec.Checked = true;
                                rdBtnDuraCab.Checked = false;
                                cter = "TTA";
                            }
                            cterDiv.Visible = true;
                        }


                        using (var client = new PowerBIClient(new Uri(Configurations.ApiUrl), Authentication.GetTokenCredentials()))
                        {
                            // Get a list of reports
                            var reports = client.Reports.GetReportsInGroup(Configurations.WorkspaceId);

                            //// Populate dropdown list
                            foreach (Report item in reports.Value)
                            {
                                // ddlReport.Items.Add(new ListItem(item.Name, item.Id.ToString()));
                                if (item.Name == BMSResource.CustomerReview)
                                {
                                    report_id = item.Id.ToString();
                                }
                            }
                            ViewState["report_id"] = report_id;
                            //// Select first item
                            //ddlReport.SelectedIndex = 2;
                        }
                        // Generate an embed token and populate embed variables
                        LoadReport();

                    }
                    catch (Exception ex)
                    {
                        objCom.LogError(ex);
                    }
                }
            }

        }


        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                if (rdBtnTaegutec.Checked)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                }
                if (rdBtnDuraCab.Checked)
                {
                    Session["cter"] = "DUR";
                    cter = "DUR";
                }
                LoadReport();


            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void clear_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        protected void LoadReport()
        {
            try
            {
                report_id = Convert.ToString(ViewState["report_id"]);
                using (var client = new PowerBIClient(new Uri(Configurations.ApiUrl), Authentication.GetTokenCredentials()))
                {

                    // Retrieve the selected report
                    var report = client.Reports.GetReportInGroup(Configurations.WorkspaceId, new Guid(report_id));

                    // Generate an embed token to view
                    var generateTokenRequestParameters = new GenerateTokenRequest(TokenAccessLevel.View);
                    var tokenResponse = client.Reports.GenerateTokenInGroup(Configurations.WorkspaceId, report.Id, generateTokenRequestParameters);
                    var newUrl = "";
                    // Populate embed variables (to be passed client-side)
                    //if (Convert.ToString(Session["RoleId"]) == "SE")
                    //    newUrl = report.EmbedUrl + "&$filter=vw_se_cust_review/assigned_salesengineer_id eq '" + Convert.ToString(Session["UserId"]) + "'";
                    //else if (Convert.ToString(Session["RoleId"]) == "BM")
                    //    newUrl = report.EmbedUrl + "&$filter=vw_se_cust_review/Customer_region eq '" + Convert.ToString(Session["BranchCode"]) + "'";
                    //else if (Convert.ToString(Session["RoleId"]) == "TM")
                    //    newUrl = report.EmbedUrl + "&$filter=vw_se_cust_review/Customer_region in (" + Convert.ToString(Session["TMBranches"]) + ")";
                    //else if (Convert.ToString(Session["RoleId"]) == "HO")
                    //    newUrl = report.EmbedUrl + "&$filter=vw_se_cust_review/cter eq '" + cter + "'";
                   
                    newUrl = report.EmbedUrl + "&$filter=vw_se_cust_review/assigned_salesengineer_id in (" + Convert.ToString(Session["MappedUsers"]) + ")";
                    if (!string.IsNullOrEmpty(cter))
                        newUrl = newUrl + " and vw_se_cust_review/cter eq '" + cter + "'";
                    objCom.LogMessage(newUrl);

                    report.EmbedUrl = newUrl;
                    embedToken = tokenResponse.Token;
                    embedUrl = report.EmbedUrl;
                    reportId = report.Id;
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
    }
}