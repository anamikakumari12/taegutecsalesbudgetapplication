﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.Owin.Host.SystemWeb;
using System.Web.UI.HtmlControls;

namespace TaegutecSalesBudget
{
    public partial class Site : System.Web.UI.MasterPage
    {
        LoginAuthentication authObj = new LoginAuthentication();
        AdminConfiguration objAdmin = new AdminConfiguration();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx"); }
            authObj.UserName = Convert.ToString(Session["UserName"]);
            authObj.LoginMailID = Convert.ToString(Session["LoginMailId"]);
            string role = Session["RoleId"].ToString();
            string startDate = "", endDate = "";
            int currentDate = Convert.ToInt32(DateTime.Today.ToString("yyyyMMdd"));
            //var menuid = Convert.ToString(Session["Menu_ID"]);
            //string[] menuids = null;
            //menuids = menuid.Split(',');
            //int ID = Convert.ToInt32(menuids[0]);
            ds = authObj.GetAllMenusBL(authObj);
            if (ds.Tables.Count > 1)
            {
                var MenuList = ds.Tables[1].AsEnumerable()

                        .Select(row => new
                        {
                            Menu_ID = row.Field<int>("Menu_ID"),
                            Menu = row.Field<string>("Menu"),
                        })
                        .Distinct().AsEnumerable();
                foreach (var drMenu in MenuList)
                {
                    var SubMenuList = ds.Tables[1].AsEnumerable()
                        .Where(dataRow => dataRow.Field<int>("Menu_ID") == drMenu.Menu_ID && dataRow.Field<int>("SubMenu_ID")>0)
                      .Select(row => new
                      {
                          SubMenu_ID = row.Field<int>("SubMenu_ID"),
                          SubMenu = row.Field<string>("SubMenu_Name"),
                          //URL = row.Field<string>("URL")
                      }).Distinct().AsEnumerable();

                    HtmlGenericControl li = new HtmlGenericControl("li");
                   
                    
                    li.Attributes.Add("class", " dropdown");
                    tabs.Controls.Add(li);
                    HtmlGenericControl anchor = new HtmlGenericControl("a");

                    anchor.InnerText = Convert.ToString(drMenu.Menu);
                    if (SubMenuList.ToList().Count > 0)
                    {

                        anchor.Attributes.Add("href", "#");
                        anchor.Attributes.Add("class", "dropdown-toggle ");
                        anchor.Attributes.Add("data-toggle", "dropdown");
                        HtmlGenericControl span = new HtmlGenericControl("span");
                        span.Attributes.Add("class", "caret");
                        anchor.Controls.Add(span);
                    }
                    else
                    {
                        var MenuURL = ds.Tables[1].AsEnumerable()
                        .Where(dataRow => dataRow.Field<int>("Menu_ID") == drMenu.Menu_ID && dataRow.Field<int>("SubMenu_ID") == 0)
                      .Select(row => new
                      {
                          URL = row.Field<string>("URL")
                      })
                       .Distinct().AsEnumerable();
                        foreach (var url in MenuURL)
                        {
                            anchor.Attributes.Add("href", Convert.ToString(url.URL));
                        }
                    }
                    li.Controls.Add(anchor);
                    if (SubMenuList.ToList().Count > 0)
                    {
                        HtmlGenericControl ul = new HtmlGenericControl("ul");
                        ul.Attributes.Add("class", "dropdown-menu multi-level");
                        ul.Attributes.Add("role", "menu");
                        ul.Attributes.Add("aria-labelledby", "dropdownMenu");


                        foreach (var subMenu in SubMenuList)
                        {
                            var SuperSubMenuList = ds.Tables[1].AsEnumerable()
                          .Where(dataRow => dataRow.Field<int>("Menu_ID") == drMenu.Menu_ID && dataRow.Field<int>("SubMenu_ID") == subMenu.SubMenu_ID && dataRow.Field<int?>("SuperSubMenu_ID") > 0)
                        .Select(row => new
                        {
                            SuperSubMenu_ID = row.Field<int>("SuperSubMenu_ID"),
                            SuperSubMenu = row.Field<string>("SuperSubMenu_Name"),
                            SupersubmenuURL = row.Field<string>("URL")
                        })
                         .Distinct().AsEnumerable();
                            if (SuperSubMenuList.ToList().Count > 0)
                            {
                                HtmlGenericControl ili = new HtmlGenericControl("li");
                                ili.Attributes.Add("class", "dropdown-submenu");
                                ul.Controls.Add(ili);
                                HtmlGenericControl ianchor = new HtmlGenericControl("a");
                                ianchor.Attributes.Add("href", "#");
                                ianchor.InnerText = Convert.ToString(subMenu.SubMenu);
                                ili.Controls.Add(ianchor);

                                HtmlGenericControl ul1 = new HtmlGenericControl("ul");
                                ul1.Attributes.Add("class", "dropdown-menu");

                                foreach (var supersubmenu in SuperSubMenuList)
                                {
                                    HtmlGenericControl ili1 = new HtmlGenericControl("li");
                                   
                                    ul1.Controls.Add(ili1);
                                    HtmlGenericControl ianchor1 = new HtmlGenericControl("a");
                                    ianchor1.Attributes.Add("tabindex", "-1");
                                    ianchor1.Attributes.Add("href", Convert.ToString(supersubmenu.SupersubmenuURL));
                                    ianchor1.InnerText = Convert.ToString(supersubmenu.SuperSubMenu);
                                    ili1.Controls.Add(ianchor1);
                                   
                                }
                                ili.Controls.Add(ul1);
                            }
                            
                            else
                            {
                                var SubMenuurl = ds.Tables[1].AsEnumerable()
                         .Where(dataRow => dataRow.Field<int>("Menu_ID") == drMenu.Menu_ID && dataRow.Field<int>("SubMenu_ID") == subMenu.SubMenu_ID)
                       .Select(row => new
                       {
                          
                           URL = row.Field<string>("URL")
                       })
                        .Distinct().AsEnumerable();
                                HtmlGenericControl ili = new HtmlGenericControl("li");
                                ul.Controls.Add(ili);
                                HtmlGenericControl ianchor = new HtmlGenericControl("a");
                                foreach(var SubMenuurlrow in SubMenuurl)
                                ianchor.Attributes.Add("href", Convert.ToString(SubMenuurlrow.URL));
                                ianchor.InnerText = Convert.ToString(subMenu.SubMenu);
                                ili.Controls.Add(ianchor);
                            }
                        }

                        li.Controls.Add(ul);
                    }
                   
                }

               // var MenuName = ds.Tables[1].AsEnumerable()
               //        .Where(dataRow => dataRow.Field<int>("ID") == ID)
               //      .Select(row => new
               //      {
               //          Menu = row.Field<string>("Menu"),
               //           //URL = row.Field<string>("URL")
               //       }).AsEnumerable();

               //foreach(var row1 in MenuName)
               // {
               //     var URL = ds.Tables[1].AsEnumerable()
               //        .Where(dataRow => dataRow.Field<string>("Menu") == row1.Menu && dataRow.Field<string>("URL") != null)
               //      .Select(row => new
               //      {
               //          URL = row.Field<string>("URL")
               //      }).FirstOrDefault();
               //     Response.Redirect(Convert.ToString(URL.URL));
               // }

                
               
            }




            //displayProjectDashboard();
            //displayBudgetTab();

            if (!IsPostBack)
            {

                System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                if (Session["UserName"] != null)
                {
                    #region SaveModules
                    SaveModules();
                    #endregion
                    if (String.IsNullOrEmpty(Convert.ToString(Session["UserRole"])))
                    {
                        lblUserName.Text = Convert.ToString(Session["UserName"]);
                        lnkAdminProfile.Visible = false;
                    }
                    else
                    {
                        lblUserName.Text = "Admin(Logged In As " + Convert.ToString(Session["UserName"]) + ")";
                        lnkAdminProfile.Visible = true;
                    }

                    if (Session["Territory"].ToString() == "DUR")
                    {
                        usernav.Style.Add("background-color", "#044758 ");
                    }
                }
                //displayBudgetTab();

            }
            if (Session["RoleId"] != null)
            {
                if (Session["RoleId"].ToString() == "Admin")
                {
                    if (Request.QueryString.Get(0) == "Budget") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "Summary") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "Reports") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "Dashboard") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RS") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RO") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RMT") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RV") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RD") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "Quote") { Response.Redirect("AdminProfile.aspx?Profile"); return; }

                    if (Request.QueryString.Get(0) == "MDPEntry" || Request.QueryString.Get(0) == "MDPReports" || Request.QueryString.Get(0) == "Projects")
                    {
                        Response.Redirect("AdminProfile.aspx?Profile");
                        return;
                    }
                }

                //if (Convert.ToString(Session["FocusFlag"]) == "1")
                //{

                //    FP1.Visible = true;
                //    FP2.Visible = true;
                //    FP3.Visible = true;
                //    FP4.Visible = true;
                //    FP5.Visible = true;
                //}
                //if (Convert.ToString(Session["Quote_ref"]) == "1")
                //{

                //    qmList1.Visible = true;
                //    qmList2.Visible = true;
                //    qmList3.Visible = true;
                //    qmList4.Visible = true;
                //    qmList5.Visible = true;
                //    if (Convert.ToString(Session["RoleId"]) == "HO")
                //    {
                //        quoteReqList.Visible = false;
                //        quoteReqList1.Visible = false;
                //        quoteReqList2.Visible = false;
                //        quoteReqList3.Visible = false;
                //        quoteReqList4.Visible = false;
                //    }
                //    else
                //    {
                //        quoteReqList.Visible = true;
                //        quoteReqList1.Visible = true;
                //        quoteReqList2.Visible = true;
                //        quoteReqList3.Visible = true;
                //        quoteReqList4.Visible = true;
                //    }
                //    if (Convert.ToString(Session["RoleId"]) == "SE")
                //    {
                //        quoteList.Visible = false;
                //        quoteList1.Visible = false;
                //        quoteList2.Visible = false;
                //        quoteList3.Visible = false;
                //        quoteList4.Visible = false;
                //    }
                //    else
                //    {
                //        quoteList.Visible = true;
                //        quoteList1.Visible = true;
                //        quoteList2.Visible = true;
                //        quoteList3.Visible = true;
                //        quoteList4.Visible = true;
                //    }

                //}
                //else
                //{
                //    qmList1.Visible = false;
                //    qmList2.Visible = false;
                //    qmList3.Visible = false;
                //    qmList4.Visible = false;
                //    qmList5.Visible = false;
                //}

            }
            Session["organization_id"] = "1";
            Session["p_user_name"] = "bsalla";
            Session["p_user_id"] = "1";
            Session["employee_id"] = "1";
            Session["employee_name"] = "Bakta G Salla";
            Session["exp_supervisor_flag"] = "Y";
            Session["vst_supervisor_flag"] = "Y";
            Session["access_exp_module"] = "1";
            Session["access_vst_module"] = "1";
            Session["access_crm_module"] = "1";
            Session["error_code"] = "0";
            Session["error_message"] = "Login Successful";
        }

        //private void displayProjectDashboard()
        //{
        //    if (Convert.ToString(Session["ProjectDashboardDisplay"]) == "N")
        //    {
        //        Dashboard1.Visible = false;
        //        Dashboard2.Visible = false;
        //        Dashboard3.Visible = false;
        //        Dashboard4.Visible = false;
        //        Dashboard5.Visible = false;
        //    }
        //    else
        //    {
        //        Dashboard1.Visible = true;
        //        Dashboard2.Visible = true;
        //        Dashboard3.Visible = true;
        //        Dashboard4.Visible = true;
        //        Dashboard5.Visible = true;
        //    }
        //}

        protected void lnkbtnLogout_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] != null)
            {

                Session["UserName"] = null;
                Session["LoginMailId"] = null;
                Session["RoleId"] = null;
                Session["UserId"] = null;
                Session["Customer_Number"] = null;
                Session["Project_Number"] = null;
                Session["Distributor_Number"] = null;
                Session["cter"] = null;

                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("Login.aspx");

                //ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }
        protected void confirmpwd_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] != null)
            {

                string strpassword = authObj.Encrypt(pwd.Text);
                authObj.MailPassword = strpassword;
                authObj.EngineerId = Convert.ToString(Session["UserId"]);
                string ErrorMessage = authObj.resetpwd(authObj);
                if (ErrorMessage == null)
                {
                    string scriptString = "<script type='text/javascript'> alert('Password was successfully reset');window.location='Login.aspx';</script>";
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                }
                else
                {
                    string scriptString = "<script type='text/javascript'> alert('Failed to reset');window.location='Login.aspx';</script>";
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        //protected void displayBudgetTab()
        //{
        //    if (Session["RoleId"] != null)
        //    {
        //        string role = Session["RoleId"].ToString();
        //        string startDate = "", endDate = "";
        //        int currentDate = Convert.ToInt32(DateTime.Today.ToString("yyyyMMdd"));
        //        if (role == "SE")
        //        {
        //            startDate = objAdmin.GetProfile("START_DATE_SE");
        //            endDate = objAdmin.GetProfile("END_DATE_SE");
        //            int start = startDate == "" ? -1 : Convert.ToInt32(Regex.Replace(startDate, "-", ""));
        //            int end = endDate == "" ? -1 : Convert.ToInt32(Regex.Replace(endDate, "-", ""));
        //            int maincount = start == -1 ? start : start - currentDate;
        //            int count = end == -1 ? end : end - currentDate;
        //            if (count >= 0 && maincount <= 0)
        //            {
        //                li_budgetTab.Visible = true;
        //                //li_budgetTab1.Visible = true;
        //                //li_budgetTab2.Visible = true;
        //                //li_budgetTab3.Visible = true;
        //                //li_budgetTab4.Visible = true;
        //            }
        //            BDD1.Visible = false;
        //            //BDD2.Visible = false;
        //            //BDD3.Visible = false;
        //            //BDD4.Visible = false;
        //            //BDD5.Visible = false;
        //        }
        //        else if (role == "BM")
        //        {
        //            startDate = objAdmin.GetProfile("START_DATE_BM");
        //            endDate = objAdmin.GetProfile("END_DATE_BM");
        //            int start = startDate == "" ? -1 : Convert.ToInt32(Regex.Replace(startDate, "-", ""));
        //            int end = endDate == "" ? -1 : Convert.ToInt32(Regex.Replace(endDate, "-", ""));
        //            int maincount = start == -1 ? start : start - currentDate;
        //            int count = end == -1 ? end : end - currentDate;
        //            if (count >= 0 && maincount <= 0)
        //            {
        //                li_budgetTab.Visible = true;
        //                //li_budgetTab1.Visible = true;
        //                //li_budgetTab2.Visible = true;
        //                //li_budgetTab3.Visible = true;
        //                //li_budgetTab4.Visible = true;
        //            }
        //        }
        //        else if (role == "TM")
        //        {

        //            startDate = objAdmin.GetProfile("START_DATE_TM");
        //            endDate = objAdmin.GetProfile("END_DATE_TM");
        //            int start = startDate == "" ? -1 : Convert.ToInt32(Regex.Replace(startDate, "-", ""));
        //            int end = endDate == "" ? -1 : Convert.ToInt32(Regex.Replace(endDate, "-", ""));
        //            int maincount = start == -1 ? start : start - currentDate;
        //            int count = end == -1 ? end : end - currentDate;
        //            if (count >= 0 && maincount <= 0)
        //            {
        //                li_budgetTab.Visible = true;
        //                //li_budgetTab1.Visible = true;
        //                //li_budgetTab2.Visible = true;
        //                //li_budgetTab3.Visible = true;
        //                //li_budgetTab4.Visible = true;
        //            }
        //        }
        //        else if (role == "HO")
        //        {
        //            startDate = objAdmin.GetProfile("START_DATE_HO");
        //            endDate = objAdmin.GetProfile("END_DATE_HO");
        //            int start = startDate == "" ? -1 : Convert.ToInt32(Regex.Replace(startDate, "-", ""));
        //            int end = endDate == "" ? -1 : Convert.ToInt32(Regex.Replace(endDate, "-", ""));
        //            int maincount = start == -1 ? start : start - currentDate;
        //            int count = end == -1 ? end : end - currentDate;
        //            if (count >= 0 && maincount <= 0)
        //            {
        //                li_budgetTab.Visible = true;
        //                //li_budgetTab1.Visible = true;
        //                //li_budgetTab2.Visible = true;
        //                //li_budgetTab3.Visible = true;
        //                //li_budgetTab4.Visible = true;
        //            }
        //        }

        //    }
        //}


        [WebMethod]
        public static string SetSession()
        {
            string output = "";
            try
            {
                HttpContext.Current.Session["Chart"] = "gdfjgsdf";

            }
            catch (Exception ex)
            {
                // objCom.ErrorLog(ex);
            }
            return output;
        }



        /// <summary>
        /// Author:K.LakshmiBindu
        /// Desc: For Saving userlogs based on time with module info used by user
        /// Date : Feb 12, 2019
        /// </summary>
        public void SaveModules()
        {
            int i = 0;
            SqlConnection cn = null;

            try
            {
                cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
                SqlCommand cmd = new SqlCommand("sp_saveModuleLogs", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter[] sp = new SqlParameter[]{
               new SqlParameter("@EngineerId",Convert.ToString(Session["UserId"])),
               new SqlParameter("@UniqueId",Convert.ToString(Session["UserGuid"])),
               new SqlParameter("@Module",Request.FilePath)
            };
                cmd.Parameters.AddRange(sp);
                cn.Open();
                i = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }


        protected void lnkAdminProfile_Click(object sender, EventArgs e)
        {
            lblUserName.Text = "Admin";
            lnkAdminProfile.Visible = false;
            authObj = Session["AdminauthObj"] as LoginAuthentication;
            String UserGuid = System.Guid.NewGuid().ToString();
            Session["UserGuid"] = UserGuid;
            Session["UserName"] = authObj.UserName;
            Session["LoginMailId"] = authObj.LoginMailID;
            Session["RoleId"] = authObj.RoleId;
            Session["UserId"] = authObj.EngineerId;
            Session["BranchCode"] = authObj.BranchCode;
            Session["BranchDesc"] = authObj.BranchDesc;
            Session["Territory"] = authObj.Territory;
            Session["EngineerId"] = authObj.EngineerId;
            Session["Password"] = authObj.MailPassword;
            Response.Redirect("AdminProfile.aspx?Profile");
        }
    }
}