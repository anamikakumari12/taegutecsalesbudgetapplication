﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.IO.Compression;
using System.Globalization;
//using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Text;
using iTextSharp.tool.xml;
using System.Web.Services;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters;
using AjaxControlToolkit;
using ClosedXML.Excel;
using System.Net.Mail;
using System.Reflection;
//using iTextSharp.text.html.simpleparser;
//using iTextSharp.text.pdf;

namespace TaegutecSalesBudget
{
    public partial class MonthlyTarget : System.Web.UI.Page
    {

        #region Global Declaration

        Budget objBudget = new Budget();
        AdminConfiguration objConfig = new AdminConfiguration();
        Reports objReports = new Reports();
        Review objRSum = new Review();
        CommonFunctions objCom = new CommonFunctions();
        public static int gridLoadedStatus;
        public static string Sbranchlist;
        public static string cter;
        public static decimal byValueIn;
        public static int tegutecheckedchanged;
        public static string roleid = "";
        #endregion

        #region Events
        /// <summary>
        /// Author :
        /// Created date :
        /// Description :Loading dropdownlist of branch,sales engineer list, customer names, customer numbers, application list, product groups based on user roles
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (!IsPostBack)
                {

                    cter = null;
                    byValueIn = 100000;
                    gridLoadedStatus = 0;
                    string name_desc = "", name_code = "";
                    int count = 0;
                    string strUserId = Session["UserId"].ToString();
                    roleid = strUserId;
                    string roleId = Session["RoleId"].ToString();
                    hdnSearch.Value = roleId;
                    Session["dtcount"] = 1;
                    //LoadBranches();
                    LoadMonth();
                    //LoadCurrentMonth();
                    //load product family details
                    if (Session["RoleId"].ToString() == "HO" || Session["RoleId"].ToString() == "TM")
                    {
                        if (Session["RoleId"].ToString() == "HO")
                        {
                            if (Session["cter"] == null && roleId == "HO")
                            {
                                Session["cter"] = "TTA";
                                cter = "TTA";

                            }

                            if (Session["cter"].ToString() == "DUR")
                            {
                                rdBtnDuraCab.Checked = true;
                                rdBtnTaegutec.Checked = false;
                                cter = "DUR";
                            }
                            else
                            {
                                rdBtnTaegutec.Checked = true;
                                rdBtnDuraCab.Checked = false;
                                cter = "TTA";
                            }
                            cterDiv.Visible = true;
                        }

                        LoadBranches();

                        BranchList_SelectedIndexChanged(null, null);
                        MonthList_SelectedIndexChanged(null, null);
                        SalesEngList_SelectedIndexChanged(null, null);
                        ddlcustomertype_SelectedIndexChanged(null, null);
                        // ApplicationList_SelectedIndexChanged(null, null);
                        //reports_Click(null, null);

                    }

                    else if (Session["RoleId"].ToString() == "BM")
                    {
                        string username = Session["UserName"].ToString();
                        string branchcode = Session["BranchCode"].ToString();
                        string branchDec = Session["BranchDesc"].ToString();
                        Session["SelectedBranchList"] = "'" + branchcode + "'";

                        //bind branch
                        BranchList.Items.Insert(0, branchcode);
                        BranchList.Visible = false;
                        BranchText.Visible = true;
                        BranchText.Value = branchDec;
                        BranchText.Disabled = true;

                        // sales engineers loading
                        DataTable dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);
                        if (dtSalesEngDetails != null)
                        {
                            SalesEngList.DataSource = dtSalesEngDetails;
                            SalesEngList.DataTextField = "EngineerName";
                            SalesEngList.DataValueField = "EngineerId";
                            SalesEngList.DataBind();
                            //ddlSalesEngineerList.Items.Insert(0, "SELECT SALES ENGINEER");
                            // ChkSalesEng.Items.Insert(0, "ALL");
                        }

                        foreach (ListItem val in SalesEngList.Items)
                        {
                            val.Selected = true;
                            if (val.Selected)
                            {
                                count++;
                                name_desc += val.Text + " , ";
                                name_code += val.Value + "','";
                            }
                        }
                        SalesEngList_SelectedIndexChanged(null, null);


                    }
                    else if (Session["RoleId"].ToString() == "SE")
                    {

                        string username = Session["UserName"].ToString();
                        string branchcode = Session["BranchCode"].ToString();
                        string branchDec = Session["BranchDesc"].ToString();
                        Session["SelectedBranchList"] = "'" + branchcode + "'";
                        Session["SelectedSalesEngineers"] = "'" + strUserId + "'";

                        //bind branch
                        BranchList.Items.Insert(0, branchcode);
                        BranchList.Visible = false;
                        BranchText.Visible = true;
                        BranchText.Value = branchDec;
                        BranchText.Disabled = true;

                        //bind Sales engineer
                        SalesEngList.Items.Insert(0, strUserId);
                        SalesEngList.Visible = false;
                        SaleText.Visible = true;
                        SaleText.Value = username;
                        SaleText.Disabled = true;

                        // customers loading
                        DataTable dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, "SE"); ;
                        if (dtCutomerDetails != null)
                        {
                            DataTable dtDeatils = new DataTable();
                            dtDeatils.Columns.Add("customer_number", typeof(string));
                            dtDeatils.Columns.Add("customer_name", typeof(string));
                            for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                            {
                                dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                            }
                            CustNameList.DataSource = dtDeatils;
                            CustNameList.DataTextField = "customer_name";
                            CustNameList.DataValueField = "customer_number";
                            CustNameList.DataBind();
                            //ddlCustomerList.Items.Insert(0, "-- SELECT CUSTOMER --");
                            //ChkCustName.Items.Insert(0, "ALL");

                            CustNumList.DataSource = dtCutomerDetails;
                            CustNumList.DataTextField = "customer_number";
                            CustNumList.DataValueField = "customer_number";
                            CustNumList.DataBind();
                            //ddlCustomerNumber.Items.Insert(0, "-- SELECT CUSTOMER NUMBER --");
                            //ChkCustNum.Items.Insert(0, "ALL");

                            foreach (ListItem val in CustNameList.Items)
                            {
                                val.Selected = true;
                                if (val.Selected)
                                {
                                    count++;
                                    name_desc += val.Text + " , ";
                                    name_code += val.Value + "','";
                                }
                            }

                            foreach (ListItem val in CustNumList.Items)
                            {
                                val.Selected = true;
                                if (val.Selected)
                                {
                                    count++;
                                    name_desc += val.Text + " , ";
                                    name_code += val.Value + "','";
                                }
                            }

                            CustNameList_SelectedIndexChanged(null, null);
                            CustNumList_SelectedIndexChanged(null, null);
                        }

                    }


                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        /// <summary>
        /// Author : Monika 
        /// Date : March 18 2021
        /// Desc : Clicking on "000" value, all the value will be displayed in thousands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Thousand_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                byValueIn = 1000;
                if (gridLoadedStatus == 1)
                {

                    reports_Click(null, null);
                }
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Monika 
        /// Date : March 18 2021
        /// Desc : Clicking on "lakh" value, all the value will be displayed in lakhs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Lakhs_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                byValueIn = 100000;
                if (gridLoadedStatus == 1)
                {

                    reports_Click(null, null);
                }
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        /// <summary>
        /// Author : Monika 
        /// Date : March 18 2021
        /// Desc : Clicking on "Unit" value, all the value will be displayed in unit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Unit_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                byValueIn = 1;
                if (gridLoadedStatus == 1)
                {
                    reports_Click(null, null);
                }
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }



        [WebMethod]
        public static string LoadCurrentMonth()
        {
            string res = "";
            string currentdate = DateTime.Now.Day.ToString();
            int date = Convert.ToInt32(currentdate);
            //int date = 26;
            string currentmonth = DateTime.Now.Month.ToString();
            int monthdate = Convert.ToInt32(currentmonth);
            DateTimeFormatInfo mfi = new DateTimeFormatInfo();
            DateTime tempDate = DateTime.Now.AddMonths(1);
            DateTime tempDate2 = new DateTime(tempDate.Year, tempDate.Month, 1);
            DateTime lastDayOfMonth = tempDate2.AddDays(-1);
            string lastday = lastDayOfMonth.Day.ToString();
            int lastdate = Convert.ToInt32(lastday);
            if (date <= lastdate)
            {
                res = "{\"msg\":\"" + monthdate + "\"}";
            }
            else
            {
                monthdate = monthdate + 1;
                if (monthdate > 12)
                {
                    monthdate = 1;
                }
                res = "{\"msg\":\"" + monthdate + "\"}";
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            return res;
        }

        [WebMethod]
        public static string Submit_Click(string month)
        {

            string res = "";
            Budget objBudget = new Budget();
            var csv = new StringBuilder();
            string year = DateTime.Now.Year.ToString();
            string role = HttpContext.Current.Session["RoleId"].ToString();
            var Json3 = JsonConvert.DeserializeObject<List<object>>(month);
            List<itemDetails> objlist = new List<itemDetails>();
            string[] month1 = Json3.Select(x => x.ToString()).ToArray();
            for (int i = 0; i < month1.Length; i++)
            {
                itemDetails item = new itemDetails();
                string[] data = month1[i].Split(',').ToArray();

                string custnum1 = data[0];
                string quan = data[2];
                string val = data[3];
                string mon = data[1];
                item.custNum = custnum1;
                item.quantity = quan;
                item.role = role;
                item.val = val;
                item.year = year;
                item.mon = mon;
                objlist.Add(item);

                res = objBudget.insertMonthlyTargetValue(custnum1, quan, val, mon, year, role);
            }
            DataTable dt = ToDataTable<itemDetails>(objlist);
            LoadDatatocsv(dt);


            return res;
        }

        public static void LoadDatatocsv(DataTable dt)
        {
            try
            {

                string filepath = ConfigurationManager.AppSettings["ExcelReportPath"] + "_" + Convert.ToString(DateTime.Now.ToString("yyyyMMddTHHmmss"));
                StringBuilder sb = new StringBuilder();

                string[] columnNames = dt.Columns.Cast<DataColumn>().
                                                  Select(column => column.ColumnName).
                                                  ToArray();
                sb.AppendLine(string.Join(",", columnNames));

                foreach (DataRow row in dt.Rows)
                {
                    string[] fields = row.ItemArray.Select(field => field.ToString()).
                                                    ToArray();
                    sb.AppendLine(string.Join(",", fields));
                }

                File.WriteAllText(filepath + ".csv", sb.ToString());
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        public class itemDetails
        {
            public string custNum { get; set; }
            public string quantity { get; set; }
            public string val { get; set; }
            public string mon { get; set; }
            public string year { get; set; }
            public string role { get; set; }



        }

        /// <summary>
        /// Author :
        /// Created date :
        /// Description :  click on branches dropdownlist, se should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BranchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int count = 0;

            string name_desc = "", name_code = "";
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                //    int[] a = BranchList.GetSelectedIndices();
                //     int c = a.Length;



                foreach (ListItem val in BranchList.Items)
                {
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }


                name_code = "'" + name_code;
                string name_desc1 = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));
                Session["SelectedBranchListName"] = name_desc1;
                string branchlist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));

                if (count == BranchList.Items.Count)
                {
                    Session["SelectedBranchList"] = "ALL";
                }
                else
                {
                    Session["SelectedBranchList"] = branchlist;
                }
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                objRSum.BranchCode = roleId == "TM" && branchcode == "ALL" ? userId : branchcode;
                objRSum.roleId = roleId;
                objRSum.flag = "SalesEngineer";
                objRSum.cter = cter;
                DataTable dtData = objRSum.getFilterAreaValue(objRSum);

                if (dtData.Rows.Count != 0)
                {
                    SalesEngList.DataSource = dtData;
                    SalesEngList.DataTextField = "EngineerName";
                    SalesEngList.DataValueField = "EngineerId";
                    SalesEngList.DataBind();
                    // ChkSalesEng.Items.Insert(0, "ALL");
                }
                else
                {
                    SalesEngList.DataSource = dtData;
                    SalesEngList.DataTextField = "EngineerName";
                    SalesEngList.DataValueField = "EngineerId";
                    SalesEngList.DataBind();
                    //ChkSalesEng.Items.Insert(0, "NO SALES ENGINEER");
                }
                foreach (ListItem val in SalesEngList.Items)
                {
                    val.Selected = true;

                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";

                }


                SalesEngList_SelectedIndexChanged(null, null);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        public void MonthList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int count = 0;

            string name_desc = "", name_code = "";
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

                foreach (ListItem val in MonthList.Items)
                {
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                string name_desc1 = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));
                Session["SelectedMonthName"] = name_desc1;
                name_code = "'" + name_code;

                string branchlist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));

                if (count == BranchList.Items.Count)
                {
                    Session["SelectedMonthList"] = "ALL";
                }
                else
                {
                    Session["SelectedMonthList"] = branchlist;
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        /// <summary>
        /// Author :
        /// Created date :
        /// Description : click on se Dropdownlist, customers should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void SalesEngList_SelectedIndexChanged(object sender, EventArgs e)
        {

            string name_desc = "", name_code = "";
            int count = 0;
            string SalesengList = null;
            string SalesengnameList = null; ;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

                foreach (ListItem val in SalesEngList.Items)
                {
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }
                string name_desc1 = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));
                Session["SelectedSalesEngineersName"] = name_desc1;
                name_code = "'" + name_code;

                SalesengList = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
                SalesengnameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

                if (count == SalesEngList.Items.Count)
                {
                    Session["SelectedSalesEngineers"] = "ALL";
                }
                else
                {
                    Session["SelectedSalesEngineers"] = SalesengList;
                }


                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                objRSum.BranchCode = (roleId == "TM" && BranchList.SelectedItem.Value == "ALL") ? userId : branchcode;
                objRSum.salesengineer_id = SalesengList.ToString() == "ALL" || SalesengList.ToString() == "" ? null : SalesengList;


                objRSum.customer_type = ddlcustomertype.SelectedItem.Value;
                objRSum.roleId = roleId;
                objRSum.flag = "CustomerType";
                objRSum.cter = cter;
                DataTable dtData = objRSum.getFilterAreaValue(objRSum);

                if (dtData.Rows.Count != 0)
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                    // ChkCustName.Items.Insert(0, "ALL");
                }
                else
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                    // ChkCustName.Items.Insert(0, "NO CUSTOMER");
                }
                if (dtData.Rows.Count != 0)
                {
                    CustNumList.DataSource = dtData;
                    CustNumList.DataTextField = "customer_number";
                    CustNumList.DataValueField = "customer_number";
                    CustNumList.DataBind();
                    // ChkCustNum.Items.Insert(0, "ALL");
                }
                else
                {
                    CustNumList.DataSource = dtData;
                    CustNumList.DataTextField = "customer_number";
                    CustNumList.DataValueField = "customer_number";
                    CustNumList.DataBind();
                    //  ChkCustNum.Items.Insert(0, "NO CUSTOMER");
                }

                foreach (ListItem val in CustNameList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                foreach (ListItem val in CustNumList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                CustNameList_SelectedIndexChanged(null, null);
                CustNumList_SelectedIndexChanged(null, null);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author :
        /// Created date :
        /// Description : click on cust_name checkbox, cust_no should be checked accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CustNameList_SelectedIndexChanged(object sender, EventArgs e)
        {


            string name_desc = "", name_code = "";
            string c_name_desc = "", c_name_code = "";
            int counter = 0, icounter = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                foreach (ListItem val in CustNameList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                        foreach (ListItem val1 in CustNumList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                icounter++;
                                val1.Selected = true;
                                c_name_desc += val1.Text + " , ";
                                c_name_code += val1.Value + "','";
                            }
                        }
                    }
                    else
                    {
                        foreach (ListItem val1 in CustNumList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                val1.Selected = false;
                            }
                        }
                    }
                }
                string name_desc1 = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));
                string num_desc1 = c_name_desc.Substring(0, Math.Max(0, c_name_desc.Length - 2));
                Session["SelectedCustomerNamesText"] = name_desc1;
                Session["SelectedCustomerNumbersText"] = num_desc1;
                name_code = "'" + name_code;
                string CustomerNamelist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));


                if (CustNameList.Items.Count == counter)
                {
                    Session["SelectedCustomerNames"] = "ALL";
                }
                else
                {
                    Session["SelectedCustomerNames"] = CustomerNamelist;
                }

                c_name_code = "'" + c_name_code;
                string CustomerNumlist = c_name_code.Substring(0, Math.Max(0, c_name_code.Length - 2));
                if (CustNumList.Items.Count == icounter)
                {

                    Session["SelectedCustomerNumbers"] = "ALL";
                }
                else
                {
                    Session["SelectedCustomerNumbers"] = CustomerNumlist;

                }

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }


            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        /// <summary>
        /// Author :
        /// Created date :
        /// Description : click on cust_no checkbox, cust_name should be checked accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CustNumList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            string c_name_desc = "", c_name_code = "";
            int counter = 0, icounter = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                foreach (ListItem val in CustNumList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                        foreach (ListItem val1 in CustNameList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                icounter++;
                                val1.Selected = true;
                                c_name_desc += val1.Text + " , ";
                                c_name_code += val1.Value + "','";
                            }
                        }
                    }
                    else
                    {
                        foreach (ListItem val1 in CustNameList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                val1.Selected = false;
                            }
                        }
                    }
                }

                name_code = "'" + name_code;
                string CustomerNumlist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));


                if (CustNameList.Items.Count == counter)
                {
                    Session["SelectedCustomerNumbers"] = "ALL";
                }
                else
                {
                    Session["SelectedCustomerNumbers"] = CustomerNumlist;
                }

                c_name_code = "'" + c_name_code;
                string CustomerNamelist = c_name_code.Substring(0, Math.Max(0, c_name_code.Length - 2));
                if (CustNumList.Items.Count == icounter)
                {

                    Session["SelectedCustomerNames"] = "ALL";
                }
                else
                {
                    Session["SelectedCustomerNames"] = CustomerNamelist;

                }

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }


            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }



        /// <summary>
        /// Author :
        /// Created date :
        /// Description :click on customer type, customers should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlcustomertype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string name_desc = "", name_code = "";
            int count = 0;
            try
            {
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                string SalesengList = Convert.ToString(Session["SelectedSalesEngineers"]);
                objRSum.BranchCode = (roleId == "TM" && branchcode == "ALL") ? userId : branchcode;
                objRSum.salesengineer_id = SalesengList;
                objRSum.customer_type = ddlcustomertype.SelectedItem.Value;
                objRSum.roleId = roleId;
                objRSum.flag = "CustomerType";
                Session["ddlcustomertype"] = ddlcustomertype.SelectedItem.Value;
                DataTable dtData = objRSum.getFilterAreaValue(objRSum);

                if (dtData.Rows.Count != 0)
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                    // ChkCustName.Items.Insert(0, "ALL");
                }
                else
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                    // ChkCustName.Items.Insert(0, "NO CUSTOMER");
                }
                if (dtData.Rows.Count != 0)
                {
                    CustNumList.DataSource = dtData;
                    CustNumList.DataTextField = "customer_number";
                    CustNumList.DataValueField = "customer_number";
                    CustNumList.DataBind();
                    //ChkCustNum.Items.Insert(0, "ALL");
                }
                else
                {
                    CustNumList.DataSource = dtData;
                    CustNumList.DataTextField = "customer_number";
                    CustNumList.DataValueField = "customer_number";
                    CustNumList.DataBind();
                    // ChkCustNum.Items.Insert(0, "NO CUSTOMER");
                }

                foreach (ListItem val in CustNameList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                foreach (ListItem val in CustNumList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                CustNameList_SelectedIndexChanged(null, null);
                CustNumList_SelectedIndexChanged(null, null);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please click on FILTER to view results');triggerPostGridLodedActions();", true);
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please click on FILTER to view results');triggerPostGridLodedActions();", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();$('#alertmsg').show().delay(5000).fadeOut();", true);

            if (rdBtnTaegutec.Checked)
            {
                Session["cter"] = "TTA";
                cter = "TTA";
            }
            if (rdBtnDuraCab.Checked)
            {
                Session["cter"] = "DUR";
                cter = "DUR";
            }
            //grdviewAllValues.DataSource = null;
            //grdviewAllValues.DataBind();

            //grdviewAllValues.DataBind();


            divgridchart.Visible = false;

            LoadBranches();
            BranchList_SelectedIndexChanged(null, null);
            SalesEngList_SelectedIndexChanged(null, null);

        }
        #endregion

        //[WebMethod]
        //public static List<customer> loadGrid(string param, string custnum,string month)
        //{
        //    Budget objBudget = new Budget();
        //    List<customer> custlist = new List<customer>();
        //    try
        //    {
        //        DataTable dtDeatils = new DataTable();
        //        string[] plist = param.Split(',');
        //        string[] custnumlist = custnum.Split(',');

        //        for (int j = 0; j < plist.Length; j++)
        //        {
        //            //string s = plist[j];
        //            //int start = s.IndexOf("(") + 1;
        //            //int end = s.IndexOf(")", start);
        //            //string result = s.Substring(start, end - start);

        //            string result = Convert.ToString(custnumlist[j]);
        //            DataTable dt = objBudget.GetMonthTargetDetails(result, month);
        //            if (dt.Rows.Count > 0)
        //            {
        //                for (int i = 0; i < dt.Rows.Count; i++)
        //                {

        //                    customer obj = new customer();
        //                    obj.status_SE = Convert.ToString(dt.Rows[i][6]);
        //                    obj.status_BM = Convert.ToString(dt.Rows[i][7]);
        //                    obj.status_HO = Convert.ToString(dt.Rows[i][8]);
        //                    obj.Month = Convert.ToString(dt.Rows[i][2]);
        //                    obj.customerName = plist[j];
        //                    obj.customernum = result;
        //                    obj.quantity = Convert.ToString(dt.Rows[i][4]);
        //                    obj.value = Convert.ToString(dt.Rows[i][5]);
        //                    custlist.Add(obj);

        //                }
        //            }
        //            else
        //            {
        //                customer obj = new customer();
        //                obj.customerName = plist[j];
        //                obj.customernum = result;
        //                custlist.Add(obj);
        //            }

        //        }



        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //    return custlist;
        //}

        protected void reports_Click(object sender, EventArgs e)
        {
            DataTable dtBind = new DataTable();
            try
            {
                msg.Text = "";

                ((DataControlField)grdMonthlySales.Columns
                        .Cast<DataControlField>()
                        .Where(fld => fld.HeaderText == "January")
                        .SingleOrDefault()).Visible = false;

                ((DataControlField)grdMonthlySales.Columns
               .Cast<DataControlField>()
               .Where(fld => fld.HeaderText == "Febraury")
               .SingleOrDefault()).Visible = false;

                ((DataControlField)grdMonthlySales.Columns
                .Cast<DataControlField>()
                .Where(fld => fld.HeaderText == "March")
                .SingleOrDefault()).Visible = false;

                ((DataControlField)grdMonthlySales.Columns
                .Cast<DataControlField>()
                .Where(fld => fld.HeaderText == "April")
                .SingleOrDefault()).Visible = false;

                ((DataControlField)grdMonthlySales.Columns
               .Cast<DataControlField>()
               .Where(fld => fld.HeaderText == "May")
               .SingleOrDefault()).Visible = false;

                ((DataControlField)grdMonthlySales.Columns
                .Cast<DataControlField>()
                .Where(fld => fld.HeaderText == "June")
                .SingleOrDefault()).Visible = false;

                ((DataControlField)grdMonthlySales.Columns
                .Cast<DataControlField>()
                .Where(fld => fld.HeaderText == "July")
                .SingleOrDefault()).Visible = false;

                ((DataControlField)grdMonthlySales.Columns
                .Cast<DataControlField>()
                .Where(fld => fld.HeaderText == "August")
                .SingleOrDefault()).Visible = false;

                ((DataControlField)grdMonthlySales.Columns
                .Cast<DataControlField>()
                .Where(fld => fld.HeaderText == "September")
                .SingleOrDefault()).Visible = false;

                ((DataControlField)grdMonthlySales.Columns
                .Cast<DataControlField>()
                .Where(fld => fld.HeaderText == "October")
                .SingleOrDefault()).Visible = false;

                ((DataControlField)grdMonthlySales.Columns
               .Cast<DataControlField>()
               .Where(fld => fld.HeaderText == "November")
               .SingleOrDefault()).Visible = false;

                ((DataControlField)grdMonthlySales.Columns
               .Cast<DataControlField>()
               .Where(fld => fld.HeaderText == "December")
               .SingleOrDefault()).Visible = false;

                Budget objBudget = new Budget();

                string Branchlist = "";
                string SElist = "";
                string CustNumlist = "";
                if (BranchList.SelectedValue == "")
                {
                    string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                    Branchlist = branchcode + ",";
                }
                else
                {
                    foreach (ListItem item in BranchList.Items)
                    {
                        if (item.Selected)
                        {
                            Branchlist += item.Value + ",";
                        }
                    }

                }

                if (SalesEngList.SelectedValue == "")
                {
                    string userId = Session["UserId"].ToString();
                    SElist = userId + ",";
                }
                else
                {
                    foreach (ListItem item in SalesEngList.Items)
                    {
                        if (item.Selected)
                        {
                            SElist += item.Value + ",";
                        }
                    }
                }
                foreach (ListItem item in CustNumList.Items)
                {
                    if (item.Selected)
                    {
                        CustNumlist += item.Value + ",";
                    }
                }


                Branchlist = Branchlist.Remove(Branchlist.Length - 1).Replace("'", "");

                SElist = SElist.Remove(SElist.Length - 1);
                CustNumlist = CustNumlist.Remove(CustNumlist.Length - 1);
                //byValueIn = Convert.ToDecimal(byValueIn + ".0");
                dtBind = objBudget.GetMonthTargetDetails(Branchlist, SElist, CustNumlist, byValueIn);
                Session.Add("DataTable", dtBind);
                int count= dtBind.Rows.Count;
                Session["dtcount"] = count;
                DataColumn Col = dtBind.Columns.Add("completionRate", typeof(string));
                Col.SetOrdinal(dtBind.Columns.Count - 1);
                string month = "";
                string completiondata = "";

                foreach (ListItem item in MonthList.Items)
                {
                    if (item.Selected)
                    {
                        month += item.Value + ",";
                    }
                }
                month = month.Remove(month.Length - 1);
                string[] monthlist = null;

                //if (byValueIn == 1000 || byValueIn == 100000)
                //{
                //    for (int i = 0; i < dtBind.Rows.Count; i++)
                //    {

                //        decimal budgetvalue = dtBind.Rows[i].ItemArray[5].ToString() == "" || dtBind.Rows[i].ItemArray[5].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[5].ToString());

                //        decimal salesvalue = dtBind.Rows[i].ItemArray[6].ToString() == "" || dtBind.Rows[i].ItemArray[6].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[6].ToString());

                //        decimal askingrate = dtBind.Rows[i].ItemArray[7].ToString() == "" || dtBind.Rows[i].ItemArray[7].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[7].ToString());

                //        decimal salesLm = dtBind.Rows[i].ItemArray[8].ToString() == "" || dtBind.Rows[i].ItemArray[8].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[8].ToString());

                //        decimal canSupplyNow = dtBind.Rows[i].ItemArray[9].ToString() == "" || dtBind.Rows[i].ItemArray[9].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[9].ToString());

                //        decimal openOrder = dtBind.Rows[i].ItemArray[10].ToString() == "" || dtBind.Rows[i].ItemArray[10].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[10].ToString());

                //        decimal totalAllocation = dtBind.Rows[i].ItemArray[11].ToString() == "" || dtBind.Rows[i].ItemArray[11].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[11].ToString());

                //        decimal allocationCM = dtBind.Rows[i].ItemArray[12].ToString() == "" || dtBind.Rows[i].ItemArray[12].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[12].ToString());

                //        decimal salesCurrentYear = dtBind.Rows[i].ItemArray[13].ToString() == "" || dtBind.Rows[i].ItemArray[13].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[13].ToString());

                //        decimal jan = dtBind.Rows[i].ItemArray[14].ToString() == "" || dtBind.Rows[i].ItemArray[14].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[14].ToString());

                //        decimal feb = dtBind.Rows[i].ItemArray[15].ToString() == "" || dtBind.Rows[i].ItemArray[15].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[15].ToString());

                //        decimal mar = dtBind.Rows[i].ItemArray[16].ToString() == "" || dtBind.Rows[i].ItemArray[16].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[16].ToString());

                //        decimal apr = dtBind.Rows[i].ItemArray[17].ToString() == "" || dtBind.Rows[i].ItemArray[17].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[17].ToString());

                //        decimal may = dtBind.Rows[i].ItemArray[18].ToString() == "" || dtBind.Rows[i].ItemArray[18].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[18].ToString());

                //        decimal jun = dtBind.Rows[i].ItemArray[19].ToString() == "" || dtBind.Rows[i].ItemArray[19].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[19].ToString());

                //        decimal jul = dtBind.Rows[i].ItemArray[20].ToString() == "" || dtBind.Rows[i].ItemArray[20].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[20].ToString());

                //        decimal aug = dtBind.Rows[i].ItemArray[21].ToString() == "" || dtBind.Rows[i].ItemArray[21].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[21].ToString());

                //        decimal sep = dtBind.Rows[i].ItemArray[22].ToString() == "" || dtBind.Rows[i].ItemArray[22].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[22].ToString());

                //        decimal oct = dtBind.Rows[i].ItemArray[23].ToString() == "" || dtBind.Rows[i].ItemArray[23].ToString() == null ? 0
                //             : Convert.ToDecimal(dtBind.Rows[i].ItemArray[23].ToString());

                //        decimal nov = dtBind.Rows[i].ItemArray[24].ToString() == "" || dtBind.Rows[i].ItemArray[24].ToString() == null ? 0
                //             : Convert.ToDecimal(dtBind.Rows[i].ItemArray[24].ToString());

                //        decimal dec = dtBind.Rows[i].ItemArray[25].ToString() == "" || dtBind.Rows[i].ItemArray[25].ToString() == null ? 0
                //             : Convert.ToDecimal(dtBind.Rows[i].ItemArray[25].ToString());


                //        budgetvalue = budgetvalue == 0 ? 0 : (budgetvalue / byValueIn);
                //        salesvalue = salesvalue == 0 ? 0 : (salesvalue / byValueIn);
                //        askingrate = askingrate == 0 ? 0 : (askingrate / byValueIn);
                //        salesLm = salesLm == 0 ? 0 : (salesLm / byValueIn);
                //        canSupplyNow = canSupplyNow == 0 ? 0 : (canSupplyNow / byValueIn);
                //        openOrder = openOrder == 0 ? 0 : (openOrder / byValueIn);
                //        totalAllocation = totalAllocation == 0 ? 0 : (totalAllocation / byValueIn);
                //        allocationCM = allocationCM == 0 ? 0 : (allocationCM / byValueIn);
                //        salesCurrentYear = salesCurrentYear == 0 ? 0 : (salesCurrentYear / byValueIn);
                //        jan = jan == 0 ? 0 : (jan / byValueIn);
                //        feb = feb == 0 ? 0 : (feb / byValueIn);
                //        mar = mar == 0 ? 0 : (mar / byValueIn);
                //        apr = apr == 0 ? 0 : (apr / byValueIn);
                //        may = may == 0 ? 0 : (may / byValueIn);
                //        jun = jun == 0 ? 0 : (jun / byValueIn);
                //        jul = jul == 0 ? 0 : (jul / byValueIn);
                //        aug = aug == 0 ? 0 : (aug / byValueIn);
                //        sep = sep == 0 ? 0 : (sep / byValueIn);
                //        oct = oct == 0 ? 0 : (oct / byValueIn);
                //        nov = nov == 0 ? 0 : (nov / byValueIn);
                //        dec = dec == 0 ? 0 : (dec / byValueIn);

                //        decimal cumulateddata = 0;
                //        if (month.Contains(','))
                //        {
                //            decimal askingrate1 = 0;

                //            if (askingrate != 0)
                //            {
                //                monthlist = month.Split(',').ToArray();
                //                for (int j = 0; j < monthlist.Length; j++)
                //                {
                //                    string val = dtBind.Rows[i]["month" + monthlist[j] + ""].ToString();
                //                    decimal currentmonthvalue = Convert.ToDecimal(val);

                //                    currentmonthvalue = currentmonthvalue / byValueIn;
                //                    //if (byValueIn == 1000)
                //                    //{
                //                    //    currentmonthvalue = Decimal.Round(currentmonthvalue) * 1000;
                //                    //}
                //                    //if (byValueIn == 100000)
                //                    //{
                //                    //    currentmonthvalue = Decimal.Round(currentmonthvalue) * 100000;
                //                    //}

                //                    currentmonthvalue = Decimal.Round(currentmonthvalue);

                //                    cumulateddata = cumulateddata + currentmonthvalue;
                //                }
                //                if (cumulateddata != 0)
                //                {
                //                    //if (byValueIn == 1000)
                //                    //{
                //                    //    askingrate1 = Decimal.Round(askingrate) * monthlist.Length * 1000;
                //                    //}
                //                    //if (byValueIn == 100000)
                //                    //{
                //                    //    askingrate1 = Decimal.Round(askingrate) * monthlist.Length * 100000;
                //                    //}

                //                    askingrate1 = Decimal.Round(askingrate) * monthlist.Length;


                //                    if (askingrate1 != 0)
                //                    {
                //                        decimal completionRate = Decimal.Round((((cumulateddata - askingrate1) / askingrate1) * 100), 2);
                //                        if (completionRate < 0)
                //                        {
                //                            if (completionRate == -100)
                //                            {
                //                                completiondata = "0%";
                //                            }
                //                            else
                //                            {
                //                                completiondata = completionRate + "%";
                //                            }
                //                        }
                //                        else
                //                        {
                //                            completiondata = "+" + completionRate + "%";
                //                        }
                //                    }
                //                    else
                //                    {
                //                        completiondata = "0";
                //                    }
                //                }
                //                else
                //                {
                //                    completiondata = "0";
                //                }
                //            }
                //            else
                //            {
                //                completiondata = "0";
                //            }

                //        }
                //        else
                //        {
                //            decimal askingrate1 = 0;

                //            string val = dtBind.Rows[i]["month" + month + ""].ToString();
                //            decimal currentmonthvalue = Convert.ToDecimal(val);
                //            currentmonthvalue = currentmonthvalue / byValueIn;
                //            if (currentmonthvalue != 0 && askingrate != 0)
                //            {

                //                askingrate1 = Decimal.Round(askingrate);
                //                currentmonthvalue = Decimal.Round(currentmonthvalue);

                //                if (askingrate1 != 0)
                //                {
                //                    decimal completionRate = Decimal.Round((((currentmonthvalue - askingrate1) / askingrate1) * 100), 2);
                //                    if (completionRate < 0)
                //                    {
                //                        if (completionRate == -100)
                //                        {
                //                            completiondata = "0%";
                //                        }
                //                        else
                //                        {
                //                            completiondata = completionRate + "%";
                //                        }

                //                    }
                //                    else
                //                    {
                //                        completiondata = "+" + completionRate + "%";
                //                    }
                //                }
                //                else
                //                {
                //                    completiondata = "0";
                //                }
                //            }
                //            else
                //            {
                //                completiondata = "0";
                //            }
                //        }



                //        object[] items = dtBind.Rows[i].ItemArray;

                //        items[5] = Decimal.Round(budgetvalue);
                //        items[6] = Decimal.Round(salesvalue);
                //        items[7] = Decimal.Round(askingrate);
                //        items[8] = Decimal.Round(salesLm);
                //        items[9] = Decimal.Round(canSupplyNow);
                //        items[10] = Decimal.Round(openOrder);
                //        items[11] = Decimal.Round(totalAllocation);
                //        items[12] = Decimal.Round(allocationCM);
                //        items[13] = Decimal.Round(salesCurrentYear);
                //        items[14] = Decimal.Round(jan);
                //        items[15] = Decimal.Round(feb);
                //        items[16] = Decimal.Round(mar);
                //        items[17] = Decimal.Round(apr);
                //        items[18] = Decimal.Round(may);
                //        items[19] = Decimal.Round(jun);
                //        items[20] = Decimal.Round(jul);
                //        items[21] = Decimal.Round(aug);
                //        items[22] = Decimal.Round(sep);
                //        items[23] = Decimal.Round(oct);
                //        items[24] = Decimal.Round(nov);
                //        items[25] = Decimal.Round(dec);
                //        items[62] = completiondata;
                //        dtBind.Rows[i].ItemArray = items;

                //    }
                //}
                //if (byValueIn == 1)
                //{
                //    for (int i = 0; i < dtBind.Rows.Count; i++)
                //    {

                //        decimal budgetvalue = dtBind.Rows[i].ItemArray[5].ToString() == "" || dtBind.Rows[i].ItemArray[5].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[5].ToString());

                //        decimal salesvalue = dtBind.Rows[i].ItemArray[6].ToString() == "" || dtBind.Rows[i].ItemArray[6].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[6].ToString());

                //        decimal askingrate = dtBind.Rows[i].ItemArray[7].ToString() == "" || dtBind.Rows[i].ItemArray[7].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[7].ToString());

                //        decimal salesLm = dtBind.Rows[i].ItemArray[8].ToString() == "" || dtBind.Rows[i].ItemArray[8].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[8].ToString());

                //        decimal canSupplyNow = dtBind.Rows[i].ItemArray[9].ToString() == "" || dtBind.Rows[i].ItemArray[9].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[9].ToString());

                //        decimal openOrder = dtBind.Rows[i].ItemArray[10].ToString() == "" || dtBind.Rows[i].ItemArray[10].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[10].ToString());

                //        decimal totalAllocation = dtBind.Rows[i].ItemArray[11].ToString() == "" || dtBind.Rows[i].ItemArray[11].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[11].ToString());

                //        decimal allocationCM = dtBind.Rows[i].ItemArray[12].ToString() == "" || dtBind.Rows[i].ItemArray[12].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[12].ToString());

                //        decimal salesCurrentYear = dtBind.Rows[i].ItemArray[13].ToString() == "" || dtBind.Rows[i].ItemArray[13].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[13].ToString());

                //        decimal jan = dtBind.Rows[i].ItemArray[14].ToString() == "" || dtBind.Rows[i].ItemArray[14].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[14].ToString());

                //        decimal feb = dtBind.Rows[i].ItemArray[15].ToString() == "" || dtBind.Rows[i].ItemArray[15].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[15].ToString());

                //        decimal mar = dtBind.Rows[i].ItemArray[16].ToString() == "" || dtBind.Rows[i].ItemArray[16].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[16].ToString());

                //        decimal apr = dtBind.Rows[i].ItemArray[17].ToString() == "" || dtBind.Rows[i].ItemArray[17].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[17].ToString());

                //        decimal may = dtBind.Rows[i].ItemArray[18].ToString() == "" || dtBind.Rows[i].ItemArray[18].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[18].ToString());

                //        decimal jun = dtBind.Rows[i].ItemArray[19].ToString() == "" || dtBind.Rows[i].ItemArray[19].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[19].ToString());

                //        decimal jul = dtBind.Rows[i].ItemArray[20].ToString() == "" || dtBind.Rows[i].ItemArray[20].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[20].ToString());

                //        decimal aug = dtBind.Rows[i].ItemArray[21].ToString() == "" || dtBind.Rows[i].ItemArray[21].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[21].ToString());

                //        decimal sep = dtBind.Rows[i].ItemArray[22].ToString() == "" || dtBind.Rows[i].ItemArray[22].ToString() == null ? 0
                //                : Convert.ToDecimal(dtBind.Rows[i].ItemArray[22].ToString());

                //        decimal oct = dtBind.Rows[i].ItemArray[23].ToString() == "" || dtBind.Rows[i].ItemArray[23].ToString() == null ? 0
                //             : Convert.ToDecimal(dtBind.Rows[i].ItemArray[23].ToString());

                //        decimal nov = dtBind.Rows[i].ItemArray[24].ToString() == "" || dtBind.Rows[i].ItemArray[24].ToString() == null ? 0
                //             : Convert.ToDecimal(dtBind.Rows[i].ItemArray[24].ToString());

                //        decimal dec = dtBind.Rows[i].ItemArray[25].ToString() == "" || dtBind.Rows[i].ItemArray[25].ToString() == null ? 0
                //             : Convert.ToDecimal(dtBind.Rows[i].ItemArray[25].ToString());


                //        budgetvalue = budgetvalue == 0 ? 0 : (budgetvalue);
                //        salesvalue = salesvalue == 0 ? 0 : (salesvalue);
                //        askingrate = askingrate == 0 ? 0 : (askingrate);
                //        salesLm = salesLm == 0 ? 0 : (salesLm);
                //        canSupplyNow = canSupplyNow == 0 ? 0 : (canSupplyNow);
                //        openOrder = openOrder == 0 ? 0 : (openOrder);
                //        totalAllocation = totalAllocation == 0 ? 0 : (totalAllocation);
                //        allocationCM = allocationCM == 0 ? 0 : (allocationCM);
                //        salesCurrentYear = salesCurrentYear == 0 ? 0 : (salesCurrentYear);
                //        jan = jan == 0 ? 0 : (jan);
                //        feb = feb == 0 ? 0 : (feb);
                //        mar = mar == 0 ? 0 : (mar);
                //        apr = apr == 0 ? 0 : (apr);
                //        may = may == 0 ? 0 : (may);
                //        jun = jun == 0 ? 0 : (jun);
                //        jul = jul == 0 ? 0 : (jul);
                //        aug = aug == 0 ? 0 : (aug);
                //        sep = sep == 0 ? 0 : (sep);
                //        oct = oct == 0 ? 0 : (oct);
                //        nov = nov == 0 ? 0 : (nov);
                //        dec = dec == 0 ? 0 : (dec);

                //        decimal cumulateddata = 0;
                //        if (month.Contains(','))
                //        {
                //            decimal askingrate1 = 0;

                //            if (askingrate != 0)
                //            {

                //                monthlist = month.Split(',').ToArray();
                //                for (int j = 0; j < monthlist.Length; j++)
                //                {

                //                    string val = dtBind.Rows[i]["month" + monthlist[j] + ""].ToString();
                //                    decimal currentmonthvalue = Convert.ToDecimal(val);
                //                    currentmonthvalue = Decimal.Round(currentmonthvalue);

                //                    currentmonthvalue = currentmonthvalue / byValueIn;
                //                    cumulateddata = cumulateddata + currentmonthvalue;
                //                }
                //                if (cumulateddata != 0)
                //                {
                //                    askingrate1 = Decimal.Round(askingrate) * monthlist.Length;

                //                    if (askingrate1 != 0)
                //                    {
                //                        decimal completionRate = Decimal.Round((((cumulateddata - askingrate1) / askingrate1) * 100), 2);
                //                        if (completionRate < 0)
                //                        {
                //                            if (completionRate == -100)
                //                            {
                //                                completiondata = "0%";
                //                            }
                //                            else
                //                            {
                //                                completiondata = completionRate + "%";
                //                            }
                //                        }
                //                        else
                //                        {
                //                            completiondata = "+" + completionRate + "%";
                //                        }
                //                    }
                //                    else
                //                    {
                //                        completiondata = "0";
                //                    }
                //                }
                //                else
                //                {
                //                    completiondata = "0";
                //                }
                //            }
                //            else
                //            {
                //                completiondata = "0";
                //            }

                //        }
                //        else
                //        {
                //            DataView dv = new DataView(dtBind);

                //            string val = dtBind.Rows[i]["month" + month + ""].ToString();
                //            decimal currentmonthvalue = Convert.ToDecimal(val);
                //            currentmonthvalue = currentmonthvalue / byValueIn;
                //            if (currentmonthvalue != 0 && askingrate != 0)
                //            {

                //                decimal completionRate = Decimal.Round((((currentmonthvalue - askingrate) / askingrate) * 100), 2);
                //                if (completionRate < 0)
                //                {
                //                    if (completionRate == -100)
                //                    {
                //                        completiondata = "0%";
                //                    }
                //                    else
                //                    {
                //                        completiondata = completionRate + "%";
                //                    }
                //                }
                //                else
                //                {
                //                    completiondata = "+" + completionRate + "%";
                //                }
                //            }
                //            else
                //            {
                //                completiondata = "0";
                //            }
                //        }



                //        object[] items = dtBind.Rows[i].ItemArray;

                //        items[5] = Decimal.Round(budgetvalue);
                //        items[6] = Decimal.Round(salesvalue);
                //        items[7] = Decimal.Round(askingrate);
                //        items[8] = Decimal.Round(salesLm);
                //        items[9] = Decimal.Round(canSupplyNow);
                //        items[10] = Decimal.Round(openOrder);
                //        items[11] = Decimal.Round(totalAllocation);
                //        items[12] = Decimal.Round(allocationCM);
                //        items[13] = Decimal.Round(salesCurrentYear);
                //        items[14] = Decimal.Round(jan);
                //        items[15] = Decimal.Round(feb);
                //        items[16] = Decimal.Round(mar);
                //        items[17] = Decimal.Round(apr);
                //        items[18] = Decimal.Round(may);
                //        items[19] = Decimal.Round(jun);
                //        items[20] = Decimal.Round(jul);
                //        items[21] = Decimal.Round(aug);
                //        items[22] = Decimal.Round(sep);
                //        items[23] = Decimal.Round(oct);
                //        items[24] = Decimal.Round(nov);
                //        items[25] = Decimal.Round(dec);
                //        items[62] = completiondata;
                //        dtBind.Rows[i].ItemArray = items;

                //    }
                //}


                string[] custlist = null;
                if (month.Contains(','))
                {
                    monthlist = month.Split(',').ToArray();
                    for (int i = 0; i < monthlist.Length; i++)
                    {
                        if (monthlist[i] == "1")
                        {
                            ((DataControlField)grdMonthlySales.Columns
                            .Cast<DataControlField>()
                            .Where(fld => fld.HeaderText == "January")
                            .SingleOrDefault()).Visible = true;


                        }
                        if (monthlist[i] == "2")
                        {
                            ((DataControlField)grdMonthlySales.Columns
                            .Cast<DataControlField>()
                            .Where(fld => fld.HeaderText == "Febraury")
                            .SingleOrDefault()).Visible = true;


                        }
                        if (monthlist[i] == "3")
                        {
                            ((DataControlField)grdMonthlySales.Columns
                            .Cast<DataControlField>()
                            .Where(fld => fld.HeaderText == "March")
                            .SingleOrDefault()).Visible = true;


                        }
                        if (monthlist[i] == "4")
                        {
                            ((DataControlField)grdMonthlySales.Columns
                            .Cast<DataControlField>()
                            .Where(fld => fld.HeaderText == "April")
                            .SingleOrDefault()).Visible = true;


                        }
                        if (monthlist[i] == "5")
                        {
                            ((DataControlField)grdMonthlySales.Columns
                            .Cast<DataControlField>()
                            .Where(fld => fld.HeaderText == "May")
                            .SingleOrDefault()).Visible = true;


                        }
                        if (monthlist[i] == "6")
                        {
                            ((DataControlField)grdMonthlySales.Columns
                            .Cast<DataControlField>()
                            .Where(fld => fld.HeaderText == "June")
                            .SingleOrDefault()).Visible = true;


                        }
                        if (monthlist[i] == "7")
                        {
                            ((DataControlField)grdMonthlySales.Columns
                            .Cast<DataControlField>()
                            .Where(fld => fld.HeaderText == "July")
                            .SingleOrDefault()).Visible = true;


                        }
                        if (monthlist[i] == "8")
                        {
                            ((DataControlField)grdMonthlySales.Columns
                            .Cast<DataControlField>()
                            .Where(fld => fld.HeaderText == "August")
                            .SingleOrDefault()).Visible = true;


                        }
                        if (monthlist[i] == "9")
                        {
                            ((DataControlField)grdMonthlySales.Columns
                            .Cast<DataControlField>()
                            .Where(fld => fld.HeaderText == "September")
                            .SingleOrDefault()).Visible = true;


                        }
                        if (monthlist[i] == "10")
                        {
                            ((DataControlField)grdMonthlySales.Columns
                            .Cast<DataControlField>()
                            .Where(fld => fld.HeaderText == "October")
                            .SingleOrDefault()).Visible = true;


                        }
                        if (monthlist[i] == "11")
                        {
                            ((DataControlField)grdMonthlySales.Columns
                            .Cast<DataControlField>()
                            .Where(fld => fld.HeaderText == "November")
                            .SingleOrDefault()).Visible = true;


                        }
                        if (monthlist[i] == "12")
                        {
                            ((DataControlField)grdMonthlySales.Columns
                            .Cast<DataControlField>()
                            .Where(fld => fld.HeaderText == "December")
                            .SingleOrDefault()).Visible = true;


                        }

                    }
                }
                else
                {
                    if (month == "1")
                    {
                        ((DataControlField)grdMonthlySales.Columns
                      .Cast<DataControlField>()
                      .Where(fld => fld.HeaderText == "January")
                      .SingleOrDefault()).Visible = true;
                    }
                    if (month == "2")
                    {
                        ((DataControlField)grdMonthlySales.Columns
                        .Cast<DataControlField>()
                        .Where(fld => fld.HeaderText == "Febraury")
                        .SingleOrDefault()).Visible = true;


                    }
                    if (month == "3")
                    {
                        ((DataControlField)grdMonthlySales.Columns
                        .Cast<DataControlField>()
                        .Where(fld => fld.HeaderText == "March")
                        .SingleOrDefault()).Visible = true;


                    }
                    if (month == "4")
                    {
                        ((DataControlField)grdMonthlySales.Columns
                        .Cast<DataControlField>()
                        .Where(fld => fld.HeaderText == "April")
                        .SingleOrDefault()).Visible = true;


                    }
                    if (month == "5")
                    {
                        ((DataControlField)grdMonthlySales.Columns
                        .Cast<DataControlField>()
                        .Where(fld => fld.HeaderText == "May")
                        .SingleOrDefault()).Visible = true;


                    }
                    if (month == "6")
                    {
                        ((DataControlField)grdMonthlySales.Columns
                        .Cast<DataControlField>()
                        .Where(fld => fld.HeaderText == "June")
                        .SingleOrDefault()).Visible = true;


                    }
                    if (month == "7")
                    {
                        ((DataControlField)grdMonthlySales.Columns
                        .Cast<DataControlField>()
                        .Where(fld => fld.HeaderText == "July")
                        .SingleOrDefault()).Visible = true;


                    }
                    if (month == "8")
                    {
                        ((DataControlField)grdMonthlySales.Columns
                        .Cast<DataControlField>()
                        .Where(fld => fld.HeaderText == "August")
                        .SingleOrDefault()).Visible = true;


                    }
                    if (month == "9")
                    {
                        ((DataControlField)grdMonthlySales.Columns
                        .Cast<DataControlField>()
                        .Where(fld => fld.HeaderText == "September")
                        .SingleOrDefault()).Visible = true;


                    }
                    if (month == "10")
                    {
                        ((DataControlField)grdMonthlySales.Columns
                        .Cast<DataControlField>()
                        .Where(fld => fld.HeaderText == "October")
                        .SingleOrDefault()).Visible = true;


                    }
                    if (month == "11")
                    {
                        ((DataControlField)grdMonthlySales.Columns
                        .Cast<DataControlField>()
                        .Where(fld => fld.HeaderText == "November")
                        .SingleOrDefault()).Visible = true;


                    }
                    if (month == "12")
                    {
                        ((DataControlField)grdMonthlySales.Columns
                        .Cast<DataControlField>()
                        .Where(fld => fld.HeaderText == "December")
                        .SingleOrDefault()).Visible = true;


                    }
                }
                int bmcount = 0;
                int hocount = 0;
                int newuser = 0;
                int modifieduser = 0;
                string cnum = "";
                int monthcount = 0;
                if (Session["RoleId"].ToString() == "HO")
                {
                    export.Visible = true;
                    custlist = CustNumlist.Split(',').ToArray();
                    for (int i = 0; i < custlist.Length; i++)
                    {
                        if (month.Contains(','))
                        {
                            monthlist = month.Split(',').ToArray();
                            for (int j = 0; j < monthlist.Length; j++)
                            {
                                monthcount = monthlist.Length;
                                string bmmonth = "Status_BM" + monthlist[j] + "";
                                string homonth = "Status_HO" + monthlist[j] + "";
                                string semonth = "Status_SE" + monthlist[j] + "";
                                string custnum = Convert.ToString(custlist[i]);
                                bool exists = dtBind.AsEnumerable().Where(c => c.Field<string>("customer_number").Equals(custnum)).Count() > 0;
                                if (exists)
                                {
                                    DataRow dataRownew = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[bmmonth]) == "0") && (Convert.ToString(r[semonth]) == "0"));
                                    if (dataRownew != null)
                                    {
                                        newuser = newuser + 1;
                                    }
                                    DataRow dataRowold = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[bmmonth]) == "0") && (Convert.ToString(r[semonth]) == "Y"));
                                    if (dataRowold != null)
                                    {
                                        modifieduser = modifieduser + 1;
                                        cnum += custnum + ",";
                                    }
                                    DataRow dataRow = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[bmmonth]) == "Y"));
                                    if (dataRow != null)
                                    {
                                        bmcount = bmcount + 1;
                                    }
                                    DataRow dataRow1 = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[homonth]) == "Y"));
                                    if (dataRow1 != null)
                                    {
                                        hocount = hocount + 1;
                                    }
                                }
                            }
                        }
                        else
                        {
                            string bmmonth = "Status_BM" + month + "";
                            string homonth = "Status_HO" + month + "";
                            string semonth = "Status_SE" + month + "";
                            monthcount = 1;
                            string custnum = Convert.ToString(custlist[i]);
                            bool exists = dtBind.AsEnumerable().Where(c => c.Field<string>("customer_number").Equals(custnum)).Count() > 0;
                            if (exists)
                            {
                                DataRow dataRownew = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[bmmonth]) == "0") && (Convert.ToString(r[semonth]) == "0"));
                                if (dataRownew != null)
                                {
                                    newuser = newuser + 1;
                                }
                                DataRow dataRowold = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[bmmonth]) == "0") && (Convert.ToString(r[semonth]) == "Y"));
                                if (dataRowold != null)
                                {
                                    modifieduser = modifieduser + 1;
                                    cnum += custnum + ",";
                                }
                                DataRow dataRow = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[bmmonth]) == "Y"));
                                if (dataRow != null)
                                {
                                    bmcount = bmcount + 1;
                                }
                                DataRow dataRow1 = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[homonth]) == "Y"));
                                if (dataRow1 != null)
                                {
                                    hocount = hocount + 1;
                                }
                            }
                        }
                    }
                    int custcount = custlist.Length;
                    int total = custcount * monthcount;
                    if (cnum != "")
                    {
                        cnum = cnum.Remove(cnum.Length - 1);
                    }


                    if (bmcount == 0 && hocount == 0 && newuser == 0)
                    {
                        msg.Text = "Target Values are under process";
                    }
                    if (bmcount == 0 && hocount == 0 && newuser > 0)
                    {
                        msg.Text = "Some of the Values are not submitted yet And New customer details are displaying here.";
                    }
                    if (bmcount != 0 && bmcount != total && newuser > 0)
                    {
                        msg.Text = "Submitted Values And New customer details are displaying here.";
                    }
                    if (bmcount != 0 && bmcount != total && modifieduser > 0)
                    {
                        //msg.Text = ""+ cnum + " are still under process.";
                        msg.Text = "Red colored customer numbers are still under process.";
                    }
                    if (hocount == total)
                    {
                        msg.Text = "Sales are already approved";
                    }

                }
                if (Session["RoleId"].ToString() == "BM" || Session["RoleId"].ToString() == "TM")
                {
                    custlist = CustNumlist.Split(',').ToArray();
                    for (int i = 0; i < custlist.Length; i++)
                    {
                        if (month.Contains(','))
                        {
                            monthlist = month.Split(',').ToArray();
                            for (int j = 0; j < monthlist.Length; j++)
                            {
                                monthcount = monthlist.Length;
                                string bmmonth = "Status_BM" + monthlist[j] + "";
                                string homonth = "Status_HO" + monthlist[j] + "";
                                string custnum = Convert.ToString(custlist[i]);
                                bool exists = dtBind.AsEnumerable().Where(c => c.Field<string>("customer_number").Equals(custnum)).Count() > 0;
                                if (exists)
                                {
                                    DataRow dataRow = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[bmmonth]) == "Y"));
                                    if (dataRow != null)
                                    {
                                        bmcount = bmcount + 1;
                                    }
                                    DataRow dataRow1 = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[homonth]) == "Y"));
                                    if (dataRow1 != null)
                                    {
                                        hocount = hocount + 1;
                                    }
                                }

                            }
                        }
                        else
                        {
                            string bmmonth = "Status_BM" + month + "";
                            string homonth = "Status_HO" + month + "";
                            monthcount = 1;
                            string custnum = Convert.ToString(custlist[i]);
                            bool exists = dtBind.AsEnumerable().Where(c => c.Field<string>("customer_number").Equals(custnum)).Count() > 0;
                            if (exists)
                            {
                                DataRow dataRow = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[bmmonth]) == "Y"));
                                if (dataRow != null)
                                {
                                    bmcount = bmcount + 1;
                                }
                                DataRow dataRow1 = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[homonth]) == "Y"));
                                if (dataRow1 != null)
                                {
                                    hocount = hocount + 1;
                                }
                            }
                        }
                    }
                    int custcount = custlist.Length;
                    int total = custcount * monthcount;
                    if (bmcount == total && hocount != total)
                    {
                        msg.Text = "Sales are already sent for Approval";
                    }
                    if (hocount == total)
                    {
                        msg.Text = "Sales are already approved";
                    }

                }

                if (Session["RoleId"].ToString() == "SE")
                {
                    custlist = CustNumlist.Split(',').ToArray();
                    for (int i = 0; i < custlist.Length; i++)
                    {
                        if (month.Contains(','))
                        {
                            monthlist = month.Split(',').ToArray();
                            for (int j = 0; j < monthlist.Length; j++)
                            {
                                monthcount = monthlist.Length;
                                string homonth = "Status_HO" + monthlist[j] + "";
                                string bmmonth = "Status_SE" + monthlist[j] + "";
                                string custnum = Convert.ToString(custlist[i]);
                                bool exists = dtBind.AsEnumerable().Where(c => c.Field<string>("customer_number").Equals(custnum)).Count() > 0;
                                if (exists)
                                {
                                    DataRow dataRow = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[bmmonth]) == "Y"));
                                    if (dataRow != null)
                                    {
                                        bmcount = bmcount + 1;
                                    }
                                    DataRow dataRow1 = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[homonth]) == "Y"));
                                    if (dataRow1 != null)
                                    {
                                        hocount = hocount + 1;
                                    }
                                }
                            }
                        }
                        else
                        {
                            string homonth = "Status_HO" + month + "";
                            string bmmonth = "Status_SE" + month + "";
                            monthcount = 1;
                            string custnum = Convert.ToString(custlist[i]);
                            bool exists = dtBind.AsEnumerable().Where(c => c.Field<string>("customer_number").Equals(custnum)).Count() > 0;
                            if (exists)
                            {
                                DataRow dataRow = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[bmmonth]) == "Y"));
                                if (dataRow != null)
                                {
                                    bmcount = bmcount + 1;
                                }
                                DataRow dataRow1 = dtBind.AsEnumerable().FirstOrDefault(r => (Convert.ToString(r["customer_number"]) == custnum) && (Convert.ToString(r[homonth]) == "Y"));
                                if (dataRow1 != null)
                                {
                                    hocount = hocount + 1;
                                }
                            }
                        }
                    }

                    int custcount = custlist.Length;
                    int total = custcount * monthcount;
                    if (bmcount == total && hocount != total)
                    {
                        msg.Text = "Sales are already sent for Approval";
                    }
                    if (hocount == total)
                    {
                        msg.Text = "Sales are already approved";
                    }

                }


                if (dtBind.Rows.Count > 0)
                {

                    grdMonthlySales.DataSource = dtBind;
                    grdMonthlySales.DataBind();
                    gridLoadedStatus = 1;
                    string previousMonth = DateTime.Now.AddMonths(-1).ToString("MMMM");
                    string currentMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month);
                    grdMonthlySales.HeaderRow.Cells[8].Text = string.Format("{0} {1}", previousMonth, "Sales");
                    grdMonthlySales.HeaderRow.Cells[6].Text = string.Format("{0}  {1}", "Sales", DateTime.Now.Year);
                    grdMonthlySales.HeaderRow.Cells[11].Text = string.Format("{0} {1}", currentMonth, "Open Order");
                    grdMonthlySales.HeaderRow.Cells[12].Text = string.Format("{0} {1}", currentMonth, "Allocation");
                    grdMonthlySales.HeaderRow.Cells[25].Text = string.Format("{0} {1}", currentMonth, "Sales");
                    decimal total = 0, totaskingrate = 0;
                    decimal totmonthval = 0;
                    decimal totalcompletionrate = 0;
                    string totlpercent = "";
                    grdMonthlySales.FooterRow.Cells[4].Text = "TOTAL";
                    grdMonthlySales.FooterRow.Cells[5].Font.Bold = true;
                    grdMonthlySales.FooterRow.Cells[0].CssClass = "footer";
                    grdMonthlySales.FooterRow.Cells[1].CssClass = "footer";
                    grdMonthlySales.FooterRow.Cells[2].CssClass = "footer";
                    grdMonthlySales.FooterRow.Cells[3].CssClass = "footer";
                    grdMonthlySales.FooterRow.Cells[4].CssClass = "footer1";

                    for (int k = 5; k <= 13; k++)
                    {
                        if (k == 6)
                        {
                            total = dtBind.AsEnumerable().Sum(row => row.Field<int>(dtBind.Columns[k]));
                            grdMonthlySales.FooterRow.Cells[25].Text = total.ToString();
                            grdMonthlySales.FooterRow.Cells[25].Font.Bold = true;
                            grdMonthlySales.FooterRow.Cells[25].CssClass = "footer";
                        }
                        if (k == 9)
                        {
                            total = dtBind.AsEnumerable().Sum(row => row.Field<int>(dtBind.Columns[k]));
                            grdMonthlySales.FooterRow.Cells[11].Text = total.ToString();
                            grdMonthlySales.FooterRow.Cells[11].Font.Bold = true;
                            grdMonthlySales.FooterRow.Cells[11].CssClass = "footer";
                        }
                        if (k == 10)
                        {
                            total = dtBind.AsEnumerable().Sum(row => row.Field<int>(dtBind.Columns[k]));
                            grdMonthlySales.FooterRow.Cells[9].Text = total.ToString();
                            grdMonthlySales.FooterRow.Cells[9].Font.Bold = true;
                            grdMonthlySales.FooterRow.Cells[9].CssClass = "footer";
                        }
                        if (k == 11)
                        {
                            total = dtBind.AsEnumerable().Sum(row => row.Field<int>(dtBind.Columns[k]));
                            grdMonthlySales.FooterRow.Cells[10].Text = total.ToString();
                            grdMonthlySales.FooterRow.Cells[10].Font.Bold = true;
                            grdMonthlySales.FooterRow.Cells[10].CssClass = "footer";
                        }
                        if (k == 13)
                        {
                            total = dtBind.AsEnumerable().Sum(row => row.Field<int>(dtBind.Columns[k]));
                            grdMonthlySales.FooterRow.Cells[6].Text = total.ToString();
                            grdMonthlySales.FooterRow.Cells[6].Font.Bold = true;
                            grdMonthlySales.FooterRow.Cells[6].CssClass = "footer";
                        }
                        if (k != 6 && k != 9 && k != 10 && k != 11 && k != 13)
                        {
                            if (k == 7)
                            {
                                totaskingrate = dtBind.AsEnumerable().Sum(row => row.Field<int>(dtBind.Columns[k]));
                                grdMonthlySales.FooterRow.Cells[7].ID = "askingrate";
                            }

                            total = dtBind.AsEnumerable().Sum(row => row.Field<int>(dtBind.Columns[k]));
                            grdMonthlySales.FooterRow.Cells[k].Text = total.ToString();
                            grdMonthlySales.FooterRow.Cells[k].Font.Bold = true;
                            grdMonthlySales.FooterRow.Cells[k].CssClass = "footer";

                        }
                    }

                    if (month.Contains(','))
                    {
                        monthlist = month.Split(',').ToArray();
                        for (int i = 0; i < monthlist.Length; i++)
                        {
                            int monthval = Convert.ToInt32(monthlist[i]);
                            total = dtBind.AsEnumerable().Sum(row => row.Field<decimal>(dtBind.Columns[monthval + 13]));
                            //total = dtBind.AsEnumerable().Sum(row => row.Field<int>(dtBind.Columns[monthval + 13]));
                            totmonthval += total;
                            grdMonthlySales.FooterRow.Cells[12 + monthval].Text = total.ToString();
                            grdMonthlySales.FooterRow.Cells[12 + monthval].Font.Bold = true;
                            grdMonthlySales.FooterRow.Cells[12 + monthval].CssClass = "footer";
                            grdMonthlySales.FooterRow.Cells[12 + monthval].ID = "txttotalValue" + monthval;
                        }
                    }
                    else
                    {

                        int monthval = Convert.ToInt32(month);
                        total = dtBind.AsEnumerable().Sum(row => row.Field<decimal>(dtBind.Columns[monthval + 13]));
                        //total = dtBind.AsEnumerable().Sum(row => row.Field<int>(dtBind.Columns[monthval + 13]));
                        totmonthval = total;
                        grdMonthlySales.FooterRow.Cells[12 + monthval].Text = total.ToString();
                        grdMonthlySales.FooterRow.Cells[12 + monthval].Font.Bold = true;
                        grdMonthlySales.FooterRow.Cells[12 + monthval].CssClass = "footer txtmonthvalue";
                        //grdMonthlySales.FooterRow.Cells[12 + monthval].ID = "footerId";
                        grdMonthlySales.FooterRow.Cells[12 + monthval].ID = "txttotalValue" + monthval;


                    }
                    if (totaskingrate != 0)
                    {
                        if (month.Contains(','))
                        {
                            totalcompletionrate = decimal.Round((((totmonthval - (totaskingrate * monthlist.Length)) / (totaskingrate * monthlist.Length)) * 100), 2);
                            if (totalcompletionrate < 0)
                            {
                                totlpercent = totalcompletionrate + "%";
                            }
                            if (totalcompletionrate == -100)
                            {
                                totlpercent = "0%";
                            }
                            if (totalcompletionrate > 0 && totalcompletionrate != -100)
                            {
                                totlpercent = "+" + totalcompletionrate + "%";
                            }
                        }
                        else
                        {
                            totalcompletionrate = decimal.Round((((totmonthval - totaskingrate) / totaskingrate) * 100), 2);
                            if (totalcompletionrate < 0)
                            {
                                totlpercent = totalcompletionrate + "%";
                            }
                            if (totalcompletionrate == -100)
                            {
                                totlpercent = "0%";
                            }
                            if (totalcompletionrate > 0 && totalcompletionrate != -100)
                            {
                                totlpercent = "+" + totalcompletionrate + "%";
                            }
                        }

                    }
                    else
                    {
                        totlpercent = "0%";
                    }
                    grdMonthlySales.FooterRow.Cells[26].Text = totlpercent.ToString();
                    grdMonthlySales.FooterRow.Cells[26].Font.Bold = true;
                    grdMonthlySales.FooterRow.Cells[26].CssClass = "footer";
                    grdMonthlySales.FooterRow.Cells[26].ID = "percentage";
                    if (cnum != "")
                    {
                        if (cnum.Contains(','))
                        {
                            string[] customerlist = cnum.Split(',');
                            foreach (GridViewRow item in grdMonthlySales.Rows)
                            {
                                Label l = (Label)item.FindControl("lblCustomerNumber");
                                for (int i = 0; i < customerlist.Length; i++)
                                {
                                    if (l.Text == customerlist[i])
                                        l.ForeColor = System.Drawing.Color.Red;
                                }
                            }
                        }
                        else
                        {
                            foreach (GridViewRow item in grdMonthlySales.Rows)
                            {
                                Label l = (Label)item.FindControl("lblCustomerNumber");

                                if (l.Text == cnum)
                                    l.ForeColor = System.Drawing.Color.Red;
                            }

                        }
                    }
                }
                else
                {
                    grdMonthlySales.DataSource = null;
                    grdMonthlySales.DataBind();
                    gridLoadedStatus = 0;
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }

        //protected void gvProduct_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    decimal GrandTotal = 0;
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        string Total = e.Row.Cells[5].Text;
        //        Label lblToalPrice = e.Row.FindControl("lblToalPrice") as Label;
        //        lblToalPrice.Text = Total.ToString();
        //        GrandTotal += Total;
        //    }
        //    else if (e.Row.RowType == DataControlRowType.Footer)
        //    {
        //        Label lblGroupTotal = e.Row.FindControl("lblGroupTotal") as Label;

        //        lblGroupTotal.Text = " Grand Total : " + GrandTotal.ToString();


        //    }


        //}

        protected void LoadMonth()
        {
            DataTable dtmonth = new DataTable();
            string name_desc = "", name_code = "";
            int count = 0;
            try
            {
                dtmonth.Columns.Add("Month", typeof(string));
                dtmonth.Columns.Add("Value", typeof(string));
                dtmonth.Rows.Add("January", "1");
                dtmonth.Rows.Add("February", "2");
                dtmonth.Rows.Add("March", "3");
                dtmonth.Rows.Add("April", "4");
                dtmonth.Rows.Add("May", "5");
                dtmonth.Rows.Add("June", "6");
                dtmonth.Rows.Add("July", "7");
                dtmonth.Rows.Add("August", "8");
                dtmonth.Rows.Add("September", "9");
                dtmonth.Rows.Add("October", "10");
                dtmonth.Rows.Add("November", "11");
                dtmonth.Rows.Add("December", "12");
                MonthList.DataSource = dtmonth;
                MonthList.DataTextField = "Month";
                MonthList.DataValueField = "Value";
                MonthList.DataBind();
                Session["DtmonthList"] = dtmonth;
                foreach (ListItem val in BranchList.Items)
                {
                    val.Selected = true;

                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    :  Load Branches into ListBox 
        /// </summary>
        protected void LoadBranches()
        {
            try
            {

                string name_desc = "", name_code = "";
                int count = 0;
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Session["BranchCode"].ToString();
                DataTable dtData = new DataTable();
                objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
                objRSum.roleId = roleId;
                objRSum.flag = "Branch";
                objRSum.cter = cter;
                dtData = objRSum.getFilterAreaValue(objRSum);
                BranchList.DataSource = dtData;
                BranchList.DataTextField = "BranchDesc";
                BranchList.DataValueField = "BranchCode";
                BranchList.DataBind();
                Session["DtBranchList"] = dtData;
                foreach (ListItem val in BranchList.Items)
                {
                    val.Selected = true;

                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                // ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);




            }

            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }


        /// <summary>
        ///  Author  : 
        /// Date    : 
        /// Desc    :  add css class to the header of gridview  by value
        /// </summary>
        protected void bindgridColor()
        {
            try
            {
                //if (grdviewAllValues.Rows.Count != 0)
                //{
                //    int color = 0;

                //    foreach (GridViewRow row in grdviewAllValues.Rows)
                //    {
                //        var Flag = row.FindControl("lblFlag") as Label;

                //        if (Flag.Text == "Heading")
                //        {
                //            for (int i = 0; i < row.Cells.Count; i++) { row.Cells[i].CssClass = "HeadergridAll"; }
                //            row.CssClass = "HeadergridAll";
                //        }

                //    }
                //}
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        //protected void export_Click1(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string currentMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month);
        //        string month = DateTime.Now.ToString("MMM");
        //        string year = DateTime.Now.Year.ToString();
        //        string date = DateTime.Now.Date.ToString("yyyy-MM-dd");
        //        //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", ""+month+" " + year + ".xls"));

        //        //Response.ContentType = "application/ms-excel";
        //        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //        Response.AddHeader("content-disposition", "attachment;filename=Sales Report " + date + ".xlsx");
        //        DataTable dt = objBudget.GetMonthTargetDetailsForReport();
        //        DataTable dt1 = objBudget.GetSaleReportForHO();

        //        GridView grdExportExcel = new GridView();
        //        grdExportExcel.DataSource = dt;
        //        grdExportExcel.DataBind();

        //        GridView grdExportExcel1 = new GridView();
        //        grdExportExcel1.DataSource = dt1;
        //        grdExportExcel1.DataBind();

        //        using (XLWorkbook wb = new XLWorkbook())
        //        {
        //            using (StringWriter sw = new StringWriter())
        //            {
        //                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
        //                {
        //                    //  Create a table to contain the grid
        //                    Table table = new Table();
        //                    table.GridLines = grdExportExcel.GridLines;

        //                    foreach (GridViewRow row in grdExportExcel.Rows)
        //                    {
        //                        table.Rows.Add(row);
        //                    }

        //                    table.Rows[0].Height = 30;
        //                    table.Rows[0].BackColor = Color.LightSeaGreen;
        //                    table.Rows[0].Font.Bold = true;

        //                    for (int i = 0; i < table.Rows.Count; i++)
        //                    {
        //                        //if (Convert.ToString(table.Rows[i].Cells[0].Text).Contains("Actual_Sales_DirectCustomer_Plan") || Convert.ToString(table.Rows[i].Cells[0].Text).Contains("Actual_Sales_Channel_Partner") || Convert.ToString(table.Rows[i].Cells[0].Text).Contains("Actual_Sales_Total"))
        //                        //{
        //                        //    table.Rows[i].BackColor = Color.PeachPuff;
        //                        //    table.Rows[i].ForeColor = Color.Black;
        //                        //    table.Rows[i].Font.Bold = true;
        //                        //}
        //                        if (Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HOL DIRECT BILLING") || Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HOE- EXPORT BILLING"))
        //                        {
        //                            table.Rows[i].BackColor = Color.LightGray;
        //                            table.Rows[i].ForeColor = Color.Red;
        //                            table.Rows[i].Font.Bold = true;
        //                        }
        //                        else
        //                        {
        //                            table.Rows[i].BackColor = Color.LightGray;
        //                            table.Rows[i].ForeColor = Color.Black;
        //                            table.Rows[i].Font.Bold = true;
        //                        }
        //                    }

        //                    sw.WriteLine("<table><tr><td colspan=3 style='font-weight: bold; font-size:20px'>SALES PLAN " + currentMonth + " " + year + "</td></tr></table>");
        //                    sw.WriteLine("<table>");

        //                    string territory;
        //                    if (rdBtnTaegutec.Checked)
        //                    {
        //                        territory = "TAEGUTEC";
        //                    }
        //                    else
        //                    {
        //                        territory = "DURACARB";
        //                    }
        //                    sw.WriteLine("<tr><td style='font-weight: bold;'>TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");

        //                    string valueIn = "";
        //                    if (ValInUnit.Checked)
        //                    {
        //                        valueIn = "SALES PLAN(In Units)-" + month + " " + year + "";
        //                    }
        //                    if (ValInThsnd.Checked)
        //                    {
        //                        valueIn = "SALES PLAN(In Thousands)-" + month + " " + year + "";
        //                    }
        //                    if (ValInLakh.Checked)
        //                    {
        //                        valueIn = "SALES PLAN(In Lakhs)-" + month + " " + year + "";
        //                    }
        //                    sw.WriteLine("<tr><td style='font-weight: bold;'>VALUE IN :" + "</td><td colspan=8 style='font-style: italic;'>" + valueIn + "</td></tr>");

        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(BranchList.SelectedItem) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(SalesEngList.SelectedItem) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER TYPE : " + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(ddlcustomertype.SelectedValue) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(CustNameList.SelectedItem) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER NUNBER :" + "</td><td style='font-style: italic; '>" + Convert.ToString(CustNumList.SelectedItem) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>MONTH :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + Convert.ToString(MonthList.SelectedItem) + "</td></tr>");

        //                    sw.WriteLine("</table><br/>");
        //                    table.RenderControl(htw);
        //                }

        //                Response.Write(sw.ToString());

        //            }
        //        }

        //        using (XLWorkbook wb1 = new XLWorkbook())
        //        {
        //            using (StringWriter sw1 = new StringWriter())
        //            {
        //                using (HtmlTextWriter htw1 = new HtmlTextWriter(sw1))
        //                {
        //                    //  Create a table to contain the grid
        //                    Table table1 = new Table();
        //                    table1.GridLines = grdExportExcel1.GridLines;

        //                    foreach (GridViewRow row in grdExportExcel1.Rows)
        //                    {
        //                        table1.Rows.Add(row);
        //                    }

        //                    table1.Rows[0].Height = 30;
        //                    table1.Rows[0].BackColor = Color.LightSeaGreen;
        //                    table1.Rows[0].Font.Bold = true;

        //                    for (int i = 0; i < table1.Rows.Count; i++)
        //                    {
        //                        if (Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("Growth over previous month") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("Growth over previous month"))
        //                        {
        //                            table1.Rows[i].ForeColor = Color.Red;
        //                            table1.Rows[i].Font.Bold = true;
        //                        }
        //                        else
        //                        {
        //                            table1.Rows[i].ForeColor = Color.Black;
        //                            table1.Rows[i].Font.Bold = true;
        //                        }
        //                        //if (Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HOL") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HOE") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HO BILLING"))
        //                        //{
        //                        //    table1.Rows[i].BackColor = Color.LightGray;
        //                        //    table1.Rows[i].Font.Bold = true;
        //                        //}
        //                        //if (Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("Grand Total"))
        //                        //{
        //                        //    table1.Rows[i].BackColor = Color.Black;
        //                        //    table1.Rows[i].ForeColor = Color.White;
        //                        //    table1.Rows[i].Font.Bold = true;
        //                        //}
        //                    }

        //                    sw1.WriteLine("<br/>");
        //                    sw1.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold;text-align:centre; font-size:20px'>SALES " + year + "</td></tr></table>");

        //                    table1.RenderControl(htw1);
        //                }

        //                Response.Write(sw1.ToString());

        //            }
        //        }

        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //        //Response.End();
        //        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
        //        //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
        //        HttpContext.Current.ApplicationInstance.CompleteRequest();

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.LogError(ex);
        //    }
        //}

        #region
        //protected void export_Click1(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string currentMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month);
        //        string month = DateTime.Now.ToString("MMM");
        //        string year = DateTime.Now.Year.ToString();
        //        string date = DateTime.Now.Date.ToString("yyyy-MM-dd");
        //        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "" + month + " " + year + ".xls"));
        //        Response.ContentType = "application/ms-excel";
        //        DataTable dt = objBudget.GetMonthTargetDetailsForReport();
        //        DataTable dt1 = objBudget.GetSaleReportForHO();

        //        GridView grdExportExcel = new GridView();
        //        grdExportExcel.DataSource = dt;
        //        grdExportExcel.DataBind();

        //        GridView grdExportExcel1 = new GridView();
        //        grdExportExcel1.DataSource = dt1;
        //        grdExportExcel1.DataBind();

        //        using (XLWorkbook wb = new XLWorkbook())
        //        {
        //            using (StringWriter sw = new StringWriter())
        //            {
        //                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
        //                {
        //                    //  Create a table to contain the grid
        //                    Table table = new Table();
        //                    table.GridLines = grdExportExcel.GridLines;

        //                    foreach (GridViewRow row in grdExportExcel.Rows)
        //                    {
        //                        table.Rows.Add(row);
        //                    }

        //                    //table.Rows[0].Height = 30;
        //                    //table.Rows[0].BackColor = Color.LightSeaGreen;
        //                    //table.Rows[0].Font.Bold = true;

        //                    if (Convert.ToString(table.Rows[0].Cells[0].Text).Contains("Branch") && Convert.ToString(table.Rows[0].Cells[1].Text).Contains("DirectCustomer Plan") &&
        //                            Convert.ToString(table.Rows[0].Cells[2].Text).Contains("Stockist Plan") && Convert.ToString(table.Rows[0].Cells[3].Text).Contains("Total"))
        //                    {
        //                        table.Rows[0].Cells[0].BackColor = Color.LightGray;
        //                        table.Rows[0].Cells[0].ForeColor = Color.Black;
        //                        table.Rows[0].Cells[0].Font.Bold = true;

        //                        table.Rows[0].Cells[1].BackColor = Color.LightGray;
        //                        table.Rows[0].Cells[1].ForeColor = Color.Black;
        //                        table.Rows[0].Cells[1].Font.Bold = true;

        //                        table.Rows[0].Cells[2].BackColor = Color.LightGray;
        //                        table.Rows[0].Cells[2].ForeColor = Color.Black;
        //                        table.Rows[0].Cells[2].Font.Bold = true;

        //                        table.Rows[0].Cells[3].BackColor = Color.LightGray;
        //                        table.Rows[0].Cells[3].ForeColor = Color.Black;
        //                        table.Rows[0].Cells[3].Font.Bold = true;
        //                    }
        //                    if (Convert.ToString(table.Rows[0].Cells[4].Text).Contains("Actual Sales DirectCustomer Plan") && Convert.ToString(table.Rows[0].Cells[5].Text).Contains("Actual Sales Channel Partner")
        //                       && Convert.ToString(table.Rows[0].Cells[6].Text).Contains("Actual Sales Total"))
        //                    {
        //                        table.Rows[0].Cells[4].BackColor = Color.PeachPuff;
        //                        table.Rows[0].Cells[4].ForeColor = Color.Black;
        //                        table.Rows[0].Cells[4].Font.Bold = true;

        //                        table.Rows[0].Cells[5].BackColor = Color.PeachPuff;
        //                        table.Rows[0].Cells[5].ForeColor = Color.Black;
        //                        table.Rows[0].Cells[5].Font.Bold = true;

        //                        table.Rows[0].Cells[6].BackColor = Color.PeachPuff;
        //                        table.Rows[0].Cells[6].ForeColor = Color.Black;
        //                        table.Rows[0].Cells[6].Font.Bold = true;
        //                    }

        //                    for (int i = 1; i <= table.Rows.Count; i++)
        //                    {
        //                        table.Rows[i].Cells[4].BackColor = Color.PeachPuff;
        //                        table.Rows[i].Cells[4].ForeColor = Color.Black;
        //                        table.Rows[i].Cells[4].Font.Bold = true;

        //                        table.Rows[i].Cells[5].BackColor = Color.PeachPuff;
        //                        table.Rows[i].Cells[5].ForeColor = Color.Black;
        //                        table.Rows[i].Cells[5].Font.Bold = true;

        //                        table.Rows[i].Cells[6].BackColor = Color.PeachPuff;
        //                        table.Rows[i].Cells[6].ForeColor = Color.Black;
        //                        table.Rows[i].Cells[6].Font.Bold = true;

        //                        table.Rows[i].Cells[0].BackColor = Color.LightGray;
        //                        table.Rows[i].Cells[0].ForeColor = Color.Black;
        //                        table.Rows[i].Cells[0].Font.Bold = true;

        //                        table.Rows[i].Cells[1].BackColor = Color.LightGray;
        //                        table.Rows[i].Cells[1].ForeColor = Color.Black;
        //                        table.Rows[i].Cells[1].Font.Bold = true;

        //                        table.Rows[i].Cells[2].BackColor = Color.LightGray;
        //                        table.Rows[i].Cells[2].ForeColor = Color.Black;
        //                        table.Rows[i].Cells[2].Font.Bold = true;

        //                        table.Rows[i].Cells[3].BackColor = Color.LightGray;
        //                        table.Rows[i].Cells[3].ForeColor = Color.Black;
        //                        table.Rows[i].Cells[3].Font.Bold = true;

        //                        if (Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HOL DIRECT BILLING") || Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HOE- EXPORT BILLING"))
        //                        {

        //                            table.Rows[i].ForeColor = Color.Red;
        //                            table.Rows[i].Font.Bold = true;
        //                        }
        //                        if (Convert.ToString(table.Rows[i].Cells[0].Text).Contains("BRANCH TOTAL") || Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HO BILLING") || Convert.ToString(table.Rows[i].Cells[0].Text).Contains("TOTAL COMPANY"))
        //                        {

        //                            table.Rows[i].BackColor = Color.LightGray;
        //                            table.Rows[i].ForeColor = Color.Black;
        //                            table.Rows[i].Font.Bold = true;
        //                        }
        //                        //if (!(Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HOL DIRECT BILLING") && Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HOE- EXPORT BILLING") && Convert.ToString(table.Rows[i].Cells[0].Text).Contains("BRANCH TOTAL")
        //                        //&& Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HO BILLING") && Convert.ToString(table.Rows[i].Cells[0].Text).Contains("TOTAL COMPANY")))
        //                        //{
        //                        //    table.Rows[i].BackColor = Color.LightGray;
        //                        //    table.Rows[i].Cells[3].ForeColor = Color.Black;
        //                        //    table.Rows[i].Cells[3].Font.Bold = false;
        //                        //}
        //                    }


        //                    //if (!(Convert.ToString(table.Rows[0].Cells[0].Text).Contains("Branch") && Convert.ToString(table.Rows[0].Cells[1].Text).Contains("DirectCustomer Plan") && Convert.ToString(table.Rows[0].Cells[2].Text).Contains("Stockist Plan") && Convert.ToString(table.Rows[0].Cells[3].Text).Contains("Total"))
        //                    // && Convert.ToString(table.Rows[0].Cells[4].Text).Contains("Actual Sales DirectCustomer Plan") && Convert.ToString(table.Rows[0].Cells[5].Text).Contains("Actual Sales Channel Partner") && Convert.ToString(table.Rows[0].Cells[6].Text).Contains("Actual Sales Total") && Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HOL DIRECT BILLING") && Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HOE- EXPORT BILLING") && Convert.ToString(table.Rows[i].Cells[0].Text).Contains("BRANCH TOTAL")
        //                    //    && Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HO BILLING") && Convert.ToString(table.Rows[i].Cells[0].Text).Contains("TOTAL COMPANY"))
        //                    //{
        //                    //    table.Rows[i].BackColor = Color.LightGray;
        //                    //}


        //                    sw.WriteLine("<table><tr><td colspan=3 style='font-weight: bold; font-size:20px'>SALES PLAN " + currentMonth + " " + year + "</td></table>");
        //                    sw.WriteLine("<table>");

        //                    string territory;
        //                    if (rdBtnTaegutec.Checked)
        //                    {
        //                        territory = "TAEGUTEC";
        //                    }
        //                    else
        //                    {
        //                        territory = "DURACARB";
        //                    }
        //                    sw.WriteLine("<tr><td style='font-weight: bold;'>TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");

        //                    string valueIn = "";
        //                    if (ValInUnit.Checked)
        //                    {
        //                        valueIn = "SALES PLAN(In Units)-" + month + " " + year + "";
        //                    }
        //                    if (ValInThsnd.Checked)
        //                    {
        //                        valueIn = "SALES PLAN(In Thousands)-" + month + " " + year + "";
        //                    }
        //                    if (ValInLakh.Checked)
        //                    {
        //                        valueIn = "SALES PLAN(In Lakhs)-" + month + " " + year + "";
        //                    }
        //                    sw.WriteLine("<tr><td style='font-weight: bold;'>VALUE IN :" + "</td><td colspan=8 style='font-style: italic;'>" + valueIn + "</td></tr>");

        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(BranchList.SelectedItem) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(SalesEngList.SelectedItem) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER TYPE : " + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(ddlcustomertype.SelectedValue) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(CustNameList.SelectedItem) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER NUNBER :" + "</td><td style='font-style: italic; '>" + Convert.ToString(CustNumList.SelectedItem) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>MONTH :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + Convert.ToString(MonthList.SelectedItem) + "</td></tr>");

        //                    sw.WriteLine("</table><br/>");
        //                    table.RenderControl(htw);
        //                    wb.Worksheets.Add(htw.ToString());
        //                }

        //                Response.Write(sw.ToString());

        //            }
        //        }

        //        using (XLWorkbook wb1 = new XLWorkbook())
        //        {
        //            using (StringWriter sw1 = new StringWriter())
        //            {
        //                using (HtmlTextWriter htw1 = new HtmlTextWriter(sw1))
        //                {
        //                    //  Create a table to contain the grid
        //                    Table table1 = new Table();
        //                    table1.GridLines = grdExportExcel1.GridLines;

        //                    foreach (GridViewRow row in grdExportExcel1.Rows)
        //                    {
        //                        table1.Rows.Add(row);
        //                    }

        //                    //table1.Rows[0].Height = 30;
        //                    //table1.Rows[0].BackColor = Color.LightSeaGreen;
        //                    //table1.Rows[0].Font.Bold = true;

        //                    for (int i = 0; i < table1.Rows.Count; i++)
        //                    {
        //                        if (Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("Growth over previous month") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("Growth over previous month"))
        //                        {
        //                            table1.Rows[i].ForeColor = Color.Red;
        //                            table1.Rows[i].Font.Bold = true;
        //                        }
        //                        if (Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("Branch") && Convert.ToString(table1.Rows[i].Cells[1].Text).Contains("Budget2021") && Convert.ToString(table1.Rows[i].Cells[2].Text).Contains("Jan2021")
        //                            && Convert.ToString(table1.Rows[i].Cells[3].Text).Contains("Feb2021") && Convert.ToString(table1.Rows[i].Cells[4].Text).Contains("Mar2021") && Convert.ToString(table1.Rows[i].Cells[5].Text).Contains("Apr2021")
        //                            && Convert.ToString(table1.Rows[i].Cells[6].Text).Contains("May2021") && Convert.ToString(table1.Rows[i].Cells[7].Text).Contains("Jun2021") && Convert.ToString(table1.Rows[i].Cells[8].Text).Contains("Jul2021")
        //                            && Convert.ToString(table1.Rows[i].Cells[9].Text).Contains("Aug2021") && Convert.ToString(table1.Rows[i].Cells[10].Text).Contains("Sep2021") && Convert.ToString(table1.Rows[i].Cells[11].Text).Contains("Oct2021")
        //                            && Convert.ToString(table1.Rows[i].Cells[12].Text).Contains("Nov2021") && Convert.ToString(table1.Rows[i].Cells[13].Text).Contains("Dec2021") && Convert.ToString(table1.Rows[i].Cells[14].Text).Contains("YTD2021")
        //                            && Convert.ToString(table1.Rows[i].Cells[15].Text).Contains("Achmnt"))
        //                        {
        //                            table1.Rows[i].ForeColor = Color.Black;
        //                            table1.Rows[i].Font.Bold = true;
        //                        }
        //                        if (Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("TOTAL") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HO BILLING") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HOL")
        //                            || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HOE") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HO Billing Total") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("Grand Total"))
        //                        {
        //                            table1.Rows[i].ForeColor = Color.Black;
        //                            table1.Rows[i].Font.Bold = true;
        //                        }

        //                    }

        //                    sw1.WriteLine("<br/>");
        //                    sw1.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold;text-align:centre; font-size:20px'>SALES " + year + "</td></table>");

        //                    table1.RenderControl(htw1);
        //                    wb1.Worksheets.Add(htw1.ToString());
        //                }

        //                Response.Write(sw1.ToString());

        //            }
        //        }

        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //        Response.End();


        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.LogError(ex);
        //    }
        //}
        #endregion

        //protected void export_Click1(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string currentMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month);
        //        string month = DateTime.Now.ToString("MMM");
        //        string year = DateTime.Now.Year.ToString();
        //        string date = DateTime.Now.Date.ToString("yyyy-MM-dd");
        //        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Monthly Sales Report " + year + ".xls"));
        //        Response.ContentType = "application/ms-excel";
        //        DataTable dt = objBudget.GetMonthTargetDetailsForReport();
        //        DataTable dt1 = objBudget.GetSaleReportForHO();

        //        GridView grdExportExcel = new GridView();
        //        grdExportExcel.DataSource = dt;
        //        grdExportExcel.DataBind();

        //        GridView grdExportExcel1 = new GridView();
        //        grdExportExcel1.DataSource = dt1;
        //        grdExportExcel1.DataBind();

        //        using (XLWorkbook wb = new XLWorkbook())
        //        {
        //            using (StringWriter sw = new StringWriter())
        //            {
        //                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
        //                {
        //                    //  Create a table to contain the grid
        //                    Table table = new Table();
        //                    table.GridLines = grdExportExcel.GridLines;

        //                    foreach (GridViewRow row in grdExportExcel.Rows)
        //                    {
        //                        table.Rows.Add(row);
        //                    }

        //                    table.Rows[0].Height = 30;
        //                    table.Rows[0].BackColor = Color.LightSeaGreen;
        //                    table.Rows[0].Font.Bold = true;

        //                    for (int i = 0; i < table.Rows.Count; i++)
        //                    {
        //                        if (Convert.ToString(table.Rows[i].Cells[0].Text).Contains("BRANCH TOTAL") || Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HO BILLING"))
        //                        {
        //                            table.Rows[i].BackColor = Color.Gray;
        //                            table.Rows[i].Font.Bold = true;
        //                        }
        //                        if (Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HOL DIRECT BILLING") || Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HOE- EXPORT BILLING"))
        //                        {
        //                            table.Rows[i].BackColor = Color.LightGray;
        //                            table.Rows[i].Font.Bold = true;
        //                        }
        //                        if (Convert.ToString(table.Rows[i].Cells[0].Text).Contains("TOTAL COMPANY"))
        //                        {
        //                            table.Rows[i].BackColor = Color.Black;
        //                            table.Rows[i].ForeColor = Color.White;
        //                            table.Rows[i].Font.Bold = true;
        //                        }
        //                    }

        //                    sw.WriteLine("<table><tr><td colspan=3 style='font-weight: bold; font-size:20px'>SALES PLAN " + currentMonth + " " + year + "</td></table>");
        //                    sw.WriteLine("<table>");

        //                    string territory;
        //                    if (rdBtnTaegutec.Checked)
        //                    {
        //                        territory = "TAEGUTEC";
        //                    }
        //                    else
        //                    {
        //                        territory = "DURACARB";
        //                    }
        //                    sw.WriteLine("<tr><td style='font-weight: bold;'>TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");

        //                    string valueIn = "";
        //                    if (ValInUnit.Checked)
        //                    {
        //                        valueIn = "SALES PLAN(In Units)-" + month + " " + year + "";
        //                    }
        //                    if (ValInThsnd.Checked)
        //                    {
        //                        valueIn = "SALES PLAN(In Thousands)-" + month + " " + year + "";
        //                    }
        //                    if (ValInLakh.Checked)
        //                    {
        //                        valueIn = "SALES PLAN(In Lakhs)-" + month + " " + year + "";
        //                    }
        //                    sw.WriteLine("<tr><td style='font-weight: bold;'>VALUE IN :" + "</td><td colspan=8 style='font-style: italic;'>" + valueIn + "</td></tr>");

        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(BranchList.SelectedItem) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(SalesEngList.SelectedItem) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER TYPE : " + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(ddlcustomertype.SelectedValue) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(CustNameList.SelectedItem) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER NUNBER :" + "</td><td style='font-style: italic; '>" + Convert.ToString(CustNumList.SelectedItem) + "</td></tr>");
        //                    sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>MONTH :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + Convert.ToString(MonthList.SelectedItem) + "</td></tr>");

        //                    sw.WriteLine("</table><br/>");
        //                    table.RenderControl(htw);
        //                    wb.Worksheets.Add(htw.ToString());
        //                }

        //                Response.Write(sw.ToString());

        //            }
        //        }

        //        using (XLWorkbook wb1 = new XLWorkbook())
        //        {
        //            using (StringWriter sw1 = new StringWriter())
        //            {
        //                using (HtmlTextWriter htw1 = new HtmlTextWriter(sw1))
        //                {
        //                    //  Create a table to contain the grid
        //                    Table table1 = new Table();
        //                    table1.GridLines = grdExportExcel1.GridLines;

        //                    foreach (GridViewRow row in grdExportExcel1.Rows)
        //                    {
        //                        table1.Rows.Add(row);
        //                    }

        //                    table1.Rows[0].Height = 30;
        //                    table1.Rows[0].BackColor = Color.LightSeaGreen;
        //                    table1.Rows[0].Font.Bold = true;

        //                    for (int i = 0; i < table1.Rows.Count; i++)
        //                    {
        //                        if (Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("TOTAL") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HO Billing Total"))
        //                        {
        //                            table1.Rows[i].BackColor = Color.Gray;
        //                            table1.Rows[i].Font.Bold = true;
        //                        }
        //                        if (Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HOL") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HOE") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HO BILLING"))
        //                        {
        //                            table1.Rows[i].BackColor = Color.LightGray;
        //                            table1.Rows[i].Font.Bold = true;
        //                        }
        //                        if (Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("Grand Total"))
        //                        {
        //                            table1.Rows[i].BackColor = Color.Black;
        //                            table1.Rows[i].ForeColor = Color.White;
        //                            table1.Rows[i].Font.Bold = true;
        //                        }
        //                    }

        //                    sw1.WriteLine("<br/>");
        //                    sw1.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold;text-align:centre; font-size:20px'>SALES " + year + "</td></table>");

        //                    table1.RenderControl(htw1);
        //                    wb1.Worksheets.Add(htw1.ToString());
        //                }

        //                Response.Write(sw1.ToString());

        //            }
        //        }

        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //        Response.End();


        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.LogError(ex);
        //    }
        //} 

        protected void export_Click1(object sender, EventArgs e)
        {
            try
            {
                string currentMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month);
                string month = DateTime.Now.ToString("MMM");
                string year = DateTime.Now.Year.ToString();
                string date = DateTime.Now.Date.ToString("yyyy-MM-dd");
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "" + month + " " + year + ".xls"));
                Response.ContentType = "application/ms-excel";
                DataTable dt = objBudget.GetMonthTargetDetailsForReport(cter);
                DataTable dt1 = objBudget.GetSaleReportForHO(cter);

                GridView grdExportExcel = new GridView();
                grdExportExcel.DataSource = dt;
                grdExportExcel.DataBind();

                GridView grdExportExcel1 = new GridView();
                grdExportExcel1.DataSource = dt1;
                grdExportExcel1.DataBind();

                using (XLWorkbook wb = new XLWorkbook())
                {
                    using (StringWriter sw = new StringWriter())
                    {
                        using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                        {
                            //  Create a table to contain the grid
                            Table table = new Table();
                            table.GridLines = grdExportExcel.GridLines;

                            foreach (GridViewRow row in grdExportExcel.Rows)
                            {
                                table.Rows.Add(row);
                            }



                            for (int i = 0; i < table.Rows.Count; i++)
                            {
                                if (i == 0)
                                {
                                    table.Rows[i].Cells[4].BackColor = Color.PeachPuff;
                                    table.Rows[i].Cells[4].ForeColor = Color.Black;
                                    table.Rows[i].Cells[4].Font.Bold = true;

                                    table.Rows[i].Cells[5].BackColor = Color.PeachPuff;
                                    table.Rows[i].Cells[5].ForeColor = Color.Black;
                                    table.Rows[i].Cells[5].Font.Bold = true;

                                    table.Rows[i].Cells[6].BackColor = Color.PeachPuff;
                                    table.Rows[i].Cells[6].ForeColor = Color.Black;
                                    table.Rows[i].Cells[6].Font.Bold = true;

                                    table.Rows[i].Cells[0].BackColor = Color.LightGray;
                                    table.Rows[i].Cells[0].ForeColor = Color.Black;
                                    table.Rows[i].Cells[0].Font.Bold = true;

                                    table.Rows[i].Cells[1].BackColor = Color.LightGray;
                                    table.Rows[i].Cells[1].ForeColor = Color.Black;
                                    table.Rows[i].Cells[1].Font.Bold = true;

                                    table.Rows[i].Cells[2].BackColor = Color.LightGray;
                                    table.Rows[i].Cells[2].ForeColor = Color.Black;
                                    table.Rows[i].Cells[2].Font.Bold = true;

                                    table.Rows[i].Cells[3].BackColor = Color.LightGray;
                                    table.Rows[i].Cells[3].ForeColor = Color.Black;
                                    table.Rows[i].Cells[3].Font.Bold = true;

                                }
                                else
                                {
                                    if (Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HOL DIRECT BILLING") || Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HOE- EXPORT BILLING"))
                                    {
                                        table.Rows[i].Cells[4].BackColor = Color.PeachPuff;
                                        table.Rows[i].Cells[4].ForeColor = Color.Red;
                                        table.Rows[i].Cells[4].Font.Bold = true;

                                        table.Rows[i].Cells[5].BackColor = Color.PeachPuff;
                                        table.Rows[i].Cells[5].ForeColor = Color.Red;
                                        table.Rows[i].Cells[5].Font.Bold = true;

                                        table.Rows[i].Cells[6].BackColor = Color.PeachPuff;
                                        table.Rows[i].Cells[6].ForeColor = Color.Red;
                                        table.Rows[i].Cells[6].Font.Bold = true;

                                        table.Rows[i].Cells[0].BackColor = Color.LightGray;
                                        table.Rows[i].Cells[0].ForeColor = Color.Red;
                                        table.Rows[i].Cells[0].Font.Bold = true;

                                        table.Rows[i].Cells[1].BackColor = Color.LightGray;
                                        table.Rows[i].Cells[1].ForeColor = Color.Red;
                                        table.Rows[i].Cells[1].Font.Bold = true;

                                        table.Rows[i].Cells[2].BackColor = Color.LightGray;
                                        table.Rows[i].Cells[2].ForeColor = Color.Red;
                                        table.Rows[i].Cells[2].Font.Bold = true;

                                        table.Rows[i].Cells[3].BackColor = Color.LightGray;
                                        table.Rows[i].Cells[3].ForeColor = Color.Red;
                                        table.Rows[i].Cells[3].Font.Bold = true;
                                    }
                                    if (Convert.ToString(table.Rows[i].Cells[0].Text).Contains("BRANCH TOTAL") || Convert.ToString(table.Rows[i].Cells[0].Text).Contains("HO BILLING") || Convert.ToString(table.Rows[i].Cells[0].Text).Contains("TOTAL COMPANY"))
                                    {

                                        table.Rows[i].Cells[4].BackColor = Color.PeachPuff;
                                        table.Rows[i].Cells[4].ForeColor = Color.Black;
                                        table.Rows[i].Cells[4].Font.Bold = true;

                                        table.Rows[i].Cells[5].BackColor = Color.PeachPuff;
                                        table.Rows[i].Cells[5].ForeColor = Color.Black;
                                        table.Rows[i].Cells[5].Font.Bold = true;

                                        table.Rows[i].Cells[6].BackColor = Color.PeachPuff;
                                        table.Rows[i].Cells[6].ForeColor = Color.Black;
                                        table.Rows[i].Cells[6].Font.Bold = true;

                                        table.Rows[i].Cells[0].BackColor = Color.LightGray;
                                        table.Rows[i].Cells[0].ForeColor = Color.Black;
                                        table.Rows[i].Cells[0].Font.Bold = true;

                                        table.Rows[i].Cells[1].BackColor = Color.LightGray;
                                        table.Rows[i].Cells[1].ForeColor = Color.Black;
                                        table.Rows[i].Cells[1].Font.Bold = true;

                                        table.Rows[i].Cells[2].BackColor = Color.LightGray;
                                        table.Rows[i].Cells[2].ForeColor = Color.Black;
                                        table.Rows[i].Cells[2].Font.Bold = true;

                                        table.Rows[i].Cells[3].BackColor = Color.LightGray;
                                        table.Rows[i].Cells[3].ForeColor = Color.Black;
                                        table.Rows[i].Cells[3].Font.Bold = true;
                                    }

                                    if (Convert.ToString(table.Rows[i].Cells[0].Text) != "HOL DIRECT BILLING" && Convert.ToString(table.Rows[i].Cells[0].Text) != "HOE- EXPORT BILLING" && Convert.ToString(table.Rows[i].Cells[0].Text) != "BRANCH TOTAL"
                                    && Convert.ToString(table.Rows[i].Cells[0].Text) != "HO BILLING" && Convert.ToString(table.Rows[i].Cells[0].Text) != "TOTAL COMPANY")
                                    {
                                        table.Rows[i].Cells[4].BackColor = Color.PeachPuff;
                                        table.Rows[i].Cells[4].ForeColor = Color.Black;
                                        table.Rows[i].Cells[4].Font.Bold = false;

                                        table.Rows[i].Cells[5].BackColor = Color.PeachPuff;
                                        table.Rows[i].Cells[5].ForeColor = Color.Black;
                                        table.Rows[i].Cells[5].Font.Bold = false;

                                        table.Rows[i].Cells[6].BackColor = Color.PeachPuff;
                                        table.Rows[i].Cells[6].ForeColor = Color.Black;
                                        table.Rows[i].Cells[6].Font.Bold = false;

                                        table.Rows[i].Cells[0].BackColor = Color.LightGray;
                                        table.Rows[i].Cells[0].ForeColor = Color.Black;
                                        table.Rows[i].Cells[0].Font.Bold = false;

                                        table.Rows[i].Cells[1].BackColor = Color.LightGray;
                                        table.Rows[i].Cells[1].ForeColor = Color.Black;
                                        table.Rows[i].Cells[1].Font.Bold = false;

                                        table.Rows[i].Cells[2].BackColor = Color.LightGray;
                                        table.Rows[i].Cells[2].ForeColor = Color.Black;
                                        table.Rows[i].Cells[2].Font.Bold = false;

                                        table.Rows[i].Cells[3].BackColor = Color.LightGray;
                                        table.Rows[i].Cells[3].ForeColor = Color.Black;
                                        table.Rows[i].Cells[3].Font.Bold = false;
                                    }
                                }
                            }

                            //sw.WriteLine("<table><tr><td colspan=3 style='font-weight: bold; font-size:20px'>SALES PLAN " + currentMonth + " " + year + "</td></table>");
                            sw.WriteLine("<table>");

                            string territory;
                            if (rdBtnTaegutec.Checked)
                            {
                                territory = "TAEGUTEC";
                            }
                            else
                            {
                                territory = "DURACARB";
                            }
                            sw.WriteLine("<tr><td style='font-weight: bold;'>TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");

                            string valueIn = "";
                            if (ValInUnit.Checked)
                            {
                                valueIn = "In Units";
                            }
                            if (ValInThsnd.Checked)
                            {
                                valueIn = "In Thousands";
                            }
                            if (ValInLakh.Checked)
                            {
                                valueIn = "In Lakhs";
                            }
                            sw.WriteLine("<tr><td style='font-weight: bold;'>VALUE IN :" + "</td><td colspan=8 style='font-style: italic;'>" + valueIn + "</td></tr>");

                            string salesselected = Session["SelectedSalesEngineers"].ToString();
                            string custname = Session["SelectedCustomerNames"].ToString();
                            string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                            string monthlist = Session["SelectedMonthList"].ToString();
                            string custnum = Session["SelectedCustomerNumbers"].ToString();
                            if (branchcode == "ALL")
                            {
                                sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(branchcode) + "</td></tr>");
                            }
                            else
                            {
                                sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["SelectedBranchListName"]) + "</td></tr>");
                            }
                            if (salesselected == "ALL")
                            {
                                sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(salesselected) + "</td></tr>");

                            }
                            else
                            {
                                sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["SelectedSalesEngineersName"]) + "</td></tr>");

                            }
                            sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER TYPE : " + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(ddlcustomertype.SelectedValue) + "</td></tr>");
                            if (custname == "ALL")
                            {
                                sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(custname) + "</td></tr>");
                            }
                            else
                            {
                                sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["SelectedCustomerNamesText"]) + "</td></tr>");
                            }
                            if (custnum == "ALL")
                            {
                                sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER NUNBER :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(custnum) + "</td></tr>");
                            }
                            else
                            {
                                sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>CUSTOMER NUNBER :" + "</td><td style='font-style: italic; '>" + Convert.ToString(Session["SelectedCustomerNumbersText"]) + "</td></tr>");
                            }
                            if (monthlist == "ALL")
                            {
                                sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>MONTH :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + Convert.ToString(monthlist) + "</td></tr>");
                            }
                            else
                            {
                                sw.WriteLine("<tr><td border='1px' style='font-weight: bold;'>MONTH :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + Convert.ToString(Session["SelectedMonthName"]) + "</td></tr>");

                            }

                            sw.WriteLine("</table><br/><br/>");
                            sw.WriteLine("<table><tr><td colspan=7 style='font-weight: bold; font-size:20px;background-color:Yellow;text-align:center'>SALES PLAN " + currentMonth + " " + year + "</td></table>");
                            table.RenderControl(htw);
                            wb.Worksheets.Add(htw.ToString());
                        }

                        Response.Write(sw.ToString());

                    }
                }

                using (XLWorkbook wb1 = new XLWorkbook())
                {
                    using (StringWriter sw1 = new StringWriter())
                    {
                        using (HtmlTextWriter htw1 = new HtmlTextWriter(sw1))
                        {
                            //  Create a table to contain the grid
                            Table table1 = new Table();
                            table1.GridLines = grdExportExcel1.GridLines;

                            foreach (GridViewRow row in grdExportExcel1.Rows)
                            {
                                table1.Rows.Add(row);
                            }

                            //table1.Rows[0].Height = 30;
                            //table1.Rows[0].BackColor = Color.LightSeaGreen;
                            //table1.Rows[0].Font.Bold = true;

                            for (int i = 0; i < table1.Rows.Count; i++)
                            {
                                if (Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("Growth over previous month") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("Growth over previous month"))
                                {
                                    table1.Rows[i].ForeColor = Color.Red;
                                    table1.Rows[i].Font.Bold = true;
                                }
                                if (Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("Branch") && Convert.ToString(table1.Rows[i].Cells[1].Text).Contains("Budget2021") && Convert.ToString(table1.Rows[i].Cells[2].Text).Contains("Jan2021")
                                    && Convert.ToString(table1.Rows[i].Cells[3].Text).Contains("Feb2021") && Convert.ToString(table1.Rows[i].Cells[4].Text).Contains("Mar2021") && Convert.ToString(table1.Rows[i].Cells[5].Text).Contains("Apr2021")
                                    && Convert.ToString(table1.Rows[i].Cells[6].Text).Contains("May2021") && Convert.ToString(table1.Rows[i].Cells[7].Text).Contains("Jun2021") && Convert.ToString(table1.Rows[i].Cells[8].Text).Contains("Jul2021")
                                    && Convert.ToString(table1.Rows[i].Cells[9].Text).Contains("Aug2021") && Convert.ToString(table1.Rows[i].Cells[10].Text).Contains("Sep2021") && Convert.ToString(table1.Rows[i].Cells[11].Text).Contains("Oct2021")
                                    && Convert.ToString(table1.Rows[i].Cells[12].Text).Contains("Nov2021") && Convert.ToString(table1.Rows[i].Cells[13].Text).Contains("Dec2021") && Convert.ToString(table1.Rows[i].Cells[14].Text).Contains("YTD2021")
                                    && Convert.ToString(table1.Rows[i].Cells[15].Text).Contains("Achmnt"))
                                {
                                    table1.Rows[i].ForeColor = Color.Black;
                                    table1.Rows[i].Font.Bold = true;
                                }
                                if (Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("TOTAL") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HO BILLING") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HOL")
                                    || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HOE") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("HO Billing Total") || Convert.ToString(table1.Rows[i].Cells[0].Text).Contains("Grand Total"))
                                {
                                    table1.Rows[i].ForeColor = Color.Black;
                                    table1.Rows[i].Font.Bold = true;
                                }

                            }

                            sw1.WriteLine("<br/>");
                            sw1.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold;text-align:centre; font-size:20px'>SALES " + year + "</td></table>");

                            table1.RenderControl(htw1);
                            wb1.Worksheets.Add(htw1.ToString());
                        }

                        Response.Write(sw1.ToString());

                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                Response.End();


            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }


        //protected void txtValue5_TextChanged(object sender, EventArgs e)
        //{
        //    string month = "";
        //    decimal total = 0;
        //    decimal totmonthval = 0;
        //    DataTable dtBind = new DataTable();
        //    dtBind = Session["DataTable"] as DataTable;

        //    foreach (ListItem item in MonthList.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            month += item.Value + ",";
        //        }
        //    }
        //    month = month.Remove(month.Length - 1);
        //    string[] monthlist = null;
        //    if (month.Contains(','))
        //    {
        //        monthlist = month.Split(',').ToArray();
        //        for (int i = 0; i < monthlist.Length; i++)
        //        {
        //            int monthval = Convert.ToInt32(monthlist[i]);
        //            total = dtBind.AsEnumerable().Sum(row => row.Field<decimal>(dtBind.Columns[monthval + 13]));
        //            totmonthval += total;
        //            grdMonthlySales.FooterRow.Cells[12 + monthval].Text = total.ToString();
        //            grdMonthlySales.FooterRow.Cells[12 + monthval].Font.Bold = true;
        //            grdMonthlySales.FooterRow.Cells[12 + monthval].CssClass = "footer";
        //            grdMonthlySales.FooterRow.Cells[12 + monthval].ID = "footerId";
        //            //TextBox txtCashCheque = new TextBox();
        //            //grdMonthlySales.FooterRow.Cells[12 + monthval].Controls.Add(txtCashCheque);
        //            //grdMonthlySales.FooterRow.Controls.Add(txtCashCheque);
        //        }
        //    }
        //    else
        //    {

        //        int monthval = Convert.ToInt32(month);
        //        total = dtBind.AsEnumerable().Sum(row => row.Field<decimal>(dtBind.Columns[monthval + 13]));
        //        totmonthval = total;
        //        grdMonthlySales.FooterRow.Cells[12 + monthval].Text = total.ToString();
        //        grdMonthlySales.FooterRow.Cells[12 + monthval].Font.Bold = true;
        //        grdMonthlySales.FooterRow.Cells[12 + monthval].CssClass = "footer";
        //        grdMonthlySales.FooterRow.Cells[12 + monthval].ID = "footerId";
        //        //TextBox txtCashCheque = new TextBox();
        //        //grdMonthlySales.FooterRow.Cells[12 + monthval].Controls.Add(txtCashCheque);
        //        //txtCashCheque.ID = "monthId";
        //        //Label lblmonth = new Label();
        //        //grdMonthlySales.FooterRow.Cells[12 + monthval].Controls.Add(lblmonth);
        //        //lblmonth.ID = "monthId";

        //    }
        //}
    }
}
