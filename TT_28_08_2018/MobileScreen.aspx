﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileScreen.aspx.cs" Inherits="TaegutecSalesBudget.MobileScreen"  %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

 <head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=edge, IE=11"/>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Sales-Budget & Performance Monitoring</title>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/respond.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script>

        $(document).ready(function () {
            //App.initLogin();
        });
     
    </script>
     <style type="text/css">
        .error {
            display:none;
        }
        .emptyfield {
             display:none;
        }
    </style>
    
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" /> 
    <link rel="stylesheet" type="text/css" href="fonts/fsquere/style.css" />
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="fonts/open-sans/open-sans.css" />    
    <link rel="stylesheet" type="text/css" href="css/footable.core.css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <%-- <link href="css/Projectsmain.css" rel="stylesheet" type="text/css" />--%>
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/style_default.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/powerbi.js"></script>

</head>
    

<body class="breakpoint-1200" >
   <%-- <script src="Scripts/powerbi.js"></script>--%>
<%--    <form id="form1" runat="server">
        <asp:DropDownList ID="ddlReport" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Page_Load" />
   <br />--%>


<div id="embedDiv" style="height: 750px; width: 100%;" />
            <script>
                debugger;
                // Read embed token
                var embedToken = "<% =this.embedToken %>";

                // Read embed URL
                var embedUrl = "<% = this.embedUrl %>";

	// Read report Id
	var reportId = "<% = this.reportId %>";

                // Get models (models contains enums)
                var models = window['powerbi-client'].models;

                // Embed configuration is used to describe what and how to embed
                // This object is used when calling powerbi.embed
                // It can also includes settings and options such as filters
                var config = {
                    type: 'report',
                    tokenType: models.TokenType.Embed,
                    accessToken: embedToken,
                    embedUrl: embedUrl,
                    id: reportId,
                    pageName: "ReportSection3c94964a0008f80ecbdc",
                    settings: {
                        filterPaneEnabled: false,
                        navContentPaneEnabled: true,
                        layoutType: models.LayoutType.MobilePortrait
                    }
                };

                // Embed the report within the div element
                var embedDiv = document.getElementById("embedDiv");
                var report = powerbi.embed(embedDiv, config);
            </script>


</body></html>


