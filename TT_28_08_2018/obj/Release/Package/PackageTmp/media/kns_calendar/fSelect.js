(function($) {

    $.fn.fSelect = function(options) {

        if (typeof options == 'string' ) {
            var settings = options;
        }
        else {
            var settings = $.extend({
                placeholder: 'Search for Sales Engineer<span class="fs-arrow"></span>',
                numDisplayed: 0,
                overflowText: '{n} selected',
                searchText: 'Search',
                showSearch: true
            }, options);
        }


        /**
         * Constructor
         */
        function fSelect(select, settings) {
            this.$select = $(select);
            this.settings = settings;
            this.create();
        }


        /**
         * Prototype class
         */
        fSelect.prototype = {
            create: function() {
                var multiple = this.$select.is('[multiple]') ? ' multiple' : '';
                this.$select.wrap('<div class="fs-wrap' + multiple + '"></div>');
                this.$select.before('<div class="fs-label-wrap"><div class="fs-label">' + this.settings.placeholder + '</div></div>');
                this.$select.before('<div class="fs-dropdown hidden"></div>');
                this.$select.addClass('hidden');
                this.reload();
            },

            reload: function() {
                var choices = '';
                if (this.settings.showSearch) {
                    choices += '<div class="fs-search"><input type="search" class="search_sp" placeholder="' + this.settings.searchText + '" /></div>';
                }
                choices += this.buildOptions(this.$select);
                this.$select.siblings('.fs-dropdown').html(choices);
                this.reloadDropdownLabel();
				$('div.fs-option').each(function(){
						$(this).hide();
				});
				var newArray = [];
					$('.reset').click(function(){
						$('.search_sp').val('');
						$('.fs-label').text('Search For SP')
						$('div.fs-option').each(function(){
						$(this).hide();
						});
					   var date = $("#calendar").fullCalendar('getDate');
						var d = new Date(date);
						//var month_int = date.getMonth();
						month1 = d.getMonth();
						newArray = [];
						var stringJson1 = JSON.stringify(newArray);
						call_ajax(month1, stringJson1);
						$('#calendar .fc-event').css('visibility','visible');
						$('.demo').val('');
						$('div.fs-option').each(function(){
						$(this).removeClass('selected');
						});
					});

				
					var countUL = 0;
					function get_sp(){
						if($('.demo').val()){
						$('#calendar .fc-event').css('visibility','hidden');
						var array = $('.demo').val().toString().split(",");
						}else{
						$('#calendar .fc-event').css('visibility','visible');
						newArray = [];
						}
						 if(!newArray[countUL]){
							newArray[countUL] = [];
						}
						var date = $("#calendar").fullCalendar('getDate');
						var d = new Date(date);
						//var month_int = date.getMonth();
						month1 = d.getMonth();
						var countLI = 0;
						$('div.selected').each(function(){
						
							val = $(this).attr('data-value');
							$('#calendar').find('.sale_person_'+val).css('visibility','visible');
							newArray[countLI] = val;
							countLI = countLI + 1;
						});
						
						var stringJson1 = JSON.stringify(newArray);
						call_ajax(month1, stringJson1);
					}
					
					/******select Sp *******************/ 
					$('div.list_of_sales table').on('click','.fs-option', function(){
					setTimeout(get_sp, 600);
					});
					
					/******calendar prev and next*******************/ 
					$('.fc-prev-button, .fc-next-button').click(function(){
					get_sp();
					});
                },

            destroy: function() {
                var $wrap = this.$select.closest('.fs-wrap');
                $wrap.find('.fs-label-wrap').remove();
                $wrap.find('.fs-dropdown').remove();
                this.$select.unwrap().removeClass('hidden');
            },

            buildOptions: function($element) {
                var $this = this;

                var choices = '';
                $element.children().each(function(i, el) {
                    var $el = $(el);

                    if ('optgroup' == $el.prop('nodeName').toLowerCase()) {
                        choices += '<div class="fs-optgroup">';
                        choices += '<div class="fs-optgroup-label">' + $el.prop('label') + '</div>';
                        choices += $this.buildOptions($el);
                        choices += '</div>';
                    }
                    else {
                        var selected = $el.is('[selected]') ? ' selected' : '';
                        choices += '<div class="fs-option' + selected + '" data-value="' + $el.prop('value') + '"><span class="fs-checkbox"><i></i></span><div class="fs-option-label">' + $el.html() + '</div></div>';
                    }
                });

                return choices;
            },

            reloadDropdownLabel: function() {
                var $wrap = this.$select.closest('.fs-wrap');
                var settings = this.settings;
                var labelText = [];

                $wrap.find('.fs-option.selected').each(function(i, el) {
                    labelText.push($(el).find('.fs-option-label').text());
                });

                if (labelText.length < 1) {
                    labelText = settings.placeholder;
                }
                else if (labelText.length > settings.numDisplayed) {
                    labelText = settings.overflowText.replace('{n}', labelText.length);
                }
                else {
                    labelText = labelText.join(', ');
                }

                $wrap.find('.fs-label').html(labelText);
                this.$select.trigger('change');
            }
        }


        /**
         * Loop through each matching element
         */
        return this.each(function() {
            var data = $(this).data('fSelect');

            if (!data) {
                data = new fSelect(this, settings);
                $(this).data('fSelect', data);
            }

            if (typeof settings == 'string') {
                data[settings]();
            }
        });
    }


    /**
     * Events
     */
    $(document).on('click touchstart', '.fs-label', function() {
        var $wrap = $(this).closest('.fs-wrap');
        $wrap.find('.fs-dropdown').toggleClass('hidden');
        $wrap.find('.fs-search input').focus();
    });

    $(document).on('click touchstart', '.fs-option', function() {
        var $this = this;
        var $wrap = $(this).closest('.fs-wrap');

        // Multiple select
        if ($wrap.hasClass('multiple')) {
            var selected = [];

            $(this).toggleClass('selected');
            $wrap.find('.fs-option.selected').each(function(i, el) {
                selected.push($(el).attr('data-value'));
            });
        }
        // Single select
        else {
            var selected = $(this).attr('data-value');
            $wrap.find('.fs-option').removeClass('selected');
            $(this).addClass('selected');
            $wrap.find('.fs-dropdown').hide();
        }

        $wrap.find('select').val(selected);
        $wrap.find('select').fSelect('reloadDropdownLabel');
    });

    $(document).on('click touchstart', function(e) {
        var $wrap = $(e.target).closest('.fs-wrap');
        if ($wrap.length < 1) {
            $('.fs-dropdown').addClass('hidden');
        }
        else {
            var is_hidden = $wrap.find('.fs-dropdown').hasClass('hidden');
            $('.fs-dropdown').addClass('hidden');
            if (!is_hidden) {
                $wrap.find('.fs-dropdown').removeClass('hidden');
            }
        }
    });

    $(document).on('keyup', '.fs-search input', function() {
		if($(this).val()){
		 $('div.fs-option').each(function(){
			$(this).show();
			});
		}else{
		 $('div.fs-option').each(function(){
			$(this).hide();
			});
		}
	  
        var $wrap = $(this).closest('.fs-wrap');
        var keywords = $(this).val();

        $wrap.find('.fs-option, .fs-optgroup-label').removeClass('hidden');

        if ('' != keywords) {
            $wrap.find('.fs-option').each(function() {
                //var regex = new RegExp(keywords, 'gi');
				//var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
				var regex = new RegExp("^"+keywords, 'i');
				var val = $(this).find('.fs-option-label').text().split(". ");
				var dropValue = val[1];
				//alert(dropValue.indexOf(keywords));
				//alert(keywords);
				//if(dropValue.indexOf(keywords) != 0){
                if (!regex.test(dropValue)) {
                    $(this).addClass('hidden');
                }
            });

            $wrap.find('.fs-optgroup-label').each(function() {
                var num_visible = $(this).closest('.fs-optgroup').find('.fs-option:not(.hidden)').length;
                if (num_visible < 1) {
                    $(this).addClass('hidden');
                }
            });
        }
    });

})(jQuery);