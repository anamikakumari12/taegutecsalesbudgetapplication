﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReviewVariation.aspx.cs"  EnableEventValidation="false" Inherits="TaegutecSalesBudget.ReviewVariation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<%--<link href="GridviewScroll.css" rel="stylesheet" />
   <script type="text/javascript" src="gridscroll.js"></script>
   <script type="text/javascript" src="js/app.js"></script>--%>

    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <style type="text/css"> 

         .btn.green {
            color: #fff;
            margin-left: auto;
            margin-right: auto;
            overflow: auto;
            cursor: pointer;
            top: -9px;
            position: relative;
            width: 100px;
            font-weight: 600;
            margin-top: 12px;
        }


         .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }   

       .control {
            padding-top: 2px;
        }


       .control_dropdown {
          width: 180px;
          height: 30px;
          border-radius: 4px!important;
          position: relative;
        }


        .HeadergridAll {
            background: #ebeef5;
            color: #fff;
            font-weight: 600;
            text-align: center;
            border-color: #ebeef5;
            font-size: 12px;          
        }

      .sum_total {
      font-size:14px;
      background-color:#999;
      color:#fff;
      }

      .grand_total {
      font-size:14px;
      background-color:#666;
      color:#fff;
      }

      #salesbycustomer table tr:last-child ,#salesbycustomertotals table tr:last-child{
      background-color:#999;
      }

       #MainContent_salesbycustomertotals  td,#MainContent_GridViewProductTotals td {
          padding:10px;
           font-size:14px;
          background-color:#52646C ;
         color:#fff;
       }
       #MainContent_salesbycustomertotals td:first-child,#MainContent_GridViewProductTotals {
               text-align:left;          
           }

      #MainContent_salesbycustomer td:first-child ,#MainContent_GridViewProduct  td:first-child    
      {
      text-align:left;
      }

      #MainContent_salesbycustomer td:nth-child(2),#MainContent_salesbycustomertotals td:nth-child(2) {
            text-align:left;
       }

      .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
      border: 1px solid #ddd;
      padding: 10px;
      font-size:13px;
      }

   
        td {
            background: #fff;
            /*text-align: right;*/
            padding: 10px;
            border-color: white;
        }
    
      
        th {
            padding: 10px;
            background: #ebeef5;
            color: #111;
            font-weight: 600;
            font-size: 13px;
            border-color: #ebeef5;
            /*height: 40px;*/
            padding: 10px;
            text-align: center;
        }

    #MainContent_salesbycustomer tr:nth-child(odd) {
    background: rgba(227,227,227,1);
}
      #MainContent_salesbycustomer tr:nth-child(even) {
    background:#f1f1f1;
}
   </style>

   <script>
       $(document).ready(function () {         
           $('table.display').DataTable();
           //bindGridView();
           //bindGridView1();

           //footerwidth();
           //function txtvalidation() {
           //    var returnvalue = true;

           //    if ($('#MainContent_txtbxPositive').val() == "" && $('#MainContent_txtbxNegative').val() == "") {
           //        alert("Please fill atleast a variance");

           //        returnvalue = false;

           //    }
           //    return returnvalue;
           //}

               $('#collapsebtn').unbind('click').bind('click', function (e) {
                   var attr = $('#product_image').attr('src');
                   $("#reportdrpdwns").slideToggle();
                   if (attr == "images/up_arrow.png") {
                       $("#product_image").attr("src", "images/down_arrow.png");
                   } else {
                       $("#product_image").attr("src", "images/up_arrow.png");
                   }
               });
       });

      
       function bindGridView()
       {
           debugger;
           var YTD_sale_17 = $('#MainContent_hdnYTD_sale_17').val();
           var  B = $('#MainContent_hdnB').val();
           var YTD_plan= $('#MainContent_hdnYTD_plan').val();
           var YTD_sale_18 = $('#MainContent_hdnYTD_sale_18').val();
           var variance = $('#MainContent_hdnvariance').val();
           var variance_prc = $('#MainContent_hdnvariance_prc').val();

           var YTD_sale_17_cp = $('#MainContent_hdnYTD_sale_17_cp').val();
           var B_cp = $('#MainContent_hdnB_cp').val();
           var YTD_plan_cp = $('#MainContent_hdnYTD_plan_cp').val();
           var YTD_sale_18_cp = $('#MainContent_hdnYTD_sale_18_cp').val();
           var variance_cp = $('#MainContent_hdnvariance_cp').val();
           var variance_prc_cp = $('#MainContent_hdnvariance_prc_cp').val();

           var YTD_sale_17_total = $('#MainContent_hdnYTD_sale_17_total').val();
           var B_total = $('#MainContent_hdnB_total').val();
           var YTD_plan_total = $('#MainContent_hdnYTD_plan_total').val();
           var YTD_sale_18_total = $('#MainContent_hdnYTD_sale_18_total').val();
           var variance_total = $('#MainContent_hdnvariance_total').val();
           var variance_prc_total = $('#MainContent_hdnvariance_prc_total').val();
           var head_content = $('#MainContent_salesbycustomer tr:first').html();
           $('#MainContent_salesbycustomer').prepend('<thead></thead>')
           $('#MainContent_salesbycustomer thead').html('<tr>' + head_content + '</tr>');
           $('#MainContent_salesbycustomer tbody tr:first').hide();
           $('#MainContent_salesbycustomer').append('<tfoot><tr> <th colspan="5" style="text-align:right">CUSTOMER TOTAL:</th><th style="text-align: center">' + YTD_sale_17 + '</th><th style="text-align: center">' + B + '</th><th style="text-align: center">' + YTD_plan + '</th><th style="text-align: center">' + YTD_sale_18 + '</th><th style="text-align: center">' + variance + '</th><th style="text-align: center">' + variance_prc +
              '</th></tr> <tr> <th colspan="5" style="text-align:right">CHANNEL PARTNER TOTAL:</th><th style="text-align: center">' + YTD_sale_17_cp + '</th><th style="text-align: center">' + B_cp + '</th><th style="text-align: center">' + YTD_plan_cp + '</th><th style="text-align: center">' + YTD_sale_18_cp + '</th><th style="text-align: center">' + variance_cp + '</th><th style="text-align: center">' + variance_prc_cp +
              '</th></tr><tr> <th colspan="5" style="text-align:right">GRAND TOTAL:</th><th style="text-align: center">' + YTD_sale_17_total + '</th><th style="text-align: center">' + B_total + '</th><th style="text-align: center">' + YTD_plan_total + '</th><th style="text-align: center">' + YTD_sale_18_total + '</th><th style="text-align: center">' + variance_total + '</th><th style="text-align: center">' + variance_prc_total + '</th></tr></tfoot>');

           $('#MainContent_salesbycustomer').DataTable(
             {                 
                 "info": false
             });

           $('#MainContent_salesbycustomer').on('search.dt', function () {
               debugger;
               var value = $('.dataTables_filter input').val();
               console.log(value); // <-- the value
              // $('#MainContent_hdnsearch').val = value;
               
               document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReviewVariation.aspx/setGrandtotal",
                    data: "{'search':'" + value + "'}",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.d);
                        var parsed = data.d;
                        console.log(parsed);
                        console.log(parsed[1]);
                        $('#MainContent_salesbycustomer tfoot').hide();
                        var YTD_sale_17 = parsed[0];
                        var B = parsed[1];
                        var YTD_plan = parsed[2];
                        var YTD_sale_18 = parsed[3];
                        var variance = parsed[4];
                        var variance_prc = parsed[5];

                        var YTD_sale_17_cp = parsed[6];
                        var B_cp = parsed[7];
                        var YTD_plan_cp = parsed[8];
                        var YTD_sale_18_cp = parsed[9];
                        var variance_cp = parsed[10];
                        var variance_prc_cp = parsed[11];

                        var YTD_sale_17_total = parsed[12];
                        var B_total = parsed[13];
                        var YTD_plan_total = parsed[14];
                        var YTD_sale_18_total = parsed[15];
                        var variance_total = parsed[16];
                        var variance_prc_total = parsed[17];   
                        $('#MainContent_salesbycustomer').append('<tfoot><tr> <th colspan="5" style="text-align:right">CUSTOMER TOTAL:</th><th style="text-align: center">' + YTD_sale_17 + '</th><th style="text-align: center">' + B + '</th><th style="text-align: center">' + YTD_plan + '</th><th style="text-align: center">' + YTD_sale_18 + '</th><th style="text-align: center">' + variance + '</th><th style="text-align: center">' + variance_prc +
               '</th></tr> <tr> <th colspan="5" style="text-align:right">CHANNEL PARTNER TOTAL:</th><th style="text-align: center">' + YTD_sale_17_cp + '</th><th style="text-align: center">' + B_cp + '</th><th style="text-align: center">' + YTD_plan_cp + '</th><th style="text-align: center">' + YTD_sale_18_cp + '</th><th style="text-align: center">' + variance_cp + '</th><th style="text-align: center">' + variance_prc_cp +
               '</th></tr><tr> <th colspan="5" style="text-align:right">GRAND TOTAL:</th><th style="text-align: center">' + YTD_sale_17_total + '</th><th style="text-align: center">' + B_total + '</th><th style="text-align: center">' + YTD_plan_total + '</th><th style="text-align: center">' + YTD_sale_18_total + '</th><th style="text-align: center">' + variance_total + '</th><th style="text-align: center">' + variance_prc_total + '</th></tr></tfoot>');

                        console.log("2");
                    },
                    error: function (result) {
                        console.log("No Match");
                    }
                });
             });
       }
       $('#MainContent_salesbycustomer').on('search.dt', function () {
           debugger;
           var value = $('.dataTables_filter input').val();
           console.log(value); // <-- the value
           // $('#MainContent_hdnsearch').val = value;

           document.getElementById('<%=hdnsearch.ClientID%>').value = value;
       });

           function bindGridView1() {
           var YTDSALE17_PRO = $('#MainContent_hdnYTDSALE17_PRO').val();
           var B2018_PRO = $('#MainContent_hdnB2018_PRO').val();
           var YTDPLAN2018_PRO = $('#MainContent_hdnYTDPLAN2018_PRO').val();
           var YTDSALE2018_PRO = $('#MainContent_hdnYTDSALE2018_PRO').val();
           var VARIANCE_PRO = $('#MainContent_hdnVARIANCE_PRO').val();
           var VARIANCE_PRC_PRO = $('#MainContent_hdnVARIANCE_PRC_PRO').val();
           var head_content = $('#MainContent_GridViewProduct tr:first').html();
           $('#MainContent_GridViewProduct').prepend('<thead></thead>')
           $('#MainContent_GridViewProduct thead').html('<tr>' + head_content + '</tr>');
           $('#MainContent_GridViewProduct tbody tr:first').hide();
           $('#MainContent_GridViewProduct').append('<tfoot><tr></th></tr><tr> <th colspan="1" style="text-align:right">PRODUCT TOTAL:</th><th style="text-align: center">' + YTDSALE17_PRO + '</th><th style="text-align: center">' + B2018_PRO + '</th><th style="text-align: center">' + YTDPLAN2018_PRO + '</th><th style="text-align: center">' + YTDSALE2018_PRO + '</th><th style="text-align: center">' + VARIANCE_PRO + '</th><th style="text-align: center">' + VARIANCE_PRC_PRO + '</th></tr></tfoot>');
           $('#MainContent_GridViewProduct').DataTable(
             {
                 "info": false
             });

       $('#MainContent_GridViewProduct').on('search.dt', function () {
           debugger;
           var value = $('.dataTables_filter input').val();
           console.log(value); // <-- the value
           $('#MainContent_hdnsearchpro').val = value;
           document.getElementById('<%=hdnsearchpro.ClientID%>').value = value;
               $.ajax({
                   type: "POST",
                   contentType: "application/json; charset=utf-8",
                   url: "ReviewVariation.aspx/setGrandtotalpro",
                   data: "{'search':'" + value + "'}",
                   dataType: "json",
                   success: function (data) {
                       console.log(data);
                       console.log(data.d);
                       var parsed = data.d;
                       console.log(parsed);
                       console.log(parsed[1]);
                       $('#MainContent_GridViewProduct tfoot').hide();
                       var YTDSALE17_PRO = parsed[0];
                       var B2018_PRO = parsed[1];
                       var YTDPLAN2018_PRO = parsed[2];
                       var YTDSALE2018_PRO = parsed[3];
                       var VARIANCE_PRO = parsed[4];
                       var VARIANCE_PRC_PRO = parsed[5];
                       $('#MainContent_GridViewProduct').append('<tfoot><tr> <th colspan="1" style="text-align:right">PRODUCT TOTAL:</th><th style="text-align: center">' + YTDSALE17_PRO + '</th><th style="text-align: center">' + B2018_PRO + '</th><th style="text-align: center">' + YTDPLAN2018_PRO + '</th><th style="text-align: center">' + YTDSALE2018_PRO + '</th><th style="text-align: center">' + VARIANCE_PRO + '</th><th style="text-align: center">' + VARIANCE_PRC_PRO + '</th></tr></tfoot>');
                       console.log("2");
                   },
                   error: function (result) {
                       console.log("No Match");
                   }
               });
       });
           }

       $('#MainContent_GridViewProduct').on('search.dt', function () {
           debugger;
           var value = $('.dataTables_filter input').val();
           console.log(value);
           document.getElementById('<%=hdnsearchpro.ClientID%>').value = value;
          });

           //gridviewScrollTrigger();

           //$(window).resize(function () {
           //    //gridviewScrollTrigger();
           //    function gridviewScrollTrigger() {

           //        gridView1 = $('#MainContent_salesbycustomer').gridviewScroll({
           //            width: $(window).width() - 60,
           //            height: 600,
           //            railcolor: "#F0F0F0",
           //            barcolor: "#606060",
           //            barhovercolor: "#606060",
           //            bgcolor: "#F0F0F0",
           //            freezesize: 0,
           //            arrowsize: 30,
           //            varrowtopimg: "Images/arrowvt.png",
           //            varrowbottomimg: "Images/arrowvb.png",
           //            harrowleftimg: "Images/arrowhl.png",
           //            harrowrightimg: "Images/arrowhr.png",
           //            headerrowcount: 1,
           //            railsize: 16,
           //            barsize: 14,
           //            verticalbar: "auto",
           //            horizontalbar: "auto",
           //            wheelstep: 1,
           //        });
           //    }
           //    footerwidth();
           //    //footerwidth1();
           //});
           //$("tr:last").css({ backgroundColor: "yellow", fontWeight: "bolder" });
           $('#product_image').unbind('click').bind('click', function (e) {
               var attr = $('#product_image').attr('src');
               $("#reportdrpdwns").slideToggle();
               if (attr == "images/up_arrow.png") {
                   $("#product_image").attr("src", "images/down_arrow.png");
               } else {
                   $("#product_image").attr("src", "images/up_arrow.png");
               }
           });
    

       //function footerwidth() {

       //    var vartoptablewidth = $('#MainContent_salesbycustomer').innerWidth();
       //    $('#MainContent_salesbycustomertotals ').innerWidth(vartoptablewidth);
       //    var arrOfTable1 = [],
       //    i = 0;
       //    $('#MainContent_salesbycustomer td').each(function () {
       //        mWid = $(this).innerWidth();
       //        arrOfTable1.push(mWid);
       //    });

       //    $('#MainContent_salesbycustomertotals td').each(function () {
       //        $(this).css("width", arrOfTable1[i] + "px");
       //        i++;
       //    });
       //}


       function isNumberKey(e) {
           if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
           }
           return true;
       }

       function triggerPostGridLodedActions() {
             bindGridView();
             bindGridView1();
           //$('#MainContent_salesbycustomer tr:last').hide();
           //footerwidth();
           //$('#MainContent_salesbycustomer tr').each(function () {

           //    $(this).find("td:eq(1)").each(function () {
           //        if ($(this).find('span').text() == "CUSTOMER TOTAL") {
           //            $(this).closest("tr").find("td").addClass("sum_total");
           //        }
           //    });
           //    $(this).find("td:eq(1)").each(function () {
           //        if ($(this).find('span').text() == "CHANNEL PARTNER TOTAL") {
           //            $(this).closest("tr").find("td").addClass("sum_total");
           //        }
           //    });
           //    $(this).find("td:eq(1)").each(function () {
           //        if ($(this).find('span').text() == "BRANCH TOTAL") {
           //            $(this).closest("tr").find("td").addClass("grand_total");
           //        }
           //    });
           //    $(this).find("td:eq(1)").each(function () {
           //        if ($(this).find('span').text() == "GRAND TOTAL") {
           //            $(this).closest("tr").find("td").addClass("grand_total");
           //        }
           //    });
           //});

           //$('#MainContent_GridViewProduct tr').each(function () {
           //    $(this).find("td:first").each(function () {
           //        if ($(this).find('span').text() == "PRODUCT TOTAL") {
           //            $(this).closest("tr").find("td").addClass("grand_total");
           //        }
           //    });
           //});

           //$('#MainContent_reports').click(function () {
           //    var returnvalue = true;
           //    if ($('#MainContent_txtbxPositive').val() == "" && $('#MainContent_txtbxNegative').val() == "") {
           //        alert("Please fill atleast a variance");
           //        returnvalue = false;
           //    }
           //    return returnvalue;
           //});

           //$('#MainContent_productReviewBtn').click(function () {
           //    var returnvalue = true;
           //    if ($('#MainContent_txtbxPositive').val() == "" && $('#MainContent_txtbxNegative').val() == "") {
           //        alert("Please fill atleast a variance");
           //        returnvalue = false;
           //    }
           //    return returnvalue;
           //});

           //function isNumberKey(e) {
           //    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
           //        return false;
           //    }
           //    return true;
           //}

           //// gridviewScrollTrigger1();
           //gridviewScrollTrigger();
           //footerwidth();

           //$('#product_image').unbind('click').bind('click', function (e) {
           //    var attr = $('#product_image').attr('src');
           //    $("#reportdrpdwns").slideToggle();
           //    if (attr == "images/up_arrow.png") {
           //        $("#product_image").attr("src", "images/down_arrow.png");
           //    } else {
           //        $("#product_image").attr("src", "images/up_arrow.png");
           //    }
           //});
       }

       //function gridviewScrollTrigger() {

       //    gridView1 = $('#MainContent_salesbycustomer').gridviewScroll({
       //        width: $(window).width() - 60,
       //        height: findminofscreenheight($(window).height, 460),
       //        railcolor: "#F0F0F0",
       //        barcolor: "#606060",
       //        barhovercolor: "#606060",
       //        bgcolor: "#F0F0F0",
       //        freezesize: 0,
       //        arrowsize: 30,
       //        varrowtopimg: "Images/arrowvt.png",
       //        varrowbottomimg: "Images/arrowvb.png",
       //        harrowleftimg: "Images/arrowhl.png",
       //        harrowrightimg: "Images/arrowhr.png",
       //        headerrowcount: 1,
       //        railsize: 16,
       //        barsize: 14,
       //        verticalbar: "auto",
       //        horizontalbar: "auto",
       //        wheelstep: 1,
       //    });
       //}

       function findminofscreenheight(a, b) {
           a = a - $("#reportdrpdwns").offset().top;
           return a < b ? a : b;
       }
       function alertClickFilter() {
           alert("Please click on Filter to view the result data");
       }

   </script>
     </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true"></asp:ScriptManager>
   
   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
         <div class="crumbs">
            <!-- Start : Breadcrumbs -->   
            <ul id="breadcrumbs" class="breadcrumb">
               <li>
                  <i class="fa fa-home"></i>
                  <a>Reports</a>
               </li>
               <li>Reviews</li>
               <li class="current">Variation</li>
                 <div>
                <ul>
               <li class="title_bedcrum"  style="list-style:none;">VARIATION</li>
                </ul>
                </div>
                 <div>
                  <ul  style="float:right;list-style:none;   margin-top: -4px; width: 247px; margin-right: -5px; "  class="alert alert-danger fade in">
                     <li>
                        <span style="margin-right:1px;vertical-align:text-bottom;  ">Val In '000 </span>
                        <asp:RadioButton ID="ValInThsnd" OnCheckedChanged="byValueIn_CheckedChanged" AutoPostBack="true"  Checked="true"  GroupName="byValueInradiobtn" runat="server" />
                        <span style="margin-right:3px; margin-left:5px;vertical-align:text-bottom;">Val In Lakhs</span>
                        <asp:RadioButton ID="ValInLakhs" OnCheckedChanged="byValueIn_CheckedChanged" AutoPostBack="true"   GroupName="byValueInradiobtn" runat="server" />
                     </li>
                  </ul>
               </div>
            </ul>
         </div>
         <!-- End : Breadcrumbs -->
         <div id="MainContent_UpdatePanel1">
              <div id="collapsebtn" class="row">
                                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;"/>                                                                
                            </div> 
    <div class="row filter_panel" id="reportdrpdwns" style="">
         <div  id="MainContent_dfltreportform" >
               <div >
                  <ul   style="float:right;list-style:none;padding-left:10px; height: 27px;margin-top:-10px" class="alert alert-danger in">
                     <li>
                       <span style="margin-right:4px;vertical-align:text-bottom;"> Customer Review</span>
                        <asp:RadioButton ID="customerReview"  OnCheckedChanged="Review_CheckedChanged" AutoPostBack="true"  Checked="true"  GroupName="reviewBy" runat="server" />
                        <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">Product Review</span>
                        <asp:RadioButton ID="productReview"   AutoPostBack="true" OnCheckedChanged="Review_CheckedChanged"  GroupName="reviewBy" runat="server" />
                     </li>
                  </ul>
               </div>
               </div>

         <div id="divCter" runat="server">
                    <ul   class="btn-info rbtn_panel">
                        <li> <span style="margin-right:4px;vertical-align:text-bottom;  ">TAEGUTEC</span>                       
                        <asp:RadioButton ID="rdBtnTaegutec"  AutoPostBack="true"  Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged"    GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">DURACARB</span>
                        <asp:RadioButton ID="rdBtnDuraCab"  AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged"     GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        </li>
                    </ul>
                </div>

               <div class="col-md-2 control" runat="server" id="divBranch">
                     <label class="label" >BRANCH</label>        
                           <asp:DropDownList ID="ddlBranchList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged"  CssClass="control_dropdown"> </asp:DropDownList>      
               </div>

               <div class="col-md-2 control"  runat="server" id="divSalesEngnr">
                     <label class="label" >SALES ENGINEER </label>
                        <asp:DropDownList ID="ddlSalesEngineerList" runat="server"
                          CssClass="control_dropdown" >
                         <%--  <asp:ListItem>--SELECT SALES ENGINEER--</asp:ListItem>--%>
                        </asp:DropDownList>               
               </div>

                <div class="col-md-2 control" runat="server" id="divngtv">
                   <asp:Label  runat="server" id="lblnegtv" >-VE (%)</asp:Label>                     
                        <asp:TextBox ID="txtbxNegative" placeholder="...Variance greater than %"  CssClass="control_dropdown"  onkeypress="return isNumberKey(event)" MaxLength="3" runat="server"> </asp:TextBox>
                    </div>

                <div class="col-md-2 control" runat="server" id="divpstv">                
                     <label class="label">+VE (%)</label>
                        <asp:TextBox ID="txtbxPositive" placeholder="...Variance greater than %"  CssClass="control_dropdown"   onkeypress="return isNumberKey(event)" MaxLength="3" runat="server"> </asp:TextBox>               
               </div>

                 <div class="clearfix"></div>
                 <div  id="btns" runat="server" class="col-md-4" style="margin-top:2px;">                                    
                        <div id="btnss" >
                           <asp:Button ID="reports"  runat="server" Text="FILTER" OnClick="reports_Click"  CssClass="btn green"  />
                           <asp:Button ID="productReviewBtn" Visible="false"  runat="server" Text="FILTER" OnClick="productReview_Click"  CssClass="btn green"  />
                           <asp:Button ID="ButtonCustExport" Visible="false"  runat="server" Text="EXPORT" OnClick="custexports_Click"  CssClass="btn green"  />
                           <asp:Button ID="ButtonPrdctExport" Visible="false"  runat="server" Text="EXPORT" OnClick="prdctexports_Click"  CssClass="btn green"  />
                          <label id="alertmsg" style="display:none; font-weight: bold; color: #0582b7;">Now click on Filter  to view results</label>
                        </div>
               </div>
                 
            </div>             </div>
            <%--           customer variation --start--%>
            <div>
               
               
                <br />
               <asp:GridView ID="salesbycustomer" AllowSorting="True" OnSorting="salesbycustomer_Sorting"   Width="100%" Style="text-align: center;" runat="server" 
                   AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="salesby_cust_RowDataBound"  >
                  <columns>
                       <asp:TemplateField HeaderText="TYPE" ItemStyle-Width="10px" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label Style="text-align: left;" ID="lblCustType" runat="server" Text='<%# Eval("customer_type") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                      <asp:TemplateField HeaderText="CUS/ CP NUMBER" HeaderStyle-CssClass="HeadergridAll" ItemStyle-Width="10px" visible="true" ItemStyle-CssClass="align-center"  >
                        <ItemTemplate>
                           <asp:Label Style="text-align: center;" ID="Labelcust_num" runat="server" Text='<%# Eval("cust_number") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>

                         <asp:TemplateField HeaderText="BRANCH"  HeaderStyle-CssClass="HeadergridAll" Visible="true"  ItemStyle-CssClass="align-left">
                           <ItemTemplate>
                               <asp:Label Style="text-align: left;" ID="lblbranch" runat="server" Text='<%# Eval("Branch") %>'></asp:Label>
                           </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="SALES ENGINEER"  HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="155px" ItemStyle-CssClass="align-left" >
                            <ItemTemplate>
                                <asp:Label Style="text-align: left;" ID="lblsalesengg" runat="server" Text='<%# Eval("EngineerName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                     <asp:TemplateField HeaderText="CUSTOMER / CHANNEL PARTNER" HeaderStyle-CssClass="HeadergridAll" visible="true" ItemStyle-Width="176px" ItemStyle-CssClass="align-left"  >
                        <ItemTemplate>
                           <asp:Label  style="text-align:left;" ID="Labelcust_name" runat="server" Text='<%# Eval("cust_name") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>

                     <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll"  ItemStyle-CssClass="align-center"  >
                        <ItemTemplate>
                           <asp:Label Style="text-align: center;" ID="Labelsales_year_0"  runat="server" Text='<%# Eval("sales_year_0") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>

                     <asp:TemplateField HeaderText="2014P"  HeaderStyle-CssClass="HeadergridAll"  ItemStyle-CssClass="align-center" >
                        <ItemTemplate>
                           <asp:Label Style="text-align: center;" ID="Labelbudget" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>

                      <asp:TemplateField HeaderText="2014P"   HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-center"  >
                        <ItemTemplate>
                           <asp:Label Style="text-align: center;" ID="lbl_YtdPlan" runat="server" Text='<%# Eval("ytdplan") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>

                     <asp:TemplateField HeaderText="2015B"  HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-center"  >
                        <ItemTemplate>
                           <asp:Label Style="text-align: center;" ID="lbl_YtdSale" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>

                     <asp:TemplateField HeaderText="VARIANCE"  HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-center" >
                      <%--   <HeaderTemplate>
                             <asp:Label ToolTip="sales ytd value- ytd plan" runat="server"></asp:Label>
                         </HeaderTemplate>--%>
                        <ItemTemplate>
                           <asp:Label Style="text-align: center;" ID="Labelvariance" runat="server" Text='<%# Eval("variance") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>

                     <asp:TemplateField HeaderText="VARIANCE IN %"   SortExpression="variancePct"  HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-center"   >
                         <HeaderTemplate>
                             <asp:Label Style="text-align: center;" ID="LabelvariancePct" runat="server" Text="VARIANCE IN %"></asp:Label>
                            <%-- <asp:ImageButton   ToolTip="Sorting" Width="8%" runat="server" ImageUrl="~/images/sort_neutral.png" ID="ibtn_VariancePerSort" OnClick="ibtn_VariancePerSort_Click" />--%>
                         </HeaderTemplate>
                        <ItemTemplate>
                           <asp:Label  ID="LabelsvariancePct" runat="server" Text='<%# Eval("variancePct") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                  </columns>
               </asp:GridView>
                 <asp:Label runat="server" ID="lbl_nodata" Text="No Data Found" Visible="false" style="color:red;"></asp:Label>
<%--                TOTALS_START--%>
                 <%--<asp:GridView ID="salesbycustomertotals" Width="100%"  Style="text-align: center;clear:both;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" ShowHeader="false"   >
                  <columns>
                      <asp:TemplateField HeaderText="CUSTOMER / CHANNEL PARTNER NUMBER" HeaderStyle-Width="195px"  visible="true"   >
                        <ItemTemplate>
                           <asp:Label  ID="Labeltcust_number" runat="server" Text='<%# Eval("cust_number") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="CUSTOMER / CHANNEL PARTNER"  visible="true"   >
                        <ItemTemplate>
                           <asp:Label style="text-align:left !important;"  ID="Labeltcust_name" runat="server" Text='<%# Eval("cust_name") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2013"  >
                        <ItemTemplate>
                           <asp:Label ID="Labeltsales_year_0"  runat="server" Text='<%# Eval("sales_year_0") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2014P"   >
                        <ItemTemplate>
                           <asp:Label ID="Labeltbudget" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                      <asp:TemplateField HeaderText="2014P" HeaderStyle-CssClass="HeadergridAll"   >
                        <ItemTemplate>
                           <asp:Label ID="Labeltytdplan" runat="server" Text='<%# Eval("ytdplan") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2015B"    >
                        <ItemTemplate>
                           <asp:Label  ID="Labeltytd" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2015B"   >
                        <ItemTemplate>
                           <asp:Label  ID="Labeltvariance" runat="server" Text='<%# Eval("variance") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2015B" >
                        <ItemTemplate>
                           <asp:Label  ID="LabeltvariancePct" runat="server" Text='<%# Eval("variancePct") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                  </columns>
               </asp:GridView>--%>
<%--                TOTALS END--%>
            </div>
            <%--          customer variation --end--%>
            <%--           product review --start--%>
            <div>
              <asp:GridView ID="GridViewProduct"  Width="100%" Style="text-align: center;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="salesby_prdct_RowDataBound"  >   
                  <columns>
                     <asp:TemplateField HeaderText="PRODUCT NAME" HeaderStyle-CssClass="HeadergridAll" visible="true"   >
                        <ItemTemplate>
                           <asp:Label  ID="Label0" runat="server" Text='<%# Eval("prdct_name") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll"   >
                        <ItemTemplate>
                           <asp:Label ID="Label2"  runat="server" Text='<%# Eval("sales_year_0") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2014P" HeaderStyle-CssClass="HeadergridAll"   >
                        <ItemTemplate>
                           <asp:Label ID="Label3" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                      <asp:TemplateField HeaderText="2014P" HeaderStyle-CssClass="HeadergridAll"   >
                        <ItemTemplate>
                           <asp:Label ID="Label4" runat="server" Text='<%# Eval("ytdplan") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"    >
                        <ItemTemplate>
                           <asp:Label  ID="Label5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="VARIANCE" HeaderStyle-CssClass="HeadergridAll"    >
                        <ItemTemplate>
                           <asp:Label  ID="Label6" runat="server" Text='<%# Eval("variance") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="VARIANCE IN %" HeaderStyle-CssClass="HeadergridAll"    >
                         <HeaderTemplate>
                             <asp:Label ID="Label7" runat="server" Text="VARIANCE IN %"></asp:Label>
                           <%--  <asp:ImageButton   ToolTip="Sorting" Width="8%" runat="server" ImageUrl="~/images/sort_neutral.png" ID="ibtn_PVariancePerSort" OnClick="ibtn_PVariancePerSort_Click" />--%>
                         </HeaderTemplate>
                        <ItemTemplate>
                           <asp:Label  ID="Label8" runat="server" Text='<%# Eval("pvariancePct") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                  </columns>
               </asp:GridView>
<%--                               <asp:GridView ID="GridViewProductTotals"  Width="100%" Style="text-align: center;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"  ShowHeader="false"  >
                  <columns>
                     <asp:TemplateField HeaderText="PRODUCT NAME" HeaderStyle-CssClass="HeadergridAll" visible="true"   >
                        <ItemTemplate>
                           <asp:Label  ID="Label0" runat="server" Text='<%# Eval("prdct_name") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll"   >
                        <ItemTemplate>
                           <asp:Label ID="Label2"  runat="server" Text='<%# Eval("sales_year_0") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2014P" HeaderStyle-CssClass="HeadergridAll"   >
                        <ItemTemplate>
                           <asp:Label ID="Label3" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                      <asp:TemplateField HeaderText="2014P" HeaderStyle-CssClass="HeadergridAll"   >
                        <ItemTemplate>
                           <asp:Label ID="Label3" runat="server" Text='<%# Eval("ytdplan") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"    >
                        <ItemTemplate>
                           <asp:Label  ID="Label4" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"    >
                        <ItemTemplate>
                           <asp:Label  ID="Label4" runat="server" Text='<%# Eval("variance") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"    >
                        <ItemTemplate>
                           <asp:Label  ID="Label4" runat="server" Text='<%# Eval("variancePct") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>
                  </columns>
               </asp:GridView>--%>

            </div>
           <asp:HiddenField ID="hdnYTD_sale_17" runat="server" />
          <asp:HiddenField ID="hdnB" runat="server" />
          <asp:HiddenField ID="hdnYTD_plan" runat="server" />
          <asp:HiddenField ID="hdnYTD_sale_18" runat="server" />       
          <asp:HiddenField ID="hdnvariance" runat="server" />
          <asp:HiddenField ID="hdnvariance_prc" runat="server" />

          <asp:HiddenField ID="hdnYTD_sale_17_cp" runat="server" />
          <asp:HiddenField ID="hdnB_cp" runat="server" />
          <asp:HiddenField ID="hdnYTD_plan_cp" runat="server" />
          <asp:HiddenField ID="hdnYTD_sale_18_cp" runat="server" />
          <asp:HiddenField ID="hdnvariance_cp" runat="server" />
          <asp:HiddenField ID="hdnvariance_prc_cp" runat="server" />

          <asp:HiddenField ID="hdnYTD_sale_17_total" runat="server" />
          <asp:HiddenField ID="hdnB_total" runat="server" />
          <asp:HiddenField ID="hdnYTD_plan_total" runat="server" />
          <asp:HiddenField ID="hdnYTD_sale_18_total" runat="server" />
          <asp:HiddenField ID="hdnvariance_total" runat="server" />
          <asp:HiddenField ID="hdnvariance_prc_total" runat="server" />
          <asp:HiddenField ID="hdnsearch" runat="server" />

          <asp:HiddenField ID="hdnYTDSALE17_PRO" runat="server" />
          <asp:HiddenField ID="hdnB2018_PRO" runat="server" />
          <asp:HiddenField ID="hdnYTDPLAN2018_PRO" runat="server" />
          <asp:HiddenField ID="hdnYTDSALE2018_PRO" runat="server" />       
          <asp:HiddenField ID="hdnVARIANCE_PRO" runat="server" />
          <asp:HiddenField ID="hdnVARIANCE_PRC_PRO" runat="server" />
       
          <asp:HiddenField ID="hdnsearchpro" runat="server" />
         

            <%--          product review --end--%>
            <%-- <%--OVERALL GRID-- END%>--%>			
         </div>
         <div class="col-md-2"style="height:25px;">
         </div>
      </ContentTemplate>
      <Triggers>
          <asp:AsyncPostBackTrigger ControlID="ddlBranchList" EventName="SelectedIndexChanged" />
          <asp:AsyncPostBackTrigger ControlID="ddlSalesEngineerList" EventName="SelectedIndexChanged" />
          <asp:AsyncPostBackTrigger ControlID="reports" EventName="Click" />
          <asp:PostBackTrigger ControlID="productReviewBtn"/>
          <asp:PostBackTrigger ControlID="ButtonCustExport" />
          <asp:PostBackTrigger ControlID="ButtonPrdctExport" />
           <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
          <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
      </Triggers>
   </asp:UpdatePanel>
   <asp:UpdateProgress id="updateProgress" runat="server">
      <ProgressTemplate>
         <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color:#fff" >Please wait</span>
         </div>
      </ProgressTemplate>
   </asp:UpdateProgress>
<%--   <script type="text/javascript" src="js/Reports.js"></script>--%>
   
</asp:Content>