﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ProjectType.aspx.cs" Inherits="TaegutecSalesBudget.ProjectType" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <link href="GridviewScroll.css" rel="stylesheet" />
       <script type="text/javascript" src="gridscroll.js"></script>
    <div class="crumbs">    <!-- Start : Breadcrumbs -->
<ul id="breadcrumbs" class="breadcrumb">
<li>
<i class="fa fa-home"></i>
<a>Admin</a>
</li>                       
<li class="current">Project Type</li>
</ul>
  </div>  <!-- End : Breadcrumbs -->
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360">
  </asp:ScriptManager>
    <br />
   <asp:UpdatePanel ID="UpdatePanel1" runat="server"  >
    <ContentTemplate>    
     <div  class="col-lg-12" >
        <div class="col-md-4">
        <label class="col-md-2 control-label " >Project Type <span style="color:red"> *</span></label>
        <div class="col-md-2">   
            <asp:TextBox ID="txtprojecttype" runat="server"  CssClass="form-control" Width="230px" AutoPostBack="True" >          
            </asp:TextBox>
            </div>
        </div>

          <div  class="col-md-4">
            <div class="col-md-2">                            
                 <asp:Button ID="Addbtn" runat="server" Text="ADD" OnClick="Add_Click" CssClass="btn green lefts" />                      
            </div>
        </div>
     </div>     
      
   <div>               
     <asp:gridview ID="Gridviewprojecttype" runat="server" Width="50%" Style="text-align: center;" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
        <Columns>     
        <asp:TemplateField HeaderText="Project Type">
            <ItemTemplate>
                <asp:Label ID="txtprojecttype" runat="server" Text='<%#Eval("Project_Type_Desc")%>'>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="DELETE" >
            <ItemTemplate>
              <asp:ImageButton ID="Delete" runat="server" ImageAlign="Right" ImageUrl="~/images/delete_icon.png" RowIndex='<%# Container.DisplayIndex %>' OnClientClick = "Confirm()" onclick="Delete_Click" />
            </ItemTemplate>
        </asp:TemplateField>                  
        </Columns>
    </asp:gridview>
   </div> 
  </ContentTemplate>
      <%--<Triggers>
             <asp:AsyncPostBackTrigger runat="server" ControlID="Addbtn" EventName="Click" />                     
      </Triggers>--%>
  </asp:UpdatePanel>


     <style type="text/css">
          .hideelement {
              display:none;
          }
          .showelement {
              display:block;
          }
        td {
            height: 30px !important;
            padding-left: 10px !important;
            text-align: left !important;
        }
        
     
        th {
            background: #006780;
            color: #fff;
            font-weight: 600;
            font-size: 13px;
            border-color: #fff;
            height: 40px;
            padding-left: 10px;
            text-align: left;
        }
   .title_new {    
             font-family: Arial, Helvetica, sans-serif;
  font-size: 14px;
  font-weight: bold;
  /* text-decoration: underline; */
  text-align: center;
  padding: 10px;
  background: #999;
  color: #fff;
  text-transform: uppercase;
                }
        .control-label2 {
    font-weight: normal;
    padding-top: 9px;
  
}
    </style>

    <script type="text/javascript">

        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.value = "";
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            document.forms[0].appendChild(confirm_value);
            if (confirm("Do you want to delete the  Record?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            //document.forms[0].appendChild(confirm_value);
            e.preventDefault();
        }

 </script>

</asp:Content>


   
