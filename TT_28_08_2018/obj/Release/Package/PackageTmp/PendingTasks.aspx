﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"  CodeBehind="PendingTasks.aspx.cs" Inherits="TaegutecSalesBudget.PendingTasks" EnableEventValidation="false" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     <style type="text/css">
        
       .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
        padding: 5px;

        }
   .HeadergridAll {
    background: #0582b8;
    color: #fff;
    font-weight: 600;
    border-color : #0582b8;
    text-align:center !important;
    }

   

    </style>
    <link href="GridviewScroll.css" rel="stylesheet" />
       <script type="text/javascript" src="gridscroll.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <div class="crumbs">    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a>Projects</a>
                        </li>
                        <li class="current">Pending Tasks</li>

                        
                        
                    </ul>

                </div>  <!-- End : Breadcrumbs -->
   <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360">
   </asp:ScriptManager>


            <div runat="server" id="cterDiv" visible="false">
                  <ul  class="btn-info rbtn_panel">
                      <li> <span style="margin-right:4px;vertical-align:text-bottom;  ">TAEGUTEC</span>
                       
                        <asp:RadioButton ID="rdBtnTaegutec"  AutoPostBack="true"  Checked="true"  OnCheckedChanged="rdBtnTaegutec_CheckedChanged"  GroupName="byCmpnyCodeInradiobtn" runat="server" />
                         <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">DURACARB</span>
                        <asp:RadioButton ID="rdBtnDuraCab"  AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged"   GroupName="byCmpnyCodeInradiobtn" runat="server" />
                     </li>
                  </ul>
               </div>
       
            <div id="Div1" class="row" runat="server" style="margin-top:1%">
                <label runat="server" style="color:red;font-size:12" id="lbl_values" runat="server" visible="false"> &nbsp &nbsp All values are in lakhs</label>
             <div id="Div2" style="float:left; padding-right: 10px; padding-left:10px;width:100%;" runat="server">
                   <asp:HiddenField runat="server" ID="hdn_CustomerNum"  />
                 <asp:HiddenField runat="server" ID="hdn_projectnum"  />
                 <asp:HiddenField runat="server" ID="hdn_distributonum"  />
               <asp:GridView ID="projectreports" runat="server" Width="100%" Style="text-align: center;" AutoGenerateColumns="false" ShowHeader="true"  AllowSorting="true" OnSorting="ReportGridView_Sorting" >
    <columns>
               
       
        <asp:TemplateField HeaderText="CUSTOMER"  SortExpression="customer_lbl" >
            
            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/> 
              <HeaderTemplate>
                <asp:Label ID="customer_lbl"  runat="server" Text="CUSTOMER"></asp:Label>
                <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_customer" OnClick="sort_customer_Click" />
            </HeaderTemplate>      
            <ItemTemplate >
            <asp:Label  ID="Label1" runat="server" Text='<%# Eval("customer_lbl") %>'></asp:Label>
                <asp:HiddenField runat="server" ID="hdn_customernumber" Value='<%# Eval("customer_num") %>'/>
            </ItemTemplate>
            
        </asp:TemplateField>

        <asp:TemplateField HeaderText="CUSTOMER"  SortExpression="customer_lbl" >
            
            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/> 
              <HeaderTemplate>
                <asp:Label ID="dist_lbl"  runat="server" Text="CHANNEL PARTNER"></asp:Label>
                <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_distributor" OnClick="sort_customer_Click" />
            </HeaderTemplate>      
            <ItemTemplate >
            <asp:Label  ID="dist_lbl" runat="server" Text='<%# Eval("distributor_lbl") %>'></asp:Label>
                <asp:HiddenField runat="server" ID="hdn_distributornumber" Value='<%# Eval("distributor_num") %>'/>
            </ItemTemplate>
            
        </asp:TemplateField>

        <asp:TemplateField HeaderText="PROJECT TITLE"  SortExpression="project_title" >
            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>  
              <HeaderTemplate>
                <asp:Label ID="title_lbl"  runat="server" Text="PROJECT TITLE"></asp:Label>
                <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_title" OnClick="sort_title_Click" />
            </HeaderTemplate>    
            <ItemTemplate >
            <asp:LinkButton ID="status_lbl" CssClass="lbl_color"  runat="server" Text='<%# Eval("project_title") %>' OnClick="status_lbl_click" style="color: #2a6496 !important;text-decoration: underline;"></asp:LinkButton>
              <asp:HiddenField runat="server" ID="hdn_projectnumber" Value='<%# Eval("project_num") %>'/>
            </ItemTemplate>
            
        </asp:TemplateField>  
        <asp:TemplateField HeaderText="CREATION DATE"   SortExpression="creation_date_lbl">
            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"/>
              <HeaderTemplate>
                <asp:Label ID="creation_date_lbl"  runat="server" Text="CREATION DATE"></asp:Label>
                <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_creation" OnClick="sort_creation_Click" />
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="creation_date_lbl1"  runat="server" Text='<%# Eval("creation_date_lbl") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>                                            
        <asp:TemplateField HeaderText="CUSTOMER POTENTIAL"   SortExpression="overall_pot_lbl">
            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"/>
              <HeaderTemplate>
                <asp:Label ID="overall_pot_lbl"  runat="server" Text="CUSTOMER POTENTIAL"></asp:Label>
                <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_overall" OnClick="sort_overall_Click" />
            </HeaderTemplate>
            <ItemTemplate>
                <asp:Label ID="Label5"  runat="server" Text='<%# Eval("overall_pot_lbl") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="PROJECT POTENTIAL"  SortExpression="potential_lakhs_lbl">
            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"/>
            <HeaderTemplate>
                <asp:Label ID="potential_lakhs_lbl"  runat="server" Text="PROJECT POTENTIAL"></asp:Label>
                <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_potential" OnClick="sort_potential_Click" />
            </HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID="Label6"  runat="server" Text='<%# Eval("potential_lakhs_lbl") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

       <asp:TemplateField HeaderText="STATUS"  HeaderStyle-HorizontalAlign="Center">
            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
            <ItemTemplate>
           <asp:LinkButton ID="status_lnk"  runat="server" Text='<%# Eval("status_lbl") %>' OnClick="status_lbl_click" style="color: #2a6496 !important;text-decoration: underline;"></asp:LinkButton></ItemTemplate>
        </asp:TemplateField>

       <asp:TemplateField HeaderText="REMARKS"  HeaderStyle-HorizontalAlign="Center">
            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
            <ItemTemplate>
                <asp:TextBox ID="txt_remarks" TextMode="multiline"  CssClass="lock_txt" Enabled="false" 
                    runat="server" Text='<%# Eval("txt_remarks") %>' Style="max-width:100%; width:102%" ></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>
       
      
                                        
       
                                         
    </columns>
</asp:GridView>

             

         </div>
          </div>


                                       
      
       
       
        
   
   <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
   <style type="text/css">
        td {
            height: 30px !important;
            padding-left: 5px;
             /*background: #fff;*/
            /*text-align: right;*/
        }
        th {
            background: #006780;
            color: #fff;
            font-weight: 600;
            font-size: 13px;
            border-color: #fff;
            height: 40px;
            padding-left: 5px;
            text-align: center;
        }
    .grand_total {
        font-size:16px;
        font-weight:bold;
        background-color:#52646C !important;
        color:#fff;
       
    }
       .cancel
       {
           color:red;
       }
       .clearCancel
       {
           display:none;
       }
       a 
       {
          color: white;
       }
       .lbl_color
       {
           color:#333 !important;
       }
       .header_image
       {
           background-image:url("Images/arrowvt.png");
           background-repeat:no-repeat;
           background-color:#006780;
           background-position:right;
       }

       .failure
       {
           color:red;
       } 

       .success
       {
           color:green;
       } 
      
         #MainContent_projectreports td:nth-child(5),#MainContent_projectreports td:nth-child(6),#MainContent_projectreports td:nth-child(10) {
            text-align:right;
       }

          #MainContent_grandTotal td:nth-child(5),#MainContent_grandTotal td:nth-child(6),#MainContent_grandTotal td:nth-child(10) {
            text-align:right;
       }

    </style>

   <script type="text/javascript">

       $(document).ready(function () {
           gridviewScrollTrigger();
           //footerwidth();

           $(window).resize(function () {
               gridviewScrollTrigger();
               //footerwidth();
           });
       });


       function gridviewScrollTrigger() {
           gridView1 = $('#MainContent_projectreports').gridviewScroll({
               width: $(window).width() - 50,
               height: 450,
               railcolor: "#F0F0F0",
               barcolor: "#606060",
               barhovercolor: "#606060",
               bgcolor: "#F0F0F0",
               freezesize: 0,
               arrowsize: 30,
               varrowtopimg: "Images/arrowvt.png",
               varrowbottomimg: "Images/arrowvb.png",
               harrowleftimg: "Images/arrowhl.png",
               harrowrightimg: "Images/arrowhr.png",
               headerrowcount: 1,
               railsize: 16,
               barsize: 14,
               verticalbar: "auto",
               horizontalbar: "auto",
               wheelstep: 1,
           });
       }






       $(function () {

           $("[id*=status_lbl]").bind("click", function () {
               var td = $("td", $(this).closest("tr"));
               var CustomerNum = $("[id*=hdn_customernumber]", td).val();
               $("#MainContent_hdn_CustomerNum").val(CustomerNum);
           });
           $("[id*=status_lnk]").bind("click", function () {
               var td = $("td", $(this).closest("tr"));
               var CustomerNum = $("[id*=hdn_customernumber]", td).val();
               $("#MainContent_hdn_CustomerNum").val(CustomerNum);
           });

           $("[id*=btn_cancel]").bind("click", function () {
               var td = $("td", $(this).closest("tr"));
               var CustomerNum = $("[id*=hdn_customernumber]", td).val();
               $("#MainContent_hdn_CustomerNum").val(CustomerNum);
           });

       });

       $(function () {
           $("[id*=status_lbl]").bind("click", function () {
               var td = $("td", $(this).closest("tr"));
               var ProjectNum = $("[id*=hdn_projectnumber]", td).val();
               $("#MainContent_hdn_projectnum").val(ProjectNum);
           });
           $("[id*=status_lnk]").bind("click", function () {
               var td = $("td", $(this).closest("tr"));
               var ProjectNum = $("[id*=hdn_projectnumber]", td).val();
               $("#MainContent_hdn_projectnum").val(ProjectNum);
           });
           $("[id*=btn_cancel]").bind("click", function () {
               var td = $("td", $(this).closest("tr"));
               var ProjectNum = $("[id*=hdn_projectnumber]", td).val();
               $("#MainContent_hdn_projectnum").val(ProjectNum);
           });

       });


       $(function () {
           $("[id*=status_lbl]").bind("click", function () {
               var td = $("td", $(this).closest("tr"));
               var DistributorNum = $("[id*=hdn_distributornumber]", td).val();
               $("#MainContent_hdn_distributonum").val(DistributorNum);
           });

           $("[id*=status_lnk]").bind("click", function () {
               var td = $("td", $(this).closest("tr"));
               var DistributorNum = $("[id*=hdn_distributornumber]", td).val();
               $("#MainContent_hdn_distributonum").val(DistributorNum);
           });

       });










</script>

</asp:Content>


