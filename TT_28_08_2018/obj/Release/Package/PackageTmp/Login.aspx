﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TaegutecSalesBudget.Login" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
 <head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=edge, IE=11"/>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Login |  Sales-Budget & Performance Monitoring</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" />
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="fonts/fsquere/style.css"/>
    <link rel='stylesheet' type='text/css' href="fonts/open-sans/open-sans.css" />
    <link href="css/main.css" rel="stylesheet" type="text/css"/>
    <link href="css/responsive.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" type="image/png" href="#" />
    <script type="text/javascript" src="js/respond.min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
     <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.js"></script> 
<%--    <script type="text/javascript" src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/aes.js "></script>--%>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.js" type="text/javascript"></script>
     <style>
         .modalBackground {
             background-color:black;
             filter:alpha(opacity=90)!important;
             opacity:0.6!important;
             z-index:20;
         }
         .modalPopup {
             padding:20px 0px 24px 10px;
             position:relative;
             width:450px;
             height:66px;
             background-color:white;
             border:1px solid black;
                filter:alpha(opacity=90)!important;
             opacity:0.6!important;
             z-index:20;
         }
         .otpbutton {
             margin-left:30px;
             margin-top:10px;
         }
     </style>
     <script>
         function SubmitsEncry() {

             var txtUserName = document.getElementById("<%=user.ClientID %>").value.trim();
            var txtpassword = document.getElementById("<%=password.ClientID %>").value.trim();

             if (txtUserName == "") {
                 alert('Please enter UserName');
                 return false;
             }
             else if (txtpassword == "") {
                 alert('Please enter Password');
                 return false;
             }
             else {
                 var key = CryptoJS.enc.Utf8.parse('8080808080808080');
                 var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

                 var encryptedlogin = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(txtUserName), key,
                 {
                     keySize: 128 / 8,
                     iv: iv,
                     mode: CryptoJS.mode.CBC,
                     padding: CryptoJS.pad.Pkcs7
                 });

                 document.getElementById("<%=HDusername.ClientID %>").value = encryptedlogin;

                var encryptedpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(txtpassword), key,
                {
                    keySize: 128 / 8,
                    iv: iv,
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7
                });

                document.getElementById("<%=HDPassword.ClientID %>").value = encryptedpassword;
                 document.getElementById("<%=password.ClientID %>").value = encryptedpassword;
                 $("#password").val(encryptedpassword);
            
            }
    }
         function showModal() {
             $("#myModal").modal('show');
             
         }

         function RequestForDroping() {
             $("#RequestIP").modal('show');
         }
         function NewRequest() {
             $("#NewRequest").modal('show');
         }


         function ValidateOtp() {
             debugger;
             if (document.getElementById('txtotp').value == "") {
                 alert("Please provide your OTP");
                 showModal();

                 return false;
             }
             else if (isNaN(document.getElementById('txtotp').value)) {
                 alert("Please provide valid OTP");
                 showModal();

                 return false;
             }
             else
                 $('#myModal').modal('toggle');

         }

        
     </script>
</head>
             
<body class="login" style="background-color: #54364a; background-size: cover; background-image: url(images/d.jpg);">
    
    <div class="logo"><!-- BEGIN LOGO -->
        <img src="images/logo.png" alt="logo" />
    </div>  <!-- END LOGO -->
    
     <div class="content">   <!-- BEGIN LOGIN -->
        
        <form  id="loginform" class="form-vertical login-form" runat="server" visible="false" >
          

            <asp:HiddenField runat="server" ID="hdndev"/>
             <div class="alert alert-danger hide">
                <asp:Button class="close" data-dismiss="alert" ID="Button1" runat="server"/>
                <%--<button class="close" data-dismiss="alert"></button>--%>
                <span>Enter any username and passowrd.</span>
           
            </div>
    
            <div class="control-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label">Username</label>
                <div class="controls">
                    <div class="input-icon left">
                        <i class="fs-user-2"></i>
                        <asp:TextBox runat="server" class="form-control" type="text" id="user" placeholder="Username" name="username"></asp:TextBox>
<asp:HiddenField ID="HDusername" runat="server" />
<%--                        <input class="form-control" type="text" id="user" placeholder="Username" name="username" runat="server"/>--%>
                    </div>
                </div>
            </div>
            
            <div class="control-group">
                <label class="control-label">Password</label>
                <div class="controls">
                    <div class="input-icon left">
                        <i class="fs-locked"></i>
                        <asp:TextBox runat="server" class="form-control" id="password" placeholder="Password" name="password" type="password"></asp:TextBox>
                        <asp:HiddenField ID="HDPassword" runat="server" />
                        <%--<input class="form-control" type="password" id="password" placeholder="Password" name="password" runat="server"/>--%>
                    </div>
                </div>
            </div>
            
            <div class="form-actions">
                <label class="checkbox">
                    <input id="Checkbox1" type="checkbox" name="remember" value="1" runat="server"/> Remember me
                </label>
                <%--<button type="submit" class="btn green pull-right">--%>
             
                    <asp:Button class="btn green" ID="loginBtn" runat="server" text="Login" OnClientClick="return SubmitsEncry();" OnClick="loginBtn_Click"/>
                    <%--<i class="fs-checkmark-2"></i> Login--%>
                <%--</button>   --%>         
            </div>
            <asp:HiddenField ID="hdnipadress" runat="server" />
            <div class="forget-password">
                <a href="Recoverpassword.aspx" class="" id="forget-password">Forgot your password ?</a>
            </div> 


        <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg form-horizontal row-border1">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <asp:Label ID="lblMessage" runat="server" Text="Please Enter Otp that you received on your mobile or Email"></asp:Label>
                    </div>
                    <div class="modal-body">
                      
                         <div class="input-icon left" style="margin-left:60px;width:240px">
                        <asp:TextBox ID="txtotp" runat="server"  class="form-control" Width="238px"  ></asp:TextBox>
                             
                             </div>
                        
                         <asp:Button ID="btnotp" runat="server" Text="Enter" OnClick="BtnOtp_Click" CssClass="btn green otpbutton" style="margin-top:15px;margin-left:30px" OnClientClick=" return  ValidateOtp() "/>
                        <br />
                        <asp:Label ID="lblOtpFailure" runat="server" ForeColor="Red" CssClass=""></asp:Label>
                    </div>
                    <div class="modal-footer">
                     
                       
                    </div>
                </div>
            </div>
        </div>

              <div class="modal fade" id="RequestIP" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <asp:Label ID="Label1" runat="server" Text="Hi User Your logging in from new Device. this device Request Will be send for approval "></asp:Label>
                         <asp:Label ID="Label2" runat="server" Text="Once you get approval from admin you can login with this device "></asp:Label>
                    </div>
                    <div class="modal-body">
                      
                             
                             
                         <asp:Button ID="btnokrequest" runat="server" Text="OK" OnClick="Btndevice_Click" CssClass="btn green otpbutton" style="margin-top:15px;margin-left:30px" />
                         <asp:Button ID="btncancelrequest" runat="server" Text="cancel" CssClass="btn green otpbutton" style="margin-top:15px;margin-left:30px" data-dismiss="modal" />
                    </div>
                    <div class="modal-footer">
                     
                       
                    </div>
                </div>
            </div>
        </div>
            
            <div class="modal fade" id="NewRequest" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <asp:Label ID="Label3" runat="server" Text="Hi User the device your logging in is not in the approved devices or in Requested devices"></asp:Label>
                         <asp:Label ID="Label4" runat="server" Text="do you want to send this device for approval. "></asp:Label>
                    </div>
                    <div class="modal-body">
                      
                             
                             
                         <asp:Button ID="Button2" runat="server" Text="OK" OnClick="Btndevice_Click" CssClass="btn green otpbutton" style="margin-top:15px;margin-left:30px" />
                         <asp:Button ID="Button3" runat="server" Text="cancel" CssClass="btn green otpbutton" style="margin-top:15px;margin-left:30px" data-dismiss="modal" />
                    </div>
                    <div class="modal-footer">
                     
                       
                    </div>
                </div>
            </div>
        </div>

               </form>  
    <!-- END LOGIN FORM --> 
                   
        
        <form class="form-vertical forget-form" > <!-- BEGIN FORGOT PASSWORD FORM -->
          <%--  <h3 class="">Forget Password ?</h3>
            <p>Enter your e-mail address below to reset your password.</p>
            --%>
            <div class="control-group">
                <div class="controls">
                    <div class="input-icon left">
                        <i class="fa fa-envelope-o"></i>
                        <input id="Text1" class="form-control" type="text" placeholder="Email" name="email" runat="server" />
                    </div>
                </div>
            </div>
            
            <div class="form-actions">
                <button type="button" id="back-btn" class="btn">
                    <i class="fs-arrow-left"></i> Back
                </button>
                <button type="submit" class="btn green pull-right">
                    <i class="fs-checkmark-2"></i> Submit
                </button>            
            </div>
        </form> <!-- END FORGOT PASSWORD FORM -->
  
    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->

    <div class="copyright">
        copyright &copy; 2014  Design & Developed by <a href="#">KNS TECHNOLOGIES PVT Ltd</a>... Sales-Budget & Performance Monitoring.    </div>
    <!-- END COPYRIGHT -->


   <%-- <script type="text/javascript" src="Scripts/login.js"></script>--%>
 
    <script type="text/javascript">
        $(document).ready(function () {
          
            App.initLogin();
          
            function ValidateEmail(email) {
                var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                return expr.test(email);
            };
            $("#loginBtn").click(function () {
                if (!ValidateEmail($("#user").val())) {
                    alert("Please provide a  valid email address");
                    return false;

                }
                else {
                    return true;
                }
            });
            $("#loginform").validate({
                rules: {
                    user: {
                        required: true,
                        //minlength: 3
                    },
                    password: {
                        required: true,
                        //minlength: 6
                    }
                },
                messages: {
                    user: {
                        required: "Please provide a username",
                        //minlength: "Username must be at least 3 characters long"
                    },
                    password: {
                        required: "Please provide a password",
                        //minlength: "Your password must be at least 5 characters long"
                    }
                }

            });
            $('#password').keypress(function (event) {
                if (event.keyCode == 13) {
                    $('#loginBtn').click();
                }
            })
        
        });

     

    </script>
   
</body>
    <script>
        $(document).ready(function () {
            $.getJSON('https://ipapi.co/json/', function (data) {
                console.log(JSON.stringify(data, null, 2));
            });

        
            function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
                //compatibility for firefox and chrome
                var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
                var pc = new myPeerConnection({
                    iceServers: []
                }),
                noop = function() {},
                localIPs = {},
                ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
                key;

                function iterateIP(ip) {
                    if (!localIPs[ip]) onNewIP(ip);
                    localIPs[ip] = true;
                }

                //create a bogus data channel
                pc.createDataChannel("");

                // create offer and set local description
                pc.createOffer().then(function(sdp) {
                    sdp.sdp.split('\n').forEach(function(line) {
                        if (line.indexOf('candidate') < 0) return;
                        line.match(ipRegex).forEach(iterateIP);
                    });

                    pc.setLocalDescription(sdp, noop, noop);
                }).catch(function(reason) {
                    // An error occurred, so handle the failure to connect
                });

                //listen for candidate events
                pc.onicecandidate = function(ice) {
                    if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
                    ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
                };
            }

            // Usage

            getUserIP(function (ip) {
                debugger;
               // alert("Got IP! :" + ip);
                
                document.getElementById("hdnipadress").value =ip;
            });

            
        });

        var cpu_id = "";
        var get_cpuid = GetObject("winmgmts:{impersonationLevel=impersonate}");
        e = new Enumerator(get_cpuid.InstancesOf("Win32_Processor"));
        for (; !e.atEnd() ; e.moveNext()) {
            var s = e.item();
            cpu_id = s.ProcessorID;
        }
        alert(cpu_id)


    </script>
</html>
