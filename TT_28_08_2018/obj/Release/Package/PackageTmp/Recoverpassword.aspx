﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Recoverpassword.aspx.cs" Inherits="TaegutecSalesBudget.Recoverpassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <title>Login |  Sales-Budget & Performance Monitoring</title>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.3.custom.css" />
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="fonts/fsquere/style.css"/>
    <link rel='stylesheet' type='text/css' href="fonts/open-sans/open-sans.css">
    <link href="css/main.css" rel="stylesheet" type="text/css"/>
    <link href="css/responsive.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" type="image/png" href="#">
   
</head>
   
<body class="login" style="background-color: #54364a; background-size: cover; background-image: url(images/b.jpg);">
    
    <div class="logo"><!-- BEGIN LOGO -->
        <img src="images/logo.png" alt="logo" />
    </div>  <!-- END LOGO -->
    
     <div class="content">   <!-- BEGIN LOGIN -->
       
               
        
        <form id="Form1" runat="server">
           <%--  --%>
            <p>Enter your e-mail address below to reset your password.</p>
            
            <div class="control-group">
                <div class="controls">
                    <div class="input-icon left">
                        <i class="fa fa-envelope-o"></i>
                       <asp:TextBox  id="uemail" class="form-control"  placeholder="Email" name="email" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            
            <div class="form-actions">
              <%--  <button type="button" id="back-btn" class="btn">
                    <i class="fs-arrow-left"></i> Back
                </button>--%>
                 <asp:Button ID="btnBack" runat="server" class="btn" Text=" Back" OnClick="btnBack_Click" Width="80px"  />          
          
                <%--<button type="submit" class="btn green pull-right">
                    <i class="fs-checkmark-2"></i> Submit
                </button>--%>  
                 <asp:Button ID="resetbtn" runat="server" class="btn" Text=" Submit" OnClick="resetbtn_Click" Width="80px" Style="background:linear-gradient(to bottom,#0582b7 0,#0478af 29%,#025c98 85%,#035995 100%);color:#fff;" />          
            </div>
       </form>
    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->
    <div class="copyright">
        copyright &copy; 2014  Design & Developed by <a href="#">KNS TECHNOLOGIES PVT Ltd</a>... Sales-Budget & Performance Monitoring.    </div>
    <!-- END COPYRIGHT -->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/respond.min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
     <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.js"></script> 

   <%-- <script type="text/javascript" src="Scripts/login.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            
            App.initLogin();
            function ValidateEmail(email) {
                var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                return expr.test(email);
            };
            $("#resetbtn").click(function () {
                if (!ValidateEmail($("#uemail").val())) {
                    alert("Please provide a  valid email address");
                    return false;

                }
                else {
                    return true;
                }
            });

        });




    </script>
</body>
</html>
