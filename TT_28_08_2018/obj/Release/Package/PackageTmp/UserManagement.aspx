﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="TaegutecSalesBudget.UserManagement" EnableEventValidation="false" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%-- <link href="GridviewScroll.css" rel="stylesheet" />
       <script type="text/javascript" src="gridscroll.js"></script>  --%>
   
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
     <script type="text/javascript" src="js/app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#Branchmultiple').hide();

            debugger;
       

            $("#Edituser #MainContent_Cblterritorybranch td input[type='checkbox']").on('change', function () {
                var currentId = $(this).attr('id');

                if (this.checked) {
                    $('#' + currentId).attr('checked', 'checked');
                } else {
                    $('#' + currentId).attr('checked', '');
                }

            });


            $('.control-label2').css({ " font-weight": "normal", "padding-top": "9px" });

            $('#MainContent_txtEmail').on('change', function () {
                $('#MainContent_mailnotexits').hide();
                $('#MainContent_mailexits').hide();
                var currentTextValue = $(this).val();
                var empcodeval = $('#MainContent_txtEmpcode').val();
                var email = currentTextValue.split('@');
                var checkStatus = checkDuplicate(currentTextValue, 5);
                var checkempcode = checkDuplicate(empcodeval, 0);
                if ($('#MainContent_rdBtnTaegutec').prop("checked")) {

                    if (email[1] != 'taegutec-india.com' || checkStatus == false || checkempcode == false) {
                        $('#MainContent_mailnotexits').show();
                        $('#MainContent_btnAdd').hide();
                        $('#MainContent_notexits').show();

                    }
                    else {
                        $('#MainContent_mailexits').show();
                        $('#MainContent_btnAdd').show();
                        $('#MainContent_lblempemail').remove();
                    }
                }
                else if ($('#MainContent_rdBtnDuraCab').prop("checked")) {

                    if (email[1] != 'duracarb-india.com' || checkStatus == false || checkempcode == false) {
                        $('#MainContent_mailnotexits').show();
                        $('#MainContent_btnAdd').hide();
                        $('#MainContent_notexits').show();
                    }
                    else {
                        $('#MainContent_mailexits').show();
                        $('#MainContent_btnAdd').show();
                        $('#MainContent_lblempemail').remove();
                    }
                }


            });

         
        });

        function addbtn(id) {
            //  $('#MainContent_txtEmpcode').on('change', function () {
            debugger;
            $('#MainContent_notexits').hide();
            $('#MainContent_exits').hide();
            var currentTextValue = $('#' + id).val();
            var checkStatus;
            // var checkStatus = checkDuplicate(currentTextValue, 0);
            if ($.inArray(currentTextValue, jEngineerId) != -1) {
                checkStatus = false;
                // found it
            }
            else {
                checkStatus = true;
            }
            if (checkStatus == false) {
                $('#MainContent_notexits').show();
                $('#MainContent_btnAdd').hide();

            }
            else {
                $('#MainContent_exits').show();
                $('#MainContent_btnAdd').show();
            }
            // });
        }

       
        function ddlchange(id) {

            debugger;
            if ($('#' + id).val() == 'TM') {
                $('#Branchmultiple').show();
                $('#Branch').hide();
            }
            else if ($('#' + id).val() == 'BM') {
                $('#Branch').show();
                $('#Branchmultiple').hide();
            }
            else if ($('#' + id).val() == 'HO' || $('#' + id).val() == 'Admin' || $('#' + id).val() == 'RO') {
                $('#Branch').hide();
                $('#Branchmultiple').hide();
            }
        }

        function ShowCreateUser(e) {
            $('#MainContent_txtEmpcode').val("");
            $('#MainContent_txtEmpName').val("");
            $('#MainContent_txtEmail').val("");
            
            $('#MainContent_txtEmpContact').val("");
            //window.location.href = "http://localhost:1234/CreateUserManagement.aspx?User";
            window.location.href = "/CreateUserManagement.aspx?User", true;
        }

       

        function bindGridView() {
            debugger;
            var head_content = $('#MainContent_GridEngInfo tr:first').html();
            $('#MainContent_GridEngInfo').prepend('<thead></thead>')
            $('#MainContent_GridEngInfo thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_GridEngInfo tbody tr:first').hide();
            $('#MainContent_GridEngInfo').DataTable(
                {
                    "info": false,
                });
        }

        function checkDuplicateEditBranch(columnId, testid) {
            debugger;
            var table = $('#MainContent_GridEngInfo').DataTable();
            var split = testid.split('_');
            var sleng = split.length - 1;
            var spliid = parseInt(split[sleng]) + 1;
            //var value = $.trim($('#MainContent_GridEngInfo tr:eq(' + spliid + ') td:eq(' + columnId + ')').text());
            var data = table.row(spliid).data();
            var value = data[columnId].substring(data[columnId].indexOf("<span>") + 6, data[columnId].lastIndexOf("</span>"));
            return value;
        }

        function checkDuplicateEdit(columnId, testid) {

            var table = $('#MainContent_GridEngInfo').DataTable();
            var split = testid.split('_');
            var sleng = split.length - 1;
            var spliid = parseInt(split[sleng]) + 1;
            //var value = $.trim($('#MainContent_GridEngInfo tr:eq(' + spliid + ') td:eq(' + columnId + ')').text());
            var data = table.row(spliid).data();
            var value = data[columnId].substring(data[columnId].indexOf(">") + 1, data[columnId].lastIndexOf("<"));
            return value;
        }

        function checkDuplicateEditbranchcode(columnId, testid) {
            debugger;
            // clearTextvalue();
            var split = testid.split('_');
            var sleng = split.length - 1;
            var spliid = parseInt(split[sleng]) + 1;
            var value = $.trim($('#MainContent_GridEngInfo tr:eq(' + spliid + ') td:eq(' + columnId + ') input').attr('value'));
            return value;

        }

        function checkDuplicateEditstatus(columnId, testid) {

            var table = $('#MainContent_GridEngInfo').DataTable();
            var split = testid.split('_');
            var sleng = split.length - 1;
            var spliid = parseInt(split[sleng]) + 1;
            var value = $.trim($('#MainContent_GridEngInfo tr:eq(' + spliid + ') td:eq(' + columnId + ') input').attr('id'));
            var data = table.row(spliid).data();
            var value = data[columnId];
            if (value.indexOf("Active") > 0) {
                return true;
            }
            else {
                return false;
            }
        }

        function findminofscreenheight(a, b) {
            a = a - $("#divfilter").offset().top;
            return a < b ? a : b;
        }

        function Confirm(event) {
            debugger;
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to delete the  Record?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            //window.location.reload(true);
            document.forms[0].appendChild(confirm_value);
            event.preventDefault();
        }

        function ConfirmBranch() {
            var confirm_valueBranch = document.createElement("INPUT");
            confirm_valueBranch.type = "hidden";
            confirm_valueBranch.name = "confirm_valueBranch";
            if (confirm("Do you want to Deallocate this Branch?")) {
                confirm_valueBranch.value = "Yes";
            } else {
                confirm_valueBranch.value = "No";
            }
            document.forms[0].appendChild(confirm_valueBranch);
            e.preventDefault();
        }

        function confirmEmailNotification() {
            debugger;
            var confirm_emailNotification = document.createElement("INPUT");
            confirm_emailNotification.type = "hidden";
            confirm_emailNotification.name = "confirm_emailNotification";
            if (confirm("Do you want to send login  credentials to all active users?")) {
                confirm_emailNotification.value = "Yes";
            } else {
                confirm_emailNotification.value = "No";
                return false;

            }
            document.forms[0].appendChild(confirm_emailNotification);
            e.preventDefault();
        }

        function isNumberKey(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false; }
            return true;
        }

        function Reset(e) {
            clearTextvalue();
            return false;
        }

       

        $('#MainContent_txtEmail').on('change', function () {
            $('#MainContent_mailnotexits').hide();
            $('#MainContent_mailexits').hide();
            var currentTextValue = $(this).val();
            var empcodeval = $('#MainContent_txtEmpcode').val();
            var email = currentTextValue.split('@');
            var checkStatus = checkDuplicate(currentTextValue, 5);
            var checkempcode = checkDuplicate(empcodeval, 0);
            if ($('#MainContent_rdBtnTaegutec').prop("checked")) {

                if (email[1] != 'taegutec-india.com' || checkStatus == false || checkempcode == false) {
                    $('#MainContent_mailnotexits').show();
                    $('#MainContent_btnAdd').hide();
                    $('#MainContent_notexits').show();

                }
                else {
                    $('#MainContent_mailexits').show();
                    $('#MainContent_btnAdd').show();
                    $('#MainContent_lblempemail').remove();
                }
            }
            else if ($('#MainContent_rdBtnDuraCab').prop("checked")) {

                if (email[1] != 'duracarb-india.com' || checkStatus == false || checkempcode == false) {
                    $('#MainContent_mailnotexits').show();
                    $('#MainContent_btnAdd').hide();
                    $('#MainContent_notexits').show();
                }
                else {
                    $('#MainContent_mailexits').show();
                    $('#MainContent_btnAdd').show();
                    $('#MainContent_lblempemail').remove();
                }
            }


        });

       
        function checkDuplicate(textValue, columnId) {
            var rowCount = $('#MainContent_GridEngInfo tr').length;
            for (i = 1; i < rowCount; i++) {
                var columnOne = $.trim($('#MainContent_GridEngInfo tr:eq(' + i + ') td:eq(' + columnId + ')').text());


                if (columnOne == textValue) {
                    return false;
                }
            }
            return true;
        }

        function clearTextvalue() {
            debugger;
            var value = "";
            $('#MainContent_txtEmpContact').val(value);
            $('#MainContent_txtEmpcode').val(value);
            $('#MainContent_txtEmpName').val(value);
            $('#MainContent_txtEmail').val(value);
            $('#MainContent_ddlUserType').val("RO");
            $('#MainContent_ddlstatus').val("1");
            $('#Branch').hide();
            $('#Branchmultiple').hide();
            $('#MainContent_notexits').hide();
            $('#MainContent_exits').hide();
            $('#MainContent_mailnotexits').hide();
            $('#MainContent_mailexits').hide();
        }

       
        function submit() {
            debugger;
            bindGridView();
            //$('#form1').submit();
        }

        function Disable() {
            return false;
        }

        function Validation() {
            debugger;
            var id = $('#MainContent_txtEmpcode').val();
            var name = $('#MainContent_txtEmpName').val();
            var contact = $('#MainContent_txtEmpContact').val();
            var emial = $('#MainContent_txtEmail').val();
            var usertype = $('#MainContent_ddlUserType').val();
            if (id == "") {
                $('#MainContent_lblempnumberid').text('Enter the Employee number');
                return false;
            }
            else {
                $('#MainContent_lblempnumberid').remove();
            }

            if (name == "") {
                $('#MainContent_lblempname').text('Enter the Employee name');
                return false;
            }
            else {
                $('#MainContent_lblempname').remove();
            }

            if (emial == "") {
                $('#MainContent_lblempemail').text('Enter the Employee Mail');
                return false;
            }
            else {
                $('#MainContent_lblempemail').remove();
            }

            emial = emial.split('@');
            if (!ValidateEmail($("#MainContent_txtEmail").val())) {
                $('#MainContent_lblempemail').text('Enter the valid email');
                return false;
            }
            else {
                $('#MainContent_lblempemail').remove();
                if (emial[1] != 'taegutec-india.com' && $('#MainContent_rdBtnTaegutec').prop("checked")) {
                    $('#MainContent_lblempemail').text('Enter the  valid email');
                    return false;
                }
                else if (emial[1] != 'duracarb-india.com' && $('#MainContent_rdBtnDuraCab').prop("checked")) {
                    $('#MainContent_lblempemail').text('Enter the  valid email');
                    return false;
                }
                else {
                    $('#MainContent_lblempemail').remove();
                }
            }
            //if (contact == "") {
            //    $('#MainContent_contactvalidation').text('Enter the contact number');
            //    return false;
            //}
            //else {
            //    $('#MainContent_contactvalidation').remove();

            //}
            if (usertype == "RO") {
                $('#MainContent_Usertypevalidation').text('Select the User Type');
                return false;
            }
            else {
                $('#MainContent_Usertypevalidation').remove();
                return true;
            }

        }

      
        function ValidateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            debugger;
            var output = re.test(email);
            console.log(output);
            // console.log(test(email));
            //  alert(email);
            debugger;
            //  return re.test(email);
            return output;

        }

        function ShowEditUser(e, id) {
            debugger;
            
            testid = id.replace("Edit", "lbl_engid");
            var engid = $("#" + testid).html();
            $('#MainContent_notexits').hide();
            $('#MainContent_exits').hide();
            $('#MainContent_mailnotexits').hide();
            $('#MainContent_mailexits').hide();
            //window.location.href = "http://localhost:1234/EditUserManagement.aspx?id=" + engid + "";
            window.location.href = "/EditUserManagement.aspx?id=" + engid + "", true;
           
        }
        

        $('#MainContent_txtEmpcode').change(function () {
            if ($(this).val() == "") {
                $('#MainContent_lblempnumberid').text('Enter the Employee number');
                return false;
            }
            else {
                $('#MainContent_lblempnumberid').remove();
            }

        });

        $('#MainContent_txtEmpName').change(function () {
            if ($(this).val() == "") {
                $('#MainContent_lblempname').text('Enter the Employee name');
                return false;
            }
            else {
                $('#MainContent_lblempname').remove();
            }

        });

        $('#MainContent_txtEmail').change(function () {
            if ($(this).val() == "") {
                $('#MainContent_lblempemail').text('Enter the Employee Mail');
                return false;
            }
            else {
                $('#MainContent_lblempemail').remove();
            }

        });

        $('#MainContent_txtEmpContact').change(function () {
            debugger;
            var contact = $(this).val();
            con = contact.length;
            if (contact == "" || contact == "0000000000" || contact == "00000000000" || contact == "000000000000") {

                $('#MainContent_contactvalidation').text('Enter a valid Contact Number');
                return false;
            }
            else if (con < 10) {
                $('#MainContent_contactvalidation').text('Enter a valid Contact Number');
                return false;
            }
            else {
                $('#MainContent_contactvalidation').text('');
            }

        });

        $('#MainContent_ddlUserType').change(function () {
            debugger;
            if ($(this).val() == "RO") {
                $('#MainContent_Usertypevalidation').text('Select the User Type');
                return false;
            }
            else {
                $('#MainContent_Usertypevalidation').remove();
            }

        });

       
        
        function engineerId() {
            console.log(jEngineerId);
        }

    </script>
    <style>
        img#MainContent_notexits {
            margin-top: -24px;
            margin-right: 3px;
        }

        img#MainContent_mailexits {
            margin-top: -24px;
            margin-right: 3px;
        }

        img#MainContent_exits {
            margin-top: -24px;
            margin-right: 3px;
        }

        img#MainContent_mailnotexits {
            margin-top: -24px;
            margin-right: 3px;
        }

        #MainContent_GridEngInfo tr {
            vertical-align: top;
        }

        #MainContent_GridEngInfo td {
            padding-top: 10px;
        }

        .lblDisplay {
            display: none;
        }

        #MainContent_ddlBranchterritary input[type="checkbox"] {
            line-height: normal !important;
            margin: 4px 6px 0 !important;
        }

        #Edituser #MainContent_Cblterritorybranch input[type="checkbox"] {
            line-height: normal !important;
            margin: 4px 6px 0 !important;
        }
    </style>
    <style type="text/css">
        a{
                color: #428bca;
        }
        .hideelement {
            display: none;
        }

        .showelement {
            display: block;
        }

        td {
            height: 8px !important;
            padding-left: 5px !important;
            text-align: left !important;
        }


        th {
            /*background: #006780;
            color: #fff;
            font-weight: 600;
            font-size: 13px;
            border-color: #fff;
            height: 40px;
            padding-left: 10px;
            text-align: left;*/
            background: #ebeef5;
            color: #fff;
            font-weight: 600;
            text-align: left;
            border-color: #ebeef5;
            font-size: 12px;
        }

        .title_new {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-weight: bold;
            /* text-decoration: underline; */
            text-align: center;
            padding: 10px;
            background: #999;
            color: #fff;
            text-transform: uppercase;
        }

        .control-label2 {
            font-weight: normal;
            padding-top: 9px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager11" runat="server" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true" AsyncPostBackTimeout="360">
    </asp:ScriptManager>
   <%-- <asp:ScriptManager ID="test" runat="server" EnablePartialRendering="false" >

    </asp:ScriptManager>--%>
    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Admin</a>
                    </li>


                    <li class="current">User Management</li>
                    <div>

                        <li class="title_bedcrum" id="tittle" runat="server" style="list-style: none;">USER MANAGEMENT</li>

                    </div>

                    <div class="col-md-2 " style="width: 160px !important; float: right">

                        <div class="form-group">
                            <asp:LinkButton runat="server" ID="lnkbtnCreateuser" Text="CREATE NEW USER" ForeColor="#0066FF" OnClientClick="ShowCreateUser(event);"
                                Font-Underline="True" Style="padding-left: 20px"></asp:LinkButton>
                        </div>
                    </div>


                </ul>

            </div>
            <!-- End : Breadcrumbs -->
            <%--<div class="col-lg-12" style="padding-top: 8px;">
                <div style="float: left;">

                    <ul class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px;">TAEGUTEC</span>

                            <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            <span style="margin-right: 4px; margin-left: 4px;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        </li>
                    </ul>
                </div>
                
            </div>--%>
            <div class="clearfix"></div>
           
            <%-- GRID VIEW STARTS--%>
            <div>
                <asp:GridView ID="GridEngInfo" runat="server" Width="100%" Style="text-align: center;" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="GridEngInfo_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="ENGINEER ID">
                            <ItemTemplate>
                                <asp:Label ID="lbl_engid" runat="server" Text='<%# Eval("EngineerId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ENGINEER NAME">
                            <ItemTemplate>
                                <asp:LinkButton CommandArgument='<%# Bind("EngineerId") %>' OnClick="lbl_name_Click1" ID="lbl_name" runat="server" CommandName="clickLink" Text='<%#Bind("EngineerName") %>'></asp:LinkButton>
                                <%--<asp:Label ID="lbl_name" runat="server" Text='<%# Eval("EngineerName") %>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                      <%--  <asp:TemplateField HeaderText="ROLE">
                            <ItemTemplate>
                                <asp:Label ID="lbl_role" runat="server" Text='<%# Eval("RoleId").ToString()=="BM" ? "BRANCH MANGER": Eval("RoleId").ToString()=="SE" ? "SALES ENGINEER": Eval("RoleId").ToString()=="HO" ? "HEAD OFFICE":Eval("RoleId").ToString()=="Admin" ? "ADMIN": Eval("RoleId").ToString()=="TM" ? "TERRITORY MANAGER":""%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>

                        <asp:TemplateField HeaderText="BRANCH">
                            <ItemTemplate>
                                <asp:HiddenField ID="regioncode" runat="server" Value='<%# Eval("region_code") %>' />
                                <asp:Label ID="lbl_branch" runat="server" Text='<%# Eval("region_description") %>' CssClass="lblDisplay" Visible="false"></asp:Label>
                               <asp:PlaceHolder runat="server" ID="ph_Region"></asp:PlaceHolder>    
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CONTACT NUMBER">
                            <ItemTemplate>
                                <asp:Label ID="lbl_cntctnum" runat="server" Text='<%# Eval("PhoneNumber") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EMAIL ID">
                            <ItemTemplate>
                                <asp:Label ID="lbl_email" runat="server" Text='<%# Eval("EmailId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="LOGIN STATUS">
                            <ItemTemplate>
                                <asp:ImageButton ID="Activestatus" runat="server" ImageAlign="AbsMiddle" OnClientClick="if(!Disable()){return false;}" ImageUrl="~/images/active .png" Visible='<%# Eval("LoginStatus").ToString() =="1"  %>' />
                                <asp:ImageButton ID="Deactivestatus" runat="server" ImageUrl="~/images/deactive .png" OnClientClick="if(!Disable()){return false;}" Visible='<%# Eval("LoginStatus").ToString() =="0"  %>' />
                                <asp:ImageButton ID="Deactivestatus1" runat="server" ImageUrl="~/images/deactive .png" OnClientClick="if(!Disable()){return false;}" Visible='<%# Eval("LoginStatus").ToString() ==""  %>' />

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EDIT/DELETE">
                            <ItemTemplate>
                                <asp:ImageButton ID="Edit" runat="server" ImageAlign="Left" ImageUrl="~/images/edit_small.png" class="editvalue" ForeColor="#0066FF" Font-Underline="True" CommandName="Edit" OnClientClick="ShowEditUser(event,this.id);" />
                                <%--<asp:Image runat="server" ImageAlign="Right" ImageUrl="~/images/delete_icon.png" />--%>
                                <%--<asp:ImageButton ID="Delete" runat="server" ImageAlign="Right" ImageUrl="~/images/delete_icon.png" RowIndex='<%# Container.DisplayIndex %>' OnClientClick="Confirm(event)" OnClick="Delete_Click1" />--%>
                                <asp:ImageButton ID="Delete" runat="server" ImageAlign="Right" ImageUrl="~/images/delete_icon.png" RowIndex='<%# Container.DisplayIndex %>' OnClick="Delete_Click1" />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>


            
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
 
    <%-- GRID VIEW END--%>
</asp:Content>
