﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteSummary.aspx.cs" Inherits="TaegutecSalesBudget.QuoteSummary" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <style>
        .btncontrol
        {
            padding-left: 5px;
            padding-top: 10px;
        }

        .btn.green
        {
            margin-top: 10px;
        }

        .gridbutton
        {
            /*padding: 5px!important;*/
            top: auto;
            overflow: initial;
            padding: 3px 20px;
            border-radius: 3px !important;
            background: #0273ab;
            color: #fff;
            border: none;
            margin: 1px;
        }

        .control_dropdown
        {
            width: 180px;
            height: 30px;
            border-radius: 4px!important;
        }

        .label
        {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

        .control
        {
            padding-top: 2px;
        }

        .popupControl
        {
            margin: 5px;
            float: right;
        }

        th,
        td
        {
            white-space: nowrap;
        }

        div.dataTables_wrapper
        {
            /*width: 800px;*/
            margin: 0 auto;
        }
    </style>
    <script type="text/javascript">
        var table1;
        $(document).ready(function () {
            debugger;
            
            LoadTable1();
            $('#MainContent_txtOrderDate').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 2019,
                maxYear: parseInt(moment().format('YYYY'), 10)
            }, function (start, end, label) {
            });
            

        });
        function LoadTable()
        { }

        function LoadData(msg) {
            debugger;
            console.log(msg);
            console.log(msg.d);
            msg = JSON.parse(msg.d);
            var roleId = '<%= Session["RoleId"] %>';
            $('#grdDetailedPriceSummary1 tbody').html("");
            var approve_Flag = 0;
            var defDate;
            for (var i = 0; i < msg.length; i++) {
                var button;
                var hdnExpiry="<input type=\"hidden\" id=\"hdnExpiryDate_"+i+"\" value=\"" + msg[i].ExpiryDate + "\" />"
                if (msg[i].Status == "Approved") {

                    //if (msg[i].Order_Flag == "1")
                    //    button = "<a href=\"#ex1\" rel=\"modal:open\"><input type=\"button\"  class=\"gridbutton\"  id=\"imgbtnPlace\" value=\"Place Order\" onclick=\"return PlaceOrder('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + msg[i].Order_type + "', '" + msg[i].Cust_number + "', '" + msg[i].Cust_Name + "', '" + msg[i].MOQ + "', '" + msg[i].QTY_perOrder + "', '" + msg[i].Order_frequency + "');\" ></a>"
                    //    + "<a href=\"#mdReason\" rel=\"modal:open\"><input type=\"button\" id=\"btnEscalate1\" value=\"Escalate\" class=\"gridbutton\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Escalated');\" /></a></td>  <td>";
                    if (roleId != "HO") {
                        if (msg[i].Order_Flag == "1")
                            button = "<a href=\"#ex1\" rel=\"modal:open\"><input type=\"button\"  class=\"gridbutton\"  id=\"imgbtnPlace\" value=\"Place Order\" onclick=\"return PlaceOrder('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + msg[i].Order_type + "', '" + msg[i].Cust_number + "', '" + msg[i].Cust_Name + "', '" + msg[i].MOQ + "', '" + msg[i].QTY_perOrder + "', '" + msg[i].Order_frequency + "', '"+msg[i].Total_QTY+"');\" ></a>"
                            + "<a href=\"#mdReason\" rel=\"modal:open\"><input type=\"button\" id=\"btnEscalate1\" value=\"Escalate\" class=\"gridbutton\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Escalated');\" /></a></td>  <td>";
                        else
                            button = "<a href=\"#mdReason\" rel=\"modal:open\"><input type=\"button\" id=\"btnEscalate\" value=\"Escalate\" class=\"gridbutton\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Escalated');\" /></a></td>  <td>";
                    }
                    else {
                        if (msg[i].Order_Flag == "1")
                            button = "<a href=\"#ex1\" rel=\"modal:open\"><input type=\"button\"  class=\"gridbutton\"  id=\"imgbtnPlace\" value=\"Place Order\" onclick=\"return PlaceOrder('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + msg[i].Order_type + "', '" + msg[i].Cust_number + "', '" + msg[i].Cust_Name + "', '" + msg[i].MOQ + "', '" + msg[i].QTY_perOrder + "', '" + msg[i].Order_frequency + "', '" + msg[i].Total_QTY + "');\" ></a></td>  <td>";
                        else
                            button = "</td>  <td>";
                    }
                    console.log(button);
                    // button = "<span id=\"tetst\" class=\"place\">Place<img id=\"lnkPlace\" class=\"place\" src=\"images/place-order.png\" style=\"    width: 30px;\" />Place</span>"
                    //
                }
                else if (msg[i].Status == "Expired") {
                    if (roleId != "HO") {
                        button = "<input type=\"button\" id=\"btnRequest\" value=\"Request for approval\" class=\"gridbutton\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Request For ReApproval');\" /></a></td>  <td>";
                    }
                    else
                        button = "</td>  <td>";
                }
                else if (msg[i].Status == "Rejected") {
                    if (roleId != "HO") {
                        button = "<a href=\"#mdReason\" rel=\"modal:open\"><input type=\"button\" id=\"btnEscalate\" value=\"Escalate\" class=\"gridbutton\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Escalated');\" /></a></td>  <td>";
                    }
                    else
                        button = "</td>  <td>";
                    console.log(button);
                    // button = "<span id=\"tetst\" class=\"place\">Place<img id=\"lnkPlace\" class=\"place\" src=\"images/place-order.png\" style=\"    width: 30px;\" />Place</span>"
                    //
                }
                else if (msg[i].Status == "Escalated") {
                    if (roleId == "BM" || roleId == "TM") {
                        button = "<a href=\"#mdReason\" rel=\"modal:open\"><input type=\"button\" id=\"btnEscalate\" value=\"Escalate\" class=\"gridbutton\" onclick=\"return Request('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', 'Escalated');\" /></a></td>  <td>";
                    }
                    else
                        button = "</td>  <td>";
                    console.log(button);
                    // button = "<span id=\"tetst\" class=\"place\">Place<img id=\"lnkPlace\" class=\"place\" src=\"images/place-order.png\" style=\"    width: 30px;\" />Place</span>"
                    //
                }
                else if (msg[i].Status == "Sent For Approval") {
                    if ((roleId == "BM" || roleId == "TM") && msg[i].Assigned_To == "BM") {
                        //if (msg[i].Accept_Visible_Flag == "1")
                            button = "<a href=\"#mdComment\" rel=\"modal:open\"><input type=\"button\"  class=\"gridbutton\"  value=\"Approve\" id=\"btnApprove\"  onclick=\"return Approve('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + i + "','Approved');\" /></a>" +
                            "<a href=\"#mdComment\" rel=\"modal:open\"><input type=\"button\"  class=\"gridbutton\"  value=\"Reject\" id=\"btnReject\"  onclick=\"return Reject('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + i + "');\" /></a></td>  <td>";

                        //else
                        //    button = "<a href=\"#mdComment\" rel=\"modal:open\"><input type=\"button\"  class=\"gridbutton\" disabled=\"disabled\" value=\"Approve\" id=\"btnApprove\"  onclick=\"return Approve('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + i + "','Approved');\" /></a>" +
                        //       "<a href=\"#mdComment\" rel=\"modal:open\"><input type=\"button\"  class=\"gridbutton\"  value=\"Reject\" id=\"btnReject\"  onclick=\"return Reject('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + i + "');\" /></a></td>  <td>";
                        approve_Flag++;
                        //+"</td>  <td>";
                        console.log(button);

                    }
                    else
                        button = "</td>  <td>";
                    console.log(button);
                    // button = "<span id=\"tetst\" class=\"place\">Place<img id=\"lnkPlace\" class=\"place\" src=\"images/place-order.png\" style=\"    width: 30px;\" />Place</span>"
                    //
                }
                else if (msg[i].Status == "Approved By BM") {
                    if (roleId == "HO") {
                        button = "<a href=\"#mdComment\" rel=\"modal:open\"><input type=\"button\"  class=\"gridbutton\"  value=\"Approve\" id=\"btnApprove\"  onclick=\"return Approve('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + i + "','Approved');\" /></a>" +
                        "<a href=\"#mdComment\" rel=\"modal:open\"><input type=\"button\"  class=\"gridbutton\"  value=\"Reject\" id=\"btnReject\"  onclick=\"return Reject('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + i + "');\" /></a></td>  <td>";
                        approve_Flag++;
                        console.log(button);

                    }
                    else
                        button = "</td>  <td>";
                    console.log(button);
                    // button = "<span id=\"tetst\" class=\"place\">Place<img id=\"lnkPlace\" class=\"place\" src=\"images/place-order.png\" style=\"    width: 30px;\" />Place</span>"
                    //
                }
                else
                    button = "</td>  <td>";

                //if (approve_Flag == 0) {

                //    comment = "<textarea  id=\"txtComment_" + i + "\" cols=\"20\" rows=\"2\" readonly></textarea><br/><span id=\"lblmsg_" + i + "\" style=\"color:red;\"></span>";
                //}
                //else {
                //    comment = "<textarea  disabled=\"disabled\" id=\"txtComment_" + i + "\" cols=\"20\" rows=\"2\"></textarea><br/><span id=\"lblmsg_" + i + "\" style=\"color:red;\"></span>";

                //}



                $("#grdDetailedPriceSummary1 tbody ").append(" <tr>  <td>" +
                    button +
                    
                    msg[i].Item_code + hdnExpiry+"</td>  <td>" +
                    msg[i].Item_Desc + "</td>  <td>" +
                    msg[i].WHS + "</td>  <td>" +
                    msg[i].Order_type + "</td>  <td>" +
                    msg[i].Order_frequency + "</td>  <td>" +
                    msg[i].Total_QTY + "</td>  <td>" +
                    msg[i].MOQ + "</td>  <td>" +
                    //msg[i].Expected_price + "</td>  <td>" +
                    msg[i].New_OfferPrice + "</td>  <td>" +
                     msg[i].Status +
                    "</td>  </tr>");
                // alert(JSON.stringify(msg));
                debugger;
                defDate = $('#hdnExpiryDate_'+i).val();
                $('#MainContent_txtExpiryDate').daterangepicker({
                    singleDatePicker: true,
                    startDate: defDate,
                    endDate: null,
                    locale: {
                        format: 'MM/DD/YYYY'
                    },
                    showDropdowns: true,
                }, function (start, end, label) {
                    debugger;
                    var id = this.element[0].id;
                    var name = this.element[0].name;
                    $('body').find("input[name^='" + name + "']").val(start.format('MM/DD/YYYY'));
                });
            }
            debugger;
            if ($.fn.dataTable.isDataTable('#grdDetailedPriceSummary1')) {

            }
            else {
                //table1.destroy();
                table1 = $('#grdDetailedPriceSummary1').DataTable({
                    // destroy: true
                });
            }

        }

        function LoadDates()
        {
            var SelectedStart = sessionStorage.getItem("selectedStart");
            var SelectedEnd = sessionStorage.getItem("selectedEnd");
            var start = (SelectedStart == null ? "'07/20/2019" : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            $('#MainContent_txtDateRange').daterangepicker({
                autoUpdateInput: true,
                locale: {
                    format: 'MM/DD/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['07/20/2019', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                var start = start.format('MM/DD/YYYY');
                var end = end.format('MM/DD/YYYY');
                sessionStorage.setItem('selectedStart', start);
                sessionStorage.setItem('selectedEnd', end);
            });


        }
        function LoadTable1() {

            LoadDates();
            if ($.fn.dataTable.isDataTable('#MainContent_grdPriceSummary')) {

            }
            else {

                var head_content = $('#MainContent_grdPriceSummary tr:first').html();
                $('#MainContent_grdPriceSummary').prepend('<thead></thead>')
                $('#MainContent_grdPriceSummary thead').html('<tr>' + head_content + '</tr>');
                $('#MainContent_grdPriceSummary tbody tr:first').hide();
                var table = $('#MainContent_grdPriceSummary').dataTable({
                    "order": [[2, 'desc']]
                });

                var table = $('#MainContent_grdPriceSummary').DataTable();
                var divdetail = document.getElementById("divdetail");
                $('#MainContent_grdPriceSummary tbody').on('click', 'td:first-child .link', function () {
                    debugger;
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                        divdetail.style.display = "none";
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        divdetail.style.display = "block";
                        var param = tr.context.innerText;
                        console.log(param);
                        $.ajax({
                            url: 'QuoteSummary.aspx/LoadDetailedGrid',
                            method: 'post',
                            datatype: 'json',
                            data: '{ref_no:"' + param + '"}',
                            contentType: "application/json; charset=utf-8",
                            success: function (msg) {
                                LoadData(msg);

                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(xhr.responseText);
                            }
                        });

                        // Open row.
                        row.child(divdetail).show();
                        //tr.addClass('shown');
                    }
                });


                var divplace = document.getElementById("divplace");
            }

        }

        function triggerPostGridLodedActions() {
            LoadTable1();
        }
        function LoadPlace(table) {
            $('#grdDetailedPriceSummary1 tbody').on('click', 'td:first-child .place', function () {
                debugger;
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child(format(row.data())).show();
                    tr.addClass('shown');
                }
            });
        }


        function format(d) {
            console.log(d);
            // `d` is the original data object for the row
            return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                '<tr>' +
                    '<td>Full name:</td>' +
                    '<td></td>' +
                '</tr>' +
                '<tr>' +
                    '<td>Extension number:</td>' +
                    '<td></td>' +
                '</tr>' +
                '<tr>' +
                    '<td>Extra info:</td>' +
                    '<td>And any further details here (images etc)...</td>' +
                '</tr>' +
            '</table>';
        }
        function PlaceOrder(ID, item, ref_num, order_type, cust_num, cust_name, MOQ, qty, frequency, total_qty) {
            debugger;
            console.log('Hi');
            console.log(item);
            console.log(ref_num);
            var divSchedule = document.getElementById('divSchedule');
            if (order_type == "schedule") {
                divSchedule.style.display = "block";
                $('#MainContent_txtQuantity').val("");
                $('#MainContent_txtMOQ').val(MOQ);
            }
            else {
                divSchedule.style.display = "none";
                $('#MainContent_txtQuantity').val(qty);
                $('#MainContent_txtMOQ').val(total_qty);
            }
            $('#MainContent_lblOrderType').val(order_type);
            $('#MainContent_txtFrequency').val(frequency);
            $('#MainContent_hdnRef').val(ref_num);
            $('#MainContent_hdnItem').val(item);
            $('#MainContent_hdnID').val(ID);
            
            $('#MainContent_hdnCustNum').val(cust_num);
            $('#MainContent_hdnCustName').val(cust_name);
        }
        function IsnullOrEmpty(val) {
            if (val != '' && val != undefined && val != '--Select--')
                return false;
            else
                return true;
        }
        function Order() {
            debugger;
            var order_type = $('#MainContent_lblOrderType').val();
            var ref_num = $('#MainContent_hdnRef').val();
            var item = $('#MainContent_hdnItem').val();
            var ID = $('#MainContent_hdnID').val();
            var quantity = $('#MainContent_txtMOQ').val();
            var schedule = $('#MainContent_ddlSchedule').val();
            var NumOrder = $('#MainContent_txtNumOrder').val();
            var cust_num = $('#MainContent_hdnCustNum').val();
            var cust_name = $('#MainContent_hdnCustName').val();
            var orderstart_date = $('#MainContent_txtOrderDate').val();
            var POComment = $('#MainContent_txtPOComment').val();
            var flag = 0;
            //if (IsnullOrEmpty(quantity)) {
            //    $("#MainContent_txtQuantity").css("border", "1px solid red");
            //    flag++;
            //}
            //else {
            //    $("#MainContent_txtQuantity").css("border", "");
            //}
            //if (order_type == 'schedule') {
            //    if (IsnullOrEmpty(NumOrder)) {
            //        $("#MainContent_txtNumOrder").css("border", "1px solid red");
            //        flag++;
            //    }
            //    else {
            //        $("#MainContent_txtNumOrder").css("border", "");
            //    }
            //}
            if (flag == 0) {
                $.ajax({
                    url: 'QuoteSummary.aspx/PlaceOrder',
                    method: 'post',
                    datatype: 'json',
                    data: '{ref_num:"' + ref_num + '", ID:"' + ID + '", quantity:"' + quantity + '", item:"' + item + '", order_type:"' + order_type + '", schedule:"' + schedule + '", OrderStartDate:"' + orderstart_date + '", cust_num:"' + cust_num + '", cust_name:"' + cust_name + '", PO_comment:"' + POComment + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        location.reload(true);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            }

        }
        function Escalate() {
            debugger;
            var ref_num = $('#MainContent_hdnRef1').val();
            var item = $('#MainContent_hdnItem1').val();
            var ID = $('#MainContent_hdnID1').val();
            var status = $('#MainContent_hdnStatus1').val();
            var reason = $('#MainContent_txtReason').val();
            if (IsnullOrEmpty(reason)) {
                $("#MainContent_txtReason").css("border", "1px solid red");
            }
            else {
                $("#MainContent_txtReason").css("border", "");
                var objList = [];
                var obj = {
                    "id": ID
                     , "item": item
                     , "ref_no": ref_num
                     , "status": status
                     , "comment": reason
                };
                objList.push(obj);
                var param = "{objList:" + JSON.stringify(objList) + "}";

                $.ajax({
                    url: 'QuoteSummary.aspx/RequestForReApproval',
                    method: 'post',
                    datatype: 'json',
                    data:param,
                    //data: '{ref_num:"' + ref_num + '", ID:"' + ID + '", item:"' + item + '", status:"' + status + '", reason:"' + reason + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        window.location.reload();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            }
        }
        function Request(ID, item, ref_num, status) {
            debugger;
            if (status == "Escalated") {
                $('#MainContent_hdnRef1').val(ref_num);
                $('#MainContent_hdnItem1').val(item);
                $('#MainContent_hdnID1').val(ID);
                $('#MainContent_hdnStatus1').val(status);
                Escalate();
            }
            else {
                var objList = [];
                var obj = {
                    "id": ID
                    , "item": item
                    , "ref_no": ref_num
                    , "status": status
                };
                objList.push(obj);
                var param = "{objList:" + JSON.stringify(objList) + "}";

                $.ajax({
                    url: 'QuoteSummary.aspx/RequestForReApproval',
                    method: 'post',
                    datatype: 'json',
                    data:param,
                    //data: '{ref_num:"' + ref_num + '", ID:"' + ID + '", item:"' + item + '", status:"' + status + '", reason:""}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        window.location.reload();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            }
        }

        function Approve(Id, item, ref, rowcount, status) {
            debugger;
            var roleId = '<%= Session["RoleId"] %>';
            var divPrice, divDate;
            
            if (roleId == 'HO') {
                divPrice = document.getElementById('divSpecial');
                divPrice.style.display = "block";
                divDate = document.getElementById('divExpiry');
                divDate.style.display = "block";
                
            }
            else {
                divPrice = document.getElementById('divRecommended');
                divPrice.style.display = "block";
            }
            //var comment_id = "txtComment_" + rowcount;
            //var comment = $("#" + comment_id).val();
            $('#MainContent_hdnrejref').val(ref);
            $('#MainContent_hdnrejitem').val(item);
            $('#MainContent_hdnrejID').val(Id);
            $('#MainContent_hdnrejStatus').val("Approved");
            
        }

        function Reject(Id, item, ref, rowcount) {
            debugger;
            var roleId = '<%= Session["RoleId"] %>';
            var divPrice;
            if (roleId == 'HO') {
                divPrice = document.getElementById('divSpecial');
                divPrice.style.display = "block";
            }
            else {
                divPrice = document.getElementById('divRecommended');
                divPrice.style.display = "block";
            }
            var errFlag = 0;
            var reason = $('#MainContent_txtReason').val();
            $('#MainContent_hdnrejref').val(ref);
            $('#MainContent_hdnrejitem').val(item);
            $('#MainContent_hdnrejID').val(Id);
            $('#MainContent_hdnrejStatus').val("Rejected");
            
        }

        function statusChange() {
            debugger;
            var roleId = '<%= Session["RoleId"] %>';
            var ref = $('#MainContent_hdnrejref').val();
            var item = $('#MainContent_hdnrejitem').val();
            var Id = $('#MainContent_hdnrejID').val();
            var status = $('#MainContent_hdnrejStatus').val();
            var reason = $('#MainContent_txtComment').val();
            var NewOfferPrice = $('#MainContent_txtNewOfferPrice').val();
            var RecPrice = $('#MainContent_txtRecPrice').val();
            if (status == "Rejected") {
                if (IsnullOrEmpty(reason)) {
                    $("#MainContent_txtReason").css("border", "1px solid red");
                }
                else {
                    if (IsnullOrEmpty(RecPrice)) {
                        RecPrice = "";
                    }
                    if (IsnullOrEmpty(NewOfferPrice)) {
                        NewOfferPrice = "";
                    }
                    var objList = [];
                    var obj = {
                        "id": Id
                        , "item": item
                        , "ref_no": ref
                        , "status": status
                        , "comment": reason
                        , "offerPrice": NewOfferPrice
                        , "RecPrice": RecPrice
                    };
                    objList.push(obj);
                    var param = "{objList:" + JSON.stringify(objList) + "}";

                    $.ajax({
                        url: 'QuoteSummary.aspx/RequestForReApproval',
                        method: 'post',
                        datatype: 'json',
                        data:param,
                        //data: '{id:"' + Id + '", item:"' + item + '", ref_no:"' + ref + '", status:"' + status + '", comment:"' + comment + '"}',
                        //data: '{ref_num:"' + ref + '", ID:"' + Id + '", item:"' + item + '", status:"' + status + '", reason:"' + reason + '", specialprice: "' + NewOfferPrice + '", RecPrice:"' + RecPrice + '"}',
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            msg = JSON.parse(msg.d);
                            alert(msg.msg);
                            window.location.reload();

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.responseText);
                        }
                    });
                }
            }
            else {
                var err_flag = 0;
                var expiry_date;
                if (IsnullOrEmpty(RecPrice)) {
                    RecPrice = "";
                }
                if (roleId == "BM" || roleId == "TM") {
                    if (IsnullOrEmpty($('#MainContent_txtComment').val())) {
                        $('#MainContent_txtComment').css("border", "1px solid red");
                        err_flag += 1;
                    }
                    else
                        $('#MainContent_txtComment').css("border", "");
                }
                if (roleId == "HO") {
                    if (IsnullOrEmpty(NewOfferPrice)) {
                        $("#MainContent_txtNewOfferPrice").css("border", "1px solid red");
                        err_flag += 1;
                    }
                    else
                        $("#MainContent_txtNewOfferPrice").css("border", "");
                    expiry_date = $('#MainContent_txtExpiryDate').val();
                }
                else {
                    if (IsnullOrEmpty(NewOfferPrice)) {
                        NewOfferPrice = "";
                    }
                }
                if (err_flag == 0) {
                    var objList = [];
                    var obj = {
                        "id": Id
                      , "item": item
                      , "ref_no": ref
                      , "status": status
                      , "comment": reason
                      , "offerPrice": NewOfferPrice
                      , "RecPrice": RecPrice
                        , "expiry_date": expiry_date
                    };
                    objList.push(obj);
                    var param = "{objList:" + JSON.stringify(objList) + "}";


                    $.ajax({
                        url: 'QuoteSummary.aspx/RequestForReApproval',
                        method: 'post',
                        datatype: 'json',
                        data:param,
                        //data: '{ref_num:"' + ref + '", ID:"' + Id + '", item:"' + item + '", status:"' + status + '", reason:"' + reason + '", specialprice: "' + NewOfferPrice + '", RecPrice:"' + RecPrice + '"}',
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            msg = JSON.parse(msg.d);
                            alert(msg.msg);
                            window.location.reload();

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.responseText);
                        }
                    });
                }
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Quote</a>
                        </li>
                        <li class="current">Price Summary</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
    <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">

        <ContentTemplate>

            <div class="col-md-12 mn_margin">
                <div id="collapsebtn" class="row">
                    <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />
                </div>
                <div class="row filter_panel" id="reportdrpdwns" runat="server">
                    <div runat="server" id="cterDiv" visible="false">
                        <ul class="btn-info rbtn_panel">
                            <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>

                                <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                                <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                                <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            </li>
                        </ul>
                    </div>


                    <div class="col-md-12 nopadding">

                        <div class="col-md-2 control">
                            <label class="label ">Branch</label>
                            <asp:DropDownList ID="ddlBranch" runat="server" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                                <asp:ListItem>ALL</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="col-md-2 control">
                            <label class="label ">Sales Engineer</label>
                            <asp:DropDownList ID="ddlSE" runat="server" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlSE_SelectedIndexChanged">
                                <asp:ListItem>ALL</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="col-md-2 control">
                            <label class="label">Customer Class </label>
                            <asp:DropDownList ID="ddlCustomerClass" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCustomerClass_SelectedIndexChanged"
                                CssClass="control_dropdown">
                                <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                <asp:ListItem Value="KA">KEY FOCUSED CUSTOMER</asp:ListItem>
                                <asp:ListItem Value="KF">‘A’ CLASS CUSTOMER</asp:ListItem>
                                <asp:ListItem Value="OT"> OTHERS</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="col-md-2 control">
                            <label class="label">Customer</label>
                            <asp:DropDownList ID="ddlCustomers" runat="server" AutoPostBack="True" CssClass="control_dropdown">
                                <asp:ListItem>ALL</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="col-md-2 control">
                            <label class="label">Requested Date</label>
                            <asp:TextBox ID="txtDateRange" CssClass="control_dropdown" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-2 btncontrol">
                            <asp:Button runat="server" ID="btnFilter" CssClass="btn green" Text="Filter" OnClick="btnFilter_Click" />
                        </div>
                    </div>



                </div>

                <asp:Label runat="server" ID="lblmessage"></asp:Label>

                <asp:GridView ID="grdPriceSummary" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Reference Number">
                            <ItemTemplate>
                                <asp:Label ID="lnkRef" CssClass="link" Text='<%# Bind("Ref_number")%>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Customer">
                            <ItemTemplate>
                                <asp:Label ID="lblCust" Text='<%# Bind("Cust_Name")%>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Requested Date">
                            <ItemTemplate>
                                <asp:Label ID="lblDate" Text='<%# Bind("Requested_date")%>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" Text='<%# Bind("Status")%>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>


            </div>
            <div id="divdetail" style="display: none;">
                <table id="grdDetailedPriceSummary1" class="display responsive nowrap" cellpadding="0" cellspacing="0">
                    <thead style="background-color: #DC5807; color: White; font-weight: bold">
                        <tr style="border: solid 1px #000000">
                            <td>Action</td>
                            <td>Item_code</td>
                            <td>Item_Desc</td>
                            <td>WHS</td>
                            <td>Order Type</td>
                            <td>Order Frequency</td>
                            <td>Total Qty</td>
                            <td>MOQ</td>
                            <%--<td>Expected Price</td>--%>
                            <td>Approved Price</td>
                            <td>Status</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div id="divplace" style="display: none;">fdgdsgfdg</div>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>


    <div id="mdReason" class="modal" style="border: solid 1px #008a8a;">
        <div class="col-md-12 controls">
            <div class="col-md-4">
                <p class="popupControl">Reason For Escalation : </p>
            </div>
            <div class="col-md-8">
                <asp:TextBox ID="txtReason" Rows="4" Columns="30" TextMode="MultiLine" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-4">
                <asp:HiddenField ID="hdnID1" runat="server" />
                <asp:HiddenField ID="hdnRef1" runat="server" />
                <asp:HiddenField ID="hdnItem1" runat="server" />
                <asp:HiddenField ID="hdnStatus1" runat="server" />
            </div>
            <div class="col-md-8">
                <input type="button" id="btnSubmitReason" class="btn green" onclick="Escalate();" title="Escalate" value="Escalate" />
                <a href="#" rel="modal:close">Close</a>
            </div>
        </div>
    </div>

    <div id="mdComment" class="modal" style="border: solid 1px #008a8a;">
        <div class="col-md-12 controls" id="divSpecial" style="display: none;">
            <div class="col-md-4">
                <p class="popupControl">Special Price : </p>
            </div>
            <div class="col-md-8">
                <asp:TextBox ID="txtNewOfferPrice"  runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12 controls" id="divExpiry" style="display: none;">
            <div class="col-md-4">
                <p class="popupControl">Expiry Date: </p>
            </div>
            <div class="col-md-8">
                <asp:TextBox ID="txtExpiryDate"  runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12 controls" id="divRecommended" style="display: none;">
            <div class="col-md-4">
                <p class="popupControl">Recommended Price : </p>
            </div>
            <div class="col-md-8">
                <asp:TextBox ID="txtRecPrice"  runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12 controls">
            <div class="col-md-4">
                <p class="popupControl">Reason for Rejection(Optional for approval) : </p>
            </div>
            <div class="col-md-8">
                <asp:TextBox ID="txtComment" Rows="4" Columns="30" TextMode="MultiLine" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-4">
                <asp:HiddenField ID="hdnrejID" runat="server" />
                <asp:HiddenField ID="hdnrejref" runat="server" />
                <asp:HiddenField ID="hdnrejitem" runat="server" />
                <asp:HiddenField ID="hdnrejStatus" runat="server" />
            </div>
            <div class="col-md-8">
                <input type="button" id="btnStatusChange" class="btn green" onclick="statusChange();" title="Submit" value="Submit" />
                <a href="#" rel="modal:close">Close</a>
            </div>
        </div>
    </div>

    <div id="ex1" class="modal" style="border: solid 1px #008a8a;">

        <div class="col-md-12 controls">
            <div class="col-md-5">
                <p class="popupControl">MOQ : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox CssClass="control_dropdown" ID="txtMOQ" ReadOnly="true" runat="server"></asp:TextBox>
            </div>
        </div>

        <div class="col-md-12 controls">
            <div class="col-md-5">
                <p class="popupControl">Order Type : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox CssClass="control_dropdown" ReadOnly="true" ID="lblOrderType" runat="server"></asp:TextBox>
            </div>

        </div>
        <div class="col-md-12 controls" id="divSchedule" style="display: none;">

            <div class="col-md-5">
                <p class="popupControl">Schedule Type : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ReadOnly="true" ID="txtFrequency" runat="server"></asp:TextBox>
            </div>
            <%-- <div class="col-md-5">
                <p class="popupControl">Number Of Order : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtNumOrder" CssClass="control_dropdown" runat="server"></asp:TextBox>
            </div>--%>
        </div>
        <%--<div class="col-md-12 controls" >
            <div class="col-md-5">
                <p class="popupControl">Quantity Per Shipment : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtQuantity" CssClass="control_dropdown" runat="server"></asp:TextBox>
            </div>
        </div>--%>
        <div class="col-md-12 controls">
            <div class="col-md-5">
                <p class="popupControl">Request date: </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtOrderDate" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12 controls">
            <div class="col-md-5">
                <p class="popupControl">Purchase Order Comment: </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtPOComment" TextMode="MultiLine" Rows="4" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-5">
                <asp:HiddenField ID="hdnCustNum" runat="server" />
                <asp:HiddenField ID="hdnCustName" runat="server" />
                <asp:HiddenField ID="hdnID" runat="server" />
                <asp:HiddenField ID="hdnRef" runat="server" />
                <asp:HiddenField ID="hdnItem" runat="server" />
            </div>
            <div class="col-md-7 btncontrol">
                <%--CausesValidation="false" --%>
                <input type="button" id="btnOrder" class="btn green" onclick="Order();" title="Place Order" value="Place Order" />
                <%--<asp:Button ID="btnPlace"  Text="Place Order" runat="server" CssClass="btnSubmit" OnClick="btnPlace_Click" />--%>
                <a href="#" rel="modal:close">Close</a>
            </div>
        </div>
    </div>


</asp:Content>

