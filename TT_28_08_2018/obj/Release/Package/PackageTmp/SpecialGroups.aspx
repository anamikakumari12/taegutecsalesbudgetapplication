﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="SpecialGroups.aspx.cs" Inherits="TaegutecSalesBudget.SpecialGroups" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link href="GridviewScroll.css" rel="stylesheet" />
       <script type="text/javascript" src="gridscroll.js"></script>
    <div class="crumbs">    <!-- Start : Breadcrumbs -->
<ul id="breadcrumbs" class="breadcrumb">
<li>
<i class="fa fa-home"></i>
<a>Admin</a>
</li>                       
<li class="current">Special Groups</li>
</ul>
  </div>  <!-- End : Breadcrumbs -->
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360">
</asp:ScriptManager>
    <br />
  <asp:UpdatePanel ID="UpdatePanel1" runat="server"  >
    <ContentTemplate>
     <div class="row" style="padding-left:15px" >
     <div  class="col-md-4 " >
        <div class="form-group">
        <label class="col-md-4 control-label " >PRODUCT FAMILY</label>
        <div class="col-md-4">   
            <asp:DropDownList ID="ddlProductFamliy" runat="server"  CssClass="form-control" Width="230px" AutoPostBack="True" OnSelectedIndexChanged="ddlProductFamliy_SelectedIndexChanged" >
            <asp:ListItem>SELECT FAMILY</asp:ListItem>
            </asp:DropDownList>
            </div>
        </div>
     </div>
      <div  class="col-md-4" >
        <div class="form-group">
        <label class="col-md-3 control-label " >PRODUCT LINE</label>
        <div class="col-md-4">   
            <asp:DropDownList ID="ddlProductLine" runat="server"  CssClass="form-control" Width="230px" >
            <asp:ListItem>SELECT PRODUCT LINE</asp:ListItem>
            </asp:DropDownList>
         </div>
        </div>
     </div>

           <div  class="col-md-4" >
        <div class="form-group">
        <label class="col-md-3 control-label " >YEAR</label>
        <div class="col-md-4">   
            <asp:DropDownList ID="B_BUDGETYEAR" runat="server"  CssClass="form-control" Width="230px" >
          
            </asp:DropDownList>
         </div>
        </div>
     </div>
         <div class="row">
             <div class="col-md-4"></div>
         <div  class="col-md-6 "  style="margin-top:4px;" >
            <div class="form-group">                            
                 <asp:Button ID="proceedbtn" runat="server" Text="FILTER" OnClick="proceed_Click"  CssClass="btn green lefts" />
                 <asp:Button ID="updatebtn" runat="server" Text="UPDATE" OnClick="update_Click"  CssClass="btn green lefts" />          
            </div>
        </div>    
             </div>  
    </div>
 <%--   ----GRID VIEW----START------%>
    <div class="row" style="padding-left:15px">
                                    <%--class="col-md-5">--%>
<asp:GridView ID="spclgroupsgrid" runat="server" Width="100%" Style="text-align: center;" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" >
    <columns>
        <asp:TemplateField HeaderText="ITEM CODE" ItemStyle-Width="100px"  >
            <ItemTemplate>
            <asp:Label  ID="item_code_lbl" runat="server" Text='<%# Eval("item_code") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
                                         
        <asp:TemplateField HeaderText="ITEM NAME" ItemStyle-Width="250px" >
            <ItemTemplate>
            <asp:Label ID="Label1"  runat="server" Text='<%# Eval("item_short_name") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="GOLD"  ItemStyle-Width="100px" Visible="false">
            <ItemTemplate>
                <asp:CheckBox ID="gold_flag"   runat="server" Checked='<%# bool.Parse(Eval("gold_flag").ToString() == "Y" ? "True": "False") %>' /> 
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="TOP" ItemStyle-Width="100px" Visible="false" >
            <ItemTemplate>
            <asp:CheckBox ID="top_flag" runat="server"  Checked='<%# bool.Parse(Eval("top_flag").ToString() == "Y" ? "True": "False") %>' /> 
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="FIVE YEARS"  ItemStyle-Width="100px" Visible="false">
            <ItemTemplate>
                <asp:CheckBox ID="five_years_flag"   runat="server"   Checked='<%# bool.Parse(Eval("five_years_flag").ToString() == "Y" ? "True": "False") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="BB" ItemStyle-Width="100px" Visible="false" >
            <ItemTemplate>
                <asp:CheckBox ID="bb_flag" Visible="false"  runat="server" Checked='<%# bool.Parse(Eval("bb_flag").ToString() == "Y" ? "True": "False") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="SPC" ItemStyle-Width="100px" Visible="false">
            <ItemTemplate>
                <asp:CheckBox ID="SPC_flag"  runat="server" Checked='<%# bool.Parse(Eval("SPC_flag").ToString() == "Y" ? "True": "False") %>' />
            </ItemTemplate>
        </asp:TemplateField>
            <asp:TemplateField HeaderText="TEN YEARS" ItemStyle-Width="100px" Visible="false">
            <ItemTemplate>
                <asp:CheckBox ID="Ten_years_flag"  runat="server" Checked='<%# bool.Parse(Eval("ten_years_flag").ToString() == "Y" ? "True": "False") %>' />
            </ItemTemplate>
        </asp:TemplateField>
                                         
    </columns>
</asp:GridView>
</div>
        </ContentTemplate>
      <Triggers>
                     <asp:AsyncPostBackTrigger ControlID="proceedbtn" EventName="Click" />
                     <asp:AsyncPostBackTrigger ControlID="updatebtn" EventName="Click"/>
                    <asp:AsyncPostBackTrigger ControlID="ddlProductFamliy" EventName="SelectedIndexChanged" />
      </Triggers>
  </asp:UpdatePanel>
    <asp:UpdateProgress id="updateProgress" runat="server">
    <ProgressTemplate>
        <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
           
             <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color:#fff" >Please wait</span>
        </div>
    </ProgressTemplate>
 </asp:UpdateProgress> 
    <%-- GRID VIEW END--%>
    <style type="text/css">
        td {
            height: 30px !important;
            padding-left: 10px;
            text-align: left;
        }
        th {
            background: #006780;
            color: #fff;
            font-weight: 600;
            font-size: 13px;
            border-color: #fff;
            height: 40px;
            padding-left: 10px;
            text-align: left;
        }
    
        
    </style>
    
    <script type="text/javascript">

        $(document).ready(function () {
            gridviewScrollTrigger();
            $(window).resize(function () {

                gridviewScrollTrigger();
            });
        });
        function gridviewScrollTrigger() {

            gridView1 = $('#MainContent_spclgroupsgrid').gridviewScroll({
                width: $(window).width() - 60,
                height: findminofscreenheight($(window).height(), 500),
                railcolor: "#F0F0F0",
                barcolor: "#606060",
                barhovercolor: "#606060",
                bgcolor: "#F0F0F0",
                freezesize: 0,
                arrowsize: 30,
                varrowtopimg: "Images/arrowvt.png",
                varrowbottomimg: "Images/arrowvb.png",
                harrowleftimg: "Images/arrowhl.png",
                harrowrightimg: "Images/arrowhr.png",
                headerrowcount: 1,
                railsize: 16,
                barsize: 14,
                verticalbar: "auto",
                horizontalbar: "auto",
                wheelstep: 1,
            });
        }

        function findminofscreenheight(a, b) {
            a = a - $("#MainContent_ddlProductFamliy").offset().top;
            return a < b ? a : b;
        }
</script>
</asp:Content>

