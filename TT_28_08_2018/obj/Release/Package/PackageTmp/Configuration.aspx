﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Configuration.aspx.cs" Inherits="TaegutecSalesBudget.Configuration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="crumbs">
        <!-- Start : Breadcrumbs -->
        <ul id="breadcrumbs" class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a>Admin</a>
            </li>


            <li class="current">Configuration</li>
        </ul>

    </div>
    <!-- End : Breadcrumbs -->
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360">
    </asp:ScriptManager>
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet-body">
                        <div id="accordion" class="panel-group">
                            <div class="panel panel-default">
                                <div id="collapseline_bTab" class="panel-heading1">
                                    <h4 class="panel-title">
                                        <img id="tab_image" src="images/button_minus.gif" align="left" />
                                        &nbsp;&nbsp;&nbsp;&nbsp; BUDGET TAB VISIBLE FOR
                                    </h4>
                                </div>
                                <div>
                                    <div id="tab" style="background: #fbfbfb;">
                                        <div id="divfilter" class="row" style="padding-left: 42px">

                                            <div class="clearfix"></div>

                                            <div class="col-md-12 ">
                                                <div class="form-group col-md-3">
                                                    <label class="control-label1" style="width: 135px !important; font-weight: 600">DATE FORMAT : </label>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label class="control-label1" style="width: 265px !important" runat="server" id="lbl_Date">YEAR - MONTH - DATE (Ex: 2015-06-25)</label>

                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="col-md-12 ">
                                                <div class="form-group col-md-3">
                                                    <label class="control-label1" style="width: 135px !important; font-weight: 600">SALES ENGINEER : </label>
                                                </div>

                                                <div class="form-group col-md-3">
                                                    <label class="col-md-3 control-label1" style="width: 130px !important">START DATE</label>
                                                    <asp:TextBox runat="server" ID="txt_SE_StartDate" class="form-control-input  col-md-6 datepicker" onKeyDown="preventBackspace();" onkeypress="return allowOnlyNumber(event);"></asp:TextBox>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label class="col-md-3 control-label1" style="width: 130px !important">END DATE</label>
                                                    <asp:TextBox runat="server" ID="txt_SE_EndDate" class="form-control-input  col-md-6 datepicker" onKeyDown="preventBackspace();" onkeypress="return allowOnlyNumber(event);"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="col-md-12 ">
                                                <div class="form-group col-md-3">
                                                    <label class="control-label1" style="width: 140px !important; font-weight: 600">BRANCH MANAGER : </label>
                                                </div>

                                                <div class="form-group col-md-3">
                                                    <label class="col-md-3 control-label1" style="width: 130px !important">START DATE</label>
                                                    <asp:TextBox runat="server" ID="txt_BM_StartDate" class="form-control-input  col-md-6 datepicker" onKeyDown="preventBackspace();" onkeypress="return allowOnlyNumber(event);"></asp:TextBox>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label class="col-md-3 control-label1" style="width: 130px !important">END DATE</label>
                                                    <asp:TextBox runat="server" ID="txt_BM_EndDate" class="form-control-input  col-md-6 datepicker" onKeyDown="preventBackspace();" onkeypress="return allowOnlyNumber(event);"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <div class="col-md-12 ">
                                                <div class="form-group col-md-3">
                                                    <label class="control-label1" style="width: 165px !important; font-weight: 600">TERRITORY MANAGER : </label>
                                                </div>

                                                <div class="form-group col-md-3">
                                                    <label class="col-md-3 control-label1" style="width: 130px !important">START DATE</label>
                                                    <asp:TextBox runat="server" ID="txt_TM_StartDate" class="form-control-input  col-md-6 datepicker" onKeyDown="preventBackspace();" onkeypress="return allowOnlyNumber(event);"></asp:TextBox>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label class="col-md-3 control-label1" style="width: 130px !important">END DATE</label>
                                                    <asp:TextBox runat="server" ID="txt_TM_EndDate" class="form-control-input  col-md-6 datepicker" onKeyDown="preventBackspace();" onkeypress="return allowOnlyNumber(event);"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>


                                            <div class="col-md-12 ">
                                                <div class="form-group col-md-3">
                                                    <label class="control-label1" style="width: 135px !important; font-weight: 600">HEAD OFFICE : </label>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label class="col-md-3 control-label1" style="width: 130px !important">START DATE</label>
                                                    <asp:TextBox runat="server" ID="txt_HO_StartDate" class="form-control-input  col-md-6 datepicker" onKeyDown="preventBackspace();" onkeypress="return allowOnlyNumber(event);"></asp:TextBox>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label class="col-md-3 control-label1" style="width: 130px !important">END DATE</label>
                                                    <asp:TextBox runat="server" ID="txt_HO_EndDate" class="form-control-input  col-md-6 datepicker" onKeyDown="preventBackspace();" onkeypress="return allowOnlyNumber(event);"></asp:TextBox>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <asp:Button runat="server" ID="btn_BVisibleUpdate" class="btn green col-md-3" Text="Update" OnClick="btn_BVisibleUpdate_Click" />
                                                </div>
                                            </div>

                                            <div class="col-md-12 ">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div id="collapseInc" class="panel-heading2">
                                    <h4 class="panel-title">
                                        <img id="Incgrid_image" src="images/button_minus.gif" align="left" />
                                        &nbsp;&nbsp;&nbsp;&nbsp; INCREASE RATE
                                    </h4>
                                </div>
                                <div>
                                    <div id="Incgrid" style="background: #fbfbfb;">
                                        <div class="row">
                                            <div class="form-group col-md-8" style="margin-top: 15px; padding-bottom: 27px; padding-left: 42px;">
                                                <label class="col-md-3 control-label1">INCREASE RATE (%)</label>
                                                <asp:TextBox runat="server" ID="txtIncRate" class="form-control-input col-md-2" Style="margin-right: 25px;" ViewStateMode="Enabled"
                                                    onkeypress="return isNumberKey(event)" MaxLength="3"></asp:TextBox>
                                                <asp:Button ID="btn_IR_Update" runat="server" Text="Update" CssClass="btn green col-md-3" OnClick="btn_IR_Update_Click" />

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div id="collapsebtn" class=" panel-heading3 ">
                                    <h4 class="panel-title">
                                        <img id="product_image" src="images/button_minus.gif" align="left">
                                        &nbsp;&nbsp;&nbsp;&nbsp; REPORTS
                                    </h4>
                                </div>
                                <div id="prdgroup" style="display: block; background: rgb(251, 251, 251);">
                                    <div class="row">
                                        <div class="form-group col-md-8" style="margin-top: 15px; padding-left: 42px;">
                                            <label class="col-md-3 control-label1">BUDGET YEAR</label>
                                            <asp:TextBox runat="server" ID="txtBudgetYear" class="form-control-input col-md-2" Style="margin-right: 25px;" ViewStateMode="Enabled"
                                                onkeypress="return isNumberKey(event)" MaxLength="4"></asp:TextBox>
                                            <asp:Button ID="btnBYUpdate" runat="server" Text="Update" CssClass="btn green col-md-3" OnClick="btnBYUpdate_Click" />

                                        </div>
                                        <div class="form-group col-md-8" style="margin-top: 15px; padding-left: 42px;">
                                            <label class="col-md-3 control-label1">ACTUAL YEAR</label>
                                            <asp:TextBox runat="server" ID="txtActualYear" class="form-control-input col-md-2" Style="margin-right: 25px;" ViewStateMode="Enabled"
                                                onkeypress="return isNumberKey(event)" MaxLength="4"></asp:TextBox>
                                            <asp:Button ID="btn_AY_Update" runat="server" Text="Update" CssClass="btn green col-md-3" OnClick="btn_AY_Update_Click" />

                                        </div>
                                        <div class="form-group col-md-8" style="margin-top: 15px; padding-left: 42px;">
                                            <label class="col-md-3 control-label1">ACTUAL MONTH</label>
                                            <asp:DropDownList runat="server" ID="ddlActualMonth" class="form-control-input col-md-2" Style="margin-right: 25px;">

                                                <asp:ListItem Value="1">JANUARY</asp:ListItem>
                                                <asp:ListItem Value="2">FEBRUARY</asp:ListItem>
                                                <asp:ListItem Value="3">MARCH</asp:ListItem>
                                                <asp:ListItem Value="4">APRIL</asp:ListItem>
                                                <asp:ListItem Value="5">MAY</asp:ListItem>
                                                <asp:ListItem Value="6">JUNE</asp:ListItem>
                                                <asp:ListItem Value="7">JULY</asp:ListItem>
                                                <asp:ListItem Value="8">AUGUST</asp:ListItem>
                                                <asp:ListItem Value="9">SEPTEMBER</asp:ListItem>
                                                <asp:ListItem Value="10">OCTOBER</asp:ListItem>
                                                <asp:ListItem Value="11">NOVEMBER</asp:ListItem>
                                                <asp:ListItem Value="12">DECEMBER</asp:ListItem>

                                            </asp:DropDownList>
                                            <%--<asp:TextBox runat="server" ID="txtActualMonth" class="form-control-input col-md-2" Style="margin-right:25px;"   ViewStateMode="Enabled" 
                                                    onkeypress="return isNumberKey(event)" MaxLength="2" ></asp:TextBox>--%>
                                            <asp:Button ID="btn_AM_Update" runat="server" Text="Update" CssClass="btn green col-md-3" OnClick="btn_AM_Update_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div id="collapseline" class="panel-heading4">
                                    <h4 class="panel-title">
                                        <img id="linegrid_image" src="images/button_minus.gif" align="left">
                                        &nbsp;&nbsp;&nbsp;&nbsp; BUDGET
                                    </h4>
                                </div>

                                <div id="linegrid" style="background: #fbfbfb;">
                                    <div class="row">
                                        <div class="form-group col-md-8" style="margin-top: 15px; padding-left: 42px;">
                                            <label class="col-md-3 control-label1">BUDGET YEAR</label>
                                            <asp:TextBox runat="server" ID="txtBudgetYear_B" class="form-control-input col-md-2" Style="margin-right: 25px;" ViewStateMode="Enabled"
                                                onkeypress="return isNumberKey(event)" MaxLength="4"></asp:TextBox>
                                            <asp:Button ID="btnBYUpdate_B" runat="server" Text="Update" CssClass="btn green col-md-3" OnClick="btnBYUpdate_B_Click" />

                                        </div>
                                        <div class="form-group col-md-8" style="margin-top: 15px; padding-left: 42px;">
                                            <label class="col-md-3 control-label1">ACTUAL YEAR</label>
                                            <asp:TextBox runat="server" ID="txtActualYear_B" class="form-control-input col-md-2" Style="margin-right: 25px;" ViewStateMode="Enabled"
                                                onkeypress="return isNumberKey(event)" MaxLength="4"></asp:TextBox>
                                            <asp:Button ID="btn_AY_Update_B" runat="server" Text="Update" CssClass="btn green col-md-3" OnClick="btn_AY_Update_B_Click" />

                                        </div>
                                        <div class="form-group col-md-8" style="margin-top: 15px; padding-left: 42px;">
                                            <label class="col-md-3 control-label1">ACTUAL MONTH</label>
                                            <asp:DropDownList runat="server" ID="ddlActualMonth_B" class="form-control-input col-md-2" Style="margin-right: 25px;">

                                                <asp:ListItem Value="1">JANUARY</asp:ListItem>
                                                <asp:ListItem Value="2">FEBRUARY</asp:ListItem>
                                                <asp:ListItem Value="3">MARCH</asp:ListItem>
                                                <asp:ListItem Value="4">APRIL</asp:ListItem>
                                                <asp:ListItem Value="5">MAY</asp:ListItem>
                                                <asp:ListItem Value="6">JUNE</asp:ListItem>
                                                <asp:ListItem Value="7">JULY</asp:ListItem>
                                                <asp:ListItem Value="8">AUGUST</asp:ListItem>
                                                <asp:ListItem Value="9">SEPTEMBER</asp:ListItem>
                                                <asp:ListItem Value="10">OCTOBER</asp:ListItem>
                                                <asp:ListItem Value="11">NOVEMBER</asp:ListItem>
                                                <asp:ListItem Value="12">DECEMBER</asp:ListItem>

                                            </asp:DropDownList>
                                            <%--<asp:TextBox runat="server" ID="txtActualMonth_B" class="form-control-input col-md-2" Style="margin-right:25px;"   ViewStateMode="Enabled" 
                                                    onkeypress="return isNumberKey(event)" MaxLength="2" ></asp:TextBox>--%>
                                            <asp:Button ID="btn_AM_Update_B" runat="server" Text="Update" CssClass="btn green col-md-3" OnClick="btn_AM_Update_B_Click" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="collapselink" class="panel-heading1">
                                    <h4 class="panel-title">
                                        <img id="img_link" src="images/button_minus.gif" align="left">
                                        &nbsp;&nbsp;&nbsp;&nbsp; LINK VISIBILITY
                                    </h4>
                                </div>
                                <div id="divlink" style="background: #fbfbfb;">
                                    <div class="row">
                                        <div class="form-group col-md-8" style="margin-top: 15px; padding-left: 42px;">
                                            <%--<as class="col-md-3 control-label1" Id="lbllink" >BRANCH WISE VISIBILITY</as>    --%>
                                            <asp:Label runat="server" CssClass="col-md-3 control-label1" ID="lbllink" Text="BRANCH WISE APPROVAL" />
                                            <asp:CheckBoxList ID="CheckBoxList1" runat="server" CssClass="form-control-input col-md-2">
                                                <asp:ListItem>Territory Manager</asp:ListItem>
                                                <asp:ListItem>Head Office</asp:ListItem>
                                            </asp:CheckBoxList>
                                            <asp:Button ID="btnlink" runat="server" Text="Update" CssClass="btn green col-md-3" Style="margin-left: 25px;" OnClick="btnlink_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div id="collapseentry" class="panel-heading2">
                                    <h4 class="panel-title">
                                        <img id="img_entry" src="images/button_minus.gif" align="left">
                                        &nbsp;&nbsp;&nbsp;&nbsp; ENABLE BUDGET ENTRY 
                                    </h4>
                                </div>
                                <div id="diventry" style="background: #fbfbfb;">
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group col-md-3">
                                                <label class="control-label1" style="width: 100% !important; padding-left: 25px;">ENABLE BUDGET ENTRY FOR : </label>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <asp:DropDownList runat="server" ID="ddlBranch" class="form-control-input"></asp:DropDownList>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label class="col-md-3 control-label1" style="width: 130px !important">YEAR</label>
                                                <asp:TextBox runat="server" ID="txtYear" Enabled="false" class="form-control-input col-md-2"></asp:TextBox>
                                                <asp:Button ID="btnEnable" runat="server" Text="Enable" Style="margin-left: 25px;" CssClass="btn green col-md-3" OnClick="btnEnable_Click" />

                                            </div>
                                            <div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default" runat="server">
                                <div id="CollapseSpclsVisibility" class="panel-heading1">
                                    <h4 class="panel-title">
                                        <img id="img_spl" src="images/button_minus.gif" align="left">
                                        &nbsp;&nbsp;&nbsp;&nbsp; SPECIAL GROUP VISIBILITY
                                    </h4>
                                </div>
                                <div id="splv" style="background: #fbfbfb;">
                                    <div class="row">
                                        <div class="form-group col-md-8" style="margin-top: 15px; padding-left: 42px;">
                                            <asp:Label CssClass="col-md-2" runat="server"> YEAR</asp:Label>
                                            <asp:DropDownList runat="server" ID="ddlyears" class="form-control-input" OnSelectedIndexChanged="ddlyears_SelectedIndexChanged" AutoPostBack="true" Width="110px"></asp:DropDownList>
                                        </div>
                                        <div class="form-group col-md-8" style="margin-top: 15px; padding-left: 42px;">
                                            <asp:Label CssClass="col-md-2" runat="server"> SPECIAL GROUPS</asp:Label>

                                            <asp:CheckBoxList runat="server" ID="pgl" class="form-control-input" RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="col-md-4" RepeatColumns="3"></asp:CheckBoxList>
                                            <asp:Button ID="btnspcl" runat="server" Text="Save" CssClass="btn green col-md-3" OnClick="btnspcl_Click" Visible="true" Style="margin-left: 25px; margin-top: 10px" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default" runat="server">
                                <div id="CollapseSyncGAL" class="panel-heading2">
                                    <h4 class="panel-title">
                                        <img id="img_sync" src="images/button_minus.gif" align="left">
                                        &nbsp;&nbsp;&nbsp;&nbsp; SYNC WITH GAL
                                    </h4>
                                </div>
                                <div id="syncgal" style="background: #fbfbfb;">
                                    <div class="row">
                                        <div class="form-group col-md-8" style="margin-top: 15px; padding-left: 42px;">
                                            <asp:Button runat="server" ID="btnsyncgal" Text="SYNC" class="form-control-input" OnClick="btnsyncGal" AutoPostBack="true" Width="110px" CssClass="btn green"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End : Inner Page container -->
        </ContentTemplate>
        <Triggers>

            <asp:AsyncPostBackTrigger ControlID="btnBYUpdate" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btn_AM_Update" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btn_IR_Update" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btn_IR_Update" EventName="Click" />
            <%--<asp:PostBackTrigger ControlID="btnNewCustomer"/>--%>
        </Triggers>
    </asp:UpdatePanel>
    <%--  <a style="display: inline;" class="scrollup" href="javascript:void(0);">Scroll</a>--%>

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <script>
        $(document).ready(function () { triggerScript(); });
        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return false;
        }
        function preventBackspace(e) {
            var evt = e || window.event;
            if (evt) {
                var keyCode = evt.charCode || evt.keyCode;
                if (keyCode === 8) {
                    if (evt.preventDefault) {
                        evt.preventDefault();
                    } else {
                        evt.returnValue = false;
                    }
                }
            }
        }
        function triggerScript() {
            $('#collapsebtn').click(function () {

                var attr = $('#product_image').attr('src');
                //var imgsrc=images/button_plus.gif;
                $("#prdgroup").slideToggle();
                if (attr == "images/button_minus.gif") {
                    $("#product_image").attr("src", "images/button_plus.gif");
                } else {
                    $("#product_image").attr("src", "images/button_minus.gif");
                }


            });
            $('#collapseline').click(function () {
                var attr = $('#linegrid_image').attr('src');
                $("#linegrid").slideToggle();
                if (attr == "images/button_minus.gif") {
                    $("#linegrid_image").attr("src", "images/button_plus.gif");
                } else {
                    $("#linegrid_image").attr("src", "images/button_minus.gif");
                }

            });


            $('#collapseInc').click(function () {
                var attr = $('#Incgrid_image').attr('src');
                $("#Incgrid").slideToggle();
                if (attr == "images/button_minus.gif") {
                    $("#Incgrid_image").attr("src", "images/button_plus.gif");
                } else {
                    $("#Incgrid_image").attr("src", "images/button_minus.gif");
                }

            });

            $('#collapseline_bTab').click(function () {
                var attr = $('#tab_image').attr('src');
                $("#tab").slideToggle();
                if (attr == "images/button_minus.gif") {
                    $("#tab_image").attr("src", "images/button_plus.gif");
                } else {
                    $("#tab_image").attr("src", "images/button_minus.gif");
                }

            });

            $('#collapselink').click(function () {
                var attr = $('#img_link').attr('src');
                $("#divlink").slideToggle();
                if (attr == "images/button_minus.gif") {
                    $("#img_link").attr("src", "images/button_plus.gif");
                } else {
                    $("#img_link").attr("src", "images/button_minus.gif");
                }

            });

            $('#collapseentry').click(function () {
                var attr = $('#img_entry').attr('src');
                $("#diventry").slideToggle();
                if (attr == "images/button_minus.gif") {
                    $("#img_entry").attr("src", "images/button_plus.gif");
                } else {
                    $("#img_entry").attr("src", "images/button_minus.gif");
                }

            });

            $('#CollapseSpclsVisibility').click(function () {
                var attr = $('#img_spl').attr('src');
                $("#splv").slideToggle();
                if (attr == "images/button_minus.gif") {
                    $("#img_spl").attr("src", "images/button_plus.gif");
                } else {
                    $("#img_spl").attr("src", "images/button_minus.gif");
                }

            });
            $('#CollapseSyncGAL').click(function () {
                var attr = $('#img_sync').attr('src');
                $("#syncgal").slideToggle();
                if (attr == "images/button_minus.gif") {
                    $("#img_sync").attr("src", "images/button_plus.gif");
                } else {
                    $("#img_sync").attr("src", "images/button_minus.gif");
                }

            });
            $('#MainContent_btnBYUpdate').click(function (e) {
                var value = $('#MainContent_txtBudgetYear').val();
                if (value != undefined && value != "" && value != "0" && !isNaN(value) && value.length == 4) { }
                else { alert("Budget Year should not be empty and It should be valid Year"); e.preventDefault(); }
            });

            $('#MainContent_btn_AY_Update').click(function (e) {
                var value = $('#MainContent_txtActualYear').val();
                if (value != undefined && value != "" && value != "0" && !isNaN(value) && value.length == 4) { }
                else { alert("Actual Year should not be empty and It should be valid Year"); e.preventDefault(); }
            });


            $('#MainContent_btnBYUpdate_B').click(function (e) {
                var value = $('#MainContent_txtBudgetYear_B').val();
                if (value != undefined && value != "" && value != "0" && !isNaN(value) && value.length == 4) { }
                else { alert("Budget Year should not be empty and It should be valid Year"); e.preventDefault(); }
            });

            $('#MainContent_btn_AY_Update_B').click(function (e) {
                var value = $('#MainContent_txtActualYear_B').val();
                if (value != undefined && value != "" && value != "0" && !isNaN(value) && value.length == 4) { }
                else { alert("Actual Year should not be empty and It should be valid Year"); e.preventDefault(); }
            });


            $('#MainContent_btn_IR_Update').click(function (e) {
                var value = $('#MainContent_txtIncRate').val();
                if (value != undefined && value != "" && !isNaN(value)) { }
                else { alert("Increase Rate should not be empty"); e.preventDefault(); }
            });



            $(function () {
                $(".datepicker").datepicker({ minDate: 0, dateFormat: 'yy-mm-dd' });
                //$(".datepickerSE").datepicker({ minDate: 0, dateFormat: 'yy-mm-dd' });
                //$(".datepickerBM").datepicker({ minDate: 0, dateFormat: 'yy-mm-dd' });
                //$(".datepickerTM").datepicker({ minDate: 0, dateFormat: 'yy-mm-dd' });
                //$(".datepickerHO").datepicker({ minDate: 0, dateFormat: 'yy-mm-dd' });

                $("#MainContent_txt_SE_StartDate").change(function () {
                    var endSE = $("#MainContent_txt_SE_StartDate").val();
                    //alert(endSE);
                    $(".datepickerSE").datepicker({ minDate: endSE, dateFormat: 'yy-mm-dd' });
                });
                $("#MainContent_txt_BM_StartDate").change(function () {
                    var endBM = $("#MainContent_txt_BM_StartDate").val();
                    $(".datepickerBM").datepicker({ minDate: endBM, dateFormat: 'yy-mm-dd' });
                });
                $("#MainContent_txt_TM_StartDate").change(function () {
                    var endTM = $("#MainContent_txt_TM_StartDate").val();
                    $(".datepickerTM").datepicker({ minDate: endTM, dateFormat: 'yy-mm-dd' });
                });
                $("#MainContent_txt_HO_StartDate").change(function () {
                    var endHO = $("#MainContent_txt_HO_StartDate").val();
                    $(".datepickerHO").datepicker({ minDate: endHO, dateFormat: 'yy-mm-dd' });
                });

            });


        }
        function isNumberKey(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false; }
            return true;
        }


    </script>




    <style type="text/css">
        th, td {
            border: 1px solid #ddd;
        }

        th {
            padding: 13px;
            text-align: center;
        }

        td {
            padding: 4px;
            /*text-align:right;*/
        }

        .control-label {
            font-weight: normal;
            padding-top: 5px;
        }

        .greendark, .color_4, .color_5 {
            text-align: left;
        }


        /*#familygrid table tr:last-child {
            background-color:#999;
        }
        #linegrid table tr:last-child {
            background-color:#999;
        }
       #familygrid table tr:last-child td:first-child {
            text-align:center;
        }
        #linegrid table tr:last-child td:first-child {
           text-align:center;
        }*/

        .color_total {
            font-size: 14px;
            background-color: #999;
        }


        .panel-group .panel {
            border-radius: 4px;
            margin-bottom: 0;
            overflow: hidden;
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .panel-heading1 {
            background: #333;
            color: #29AAe1 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading2 {
            background: #999;
            color: #333 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading3 {
            background: #333;
            color: #8ac340 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading4 {
            background: #999;
            color: #91298E !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading5 {
            background: #333;
            color: #F05A26 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading6 {
            background: #999;
            color: #002238 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading7 {
            background: #333;
            color: #fff !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }
    </style>
</asp:Content>
