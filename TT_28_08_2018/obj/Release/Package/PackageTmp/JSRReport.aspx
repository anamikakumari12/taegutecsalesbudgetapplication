﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JSRReport.aspx.cs" Inherits="TaegutecSalesBudget.JSRReport" MasterPageFile="~/Site.Master" %>


<asp:Content ContentPlaceHolderID="HeadContent" ID="content1" runat="server">
    <script type="text/javascript" src="js/Chart.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" ID="content2" runat="server">
       <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true" >
   </asp:ScriptManager>
   
     
   <asp:UpdatePanel ID="UpdatePanel1" runat="server"  >
       <ContentTemplate>  
         
       <div class="crumbs">
            <!-- Start : Breadcrumbs -->
            <ul id="breadcrumbs" class="breadcrumb">
               <li class="current">JSR REPORT</li>
                 <div>
                <ul>
               <li class="title_bedcrum"  style="list-style:none;">JSR REPORT</li>
                </ul>
                     </div>
            </ul>
             <!-- End : Breadcrumbs -->
         </div>

        <div id="collapsebtn" class="row">
            <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%; padding:10px;"/> 
        </div>    
                 
       <div class="row filter_panel" id="reportdrpdwns" runat="server" >
       <div runat="server" id="cterDiv" visible="false">
                       <ul id="divCter" runat="server" class="btn-info rbtn_panel"  >
                    <li> <span style="margin-right:4px;vertical-align:text-bottom;  ">TAEGUTEC</span>
                       
                    <asp:RadioButton ID="rdBtnTaegutec"  AutoPostBack="true"  Checked="true"  OnCheckedChanged="rdBtnTaegutec_CheckedChanged"   GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">DURACARB</span>
                    <asp:RadioButton ID="rdBtnDuraCab"  AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged"    GroupName="byCmpnyCodeInradiobtn" runat="server" />
                    </li>
                </ul>
               </div>
                       <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label"><span id="fdstar" style="color:red;">*</span>MONTH</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="txtMonth" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                </div>             
                <div class="col-md-4" runat="server" id="divBranch">
                                <div class="form-group">
                                                <label class="col-md-4 control-label">BRANCH</label>
                                                <div class="col-md-4">    
                                                    <asp:DropDownList ID="ddlBranchList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control select2" Width="230px">
                                                </asp:DropDownList>                                             
                                                </div>
                                            </div>

                                        </div>
                                       <div  class="col-md-4" runat="server" id="divSE">
                                        <div class="form-group">
                                                <label class="col-md-4 control-label">SALES ENGINEER </label>
                                                <div class="col-md-4" >
                                                    
                            <asp:DropDownList ID="ddlSalesEngineerList" runat="server" AutoPostBack="True"
                                   CssClass="form-control" Width="230px" >
                                                     <asp:ListItem>--SELECT SALES ENGINEER--</asp:ListItem>
                              </asp:DropDownList>    
                                                                                                
                                                </div>
                                            </div>
                                            
                                        </div>
                                         <div class="col-md-4" runat="server" id="divCT">
                                   <div class="form-group">
                                   <label class="col-md-4 control-label">CUSTOMER TYPE </label>
                                   <div class="col-md-4">    
                                   <asp:DropDownList ID="ddlcustomertype" runat="server" AutoPostBack="True"
                                      CssClass="form-control" Width="230px">
                                   <asp:ListItem Text="ALL" Value="ALL" />
                                   <asp:ListItem Text="CUSTOMER" Value="C" />
                                   <asp:ListItem Text="CHANNEL PARTNER" Value="D" />
                                     </asp:DropDownList>                                                
                                   </div>
                                   </div>
                                       
                                         </div>   
                                        <div class="col-md-4" runat="server" id="divcustomer" >
<div class="form-group">
                                                <label class="col-md-4 control-label">CUSTOMER NAME </label>
                                                <div class="col-md-4">    
<asp:DropDownList ID="ddlCustomerList" runat="server" 
                                                    CssClass="form-control" Width="230px">
                                                     <asp:ListItem>--SELECT CUSTOMER --</asp:ListItem>
                                                </asp:DropDownList>                                             
                                                </div>
                                            </div>
                                            
                                        </div>

                                         <div  class="col-md-4 " >
                                            <div class="form-group">
                                            <label class="col-md-4 control-label " >CUSTOMER NUMBER</label>
                                            <div class="col-md-4">   
                                                <asp:DropDownList ID="ddlCustomerNumber" runat="server"  CssClass="form-control" Width="230px" >
                                                <asp:ListItem>SELECT CUSTOMER NUMBER</asp:ListItem>
                                                </asp:DropDownList>
                                             </div>
                                            </div>
                                        </div>
                                                                                     
                                        <div  class="col-md-4 " >
                                            <div class="form-group">
                                            <label class="col-md-4 control-label " >PRODUCT GROUP</label>
                                            <div class="col-md-4">   
                                                 <asp:DropDownList ID="ddlProductGroup" runat="server"  CssClass="form-control" Width="230px" AutoPostBack="true" >
                                                
                                                <asp:ListItem Value="ALL" Text="ALL"></asp:ListItem>
                                                <asp:ListItem Value="GOLD" Text="GOLD"></asp:ListItem>
                                                <asp:ListItem Value="BB" Text="BB"></asp:ListItem>
                                                <asp:ListItem Value="5YRS" Text="5YRS"></asp:ListItem>
                                                <asp:ListItem Value="SPC" Text="SPC"></asp:ListItem>
                                                <asp:ListItem Value="TOP" Text="TOP"></asp:ListItem>
                                                </asp:DropDownList>
                                             </div>
                                            </div>
                                        </div>
                                         <div  class="col-md-4 " >
                                            <div class="form-group">
                                            <label class="col-md-4 control-label " >PRODUCT FAMILY</label>
                                            <div class="col-md-4">   
                                                <asp:DropDownList ID="ddlProductFamliy" runat="server"  CssClass="form-control" Width="230px"  AutoPostBack="True">
                                                <asp:ListItem>SELECT FAMILY</asp:ListItem>
                                                </asp:DropDownList>
                                             </div>
                                            </div>
                                        </div>

                                        <div  class="col-md-4 " >
                                            <div class="form-group">
                                            <label class="col-md-4 control-label " >PRODUCT SUB-FAMILY</label>
                                            <div class="col-md-4">   
                                                <asp:DropDownList ID="ddlProductSubFamily" runat="server"  CssClass="form-control" Width="230px"  >
                                                <asp:ListItem>SELECT SUB-FAMILY</asp:ListItem>
                                                </asp:DropDownList>
                                             </div>
                                            </div>
                                        </div>
                                     <div  class="col-md-4 " >
                                            <div class="form-group">
                                            <label class="col-md-4 control-label " >APPLICATION</label>
                                            <div class="col-md-4">   
                                                <asp:DropDownList ID="ddlApplication" runat="server"  CssClass="form-control" Width="230px"  >
                                                <asp:ListItem>SELECT Application</asp:ListItem>
                                                </asp:DropDownList>
                                             </div>
                                            </div>
                                        </div>
                                    <div  class="col-md-4 " >
                                            <div class="form-group">
                                            <label class="col-md-4 control-label " >FILTER BY</label>
                                            <div class="col-md-4">   
                                                <asp:DropDownList ID="ddlFilter" runat="server"  CssClass="form-control" Width="230px"  >
                                                <asp:ListItem>SELECT Application</asp:ListItem>
                                                <asp:ListItem Value="Family" Text="FILTER BY FAMILY"></asp:ListItem>
                                                <asp:ListItem Value="Branch" Text="FILTER BY BRANCH"></asp:ListItem>
                                                </asp:DropDownList>
                                             </div>
                                            </div>
                                        </div>
                                        <div  class="col-md-4 " >
                                            <div class="form-group">
                                           
                                            <div class="col-md-4">   
                                                 <asp:Button ID="reports" runat="server" Text="FILTER" OnClick="reports_Click"  CssClass="btn green" Style="top: -5px !important;" />
                                                 <label id="alertmsg" style="display:none; font-weight: bold; color: #0582b7;">Now click on Filter to view results</label>
                       
                                             </div>
                                            </div>
                                        </div>
 
                                </div>          
          
            <br />

           <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
                <%--     CssClass="table table-bordered table-hover responsive" --%>
               
            </div>
           </ContentTemplate>
           </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
      <ProgressTemplate>
         <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
         </div>
      </ProgressTemplate>
   </asp:UpdateProgress>
       <script>
           $(document).ready(function () {
               BindCalender();
            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#MainContent_reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });

           });
           function BindCalender() {
               $("#MainContent_txtMonth").datepicker({
                   showOn: "both",
                   buttonImageOnly: true,
                   buttonText: "",
                   changeYear: true,
                   changeMonth: true,
                   yearRange: '-2:+0',
                   dateFormat: 'mm/yy',
                   buttonImage: "images/calander_icon.png",
               });
           }

           function triggerPostGridLodedActions() {
               BindCalender();
               $('#product_image').unbind('click').bind('click', function (e) {
                   var attr = $('#product_image').attr('src');
                   $("#MainContent_reportdrpdwns").slideToggle();
                   if (attr == "images/up_arrow.png") {
                       $("#product_image").attr("src", "images/down_arrow.png");
                   } else {
                       $("#product_image").attr("src", "images/up_arrow.png");
                   }
               });
           }
           </script>
</asp:Content>