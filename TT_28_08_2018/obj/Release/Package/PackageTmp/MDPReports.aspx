﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MDPReports.aspx.cs" Inherits="TaegutecSalesBudget.MDPReports" EnableEventValidation="true" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="js/moment.js"></script>
    <script src="js/daterangepicker.js"></script>
    <link href="css/daterangepicker.css" rel="stylesheet" />
    <%-- <link href="css/Projectsmain.css" rel="stylesheet" type="text/css" />--%>
    <%-- <link href="GridviewScroll.css" rel="stylesheet" />
    <script type="text/javascript" src="gridscroll.js"></script>
    <script type="text/javascript" src="js/app.js"></script>--%>

    <style>
        .modal-body {
            max-height: calc(200vh - 410px);
            overflow-y: auto;
        }

        .btncontrol {
            padding-left: 250px;
            padding-top: 10px;
        }

        .btn.green {
            margin-top: 10px;
        }

        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            /*border: 1px solid #ddd;*/
            border: solid 1px #ebeef5;
            padding: 5px;
            line-height: 1px !important;
        }

        .HeadergridAll {
            background: #ebeef5;
            color: #fff;
            font-weight: 400;
            text-align: center;
            border-color: #ebeef5;
            font-size: 12px;
            /*width:200px;*/
        }

        .HeadergridAll1 {
            background: #035995;
            color: #fff;
            font-weight: 400;
            text-align: center;
            border-color: #ebeef5;
            font-size: 12px;
            /*width:200px;*/
        }

        tbody tr td {
            font-size: 12px;
        }

        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px !important;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

        .control {
            padding-top: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="crumbs">
        <!-- Start : Breadcrumbs -->
        <ul id="breadcrumbs" class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a>Projects</a>
            </li>
            <li class="current">MDP Reports</li>
        </ul>
    </div>

    <!-- End : Breadcrumbs -->
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />
            </div>
            <div class="row filter_panel" id="reportdrpdwns" runat="server">
                <div>

                    <div class="col-md-6" style="padding-right: 3px;">
                        <ul id="divProjectType" runat="server" visible="true" class="btn-info rbtn_panel" style="float: left; width: 270px;">
                            <li><span style="margin-right: 4px; vertical-align: text-bottom;">Marketing Project</span>
                                <asp:RadioButton ID="rdbNormalProject" AutoPostBack="true" Checked="true" GroupName="byProjectType" runat="server" />
                                <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">Tech Support Project</span>
                                <asp:RadioButton ID="rdbtsProject" AutoPostBack="true" GroupName="byProjectType" runat="server" />
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6" style="height:30px;">
                        <div runat="server" id="cterDiv" style="float: right;" visible="false">
                            <ul class="btn-info rbtn_panel" style="float: right;">
                                <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>

                                    <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                                    <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                                    <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="col-md-2 control">
                    <label class="label">Project Status</label>
                    <asp:DropDownList ID="ddlStatus" runat="server"
                        CssClass="control_dropdown" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem>ALL</asp:ListItem>
                        <asp:ListItem>OPEN</asp:ListItem>
                        <asp:ListItem>SUCCESS</asp:ListItem>
                        <asp:ListItem>FAILURE</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="col-md-2 control">
                    <label class="label">Project Type</label>
                    <asp:DropDownList ID="ddlptype" runat="server" CssClass="control_dropdown">
                        <asp:ListItem>ALL</asp:ListItem>
                    </asp:DropDownList>

                </div>
                <div class="col-md-2 control">
                    <label class="label ">Branch</label>
                    <asp:DropDownList ID="ddlBranch" runat="server" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                        <asp:ListItem>ALL</asp:ListItem>
                    </asp:DropDownList>

                </div>
                <div class="col-md-2 control">
                    <label class="label">Customer Class </label>
                    <asp:DropDownList ID="ddlCustomerClass" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCustomerClass_SelectedIndexChanged"
                        CssClass="control_dropdown">
                        <asp:ListItem Value="ALL">ALL</asp:ListItem>
                        <asp:ListItem Value="KA">KEY FOCUSED CUSTOMER</asp:ListItem>
                        <asp:ListItem Value="KF">‘A’ CLASS CUSTOMER</asp:ListItem>
                        <asp:ListItem Value="OT"> OTHERS</asp:ListItem>
                    </asp:DropDownList>

                </div>
                <div class="col-md-2 control">
                    <label class="label ">Customer</label>
                    <asp:DropDownList ID="ddlCustomer" runat="server" CssClass="control_dropdown" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem>ALL</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="col-md-2 control">
                    <label class="label">Industry</label>
                    <asp:DropDownList ID="ddlIndustryList" runat="server"
                        CssClass="control_dropdown">
                        <asp:ListItem>ALL</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="col-md-2 control">
                    <label class="label">Sub-Industry</label>
                    <asp:DropDownList ID="ddlsubIndustryList" runat="server"
                        CssClass="control_dropdown">
                        <asp:ListItem>ALL</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="col-md-2 control">
                    <label class="label">Owner</label>
                    <asp:DropDownList ID="ddlOwnerList" runat="server"
                        CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlOwnerList_SelectedIndexChanged">
                        <asp:ListItem>ALL</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2 control">
                    <label class="label">Reviewer</label>
                    <asp:DropDownList ID="ddlReviewerList" runat="server"
                        CssClass="control_dropdown" OnSelectedIndexChanged="ddlReviewerList_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem>ALL</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="col-md-2 control">
                    <label class="label">Escalate To</label>
                    <asp:DropDownList ID="ddlEscalate" runat="server"
                        CssClass="control_dropdown" OnSelectedIndexChanged="ddlEscalate_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem>ALL</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2 control">
                    <label>Creation Date</label>
                    <div>
                        <asp:TextBox ID="creationrange" autocomplete="Off" class="filter_select_control" runat="server"></asp:TextBox>
                    </div>
                </div>

                <div class="col-md-2 control">
                    <label>Target Date</label>
                    <div>
                        <asp:TextBox ID="targetrange" autocomplete="Off" class="filter_select_control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="col-md-2 control">
                        <label class="label">Search Text</label>
                        <asp:TextBox ID="txtSearch" autocomplete="Off" class="control_dropdown" runat="server"></asp:TextBox>

                    </div>
                    <div class="col-md-10 btncontrol">
                        <asp:Button ID="proceed" runat="server" Text="FILTER" CssClass="btn green" OnClick="proceed_Click" />
                        <asp:Button runat="server" ID="save" Text="SAVE " CssClass="btn green" OnClick="save_Click" />
                        <asp:Button runat="server" ID="export" Text="EXPORT " CssClass="btn green" OnClick="export_Click" />
                        <asp:Button runat="server" ID="back" Text="CLEAR" OnClientClick="setDates();" CssClass="btn green" OnClick="back_Click" />
                        <asp:Button runat="server" ID="btnreset" Text="RESET" OnClientClick="setDates();" CssClass="btn green" OnClick="btnreset_Click" />
                    </div>
                </div>
            </div>
            <div id="Div1" class="row" runat="server" style="margin-top: 1%">
                <label runat="server" style="color: red; font-size: 12" id="lbl_values" runat="server" visible="false">&nbsp &nbsp All values are in lakhs.</label>
                <label runat="server" style="color: red; font-size: 12" id="lbl_Message" runat="server" visible="false">&nbsp &nbsp No Project to display.</label>
                <label runat="server" style="color: red; font-size: 12" id="lbl_customer" runat="server" visible="false">&nbsp &nbsp For the selected Owner/Reviewer/Escalate To, there are no projects. Please change the selection.</label>
                <label runat="server" style="color: red; font-size: 12" id="lbl_nocustomer" runat="server" visible="false">&nbsp &nbsp</label>
                <asp:Label runat="server" Style="color: red; font-size: 12" ID="lbl_exception" runat="server"> </asp:Label>
                <div id="Div2" style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;" runat="server">
                    <asp:HiddenField runat="server" ID="hdn_CustomerNum" />
                    <asp:HiddenField runat="server" ID="hdn_projectnum" />
                    <asp:HiddenField runat="server" ID="hdn_distributornum" />
                    <asp:GridView ID="projectreports" runat="server" Style="text-align: center;" AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField HeaderText="SALES ENGINEER" HeaderStyle-CssClass="HeadergridAll" SortExpression="Engineer_Name">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="Engineer_Name" runat="server" Text="SALES ENGINEER"></asp:Label>
                                    <%--  <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_salesengineer" OnClick="sort_salesengineer_Click" />--%>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="salesengineer_lbl" runat="server" Text='<%# Eval("Engineer_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="CHANNEL PARTNER" HeaderStyle-CssClass="HeadergridAll" SortExpression="distributor_lbl">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="distributor_lbl" runat="server" Text="CHANNEL PARTNER"></asp:Label>
                                    <%-- <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_distributor" OnClick="sort_distributor_Click" />--%>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="distributor_text_lbl" runat="server" Text='<%# Eval("distributor_lbl") %>'></asp:Label>
                                    <asp:HiddenField runat="server" ID="hdn_distributornumber" Value='<%# Eval("distributor_num") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="END CUSTOMER" HeaderStyle-CssClass="HeadergridAll" SortExpression="customer_lbl">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="customer_lbl" runat="server" Text="END CUSTOMER"></asp:Label>
                                    <%--  <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_customer" OnClick="sort_customer_Click" />--%>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("customer_lbl") %>'></asp:Label>
                                    <asp:HiddenField runat="server" ID="hdn_customernumber" Value='<%# Eval("customer_num") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PROJECT TITLE" HeaderStyle-CssClass="HeadergridAll" SortExpression="project_title">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="title_lbl" runat="server" Text="PROJECT TITLE"></asp:Label>
                                    <%--  <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_title" OnClick="sort_title_Click" />--%>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("project_title") %>'></asp:Label>
                                    <asp:HiddenField runat="server" ID="hdn_projectnumber" Value='<%# Eval("project_num") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PROJECT TYPE" HeaderStyle-CssClass="HeadergridAll" SortExpression="project_type">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="type_lbl" runat="server" Text="PROJECT TYPE"></asp:Label>
                                    <%-- <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_type" OnClick="sort_type_Click" />--%>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("project_type") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="EXISTING PRODUCT" HeaderStyle-CssClass="HeadergridAll" SortExpression="existing_product_lbl">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="existing_product_lbl" runat="server" Text="EXISTING BRAND"></asp:Label>
                                    <%--  <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_existing_product" OnClick="sort_existing_product_Click" />--%>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="existingproduct_lbl" runat="server" Text='<%# Eval("existing_product_lbl") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="TAGUTEC PRODUCT" HeaderStyle-CssClass="HeadergridAll" SortExpression="component_lbl">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="component_lbl" runat="server" Text="COMPONENT"></asp:Label>
                                    <%--  <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_tagutecproduct" OnClick="sort_tagutecproduct_Click" />--%>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="tagutecproduct_lbl" runat="server" Text='<%# Eval("component_lbl") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <%--<asp:TemplateField HeaderText="CREATION DATE" SortExpression="creation_date_lbl">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="creation_date_lbl" runat="server" Text="CREATION DATE"></asp:Label>
                                    <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_creation" OnClick="sort_creation_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="creation_date_lbl_1" runat="server" Text='<%# Eval("creation_date_lbl") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="INDUSTRY" HeaderStyle-CssClass="HeadergridAll">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="industry_lbl" runat="server" Text="INDUSTRY"></asp:Label>
                                    <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_industry" OnClick="sort_industry_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("industry_lbl") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>--%>

                            <%--<asp:TemplateField HeaderText="SUB INDUSTRY" HeaderStyle-CssClass="HeadergridAll" >
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                <HeaderTemplate>
                                    <asp:Label ID="sub_industry_lbl"  runat="server" Text="SUB INDUSTRY"></asp:Label>
                                    <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_sub_industry" OnClick="sort_sub_industry_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                <asp:Label ID="Label4"  runat="server" Text='<%# Eval("sub_industry_lbl") %>'></asp:Label>
                
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                            <%--<asp:TemplateField HeaderText="OVERALL POTENTIAL" HeaderStyle-CssClass="HeadergridAll" SortExpression="overall_pot_lbl">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="overall_pot_lbl" runat="server" Text="OVERALL POTENTIAL"></asp:Label>
                                    <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_overall" OnClick="sort_overall_Click" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("overall_pot_lbl") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                            <asp:TemplateField HeaderText="PROJECT POTENTIAL" HeaderStyle-CssClass="HeadergridAll" SortExpression="potential_lakhs_lbl">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="potential_lakhs_lbl" runat="server" Text="PROJECT POTENTIAL"></asp:Label>
                                    <%-- <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_potential" OnClick="sort_potential_Click" />--%>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("potential_lakhs_lbl") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <%--<asp:TemplateField HeaderText="EXISTING PRODUCT">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                <ItemTemplate>
                                   <asp:Label ID="existing_product_lbl"  runat="server" Text='<%# Eval("existing_product_lbl") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                            <%-- <asp:TemplateField HeaderText="COMPONENT"  >
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                <ItemTemplate>
                                    <asp:Label ID="component_lbl"  runat="server" Text='<%# Eval("component_lbl") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                            <asp:TemplateField HeaderText="BUSINESS EXPECTED" HeaderStyle-CssClass="HeadergridAll" SortExpression="Business_Expected">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="Business_Expected" runat="server" Text="BUSINESS EXPECTED"></asp:Label>
                                    <%--  <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_businessexpected" OnClick="sort_businessexpected_Click" />--%>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="businessexpected_lbl" runat="server" Text='<%# Eval("Business_Expected") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="MONTHLY BUSINESS EXPECTED" HeaderStyle-CssClass="HeadergridAll" SortExpression="monthly_exp_val">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="monthly_exp_val" runat="server" Text="MONTHLY BUSINESS EXPECTED"></asp:Label>
                                    <%-- <asp:ImageButton ToolTip="sorting" Width="15px" runat="server" ImageUrl="~/images/sort_neutral.png" ID="sort_monthly" OnClick="sort_monthly_Click" />--%>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("monthly_exp_val") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PROJECT OWNER" HeaderStyle-CssClass="HeadergridAll">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("project_owner") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TAEGUTEC CUTTER" HeaderStyle-CssClass="HeadergridAll">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("TaegutecCutter") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TAEGUTEC INSERT" HeaderStyle-CssClass="HeadergridAll">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("TaegutecInsert") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="COMP.CUTTER" HeaderStyle-CssClass="HeadergridAll">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("CompetitorCutter") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="COMP.INSERT" HeaderStyle-CssClass="HeadergridAll">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("CompetitorInsert") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>




                            <asp:TemplateField HeaderText="STATUS" HeaderStyle-CssClass="HeadergridAll">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="status_lbl" CssClass="lbl_color" runat="server" Text='<%# Eval("status_lbl") %>' OnCommand="status_lbl_Command"></asp:LinkButton>
                                    <%--<asp:Image ID="Image1"  runat="server" Height = "15" ImageUrl="~/images/report_status_red.png" Visible = '<%# Eval("red").ToString() == "yes"  %>' />
                                    <asp:Image ID="Image2"  runat="server" Height = "15" ImageUrl="~/images/report_status_yellow.png" Visible = '<%# Eval("yellow").ToString() == "yes"  %>' />
                                    <asp:Image ID="Image3"  runat="server" Height = "15" ImageUrl="~/images/report_status_green.png" Visible = '<%# Eval("green").ToString() == "yes"  %>' />--%>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <%--                            <asp:TemplateField HeaderText="STAGE 1" HeaderStyle-CssClass="HeadergridAll" SortExpression="monthly_exp_val">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="stage1" runat="server" Text="STAGE 1"></asp:Label>
                                    <asp:Label ID="stage1start" runat="server" Text="START DATE"></asp:Label>
                                    <asp:Label ID="stage1end" runat="server" Text="END DATE"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="stage1_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="STAGE 2" HeaderStyle-CssClass="HeadergridAll" SortExpression="monthly_exp_val">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="stage2" runat="server" Text="STAGE 2"></asp:Label>
                                    <asp:Label ID="stage2start" runat="server" Text="START DATE"></asp:Label>
                                    <asp:Label ID="stage2end" runat="server" Text="END DATE"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="stage2_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="STAGE 3" HeaderStyle-CssClass="HeadergridAll" SortExpression="monthly_exp_val">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="stage3" runat="server" Text="STAGE 3"></asp:Label>
                                    <asp:Label ID="stage3start" runat="server" Text="START DATE"></asp:Label>
                                    <asp:Label ID="stage3end" runat="server" Text="END DATE"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="stage3_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="STAGE 4" HeaderStyle-CssClass="HeadergridAll" SortExpression="monthly_exp_val">
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:Label ID="stage4" runat="server" Text="STAGE 4"></asp:Label>
                                    <asp:Label ID="stage4start" runat="server" Text="START DATE"></asp:Label>
                                    <asp:Label ID="stage4end" runat="server" Text="END DATE"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="stage4_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                            <asp:TemplateField HeaderText="NO OF VISITS" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="visit_lbl" CssClass="lbl_color" runat="server" Text='<%# Eval("noofvisits") %>' OnCommand="LoadVisitDetails" CommandArgument='<%# Eval("project_num") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="REMARKS" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:TextBox ID="txt_remarks" TextMode="multiline" Columns="20" Rows="2" CssClass="lock_txt" runat="server" Text='<%# Eval("txt_remarks") %>' Visible='<%# Eval("txt_remarks").ToString()!="Grand Total" %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <%--<asp:TemplateField HeaderText="OUTCOME" HeaderStyle-CssClass="HeadergridAll"  HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                                <ItemTemplate>               
                                    <asp:Label ID="result_lbl"  runat="server"  Text= '<%# Eval("result")%>' Font-Bold="true" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                            <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <%--<asp:Button runat="server" ID="btn_download" Text="View Docs"  CssClass="btn red" style="top:0px;left:2px;background:#0582b7;color:#fff" OnClick="btn_download_click"   ></asp:Button>--%>
                                    <asp:ImageButton runat="server" ID="btn_download" ToolTip="View Docs" ImageUrl="~/images/download_icon.png" CssClass="download" OnClick="btn_download_click"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <%--<asp:Button runat="server" ID="btn_cancel" Text="DELETE"  CssClass="btn red" style="top:0px;left:2px;background:red;color:#fff" OnClick="btn_cancel_click" OnClientClick="return VerifyDelete()" Visible = '<%# Eval("cancel").ToString() == "yes" ? true : false %>'  ></asp:Button>--%>
                                    <asp:ImageButton runat="server" ID="btn_cancel" ToolTip="DELETE" ImageUrl="~/images/delete_icon2.png" CssClass="delete" OnClick="btn_cancel_click" OnClientClick="return VerifyDelete()" Visible='<%# Eval("cancel").ToString() == "yes" ? true : false %>'></asp:ImageButton>
                                    <asp:Label ID="cancel_lbl" runat="server" Text="Cancelled" Visible='<%# Eval("cancel").ToString() == "no" ? true : false %>' Font-Bold="true" CssClass="cancel"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div id="ProjectDetails" class="modal-body" style="width: 100%; display: none; text-align: left !important; height: auto !important">
                <div class="col-md-8 control">
                    <label class="label ">Project Title:</label>
                    <asp:Label ID="lblprojectitle" runat="server"></asp:Label>
                </div>
                <div class="col-md-4 control">
                    <label class="label ">Number:</label>
                    <asp:Label ID="lblname" runat="server"></asp:Label>
                </div>
                <br />
                <asp:GridView ID="Grdvisitdetails" runat="server" Style="text-align: center;" AutoGenerateColumns="False" EmptyDataText="No visit Found !">
                    <Columns>
                        <asp:TemplateField HeaderText="VISITOR" HeaderStyle-CssClass="HeadergridAll1">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label ID="lbl_emp_name" runat="server" Text='<%# Eval("emp_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Visited Date" HeaderStyle-CssClass="HeadergridAll1">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label ID="lbl_planned_date" runat="server" Text='<%# Eval("planned_date") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Visit Report" HeaderStyle-CssClass="HeadergridAll1">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label ID="lbl_vst_rep" runat="server" Text='<%# Eval("vst_rep") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Check-in Time" HeaderStyle-CssClass="HeadergridAll1">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label ID="lbl_check_in" runat="server" Text='<%# Eval("check_in") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Check-out Time" HeaderStyle-CssClass="HeadergridAll1">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label ID="lbl_check_out" runat="server" Text='<%# Eval("check_out") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

            <asp:HiddenField ID="hdnprojectpotential" runat="server" />
            <asp:HiddenField ID="hdnbusinessexpected" runat="server" />
            <asp:HiddenField ID="hdnmonthlybusinessexpected" runat="server" />
            <asp:HiddenField ID="hdnsearch" runat="server" />
            <%--     <asp:GridView ID="grandTotal" Width="100%" runat="server" Style="text-align: center;" AutoGenerateColumns="false" ShowHeader="false" CssClass="grand_total">
                        <Columns>

                            <asp:TemplateField>

                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                <ItemTemplate>
                                    <asp:Label ID="grand_salesengineer_lbl" runat="server" Text='GRAND TOTAL'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField>

                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                <ItemTemplate>
                                    <asp:Label ID="grand_distributor_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField>

                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px"/>
                                <ItemTemplate>
                                    <asp:Label ID="grand_customer_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                <ItemTemplate>
                                    <asp:Label ID="grand_title_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                <ItemTemplate>
                                    <asp:Label ID="grand_existingproduct_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px"/>
                                <ItemTemplate>
                                    <asp:Label ID="grand_tagutecproduct_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                           
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="150px" />
                                <ItemTemplate>
                                    <asp:Label ID="grand_potential_lakhs_lbl" runat="server" Text='<%# Eval("pot_lakh") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                           
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="150px" />
                                <ItemTemplate>
                                    <asp:Label ID="grand_business_exp_val" runat="server" Text='<%# Eval("total_business_expected") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="150px"/>
                                <ItemTemplate>
                                    <asp:Label ID="grand_monthly_exp_val" runat="server" Text='<%# Eval("total_monthly_expected") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>

                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px"/>
                                <ItemTemplate>
                                    <asp:Label ID="grand_stage1_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField>

                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                <ItemTemplate>
                                    <asp:Label ID="grand_stage2_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"  Width="150px"/>
                                <ItemTemplate>
                                    <asp:Label ID="grand_stage3_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="grand_stage4_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="grand_status_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="grand_remarks" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                           
                            <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="grand_download_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <asp:Label ID="grand_cancel_lbl" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>--%>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlEscalate" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlReviewerList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlCustomer" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlStatus" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlBranch" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlOwnerList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="proceed" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="save" EventName="Click" />
            <%-- <asp:AsyncPostBackTrigger ControlID="visit_lbl" EventName="Command" />--%>
            <asp:PostBackTrigger ControlID="export" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <style type="text/css">
        td {
            height: 30px !important;
            padding: 5px;
            /*background: #fff;*/
            /*text-align: right;*/
        }

        th {
            background: #ebeef5;
            color: #111;
            font-weight: 600;
            font-size: 13px;
            border-color: #ebeef5;
            height: 40px;
            padding: 10px;
            text-align: center;
        }

        /*.grand_total {
            font-size: 16px;
            font-weight: bold;
            background-color: #52646C !important;
            color: #fff;
        }*/

        /*.MainTotal {
       background: #FFA500;
       padding: 5px;
       color: #fff;

        }*/

        .cancel {
            color: red;
        }

        .clearCancel {
            display: none;
        }

        a {
            color: white;
        }

        .lbl_color {
            color: blue !important;
        }

        /*.header_image {
            background-image: url("Images/arrowvt.png");
            background-repeat: no-repeat;
            background-color: #006780;
            background-position: right;
        }*/

        .failure {
            color: red;
        }

        .success {
            color: green;
        }

        /*#MainContent_projectreports td:nth-child(7), #MainContent_projectreports td:nth-child(8), #MainContent_projectreports td:nth-child(9) {
            text-align: right;
        }*/

        /*#MainContent_grandTotal td:nth-child(7), #MainContent_grandTotal td:nth-child(8), #MainContent_grandTotal td:nth-child(9) {
            text-align: right;
        }*/

        #MainContent_projectreports td:nth-child(13) {
            width: 25px;
        }

        .ui-datepicker-trigger {
            float: right;
            margin-top: -31px;
            padding: 8px;
            border-left: 1px solid #ccc;
            cursor: pointer;
        }

        .download {
            /*max-height:20%;*/
            width: 35px !important;
        }

        .delete {
            /*max-height:20%;*/
            width: 38px !important;
        }
        /*tfoot
        {
            border: solid 1px #ebeef5;
            padding: 3px 10px;
        }*/

        tbody {
            border-color: #fff;
        }
    </style>

    <script type="text/javascript" class="init">

        function setDates() {
            debugger;
            $("#<%= creationrange.ClientID %>").daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                },
                startDate: '01/01/2010',
                endDate: moment()
            });
            // setcreateDates();
            var startdate = "01/01/2010";
            var today = new Date();
            var dt = new Date(new Date().getFullYear() + 5, new Date().getMonth() + new Date().getDate());
            console.log(today);
            console.log(dt);
            today = today.getDate() + "/" + (today.getMonth() + 1) + "/" + today.getFullYear();
            sessionStorage.setItem('createselectedStart', startdate);
            sessionStorage.setItem('createselectedEnd', today);
            dt = "31/12/" + dt.getFullYear();
            console.log(dt);
            $("#<%= targetrange.ClientID %>").daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                },
                startDate: '01/01/2010',
                endDate: moment().add(5, 'year')
            });
            // settargetDates();
            debugger;
            var target_startdate = "01/01/2015";
            sessionStorage.setItem('targetselectedStart', target_startdate);
            sessionStorage.setItem('targetselectedEnd', dt);
            //new Date(new Date().getFullYear() + 5, new Date().getMonth(), new Date().getDate()));


        }
        function setcreateDates() {
            debugger;
            var createdate = $("#<%= creationrange.ClientID %>").val();
            var startdate = "01/01/2010 - ";
            var today = new Date();
            today = today.getDate() + "/" + (today.getMonth() + 1) + "/" + today.getFullYear();
            if (createdate = startdate.concat(today)) {
                $("#<%= creationrange.ClientID %>").val('All Date');
            }
        }
        function settargetDates() {
            var targetdate = $("#<%= targetrange.ClientID %>").val();
            var startdate = "01/01/2010 - ";
            var today = add_years(new Date(), 5);
            today = today.getDate() + "/" + (today.getMonth() + 1) + "/" + today.getFullYear();

            if (targetdate = startdate.concat(today)) {
                $("#<%= targetrange.ClientID %>").val('All Date');
            }
        }
        function add_years(dt, n) {
            return new Date(dt.setFullYear(dt.getFullYear() + n));
        }
        $(document).ready(function () {
            debugger;
            // bindGridView();
            //gridviewScrollTrigger();
            //BindCalendar();
            // gridviewScrollTrigger();
            allowOnlyNumber(function allowOnlyNumber(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return true;
                return false;
                $('#product_image').unbind('click').bind('click', function (e) {
                    var attr = $('#product_image').attr('src');
                    $("#MainContent_reportdrpdwns").slideToggle();
                    if (attr == "images/up_arrow.png") {
                        $("#product_image").attr("src", "images/down_arrow.png");
                    } else {
                        $("#product_image").attr("src", "images/up_arrow.png");
                    }
                });
            });
            debugger;
            //$(function () {
            var SelectedStart = sessionStorage.getItem("createselectedStart");
            var SelectedEnd = sessionStorage.getItem("createselectedEnd");
            var start = (SelectedStart == null ? "01/01/2010" : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            var targetSelectedStart = sessionStorage.getItem("targetselectedStart");
            var targetSelectedEnd = sessionStorage.getItem("targetselectedEnd");
            var targetstart = (targetSelectedStart == null ? "01/01/2010" : targetSelectedStart);
            var targetend = (targetSelectedEnd == null ? moment().add(5, 'year') : targetSelectedEnd);

            $("#<%= creationrange.ClientID %>").daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['01/01/2010', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                debugger;
                var start = start.format('DD/MM/YYYY');
                var end = end.format('DD/MM/YYYY');
                sessionStorage.setItem('createselectedStart', start);
                sessionStorage.setItem('createselectedEnd', end);
                if (start == '01/01/2010' && end == moment().format('DD/MM/YYYY')) {
                    //$("#<%= creationrange.ClientID %>").val('All Date');
                }
            });


            $("#<%= targetrange.ClientID %>").daterangepicker({

                locale: {
                    format: 'DD/MM/YYYY'
                },
                startDate: targetstart,
                endDate: targetend,
                ranges: {
                    'All Date': ['01/01/2010', moment().add(5, 'year')],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                var start = start.format('DD/MM/YYYY');
                var end = end.format('DD/MM/YYYY');
                sessionStorage.setItem('targetselectedStart', start);
                sessionStorage.setItem('targetselectedEnd', end);
                if (start == '01/01/2010' && end == moment().format('DD/MM/YYYY')) {
                    //$("#<%= targetrange.ClientID %>").val('All Date');
                }
            });
        });
        //$(function () {
        debugger;
        var SelectedStart = sessionStorage.getItem("createselectedStart");
        var SelectedEnd = sessionStorage.getItem("createselectedEnd");
        var start = (SelectedStart == null ? "01/01/2010" : SelectedStart);
        var end = (SelectedEnd == null ? moment() : SelectedEnd);
        var targetSelectedStart = sessionStorage.getItem("targetselectedStart");
        var targetSelectedEnd = sessionStorage.getItem("targetselectedEnd");
        var targetstart = (targetSelectedStart == null ? "01/01/2010" : targetSelectedStart);
        var targetend = (targetSelectedEnd == null ? moment().add(5, 'year') : targetSelectedEnd);

        $("#<%= creationrange.ClientID %>").daterangepicker({
            locale: {
                format: 'DD/MM/YYYY'
            },
            startDate: start,
            endDate: end,
            ranges: {
                'All Date': ['01/01/2010', moment()],
                'Last Year': [moment().subtract(1, 'year'), moment()],
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
        }, function (start, end, label) {
            debugger;
            var start = start.format('DD/MM/YYYY');
            var end = end.format('DD/MM/YYYY');
            sessionStorage.setItem('createselectedStart', start);
            sessionStorage.setItem('createselectedEnd', end);
            if (start == '01/01/2010' && end == moment().format('DD/MM/YYYY')) {
                //$("#<%= creationrange.ClientID %>").val('All Date');
            }
        });

        //});
        //$(function () {
        $("#<%= targetrange.ClientID %>").daterangepicker({
            locale: {
                format: 'DD/MM/YYYY'
            },
            startDate: targetstart,
            endDate: targetend,
            ranges: {
                'All Date': ['01/01/2010', moment().add(5, 'year')],
                'Last Year': [moment().subtract(1, 'year'), moment()],
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
        }, function (start, end, label) {
            var start = start.format('DD/MM/YYYY');
            var end = end.format('DD/MM/YYYY');
            sessionStorage.setItem('targetselectedStart', start);
            sessionStorage.setItem('targetselectedEnd', end);
            if (start == '01/01/2010' && end == moment().format('DD/MM/YYYY')) {
                //$("#<%= targetrange.ClientID %>").val('All Date');
            }
        });
        //});
        function bindGridView() {
            debugger;
            var SelectedStart = sessionStorage.getItem("createselectedStart");
            var SelectedEnd = sessionStorage.getItem("createselectedEnd");
            var start = (SelectedStart == null ? "01/01/2010" : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            var targetSelectedStart = sessionStorage.getItem("targetselectedStart");
            var targetSelectedEnd = sessionStorage.getItem("targetselectedEnd");
            var targetstart = (targetSelectedStart == null ? "01/01/2010" : targetSelectedStart);
            var targetend = (targetSelectedEnd == null ? moment().add(5, 'year') : targetSelectedEnd);

            var projectpotential = $('#MainContent_hdnprojectpotential').val();
            var businessexpected = $('#MainContent_hdnbusinessexpected').val();
            var monthlybusinessexpected = $('#MainContent_hdnmonthlybusinessexpected').val();
            console.log("value");
            console.log(projectpotential);
            var head_content = $('#MainContent_projectreports tr:first').html();
            $('#MainContent_projectreports').prepend('<thead></thead>')
            $('#MainContent_projectreports thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_projectreports tbody tr:first').hide();
            $('#MainContent_projectreports').append('<tfoot><tr> <th colspan="7" style="text-align:right"> GRAND TOTAL:</th><th style="text-align: right">' + projectpotential + '</th><th style="text-align: right">' + businessexpected + '</th><th style="text-align: right">' + monthlybusinessexpected + '</th></tr></tfoot>');
            $('#MainContent_projectreports').DataTable(
                {
                    "info": false
                });

            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#MainContent_reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
            $('#MainContent_projectreports').on('search.dt', function () {
                debugger;
                var value = $('.dataTables_filter input').val();
                console.log(value); // <-- the value
                //$('#MainContent_hdnsearch').val = value;
                document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "MDPReports.aspx/setGrandtotal",
                    data: "{'search':'" + value + "'}",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.d);
                        var parsed = data.d;
                        console.log(parsed);
                        console.log(parsed[1]);
                        //$('#MainContent_targetvssales').prepend('<tfoot></tfoot>');
                        $('#MainContent_projectreports tfoot').hide();
                        var projectpotential = parsed[0];
                        var businessexpected = parsed[1];
                        var monthlybusinessexpected = parsed[2];
                        //bindGridView();
                        $('#MainContent_projectreports').append('<tfoot><tr> <th colspan="7" style="text-align:right">GRAND TOTAL:</th><th style="text-align: right">' + projectpotential + '</th><th style="text-align: right">' + businessexpected + '</th><th style="text-align: right">' + monthlybusinessexpected + '</th></tr></tfoot>');
                        console.log("2");
                    },
                    error: function (result) {
                        console.log("No Match");
                    }
                });
            });
            //$(function () {
            $("#<%= creationrange.ClientID %>").daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['01/01/2010', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                var start = start.format('DD/MM/YYYY');
                var end = end.format('DD/MM/YYYY');
                sessionStorage.setItem('createselectedStart', start);
                sessionStorage.setItem('createselectedEnd', end);
                if (start == '01/01/2010' && end == moment().format('DD/MM/YYYY')) {
                    //$("#<%= creationrange.ClientID %>").val('All Date');
                }
            });
            //});
            //$(function () {
            $("#<%= targetrange.ClientID %>").daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                },
                startDate: targetstart,
                endDate: targetend,
                ranges: {
                    'All Date': ['01/01/2010', moment().add(5, 'year')],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                var start = start.format('DD/MM/YYYY');
                var end = end.format('DD/MM/YYYY');
                sessionStorage.setItem('targetselectedStart', start);
                sessionStorage.setItem('targetselectedEnd', end);
                if (start == '01/01/2010' && end == moment().format('DD/MM/YYYY')) {
                    //$("#<%= targetrange.ClientID %>").val('All Date');
                }
            });
            //});
        }
        $('#MainContent_projectreports').on('search.dt', function () {
            debugger;
            var value = $('.dataTables_filter input').val();
            console.log(value); // <-- the value
            //$('#MainContent_hdnsearch').val = value;
            document.getElementById('<%=hdnsearch.ClientID%>').value = value;

        });

        $(function () {
            $("[id*=status_lbl]").bind("click", function () {
                var td = $("td", $(this).closest("tr"));
                var CustomerNum = $("[id*=hdn_customernumber]", td).val();
                $("#MainContent_hdn_CustomerNum").val(CustomerNum);
            });

            $("[id*=btn_cancel]").bind("click", function () {
                var td = $("td", $(this).closest("tr"));
                var CustomerNum = $("[id*=hdn_customernumber]", td).val();
                $("#MainContent_hdn_CustomerNum").val(CustomerNum);
            });

            $("[id*=btn_download]").bind("click", function () {
                var td = $("td", $(this).closest("tr"));
                var CustomerNum = $("[id*=hdn_customernumber]", td).val();
                $("#MainContent_hdn_CustomerNum").val(CustomerNum);
            });

        });

        $(function () {
            $("[id*=status_lbl]").bind("click", function () {
                var td = $("td", $(this).closest("tr"));
                var ProjectNum = $("[id*=hdn_projectnumber]", td).val();
                $("#MainContent_hdn_projectnum").val(ProjectNum);
            });

            $("[id*=btn_cancel]").bind("click", function () {
                var td = $("td", $(this).closest("tr"));
                var ProjectNum = $("[id*=hdn_projectnumber]", td).val();
                $("#MainContent_hdn_projectnum").val(ProjectNum);
            });
            $("[id*=btn_download]").bind("click", function () {
                var td = $("td", $(this).closest("tr"));
                var ProjectNum = $("[id*=hdn_projectnumber]", td).val();
                $("#MainContent_hdn_projectnum").val(ProjectNum);
            });

        });

        $(function () {
            $("[id*=status_lbl]").bind("click", function () {
                var td = $("td", $(this).closest("tr"));
                var DistributorNum = $("[id*=hdn_distributornumber]", td).val();
                $("#MainContent_hdn_distributornum").val(DistributorNum);
            });

        });



        $('#MainContent_projectreports tr').each(function () {

            $(this).find("td").each(function () {

                if ($(this).find('span').text() == "FAILURE") {
                    $(this).find('span').addClass("failure");

                }
            });

        });

        $('#MainContent_projectreports tr').each(function () {

            $(this).find("td").each(function () {

                if ($(this).find('span').text() == "SUCCESS") {
                    $(this).find('span').addClass("success");

                }
            });

        });

        $('#product_image').unbind('click').bind('click', function (e) {
            debugger;
            var attr = $('#product_image').attr('src');
            $("#MainContent_reportdrpdwns").slideToggle();
            if (attr == "images/up_arrow.png") {
                $("#product_image").attr("src", "images/down_arrow.png");
            } else {
                $("#product_image").attr("src", "images/up_arrow.png");
            }
        });

        //$(window).resize(function () {

        //function gridviewScrollTrigger() {
        //    if ($("#MainContent_Grdvisitdetails").height() >= 450) {
        //        gridView1 = $('#MainContent_Grdvisitdetails').gridviewScroll({
        //            width: $(window).width() - 50,
        //            height: 350,
        //            railcolor: "#F0F0F0",
        //            barcolor: "#606060",
        //            barhovercolor: "#606060",
        //            bgcolor: "#F0F0F0",
        //            freezesize: 0,
        //            arrowsize: 30,
        //            varrowtopimg: "Images/arrowvt.png",
        //            varrowbottomimg: "Images/arrowvb.png",
        //            harrowleftimg: "Images/arrowhl.png",
        //            harrowrightimg: "Images/arrowhr.png",
        //            headerrowcount: 2,
        //            railsize: 16,
        //            barsize: 14,
        //            verticalbar: "auto",
        //            horizontalbar: "auto",
        //            overflow: 'none',
        //            wheelstep: 1,
        //        });
        //    }
        //}

        //});

        $(document).on('change', '#MainContent_Created_To', function () {
            var Target_from = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_Created_from').val());
            var Target_To = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_Created_To').val());
            if (Target_from != "" && Target_from != undefined && Target_from != null)
                if (new Date(Target_To) < new Date(Target_from)) {
                    alert("To VISIT Date can not be less than the VISIT From Date");
                    var vaue = "";
                    $("#MainContent_Created_To").val(vaue);
                    return false;
                }
            return true;
        });

        $(document).on('change', '#MainContent_Created_from', function () {
            debugger;
            var Target_from = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_Created_from').val());
            var Target_To = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_Created_To').val());
            if (Target_To != "" && Target_To != undefined && Target_To != null)
                if (new Date(Target_To) < new Date(Target_from)) {
                    alert("VISIT From Date can not be greater than the VISIT To Date");
                    var vaue = "";
                    $("#MainContent_Created_from").val(vaue);
                    return false;
                }
            return true;
        });

        $(document).on('change', '#MainContent_Target_To', function () {
            var Target_from = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_Target_From').val());
            var Target_To = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_Target_To').val());
            if (Target_from != "" && Target_from != undefined && Target_from != null)
                if (new Date(Target_To) < new Date(Target_from)) {
                    alert("To VISIT Date can not be less than the VISIT From Date");
                    var vaue = "";
                    $("#MainContent_Target_To").val(vaue);
                    return false;
                }
            return true;
        });

        $(document).on('change', '#MainContent_Target_From', function () {
            var Target_from = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_Target_From').val());
            var Target_To = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_Target_To').val());
            if (Target_To != "" && Target_To != undefined && Target_To != null)
                if (new Date(Target_To) < new Date(Target_from)) {
                    alert("VISIT From Date can not be greater than the VISIT To Date");
                    var vaue = "";
                    $("#MainContent_Target_From").val(vaue);
                    return false;
                }
            return true;
        });
        //});

        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return true;
            return false;
        }


        function BindCalendar() {
            debugger;
            $("#MainContent_Target_From").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-60:c+60",
                // minDate: new Date(),
                dateFormat: 'dd/mm/yy',
                buttonImage: "images/calander_icon.png",
            });
            $("#MainContent_Target_To").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-60:c+60",
                // minDate: new Date(),
                dateFormat: 'dd/mm/yy',
                buttonImage: "images/calander_icon.png",
            });

            $("#MainContent_Created_from").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-60:c+60",
                // minDate: new Date(),
                dateFormat: 'dd/mm/yy',
                buttonImage: "images/calander_icon.png",
            });

            $("#MainContent_Created_To").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-60:c+60",
                //  minDate: new Date(),
                dateFormat: 'dd/mm/yy',
                buttonImage: "images/calander_icon.png",
            });
        }

        //function gridviewScrollTrigger() {
        //    debugger;
        //        gridView1 = $('#MainContent_Grdvisitdetails').gridviewScroll({
        //            width: $(window).width() - 30,
        //            height: 450,
        //            railcolor: "#F0F0F0",
        //            barcolor: "#606060",
        //            barhovercolor: "#606060",
        //            bgcolor: "#F0F0F0",
        //            //freezesize: 5,
        //            arrowsize: 30,
        //            varrowtopimg: "Images/arrowvt.png",
        //            varrowbottomimg: "Images/arrowvb.png",
        //            harrowleftimg: "Images/arrowhl.png",
        //            harrowrightimg: "Images/arrowhr.png",
        //            headerrowcount: 2,
        //            railsize: 16,
        //            barsize: 14,
        //            verticalbar: "auto",
        //            horizontalbar: "auto",
        //            wheelstep: 1,
        //        });

        //    }


        //function gridviewScrollTrigger1() {
        //    gridView1 = $('#MainContent_projectreports').gridviewScroll({
        //        width: $(window).width() - 30,
        //        height: 450,
        //        railcolor: "#F0F0F0",
        //        barcolor: "#606060",
        //        barhovercolor: "#606060",
        //        bgcolor: "#F0F0F0",
        //        freezesize: 0,
        //        arrowsize: 30,
        //        varrowtopimg: "Images/arrowvt.png",
        //        varrowbottomimg: "Images/arrowvb.png",
        //        harrowleftimg: "Images/arrowhl.png",
        //        harrowrightimg: "Images/arrowhr.png",
        //        headerrowcount: 2,
        //        railsize: 16,
        //        barsize: 14,
        //        verticalbar: "auto",
        //        horizontalbar: "auto",
        //        wheelstep: 1,
        //    });
        //}

        function triggerPostGridLodedActions1() {
            bindGridView();
            // gridviewScrollTrigger();
            BindCalendar();
            // gridviewScrollTrigger();

            $(function () {
                $("[id*=status_lbl]").bind("click", function () {
                    var td = $("td", $(this).closest("tr"));
                    var CustomerNum = $("[id*=hdn_customernumber]", td).val();
                    $("#MainContent_hdn_CustomerNum").val(CustomerNum);
                });

                $("[id*=btn_cancel]").bind("click", function () {
                    var td = $("td", $(this).closest("tr"));
                    var CustomerNum = $("[id*=hdn_customernumber]", td).val();
                    $("#MainContent_hdn_CustomerNum").val(CustomerNum);
                });

                $("[id*=btn_download]").bind("click", function () {
                    var td = $("td", $(this).closest("tr"));
                    var CustomerNum = $("[id*=hdn_customernumber]", td).val();
                    $("#MainContent_hdn_CustomerNum").val(CustomerNum);
                });

            });

            $(function () {
                $("[id*=status_lbl]").bind("click", function () {
                    var td = $("td", $(this).closest("tr"));
                    var ProjectNum = $("[id*=hdn_projectnumber]", td).val();
                    $("#MainContent_hdn_projectnum").val(ProjectNum);
                });

                $("[id*=btn_cancel]").bind("click", function () {
                    var td = $("td", $(this).closest("tr"));
                    var ProjectNum = $("[id*=hdn_projectnumber]", td).val();
                    $("#MainContent_hdn_projectnum").val(ProjectNum);
                });
                $("[id*=btn_download]").bind("click", function () {
                    var td = $("td", $(this).closest("tr"));
                    var ProjectNum = $("[id*=hdn_projectnumber]", td).val();
                    $("#MainContent_hdn_projectnum").val(ProjectNum);
                });

            });

            $(function () {
                $("[id*=status_lbl]").bind("click", function () {

                    var td = $("td", $(this).closest("tr"));
                    var DistributorNum = $("[id*=hdn_distributornumber]", td).val();
                    $("#MainContent_hdn_distributornum").val(DistributorNum);
                });

            });

            $('#MainContent_projectreports tr').each(function () {

                $(this).find("td").each(function () {

                    if ($(this).find('span').text() == "FAILURE") {
                        $(this).find('span').addClass("failure");
                    }
                });
            });

            $('#MainContent_projectreports tr').each(function () {

                $(this).find("td").each(function () {

                    if ($(this).find('span').text() == "SUCCESS") {
                        $(this).find('span').addClass("success");

                    }
                });
            });


            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#MainContent_reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
        }

        function triggerPostGridLodedActions() {
            bindGridView();
            //gridviewScrollTrigger();
            BindCalendar();
            $(function () {
                $("[id*=status_lbl]").bind("click", function () {
                    var td = $("td", $(this).closest("tr"));
                    var CustomerNum = $("[id*=hdn_customernumber]", td).val();
                    $("#MainContent_hdn_CustomerNum").val(CustomerNum);
                });

                $("[id*=btn_cancel]").bind("click", function () {
                    var td = $("td", $(this).closest("tr"));
                    var CustomerNum = $("[id*=hdn_customernumber]", td).val();
                    $("#MainContent_hdn_CustomerNum").val(CustomerNum);
                });

                $("[id*=btn_download]").bind("click", function () {
                    var td = $("td", $(this).closest("tr"));
                    var CustomerNum = $("[id*=hdn_customernumber]", td).val();
                    $("#MainContent_hdn_CustomerNum").val(CustomerNum);
                });

            });

            $(function () {
                $("[id*=status_lbl]").bind("click", function () {
                    var td = $("td", $(this).closest("tr"));
                    var ProjectNum = $("[id*=hdn_projectnumber]", td).val();
                    $("#MainContent_hdn_projectnum").val(ProjectNum);
                });

                $("[id*=btn_cancel]").bind("click", function () {
                    var td = $("td", $(this).closest("tr"));
                    var ProjectNum = $("[id*=hdn_projectnumber]", td).val();
                    $("#MainContent_hdn_projectnum").val(ProjectNum);
                });
                $("[id*=btn_download]").bind("click", function () {
                    var td = $("td", $(this).closest("tr"));
                    var ProjectNum = $("[id*=hdn_projectnumber]", td).val();
                    $("#MainContent_hdn_projectnum").val(ProjectNum);
                });

            });

            $(function () {
                $("[id*=status_lbl]").bind("click", function () {

                    var td = $("td", $(this).closest("tr"));
                    var DistributorNum = $("[id*=hdn_distributornumber]", td).val();
                    $("#MainContent_hdn_distributornum").val(DistributorNum);
                });
            });


            $('#MainContent_projectreports tr').each(function () {

                $(this).find("td:first").each(function () {

                    if ($(this).find('span').text() == "GRAND TOTAL") {
                        $(this).closest("tr").find("td").addClass("grand_total");

                    }
                });
            });

            $('#MainContent_projectreports tr').each(function () {

                $(this).find("td").each(function () {

                    if ($(this).find('span').text() == "FAILURE") {
                        $(this).find('span').addClass("failure");

                    }
                });
            });

            $('#MainContent_projectreports tr').each(function () {

                $(this).find("td").each(function () {

                    if ($(this).find('span').text() == "SUCCESS") {
                        $(this).find('span').addClass("success");

                    }
                });
            });

            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#MainContent_reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
        }

        function findminofscreenheight(a, b) {
            a = a - $("#lblUserName").offset().top;
            return a < b ? a : b;
        }

        function VerifyDelete() {

            if (confirm("Are you sure, you want to delete this project ?")) {
                return true;
            }
            else {
                return false;
            }

        }


        function ShowPopUp() {
            debugger;
            triggerPostGridLodedActions();
            //var table = $('#projectreports').DataTable();
            //var info = table.page.info();
            //console.log(info.page);
            dclg = $("#ProjectDetails").dialog(
                           {
                               resizable: false,
                               draggable: true,
                               modal: true,
                               title: "Visit Details",
                               width: "600",
                               height: "400",

                               open: function () {
                                   debugger;
                                   $(this).dialog('open');
                               },
                               close: function () {
                                   $(this).dialog('close');
                                   $(".ui-widget-overlay").css('background', 'none');
                               },

                           });

            //dclg.parent().appendTo(jQuery("form:first"));
            //e.preventDefault();

        }




    </script>

</asp:Content>


