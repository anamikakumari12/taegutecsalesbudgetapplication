﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HOReports.aspx.cs" EnableEventValidation="false" Inherits="TaegutecSalesBudget.HOReports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  
   <!-- End : Breadcrumbs -->
   <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360"></asp:ScriptManager>
  
   <asp:UpdatePanel ID="UpdatePanel1" runat="server"  >
      <ContentTemplate>
            <div class="crumbs">
      <!-- Start : Breadcrumbs -->
      <ul id="breadcrumbs" class="breadcrumb">
         <li>
            <i class="fa fa-home"></i>
            <a>Reports</a>
         </li>
         <li class="current">Budget</li>  
        
             <div>
                  <ul  style="float:right;list-style:none;   margin-top: -4px; width: 247px; margin-right: -5px; "  class="alert alert-danger in">
                      <li> <span style="margin-right:4px; vertical-align:text-bottom; ">Val In '000</span>                      
                        <asp:RadioButton ID="ValInThsnd" OnCheckedChanged="byValueIn_CheckedChanged" AutoPostBack="true"  Checked="true"   GroupName="byValueInradiobtn" runat="server" />
                         <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">Val In Lakhs</span>
                        <asp:RadioButton ID="ValInLakhs" OnCheckedChanged="byValueIn_CheckedChanged" AutoPostBack="true"   GroupName="byValueInradiobtn" runat="server" />
                     </li>
                  </ul>
               </div>
      </ul>                               
   </div>

         <div id="collapsedropdwns" class="row" >
               <img id="Imgcollapsedropdwns" src="images/up_arrow.png" align="left" style="margin-left: 46%;cursor:pointer"/>                                                               
          </div>      
     
         <div class="row filter_panel" id="reportdrpdwns" runat="server">
              <div id="divCter" runat="server" visible="false">
                  <ul class="btn-info rbtn_panel" >
                      <li> <span style="margin-right:4px;vertical-align:text-bottom;  ">TAEGUTEC</span>
                       
                        <asp:RadioButton ID="rdBtnTaegutec"  AutoPostBack="true"  Checked="true"  OnCheckedChanged="rdBtnTaegutec_CheckedChanged"  GroupName="byCmpnyCodeInradiobtn" runat="server" />
                         <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">DURACARB</span>
                        <asp:RadioButton ID="rdBtnDuraCab"  AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged"   GroupName="byCmpnyCodeInradiobtn" runat="server" />
                     </li>
                  </ul>
               </div>
         
            <div class="col-md-2 control"> 
                  <label class="label">BRANCH</label>
                     <asp:DropDownList ID="ddlBranchList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged"
                        CssClass="control_dropdown">
                     </asp:DropDownList>    
            </div>

            <div  class="col-md-2 control">
                  <label class="label">SALES ENGINEER </label>
                     <asp:DropDownList ID="ddlSalesEngineerList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSalesEngineerList_SelectedIndexChanged"
                        CssClass="control_dropdown" >
                        <asp:ListItem>--SELECT SALES ENGINEER--</asp:ListItem>
                     </asp:DropDownList>
            </div>

            <div class="col-md-2 control">
                  <label class="label">CUSTOMER TYPE </label>
                     <asp:DropDownList ID="ddlcustomertype" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlcustomertype_SelectedIndexChanged"
                        CssClass="control_dropdown" >
                        <asp:ListItem Text="ALL" Value="ALL" />
                        <asp:ListItem Text="CUSTOMER" Value="C" />
                        <asp:ListItem Text="CHANNEL PARTNER" Value="D" />
                     </asp:DropDownList>
            </div>

            <div class="col-md-2 control"> 
                  <label class="label">CUSTOMER NAME </label>
                     <asp:DropDownList ID="ddlCustomerList" runat="server" 
                        CssClass="control_dropdown">
                        <asp:ListItem>--SELECT CUSTOMER --</asp:ListItem>
                     </asp:DropDownList>
            </div>

            <div  class="col-md-2 control ">
                  <label class="label " >CUSTOMER NUMBER</label>
                     <asp:DropDownList ID="ddlCustomerNumber" runat="server"  CssClass="control_dropdown" >
                        <asp:ListItem>SELECT CUSTOMER NUMBER</asp:ListItem>
                     </asp:DropDownList>          
            </div>

           <div  class="col-md-4 " style="margin-top:2px;">
               <div class="form-group">               
                     <asp:Button ID="reports" runat="server" Text="FILTER" OnClick="reports_Click"  CssClass="btn green" />
                       <asp:Button ID="exportbtn" runat="server" Text="EXPORT" OnClick="export_Click"  CssClass="btn green"  />
                      <label id="alertmsg" style="display:none; font-weight: bold; color: #0582b7;">Now click on Filter  to view results</label>                  
               </div>
            </div>
              
         </div>
         <div class="row" id="reportsgrid" runat="server">
            <div class="col-md-12">
               <div class="portlet-body">
                  <div id="accordion" class="panel-group">
                     <div class="panel panel-default">
                        <div id="collapsebtn"  class=" panel-heading1 " >
                           <h4 class="panel-title">
                              <img id="product_image" src="images/button_plus.gif" align="left"/>  &nbsp;&nbsp;&nbsp;&nbsp; Product Group
                           </h4>
                        </div>
                        <div id="prdgroup">
                           <div style="height: 0px;" >
                              <div  style="background:#fbfbfb;">
                                 <div>
                                    <%--class="col-md-5">--%>
                                    <asp:GridView ID="goldproducts" style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" Visible="false">
                                       <columns>
                                          <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                             <ItemTemplate>
                                                <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                             <ItemTemplate>  
                                                 <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                             </ItemTemplate>  
                                             </asp:TemplateField> --%> 
                                          <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Label7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                       </columns>
                                    </asp:GridView>
                                 </div>
                              </div>
                              <div >
                                 <div>
                                    <asp:GridView ID="fiveyrsproducts" style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" Visible="false">
                                       <columns>
                                          <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                             <ItemTemplate>
                                                <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                             <ItemTemplate>  
                                                 <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                             </ItemTemplate>  
                                             </asp:TemplateField> --%> 
                                          <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelf7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                       </columns>
                                    </asp:GridView>
                                 </div>
                              </div>
                              <div style="background:#fbfbfb;">
                                 <div>
                                    <asp:GridView ID="spcproducts" style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" Visible="false" >
                                       <columns>
                                          <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                             <ItemTemplate>
                                                <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                             <ItemTemplate>  
                                                 <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                             </ItemTemplate>  
                                             </asp:TemplateField> --%> 
                                          <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labels7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                       </columns>
                                    </asp:GridView>
                                 </div>
                              </div>
                              <div >
                                 <div>
                                    <asp:GridView ID="topproducts" style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" Visible="false" >
                                       <columns>
                                          <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                             <ItemTemplate>
                                                <asp:Label  ID="topproductslbl" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                             <ItemTemplate>  
                                                 <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                             </ItemTemplate>  
                                             </asp:TemplateField> --%> 
                                          <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelt7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                       </columns>
                                    </asp:GridView>
                                 </div>
                              </div>
                              <div  style="background:#fbfbfb;">
                                 <div >
                                    <asp:GridView style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" ID="bbproducts" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" Visible="false" >
                                       <columns>
                                          <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                             <ItemTemplate>
                                                <asp:Label  ID="lblbbproducts" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                             <ItemTemplate>  
                                                 <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                             </ItemTemplate>  
                                             </asp:TemplateField> --%> 
                                          <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="Labelbb7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                       </columns>
                                    </asp:GridView>
                                 </div>
                              </div>

                             <%--  //added columns for 10 yrs--%>
                                <div  style="background:#fbfbfb;">
                                 <div >
                                    <asp:GridView style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" ID="tenyrsproducts" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" Visible="false">
                                       <columns>
                                          <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                             <ItemTemplate>
                                                <asp:Label  ID="lblten1" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                             <ItemTemplate>  
                                                 <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                             </ItemTemplate>  
                                             </asp:TemplateField> --%> 
                                          <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="lblten2"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="lblten3"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="lblten4" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="lblten5" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="lblten6" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="lblten7" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                             <ItemTemplate>
                                                <asp:Label ID="lblten8" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                             </ItemTemplate>
                                          </asp:TemplateField>
                                       </columns>
                                    </asp:GridView>
                                 </div>
                              </div>
                               <%--for 10 yrs--%>

                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default" id="tab2">
                        <div id="collapseline" class="panel-heading2">
                           <h4 class="panel-title">
                              <img id="linegrid_image" src="images/button_plus.gif" align="left"/> &nbsp;&nbsp;&nbsp;&nbsp; SALES BY LINE
                           </h4>
                        </div>
                        <div >
                           <div id="linegrid"  style="background:#fbfbfb;">
                             
                                 <asp:GridView ID="salesbylinegrid"  style="margin-top: 25px;margin-left: 15px;width: 90%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" >
                                    <columns>
                                       <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                          <ItemTemplate>
                                             <asp:Label  ID="lbl_linegrid" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                          <ItemTemplate>  
                                              <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                          </ItemTemplate>  
                                          </asp:TemplateField> --%> 
                                       <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                       <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                          <ItemTemplate>
                                             <asp:Label ID="Labell7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                    </columns>
                                 </asp:GridView>
                             
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default" id="tab3">
                        <div  id="collapsefamily" class="panel-heading3">
                           <h4 class="panel-title">
                              <img id="familygrid_image" src="images/button_plus.gif" align="left"/>  &nbsp;&nbsp;&nbsp;&nbsp; SALES BY FAMILY
                           </h4>
                        </div>
                        <div  >
                           <div id="familygrid"  style="background:#fbfbfb;">
                              <asp:GridView ID="salesbyfamilygrid" style="margin-top: 25px;margin-left: 15px;width: 90%;"  runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="goldproducts_RowDataBound" >
                                 <columns>
                                    <asp:TemplateField HeaderText="" HeaderStyle-CssClass="HeadergridAll" HeaderStyle-Width="350px">
                                       <ItemTemplate>
                                          <asp:Label  ID="lblfamilygrid" runat="server" Text='<%# Eval("FixedRow") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="2012" HeaderStyle-CssClass="HeadergridAll" visible="false">  
                                       <ItemTemplate>  
                                           <asp:Label  ID="gldprdct2012" runat="server" Text='<%# Eval("sales_value_year_2") %>'></asp:Label>  
                                       </ItemTemplate>  
                                       </asp:TemplateField> --%> 
                                    <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf1"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2014B" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf2"  runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf3" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf4" runat="server" Text='<%# Eval("p_sales_value_year_1") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf5" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf6" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015P" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelsf7" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                 </columns>
                              </asp:GridView>
                              <div class="col-md-2">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default" id="tab4">
                        <div  id="collapse_app" class="panel-heading4">

                           <h4 class="panel-title">
                              <img id="salesapp_img" src="images/button_plus.gif" align="left"/>  &nbsp;&nbsp;&nbsp;&nbsp; SALES BY APPLICATION                                                              
                           </h4>
                        </div>
                        <div>                            
                           <div id="salesappgrid"  style="background:#fbfbfb;">                            
                              <div id="div1" runat="server">
                                  <ul style="width:150px;" class="btn-info rbtn_panel" >
                                      <li> <span style="margin-right:4px;vertical-align:text-bottom;  ">VALUE</span>                       
                                        <asp:RadioButton ID="rbtn_Value"  Checked="true"  GroupName="byValueorQty" runat="server" onclick="value_or_qty_Change();" />
                                         <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">QTY</span>
                                        <asp:RadioButton ID="rbtn_Quantity"  GroupName="byValueorQty" runat="server" onclick="value_or_qty_Change();"/>
                                     </li>
                                  </ul>
                               </div>

                               <div id="div2" runat="server">
                                  <ul style="float:right;list-style:none;   margin-top: -22px; width: 265px; margin-right: -5px; "  class="alert alert-danger fade in" >
                                      <li> 
                                          <span style="margin-right:-1px; margin-left:-35px;vertical-align:text-bottom;">Val In Units</span>
                                        <asp:RadioButton ID="Rdbtnunits" OnCheckedChanged="byValueorunitsorlacks_CheckedChanged" AutoPostBack="true" GroupName="byunitsorvalueorlacks" runat="server"/>
                                          <span style="margin-right:-1px; margin-left:5px; vertical-align:text-bottom;  ">Val In '000</span>                       
                                        <asp:RadioButton ID="Rdbtnvalue" OnCheckedChanged="byValueorunitsorlacks_CheckedChanged" AutoPostBack="true" Checked="true"  GroupName="byunitsorvalueorlacks" runat="server"/>
                                         <span style="margin-right:0px; margin-left:6px;vertical-align:text-bottom;">Val In Lakhs</span>
                                        <asp:RadioButton ID="Rdbtnlacks" OnCheckedChanged="byValueorunitsorlacks_CheckedChanged" AutoPostBack="true" GroupName="byunitsorvalueorlacks" runat="server"/>                                         
                                     </li>
                                  </ul>
                               </div>

                              <asp:GridView ID="salesbyapp" style="margin-top: 25px;margin-left: 15px;width: 90%;"  runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="salesbyapp_RowDataBound" >
                                 <columns>
                                      <asp:TemplateField HeaderText="APPLICATION CODE" HeaderStyle-CssClass="HeadergridAll"  HeaderStyle-Width="95px" >
                                       <ItemTemplate>
                                          <asp:Label ID="Labela1"  runat="server" Text='<%# Eval("app_Code") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="APPLICATION DESCRIPTION" HeaderStyle-CssClass="HeadergridAll"   HeaderStyle-Width="240px" >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela0" runat="server" Text='<%# Eval("app_Desc") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>                                  
                                    <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll" >
                                       <ItemTemplate>
                                          <asp:Label ID="Labela2"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2014P" HeaderStyle-CssClass="HeadergridAll" >
                                       <ItemTemplate>
                                          <asp:Label ID="Labela3" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela4" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela5" runat="server" Text='<%# Eval("change") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela6" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"  >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela7" runat="server" Text='<%# Eval("acvmnt") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"  >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela8" runat="server" Text='<%# Eval("askrate") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                 </columns>
                              </asp:GridView>

                              <asp:GridView ID="salesbyapp_qty" style="margin-top: 25px;margin-left: 15px;width: 90%;"  runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="salesbyapp_qty_RowDataBound" >
                                 <columns>
                                      <asp:TemplateField HeaderText="APPLICATION CODE" HeaderStyle-CssClass="HeadergridAll"  HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center">
                                       <ItemTemplate>
                                          <asp:Label ID="Labela1"  runat="server" Text='<%# Eval("app_Code") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="APPLICATION DESCRIPTION" HeaderStyle-CssClass="HeadergridAll"   HeaderStyle-Width="240px" >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela0" runat="server" Text='<%# Eval("app_Desc") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>                                  
                                    <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll" >
                                       <ItemTemplate>
                                          <asp:Label ID="Labela2"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2014P" HeaderStyle-CssClass="HeadergridAll" >
                                       <ItemTemplate>
                                          <asp:Label ID="Labela3" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela4" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela5" runat="server" Text='<%# Eval("change") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela6" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"  >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela7" runat="server" Text='<%# Eval("acvmnt") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"  >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labela8" runat="server" Text='<%# Eval("askrate") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                 </columns>
                              </asp:GridView>
                                                                 
                              <div class="col-md-2">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default" id="tab5">
                        <div  id="cust_app" class="panel-heading5">
                           <h4 class="panel-title">
                              <img id="slsbycust_image" src="images/button_plus.gif" align="left"/>  &nbsp;&nbsp;&nbsp;&nbsp; SALES BY CUSTOMER
                           </h4>
                        </div>
                        <div  >
                           <div id="slsbycustomergrid"  style="background:#fbfbfb;">
                              <asp:GridView ID="salesbycustomer" style="margin-top: 25px;margin-left: 15px;width: 90%;"  runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="salesbyapp_cust_RowDataBound" >
                                 <columns>
                                     <asp:TemplateField HeaderText="CUSTOMER NUMBER" HeaderStyle-CssClass="HeadergridAll" visible="true" HeaderStyle-Width="95px">
                                       <ItemTemplate>
                                          <asp:Label  ID="Labelc0" runat="server" Text='<%# Eval("cust_number") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CUSTOMER NAME" HeaderStyle-CssClass="HeadergridAll" visible="true" HeaderStyle-Width="240px">
                                       <ItemTemplate>
                                          <asp:Label  ID="Labelcust_name" runat="server" Text='<%# Eval("cust_name") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2013" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Label2"  runat="server" Text='<%# Eval("sales_value_year_1") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2014P" HeaderStyle-CssClass="HeadergridAll">
                                       <ItemTemplate>
                                          <asp:Label ID="Labelc3" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll" >
                                       <ItemTemplate>
                                          <asp:Label  ID="lbl_Budget" runat="server" Text='<%# Eval("estimate_value_next_year") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"   >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labelc4" runat="server" Text='<%# Eval("change") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"  >
                                       <ItemTemplate>
                                          <asp:Label  ID="lbl_YtdSale" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll"  >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labelc5" runat="server" Text='<%# Eval("acvmnt") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2015B" HeaderStyle-CssClass="HeadergridAll" >
                                       <ItemTemplate>
                                          <asp:Label  ID="Labelc6" runat="server" Text='<%# Eval("askrate") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                 </columns>
                              </asp:GridView>
                           </div>
                        </div>
                     </div>
                     <table id="header_fixed"   ></table>
                  </div>
               </div>
            </div>
            <!-- End : Inner Page container -->
            <a style="display: none;" class="scrollup" href="javascript:void(0);">Scroll</a>
         </div>
         <!-- End : Inner Page container -->
         <a href="javascript:void(0);" class="scrollup" style="display: none;">Scroll</a>
         </div>  <!-- End : Inner Page Content -->
         </div>
         </div>  <!-- End : container -->
      </ContentTemplate>
      <Triggers>
         <asp:AsyncPostBackTrigger ControlID="ddlBranchList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="ddlSalesEngineerList" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="ddlcustomertype" EventName="SelectedIndexChanged" />
         <asp:AsyncPostBackTrigger ControlID="reports" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="ValInThsnd" EventName="CheckedChanged" />
        <asp:AsyncPostBackTrigger ControlID="ValInLakhs" EventName="CheckedChanged" />
       <%-- <asp:AsyncPostBackTrigger ControlID="Rdbtnunits" EventName="CheckedChanged" />
        <asp:AsyncPostBackTrigger ControlID="Rdbtnvalue" EventName="CheckedChanged" />
        <asp:AsyncPostBackTrigger ControlID="Rdbtnlacks" EventName="CheckedChanged" />--%>
        <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
        <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
       
          <asp:PostBackTrigger ControlID="exportbtn" />
      </Triggers>
   </asp:UpdatePanel>
   <asp:UpdateProgress ID="updateProgress" runat="server">
      <ProgressTemplate>
         <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
         </div>
      </ProgressTemplate>
   </asp:UpdateProgress>
   <script type="text/javascript" src="js/jquery-ui.min.js"></script>
   <script type="text/javascript" src="js/ReportsBudget.js"></script>   

   <style type="text/css">
       @media (min-width: 768px) and (max-width: 900px) {
    th,td {
        font-size:9px;
    }
    }

        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px!important;
            position: relative;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }
         .control {
            padding-top: 2px;
        }
          .btn.green {
            color: #fff;
            margin-left: auto;
            margin-right: auto;
            overflow: auto;
            cursor: pointer;
            top: -9px;
            position: relative;
            width: 100px;
            font-weight: 600;
            margin-top: 12px;
        }

      #header_fixed {
      display:none;
      position:fixed;
      top:50px;
      background-color:#006780;
      margin-left:15px;
      color:#fff;
      }
      th,td {
      border: 1px solid #ddd;
      }
      th {
      padding:13px;
      text-align:center;
      }
      td {
      padding:4px;
      text-align:right;
      }
      .greendark, .color_4, .color_5 {
      text-align:left !important;
      font-size:12px !important;
      }
      #familygrid table tr:last-child {
      background-color:#999;
      }
      #linegrid table tr:last-child {
      background-color:#999;
      }
      #familygrid table tr:last-child td:first-child {
      text-align:center;
      }
      #linegrid table tr:last-child td:first-child {
      text-align:center;
      }
      #slsbycustomergrid table  td:first-child {
      text-align:left;
      }
      .color_total { 
      font-size:14px;
      background-color:#999;
      }
      .sum_total {
      font-size:14px;
      background-color:#999;
      }
       #slsbycustomergrid,#linegrid,#familygrid,#prdgroup,#salesappgrid {
           display:none;
       }
      .panel-group .panel {
      border-radius: 4px;
      margin-bottom: 0;
      overflow: hidden;
      margin-top: 20px;
      margin-bottom: 20px;
      }
      .panel-heading1 { background:#333; color:#29AAe1 !important; padding: 15px; color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading2 { background:#999; color:#333 !important; padding: 15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading3 { background:#333; color:#8ac340 !important;  padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading4 { background:#999; color:#91298E !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading5 { background:#333; color:#F05A26   !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading6 { background:#999; color:#002238  !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading7 { background:#333; color:#fff  !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
      .panel-heading8 { background:#333; color:#F05A26   !important; padding:  15px;  color:#fff; font-size: 16px; font-weight: bolder; text-transform:uppercase;text-align:center;}
   </style>
   <script type="text/javascript" src="js/select2.min.js"></script>
   <!-- Mirrored from flatsquare.themesway.com/default/tables-responsive.html by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 30 Jan 2014 09:45:44 GMT -->
</asp:Content>