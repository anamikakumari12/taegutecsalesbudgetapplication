﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TaegutecSalesBudget {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class BMSResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal BMSResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TaegutecSalesBudget.BMSResource", typeof(BMSResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Branch.
        /// </summary>
        internal static string Branch {
            get {
                return ResourceManager.GetString("Branch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to BranchReview.
        /// </summary>
        internal static string BranchReview {
            get {
                return ResourceManager.GetString("BranchReview", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company.
        /// </summary>
        internal static string Company {
            get {
                return ResourceManager.GetString("Company", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Customer.
        /// </summary>
        internal static string Customer {
            get {
                return ResourceManager.GetString("Customer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CustomerReview.
        /// </summary>
        internal static string CustomerReview {
            get {
                return ResourceManager.GetString("CustomerReview", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CustomerType.
        /// </summary>
        internal static string CustomerType {
            get {
                return ResourceManager.GetString("CustomerType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Engineer.
        /// </summary>
        internal static string Engineer {
            get {
                return ResourceManager.GetString("Engineer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to FocusProductReport.
        /// </summary>
        internal static string FocusProductReport {
            get {
                return ResourceManager.GetString("FocusProductReport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to KitItemPriceList.
        /// </summary>
        internal static string KitItemPriceList {
            get {
                return ResourceManager.GetString("KitItemPriceList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MappedUsers.
        /// </summary>
        internal static string MappedUsers {
            get {
                return ResourceManager.GetString("MappedUsers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MonthlyReview.
        /// </summary>
        internal static string MonthlyReview {
            get {
                return ResourceManager.GetString("MonthlyReview", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PriceChangerList.
        /// </summary>
        internal static string PriceChangerList {
            get {
                return ResourceManager.GetString("PriceChangerList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PriceList.
        /// </summary>
        internal static string PriceList {
            get {
                return ResourceManager.GetString("PriceList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to QuarterlyReview.
        /// </summary>
        internal static string QuarterlyReview {
            get {
                return ResourceManager.GetString("QuarterlyReview", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ReprtDashboard.
        /// </summary>
        internal static string ReportDashboard {
            get {
                return ResourceManager.GetString("ReportDashboard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to abcd.
        /// </summary>
        internal static string ReportDashboardId {
            get {
                return ResourceManager.GetString("ReportDashboardId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2.
        /// </summary>
        internal static string ReportDashboardURLID {
            get {
                return ResourceManager.GetString("ReportDashboardURLID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RevisedPrice.
        /// </summary>
        internal static string RevisedPrice {
            get {
                return ResourceManager.GetString("RevisedPrice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SEReview.
        /// </summary>
        internal static string SEReview {
            get {
                return ResourceManager.GetString("SEReview", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SFEEDTecReport.
        /// </summary>
        internal static string SFEEDReviewReport {
            get {
                return ResourceManager.GetString("SFEEDReviewReport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to VarianceReview.
        /// </summary>
        internal static string VarianceReview {
            get {
                return ResourceManager.GetString("VarianceReview", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to WeeklyReview.
        /// </summary>
        internal static string WeeklyReview {
            get {
                return ResourceManager.GetString("WeeklyReview", resourceCulture);
            }
        }
    }
}
