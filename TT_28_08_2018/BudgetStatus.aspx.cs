﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class BudgetStatus : System.Web.UI.Page
    {   
        #region GlobalDeclareation
        CommonFunctions comFun = new CommonFunctions();
        Budget objBudget = new Budget();
        Reports objReports = new Reports();
        Review objRSum = new Review();
        public static string cter;
        #endregion


        #region Events
        /// <summary>
        /// Author: Neha
        /// Date: Nov 28,2018
        /// Desc: Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                cter = null;

                if (Session["RoleId"].ToString() == "HO" || Session["RoleId"].ToString() == "TM")
                {
                    if (Session["cter"] == null)
                    {
                        Session["cter"] = "TTA";
                        cter = "TTA";
                    }

                    if (Convert.ToString(Session["cter"]) == "DUR")
                    {
                        rdBtnDuraCab.Checked = true;
                        rdBtnTaegutec.Checked = false;
                        cter = "DUR";
                    }
                    else
                    {
                        rdBtnTaegutec.Checked = true;
                        rdBtnDuraCab.Checked = false;
                        cter = "TTA";
                    }
                    cterDiv.Visible = true;
                    LoadBranches();
                    ddlBranchList_SelectedIndexChanged(null, null);
                    LoadCustomers(strUserId, roleId);

                }

                else if (Session["RoleId"].ToString() == "BM")
                {
                    cterDiv.Visible = false;
                    string username = Session["UserName"].ToString();
                    string branchcode = Session["BranchCode"].ToString();
                    string branchDec = Session["BranchDesc"].ToString();
                    Session["SelectedBranchList"] = "'" + branchcode + "'";
                    //bind branch
                    ddlBranchList.Items.Insert(0, branchcode);
                    divBranch.Visible = false;
                    // sales engineers loading
                    DataTable dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);
                    if (dtSalesEngDetails != null)
                    {
                        ddlSalesEngineerName.DataSource = dtSalesEngDetails;
                        ddlSalesEngineerName.DataTextField = "EngineerName";
                        ddlSalesEngineerName.DataValueField = "EngineerId";
                        ddlSalesEngineerName.DataBind();
                        ddlSalesEngineerName.Items.Insert(0, "ALL");
                    }
                    else
                    {
                        ddlSalesEngineerName.DataSource = null;
                        ddlSalesEngineerName.DataBind();
                    }

                    SalesEngList_SelectedIndexChanged(null, null);
                    LoadCustomers(strUserId, roleId);
                    
                }
                else if (Session["RoleId"].ToString() == "SE")
                {
                    cterDiv.Visible = false;
                    string username = Session["UserName"].ToString();
                    string branchcode = Session["BranchCode"].ToString();
                    string branchDec = Session["BranchDesc"].ToString();
                    // string customer = Convert.ToString(Session["EngineerId"]);
                    Session["SelectedBranchList"] = "'" + branchcode + "'";
                    Session["SelectedSalesEngineers"] = "'" + strUserId + "'";
                    //bind branch
                    ddlBranchList.Items.Insert(0, branchcode);
                    divBranch.Visible = false;
                    //bind Sales engineer
                    ddlSalesEngineerName.Items.Insert(0, strUserId);
                    divSE.Visible = false;

                    objRSum.flag = "CustomerType";
                    objRSum.salesengineer_id = strUserId;
                    //DataTable dtData1 = objRSum.getFilterAreaValue(objRSum);
                    //if (dtData1.Rows.Count != 0)
                    //{
                    //    ddlCustomerName.DataSource = dtData1;
                    //    ddlCustomerName.DataTextField = "customer_short_name";
                    //    ddlCustomerName.DataValueField = "customer_number";
                    //    ddlCustomerName.DataBind();
                    //    ddlCustomerName.Items.Insert(0, "ALL");
                    //}
                    //else
                    //{
                    //    ddlCustomerName.DataSource = dtData1;
                    //    ddlCustomerName.DataTextField = "customer_short_name";
                    //    ddlCustomerName.DataValueField = "customer_number";
                    //    ddlCustomerName.DataBind();
                    //    ddlCustomerName.Items.Insert(0, "NO CUSTOMER");
                    //}

                    divSE.Visible = false;
                    LoadCustomers(strUserId, roleId);

                }
            }
        }
        /// <summary>
        /// Author:Neha
        /// Date: Nov 28,2018
        /// Desc: Select the company type and store in session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {

            if (rdBtnTaegutec.Checked)
            {
                Session["cter"] = "TTA";
                cter = "TTA";
            }
            if (rdBtnDuraCab.Checked)
            {
                Session["cter"] = "DUR";
                cter = "DUR";
            }
            budgetstatusvalues.DataSource = null;
            budgetstatusvalues.DataBind();
            LoadBranches();
            ddlBranchList_SelectedIndexChanged(null, null);
            SalesEngList_SelectedIndexChanged(null, null);
        }
        /// <summary>
        /// Author:Neha
        /// Date: Nov 28,2018
        /// Desc:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SalesEngList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            int count = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            foreach (ListItem val in ddlSalesEngineerName.Items)
            {
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }

            name_code = "'" + name_code;
            string SalesengList = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
            string SalesengnameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

            if (count == ddlSalesEngineerName.Items.Count)
            {
                Session["SelectedSalesEngineers"] = "ALL";
            }

            else
            {
                Session["SelectedSalesEngineers"] = SalesengList;
            }

            if (SalesengList == "'ALL'")
            {
                SalesengList = "";
            }
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            // string branchcode = Convert.ToString(Session["SelectedBranchList"]);
            string branchcode = ddlBranchList.SelectedItem.Value;
            if (branchcode == "")
            {
                branchcode = "ALL";
            }
            objRSum.BranchCode = (roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL") ? userId : branchcode;
            objRSum.salesengineer_id = SalesengList.ToString() == "ALL" || SalesengList.ToString() == "" ? null : SalesengList;
            // objRSum.customer_type = ddlcustomertype.SelectedItem.Value;
            objRSum.roleId = roleId;
            objRSum.flag = "CustomerType";
            objRSum.cter = cter;
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);
            dtData.Columns.Add("c_name", typeof(string));


            //for (int i = 0; i < dtData.Rows.Count; i++)
            //{
            //    dtData.Rows[i]["c_name"] = string.Concat(Convert.ToString(dtData.Rows[i]["customer_short_name"]), "(", Convert.ToString(dtData.Rows[i]["customer_number"]), ")");
                
            //}
            if (dtData.Rows.Count != 0)
            {
                ddlCustomerName.DataSource = dtData;
                ddlCustomerName.DataTextField = "customer_short_name";
                ddlCustomerName.DataValueField = "customer_number";
                ddlCustomerName.DataBind();
                ddlCustomerName.Items.Insert(0, "ALL");
            }
            else
            {
                ddlCustomerName.DataSource = dtData;
                ddlCustomerName.DataTextField = "customer_short_name";
                ddlCustomerName.DataValueField = "customer_number";
                ddlCustomerName.DataBind();
                ddlCustomerName.Items.Insert(0, "NO CUSTOMER");
            }

            if (dtData.Rows.Count != 0)
            {
                ddlCustomerNumber.DataSource = dtData;
                ddlCustomerNumber.DataTextField = "customer_number";
                ddlCustomerNumber.DataValueField = "customer_number";
                ddlCustomerNumber.DataBind();
                ddlCustomerNumber.Items.Insert(0, "ALL");
            }
            else
            {
                ddlCustomerNumber.DataSource = dtData;
                ddlCustomerNumber.DataTextField = "customer_number";
                ddlCustomerNumber.DataValueField = "customer_number";
                ddlCustomerNumber.DataBind();
                ddlCustomerNumber.Items.Insert(0, "NO CUSTOMER");
            }
            //DataTable dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, "SE");
            //if (dtCutomerDetails.Rows.Count != 0)
            //{
            //    ddlCustomerNumber.DataSource = dtCutomerDetails;
            //    ddlCustomerNumber.DataTextField = "customer_number";
            //    ddlCustomerNumber.DataValueField = "customer_number";
            //    ddlCustomerNumber.DataBind();
            //    ddlCustomerName.Items.Insert(0, "ALL");

            //}
            //else
            //{
            //    ddlCustomerNumber.DataSource = dtCutomerDetails;
            //    ddlCustomerNumber.DataTextField = "customer_number";
            //    ddlCustomerNumber.DataValueField = "customer_number";
            //    ddlCustomerNumber.DataBind();
            //    ddlCustomerName.Items.Insert(0, "NO CUSTOMER");
            //}
            
            budgetstatusvalues.DataSource = null;
            budgetstatusvalues.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        protected void ddlBranchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
           
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            objRSum.BranchCode = roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL" ? userId : ddlBranchList.SelectedItem.Value;
            objRSum.roleId = roleId;
            objRSum.flag = "SalesEngineer";
            objRSum.cter = cter;
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);

            if (dtData.Rows.Count != 0)
            {
                ddlSalesEngineerName.DataSource = dtData;
                ddlSalesEngineerName.DataTextField = "EngineerName";
                ddlSalesEngineerName.DataValueField = "EngineerId";
                ddlSalesEngineerName.DataBind();
                ddlSalesEngineerName.Items.Insert(0, "ALL");
            }
            else
            {
                ddlSalesEngineerName.DataSource = dtData;
                ddlSalesEngineerName.DataTextField = "EngineerName";
                ddlSalesEngineerName.DataValueField = "EngineerId";
                ddlSalesEngineerName.DataBind();
                ddlSalesEngineerName.Items.Insert(0, "NO SALES ENGINEER");
            }

            objRSum.flag = "CustomerType";
            DataTable dtData1 = objRSum.getFilterAreaValue(objRSum);
            dtData1.Columns.Add("c_name", typeof(string));


            //for (int i = 0; i < dtData1.Rows.Count; i++)
            //{
            //    dtData1.Rows[i]["c_name"] = string.Concat(Convert.ToString(dtData1.Rows[i]["customer_short_name"]), "(", Convert.ToString(dtData1.Rows[i]["customer_number"]), ")");
            //}
            if (dtData1.Rows.Count != 0)
            {
                ddlCustomerName.DataSource = dtData1;
                ddlCustomerName.DataTextField = "customer_short_name";
                ddlCustomerName.DataValueField = "customer_number";
                ddlCustomerName.DataBind();
                ddlCustomerName.Items.Insert(0, "ALL");
            }
            else
            {
                ddlCustomerName.DataSource = dtData1;
                ddlCustomerName.DataTextField = "customer_short_name";
                ddlCustomerName.DataValueField = "customer_number";
                ddlCustomerName.DataBind();
                ddlCustomerName.Items.Insert(0, "NO CUSTOMER");
            }

            if (dtData1.Rows.Count != 0)
            {
                ddlCustomerNumber.DataSource = dtData1;
                ddlCustomerNumber.DataTextField = "customer_number";
                ddlCustomerNumber.DataValueField = "customer_number";
                ddlCustomerNumber.DataBind();
                ddlCustomerNumber.Items.Insert(0, "ALL");
            }
            else
            {
                ddlCustomerNumber.DataSource = dtData1;
                ddlCustomerNumber.DataTextField = "customer_number";
                ddlCustomerNumber.DataValueField = "customer_number";
                ddlCustomerNumber.DataBind();
                ddlCustomerNumber.Items.Insert(0, "NO CUSTOMER");
            }
            budgetstatusvalues.DataSource = null;
            budgetstatusvalues.DataBind();
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        protected void status_click(object sender, EventArgs e)
        {
            try
            {   
                DataTable dt = new DataTable();
                if (Convert.ToString(Session["RoleId"]) == "SE" || Convert.ToString(Session["RoleId"]) == "BM")
                {
                    cter = Convert.ToString(Session["Territory"]);
                }
                objRSum.cter = Convert.ToString(cter);
                objRSum.BranchCode = Convert.ToString(ddlBranchList.SelectedValue);
                objRSum.salesengineer_id = Convert.ToString(ddlSalesEngineerName.SelectedValue);
                objRSum.customer_number = Convert.ToString(ddlCustomerNumber.SelectedValue);
                objRSum.userid = Convert.ToString(Session["UserId"]);
                if (objRSum.BranchCode == "ALL") { objRSum.BranchCode = null; }
                if (objRSum.customer_number == "ALL") { objRSum.customer_number = null; }
                if (objRSum.salesengineer_id == "ALL") { objRSum.salesengineer_id = null; }
                if (objRSum.userid == "ALL") { objRSum.userid = null; }
               
                dt = objRSum.budgetstatus(objRSum);
                if (dt != null && dt.Rows.Count > 0)
                {
                    budgetstatusvalues.DataSource = dt;
                    //lblResult.Text = "Please click on filter.";
                }
                else
                {
                    budgetstatusvalues.DataSource = null;
                    //lblResult.Text = "No records to display.";
                }
                budgetstatusvalues.DataBind();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                comFun.LogError(ex);
            }
        }

        protected void ddlCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserName"] == null || Session["RoleId"] == null)
            { Response.Redirect("Login.aspx?Login"); return; }

            string CustomerNumber = ddlCustomerName.SelectedItem.Value;
            
            ddlCustomerNumber.Text = CustomerNumber;
            // DataTable dtMain = loadBudget(CustomerNumber);
            budgetstatusvalues.DataSource = null;
            budgetstatusvalues.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }
        protected void ddlCustomerNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserName"] == null || Session["RoleId"] == null)
            { Response.Redirect("Login.aspx?Login"); return; }

            string CustomerNumber = ddlCustomerNumber.SelectedItem.Value;

            ddlCustomerName.SelectedValue = CustomerNumber;
            // DataTable dtMain = loadBudget(CustomerNumber);
            budgetstatusvalues.DataSource = null;
            budgetstatusvalues.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }
        #endregion


        #region Methods
        /// <summary>
        /// Author: Neha
        /// Date: Nov 27,2018
        /// Desc: 
        /// </summary>
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            DataTable dtData = new DataTable();
            objRSum.BranchCode = userId;
            objRSum.roleId = roleId;
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);
            ddlBranchList.DataSource = dtData;
            ddlBranchList.DataTextField = "BranchDesc";
            ddlBranchList.DataValueField = "BranchCode";
            ddlBranchList.DataBind();
            ddlBranchList.Items.Insert(0, "ALL");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strUserId"></param>
        /// <param name="roleId"></param>
        protected void LoadCustomers(string strUserId, string roleId)
        {
            DataTable dtCutomerDetails = new DataTable();

            dtCutomerDetails = objBudget.LoadCustomerDetails(roleId == "TM" ? "ALL" : strUserId, roleId, roleId == "TM" ? strUserId : null, Convert.ToString(Session["cter"]));
            if (dtCutomerDetails != null)
            {
                //DataTable dtDeatils = new DataTable();
                //dtDeatils.Columns.Add("customer_number", typeof(string));
                //dtDeatils.Columns.Add("customer_name", typeof(string));
                dtCutomerDetails.Columns.Add("c_name", typeof(string));


                for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                {
                    dtCutomerDetails.Rows[i]["c_name"] = string.Concat(Convert.ToString(dtCutomerDetails.Rows[i]["customer_short_name"]), "(", Convert.ToString(dtCutomerDetails.Rows[i]["customer_number"]), ")");
                    //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                }
                ddlCustomerName.DataSource = dtCutomerDetails;
                ddlCustomerName.DataTextField = "c_name";
                ddlCustomerName.DataValueField = "customer_number";
                ddlCustomerName.DataBind();
                ddlCustomerName.Items.Insert(0, "ALL");

                ddlCustomerNumber.DataSource = dtCutomerDetails;
                ddlCustomerNumber.DataTextField = "customer_number";
                ddlCustomerNumber.DataValueField = "customer_number";
                ddlCustomerNumber.DataBind();
                ddlCustomerNumber.Items.Insert(0, "ALL");

            }
        }
        #endregion
    }

}