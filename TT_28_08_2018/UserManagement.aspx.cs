﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class UserManagement : System.Web.UI.Page
    {
        #region GlobalDeclareation
        public string Empnumber, Empname, Empmail, Empusertype, EmpPassword, EmpBranch, EditEmpstatus, editempcontact;
        public int Empstatus;
        public long Empcontact;
        EngineerInfo objEngInfo = new EngineerInfo();
        AdminConfiguration objAdmin = new AdminConfiguration();
        LoginAuthentication objauth = new LoginAuthentication();
        Reports objReports = new Reports();
        List<string> cssList = new List<string>();
        Review objRSum = new Review();
        #endregion

        #region Events
        public static string cter;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {
                //if (Session["cter"] == null)
                //{
                //    Session["cter"] = "TTA";
                //    cter = "TTA";
                //    rdBtnTaegutec.Checked = true;
                //    rdBtnDuraCab.Checked = false;

                //}
                //else if (Session["cter"].ToString() == "DUR")
                //{
                //    cter = "DUR";
                //    rdBtnDuraCab.Checked = true;
                //    rdBtnTaegutec.Checked = false;
                //}
                //else
                //{
                //    cter = "TTA";
                //}
                LoadBranches();
                //LoadBranchesforBranchmanager();
                //LoadBranchesforTerritary();
                LoadEngineerInfo();
                //bindgridColor();
            }
            if (ViewState["GridData"] != null)
            {
                DataTable dtData = ViewState["GridData"] as DataTable;
                if (dtData.Rows.Count != 0)
                {
                    GridEngInfo.DataSource = dtData;
                    GridEngInfo.DataBind();
                }
            }
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);
        }
        protected void GridEngInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string data = "";
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string c = (e.Row.FindControl("lbl_branch") as Label).Text;
                string[] lines = Regex.Split(c, ",");
                foreach (var item in lines)
                {
                    PlaceHolder emails = e.Row.FindControl("ph_Region") as PlaceHolder;

                    data = item.ToString();
                    Label tags = new Label();
                    tags.EnableViewState = true;
                    tags.Text = data;
                    emails.Controls.Add(tags);
                    if (lines.Length != 1)
                    {
                        emails.Controls.Add(new LiteralControl("<br />"));
                    }
                }
            }
        }
     
        protected void Delete_Click1(object sender, ImageClickEventArgs e)
        {
            int flag = 0;
            //string confirmValue = Request.Form["confirm_value"];
            //if (confirmValue == "Yes")
            //{
                ImageButton ibtn1 = sender as ImageButton;
                int rowIndex = Convert.ToInt32(ibtn1.Attributes["RowIndex"]);
                GridViewRow row = GridEngInfo.Rows[rowIndex];
                var id = row.FindControl("lbl_engid") as System.Web.UI.WebControls.Label;
                string EngId = id.Text;
               
                objEngInfo.Deletebyadmin(EngId, flag);

                LoadEngineerInfo();
                // LoadBranches();
                //LoadBranchesforBranchmanager();
                //LoadBranchesforTerritary();
            //}
             ScriptManager.RegisterStartupScript(this, GetType(), "showalerts", "alert('Sucessfully deleted');", true);
           ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);

        }

       
        #endregion

        #region Methods
        protected void LoadEngineerInfo()
        {
            DataTable dt = new DataTable();
            dt = objEngInfo.getUserDetails();
            ViewState["GridData"] = null;
            ViewState["GridData"] = dt;
            GridEngInfo.DataSource = dt;
            GridEngInfo.DataBind();
        }

        protected void lbl_name_Click1(object sender, EventArgs e)
        {
            try
            {
                LoginAuthentication authObj = new LoginAuthentication();
                authObj.EngineerId = Convert.ToString((sender as LinkButton).CommandArgument);
                string ErrorMessage = authObj.AuthenticateUser(authObj);
                Session["Quote_ref"] = authObj.Quote_flag;
                Session["PhoneNumber"] = authObj.PhoneNumber;

                String UserGuid = System.Guid.NewGuid().ToString();
                Session["UserGuid"] = UserGuid;
                //Session["IP"] = ipAddress;
                Session["UserName"] = authObj.UserName;
                Session["LoginMailId"] = authObj.LoginMailID;
                Session["RoleId"] = authObj.RoleId;
                Session["UserId"] = authObj.EngineerId;
                Session["BranchCode"] = authObj.BranchCode;
                Session["BranchDesc"] = authObj.BranchDesc;
                Session["Territory"] = authObj.Territory;
                Session["EngineerId"] = authObj.EngineerId;
                Session["Password"] = authObj.MailPassword;
                Session["UserRole"] = "Admin";
                if (Session["RoleId"] != null)
                {
                    if (Session["RoleId"].ToString() == "Admin") { Response.Redirect("AdminProfile.aspx?Profile", false); }
                    else
                    {
                        Otp obj = new Otp();
                        obj.getFocusReportFlag(Convert.ToString(Session["UserId"]));
                        obj.SetCompanyCode(Convert.ToString(Session["UserId"]));
                        if (Convert.ToString(Session["Quote_ref"]) == "1")
                        {
                            if (Session["RoleId"].ToString() == "SE")
                            {
                                //Response.Redirect("QuoteSummary.aspx?Quote", false);
                                string url = "QuoteSummary.aspx?Quote";
                                string redirectURL = Page.ResolveClientUrl(url);
                                string script = "window.location = '" + redirectURL + "';";
                                ScriptManager.RegisterStartupScript(this, typeof(Page), "RedirectTo", script, true);
                            }
                            else
                            {
                                if (Convert.ToString(Session["RoleId"]) == "TM")
                                {
                                    //obj.SetTMBranches(Convert.ToString(Session["UserId"]));
                                }
                                Response.Redirect("PendingQuotes.aspx?Quote", false);
                            }
                            Context.ApplicationInstance.CompleteRequest();
                            
                        }
                        else
                        {
                            if (Convert.ToString(Session["RoleId"]) == "TM")
                            {
                                //obj.SetTMBranches(Convert.ToString(Session["UserId"]));
                            }
                            Response.Redirect("ReportDashboard.aspx?RD");
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                ScriptManager.RegisterStartupScript(this, GetType(), "mailfailed", "alert('Failed to load');", true);
            }

        }

        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            DataTable dtData = new DataTable();
            objRSum.roleId = "HO";
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);


            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);

        }
        //protected void LoadBranchesforBranchmanager()
        //{
        //    DataTable dtBranchesList = new DataTable();
        //    dtBranchesList = objEngInfo.LoadUserInfo(0, cter);

        //    ddlBranch.DataSource = dtBranchesList;
        //    ddlBranch.DataTextField = "region_description";
        //    ddlBranch.DataValueField = "region_code";
        //    ddlBranch.DataBind();

        //}
        //protected void LoadBranchesforTerritary()
        //{
        //    DataTable dtBranchesList = new DataTable();
        //    dtBranchesList = objEngInfo.LoadUserInfo(1);

        //    ddlBranchterritary.DataSource = dtBranchesList;
        //    ddlBranchterritary.DataTextField = "region_description";
        //    ddlBranchterritary.DataValueField = "region_code";
        //    ddlBranchterritary.DataBind();


        //}
        //protected void bindgridColor()
        //{
        //    if (GridEngInfo.Rows.Count != 0)
        //    {
        //        int color = 0;

        //        foreach (GridViewRow row in GridEngInfo.Rows)
        //        {

        //            color++;
        //            if (color == 1) { row.CssClass = "color_Product1 "; }
        //            else if (color == 2)
        //            {
        //                row.CssClass = "color_Product2 ";
        //                color = 0;
        //            }

        //        }
        //    }
        //}
        //protected void LoadCSS()
        //{
        //    cssList.Add("color_3");
        //    cssList.Add("color_4");
        //    cssList.Add("color_3");


        //}
        //protected string GetCSS(int colorIndex)
        //{
        //    string index = Convert.ToString(colorIndex);
        //    string cIndex = index[index.Length - 1].ToString();

        //    if (cIndex.Contains("1"))
        //    { return cssList.ElementAt(1); }
        //    else if (cIndex.Contains("2"))
        //    { return cssList.ElementAt(2); }
        //    else { return cssList.ElementAt(2); }

        //}
        #endregion
    }
}