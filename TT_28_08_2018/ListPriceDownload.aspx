﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListPriceDownload.aspx.cs" Inherits="TaegutecSalesBudget.ListPriceDownload" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />

      <script type="text/javascript">

        var table1;
        $(document).ready(function () {
            debugger;
            //LoadTable();

            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#MainContent_reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
        });
        function LoadTable() {
            var head_content = $('#MainContent_grdListPrice tr:first').html();
            $('#MainContent_grdListPrice').prepend('<thead></thead>')
            $('#MainContent_grdListPrice thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdListPrice tbody tr:first').hide();
            $('#MainContent_grdListPrice').DataTable(
                {
                    "info": false
                    //"paging": false,
                    //"sScrollY": ($(window).height() - 200),
                });

            var head_content1 = $('#MainContent_grdListPrice1 tr:first').html();
            $('#MainContent_grdListPrice1').prepend('<thead></thead>')
            $('#MainContent_grdListPrice1 thead').html('<tr>' + head_content1 + '</tr>');
            $('#MainContent_grdListPrice1 tbody tr:first').hide();
            $('#MainContent_grdListPrice1').DataTable(
                {
                    "info": false,
                    "paging": false,
                    "sScrollY": ($(window).height() - 200),
                });


        }

          function LoadSecondGrid() {
              var $RowSelected1 = $("#MainContent_grdListPrice");

              if ($RowSelected1.length > 0) {
                  $("#selectlist").show();
                  $("#showdropdown").show();
                  $(".rightside_box").show();
                  $("#MainContent_pnlEdit").css({
                      "border-color": "#ddd",
                      "border-width": "1px",
                      "border-style": "solid"
                  });
              }
              else {
                  var status = '<%=Session["gridstatus"].ToString()%>';
                  if (status == "empty") {
                      $("#selectlist").show();
                      $("#showdropdown").show();
                      $("#MainContent_pnlEdit").css({
                          "border-color": "#ddd",
                          "border-width": "1px",
                          "border-style": "solid"
                      });
                  }
              }
            var $RowSelected = $("#MainContent_grdListPrice1");

            if ($RowSelected.length > 0) {
                $("#selectedlist").show();
                $("#MainContent_txtsearch1").show();
                $("#MainContent_searchbtn1").show();
                $(".rightside_box").hide();
                $("#MainContent_Panel2").css({
                    "border-color": "#ddd",
                    "border-width": "1px",
                    "border-style": "solid"
                });
              }
            else {
                var status = '<%=Session["gridstatus"].ToString()%>';
                if (status == "empty") {
                    $("#selectedlist").show();
                    $("#MainContent_txtsearch1").show();
                    $("#MainContent_searchbtn1").show();
                    $(".rightside_box").hide();
                    $("#MainContent_Panel2").css({
                        "border-color": "#ddd",
                        "border-width": "1px",
                        "border-style": "solid"
                    });
                }
            }

              $('#product_image').unbind('click').bind('click', function (e) {
                  var attr = $('#product_image').attr('src');
                  $("#MainContent_reportdrpdwns").slideToggle();
                  if (attr == "images/up_arrow.png") {
                      $("#product_image").attr("src", "images/down_arrow.png");
                  } else {
                      $("#product_image").attr("src", "images/up_arrow.png");
                  }
              });
          }

        
        function selectAll(obj) {
            debugger;
            var id = obj.children[0].id.replace('chk_All', 'hdn_chk_All');
            if ($('#' + id).val() == "0")
                $('#' + id).val("1");
            else
                $('#' + id).val("0");
            var totaltrCount = $("[id*=MainContent_grdListPrice] tr").length;
            var trCount = totaltrCount;
            //sessionStorage.getItem("tot_Count");
            //$("[id*=grdPendingQuote] td").closest("tr").length;
            var multi_btn_flag = 0;
            var hdn_chk_id;
            var chk_id;
            if ($('#' + id).val() == "0") {
                for (i = 0; i < trCount; i++) {
                    hdn_chk_id = "MainContent_grdListPrice_Hdnchk_" + i;
                    chk_id = "MainContent_grdListPrice_chk_multi_" + i;
                    $("input[type='checkbox']").prop('checked', false);
                    //$("input[id=" + chk_id + "]").prop('checked', false);
                    $('#' + hdn_chk_id).val("0");

                }

            }
            else {
                for (i = 0; i < trCount; i++) {
                    hdn_chk_id = "MainContent_grdListPrice_Hdnchk_" + i;
                    chk_id = "MainContent_grdListPrice_chk_multi_" + i;
                    $("input[type='checkbox']").prop('checked', true);
                    //$("input[id=" + chk_id + "]").prop('checked', true);
                    $('#' + hdn_chk_id).val("1");

                }

            }
          } 

          $(document).on('click', '.filter', function (e) {
              jQuery(".loader_div").show();

          }); 

          $(document).on('click', '.chk_multi_add', function (e) {
              jQuery(".loader_div").show();

          });

          $(document).on('click', '.chk1', function (e) {
              jQuery(".loader_div").show();

          });

          $(document).on('click', '.chk_multi_All', function (e) {
              jQuery(".loader_div").show();

          }); 

          $(document).on('click', '.chk_multi_All1', function (e) {
              jQuery(".loader_div").show();

          }); 

          $(document).on('click', '.search', function (e) {
              jQuery(".loader_div").show();

          });

          $(document).on('change', '.showrecord', function (e) {
              jQuery(".loader_div").show();

          });

          $(document).on('click', '.clear', function (e) {
              jQuery(".loader_div").show();

          });

          function controlEnter(obj, event) {
              var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
              if (keyCode == 13) {
                  __doPostBack(obj, '');
                  return false;
              }
              else {
                  return true;
              }
          }
          

      </script>
    <style>
       
        .label{
            color:black;
        }
         .search {
            position: absolute;
            top: 3px;
            right: 4px;
            width: 20px;
        }

        .search1 {
            position: absolute;
            top: 10px;
            right: 10px;
            width: 20px;
        }

         .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px !important;
        }

        .GridPager a,
        .GridPager span {
            display: inline-block;
            padding: 0px 9px;
            margin-right: 4px;
            border-radius: 3px;
            border: solid 1px #c0c0c0;
            background: #e9e9e9;
            box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
            font-size: .875em;
            font-weight: bold;
            text-decoration: none;
            color: #717171;
            text-shadow: 0px 1px 0px rgba(255,255,255, 1);
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background: #375b67;
            box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
            color: #f0f0f0;
            text-shadow: 0px 0px 3px rgba(0,0,0, .5);
            border: 1px solid #3AC0F2;
        }
        .loader_div
        {
            position: fixed;
            top: 0;
            bottom: 0%;
            left: 0;
            right: 0%;
            z-index: 99;
            opacity: 0.7;
            display: none;
            background: lightgrey url('../../../images/loader.gif') center center no-repeat;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--  <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" ScriptMode="Debug">
         
   </asp:ScriptManager>--%>
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" ScriptMode="Debug" EnablePartialRendering="false">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>List Price Download</a>
                    </li>
                    <div>
                        <li class="title_bedcrum" align="left" style="margin-left: 6%;">LIST PRICE DOWNLOAD</li>
                        <%--<ul>
                            <li class="title_bedcrum" style="margin-left: 6%;">LIST PRICE DOWNLOAD</li>
                        </ul>--%>
                    </div>
                    <%--<li class="title_bedcrum" align="left" style="margin-left: 6%;">LIST PRICE DOWNLOAD</li>--%>

                </ul>
                <!-- End : Breadcrumbs -->
            </div>

            <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />

            </div>

              <div id="MainContent_reportdrpdwns" class="row filter_panel ">
                <div runat="server" id="cterDiv" visible="false">
                    <ul id="divCter" runat="server" class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>
                            <asp:RadioButton ID="rdBtnTaegutec" Checked="true" Enabled="false" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" AutoPostBack="true" onChange="hideTable();" />
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" Enabled="false" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" AutoPostBack="true" onChange="hideTable();" />
                        </li>
                    </ul>
                </div>
                  <br />
                <div class="col-md-2 control" runat="server" id="divBranch">
                    <label class="label">BRANCH</label>
                    <input type="text" id="BranchText" runat="server" visible="false" />
                   <asp:DropDownList runat="server" ID="BranchList" CssClass="control_dropdown" style="height: 29px;" AutoPostBack="true" OnSelectedIndexChanged="BranchList_SelectedIndexChanged"></asp:DropDownList>

                </div>
                <input type="hidden" id="blist" runat="server" />
                <div class="col-md-2 control" runat="server" id="divSE">
                    <label class="label">SALES ENGINEER </label>
                    <input type="text" id="SaleText" runat="server" visible="false" />
                    <asp:DropDownList runat="server" ID="SalesEngList" CssClass="control_dropdown" style="height: 29px;" AutoPostBack="true" OnSelectedIndexChanged="SalesEngList_SelectedIndexChanged"></asp:DropDownList>
                    <input type="hidden" id="slist" runat="server" />
                </div>

                <div class="col-md-2 control">
                    <label class="label">CUSTOMER TYPE </label>
                    <asp:DropDownList ID="ddlcustomertype" runat="server" style="height: 29px;" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlcustomertype_SelectedIndexChanged">
                        <asp:ListItem Text="ALL" Value="ALL" />
                        <asp:ListItem Text="CUSTOMER" Value="C" />
                        <asp:ListItem Text="CHANNEL PARTNER" Value="D" />
                    </asp:DropDownList>
                </div>
                <asp:Label ID="Sctype" runat="server"></asp:Label>

                <div class="col-md-2 control">
                    <label class="label">CUSTOMER NAME </label>
                    <asp:DropDownList runat="server" ID="CustNameList" CssClass="control_dropdown" style="height: 29px;" AutoPostBack="true" OnSelectedIndexChanged="CustNameList_SelectedIndexChanged"></asp:DropDownList>
                    <input type="hidden" id="lcna" runat="server" />
                </div>

               <%-- <div class="col-md-2 control">
                    <label class="label ">CUSTOMER NUMBER</label>
                    <asp:DropDownList runat="server" style="height: 29px;" CssClass="control_dropdown" ID="CustNumList" AutoPostBack="true" OnSelectedIndexChanged="CustNumList_SelectedIndexChanged"></asp:DropDownList>
                </div>--%>
                
                <input type="hidden" id="lcnu" runat="server" />

                <input type="hidden" runat="server" id="apl" />
                <div class="col-md-2 ">
                   
                    <label id="alertmsg" style="display: none; font-weight: bold; color: #0582b7;">Now click on Filter to view results</label>
                </div>
                <asp:Button ID="reports" runat="server" CssClass="btn green filter" OnClick="reports_Click" Style="top: 9px !important;margin-left: -200px;" Text="FILTER" />
            </div>

            <br />
            <div class="col-md-6 mn_margin" style="margin-top: 2%;">
                <div id="selectlist" class="filter_panel" style="background:#006780;color: white; padding: 10px;display: none; font-weight: bold;">Select List Price To Download</div>

            <asp:Panel runat="server" ID="pnlEdit" Style="margin-top: 0;overflow: auto;margin-right: -1px;height: 350px;padding: 5px;">
                <div class="col-md-12" id="showdropdown" style="display: none;">
                        <div class="col-md-4">
                            <label class="label" style="color: black; font-size: 14px;margin-left:-11px;">Show </label>
                            <asp:DropDownList ID="ddlselect" CssClass="showrecord" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlselect_SelectedIndexChanged">
                                <asp:ListItem Text="10" Value="10" />
                                <asp:ListItem Text="25" Value="25" />
                                <asp:ListItem Text="50" Value="50" />
                                <asp:ListItem Text="100" Value="100" />
                            </asp:DropDownList>
                        </div>
                        <div class="wrapper" style="position: relative">
                            <asp:TextBox runat="server" ID="txtsearch" CssClass="control_dropdown" placeholder="Search" Style="float: right;margin-bottom:10px;"></asp:TextBox>
                            <asp:ImageButton runat="server" CssClass="search" ImageUrl="images/Search.png" ID="searchbtn" OnClick="searchbtn_Click" />
                            <%--<asp:Button ID="Button1" runat="server" Text="Go" style="position:absolute;top:0;right:0;" />--%>
                        </div>
                    </div>
            <asp:GridView ID="grdListPrice" CssClass="display compact" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        PageSize="10" OnPageIndexChanging="grdListPrice_PageIndexChanging" AllowSorting="true" Style="border-collapse: collapse; width: 100%; border: azure;">
                        <PagerStyle HorizontalAlign="right" CssClass="GridPager" />
                    <%--<asp:GridView ID="grdListPrice" CssClass="display compact" runat="server" AutoGenerateColumns="false"
                        OnPageIndexChanging="grdListPrice_PageIndexChanging" AllowSorting="true" Style="border-collapse: collapse; width: 100%; border: azure;">--%>
                        <Columns>

                            <asp:TemplateField>
                                <HeaderTemplate>

                                    <%-- <asp:CheckBox ID="chk_All" runat="server" CssClass="chk_multi_All" AutoPostBack="true" onchange="selectAll(this);" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />--%>
                                    <asp:CheckBox ID="chk_All1" runat="server" CssClass="chk_multi_All1" AutoPostBack="true" EnableViewState="false" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />

                                    <asp:HiddenField ID="hdn_chk_All1" runat="server" Value="0" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%--<asp:CheckBox ID="chk_multi" runat="server" CssClass="chk_multi_add" class="chkbox" />--%>
                                    <asp:CheckBox ID="chk_multi" runat="server" CssClass="chk_multi_add" AutoPostBack="true" OnCheckedChanged="chkbox_CheckedChanged" />
                                    <asp:HiddenField ID="hdn_chk_multi" runat="server" Value='<%# Bind("Item_code") %>' />
                                    <asp:HiddenField ID="Hdnchk" runat="server" Value="0" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Group">
                                <ItemTemplate>
                                    <asp:Label ID="lblgrp" Text='<%#Bind("Group") %>' runat="server" Width="70px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblitem" Text='<%#Bind("Item_code") %>' runat="server" Width="70px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Desc">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemDesc" Text='<%#Bind("Item_Desc") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Product Family">
                                <ItemTemplate>
                                    <asp:Label ID="lblpf" Text='<%#Bind("ProductFamily") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblsc" Text='<%#Bind("StockCode") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="List Price">
                                <ItemTemplate>
                                     <div style="text-align: right">
                                    <asp:Label ID="lblLP" Text='<%#Bind("Price") %>' runat="server"></asp:Label>
                                         </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </div>
            <div class="col-md-6 mn_margin" style="margin-top: 2%;">
                <div class="rightside_box" style="height: 386px;border: solid 1px #ddd;display:none; padding: 100px 60px;text-align: center;color: #375b67;">
                    <h4 style="font-weight: bold;font-size: 14px;font-family:Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif;">Please select item from left table to collect your selection in right table for PDF generation.</h4>
                </div>
                <div id="selectedlist" style="background:#006780;color: white; display: none; padding: 10px; font-weight: bold;margin: 0 0 0 0 !important;" class="filter_panel">
                    Selected List Price To Download
             <%--<asp:Button runat="server" ID="btnpdf" CssClass="btnSubmit" style="float:right;background: #036ea7;color:white" OnClick="btnpdf_Click" Text="Export PDF" />
                    <asp:Button runat="server" ID="Butclear" CssClass="btnSubmit" style="float:right;margin-right: 5px;background: #036ea7;color:white" OnClick="btnclear_Click" Text="Clear" />--%>
                    <asp:Button runat="server" ID="btnpdf" CssClass="btn green" style="float:right;top: -5px !important;margin-top: -4px;margin-right: 10px;" OnClick="btnpdf_Click" Text="Export PDF" />
                    <asp:Button runat="server" ID="Butclear" CssClass="btn green clear" style="float:right;top: -5px !important;margin-top: -4px;margin-right: 10px;" OnClick="btnclear_Click" Text="Clear" />
                </div>
                <asp:Panel runat="server" ID="Panel2" Style="margin-top: 0px;height: 350px;overflow: auto;margin-left: -1px;padding: 5px;width:100%;position: relative;">
                    <div class="col-md-12">
                        <asp:TextBox runat="server" ID="txtsearch1" CssClass="control_dropdown" placeholder="Search" Style="float: right; display: none;margin-bottom:10px;position:relative"></asp:TextBox>
                        <asp:ImageButton runat="server" CssClass="search1 search" ImageUrl="images/Search.png" Style="display: none" ID="searchbtn1" OnClick="searchbtn1_Click" />
                    </div>
                    <asp:GridView ID="grdListPrice1" CssClass="display compact1" runat="server" AutoGenerateColumns="false" AllowSorting="true" Style="border-collapse: collapse; width: 100%; border: azure;">

                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>

                                    <%-- <asp:CheckBox ID="chk_All" runat="server" CssClass="chk_multi_All" AutoPostBack="true" onchange="selectAll(this);" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />--%>
                                    <asp:CheckBox ID="chk_All" runat="server" CssClass="chk_multi_All" AutoPostBack="true" EnableViewState="false" OnCheckedChanged="chkboxUnSelectAll_CheckedChanged" />

                                    <asp:HiddenField ID="hdn_chk_All" runat="server" Value="0" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk1" runat="server" CssClass="chk1" Checked="true" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged1" />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Group">
                                <ItemTemplate>
                                    <asp:Label ID="lblgrp" Text='<%#Bind("Group") %>' runat="server" Width="70px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblitem" Text='<%#Bind("Item_code") %>' runat="server" Width="70px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Desc">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemDesc" Text='<%#Bind("Item_Desc") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Product Family">
                                <ItemTemplate>
                                    <asp:Label ID="lblpf" Text='<%#Bind("ProductFamily") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblsc" Text='<%#Bind("StockCode") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="List Price">
                                <ItemTemplate>
                                     <div style="text-align: right">
                                    <asp:Label ID="lblLP" Text='<%#Bind("Price") %>' runat="server"></asp:Label>
                                         </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </asp:Panel>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnpdf" />
             <asp:PostBackTrigger ControlID="Butclear" />
            <%--<asp:AsyncPostBackTrigger ControlID="chk_All" EventName="checkedclick"/>--%>
        </Triggers>
    </asp:UpdatePanel>
     <div id="loader_div" class="loader_div"></div>
  <%-- <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7; visibility:visible" >
                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff" >Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
</asp:Content>
