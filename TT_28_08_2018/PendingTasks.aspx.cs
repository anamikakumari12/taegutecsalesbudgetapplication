﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class PendingTasks : System.Web.UI.Page
    {
        MDP objMDP = new MDP();
        static DataTable dtprojectReport;
        public static bool potSort;
        static DataTable sortedReports = null;
        public static string cter;
        CommonFunctions objFunc = new CommonFunctions();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                Session["Customer_Number"] = null;
                Session["Project_Number"] = null;
                Session["Project"] = null;
                if (!IsPostBack)
                {
                    cter = null;
                    string roleId = Session["RoleId"].ToString();

                    if (roleId == "HO")
                    {
                        if (Session["cter"] == null)
                        {
                            Session["cter"] = "TTA";
                            cter = "TTA";

                        }
                        if (Session["cter"].ToString() == "DUR")
                        {
                            rdBtnDuraCab.Checked = true;
                            rdBtnTaegutec.Checked = false;
                            cter = "DUR";
                        }
                        else
                        {
                            rdBtnTaegutec.Checked = true;
                            rdBtnDuraCab.Checked = false;
                            cter = "TTA";
                        }
                        cterDiv.Visible = true;
                    }
                    proceed_Click(null, null);

                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void proceed_Click(object sender, EventArgs e)
        {
            dtprojectReport = new DataTable();
            string strUserId = Session["UserId"].ToString();
            Session["Project"] = "Existing";
            try
            {
                DataTable projectReport = objMDP.getProjectListForApproval(strUserId, cter);

                dtprojectReport.Columns.Add("customer_num", typeof(string));
                dtprojectReport.Columns.Add("customer_lbl", typeof(string));
                dtprojectReport.Columns.Add("distributor_lbl", typeof(string));
                dtprojectReport.Columns.Add("distributor_num", typeof(string));
                dtprojectReport.Columns.Add("project_title", typeof(string));
                dtprojectReport.Columns.Add("project_num", typeof(string));
                dtprojectReport.Columns.Add("creation_date_lbl", typeof(string));
                dtprojectReport.Columns.Add("overall_pot_lbl", typeof(string));
                dtprojectReport.Columns.Add("potential_lakhs_lbl", typeof(string));
                dtprojectReport.Columns.Add("status_lbl", typeof(string));
                dtprojectReport.Columns.Add("txt_remarks", typeof(string));


                if (projectReport.Rows.Count > 0)
                {
                    for (int i = 0; i < projectReport.Rows.Count; i++)
                    {
                        string customer_num = projectReport.Rows[i].ItemArray[0].ToString();
                        string customer_lbl = projectReport.Rows[i].ItemArray[1].ToString() + " (" + projectReport.Rows[i].ItemArray[0].ToString() + " )";
                        string distributor_num = projectReport.Rows[i].ItemArray[2].ToString();
                        string distributor_lbl = "";
                        if (distributor_num != "")
                        {
                            distributor_lbl = objMDP.get_distributorname_by_number(distributor_num) + " (" + distributor_num + " )";
                        }

                        string ptoject_title = projectReport.Rows[i].ItemArray[3].ToString() + " (" + projectReport.Rows[i].ItemArray[4].ToString() + " )";
                        string project_num = projectReport.Rows[i].ItemArray[4].ToString();
                        string creation_date_lbl = projectReport.Rows[i].ItemArray[10].ToString();
                        string overall_pot_lbl = projectReport.Rows[i].ItemArray[5].ToString();
                        string potential_lakhs_lbl = projectReport.Rows[i].ItemArray[6].ToString();
                        string status_lbl = "";
                        if (projectReport.Rows[i].ItemArray[8].ToString() == "True")
                        {
                            status_lbl = "Rejected";
                        }
                        if (projectReport.Rows[i].ItemArray[9].ToString() == "True")
                        {
                            status_lbl = "Pending Approval";
                        }
                        string txt_remarks = projectReport.Rows[i].ItemArray[7].ToString();
                        dtprojectReport.Rows.Add(customer_num, customer_lbl, distributor_lbl, distributor_num, ptoject_title, project_num, creation_date_lbl, overall_pot_lbl, potential_lakhs_lbl, status_lbl, txt_remarks);


                    }
                    projectreports.DataSource = dtprojectReport;

                    projectreports.DataBind();
                    // this.RegisterPostBackControl();
                    bindgridColor();
                    sortedReports = dtprojectReport;
                }
                else
                {

                    DataTable dtReport = new DataTable();
                    projectreports.DataSource = dtReport;
                    projectreports.DataBind();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('No project to display.');", true);

                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void bindgridColor()
        {
            try
            {
                if (projectreports.Rows.Count != 0)
                {
                    int color = 0;

                    foreach (GridViewRow row in projectreports.Rows)
                    {

                        color++;
                        if (color == 1) { row.CssClass = "color_Product1 "; }
                        else if (color == 2)
                        {
                            row.CssClass = "color_Product2 ";
                            color = 0;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void status_lbl_click(object sender, EventArgs e)
        {
            try
            {
                Session["Customer_Number"] = hdn_CustomerNum.Value;
                Session["Project_Number"] = hdn_projectnum.Value;
                Session["Distributor_Number"] = hdn_distributonum.Value;
                Session["Project"] = "Existing";
                if (Session["Customer_Number"] != null)
                {
                    Response.Redirect("MDPEntry.aspx?Projects");
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void ReportGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //Retrieve the table from the session object.
                DataTable dt = sortedReports.Copy();
                EnumerableRowCollection<DataRow> dr1 = null;
                //dt.Rows[dt.Rows.Count - 1].Delete();

                if (dt != null)
                {

                    //Sort the data.

                    if (GetSortDirection(e.SortDirection) == "ASC")
                    {
                        dr1 = (from row in dt.AsEnumerable()

                               orderby row[e.SortExpression] ascending

                               select row);
                    }
                    else
                    {
                        dr1 = (from row in dt.AsEnumerable()

                               orderby row[e.SortExpression] descending

                               select row);
                    }
                    DataTable dx = dr1.AsDataView().ToTable();
                    //dx.Rows.Add(sortedReports.Rows[sortedReports.Rows.Count - 1].ItemArray);
                    projectreports.DataSource = dx;
                    projectreports.DataBind();


                    bindgridColor();
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

        }

        private string GetSortDirection(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;
            try
            {
                switch (sortDirection)
                {
                    case SortDirection.Ascending:
                        newSortDirection = "ASC";
                        break;
                    case SortDirection.Descending:
                        newSortDirection = "DESC";
                        break;
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return newSortDirection;
        }

        protected void sort_potential_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (potSort == false)
                {
                    ReportGridView_Sorting(null, new GridViewSortEventArgs("potential_lakhs_lbl", SortDirection.Ascending));
                    potSort = true;
                }
                else
                {
                    ReportGridView_Sorting(null, new GridViewSortEventArgs("potential_lakhs_lbl", SortDirection.Descending));
                    potSort = false;
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void sort_title_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (potSort == false)
                {
                    ReportGridView_Sorting(null, new GridViewSortEventArgs("project_title", SortDirection.Ascending));
                    potSort = true;
                }
                else
                {
                    ReportGridView_Sorting(null, new GridViewSortEventArgs("project_title", SortDirection.Descending));
                    potSort = false;
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void sort_creation_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (potSort == false)
                {
                    ReportGridView_Sorting(null, new GridViewSortEventArgs("creation_date_lbl", SortDirection.Ascending));
                    potSort = true;
                }
                else
                {
                    ReportGridView_Sorting(null, new GridViewSortEventArgs("creation_date_lbl", SortDirection.Descending));
                    potSort = false;
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void sort_customer_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (potSort == false)
                {
                    ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_lbl", SortDirection.Ascending));
                    potSort = true;
                }
                else
                {
                    ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_lbl", SortDirection.Descending));
                    potSort = false;
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void sort_overall_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (potSort == false)
                {
                    ReportGridView_Sorting(null, new GridViewSortEventArgs("overall_pot_lbl", SortDirection.Ascending));
                    potSort = true;
                }
                else
                {
                    ReportGridView_Sorting(null, new GridViewSortEventArgs("overall_pot_lbl", SortDirection.Descending));
                    potSort = false;
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        //private void RegisterPostBackControl()
        //{
        //    foreach (GridViewRow row in projectreports.Rows)
        //    {
        //        LinkButton lnkFull = row.FindControl("status_lbl") as LinkButton;
        //        ScriptManager.GetCurrent(this).RegisterPostBackControl(lnkFull);
        //    }
        //}
        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdBtnTaegutec.Checked)
                {
                    cter = "TTA";
                    Session["cter"] = "TTA";
                }
                if (rdBtnDuraCab.Checked)
                {
                    cter = "DUR";
                    Session["cter"] = "DUR";
                }
                proceed_Click(null, null);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
    }
}