﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace TaegutecSalesBudget
{
    /// <summary>
    /// Summary description for FileUpload
    /// </summary>
    public class QuoteFileUpload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Files.Count > 0)
            {
                string ref_number = context.Request.QueryString[0];
                string item = context.Request.QueryString[1];
                string role = context.Request.QueryString[2];
                //Fetch the Uploaded File.
                HttpPostedFile postedFile = context.Request.Files[0];
                string path = string.Empty;
                path = (Convert.ToString(ref_number)).Replace("/", "_") + "/";
                string folder = ConfigurationManager.AppSettings["Escalation_Folder"].ToString() + (Convert.ToString(ref_number)).Replace("/", "_") + "/";
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                path += Convert.ToString(item) + "/";
                folder += Convert.ToString(item) + "/";
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                if (role == "HO")
                {
                    folder += "HO/";
                    path += "HO/";
                }
                else
                {
                    folder += "BM/";
                    path += "BM/";
                }
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                string filename = (Convert.ToString(ref_number)).Replace("/", "_") + "_" + Convert.ToString(item) + "_" + DateTime.Now.ToString("ddMMyyyyhhmm") + Path.GetExtension(postedFile.FileName);
                path = Path.Combine(path, filename);
                string filepath = Path.Combine(folder, filename);

                //Set the File Name.
                // string fileName = Path.GetFileName(postedFile.FileName);

                //Save the File in Folder.
                postedFile.SaveAs(filepath);

                //Send File details in a JSON Response.
                string json = new JavaScriptSerializer().Serialize(
                    new
                    {
                        name = path
                    });
                context.Response.StatusCode = (int)HttpStatusCode.OK;
                context.Response.ContentType = "text/json";
                context.Response.Write(json);
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}