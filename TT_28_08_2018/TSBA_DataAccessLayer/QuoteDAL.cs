﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSBA_BusinessObjects;

namespace TSBA_DataAccessLayer
{
    public class QuoteDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable GetItemDescDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getItemDesc, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objQuoteBO.Cust_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetFrequentItemsDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getFrequentlyOrderedItem, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.Requested_By, SqlDbType.VarChar, 100).Value = objQuoteBO.Cust_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetQuoteStatusLogDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getStatusLog, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

       
        public DataTable GetAPListDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getAgreementPrice, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objQuoteBO.Cust_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetCustomerListDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getCustomers_Quote, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.assigned_salesengineer_id, SqlDbType.VarChar, 10).Value = objQuoteBO.SE_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.roleId, SqlDbType.VarChar, 50).Value = objQuoteBO.role_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.custtype, SqlDbType.VarChar, 10).Value = objQuoteBO.cust_type;
                sqlcmd.Parameters.Add(ResourceFileDAL.branchcode, SqlDbType.VarChar, 10).Value = objQuoteBO.branch;
                sqlcmd.Parameters.Add(ResourceFileDAL.cter, SqlDbType.VarChar, 10).Value = objQuoteBO.cter;
                sqlcmd.Parameters.Add(ResourceFileDAL.customer_class, SqlDbType.VarChar, 10).Value = objQuoteBO.customer_class;
                sqlcmd.Parameters.Add(ResourceFileDAL.territory_engineer_id, SqlDbType.VarChar, 10).Value = objQuoteBO.TM_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public QuoteBO DraftQuotesDAL(DataTable dt)
        {
            throw new NotImplementedException();
        }

        public DataTable GetRecentItemsDAL(QuoteBO objQuoteBO)
        {

            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getRecentlyOrderedItem, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.Requested_By, SqlDbType.VarChar, 100).Value = objQuoteBO.Cust_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetItemDetailsDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_GetItemDetailsForQuote, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objQuoteBO.Cust_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public QuoteBO SaveQuotesDAL(DataTable dt)
        {
            QuoteBO objQuoteBO = new QuoteBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda = null;
            DataTable dtoutput = new DataTable();
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_saveQuotes, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.tblQuote, dt);
                sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                objQuoteBO.Err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                objQuoteBO.Err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);
                objQuoteBO.Ref_Number = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.RefNumber].Value);

                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getMailDetailsForQuote, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Module, SqlDbType.VarChar, -1).Value = "Quote";
                sqlcmd.Parameters.Add(ResourceFileDAL.Component, SqlDbType.VarChar, -1).Value = "SubmitQuote";
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
                if (dtoutput != null)
                {
                    if (dtoutput.Rows.Count > 0)
                    {
                        objQuoteBO.to = Convert.ToString(dtoutput.Rows[0]["To"]);
                        objQuoteBO.cc = Convert.ToString(dtoutput.Rows[0]["CC"]);
                        objQuoteBO.subject = Convert.ToString(dtoutput.Rows[0]["Subject"]);
                        objQuoteBO.message = Convert.ToString(dtoutput.Rows[0]["Message"]);
                    }
                }

            }
            catch (Exception ex)
            {
                objQuoteBO.Err_code = 202;
                objQuoteBO.Err_msg = ex.Message;
                objCom.LogError(ex);
            }
            return objQuoteBO;
        }

        public string SubmitStatusDAL(int ID, string status, string ocNumber, int ocflag, string comment, string changedDate, string loggedBy)
        {
            string result = "";
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.insertUpdateQuotestatus, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.quoteId1, SqlDbType.Int).Value = ID;
                sqlcmd.Parameters.Add(ResourceFileDAL.ocflag, SqlDbType.Int, 200).Value = ocflag;
                sqlcmd.Parameters.Add(ResourceFileDAL.quotestatus, SqlDbType.VarChar, 100).Value = status;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag1, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.userId1, SqlDbType.VarChar, 100).Value = loggedBy;
                sqlcmd.Parameters.Add(ResourceFileDAL.StatusChangeDate, SqlDbType.DateTime, 100).Value = changedDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.OcNumber, SqlDbType.VarChar, 100).Value = ocNumber;
                sqlcmd.Parameters.Add(ResourceFileDAL.Comment1, SqlDbType.VarChar, 100).Value = comment;

                sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                result = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);


            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return result;
        }

        public DataTable GetQuoteDetailsForPADAL(QuoteBO objquoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getQuoteDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objquoteBO.Item_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, 100).Value = objquoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Status, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Status;
                sqlcmd.Parameters.Add(ResourceFileDAL.customer_number, SqlDbType.VarChar, 100).Value = objquoteBO.Cust_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.branchcode, SqlDbType.VarChar, 100).Value = objquoteBO.branch;
                sqlcmd.Parameters.Add(ResourceFileDAL.StartDate, SqlDbType.VarChar, 100).Value = objquoteBO.StartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.EndDate, SqlDbType.VarChar, 100).Value = objquoteBO.EndDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.assigned_salesengineer_id, SqlDbType.VarChar, 100).Value = objquoteBO.SE_id;
                //sqlcmd.CommandTimeout = 100000000;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
        public DataTable GetQuoteDetailsDAL(QuoteBO objquoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getQuoteDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objquoteBO.Item_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, 100).Value = objquoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.customer_number, SqlDbType.VarChar, 100).Value = objquoteBO.Cust_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.branchcode, SqlDbType.VarChar, 100).Value = objquoteBO.branch;
                sqlcmd.Parameters.Add(ResourceFileDAL.StartDate, SqlDbType.VarChar, 100).Value = objquoteBO.StartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.EndDate, SqlDbType.VarChar, 100).Value = objquoteBO.EndDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.assigned_salesengineer_id, SqlDbType.VarChar, 100).Value = objquoteBO.SE_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetQuoteSummaryDAL(QuoteBO objquoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getQuoteSummary, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.StartDate, SqlDbType.VarChar, 100).Value = objquoteBO.StartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.EndDate, SqlDbType.VarChar, 100).Value = objquoteBO.EndDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objquoteBO.Cust_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.branchcode, SqlDbType.VarChar, 100).Value = objquoteBO.branch;
                sqlcmd.Parameters.Add(ResourceFileDAL.assigned_salesengineer_id, SqlDbType.VarChar, 100).Value = objquoteBO.SE_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.cter, SqlDbType.VarChar, 100).Value = objquoteBO.cter;
                sqlcmd.Parameters.Add(ResourceFileDAL.customer_class, SqlDbType.VarChar, 100).Value = objquoteBO.customer_class;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.roleId, SqlDbType.VarChar, 100).Value = objquoteBO.role_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.user_id, SqlDbType.VarChar, 100).Value = objquoteBO.user_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetCompetitorsDAL()
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getCompetitorsForQuote, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public QuoteBO PlaceOrderDAL(QuoteBO objBO)
        {

            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda = null;
            DataTable dtoutput = new DataTable();
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_PlaceOrderForQuote, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.QuoteId, SqlDbType.Int).Value = objBO.QuoteID;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, 200).Value = objBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objBO.Item_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.OrderType, SqlDbType.VarChar, 100).Value = objBO.OrderType;
                sqlcmd.Parameters.Add(ResourceFileDAL.ScheduleType, SqlDbType.VarChar, 100).Value = objBO.scheduleType;
                sqlcmd.Parameters.Add(ResourceFileDAL.OrderByID, SqlDbType.VarChar, 100).Value = objBO.orderbyId;
                sqlcmd.Parameters.Add(ResourceFileDAL.OrderByName, SqlDbType.VarChar, 100).Value = objBO.orderbyName;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objBO.Cust_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustName, SqlDbType.VarChar, -1).Value = objBO.Cust_Name;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = ResourceFileDAL.Quote_Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.Quantity, SqlDbType.VarChar, 100).Value = objBO.quantity;
                sqlcmd.Parameters.Add(ResourceFileDAL.OrderStartDate, SqlDbType.VarChar, 100).Value = objBO.OrderStartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.Reason, SqlDbType.VarChar, -1).Value = objBO.POComment;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                objBO.Err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                objBO.Err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);


                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getMailDetailsForQuote, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Module, SqlDbType.VarChar, -1).Value = "Quote";
                sqlcmd.Parameters.Add(ResourceFileDAL.Component, SqlDbType.VarChar, -1).Value = "PlaceOrder";
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objBO.Item_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
                if (dtoutput != null)
                {
                    if (dtoutput.Rows.Count > 0)
                    {
                        objBO.to = Convert.ToString(dtoutput.Rows[0]["To"]);
                        objBO.cc = Convert.ToString(dtoutput.Rows[0]["CC"]);
                        objBO.subject = Convert.ToString(dtoutput.Rows[0]["Subject"]);
                        objBO.message = Convert.ToString(dtoutput.Rows[0]["Message"]);
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objBO;
        }

        //public QuoteBO RequestForReApprovalDAL(QuoteBO objBO)
        //{
        //    SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
        //    SqlCommand sqlcmd = null;
        //    SqlDataAdapter sqlda = null;
        //    DataTable dtoutput = new DataTable();
        //    try
        //    {
        //        sqlconn.Open();
        //        sqlcmd = new SqlCommand(ResourceFileDAL.sp_quoteStatusChange, sqlconn);
        //        sqlcmd.CommandType = CommandType.StoredProcedure;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.ID, SqlDbType.Int).Value = objBO.QuoteID;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, 200).Value = objBO.Ref_Number;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objBO.Item_Number;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = objBO.role_id;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.changeby, SqlDbType.VarChar, -1).Value = objBO.orderbyId;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.Status, SqlDbType.VarChar, -1).Value = objBO.Status;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.Reason, SqlDbType.VarChar, -1).Value = objBO.Reason;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.MOQ, SqlDbType.VarChar, 100).Value = objBO.MOQ;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.OfferPrice, SqlDbType.VarChar, 100).Value = objBO.Offer_Price;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.MultiOrderFlag, SqlDbType.Int).Value = objBO.MultiOrderFlag;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;


        //        //sqlcmd = new SqlCommand(ResourceFileDAL.sp_RequestForReApproval, sqlconn);
        //        //sqlcmd.CommandType = CommandType.StoredProcedure;
        //        //sqlcmd.Parameters.Add(ResourceFileDAL.ID, SqlDbType.Int).Value = objBO.QuoteID;
        //        //sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, 200).Value = objBO.Ref_Number;
        //        //sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objBO.Item_Number;
        //        //sqlcmd.Parameters.Add(ResourceFileDAL.Requested_By, SqlDbType.VarChar, 100).Value = objBO.orderbyId;  
        //        //sqlcmd.Parameters.Add(ResourceFileDAL.Status, SqlDbType.VarChar, 100).Value = objBO.Status;
        //        //sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
        //        //sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
        //        sqlcmd.ExecuteNonQuery();
        //        objBO.Err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
        //        objBO.Err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);


        //        sqlcmd = new SqlCommand(ResourceFileDAL.sp_getMailDetailsForQuote, sqlconn);
        //        sqlcmd.CommandType = CommandType.StoredProcedure;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.Module, SqlDbType.VarChar, -1).Value = "Quote";
        //        sqlcmd.Parameters.Add(ResourceFileDAL.Component, SqlDbType.VarChar, -1).Value = "ReSubmitQuote";
        //        sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objBO.Ref_Number;
        //        sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objBO.Item_Number;
        //        sqlda = new SqlDataAdapter(sqlcmd);
        //        sqlda.Fill(dtoutput);
        //        if (dtoutput != null)
        //        {
        //            if (dtoutput.Rows.Count > 0)
        //            {
        //                objBO.to = Convert.ToString(dtoutput.Rows[0]["To"]);
        //                objBO.cc = Convert.ToString(dtoutput.Rows[0]["CC"]);
        //                objBO.subject = Convert.ToString(dtoutput.Rows[0]["Subject"]);
        //                objBO.message = Convert.ToString(dtoutput.Rows[0]["Message"]);
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.LogError(ex);
        //    }
        //    finally
        //    {
        //        sqlconn.Close();
        //    }
        //    return objBO;
        //}

        public DataTable GetPendingTasksDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlDataAdapter sqlda = null;
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getPendingQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.StartDate, SqlDbType.VarChar,100).Value = objQuoteBO.StartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.EndDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.role, SqlDbType.VarChar, 10).Value = objQuoteBO.role_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.cter, SqlDbType.VarChar, 10).Value = objQuoteBO.cter;
                sqlcmd.Parameters.Add(ResourceFileDAL.assigned_salesengineer_id, SqlDbType.VarChar, 10).Value = objQuoteBO.user_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetEscalatedQuotesDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlDataAdapter sqlda = null;
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getEscalatedQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.StartDate, SqlDbType.VarChar, 100).Value = objQuoteBO.StartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.EndDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.role, SqlDbType.VarChar, 10).Value = objQuoteBO.role_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.cter, SqlDbType.VarChar, 10).Value = objQuoteBO.cter;
                sqlcmd.Parameters.Add(ResourceFileDAL.assigned_salesengineer_id, SqlDbType.VarChar, 10).Value = objQuoteBO.user_id;
                sqlcmd.CommandTimeout = 100000000;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetIntEscalatedQuotesDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlDataAdapter sqlda = null;
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getInternalEscalatedQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.StartDate, SqlDbType.VarChar, 100).Value = objQuoteBO.StartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.EndDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.role, SqlDbType.VarChar, 10).Value = objQuoteBO.role_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.cter, SqlDbType.VarChar, 10).Value = objQuoteBO.cter;
                sqlcmd.Parameters.Add(ResourceFileDAL.assigned_salesengineer_id, SqlDbType.VarChar, 10).Value = objQuoteBO.user_id;
                sqlcmd.CommandTimeout = 100000000;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }


        public List<QuoteBO> updateQuoteStatusDAL(DataTable dtQuote)
        {
            List<QuoteBO> objList = new List<QuoteBO>();
            QuoteBO objOutput = new QuoteBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda = null;
            DataSet ds = new DataSet();
            DataTable dtoutput = new DataTable();
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_quoteStatusChange, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.tblQuote, dtQuote);
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(ds);

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ds.Tables[0].Columns.Remove("Item");
                            ds.Tables[0].AcceptChanges();
                            DataView view = new DataView(ds.Tables[0]);
                            string[] param = { "Ref_Number", "Quotation_no", "MailTo", "MAilCC", "Subject", "Body", "error_code", "error_msg", "PDF_Flag" };
                            DataTable distinctValues = view.ToTable(true, param);

                            var row = distinctValues.AsEnumerable()
                                .Where(o => o.Field<string>("MailTo") != null || o.Field<string>("MailTo") != "")
                                .Select(o => new QuoteBO
                                {
                                    Ref_Number = o.Field<string>("Ref_Number")
                                    ,
                                    Quotation_no = o.Field<string>("Quotation_no")
                                    ,
                                    to = o.Field<string>("MailTo")
                                    ,
                                    cc = o.Field<string>("MAilCC")
                                    ,
                                    subject = o.Field<string>("Subject")
                                    ,
                                    message = o.Field<string>("Body")
                                    ,
                                    Err_code = o.Field<Int32>("error_code")
                                    ,
                                    Err_msg = o.Field<string>("error_msg")
                                    ,
                                    pdf_flag = o.Field<Int32>("PDF_Flag")
                                }).Distinct().ToList();
                            objList = row;
                        }
                    }
                }
                //sqlcmd.Parameters.Add(ResourceFileDAL.ID, SqlDbType.Int).Value = objQuoteBO.QuoteID;
                //sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, 200).Value = objQuoteBO.Ref_Number;
                //sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                //sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = objQuoteBO.role_id;
                //sqlcmd.Parameters.Add(ResourceFileDAL.changeby, SqlDbType.VarChar, -1).Value = objQuoteBO.ChangeBy;
                //sqlcmd.Parameters.Add(ResourceFileDAL.Status, SqlDbType.VarChar, -1).Value = objQuoteBO.Status;
                //sqlcmd.Parameters.Add(ResourceFileDAL.Reason, SqlDbType.VarChar, -1).Value = objQuoteBO.Reason;
                //sqlcmd.Parameters.Add(ResourceFileDAL.MOQ, SqlDbType.VarChar, 100).Value = objQuoteBO.MOQ;
                //sqlcmd.Parameters.Add(ResourceFileDAL.OfferPrice, SqlDbType.VarChar, 100).Value = objQuoteBO.Offer_Price;
                //sqlcmd.Parameters.Add(ResourceFileDAL.MultiOrderFlag, SqlDbType.Int).Value = objQuoteBO.MultiOrderFlag;
                //sqlcmd.Parameters.Add(ResourceFileDAL.RecommendedPrice, SqlDbType.VarChar, 100).Value = objQuoteBO.RecommendedPrice;
                //sqlcmd.Parameters.Add(ResourceFileDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                //sqlcmd.Parameters.Add(ResourceFileDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;

                //sqlcmd.ExecuteNonQuery();

                //objOutput.Err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.error_code].Value);
                //objOutput.Err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.error_msg].Value);

                //sqlcmd = new SqlCommand(ResourceFileDAL.sp_getMailDetailsForQuote, sqlconn);
                //sqlcmd.CommandType = CommandType.StoredProcedure;
                //sqlcmd.Parameters.Add(ResourceFileDAL.Module, SqlDbType.VarChar, -1).Value = "Quote";
                //sqlcmd.Parameters.Add(ResourceFileDAL.Component, SqlDbType.VarChar, -1).Value = "StatusChange";
                //sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                //sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                //sqlda = new SqlDataAdapter(sqlcmd);
                //sqlda.Fill(dtoutput);
                //if (dtoutput != null)
                //{
                //    if (dtoutput.Rows.Count > 0)
                //    {
                //        objOutput.to = Convert.ToString(dtoutput.Rows[0]["To"]);
                //        objOutput.cc = Convert.ToString(dtoutput.Rows[0]["CC"]);
                //        objOutput.subject = Convert.ToString(dtoutput.Rows[0]["Subject"]);
                //        objOutput.message = Convert.ToString(dtoutput.Rows[0]["Message"]);
                //    }
                //}

            }
            catch (Exception ex)
            {
                objOutput.Err_code = 1;
                objOutput.Err_msg = "There is some error in status update. Please try again.";
                objList.Add(objOutput);
                objCom.LogError(ex);
            }
            return objList;
        }

        public DataTable getQuoteFormatDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getQuoteFormat, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Quotation_no, SqlDbType.VarChar, -1).Value = objQuoteBO.Quotation_no;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, -1).Value = objQuoteBO.Item_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable getQuotePOFormatDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getQuotePO, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, -1).Value = objQuoteBO.Item_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetAgreementPriceDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getAgreementPriceByQTY, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.CustNumber, SqlDbType.VarChar, 100).Value = objQuoteBO.Cust_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.CP_Number, SqlDbType.VarChar, 100).Value = null;
                sqlcmd.Parameters.Add(ResourceFileDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                sqlcmd.Parameters.Add(ResourceFileDAL.Quantity, SqlDbType.VarChar, 100).Value = objQuoteBO.quantity;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }


        public DataTable GetSplQuoteDropdownsDAL(SplQuoteBO objSplQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_GetSplCategories, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Value, SqlDbType.VarChar, -1).Value = objSplQuoteBO.DropdwonValue;
                sqlcmd.Parameters.Add(ResourceFileDAL.DDLNo, SqlDbType.Int).Value = objSplQuoteBO.DropdwonNumber;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetCustomerDetailsDAL(SplQuoteBO objSplQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.getCustomerDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.roleId, SqlDbType.VarChar, 50).Value = "HO";
                sqlcmd.Parameters.Add(ResourceFileDAL.custtype, SqlDbType.VarChar, 10).Value = objSplQuoteBO.CustomerType;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
        public DataTable GetCPCustomerDetailsDAL(SplQuoteBO objSplQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_GetCustomersBasedOnDistributor, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.DISTRIBUTOR, SqlDbType.VarChar, 10).Value = objSplQuoteBO.Distributor;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetOfferPriceSplItemDAL(SplQuoteBO objSplQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.getOfferPriceForSplItem, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.Value1, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value1;
                sqlcmd.Parameters.Add(ResourceFileDAL.Value2, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value2;
                sqlcmd.Parameters.Add(ResourceFileDAL.Value3, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value3;
                sqlcmd.Parameters.Add(ResourceFileDAL.Value4, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value4;
                sqlcmd.Parameters.Add(ResourceFileDAL.Value5, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value5;
                sqlcmd.Parameters.Add(ResourceFileDAL.Value6, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value6;
                sqlcmd.Parameters.Add(ResourceFileDAL.Value7, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value7;
                sqlcmd.Parameters.Add(ResourceFileDAL.Value8, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value8;
                sqlcmd.Parameters.Add(ResourceFileDAL.Value9, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value9;
                sqlcmd.Parameters.Add(ResourceFileDAL.Value10, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value10;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
    }
}
