﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSBA_BusinessObjects;

namespace TSBA_DataAccessLayer
{
    public class CommonDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable GetMasterDropodwnDAL(CommonBO objBO) 
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getMasterDropdowns, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.user_id, SqlDbType.VarChar, 100).Value = objBO.UserId;
                sqlcmd.Parameters.Add(ResourceFileDAL.Flag, SqlDbType.VarChar, 100).Value = objBO.Flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.cter, SqlDbType.VarChar, 100).Value = objBO.cter;
                sqlcmd.Parameters.Add(ResourceFileDAL.branchcode, SqlDbType.VarChar, -1).Value = objBO.branch;
                sqlcmd.Parameters.Add(ResourceFileDAL.assigned_salesengineer_id, SqlDbType.VarChar, -1).Value = objBO.engineer;
                sqlcmd.Parameters.Add(ResourceFileDAL.customerType, SqlDbType.VarChar, -1).Value = objBO.customer_type;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

    }
}
