﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class QuoteDetails : System.Web.UI.Page
    {
        CommonFunctions objCom = new CommonFunctions();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["LoginMailId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                try
                {
                    string id = Convert.ToString(Request.QueryString["id"]);
                    string Escid = Convert.ToString(Request.QueryString["Escid"]);
                    string IEscid = Convert.ToString(Request.QueryString["IEscid"]);
                    if (string.IsNullOrEmpty(id) && string.IsNullOrEmpty(Escid) && string.IsNullOrEmpty(IEscid))
                    {
                        lblmsg.Text = "There is a problem fetching details for selected quote. Please try again.";
                    }
                    if (!string.IsNullOrEmpty(id))
                    {
                        Session["pending_quote_flag"] = "P";
                        DataTable dt = new DataTable();
                        dt = (DataTable)Session["PendingQuotes"];
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                var rows = from row in dt.AsEnumerable()
                                           where row.Field<int>("ID") == Convert.ToInt32(id)
                                           select row;
                                dt = rows.CopyToDataTable();
                                LoadLabels(dt);
                            }

                        }
                    }
                    if (!string.IsNullOrEmpty(Escid))
                    {
                        Session["pending_quote_flag"] = "E";
                        DataTable dt = new DataTable();
                        dt = (DataTable)Session["EscalatedQuotes"];
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                var rows = from row in dt.AsEnumerable()
                                           where row.Field<int>("ID") == Convert.ToInt32(Escid)
                                           select row;
                                dt = rows.CopyToDataTable();
                                LoadLabels(dt);
                            }

                        }
                    }
                    if (!string.IsNullOrEmpty(IEscid))
                    {
                        Session["pending_quote_flag"] = "IE";
                        DataTable dt = new DataTable();
                        dt = (DataTable)Session["IntEscalatedQuotes"];
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                var rows = from row in dt.AsEnumerable()
                                           where row.Field<int>("ID") == Convert.ToInt32(IEscid)
                                           select row;
                                dt = rows.CopyToDataTable();
                                LoadLabels(dt);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    lblmsg.Text = "Error in loading. Please try again.";
                    objCom.LogError(ex);
                }

            }

        }

        private void LoadLabels(DataTable dt)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {
                    hdnitem.Value = Convert.ToString(dt.Rows[0]["Item_code"]);
                    hdnID.Value = Convert.ToString(dt.Rows[0]["ID"]);
                    lblref.Text = Convert.ToString(dt.Rows[0]["Ref_number"]);
                    lblItem.Text = String.Concat(Convert.ToString(dt.Rows[0]["Item_Desc"]));
                    lblWHS.Text = Convert.ToString(dt.Rows[0]["WHS"]);
                    //lblOrderType.Text = Convert.ToString(dt.Rows[0]["Order_type"]);
                    //lblOrderFrequency.Text = Convert.ToString(dt.Rows[0]["Order_frequency"]);
                    lblTotalQty.Text = Convert.ToString(dt.Rows[0]["Total_QTY"]);
                    lblLP.Text = Convert.ToString(dt.Rows[0]["List_Price"]);
                    //lblEP.Text = Convert.ToString(dt.Rows[0]["Expected_price"]);
                    //lblDR.Text = Convert.ToString(dt.Rows[0]["DC_rate"]);
                    lblCP.Text = Convert.ToString(dt.Rows[0]["CP_Name"]);
                    lblCustomer.Text = Convert.ToString(dt.Rows[0]["Cust_Name"]);
                    lblSP.Text = Convert.ToString(dt.Rows[0]["Cust_SP"]);
                    //lblComC.Text = Convert.ToString(dt.Rows[0]["Comp_Name"]);
                    //lblComP.Text = Convert.ToString(dt.Rows[0]["Comp_Desc"]);
                    //lblComSP.Text = Convert.ToString(dt.Rows[0]["Comp_SP"]);
                    //lblReqMOQ.Text = Convert.ToString(dt.Rows[0]["QTY_perOrder"]);
                    lblaveSell.Text = Convert.ToString(dt.Rows[0]["AverageSellingPrice"]);
                    lblUnitPrice.Text = Convert.ToString(dt.Rows[0]["UnitPrice"]);
                    lblStock.Text = Convert.ToString(dt.Rows[0]["Stock"]);
                    lblStockCode.Text = Convert.ToString(dt.Rows[0]["StockCode"]);
                    txtOrdValidity.Text = string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["OrderValidity"]))?"": Convert.ToString(dt.Rows[0]["OrderValidity"]);
                    if (Convert.ToString(Session["RoleId"]) == "HO")
                    {
                        lblHeadingGP.Text = "GP % :";
                        lblGP.Text = Convert.ToString(dt.Rows[0]["GP"]);
                    }
                    //lbltotalOrderValue.Text = Convert.ToString(dt.Rows[0]["TotOrderValue"]);
                    lblOfferPrice.Text = Convert.ToString(dt.Rows[0]["Offer_price"]);
                    if(Convert.ToString(Session["pending_quote_flag"]) == "E")
                    {
                        lblCom_head.Text = "Last Upadated Comment: ";
                        lblComment.Text = Convert.ToString(dt.Rows[0]["StatusChange_Comment"]);
                    }
                    //if (Convert.ToString(Session["RoleId"]) == "BM")
                    //{
                    //    if (Convert.ToString(dt.Rows[0]["SE_Status"]) == "Approved By SE")
                    //    {
                    //        lblCom_head.Text = "COMMENT BY SE: ";
                    //        lblComment.Text = Convert.ToString(dt.Rows[0]["CommentBySE"]);
                    //    }
                    //    else
                    //    {
                    //        lblCom_head.Text = "STATUS BY SE: ";
                    //        lblComment.Text = Convert.ToString(dt.Rows[0]["SE_Status"]);
                    //    }
                    //}
                    //if (Convert.ToString(dt.Rows[0]["StockCode"]) == "1" || Convert.ToString(dt.Rows[0]["StockCode"]) == "6")
                    //{
                    //    txtMOQ.Enabled = false;
                    //}
                    //else
                    //{
                    //    txtMOQ.Enabled = true;
                    //}
                    if(Convert.ToString(Session["RoleId"])=="HO")
                    {
                        txtOrdValidity.Enabled = true;
                        //txtMOQ.Enabled = true;
                        txtOrderQuant.Enabled = true;
                        txtNewOfferPrice.Enabled = true;
                    }
                    else
                    {
                        txtOrdValidity.Enabled = false;
                       // txtMOQ.Enabled = false;
                        txtOrderQuant.Enabled = false;
                        txtNewOfferPrice.Enabled = false;

                    }
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            string flag = Convert.ToString(Session["pending_quote_flag"]);
            ClientScript.RegisterStartupScript(Page.GetType(), "scr", "window.opener.parentFunction('" + hdnparam.Value + "','"+ flag + "'); window.close();", true);
        
    }
    }
}