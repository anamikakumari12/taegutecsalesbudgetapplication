﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class CreateUserManagement : System.Web.UI.Page
    {
        #region GlobalDeclareation
        public string Empnumber, Empname, Empmail, Empusertype, EmpPassword, EmpBranch, EditEmpstatus, editempcontact;
        public int Empstatus;
        public long Empcontact;
        EngineerInfo objEngInfo = new EngineerInfo();
   
        List<string> cssList = new List<string>();
        Review objRSum = new Review();
        #endregion
        public static string cter;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {
                //if (Session["cter"] == null)
                //{
                //    Session["cter"] = "TTA";
                //    cter = "TTA";


                //}
                //else if (Session["cter"].ToString() == "DUR")
                //{
                //    cter = "DUR";

                //}
                //else
                //{
                //    cter = "TTA";
                //}
                LoadBranches();
                LoadBranchesforBranchmanager();
                LoadEngineerInfo();
            }
            if (ViewState["GridData"] != null)
            {
                DataTable dtData = ViewState["GridData"] as DataTable;
                if (dtData.Rows.Count != 0)
                {
                    GridEngInfo.DataSource = dtData;
                    GridEngInfo.DataBind();
                }
            }
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);
        }

        

        protected void GridEngInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string data = "";
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string c = (e.Row.FindControl("lbl_branch") as Label).Text;
                string[] lines = Regex.Split(c, ",");
                foreach (var item in lines)
                {
                    PlaceHolder emails = e.Row.FindControl("ph_Region") as PlaceHolder;

                    data = item.ToString();
                    Label tags = new Label();
                    tags.EnableViewState = true;
                    tags.Text = data;
                    emails.Controls.Add(tags);
                    if (lines.Length != 1)
                    {
                        emails.Controls.Add(new LiteralControl("<br />"));
                    }
                }
            }
        }

        #region Methods
        protected void LoadEngineerInfo()
        {
            DataTable dt = new DataTable();
            dt = objEngInfo.getUserDetails();
            Session.Add("dt", dt);
            ViewState["GridData"] = null;
            ViewState["GridData"] = dt;
            GridEngInfo.DataSource = dt;
            GridEngInfo.DataBind();
        }

        [WebMethod]
        public static string Add_Click(string EngId, string EngName, string EngEmail, string status,string contactNum,string Role,string usermapped,string nousermapped,string branch)
        {
            string res = "";
            EngineerInfo objEngInfo = new EngineerInfo();
            LoginAuthentication objauth = new LoginAuthentication();
            DataTable Tissues =HttpContext.Current.Session["dt"] as DataTable;
            //string pwd = PasswordSecurityGenerate.Generate(8, 8);
            string pwd = "Z6HePKN/oxTiAwtLKBMm4A==";
           string EmpPassword = objauth.Decrypt(pwd);
           long Empcontact = string.IsNullOrEmpty(contactNum) ? 0 : Convert.ToInt64(contactNum);
            bool exists = Tissues.AsEnumerable().Where(c => c.Field<string>("EmailId").Equals(EngEmail)).Count() > 0;
            if (exists)
            {
                res = "{\"msg\":\"EmailId Exists\"}";
            }
            else
            {
                objEngInfo.CreateUserDet(EngId, EngName, EngEmail, status, Empcontact, Role, usermapped, nousermapped,branch);
                try
                {
                    MailMessage email = new MailMessage();
                    string pwd1 = pwd;
                    email.To.Add(new MailAddress(EngEmail)); //Destination Recipient e-mail address.
                    email.Subject = " Welcome  To Sales-Budget & Performance Monitoring";//Subject for your request
                    email.Body = " <br/><br/>Your Username: " + EngEmail + "<br/><br/>Your Password: " + EmpPassword + "<br/><br/>" + "From" + "<br/>" + "IT Team - TaeguTec";
                    email.IsBodyHtml = true;
                    SmtpClient smtpc = new SmtpClient();
                    smtpc.Send(email);
                }
                catch (Exception Ex)
                {
                    res = "{\"msg\":\"Failed to send email\"}";
                }
            }
            
            return res;
        }
        
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            DataTable dtData = new DataTable();
            objRSum.roleId = "HO";
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);


            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);

        }

        protected void LoadBranchesforBranchmanager()
        {
            DataTable dtBranchesList = new DataTable();
            dtBranchesList = objEngInfo.LoadUserInfo(0, cter);

            ddlBranch.DataSource = dtBranchesList;
            ddlBranch.DataTextField = "region_description";
            ddlBranch.DataValueField = "region_code";
            ddlBranch.DataBind();
            ddlBranch.Items.Insert(0, "--Select the Branch--");
        }

        
        #endregion
    }
}