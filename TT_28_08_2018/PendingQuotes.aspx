﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PendingQuotes.aspx.cs" Inherits="TaegutecSalesBudget.PendingQuotes" MasterPageFile="~/Site.Master" %>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css" />
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
    <script src="http://cdn.datatables.net/plug-ins/1.10.24/sorting/date-uk.js"></script>

    <link href="css/Tabs.css" rel="stylesheet" />

    <style>
        .disabled {
            background-color: #e4e4e4 !important;
            border: 1px solid #aaa !important;
            border-radius: 4px !important;
            cursor: default !important;
            float: left !important;
            /* margin-right: 5px; */
            /* margin-top: 5px; */
            padding: 0 5px !important;
        }

        .btncontrol {
            padding-left: 5px;
            padding-top: 10px;
        }

        .btn.green {
            margin-top: 10px;
            padding: 5px;
        }

        .gridbtn {
            width: auto !important;
        }

        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px !important;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

        .control {
            padding-top: 2px;
        }

        .popupControl {
            margin: 5px;
            float: right;
        }

        th,
        td {
            white-space: nowrap;
        }

        div.dataTables_wrapper {
            /*width: 800px;*/
            margin: 0 auto;
        }

        .loader_div {
            position: fixed;
            top: 0;
            bottom: 0%;
            left: 0;
            right: 0%;
            z-index: 99;
            opacity: 0.7;
            display: none;
            background: lightgrey url('../../../images/loader.gif') center center no-repeat;
        }
    </style>
    <script type="text/javascript">
        //window.onload = function () {
        //    var fileupload = document.getElementById("FileUpload1");
        //    var filePath = document.getElementById("spnFilePath");
        //    var button = document.getElementById("btnFileUpload");
        //    button.onclick = function () {
        //        fileupload.click();
        //    };
        //    fileupload.onchange = function () {
        //        var fileName = fileupload.value.split('\\')[fileupload.value.split('\\').length - 1];
        //        filePath.innerHTML = "<b>Selected File: </b>" + fileName;
        //    };
        //};
        $(document).ready(function () {
            debugger;
            var roleId = '<%= Session["RoleId"] %>';
            $('#MainContent_btnMultiApprove').hide();
            $('.checkbox').on('change', function (evt, obj) {
                debugger;
                var id = this.children[0].id.replace('chk_multi', 'hdn_chk_multi');
                if ($('#' + id).val() == "0")
                    $('#' + id).val("1");
                else
                    $('#' + id).val("0");
                var totaltrCount = $("[id*=grdPendingQuote] tr").length;
                var trCount = $("[id*=grdPendingQuote] td").closest("tr").length;
                var multi_btn_flag = 0;
                var chk_id;
                for (i = 0; i < trCount; i++) {
                    chk_id = "MainContent_grdPendingQuote_hdn_chk_multi_" + i;
                    if ($('#' + chk_id).val() == "1") {
                        multi_btn_flag++;
                        break;
                    }
                }
                if (roleId == "HO") {
                    $('#MainContent_btnMultiApprove').val("Approve");
                    $("#MainContent_btnMultiApprove").attr('style', 'width:10%;');
                }
                else {
                    $('#MainContent_btnMultiApprove').val("Forward to Next Level");
                    $("#MainContent_btnMultiApprove").attr('style', 'width:10%;');
                }
                if (multi_btn_flag > 0) {

                    $('#MainContent_btnMultiApprove').show();
                }
                else {
                    $('#MainContent_btnMultiApprove').hide();
                }

            })

            $(".files").change(function (e) {
                debugger;
                //$("#FileUpload1").get(0)
                //var id = this.id.replace("file", "");
                //var div_id = this.id.replace("file", "filename");

                var files = e.target.files[0];
                e.target.parentElement.parentElement.children[2].children[2].innerText = files.name;
                
            });
            
            $(".filesHO").change(function (e) {
                debugger;
                //$("#FileUpload1").get(0)
                //var id = this.id.replace("file", "");
                //var div_id = this.id.replace("file", "filename");

                var files = e.target.files[0];
                e.target.parentElement.parentElement.children[2].children[2].innerText = files.name;

            });
            //var totaltrCount = $("[id*=grdEscalatedQuote] tr").length;
            //var trCount = $("[id*=grdEscalatedQuote] td").closest("tr").length;
            //var date_id, hdn_date_id;
            //for (i = 0; i < trCount; i++) {
            //    date_id = "MainContent_grdEscalatedQuote_txtEscExpiryDate_" + i;
            //    hdn_date_id = "MainContent_grdEscalatedQuote_hdnEscExpiryDate_" + i;
            //    var defDate = $('#' + hdn_date_id).val();
            //    debugger;
            //    $('#' + date_id).daterangepicker({
            //        singleDatePicker: true,
            //        startDate: defDate,
            //        endDate: null,
            //        locale: {
            //            format: 'MM/DD/YYYY'
            //        },
            //        showDropdowns: true,
            //    }, function (start, end, label) {
            //        debugger;
            //        var id = this.element[0].id;
            //        var hdnid = this.element[0].id.replace("txtEscExpiryDate", "hdnEscExpiryDate");
            //        $('#' + hdnid).val(start.format('MM/DD/YYYY'));
            //        var name = this.element[0].name;
            //        $('body').find("input[name^='" + name + "']").val(start.format('MM/DD/YYYY'));
            //    });
            //}
            //totaltrCount = $("[id*=grdPendingQuote] tr").length;
            //trCount = $("[id*=grdPendingQuote] td").closest("tr").length;
            //date_id, hdn_date_id;
            //for (i = 0; i < trCount; i++) {
            //    date_id = "MainContent_grdPendingQuote_txtExpiryDate_" + i;
            //    hdn_date_id = "MainContent_grdPendingQuote_hdnExpiryDate_" + i;
            //    var defDate = $('#' + hdn_date_id).val();
            //    debugger;
            //    $('#' + date_id).daterangepicker({
            //        singleDatePicker: true,
            //        startDate: defDate,
            //        endDate: null,
            //        locale: {
            //            format: 'MM/DD/YYYY'
            //        },
            //        showDropdowns: true,
            //    }, function (start, end, label) {
            //        debugger;
            //        var id = this.element[0].id;
            //        var hdnid = this.element[0].id.replace("txtExpiryDate", "hdnExpiryDate");
            //        $('#' + hdnid).val(start.format('MM/DD/YYYY'));
            //        var name = this.element[0].name;
            //        $('body').find("input[name^='" + name + "']").val(start.format('MM/DD/YYYY'));
            //    });
            //}
            LoadTable();
            if (roleId == "HO") {


                $(".approve").attr('title', 'Approve');
                $(".reject").attr('title', 'Reject');
                $("#MainContent_grdPendingQuote_lblComment").text("Comment");
                $(".Escapprove").attr('title', 'Approve');
                $(".Escreject").attr('title', 'Reject');
                $(".internal").attr('title', 'Send for Reconsideration');
            }
            else {

                $(".approve").attr('title', 'Yes');
                $(".reject").attr('title', 'No');
                $("#MainContent_grdPendingQuote_lblComment").text("Recommendation");
                $(".Escapprove").attr('title', 'Yes');
                $(".Escreject").attr('title', 'No');
                
            }
        });

        function LoadTable() {
            var roleId = '<%= Session["RoleId"] %>';
            var SelectedStart = sessionStorage.getItem("selectedPendingStart");
            var SelectedEnd = sessionStorage.getItem("selectedPendingEnd");
            var start = (SelectedStart == null ? "'07/20/2019" : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            $('#MainContent_txtDateRange').daterangepicker({
                autoUpdateInput: true,
                locale: {
                    format: 'MM/DD/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['07/20/2019', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                var start = start.format('MM/DD/YYYY');
                var end = end.format('MM/DD/YYYY');
                sessionStorage.setItem('selectedPendingStart', start);
                sessionStorage.setItem('selectedPendingEnd', end);
            });

            var head_content1 = $('#MainContent_grdEscalatedQuote tr:first').html();
            $('#MainContent_grdEscalatedQuote').prepend('<thead></thead>')
            $('#MainContent_grdEscalatedQuote thead').html('<tr>' + head_content1 + '</tr>');
            $('#MainContent_grdEscalatedQuote tbody tr:first').hide();
            if (roleId == "HO") {
                var table1 = $('#MainContent_grdEscalatedQuote').dataTable({

                    //"columns": [{ type: 'date-uk', targets: 0 }                ],
                    //"order": [[1, "asc"]]
                    "order": [[15, 'desc']],
                    scrollY: "350px",
                    scrollX: true,
                    scrollCollapse: true,
                    //fixedColumns: true,
                    //fixedColumns: {
                    //    leftColumns: 1,
                    //    rightColumns: 4
                    //},
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    dom: 'lBfrtip',
                    columnDefs: [
                        { width: 300, targets: 0 },
                        { type: 'date-uk', targets: 15 }
                    ],
                    fixedColumns: true
                });
            }
            else {
                var table1 = $('#MainContent_grdEscalatedQuote').dataTable({

                    //"columns": [{ type: 'date-uk', targets: 0 }                ],
                    //"order": [[1, "asc"]]
                    "order": [[11, 'desc']],
                    scrollY: "350px",
                    scrollX: true,
                    scrollCollapse: true,
                    //fixedColumns: true,
                    //fixedColumns: {
                    //    leftColumns: 1,
                    //    rightColumns: 4
                    //},
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    dom: 'lBfrtip',
                    columnDefs: [
                        { width: 300, targets: 0 },
                        { type: 'date-uk', targets: 11 }
                    ],
                    fixedColumns: true
                });
            }
            $('#MainContent_btnMultiApprove').hide();

        }

        function validateMultiple(evt, obj) {
            debugger;
            var totaltrCount = $("[id*=grdPendingQuote] tr").length;
            var trCount = $("[id*=grdPendingQuote] td").closest("tr").length;
            var errorFlag = 0;
            var output;
            var id;
            var chk_id;
            var err_msg = [];
            var obj = [];
            for (i = 0; i < trCount; i++) {
                id = "MainContent_grdPendingQuote_btnApprove_" + i;
                chk_id = "MainContent_grdPendingQuote_hdn_chk_multi_" + i;
                if ($('#' + chk_id).val() == "1") {
                    output = validateFieldsbyId(id);
                    errorFlag += output.errFlag;
                    if (output.errFlag == 0) {
                        obj.push(output.json);
                    }
                    else {
                        err_msg.push(output.err_comment);
                    }
                }
            }
            debugger;
            if (errorFlag > 0)
                return false;
            else {
                var param = "{objList:" + JSON.stringify(obj) + "}";
                $.ajax({
                    url: 'PendingQuotes.aspx/UpdateQuoteStatus',
                    method: 'post',
                    datatype: 'json',
                    data: param,
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        window.location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Error");
                    }
                });
            }
        }

        function validateFieldsbyId(id) {
            var errFlag = 0, comment_id, lbl, offerprice_id, ref_id, refnum_id, item_id, MOQ_id, multiflag_id, status, RMOQ_id, MOQ, RMOQ, lblMOQmsg_id, stock_id, expiry_date;
            var err_comment;
            var lblpricemsg_id;
            var txtRecPrice_id;
            var lblTargetPrice_id;
            var lblrecpricemsg_id;
            var RecPrice;
            var oldofferprice_id, oldofferprice;
            var expiry_id;
            var offerprice;
            var txtOrdValidity_id, txtOrderQuant_id, txtOrderQuant, txtOrdValidity;
            var roleId = '<%= Session["RoleId"] %>';
            if (id.includes("btnReject")) {
                comment_id = id.replace("btnReject", "txtComment");
                lbl = id.replace("btnReject", "lblmsg");
                offerprice_id = id.replace("btnReject", "txtNewOfferPrice");
                ref_id = id.replace("btnReject", "hdnID");
                refnum_id = id.replace("btnReject", "lblref");
                item_id = id.replace("btnReject", "lblitem");
                MOQ_id = id.replace("btnReject", "txtMOQ");
                RMOQ_id = id.replace("btnReject", "lblQTYPO");
                multiflag_id = id.replace("btnReject", "chkMulti");
                txtRecPrice_id = id.replace("btnReject", "txtRecPrice");
                RecPrice = $("#" + txtRecPrice_id).val();
                expiry_id = id.replace("btnReject", "txtExpiryDate");
                MOQ = $("#" + MOQ_id).val();
                if (IsnullOrEmpty(RecPrice)) {
                    RecPrice = "";
                }
                if (IsnullOrEmpty(MOQ)) {
                    MOQ = "";
                }
                offerprice = $("#" + offerprice_id).val();
                if (IsnullOrEmpty(offerprice)) {
                    offerprice = "";
                }
                if (IsnullOrEmpty($("#" + comment_id).val())) {
                    $("#" + comment_id).css("border", "1px solid red");
                    $("span#" + lbl).text("*");
                    err_comment = "*Comment is mandatory.";
                    errFlag++;
                }
                else {
                    $("span#" + lbl).text("");
                    $("#" + comment_id).css("border", "");
                    status = "Rejected";
                }

            }
            else if (id.includes("btnApprove")) {
                //lbl = id.replace("btnApprove", "lblpricemsg");

                comment_id = id.replace("btnApprove", "txtComment");
                offerprice_id = id.replace("btnApprove", "txtNewOfferPrice");
                oldofferprice_id = id.replace("btnApprove", "txtOfferPrice");
                ref_id = id.replace("btnApprove", "hdnID");
                refnum_id = id.replace("btnApprove", "lblref");

                item_id = id.replace("btnApprove", "lblitem");
                MOQ_id = id.replace("btnApprove", "txtMOQ");
                RMOQ_id = id.replace("btnApprove", "lblQTYPO");
                multiflag_id = id.replace("btnApprove", "chkMulti");
                MOQ = $("#" + MOQ_id).val();
                RMOQ = $("#" + RMOQ_id).val();
                lblMOQmsg_id = id.replace("btnApprove", "lblMOQmsg");
                lblpricemsg_id = id.replace("btnApprove", "lblpricemsg");
                txtRecPrice_id = id.replace("btnApprove", "txtRecPrice");
                lblTargetPrice_id = id.replace("btnApprove", "lblTargetPrice");
                lblrecpricemsg_id = id.replace("btnApprove", "lblrecpricemsg");
                lbl = id.replace("btnApprove", "lblmsg");
                offerprice = $("#" + offerprice_id).val();
                var TargetPrice = $("span#" + lblTargetPrice_id).text();
                RecPrice = $("#" + txtRecPrice_id).val();
                oldofferprice = $("#" + oldofferprice_id).val();
                expiry_id = id.replace("btnApprove", "txtExpiryDate");
                stock_id = id.replace("btnApprove", "hdnStockCode");
                var stock = $('#' + stock_id).val();

                txtOrdValidity_id = id.replace("btnApprove", "txtOrdValidity");
                txtOrderQuant_id = id.replace("btnApprove", "txtOrderQuant");
                // ddlOrderFreq_id = id.replace("btnApprove", "ddlEscOrderFreq");
                txtOrderQuant = $("#" + txtOrderQuant_id).val();
                txtOrdValidity = $("#" + txtOrdValidity_id).val();

                if (roleId == "BM" || roleId == "TM") {

                    if (!IsnullOrEmpty(RecPrice)) {
                        if (parseFloat(parseFloat(TargetPrice).toFixed(2)) > parseFloat(parseFloat(RecPrice).toFixed(2))) {
                            //  if (parseFloat(TargetPrice) > parseFloat(RecPrice)) {
                            $("#" + txtRecPrice_id).css("border", "1px solid red");
                            $("span#" + lblrecpricemsg_id).text("*");
                            //$("#MainContent_lblErrMsg").text("*Price should be equal to or greater than Expected Price.");
                            err_comment = "*Price should be equal to or greater than Expected Price.";
                            errFlag++;
                        }
                        else {
                            $("#" + txtRecPrice_id).css("border", "");
                            $("span#" + lblrecpricemsg_id).text("");
                        }
                    }
                    else {
                        RecPrice = "";
                        $("#" + txtRecPrice_id).css("border", "");
                        $("span#" + lblrecpricemsg_id).text("");
                    }
                    if (IsnullOrEmpty($("#" + comment_id).val())) {
                        $("#" + comment_id).css("border", "1px solid red");
                        // $("span#" + lbl).text("*Required");
                        err_comment = "*Comment is mandatory.";
                        errFlag++;
                    }
                    else {
                        //$("span#" + lbl).text("");
                        $("#" + comment_id).css("border", "");
                    }
                }
                if (roleId == "SE") {
                    if (IsnullOrEmpty($("#" + comment_id).val())) {
                        $("#" + comment_id).css("border", "1px solid red");
                        // $("span#" + lbl).text("*Required");
                        err_comment = "*Comment is mandatory.";
                        errFlag++;
                    }
                    else {
                        //$("span#" + lbl).text("");
                        $("#" + comment_id).css("border", "");
                    }
                }
                if (roleId == "HO") {
                    if (IsnullOrEmpty(txtOrderQuant)) {
                        $("#" + txtOrderQuant_id).css("border", "1px solid red");
                        err_comment = "*Break quantity is required.";
                        errFlag++;
                    }
                    else {
                        $("#" + txtOrderQuant_id).css("border", "");
                    }
                    if (IsnullOrEmpty(MOQ)) {
                        if (stock == "1" || stock == "6") {
                            MOQ = RMOQ;
                            $("#" + MOQ_id).css("border", "");
                            //$("span#" + lblMOQmsg_id).text("");
                        }
                        else {
                            //$("span#" + lblMOQmsg_id).text("*");
                            $("#" + MOQ_id).css("border", "1px solid red");
                            err_comment = "*MOQ is mandatory.";
                            errFlag++;

                        }
                    }
                    else if (parseInt(MOQ) > parseInt(txtOrderQuant)) {
                        $("#" + MOQ_id).css("border", "1px solid red");
                        //$("span#" + lblMOQmsg_id).text("*");
                        err_comment = "*Break quantity should be equal to or greater than MOQ.";
                        errFlag++;
                    }
                    else {
                        //$("span#" + lblMOQmsg_id).text("");
                        $("#" + MOQ_id).css("border", "");
                    }



                    //else {
                    //    $("span#" + lblMOQmsg_id).text("");
                    //}
                }
                else {
                    if (IsnullOrEmpty(MOQ)) {
                        MOQ = "";
                    }
                    $("span#" + lblMOQmsg_id).text("");
                }
                if (roleId == "HO") {
                    expiry_date = $("#" + expiry_id).val();
                    if (IsnullOrEmpty($("#" + offerprice_id).val())) {
                        if (IsnullOrEmpty(RecPrice) && IsnullOrEmpty(oldofferprice)) {
                            $("#" + offerprice_id).css("border", "1px solid red");
                            $("span#" + lblpricemsg_id).text("*");
                            //$("#MainContent_lblErrMsg").text("*Required");
                            err_comment = "*Required";
                            errFlag++;
                        }
                        else if (IsnullOrEmpty(RecPrice)) {
                            $("#" + offerprice_id).css("border", "1px solid red");
                            $("span#" + lblpricemsg_id).text("*");
                            //$("#MainContent_lblErrMsg").text("*Required");
                            err_comment = "*Required";
                            errFlag++;
                        }
                        else if (IsnullOrEmpty(oldofferprice)) {
                            offerprice = RecPrice;
                            $("#" + offerprice_id).css("border", "");
                            $("span#" + lblpricemsg_id).text("");
                            status = "Approved";
                        }

                    }
                    else {
                        if (parseFloat(parseFloat(TargetPrice).toFixed(2)) > parseFloat(parseFloat(offerprice).toFixed(2))) {
                            // if (parseFloat(TargetPrice) > parseFloat(offerprice)) {
                            $("#" + offerprice_id).css("border", "1px solid red");
                            $("span#" + lblpricemsg_id).text("*");
                            //$("#MainContent_lblErrMsg").text("*Price should be equal to or greater than Expected Price.");
                            err_comment = "*Price should be equal to or greater than Expected Price.";
                            errFlag++;
                        }
                        else {
                            $("#" + offerprice_id).css("border", "");
                            $("span#" + lblpricemsg_id).text("");
                            status = "Approved";
                        }

                    }
                }
                else {
                    if (IsnullOrEmpty($("#" + offerprice_id).val())) {
                        offerprice = "";
                    }
                    if (roleId == "SE") {
                        status = "Approved By SE";
                    }
                    else {
                        status = "Approved By BM";
                    }
                }


            }
            var json;
            if (errFlag > 0) {
                $("#MainContent_lblErrMsg").text(err_comment);
            }
            else {
                var ref = $("#" + ref_id).val();
                var refnum = $("#" + refnum_id).val();
                var item = $("#" + item_id).text();

                var multiflag;
                if ($("#" + multiflag_id).is(':checked'))
                    multiflag = 1;
                else
                    multiflag = 0;
                var offerprice = $("#" + offerprice_id).val();
                var comment = $("#" + comment_id).val();

                json = {
                    "id": ref
                    , "item": item
                    , "ref_no": refnum
                    , "status": status
                    , "comment": comment
                    , "MOQ": MOQ
                    , "offerPrice": offerprice
                    , "multiorder_flag": multiflag
                    , "RecPrice": RecPrice
                    , "expiry_date": expiry_date
                };
                $("#MainContent_lblErrMsg").text("");
            }

            var output = {
                "errFlag": errFlag,
                "err_comment": err_comment,
                "json": json
            };

            return output;
        }

        function validateFields(evt, obj) {
            debugger;
            jQuery(".loader_div").show();
            $("#MainContent_lblErrMsg").text("");
            var errFlag = 0;
            var id = obj.id;
            var comment_id;
            var lbl;
            var offerprice_id;
            var ref_id;
            var refnum_id;
            var item_id;
            var MOQ_id;
            var RMOQ_id;
            var multiflag_id;
            var status;
            var MOQ;
            var RMOQ;
            var lblMOQmsg_id;
            var offerprice;
            var lblpricemsg_id;
            var txtRecPrice_id;
            var lblTargetPrice_id;
            var lblrecpricemsg_id;
            var RecPrice;
            var oldofferprice_id;
            var oldofferprice;
            var stock_id;
            var expiry_id;
            var expiry_date;
            var stock;

            var txtOrdValidity_id;
            var txtOrderQuant_id;
            var ddlOrderFreq_id;
            var txtOrderQuant;
            var txtOrdValidity;
            var roleId = '<%= Session["RoleId"] %>';
            if (id.includes("btnReject")) {
                comment_id = id.replace("btnReject", "txtComment");
                lbl = id.replace("btnReject", "lblmsg");
                offerprice_id = id.replace("btnReject", "txtNewOfferPrice");
                ref_id = id.replace("btnReject", "hdnID");
                refnum_id = id.replace("btnReject", "lblref");
                item_id = id.replace("btnReject", "lblitem");
                MOQ_id = id.replace("btnReject", "txtMOQ");
                RMOQ_id = id.replace("btnReject", "lblQTYPO");
                multiflag_id = id.replace("btnReject", "chkMulti");
                txtRecPrice_id = id.replace("btnReject", "txtRecPrice");
                RecPrice = $("#" + txtRecPrice_id).val();
                expiry_id = id.replace("btnReject", "txtExpiryDate");
                if (IsnullOrEmpty(RecPrice)) {
                    RecPrice = "";
                }
                MOQ = $("#" + MOQ_id).val();
                if (IsnullOrEmpty(MOQ)) {
                    MOQ = "";
                }
                offerprice = $("#" + offerprice_id).val();
                if (IsnullOrEmpty(offerprice)) {
                    offerprice = "";
                }
                if (IsnullOrEmpty($("#" + comment_id).val())) {
                    $("#" + comment_id).css("border", "1px solid red");
                    $("span#" + lbl).text("*Required");
                    errFlag++;
                }
                else {
                    $("span#" + lbl).text("");
                    $("#" + comment_id).css("border", "");
                    status = "Rejected";
                }
            }
            else if (id.includes("btnApprove")) {
                comment_id = id.replace("btnApprove", "txtComment");
                offerprice_id = id.replace("btnApprove", "txtNewOfferPrice");
                oldofferprice_id = id.replace("btnApprove", "txtOfferPrice");
                ref_id = id.replace("btnApprove", "hdnID");
                refnum_id = id.replace("btnApprove", "lblref");
                item_id = id.replace("btnApprove", "lblitem");
                MOQ_id = id.replace("btnApprove", "txtMOQ");
                RMOQ_id = id.replace("btnApprove", "lblQTYPO");
                multiflag_id = id.replace("btnApprove", "chkMulti");
                MOQ = $("#" + MOQ_id).val();
                RMOQ = $("#" + RMOQ_id).text();
                lblMOQmsg_id = id.replace("btnApprove", "lblMOQmsg");
                lblpricemsg_id = id.replace("btnApprove", "lblpricemsg");
                txtRecPrice_id = id.replace("btnApprove", "txtRecPrice");
                lblTargetPrice_id = id.replace("btnApprove", "lblTargetPrice");
                lblrecpricemsg_id = id.replace("btnApprove", "lblrecpricemsg");
                lbl = id.replace("btnApprove", "lblmsg");
                offerprice = $("#" + offerprice_id).val();
                var TargetPrice = $("span#" + lblTargetPrice_id).text();
                RecPrice = $("#" + txtRecPrice_id).val();
                oldofferprice = $("#" + oldofferprice_id).val();
                expiry_id = id.replace("btnApprove", "txtExpiryDate");
                stock_id = id.replace("btnApprove", "hdnStockCode");
                stock = $('#' + stock_id).val();

                txtOrdValidity_id = id.replace("btnApprove", "txtOrdValidity");
                txtOrderQuant_id = id.replace("btnApprove", "txtOrderQuant");
                // ddlOrderFreq_id = id.replace("btnApprove", "ddlEscOrderFreq");
                txtOrderQuant = $("#" + txtOrderQuant_id).val();
                txtOrdValidity = $("#" + txtOrdValidity_id).val();

                if (roleId == "BM" || roleId == "TM") {

                    if (!IsnullOrEmpty(RecPrice)) {
                        if (parseFloat(parseFloat(TargetPrice).toFixed(2)) > parseFloat(parseFloat(RecPrice).toFixed(2))) {
                            // if (parseFloat(TargetPrice) > parseFloat(RecPrice)) {
                            $("#" + txtRecPrice_id).css("border", "1px solid red");
                            $("span#" + lblrecpricemsg_id).text("*");
                            $("#MainContent_lblErrMsg").text("*Price should be equal to or greater than Expected Price.");
                            errFlag++;
                        }
                        else {
                            $("#" + txtRecPrice_id).css("border", "");
                            $("span#" + lblrecpricemsg_id).text("");
                        }
                    }
                    else {
                        RecPrice = "";
                        $("#" + txtRecPrice_id).css("border", "");
                        $("span#" + lblrecpricemsg_id).text("");
                    }
                    if (IsnullOrEmpty($("#" + comment_id).val())) {
                        $("#" + comment_id).css("border", "1px solid red");
                        $("span#" + lbl).text("*Required");
                        errFlag++;
                    }
                    else {
                        $("span#" + lbl).text("");
                        $("#" + comment_id).css("border", "");
                    }
                }
                else if (roleId == "SE") {
                    if (IsnullOrEmpty($("#" + comment_id).val())) {
                        $("#" + comment_id).css("border", "1px solid red");
                        $("span#" + lbl).text("*Required");
                        errFlag++;
                    }
                    else {
                        $("span#" + lbl).text("");
                        $("#" + comment_id).css("border", "");
                    }
                }
                if (roleId == "HO") {
                    if (IsnullOrEmpty(txtOrderQuant)) {
                        $("#" + txtOrderQuant_id).css("border", "1px solid red");
                        $("#MainContent_lblErrMsg").text("*Break quantity is required.");
                        errFlag++;
                    }
                    else {
                        $("#" + txtOrderQuant_id).css("border", "");
                    }
                    if (IsnullOrEmpty(MOQ)) {
                        if (stock == "1" || stock == "6") {
                            MOQ = RMOQ;
                            $("#" + MOQ_id).css("border", "");
                            //$("span#" + lblMOQmsg_id).text("");
                        }
                        else {
                            //$("span#" + lblMOQmsg_id).text("*");
                            $("#" + MOQ_id).css("border", "1px solid red");
                            $("#MainContent_lblErrMsg").text("*MOQ is mandatory.");
                            errFlag++;

                        }
                    }
                    else if (parseInt(MOQ) > parseInt(txtOrderQuant)) {
                        $("#" + MOQ_id).css("border", "1px solid red");
                        //$("span#" + lblMOQmsg_id).text("*");
                        $("#MainContent_lblEscresult").text("*Break quantity should be equal to or greater than MOQ.");
                        errFlag++;
                    }
                    else {
                        //$("span#" + lblMOQmsg_id).text("");
                        $("#" + MOQ_id).css("border", "");
                    }
                    //if (IsnullOrEmpty(MOQ)) {
                    //    MOQ = RMOQ;
                    //    $("span#" + lblMOQmsg_id).text("");
                    //}


                }
                else {
                    if (IsnullOrEmpty(MOQ)) {
                        MOQ = "";
                    }
                    $("span#" + lblMOQmsg_id).text("");
                }
                if (roleId == "HO") {
                    expiry_date = $("#" + expiry_id).val();
                    if (IsnullOrEmpty($("#" + offerprice_id).val())) {
                        if (IsnullOrEmpty(RecPrice) && IsnullOrEmpty(oldofferprice)) {
                            $("#" + offerprice_id).css("border", "1px solid red");
                            $("span#" + lblpricemsg_id).text("*");
                            $("#MainContent_lblErrMsg").text("*Required");
                            errFlag++;
                        }
                        else if (IsnullOrEmpty(RecPrice)) {
                            $("#" + offerprice_id).css("border", "1px solid red");
                            $("span#" + lblpricemsg_id).text("*");
                            $("#MainContent_lblErrMsg").text("*Required");
                            errFlag++;
                        }
                        else if (IsnullOrEmpty(oldofferprice)) {
                            offerprice = RecPrice;
                            $("#" + offerprice_id).css("border", "");
                            $("span#" + lblpricemsg_id).text("");
                            status = "Approved";
                        }

                    }
                    else {
                        if (parseFloat(parseFloat(TargetPrice).toFixed(2)) > parseFloat(parseFloat(offerprice).toFixed(2))) {
                            // if (parseFloat(TargetPrice) > parseFloat(offerprice)) {
                            $("#" + offerprice_id).css("border", "1px solid red");
                            $("span#" + lblpricemsg_id).text("*");
                            $("#MainContent_lblErrMsg").text("*Price should be equal to or greater than Expected Price.");
                            errFlag++;
                        }
                        else {
                            $("#" + offerprice_id).css("border", "");
                            $("span#" + lblpricemsg_id).text("");
                            status = "Approved";
                        }

                    }
                }
                else {
                    if (IsnullOrEmpty($("#" + offerprice_id).val())) {
                        offerprice = "";
                    }
                    if (roleId == "SE") {
                        status = "Approved By SE";
                    }
                    else {
                        status = "Approved By BM";
                    }
                }

            }
            if (errFlag > 0) {

                jQuery(".loader_div").hide();
                return false;
            }
            else {
                var ref = $("#" + ref_id).val();
                var refnum = $("#" + refnum_id).val();
                var item = $("#" + item_id).text();
                var multiflag;
                if ($("#" + multiflag_id).is(':checked'))
                    multiflag = 1;
                else
                    multiflag = 0;

                var comment = $("#" + comment_id).val();

                var objList = [];
                var obj = {
                    "id": ref
                    , "item": item
                    , "ref_no": refnum
                    , "status": status
                    , "comment": comment
                    , "MOQ": MOQ
                    , "offerPrice": offerprice
                    , "multiorder_flag": multiflag
                    , "RecPrice": RecPrice
                    , "expiry_date": expiry_date
                };
                objList.push(obj);
                var param = "{objList:" + JSON.stringify(objList) + "}";
                $.ajax({
                    url: 'PendingQuotes.aspx/UpdateQuoteStatus',
                    method: 'post',
                    datatype: 'json',
                    data: param,
                    //data: '{id:"' + ref + '", item:"' + item + '", ref_no:"' + refnum + '", status:"' + status + '", comment:"' + comment + '", MOQ:"' + MOQ + '", offerPrice:"' + offerprice + '", multiorder_flag:' + multiflag + ', RecPrice: "' + RecPrice + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        window.location.reload();
                        jQuery(".loader_div").hide();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Error");
                        jQuery(".loader_div").hide();
                    }
                });
                return true;
            }
        }
        function IsnullOrEmpty(val) {
            if (val != '' && val != undefined && val != '--Select--')
                return false;
            else
                return true;
        }

        //function fillMOQ(obj) {
        //    debugger;
        //    var id = obj.id;
        //    var stock_id = id.replace("txtEscOrderQuant", "hdnEscStockCode");
        //    var stock = $('#' + stock_id).val();
        //    id = id.replace("txtEscOrderQuant", "txtEscMOQ");
        //    if (!(stock == "1" || stock == "6")) {
        //        $("#" + id).val($("#" + obj.id).val());
        //    }
        //}

        function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                    // Number of digits right of decimal point.
                    (match[1] ? match[1].length : 0)
                    // Adjust for scientific notation.
                    - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;
                if (charCode == 46) return false;
            }
            else {
                if (value.length > 10) {
                    if (charCode == 46) return true;
                    else return false;
                }
            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function setTextareaVal(evt, obj) {
            debugger;
            var value = obj.value;
            $("#" + obj.id).val(value);
        }

        function AddGP(obj) {
            debugger;
            var value = obj.value;
            var id = obj.id;
            id = id.replace("txtNewOfferPrice", "hdnCP");
            var cp = $("#" + id).val();
            var lbl_id = id.replace("hdnCP", "lblGP_Special");
            var gp = Math.round((value - cp) * 100 / value);
            $("#" + lbl_id).text(gp);
        }
        //function AddEscGP(obj) {
        //    debugger;
        //    var value = obj.value;
        //    var id = obj.id;
        //    var LP_id;
        //    var LP;
        //    if (id.contains('txtEscNewOfferPrice')) {
        //        id = id.replace("txtEscNewOfferPrice", "hdnEscCP");
        //        LP_id = id.replace("txtEscNewOfferPrice", "hdnIEscLP"); 
        //    }
        //    else if (id.contains('txtIEscDiscount')) {
        //        id = id.replace("txtIEscDiscount", "hdnEscCP");
        //        LP_id = id.replace("txtIEscDiscount", "hdnIEscLP"); 
        //    }
        //    LP = $("#" + LP_id).val();
        //    var cp = $("#" + id).val();
        //    var lbl_id = id.replace("hdnEscCP", "lblEscGP_Special");
        //    var gp = Math.round((value - cp) * 100 / value);
        //    $("#" + lbl_id).text(gp);
        //}

        function AddEscGP(obj) {
            debugger;
            var value;
            var id = obj.id;
            var LP_id;
            var LP;
            var discount;
            var discount_id;
            var spl_id;
            if (id.includes('txtEscNewOfferPrice')) {
                value = obj.value;
                LP_id = id.replace("txtEscNewOfferPrice", "hdnEscLP");
                LP = $("#" + LP_id).val();
                discount = Math.round((LP - value) * 100 / LP);
                discount_id = id.replace("txtEscNewOfferPrice", "txtEscDiscount");
                $("#" + discount_id).val(discount);

                id = id.replace("txtEscNewOfferPrice", "hdnEscCP");
            }
            else if (id.includes('txtEscDiscount')) {
                discount = obj.value;
                LP_id = id.replace("txtEscDiscount", "hdnEscLP");
                LP = $("#" + LP_id).val();
                value = LP - Math.round(LP * discount / 100);
                spl_id = id.replace("txtEscDiscount", "txtEscNewOfferPrice");
                $("#" + spl_id).val(value);
                id = id.replace("txtEscDiscount", "hdnEscCP");
            }
            var cp = $("#" + id).val();
            var lbl_id = id.replace("hdnEscCP", "lblEscGP_Special");
            var gp = Math.round((value - cp) * 100 / value);
            $("#" + lbl_id).text(gp);
        }

        function AddIEscGP(obj) {
            debugger;
            var value;
            var id = obj.id;
            var LP_id;
            var LP;
            var discount;
            var discount_id;
            var spl_id;
            if (id.includes('txtIEscNewOfferPrice')) {
                value = obj.value;
                LP_id = id.replace("txtIEscNewOfferPrice", "hdnIEscLP");
                LP = $("#" + LP_id).val();
                discount = Math.round((LP - value) * 100 / LP);
                discount_id = id.replace("txtIEscNewOfferPrice", "txtIEscDiscount");
                $("#" + discount_id).val(discount);

                id = id.replace("txtIEscNewOfferPrice", "hdnIEscCP");
            }
            else if (id.includes('txtIEscDiscount')) {
                discount = obj.value;
                LP_id = id.replace("txtIEscDiscount", "hdnIEscLP");
                LP = $("#" + LP_id).val();
                value = LP - Math.round(LP * discount / 100);
                spl_id = id.replace("txtIEscDiscount", "txtIEscNewOfferPrice");
                $("#" + spl_id).val(value);

                id = id.replace("txtIEscDiscount", "hdnIEscCP");
            }
            var cp = $("#" + id).val();
            var lbl_id = id.replace("hdnIEscCP", "lblIEscGP_Special");
            var gp = Math.round((value - cp) * 100 / value);
            $("#" + lbl_id).text(gp);
        }
        document.addEventListener("DOMContentLoaded", function () {
            debugger;
            var tabs = document.querySelectorAll('.tabbed li');
            var switchers = document.querySelectorAll('.switcher-box a');
            var skinable = document.getElementById('skinable');

            for (var i = 0, len = tabs.length; i < len; i++) {
                tabs[i].addEventListener("click", function () {
                    if (this.classList.contains('active')) {
                        debugger;
                        return;
                    }
                    var parent = this.parentNode,
                        innerTabs = parent.querySelectorAll('li');

                    for (var index = 0, iLen = innerTabs.length; index < iLen; index++) {
                        innerTabs[index].classList.remove('active');
                    }

                    this.classList.add('active');
                });
            }

            for (var i = 0, len = switchers.length; i < len; i++) {
                switchers[i].addEventListener("click", function () {
                    if (this.classList.contains('active'))
                        return;

                    var parent = this.parentNode,
                        innerSwitchers = parent.querySelectorAll('a'),
                        skinName = this.getAttribute('skin');

                    for (var index = 0, iLen = innerSwitchers.length; index < iLen; index++) {
                        innerSwitchers[index].classList.remove('active');
                    }

                    this.classList.add('active');
                    skinable.className = 'tabbed round ' + skinName;
                });
            }
        });


        function tabchange(e) {
            debugger;
            if (e.id == "MainContent_requestedList") {
                $('#MainContent_divRequested').css("display", "block");
                $('#MainContent_divEscalated').css("display", "none");
                $('#MainContent_divIntEscalated').css("display", "none");
                $('#MainContent_divEscalated').removeClass("active");
                $('#MainContent_divRequested').addClass("active");
                $('#MainContent_divIntEscalated').removeClass("active");
                if (!$.fn.DataTable.isDataTable('#MainContent_grdPendingQuote')) {
                    var head_content = $('#MainContent_grdPendingQuote tr:first').html();
                    $('#MainContent_grdPendingQuote').prepend('<thead></thead>')
                    $('#MainContent_grdPendingQuote thead').html('<tr>' + head_content + '</tr>');
                    $('#MainContent_grdPendingQuote tbody tr:first').hide();
                    var table = $('#MainContent_grdPendingQuote').dataTable({
                        //"order": [[2, 'desc']]
                        scrollY: "350px",
                        scrollX: true,
                        scrollCollapse: true,
                        //fixedColumns: true,
                        //fixedColumns: {
                        //    leftColumns: 1,
                        //    rightColumns: 4
                        //},
                        paging: false
                        //lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        //dom: 'lBfrtip',
                    });
                }
            } 
            else if (e.id == "MainContent_intescalatedList") {
                $('#MainContent_divRequested').css("display", "none");
                $('#MainContent_divEscalated').css("display", "none");
                $('#MainContent_divIntEscalated').css("display", "block");
                $('#MainContent_divEscalated').removeClass("active");
                $('#MainContent_divRequested').removeClass("active");
                $('#MainContent_divIntEscalated').addClass("active");
                if (!$.fn.DataTable.isDataTable('#MainContent_grdIntEscalatedQuote')) {
                    var head_content = $('#MainContent_grdIntEscalatedQuote tr:first').html();
                    $('#MainContent_grdIntEscalatedQuote').prepend('<thead></thead>')
                    $('#MainContent_grdIntEscalatedQuote thead').html('<tr>' + head_content + '</tr>');
                    $('#MainContent_grdIntEscalatedQuote tbody tr:first').hide();
                    var table = $('#MainContent_grdIntEscalatedQuote').dataTable({
                        //"order": [[2, 'desc']]
                        scrollY: "350px",
                        scrollX: true,
                        scrollCollapse: true,
                        //fixedColumns: true,
                        //fixedColumns: {
                        //    leftColumns: 1,
                        //    rightColumns: 4
                        //},
                        paging: false
                        //lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        //dom: 'lBfrtip',
                    });
                }
            } 
            else {
                $('#MainContent_divRequested').css("display", "none");
                $('#MainContent_divEscalated').css("display", "block");
                $('#MainContent_divIntEscalated').css("display", "none");
                $('#MainContent_divEscalated').addClass("active");
                $('#MainContent_divRequested').removeClass("active");
                $('#MainContent_divIntEscalated').removeClass("active");
                //if (!$.fn.DataTable.isDataTable('#MainContent_grdEscalatedQuote')) {


                //}
            }

        }

        function OpenDetailView(evt, obj) {
            evt.preventDefault();
            var id = obj.id;
            var ref_id = id.replace("btnView", "hdnID");
            sessionStorage.setItem('ref_id', ref_id);
            var refnum_id = id.replace("btnView", "lblref");
            var item_id = id.replace("btnView", "lblitem");
            var ref = $("#" + ref_id).val();
            var refnum = $("#" + refnum_id).val();
            var item = $("#" + item_id).text();
            var ordervalidity_id = id.replace("btnView", "txtOrdValidity");
            var ordervalidity = $("#" + ordervalidity_id).val();
            sessionStorage.setItem('ordervalidity', ordervalidity);
            var orderqty_id = id.replace("btnView", "txtOrderQuant");
            var orderqty = $("#" + orderqty_id).val();
            sessionStorage.setItem('orderqty', orderqty);
            var MOQ_id = id.replace("btnView", "txtMOQ");
            var MOQ = $("#" + MOQ_id).val();
            sessionStorage.setItem('MOQ', MOQ);
            var SP_id = id.replace("btnView", "txtNewOfferPrice");
            var SP = $("#" + SP_id).val();
            sessionStorage.setItem('sysPrice', SP);
            var comment_id = id.replace("btnView", "txtComment");
            var comment = $("#" + comment_id).val();
            sessionStorage.setItem('comment', comment);
            win = window.open("QuoteDetails.aspx?id=" + ref, "_blank", "WIDTH=900,HEIGHT=650,scrollbars=no, menubar=no,resizable=yes,directories=no,location=no");
        }
        function OpenEscDetailView(evt, obj) {
            evt.preventDefault();
            var id = obj.id;
            var ref_id = id.replace("btnView", "hdnEscID");
            sessionStorage.setItem('ref_id', ref_id);
            var refnum_id = id.replace("btnView", "lblEscref");
            var item_id = id.replace("btnView", "lblEscitem");
            var ref = $("#" + ref_id).val();
            var refnum = $("#" + refnum_id).val();
            var item = $("#" + item_id).text();
            var ordervalidity_id = id.replace("btnView", "txtEscOrdValidity");
            var ordervalidity = $("#" + ordervalidity_id).val();
            sessionStorage.setItem('ordervalidity', ordervalidity);
            var orderqty_id = id.replace("btnView", "txtEscOrderQuant");
            var orderqty = $("#" + orderqty_id).val();
            sessionStorage.setItem('orderqty', orderqty);
            //var MOQ_id = id.replace("btnView", "txtEscMOQ");
            //var MOQ = $("#" + MOQ_id).val();
            // sessionStorage.setItem('MOQ', MOQ);
            var SP_id = id.replace("btnView", "txtEscNewOfferPrice");
            var SP = $("#" + SP_id).val();
            sessionStorage.setItem('sysPrice', SP);
            var comment_id = id.replace("btnView", "txtEscComment");
            var comment = $("#" + comment_id).val();
            sessionStorage.setItem('comment', comment);
            win = window.open("QuoteDetails.aspx?Escid=" + ref, "_blank", "WIDTH=900,HEIGHT=650,scrollbars=no, menubar=no,resizable=yes,directories=no,location=no");
            //evt.preventDefault();
            //var id = obj.id;
            //var ref_id = id.replace("btnView", "hdnEscID");
            //var refnum_id = id.replace("btnView", "lblEscref");
            //var item_id = id.replace("btnView", "lblEscitem");
            //var ref = $("#" + ref_id).val();
            //var refnum = $("#" + refnum_id).text();
            //var item = $("#" + item_id).text();
            //window.open("QuoteDetails.aspx?Escid=" + ref, "_blank", "WIDTH=600,HEIGHT=400,scrollbars=no, menubar=no,resizable=yes,directories=no,location=no");
        }
        function OpenIEscDetailView(evt, obj) {
            evt.preventDefault();
            var id = obj.id;
            var ref_id = id.replace("btnIView", "hdnIEscID");
            sessionStorage.setItem('ref_id', ref_id);
            var refnum_id = id.replace("btnIView", "lblIEscref");
            var item_id = id.replace("btnIView", "lblIEscitem");
            var ref = $("#" + ref_id).val();
            var refnum = $("#" + refnum_id).val();
            var item = $("#" + item_id).text();
            var ordervalidity_id = id.replace("btnIView", "txtIEscOrdValidity");
            var ordervalidity = $("#" + ordervalidity_id).val();
            sessionStorage.setItem('ordervalidity', ordervalidity);
            var orderqty_id = id.replace("btnIView", "txtIEscOrderQuant");
            var orderqty = $("#" + orderqty_id).val();
            sessionStorage.setItem('orderqty', orderqty);
            var SP_id = id.replace("btnIView", "txtIEscNewOfferPrice");
            var SP = $("#" + SP_id).val();
            sessionStorage.setItem('sysPrice', SP);
            var comment_id = id.replace("btnIView", "txtIEscComment");
            var comment = $("#" + comment_id).val();
            sessionStorage.setItem('comment', comment);
            win = window.open("QuoteDetails.aspx?IEscid=" + ref, "_blank", "WIDTH=900,HEIGHT=650,scrollbars=no, menubar=no,resizable=yes,directories=no,location=no");
        }
        function validateEscalationFields(evt, obj) {
            $("#MainContent_lblEscresult").text("");
            debugger;
            var errFlag = 0;
            var id = obj.id;
            var comment_id;
            var lbl;
            var offerprice_id;
            var ref_id;
            var refnum_id;
            var item_id;
            var MOQ_id;
            var RMOQ_id;
            var multiflag_id;
            var status;
            var MOQ;
            var RMOQ;
            var lblMOQmsg_id;
            var offerprice;
            var lblpricemsg_id;
            var txtRecPrice_id;
            var lblTargetPrice_id;
            var lblrecpricemsg_id;
            var RecPrice;
            var oldofferprice_id;
            var oldofferprice;
            var stock;
            var txtOrdValidity_id;
            var txtOrderQuant_id;
            var ddlOrderFreq_id;
            var txtOrderQuant;
            var txtOrdValidity;
            var remarks_id;
            var Rlbl;
            var fileUpload;
            var roleId = '<%= Session["RoleId"] %>';
            if (id.includes("btnEscReject")) {
                comment_id = id.replace("btnEscReject", "txtEscComment");
                lbl = id.replace("btnEscReject", "lblEscmsg");
                remarks_id = id.replace("imgSendBack", "txtEscRemarks");
                Rlbl = id.replace("imgSendBack", "lblEscRmsg");
                offerprice_id = id.replace("btnEscReject", "txtEscNewOfferPrice");
                ref_id = id.replace("btnEscReject", "hdnEscID");
                refnum_id = id.replace("btnEscReject", "lblEscref");
                item_id = id.replace("btnEscReject", "lblEscitem");
                //MOQ_id = id.replace("btnEscReject", "txtEscMOQ");
                //RMOQ_id = id.replace("btnEscReject", "lblEscQTYPO");
                multiflag_id = id.replace("btnEscReject", "chkEscMulti");

                txtRecPrice_id = id.replace("btnEscReject", "txtEscRecPrice");
                RecPrice = $("#" + txtRecPrice_id).val();
                if (IsnullOrEmpty(RecPrice)) {
                    RecPrice = "";
                }

                //MOQ = $("#" + MOQ_id).val();
                //if (IsnullOrEmpty(MOQ)) {
                //    MOQ = "";
                //}
                offerprice = $("#" + offerprice_id).val();
                if (IsnullOrEmpty(offerprice)) {
                    offerprice = "";
                }
                if (IsnullOrEmpty($("#" + comment_id).val())) {
                    $("#" + comment_id).css("border", "1px solid red");
                    $("span#" + lbl).text("*Required");
                    errFlag++;
                }
                
                else {
                    $("span#" + lbl).text("");
                    $("#" + comment_id).css("border", "");
                    status = "Rejected";
                }
                if (roleId == "HO") {
                    remarks = $("#" + remarks_id).val();
                    fileUpload = $("#" + id.replace("btnEscReject", "fileUploadHO")).get(0);
                }
                else
                    fileUpload = $("#" + id.replace("btnEscReject", "fileUploadBM")).get(0);
            }
            else if (id.includes("btnEscApprove")) {
                comment_id = id.replace("btnEscApprove", "txtEscComment");
                lbl = id.replace("btnEscApprove", "lblEscmsg");
                offerprice_id = id.replace("btnEscApprove", "txtEscNewOfferPrice");
                remarks_id = id.replace("imgSendBack", "txtEscRemarks");
                Rlbl = id.replace("imgSendBack", "lblEscRmsg");
                oldofferprice_id = id.replace("btnApprove", "txtEscOfferPrice");
                ref_id = id.replace("btnEscApprove", "hdnEscID");
                refnum_id = id.replace("btnEscApprove", "lblEscref");
                item_id = id.replace("btnEscApprove", "lblEscitem");
                //MOQ_id = id.replace("btnEscApprove", "txtEscMOQ");
                //RMOQ_id = id.replace("btnEscApprove", "lblEscQTYPO");
                multiflag_id = id.replace("btnEscApprove", "chkEscMulti");
                lblpricemsg_id = id.replace("btnEscApprove", "lblEscpricemsg");
                stock_id = id.replace("btnEscApprove", "hdnEscStockCode");


                txtOrdValidity_id = id.replace("btnEscApprove", "txtEscOrdValidity");
                txtOrderQuant_id = id.replace("btnEscApprove", "txtEscOrderQuant");
                /* ddlOrderFreq_id = id.replace("btnEscApprove", "ddlEscOrderFreq");*/
                txtOrderQuant = $("#" + txtOrderQuant_id).val();
                txtOrdValidity = $("#" + txtOrdValidity_id).val();
                stock = $('#' + stock_id).val();
                //MOQ = $("#" + MOQ_id).val();
                //RMOQ = $("#" + RMOQ_id).val();
                offerprice = $("#" + offerprice_id).val();
                lblMOQmsg_id = id.replace("btnEscApprove", "lblEscMOQmsg");

                txtRecPrice_id = id.replace("btnEscApprove", "txtEscRecPrice");
                /*    lblTargetPrice_id = id.replace("btnEscApprove", "lblEscTargetPrice");*/
                lblrecpricemsg_id = id.replace("btnEscApprove", "lblEscrecpricemsg");
                /* var TargetPrice = $("span#" + lblTargetPrice_id).text();*/
                var RecPrice = $("#" + txtRecPrice_id).val();
                oldofferprice = $("#" + oldofferprice_id).val();
                //if (roleId == "BM" || roleId == "TM") {
                //    if (!IsnullOrEmpty(RecPrice)) {

                //        if (parseFloat(parseFloat(TargetPrice).toFixed(2)) > parseFloat(parseFloat(RecPrice).toFixed(2))) {
                //            //if (parseFloat(TargetPrice) > parseFloat(RecPrice)) {
                //            $("#" + txtRecPrice_id).css("border", "1px solid red");
                //            $("span#" + lblrecpricemsg_id).text("*");
                //            $("#MainContent_lblEscresult").text("*Price should be equal to or greater than Expected Price.");
                //            errFlag++;
                //        }
                //        else {
                //            $("#" + txtRecPrice_id).css("border", "");
                //            $("span#" + lblrecpricemsg_id).text("");
                //        }
                //    }
                //}
                /* if (roleId == "HO") {*/
                //if (IsnullOrEmpty(MOQ)) {
                //    MOQ = RMOQ;
                //    $("span#" + lblMOQmsg_id).text("");
                //}
                if (IsnullOrEmpty(txtOrderQuant)) {
                    $("#" + txtOrderQuant_id).css("border", "1px solid red");
                    $("#MainContent_lblEscresult").text("*Break quantity is required.");
                    errFlag++;
                }
                else {
                    $("#" + txtOrderQuant_id).css("border", "");
                }
                //if (IsnullOrEmpty(MOQ)) {
                //    if (stock == "1" || stock == "6") {
                //        MOQ = RMOQ;
                //        $("#" + MOQ_id).css("border", "");
                //        //$("span#" + lblMOQmsg_id).text("");
                //    }
                //    else {
                //        //$("span#" + lblMOQmsg_id).text("*");
                //        $("#" + MOQ_id).css("border", "1px solid red");
                //        $("#MainContent_lblEscresult").text("*MOQ is mandatory.");
                //        errFlag++;

                //    }
                //}
                //else if (parseInt(MOQ) > parseInt(txtOrderQuant)) {
                //    $("#" + MOQ_id).css("border", "1px solid red");
                //    //$("span#" + lblMOQmsg_id).text("*");
                //    $("#MainContent_lblEscresult").text("*Break quantity should be equal to or greater than MOQ.");
                //    errFlag++;
                //}
                //else {
                //    //$("span#" + lblMOQmsg_id).text("");
                //    $("#" + MOQ_id).css("border", "");
                //}
                //}
                //else {
                //    if (IsnullOrEmpty(MOQ)) {
                //        MOQ = "";
                //    }
                //    //$("span#" + lblMOQmsg_id).text("");
                //    $("#" + MOQ_id).css("border", "");
                //}
                if (IsnullOrEmpty($("#" + comment_id).val())) {
                    $("#" + comment_id).css("border", "1px solid red");
                    $("span#" + lbl).text("*");
                    $("#MainContent_lblEscresult").text("*Required");
                    errFlag++;
                }
               
                else {
                    $("span#" + lbl).text("");
                    $("#" + comment_id).css("border", "");
               
                }


                if (roleId == "HO") {
                    //expiry_date = $("#" + expiry_id).val();
                    if (IsnullOrEmpty($("#" + offerprice_id).val())) {
                        if (IsnullOrEmpty(RecPrice) && IsnullOrEmpty(oldofferprice)) {
                            $("#" + offerprice_id).css("border", "1px solid red");
                            $("span#" + lblpricemsg_id).text("*");
                            $("#MainContent_lblEscresult").text("*Required");
                            errFlag++;
                        }
                        else if (IsnullOrEmpty(RecPrice)) {
                            $("#" + offerprice_id).css("border", "1px solid red");
                            $("span#" + lblpricemsg_id).text("*");
                            $("#MainContent_lblEscresult").text("*Required");
                            errFlag++;
                        }
                        else {
                            offerprice = RecPrice;
                            $("#" + offerprice_id).css("border", "");
                            $("span#" + lblpricemsg_id).text("");
                            status = "Escalated & Approved";
                        }

                    }
                    else {

                        //if (parseFloat(parseFloat(TargetPrice).toFixed(2)) > parseFloat(parseFloat(offerprice).toFixed(2))) {
                        //    $("#" + offerprice_id).css("border", "1px solid red");
                        //    $("span#" + lblpricemsg_id).text("*");
                        //    $("#MainContent_lblEscresult").text("*Price should be equal to or greater than Expected Price.");
                        //    errFlag++;
                        //}
                        //else
                        if (parseFloat(parseFloat(oldofferprice_id).toFixed(2)) > parseFloat(parseFloat(offerprice).toFixed(2))) {
                            $("#" + offerprice_id).css("border", "1px solid red");
                            $("span#" + lbl).text("*");
                            $("#MainContent_lblEscresult").text("*Price should be equal to or greater than System Price.");
                            errFlag++;
                        }
                        else {
                            $("#" + offerprice_id).css("border", "");
                            $("span#" + lblpricemsg_id).text("");
                        }
                        status = "Escalated & Approved";
                    }

                    if (IsnullOrEmpty($("#" + remarks_id).val())) {
                        $("#" + remarks_id).css("border", "1px solid red");
                        $("span#" + Rlbl).text("*Required");
                        errFlag++;
                    }
                    else {
                        $("#" + remarks_id).css("border", "");
                        $("span#" + Rlbl).text("");
                    }
                }
                else if (roleId == "BM" || roleId == "TM") {
                    if (IsnullOrEmpty(offerprice)) {
                        offerprice = "";
                    }
                    status = "Escalated By BM";
                }
                if (roleId == "HO") {
                    remarks = $("#" + remarks_id).val();
                    fileUpload = $("#" + id.replace("btnEscApprove", "fileUploadHO")).get(0);
                }
                else
                    fileUpload = $("#" + id.replace("btnEscApprove", "fileUploadBM")).get(0);
            }
            else if (id.includes("imgSendBack")) {
                comment_id = id.replace("imgSendBack", "txtEscComment");
                lbl = id.replace("imgSendBack", "lblEscmsg");
                remarks_id = id.replace("imgSendBack", "txtEscRemarks");
                Rlbl = id.replace("imgSendBack", "lblEscRmsg");
                offerprice_id = id.replace("imgSendBack", "txtEscNewOfferPrice");
                ref_id = id.replace("imgSendBack", "hdnEscID");
                refnum_id = id.replace("imgSendBack", "lblEscref");
                item_id = id.replace("imgSendBack", "lblEscitem");
                //MOQ_id = id.replace("btnEscReject", "txtEscMOQ");
                //RMOQ_id = id.replace("btnEscReject", "lblEscQTYPO");
                multiflag_id = id.replace("imgSendBack", "chkEscMulti");

                txtRecPrice_id = id.replace("imgSendBack", "txtEscRecPrice");
                RecPrice = $("#" + txtRecPrice_id).val();
                if (IsnullOrEmpty(RecPrice)) {
                    RecPrice = "";
                }

                //MOQ = $("#" + MOQ_id).val();
                //if (IsnullOrEmpty(MOQ)) {
                //    MOQ = "";
                //}
                offerprice = $("#" + offerprice_id).val();
                if (IsnullOrEmpty(offerprice)) {
                    offerprice = "";
                }
                if (IsnullOrEmpty($("#" + comment_id).val())) {
                    $("#" + comment_id).css("border", "1px solid red");
                    $("span#" + lbl).text("*Required");
                    errFlag++;
                }
                else {
                    $("span#" + lbl).text("");
                    $("#" + comment_id).css("border", "");
                   
                }
                if (roleId == "HO") {
                    status = "Int Escalated Back To BM";
                    remarks = $("#" + remarks_id).val();
                    fileUpload = $("#" + id.replace("imgSendBack", "fileUploadHO")).get(0);
                }
                else {
                    fileUpload = $("#" + id.replace("imgSendBack", "fileUploadBM")).get(0);
                }
            }
            if (errFlag > 0) {
                return false;
            }
            else {
                var ref = $("#" + ref_id).val();
                var refnum = $("#" + refnum_id).val();
                var item = $("#" + item_id).text();

                var multiflag;
                //if ($("#" + multiflag_id).is(':checked'))
                //    multiflag = 1;
                //else
                multiflag = 0;

                var comment = $("#" + comment_id).val();
                var remarks = "";
                var objList = [];
               
                var files = fileUpload.files;
                if (files.length > 0) {
                    var fileData = new FormData();

                    // Looping over all files and add it to FormData object  
                    for (var i = 0; i < files.length; i++) {
                        fileData.append(files[i].name, files[i]);
                    }
                    //fileData.append('ref_num', refnum);
                    //fileData.append('item', item);

                    $.ajax({
                        url: 'QuoteFileUpload.ashx?ref_num=' + refnum + '&item=' + item+'&roleId='+roleId,
                       // url: 'FileUpload.ashx',
                        type: 'POST',
                        data: fileData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (file) {
                            var obj = {
                                "id": ref
                                , "item": item
                                , "ref_no": refnum
                                , "status": status
                                , "comment": comment
                                , "MOQ": txtOrderQuant
                                , "offerPrice": offerprice
                                , "multiorder_flag": multiflag
                                , "RecPrice": RecPrice
                                , "Order_Validity": txtOrdValidity
                                , "Approved_OrderQty": txtOrderQuant
                                , "Approved_OrderFreq": ""
                                , "file": file.name
                                , "Internal_Remarks": remarks
                            };
                            objList.push(obj);
                            var param = "{objList:" + JSON.stringify(objList) + "}";

                            $.ajax({
                                url: 'PendingQuotes.aspx/UpdateQuoteStatus',
                                method: 'post',
                                datatype: 'json',
                                data: param,
                                //data: '{id:"' + ref + '", item:"' + item + '", ref_no:"' + refnum + '", status:"' + status + '", comment:"' + comment + '", MOQ:"' + MOQ + '", offerPrice:"' + offerprice + '", multiorder_flag:' + multiflag + ', RecPrice: "' + RecPrice + '"}',
                                contentType: "application/json; charset=utf-8",
                                success: function (msg) {
                                    msg = JSON.parse(msg.d);
                                    alert(msg.msg);
                                    window.location.reload();

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert("Error");
                                }
                            });
                        },
                        error: function (err) {
                            alert(err.statusText);
                        }
                    });
                }
                // Create FormData object  
                else {
                    var obj = {
                        "id": ref
                        , "item": item
                        , "ref_no": refnum
                        , "status": status
                        , "comment": comment
                        , "MOQ": txtOrderQuant
                        , "offerPrice": offerprice
                        , "multiorder_flag": multiflag
                        , "RecPrice": RecPrice
                        , "Order_Validity": txtOrdValidity
                        , "Approved_OrderQty": txtOrderQuant
                        , "Approved_OrderFreq": ""
                        , "file": ""
                        , "Internal_Remarks": remarks
                    };
                    objList.push(obj);
                    var param = "{objList:" + JSON.stringify(objList) + "}";

                    $.ajax({
                        url: 'PendingQuotes.aspx/UpdateQuoteStatus',
                        method: 'post',
                        datatype: 'json',
                        data: param,
                        //data: '{id:"' + ref + '", item:"' + item + '", ref_no:"' + refnum + '", status:"' + status + '", comment:"' + comment + '", MOQ:"' + MOQ + '", offerPrice:"' + offerprice + '", multiorder_flag:' + multiflag + ', RecPrice: "' + RecPrice + '"}',
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            msg = JSON.parse(msg.d);
                            alert(msg.msg);
                            window.location.reload();

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("Error");
                        }
                    });
                }
                return true;
            }
        }
        function validateIEscalationFields(evt, obj) {
            $("#MainContent_lblIEscresult").text("");
            debugger;
            var errFlag = 0;
            var id = obj.id;
            var comment_id;
            var lbl;
            var offerprice_id;
            var ref_id;
            var refnum_id;
            var item_id;
            var MOQ_id;
            var RMOQ_id;
            var multiflag_id;
            var status;
            var MOQ;
            var RMOQ;
            var lblMOQmsg_id;
            var offerprice;
            var lblpricemsg_id;
            var txtRecPrice_id;
            var lblTargetPrice_id;
            var lblrecpricemsg_id;
            var RecPrice;
            var oldofferprice_id;
            var oldofferprice;
            var stock;
            var txtOrdValidity_id;
            var txtOrderQuant_id;
            var ddlOrderFreq_id;
            var txtOrderQuant;
            var txtOrdValidity;
            var remarks_id;
            var Rlbl;
            var fileUpload;
            var remarks = "";
            var roleId = '<%= Session["RoleId"] %>';
            if (id.includes("btnIEscReject")) {
                comment_id = id.replace("btnIEscReject", "txtIEscComment");
                lbl = id.replace("btnIEscReject", "lblIEscmsg");
                remarks_id = id.replace("btnIEscReject", "txtIEscRemarks");
                Rlbl = id.replace("btnIEscReject", "lblIEscRmsg");
                offerprice_id = id.replace("btnIEscReject", "txtIEscNewOfferPrice");
                ref_id = id.replace("btnIEscReject", "hdnIEscID");
                refnum_id = id.replace("btnIEscReject", "lblIEscref");
                item_id = id.replace("btnIEscReject", "lblIEscitem");
                multiflag_id = id.replace("btnIEscReject", "chkIEscMulti");

                txtRecPrice_id = id.replace("btnIEscReject", "txtIEscRecPrice");
                RecPrice = $("#" + txtRecPrice_id).val();
                if (IsnullOrEmpty(RecPrice)) {
                    RecPrice = "";
                }
                offerprice = $("#" + offerprice_id).val();
                if (IsnullOrEmpty(offerprice)) {
                    offerprice = "";
                }
                if (IsnullOrEmpty($("#" + comment_id).val())) {
                    $("#" + comment_id).css("border", "1px solid red");
                    $("span#" + lbl).text("*Required");
                    errFlag++;
                }

                else {
                    $("span#" + lbl).text("");
                    $("#" + comment_id).css("border", "");
                    status = "Rejected";
                }
                if (roleId == "HO") {
                    remarks = $("#" + remarks_id).val();
                    fileUpload = $("#" + id.replace("btnIEscReject", "IfileUploadHO")).get(0);
                }
                else
                    fileUpload = $("#" + id.replace("btnIEscReject", "IfileUploadBM")).get(0);
            }
            else if (id.includes("btnIEscApprove")) {
                comment_id = id.replace("btnIEscApprove", "txtIEscComment");
                lbl = id.replace("btnIEscApprove", "lblIEscmsg");
                offerprice_id = id.replace("btnIEscApprove", "txtIEscNewOfferPrice");
                remarks_id = id.replace("btnIEscApprove", "txtIEscRemarks");
                Rlbl = id.replace("btnIEscApprove", "lblIEscRmsg");
                oldofferprice_id = id.replace("btnIEscApprove", "txtIEscOfferPrice");
                ref_id = id.replace("btnIEscApprove", "hdnIEscID");
                refnum_id = id.replace("btnIEscApprove", "lblIEscref");
                item_id = id.replace("btnIEscApprove", "lblIEscitem")
                multiflag_id = id.replace("btnIEscApprove", "chkIEscMulti");
                lblpricemsg_id = id.replace("btnIEscApprove", "lblIEscpricemsg");
                stock_id = id.replace("btnIEscApprove", "hdnIEscStockCode");
                txtOrdValidity_id = id.replace("btnIEscApprove", "txtIEscOrdValidity");
                txtOrderQuant_id = id.replace("btnIEscApprove", "txtIEscOrderQuant");
                txtOrderQuant = $("#" + txtOrderQuant_id).val();
                txtOrdValidity = $("#" + txtOrdValidity_id).val();
                stock = $('#' + stock_id).val();
                //MOQ = $("#" + MOQ_id).val();
                //RMOQ = $("#" + RMOQ_id).val();
                offerprice = $("#" + offerprice_id).val();
                lblMOQmsg_id = id.replace("btnIEscApprove", "lblIEscMOQmsg");

                txtRecPrice_id = id.replace("btnIEscApprove", "txtIEscRecPrice");
                /*    lblTargetPrice_id = id.replace("btnEscApprove", "lblEscTargetPrice");*/
                lblrecpricemsg_id = id.replace("btnIEscApprove", "lblIEscrecpricemsg");
                /* var TargetPrice = $("span#" + lblTargetPrice_id).text();*/
                var RecPrice = $("#" + txtRecPrice_id).val();
                oldofferprice = $("#" + oldofferprice_id).val();
                if (IsnullOrEmpty(txtOrderQuant)) {
                    $("#" + txtOrderQuant_id).css("border", "1px solid red");
                    $("#MainContent_lblIEscresult").text("*Break quantity is required.");
                    errFlag++;
                }
                else {
                    $("#" + txtOrderQuant_id).css("border", "");
                }
                if (roleId == "HO") {
                    if (IsnullOrEmpty($("#" + remarks_id).val())) {
                        $("#" + remarks_id).css("border", "1px solid red");
                        $("span#" + Rlbl).text("*Required");
                        errFlag++;
                    }
                    else {
                        $("#" + remarks_id).css("border", "");
                        $("span#" + Rlbl).text("");
                    }
                }
                if (IsnullOrEmpty($("#" + comment_id).val())) {
                    $("#" + comment_id).css("border", "1px solid red");
                    $("span#" + lbl).text("*");
                    $("#MainContent_lblIEscresult").text("*Required");
                    errFlag++;
                }
                
                else {
                    $("span#" + lbl).text("");
                    $("#" + comment_id).css("border", "");
                }


                if (roleId == "HO") {
                    //expiry_date = $("#" + expiry_id).val();
                    if (IsnullOrEmpty($("#" + offerprice_id).val())) {
                        if (IsnullOrEmpty(RecPrice) && IsnullOrEmpty(oldofferprice)) {
                            $("#" + offerprice_id).css("border", "1px solid red");
                            $("span#" + lblpricemsg_id).text("*");
                            $("#MainContent_lblIEscresult").text("*Required");
                            errFlag++;
                        }
                        else if (IsnullOrEmpty(RecPrice)) {
                            $("#" + offerprice_id).css("border", "1px solid red");
                            $("span#" + lblpricemsg_id).text("*");
                            $("#MainContent_lblIEscresult").text("*Required");
                            errFlag++;
                        }
                        else {
                            offerprice = RecPrice;
                            $("#" + offerprice_id).css("border", "");
                            $("span#" + lblpricemsg_id).text("");
                            status = "Escalated & Approved";
                        }

                    }
                    else {
                        if (parseFloat(parseFloat(oldofferprice_id).toFixed(2)) > parseFloat(parseFloat(offerprice).toFixed(2))) {
                            $("#" + offerprice_id).css("border", "1px solid red");
                            $("span#" + lbl).text("*");
                            $("#MainContent_lblIEscresult").text("*Price should be equal to or greater than System Price.");
                            errFlag++;
                        }
                        else {
                            $("#" + offerprice_id).css("border", "");
                            $("span#" + lblpricemsg_id).text("");
                        }
                        status = "Escalated & Approved";
                    }
                }
                else if (roleId == "BM" || roleId == "TM") {
                    if (IsnullOrEmpty(offerprice)) {
                        offerprice = "";
                    }
                    status = "Int Escalated By BM";
                }
                if (roleId == "HO") {
                    remarks = $("#" + remarks_id).val();
                    fileUpload = $("#" + id.replace("btnIEscApprove", "IfileUploadHO")).get(0);
                }
                else
                    fileUpload = $("#" + id.replace("btnIEscApprove", "IfileUploadBM")).get(0);
            }
            else if (id.includes("imgISendBack")) {
                comment_id = id.replace("imgISendBack", "txtIEscComment");
                lbl = id.replace("imgISendBack", "lblIEscmsg");
                remarks_id = id.replace("imgISendBack", "txtIEscRemarks");
                Rlbl = id.replace("imgISendBack", "lblIEscRmsg");
                offerprice_id = id.replace("imgISendBack", "txtIEscNewOfferPrice");
                ref_id = id.replace("imgISendBack", "hdnIEscID");
                refnum_id = id.replace("imgISendBack", "lblIEscref");
                item_id = id.replace("imgISendBack", "lblIEscitem");
                multiflag_id = id.replace("imgISendBack", "chkIEscMulti");

                txtRecPrice_id = id.replace("imgISendBack", "txtIEscRecPrice");
                RecPrice = $("#" + txtRecPrice_id).val();
                if (IsnullOrEmpty(RecPrice)) {
                    RecPrice = "";
                }

                offerprice = $("#" + offerprice_id).val();
                if (IsnullOrEmpty(offerprice)) {
                    offerprice = "";
                }
                if (IsnullOrEmpty($("#" + comment_id).val())) {
                    $("#" + comment_id).css("border", "1px solid red");
                    $("span#" + lbl).text("*Required");
                    errFlag++;
                }
                else {
                    $("span#" + lbl).text("");
                    $("#" + comment_id).css("border", "");
                    status = "Int Escalated Back To BM";
                }
                if (roleId == "HO") {
                    remarks = $("#" + remarks_id).val();
                    fileUpload = $("#" + id.replace("imgISendBack", "IfileUploadHO")).get(0);
                }
                else
                    fileUpload = $("#" + id.replace("imgISendBack", "IfileUploadBM")).get(0);
            }
            if (errFlag > 0) {
                return false;
            }
            else {
                var ref = $("#" + ref_id).val();
                var refnum = $("#" + refnum_id).val();
                var item = $("#" + item_id).text();

                var multiflag;
                //if ($("#" + multiflag_id).is(':checked'))
                //    multiflag = 1;
                //else
                multiflag = 0;

                var comment = $("#" + comment_id).val();
                
                var objList = [];

                var files = fileUpload.files;
                if (files.length > 0) {
                    var fileData = new FormData();

                    // Looping over all files and add it to FormData object  
                    for (var i = 0; i < files.length; i++) {
                        fileData.append(files[i].name, files[i]);
                    }
                    //fileData.append('ref_num', refnum);
                    //fileData.append('item', item);

                    $.ajax({
                        url: 'QuoteFileUpload.ashx?ref_num=' + refnum + '&item=' + item + '&roleId=' + roleId,
                        // url: 'FileUpload.ashx',
                        type: 'POST',
                        data: fileData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (file) {
                            var obj = {
                                "id": ref
                                , "item": item
                                , "ref_no": refnum
                                , "status": status
                                , "comment": comment
                                , "MOQ": txtOrderQuant
                                , "offerPrice": offerprice
                                , "multiorder_flag": multiflag
                                , "RecPrice": RecPrice
                                , "Order_Validity": txtOrdValidity
                                , "Approved_OrderQty": txtOrderQuant
                                , "Approved_OrderFreq": ""
                                , "file": file.name
                                , "Internal_Remarks": remarks
                            };
                            objList.push(obj);
                            var param = "{objList:" + JSON.stringify(objList) + "}";

                            $.ajax({
                                url: 'PendingQuotes.aspx/UpdateQuoteStatus',
                                method: 'post',
                                datatype: 'json',
                                data: param,
                                //data: '{id:"' + ref + '", item:"' + item + '", ref_no:"' + refnum + '", status:"' + status + '", comment:"' + comment + '", MOQ:"' + MOQ + '", offerPrice:"' + offerprice + '", multiorder_flag:' + multiflag + ', RecPrice: "' + RecPrice + '"}',
                                contentType: "application/json; charset=utf-8",
                                success: function (msg) {
                                    msg = JSON.parse(msg.d);
                                    alert(msg.msg);
                                    window.location.reload();

                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert("Error");
                                }
                            });
                        },
                        error: function (err) {
                            alert(err.statusText);
                        }
                    });
                }
                // Create FormData object  
                else {
                    var obj = {
                        "id": ref
                        , "item": item
                        , "ref_no": refnum
                        , "status": status
                        , "comment": comment
                        , "MOQ": txtOrderQuant
                        , "offerPrice": offerprice
                        , "multiorder_flag": multiflag
                        , "RecPrice": RecPrice
                        , "Order_Validity": txtOrdValidity
                        , "Approved_OrderQty": txtOrderQuant
                        , "Approved_OrderFreq": ""
                        , "file": ""
                        , "Internal_Remarks": remarks
                    };
                    objList.push(obj);
                    var param = "{objList:" + JSON.stringify(objList) + "}";

                    $.ajax({
                        url: 'PendingQuotes.aspx/UpdateQuoteStatus',
                        method: 'post',
                        datatype: 'json',
                        data: param,
                        //data: '{id:"' + ref + '", item:"' + item + '", ref_no:"' + refnum + '", status:"' + status + '", comment:"' + comment + '", MOQ:"' + MOQ + '", offerPrice:"' + offerprice + '", multiorder_flag:' + multiflag + ', RecPrice: "' + RecPrice + '"}',
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            msg = JSON.parse(msg.d);
                            alert(msg.msg);
                            window.location.reload();

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("Error");
                        }
                    });
                }
                return true;
            }
        }

        function DownloadFile(evt, obj) {
            debugger;
            var filename = $('#' + obj.id).text();
            $.ajax({
                url: 'FileDownload.ashx?file_name=' + filename,
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false,
                success: function (file) {
                },
                error: function (err) {
                    alert(err.statusText);
                }
            });
        }
        function parentFunction(val, flag) {
            debugger;
            console.log(val);
            win.close();
            var obj = JSON.parse(val);
            console.log(obj);
            if (flag == 'P') {
                var id = sessionStorage.getItem('ref_id');
                var orderValidity_id = id.replace("hdnID", "txtOrdValidity");
                var OrderQuant_id = id.replace("hdnID", "txtOrderQuant");
                var OrderFreq_id = id.replace("hdnID", "ddlOrderFreq");
                var MOQ_id = id.replace("hdnID", "txtMOQ");
                var NewOfferPrice_id = id.replace("hdnID", "txtNewOfferPrice");
                var Comment_id = id.replace("hdnID", "txtComment");
                $("#" + orderValidity_id).val(obj.objList[0].Order_Validity);
                $("#" + OrderQuant_id).val(obj.objList[0].Approved_OrderQty);
                $("#" + OrderFreq_id).val(obj.objList[0].Approved_OrderFreq);
                $("#" + MOQ_id).val(obj.objList[0].MOQ);
                $("#" + NewOfferPrice_id).val(obj.objList[0].offerPrice);
                $("#" + Comment_id).val(obj.objList[0].comment);
            }
            else if (flag == 'E') {
                var id = sessionStorage.getItem('ref_id');
                var orderValidity_id = id.replace("hdnEscID", "txtEscOrdValidity");
                var OrderQuant_id = id.replace("hdnEscID", "txtEscOrderQuant");
                var OrderFreq_id = id.replace("hdnEscID", "ddlEscOrderFreq");
                var MOQ_id = id.replace("hdnEscID", "txtEscMOQ");
                var NewOfferPrice_id = id.replace("hdnEscID", "txtEscNewOfferPrice");
                var Comment_id = id.replace("hdnEscID", "txtEscComment");
                $("#" + orderValidity_id).val(obj.objList[0].Order_Validity);
                $("#" + OrderQuant_id).val(obj.objList[0].Approved_OrderQty);
                $("#" + OrderFreq_id).val(obj.objList[0].Approved_OrderFreq);
                $("#" + MOQ_id).val(obj.objList[0].MOQ);
                $("#" + NewOfferPrice_id).val(obj.objList[0].offerPrice);
                $("#" + Comment_id).val(obj.objList[0].comment);
            }
            else if (flag == 'IE') {
                var id = sessionStorage.getItem('ref_id');
                var orderValidity_id = id.replace("hdnIEscID", "txtIEscOrdValidity");
                var OrderQuant_id = id.replace("hdnIEscID", "txtIEscOrderQuant");
                var OrderFreq_id = id.replace("hdnIEscID", "ddlIEscOrderFreq");
                var MOQ_id = id.replace("hdnIEscID", "txtIEscMOQ");
                var NewOfferPrice_id = id.replace("hdnIEscID", "txtIEscNewOfferPrice");
                var Comment_id = id.replace("hdnIEscID", "txtIEscComment");
                $("#" + orderValidity_id).val(obj.objList[0].Order_Validity);
                $("#" + OrderQuant_id).val(obj.objList[0].Approved_OrderQty);
                $("#" + OrderFreq_id).val(obj.objList[0].Approved_OrderFreq);
                $("#" + MOQ_id).val(obj.objList[0].MOQ);
                $("#" + NewOfferPrice_id).val(obj.objList[0].offerPrice);
                $("#" + Comment_id).val(obj.objList[0].comment);
            }
        }

    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">

    <div class="crumbs">
        <!-- Start : Breadcrumbs -->
        <ul id="Ul1" class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a>Quote</a>
            </li>
            <li class="current">Pending Quotes</li>
        </ul>
    </div>
    <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
    <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <%--<asp:Panel runat="server" ID="pnlData">--%>
        <ContentTemplate>

            <div class="col-md-12 mn_margin">
                <div id="collapsebtn" class="row">
                    <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />
                </div>
                <div class="row filter_panel" id="reportdrpdwns" runat="server">
                    <div runat="server" id="cterDiv" visible="false">
                        <ul class="btn-info rbtn_panel" >
                            <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>

                                <asp:RadioButton Enabled="false" ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                                <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                                <asp:RadioButton Enabled="false" ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            </li>
                        </ul>
                    </div>


                    <div class="col-md-12 nopadding">

                        <div class="col-md-2 control">
                            <label class="label">Requested Date</label>
                            <asp:TextBox ID="txtDateRange" CssClass="control_dropdown" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-2 btncontrol">
                            <asp:Button runat="server" ID="btnFilter" CssClass="btn green" Text="Filter" OnClick="btnFilter_Click" />
                        </div>
                    </div>



                </div>


                <div id="divQuotes" runat="server">
                    <div class="tabbed skin-turquoise round" id="skinable" style="margin-bottom: 10px;">
                        <ul>
                         <%--   <li id="requestedList" runat="server" onclick="tabchange(this);">Requested Quotes</li>--%>
                            <li id="intescalatedList" runat="server" onclick="tabchange(this);">Internal Escalated Quotes</li>
                            <li id="escalatedList" runat="server" class="active" onclick="tabchange(this);">Escalated Quotes</li>
                        </ul>
                    </div>
                    <div class="col-md-12 nopad" id="divRequested" runat="server" style="display: none;">
                        <asp:Button ID="btnMultiApprove" runat="server" CssClass="btn green" Text="Approve" OnClientClick="return validateMultiple(event,this);" />
                        <asp:Label runat="server" ID="lblresult"></asp:Label>
                        <asp:Label ID="lblErrMsg" runat="server" Style="color: red;"></asp:Label>
                        <asp:GridView ID="grdPendingQuote" Style="width: 100%;" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chk_multi" runat="server" CssClass="checkbox" />
                                        <asp:HiddenField ID="hdn_chk_multi" runat="server" Value="0" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Code">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID")%>' />
                                        <asp:HiddenField ID="lblref" runat="server" Value='<%#Bind("Ref_number") %>'></asp:HiddenField>
                                        <asp:HiddenField runat="server" ID="hdnStockCode" Value='<%# Eval("StockCode")%>' />
                                        <asp:Label ID="lblitem" runat="server" Text='<%#Bind("Item_code") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Description" HeaderStyle-Width="200px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblitemdesc" runat="server" Text='<%#Bind("Item_Desc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderTargetPrice" Text="Exp Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTargetPrice" Text='<%#Bind("Expected_price") %>' CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblMulti" Text="MOF" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkMulti" runat="server" Checked="true" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Ord.Valdy" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="lblQTYPO" Value='<%#Bind("QTY_perOrder") %>' runat="server"></asp:HiddenField>
                                        <asp:TextBox ID="txtOrdValidity" Text='<%#Bind("OrderValidity") %>' onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Brk Qty" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtOrderQuant" onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server"></asp:TextBox>
                                        <asp:TextBox ID="txtMOQ" onkeypress="return isNumberKey(event,this);" Style="width: 80px; display: none;" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--  <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblMOQ" Text="MOQ" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtMOQ" onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server"></asp:TextBox>
                                        <asp:Label ID="lblMOQmsg" runat="server" Style="color: red;"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblOfferPrice" Text="Sys Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtOfferPrice" Enabled="false" Style="width: 80px;" runat="server" Text='<%#Bind("Offer_price") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblGPOffer" Text="GP(Sys)" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblGP_Offer" runat="server" Text='<%#Bind("GP") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Rec. Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtRecPrice" onkeyup="setTextareaVal(event,this);" onkeypress="return isNumberKey(event,this);" CssClass="number1" Style="width: 80px;" runat="server" Text='<%#Bind("Recommended_Price") %>'></asp:TextBox>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblNewOfferPrice" Text="Spl Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtNewOfferPrice" onkeyup="setTextareaVal(event,this); AddGP(this);" onkeypress="return isNumberKey(event,this);" CssClass="number1" Style="width: 80px;" runat="server" Text='<%#Bind("New_OfferPrice") %>'></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lblpricemsg" runat="server" Style="color: red;"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblGPSpecial" Text="GP(Spl)" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnCP" Value='<%#Bind("CP") %>' runat="server" />
                                        <asp:Label ID="lblGP_Special" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="cssComment">
                                    <HeaderTemplate>
                                        <asp:Label ID="lblComment" Text="Comment" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox Rows="2" Columns="20" ID="txtComment" TextMode="MultiLine" runat="server"></asp:TextBox>
                                        <asp:Label ID="lblmsg" runat="server" Style="color: red;"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Actions" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="width: 90px;">
                                            <asp:ImageButton CssClass="approve" ToolTip="Approve" ImageUrl="images/icons/approve.png" OnClientClick="return validateFields(event,this);" CommandName="Approve" ID="btnApprove" runat="server" Style="height: 25px;" />
                                            <asp:ImageButton CssClass="reject" ToolTip="Reject" ImageUrl="images/icons/reject.png" OnClientClick="return validateFields(event,this);" CommandName="Reject" CommandArgument='<%#Bind("ID") %>' ID="btnReject" runat="server" Style="height: 25px;" />
                                            <asp:ImageButton ToolTip="View Details" ImageUrl="images/icons/view_details.png" runat="server" ID="btnView" OnClientClick="OpenDetailView(event,this);" Style="height: 25px;" />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </div>
                    <div class="col-md-12 nopad" id="divEscalated" runat="server" style="display: block;">
                        <asp:Label runat="server" ID="lblEscresult" Style="color: red;"></asp:Label>
                        <asp:GridView ID="grdEscalatedQuote" Style="width: 100%;" CssClass="display compact" runat="server" AutoGenerateColumns="false" OnRowDataBound="grdEscalatedQuote_RowDataBound">
                            <Columns>

                                <asp:TemplateField HeaderText="Cust Name" ItemStyle-Width="80px" HeaderStyle-Width="80px">
                                    <ItemTemplate>
                                        <asp:Label ID="txtEscCustName" Text='<%#Bind("RaisedBy") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Reference No">
                                    <ItemTemplate>
                                         <asp:HiddenField ID="hdnEscID" runat="server" Value='<%# Eval("ID")%>' />
                                        <asp:HiddenField ID="hdnEscStockCode" runat="server" Value='<%# Eval("StockCode")%>' />
                                        <asp:Label ID="lblEscref" runat="server" Text='<%#Bind("Ref_number") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <%--                                <asp:TemplateField HeaderText="Requested By">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEscreqby" runat="server" Text='<%#Bind("RaisedBy") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Item Code">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnEscID" runat="server" Value='<%# Eval("ID")%>' />
                                        <asp:HiddenField ID="hdnEscStockCode" runat="server" Value='<%# Eval("StockCode")%>' />
                                        <asp:HiddenField ID="lblEscref" runat="server" Value='<%#Bind("Ref_number") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hdnEscLP" runat="server" Value='<%#Bind("List_Price") %>'></asp:HiddenField>
                                        <asp:Label ID="lblEscitem" runat="server" Text='<%#Bind("Item_code") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" HeaderStyle-Width="150px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEscitemdesc" runat="server" Text='<%#Bind("Item_Desc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="End Customer" HeaderStyle-Width="150px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEscendcust" runat="server" Text='<%#Bind("End_Customer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--               <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblEscHeaderTargetPrice" Text="Exp Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEscTargetPrice" Text='<%#Bind("Expected_price") %>' CssClass="number1" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="20px" HeaderText="MOF" ItemStyle-Width="20px" ControlStyle-Width="20px">
                                  <HeaderTemplate >
                                        <asp:Label ID="lblEscMulti" Text="Multiple Order Flag" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkEscMulti" runat="server" Checked="true" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                 <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Exp.Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtEscEP" Enabled="false" Style="width: 80px;" runat="server" Text='<%#Bind("Expected_price") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Ord.Valdy" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="lblEscQTYPO" Value='<%#Bind("QTY_perOrder") %>' runat="server"></asp:HiddenField>
                                        <asp:TextBox ID="txtEscOrdValidity" Text='<%#Bind("OrderValidity") %>' onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Brk Qty" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtEscOrderQuant" onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server" Text='<%#Bind("Approved_OrderQty") %>'></asp:TextBox>
                                        <%--<asp:TextBox ID="txtEscMOQ" onkeypress="return isNumberKey(event,this);" CssClass="number1" Style="width: 80px; display: none;" runat="server"></asp:TextBox>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblEscOfferPrice" Text="Sys.Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtEscOfferPrice" ReadOnly="true" CssClass="number1" Style="width: 80px;" runat="server" Text='<%#Bind("New_OfferPrice") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblEscGPOffer" Text="GP(Sys)" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEscGP_Offer" runat="server" Text='<%#Bind("GP") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblEscApprovedPrice" Text="App. Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEscAppPrice" runat="server" Text='<%#Bind("New_OfferPrice") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblEscRecommendedPrice" Text="Rec. Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtEscRecPrice" Enabled="false" onkeypress="return isNumberKey(event,this);" onkeyup="setTextareaVal(event,this);" CssClass="number1" Style="width: 80px;" runat="server" Text='<%#Bind("Recommended_Price") %>'></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lblEscrecpricemsg" runat="server" Style="color: red;"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblEscNewOfferPrice" Text="Spl Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtEscNewOfferPrice" onkeypress="return isNumberKey(event,this);" onkeyup="setTextareaVal(event,this); AddEscGP(this);" CssClass="number1" Style="width: 80px;" runat="server"></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lblEscpricemsg" runat="server" Style="color: red;"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblEscDiscount" Text="Spl Pr Dsc" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtEscDiscount" onkeyup="setTextareaVal(event,this); AddGP(this);" onkeypress="return isNumberKey(event,this);" CssClass="number1" Style="width: 80px;" runat="server" ></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblEscGPSpecial" Text="GP(Spl)" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnEscCP" Value='<%#Bind("CP") %>' runat="server" />
                                        <asp:Label ID="lblEscGP_Special" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Last Updated Comment" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEscLastComment" Text='<%#Bind("StatusChange_Comment") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                        
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblEscComment" Text="Comment" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox Rows="2" Columns="20" ID="txtEscComment" TextMode="MultiLine" runat="server" onkeyup="setTextareaVal(event,this);"></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lblEscmsg" runat="server" Style="color: red;"></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblAttachments" Text="File Upload By CP" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="fileCP" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("Escalation_file") %>' Text='<%# Eval("Escalation_file_name") %>' OnCommand="fileBM_Command"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblAttachments" Text="File Upload By BM" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>

                                        <asp:LinkButton ID="fileBM" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("BM_Approval_file") %>' Text='<%# Eval("BM_Approval_file_name") %>' OnCommand="fileBM_Command"></asp:LinkButton>
                                        <br />
                                        <%--   <asp:ImageButton ID="btnFileUpload" runat="server" ToolTip="Select File" ImageUrl="~/images/upload_1.png" style="width:70px;" />
                                        <span id="spnFilePath"></span>
                                        <input type="file" id="FileUpload1" style="display: none" />--%>
                                        <div>
                                            <asp:Label runat="server" AssociatedControlId="fileUploadBM" Visible='<%# (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM")?true:false%>' >
                                            <img src="images/upload_1.png" style="width:70px;" /></asp:Label>
                                            <asp:FileUpload ID="fileUploadBM" CssClass="files" runat="server" style="display: none;" />
                                            <asp:Label runat="server" ID="filename"></asp:Label>
                                           <%-- <input class="files" id="fileUploadBM" name="fileUploadBM" style="display: none;" type="file" value="">
                                            <label id="filename">
                                            </label>--%>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblAttachments" Text="File Upload By HO" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="fileHO" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("HO_Approval_file") %>' Text='<%# Eval("HO_Approval_file_name") %>' OnCommand="fileBM_Command"></asp:LinkButton>
                                        <br />
                                        <div>
                                            <asp:Label runat="server" AssociatedControlId="fileUploadHO" Visible='<%# (Convert.ToString(Session["RoleId"])=="HO")?true:false%>'>
                                            <img src="images/upload_1.png" style="width:70px;" /></asp:Label>
                                            <asp:FileUpload ID="fileUploadHO" CssClass="filesHO" runat="server" style="display: none;" />
                                            <asp:Label runat="server" ID="filenameHO"></asp:Label>
                                         
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Last Updated Date" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEscLastDate" Text='<%#Bind("StatusChange_Date") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblEscRemarks" Text="Internal Remarks" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox Rows="2" Columns="20" ID="txtEscRemarks" TextMode="MultiLine" runat="server" onkeyup="setTextareaVal(event,this);"></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lblEscRmsg" runat="server" Style="color: red;"></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblEscAction" Text="Actions" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="width: 100px;">
                                            <%--    <asp:Button OnClientClick="return validateEscalationFields(event,this);" CommandName="Approve" CommandArgument='<%#Bind("ID") %>' Text="Approve" ID="btnEscApprove" CssClass="btn green gridbtn" runat="server" />
                                            <asp:Button OnClientClick="return validateEscalationFields(event,this);" CommandName="Reject" CommandArgument='<%#Bind("ID") %>' Text="Reject" ID="btnEscReject" CssClass="btn green gridbtn" runat="server" />
                                            <asp:Button runat="server" ID="btnView" CssClass="btn green gridbtn" Text="View Details" OnClientClick="OpenEscDetailView(event,this);" />
                                            --%>
                                            <asp:ImageButton CssClass="Escapprove" ToolTip="Approve" ImageUrl="images/icons/approve.png" OnClientClick="return validateEscalationFields(event,this);" CommandName="Approve" ID="btnEscApprove" runat="server" Style="height: 25px;" />
                                            <asp:ImageButton CssClass="Escreject" ToolTip="Reject" ImageUrl="images/icons/reject.png" OnClientClick="return validateEscalationFields(event,this);" CommandName="Reject" CommandArgument='<%#Bind("ID") %>' ID="btnEscReject" runat="server" Style="height: 25px;" />
                                            <asp:ImageButton ToolTip="View Details" ImageUrl="images/icons/view_details.png" runat="server" ID="btnView" OnClientClick="OpenEscDetailView(event,this);" Style="height: 25px;" />
                                            <asp:ImageButton CssClass="Escapprove internal"  Visible='<%# (Convert.ToString(Session["RoleId"])=="HO")?true:false%>' ToolTip="SendBackToManager" ImageUrl="images/next.png" OnClientClick="return validateEscalationFields(event,this);" CommandName="Approve" ID="imgSendBack" runat="server" Style="height: 25px;" />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                     <div class="col-md-12 nopad" id="divIntEscalated" runat="server" style="display: none;">
                        <asp:Label runat="server" ID="lblIEscresult" Style="color: red;"></asp:Label>
                        <asp:GridView ID="grdIntEscalatedQuote" Style="width: 100%;" CssClass="display compact" runat="server" AutoGenerateColumns="false" OnRowDataBound="grdIntEscalatedQuote_RowDataBound">
                            <Columns>

                                <asp:TemplateField HeaderText="Cust Name" ItemStyle-Width="80px" HeaderStyle-Width="80px">
                                    <ItemTemplate>
                                        <asp:Label ID="txtIEscCustName" Text='<%#Bind("RaisedBy") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Code">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnIEscID" runat="server" Value='<%# Eval("ID")%>' />
                                        <asp:HiddenField ID="hdnIEscStockCode" runat="server" Value='<%# Eval("StockCode")%>' />
                                        <asp:HiddenField ID="lblIEscref" runat="server" Value='<%#Bind("Ref_number") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hdnIEscLP" runat="server" Value='<%#Bind("List_Price") %>'></asp:HiddenField>
                                        
                                        <asp:Label ID="lblIEscitem" runat="server" Text='<%#Bind("Item_code") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIEscitemdesc" runat="server" Text='<%#Bind("Item_Desc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="End Customer" HeaderStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIEscendcust" runat="server" Text='<%#Bind("End_Customer") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Exp.Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtIEscEP" Enabled="false" Style="width: 80px;" runat="server" Text='<%#Bind("Expected_price") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Ord.Valdy" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="lblIEscQTYPO" Value='<%#Bind("QTY_perOrder") %>' runat="server"></asp:HiddenField>
                                        <asp:TextBox ID="txtIEscOrdValidity" Text='<%#Bind("OrderValidity") %>' onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Brk Qty" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtIEscOrderQuant" onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server" Text='<%#Bind("Approved_OrderQty") %>'></asp:TextBox>
                                        <%--<asp:TextBox ID="txtEscMOQ" onkeypress="return isNumberKey(event,this);" CssClass="number1" Style="width: 80px; display: none;" runat="server"></asp:TextBox>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblIEscOfferPrice" Text="Sys.Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtIEscOfferPrice" ReadOnly="true" CssClass="number1" Style="width: 80px;" runat="server" Text='<%#Bind("New_OfferPrice") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblIEscGPOffer" Text="GP(Sys)" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblIEscGP_Offer" runat="server" Text='<%#Bind("GP") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblIEscRecommendedPrice" Text="Rec. Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtIEscRecPrice" Enabled="false" onkeypress="return isNumberKey(event,this);" onkeyup="setTextareaVal(event,this);" CssClass="number1" Style="width: 80px;" runat="server" Text='<%#Bind("Recommended_Price") %>'></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lblIEscrecpricemsg" runat="server" Style="color: red;"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblIEscNewOfferPrice" Text="Spl Price" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtIEscNewOfferPrice" onkeypress="return isNumberKey(event,this);" onkeyup="setTextareaVal(event,this); AddIEscGP(this);" CssClass="number1" Style="width: 80px;" runat="server"></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lblIEscpricemsg" runat="server" Style="color: red;"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblIEscDiscount" Text="Spl Pr Dsc" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtIEscDiscount" onkeypress="return isNumberKey(event,this);" onkeyup="setTextareaVal(event,this); AddIEscGP(this);" CssClass="number1" Style="width: 80px;" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblIEscGPSpecial" Text="GP(Spl)" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnIEscCP" Value='<%#Bind("CP") %>' runat="server" />
                                        <asp:Label ID="lblIEscGP_Special" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblIEscComment" Text="Comment" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox Rows="2" Columns="20" ID="txtIEscComment" TextMode="MultiLine" runat="server" onkeyup="setTextareaVal(event,this);"></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lblIEscmsg" runat="server" Style="color: red;"></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblIAttachments" Text="File Upload By CP" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="IfileCP" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("Escalation_file") %>' Text='<%# Eval("Escalation_file_name") %>' OnCommand="fileBM_Command"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblIAttachments" Text="File Upload By BM" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>

                                        <asp:LinkButton ID="IfileBM" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("BM_Approval_file") %>' Text='<%# Eval("BM_Approval_file_name") %>' OnCommand="fileBM_Command"></asp:LinkButton>
                                        <br />
                                        <div>
                                            <asp:Label runat="server" AssociatedControlId="IfileUploadBM" Visible='<%# (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM")?true:false%>' >
                                            <img src="images/upload_1.png" style="width:70px;" /></asp:Label>
                                            <asp:FileUpload ID="IfileUploadBM" CssClass="files" runat="server" style="display: none;" />
                                            <asp:Label runat="server" ID="Ifilename"></asp:Label>
                                           <%-- <input class="files" id="fileUploadBM" name="fileUploadBM" style="display: none;" type="file" value="">
                                            <label id="filename">
                                            </label>--%>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblIAttachments" Text="File Upload By HO" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="IfileHO" CssClass="lbl_color" runat="server" CommandArgument='<%# Eval("HO_Approval_file") %>' Text='<%# Eval("HO_Approval_file_name") %>' OnCommand="fileBM_Command"></asp:LinkButton>
                                        <br />
                                        <div>
                                            <asp:Label runat="server" AssociatedControlId="IfileUploadHO" Visible='<%# (Convert.ToString(Session["RoleId"])=="HO")?true:false%>'>
                                            <img src="images/upload_1.png" style="width:70px;" /></asp:Label>
                                            <asp:FileUpload ID="IfileUploadHO" CssClass="filesHO" runat="server" style="display: none;" />
                                            <asp:Label runat="server" ID="IfilenameHO"></asp:Label>
                                         
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label Text="Last Updated Date" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblIEscLastDate" Text='<%#Bind("StatusChange_Date") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblIEscRemarks" Text="Internal Remarks" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox Rows="2" Columns="20" ID="txtIEscRemarks" TextMode="MultiLine" runat="server" onkeyup="setTextareaVal(event,this);"></asp:TextBox>
                                        <br />
                                        <asp:Label ID="lblIEscRmsg" runat="server" Style="color: red;"></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblIEscAction" Text="Actions" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="width: 100px;">
                                            <asp:ImageButton CssClass="Escapprove" ToolTip="Approve" ImageUrl="images/icons/approve.png" OnClientClick="return validateIEscalationFields(event,this);" CommandName="Approve" ID="btnIEscApprove" runat="server" Style="height: 25px;" />
                                           <asp:ImageButton CssClass="Escreject"  Visible='<%# (Convert.ToString(Session["RoleId"])=="HO")?true:false%>' ToolTip="Reject" ImageUrl="images/icons/reject.png" OnClientClick="return validateIEscalationFields(event,this);" CommandName="Reject" CommandArgument='<%#Bind("ID") %>' ID="btnIEscReject" runat="server" Style="height: 25px;" />
                                            <asp:ImageButton ToolTip="View Details" ImageUrl="images/icons/view_details.png" runat="server" ID="btnIView" OnClientClick="OpenIEscDetailView(event,this);" Style="height: 25px;" />
                                            <asp:ImageButton CssClass="Escapprove internal"  Visible='<%# (Convert.ToString(Session["RoleId"])=="HO")?true:false%>' ToolTip="SendBackToManager" ImageUrl="images/next.png" OnClientClick="return validateIEscalationFields(event,this);" CommandName="Approve" ID="imgISendBack" runat="server" Style="height: 25px;" />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="loader_div" class="loader_div"></div>
</asp:Content>
