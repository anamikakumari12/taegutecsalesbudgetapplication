﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SpecialQuote.aspx.cs" Inherits="TaegutecSalesBudget.SpecialQuote" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link href="css/Tabs.css" rel="stylesheet" />
    <style>
        .row {
            margin-left: 15px;
            margin-right: 15px;
            margin-top:15px;
        }

        .col-md-2 {
            width: 15%;
        }

        .btncontrol {
            padding-left: 5px;
            padding-top: 10px;
        }

        .btn.green {
            margin-top: 10px;
        }

        .gridbutton {
            /*padding: 5px!important;*/
            top: auto;
            overflow: initial;
            padding: 3px 20px;
            border-radius: 3px !important;
            background: #0273ab;
            color: #fff;
            border: none;
            margin: 1px;
        }

        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px !important;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
            font-size: 100%;
            font-weight: 700;
        }

        .control {
            padding-top: 5px;
        }

        .popupControl {
            margin: 5px;
            float: right;
        }

        th,
        td {
            white-space: nowrap;
        }

        div.dataTables_wrapper {
            /*width: 800px;*/
            margin: 0 auto;
        }
    </style>

    <script>
        function validateFields() {
            debugger;
            var errorflag = 0;
            if (!isNullorEmpty($('#body_ddlCustomerType').val())) {
                $('#body_ddlCustomerType').css("border", 'solid 1px red');
                errorflag++;
            }
            else {
                $('#body_ddlCustomerType').css("border", 'solid 1px #ccc');
            }
            if (!isNullorEmpty($('#body_ddlCustomers').val())) {
                $('#body_ddlCustomers').css("border", 'solid 1px red');
                errorflag++;
            }
            else {
                $('#body_ddlCustomers').css("border", 'solid 1px #ccc');
            }
            var obj = $('#body_ddlCat1');
            errorflag += checkForCat(obj);
            obj = $('#body_ddlCat2');
            errorflag += checkForCat(obj);
            errorflag += checkForCat($('#body_ddlCat3'));
            errorflag += checkForCat($('#body_ddlCat4'));
            errorflag += checkForCat($('#body_ddlCat5'));
            errorflag += checkForCat($('#body_ddlCat6'));
            errorflag += checkForCat($('#body_ddlCat7'));
            errorflag += checkForCat($('#body_ddlCat8'));
            if (errorflag > 0) {
                $('#divItem').attr("style", "display:none;")

            }
            else {
                $('#divItem').attr("style", "display:block;")
            }
            return errorflag;
        }
        function validateAllFields() {
            debugger;
            var errorflag = 0;
            errorflag = validateFields();
            errorflag += checkForCat($('#body_txtItem'));
            if (errorflag > 0) {
                return false;
            }
            else
                return true;
        }
        function checkForCat(obj) {
            if (obj.length > 0)
                if (obj.is(':visible'))
                    if (!isNullorEmpty(obj.val())) {
                        obj.css("border", 'solid 1px red');
                        return 1;
                    }
                    else {
                        obj.css("border", 'solid 1px #ccc');
                        return 0;
                    }
                else
                    return 0;
            else
                return 0;
        }
        function isNullorEmpty(val) {
            if (val == undefined || val == '' || val == null || val == '-- Select Type --' || val == '-- Select --')
                return false;
            else
                return true;
        }

        $('#body_btnSave').on('click', function (evt, obj) {
            //debugger;
            jQuery(".loader_div").show();
            if (validateAllFields()) {
                var totaltrCount = $("[id*=grdPriceRequest] tr").length;
                var trCount = $("[id*=grdPriceRequest] td").closest("tr").length;
                var param;
                var paramList = [];
                //for (rowCount = 0; rowCount < trCount; rowCount++) {
                //    item_id = $("[id*=grdPriceRequest] td").closest("tr").find(".ddl_item").get(rowCount).id;

                //    i = item_id.substring(item_id.lastIndexOf("_") + 1, item_id.length)
                param = {
                    Cust_type: $('#body_ddlCustomerType').val(),
                    Cust_number: $('#body_ddlCustomers').val(),
                    CPCust_number: $('#body_ddlCPCustomers').val(),
                    Cat1: $('#body_ddlCat1').val() == undefined ? '' : $('#body_ddlCat1').val(),
                    Cat2: $('#body_ddlCat2').val() == undefined ? '' : $('#body_ddlCat2').val(),
                    Cat3: $('#body_ddlCat3').val() == undefined ? '' : $('#body_ddlCat3').val(),
                    Cat4: $('#body_ddlCat4').val() == undefined ? '' : $('#body_ddlCat4').val(),
                    Cat5: $('#body_ddlCat5').val() == undefined ? '' : $('#body_ddlCat5').val(),
                    Cat6: $('#body_ddlCat6').val() == undefined ? '' : $('#body_ddlCat6').val(),
                    Cat7: $('#body_ddlCat7').val() == undefined ? '' : $('#body_ddlCat7').val(),
                    Cat8: $('#body_ddlCat8').val() == undefined ? '' : $('#body_ddlCat8').val(),
                    Item_type: 'Special',
                    Item_name: $('#body_txtItem').val(),
                    ref_item: $('#body_ddlItem').val()

                }
                paramList.push(param);
                //}
                var dataParam = {
                    obj: paramList
                }

                $.ajax({
                    url: 'SpecialQuote.aspx/CreateRFQ',
                    method: 'post',
                    datatype: 'json',
                    data: JSON.stringify(dataParam),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        alert(data.d.ErrorMsg);
                        location.reload(true);
                        jQuery(".loader_div").hide();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                        jQuery(".loader_div").hide();
                    }
                });
            }
            else {
                alert("Please check error message.");
                jQuery(".loader_div").hide();
            }


        });
        function AddRow() {
            //debugger;
            if (validateAllFields()) {
                var param;
                var paramList = [];
                param = {
                    Cust_type: $('#body_ddlCustomerType').val(),
                    Cust_number: $('#body_ddlCustomers').val(),
                    CPCust_number: $('#body_ddlCPCustomers').val(),
                    Cat1: $('#body_ddlCat1').val() == undefined ? '' : $('#body_ddlCat1').val(),
                    Cat2: $('#body_ddlCat2').val() == undefined ? '' : $('#body_ddlCat2').val(),
                    Cat3: $('#body_ddlCat3').val() == undefined ? '' : $('#body_ddlCat3').val(),
                    Cat4: $('#body_ddlCat4').val() == undefined ? '' : $('#body_ddlCat4').val(),
                    Cat5: $('#body_ddlCat5').val() == undefined ? '' : $('#body_ddlCat5').val(),
                    Cat6: $('#body_ddlCat6').val() == undefined ? '' : $('#body_ddlCat6').val(),
                    Cat7: $('#body_ddlCat7').val() == undefined ? '' : $('#body_ddlCat7').val(),
                    Cat8: $('#body_ddlCat8').val() == undefined ? '' : $('#body_ddlCat8').val(),
                    Item_type: 'Special',
                    Item_name: $('#body_txtItem').val(),
                    ref_item: $('#body_ddlItem').val()

                }
                paramList.push(param);
                //}
                var dataParam = {
                    obj: paramList
                }
                LoadData(dataParam);
            }
            else {
                alert("Please check error message.");
            }
        }
        function LoadData(msg) {
            debugger;
            console.log(msg);
            console.log(msg.obj);
            msg = msg.obj;
            $('#grdSplItemDetails tbody').html("");
            for (var i = 0; i < msg.length; i++) {
                $("#grdSplItemDetails tbody ").append(" <tr>  <td>" +
                    msg[i].Item_type + "</td>  <td>" +
                    msg[i].Item_name + "</td>  <td>" +
                    msg[i].ref_item + "</td>  <td>" +
                    msg[i].Cust_type + "</td>  <td>" +
                    msg[i].Cust_number + "</td>  <td>" +
                    msg[i].Cat1 + "</td>  <td>" +
                    msg[i].Cat2 + "</td>  <td>" +
                    msg[i].Cat3 + "</td>  <td>" +
                    msg[i].Cat4 + "</td>  <td>" +
                    msg[i].Cat5 + "</td>  <td>" +
                    msg[i].Cat6 + "</td>  <td>" +
                    msg[i].Cat7 + "</td>  <td>" +
                    msg[i].Cat8 + "</td>  <td>" +
                    "</td>  </tr>");
            }
            debugger;
            if ($.fn.dataTable.isDataTable('#grdSplItemDetails')) {
                //$('#grdDetailedPriceSummary1').DataTable().destroy();
                //table1.destroy();
                //table1 = $('#grdDetailedPriceSummary1').DataTable({
                //    //destroy: true,
                //});
            }
            else {
                //table1.destroy();
                table1 = $('#grdSplItemDetails').DataTable({
                    // destroy: true
                });
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Special Quote</a>
                        </li>
                        <li class="current">Special Item Price</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
    <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">

        <ContentTemplate>

            <div class="col-md-12 mn_margin">
                <div id="collapsebtn" class="row">
                    <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />
                </div>
                <div class="row filter_panel" id="reportdrpdwns" runat="server">


                    <div class="col-md-12 nopadding">

                        <div class="col-md-2 control">
                            <asp:Label runat="server" ID="lblCType" CssClass="label">Customer Type</asp:Label>
                            <asp:DropDownList runat="server" ID="ddlCustomerType" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerType_SelectedIndexChanged">
                                <asp:ListItem Text="-- Select Type --" Value=""></asp:ListItem>
                                <asp:ListItem Text="Customers" Value="C"></asp:ListItem>
                                <asp:ListItem Text="Distributors" Value="D"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2 control">
                            <asp:Label runat="server" ID="lblCustomers" CssClass="label"></asp:Label>
                            <asp:DropDownList runat="server" ID="ddlCustomers" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomers_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 control" id="divCPCustomers" visible="false" runat="server">
                            <asp:Label runat="server" ID="lblCPCustomers" CssClass="label">Customers of Distributors</asp:Label>
                            <asp:DropDownList runat="server" ID="ddlCPCustomers" CssClass="control_dropdown"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 control">
                            <asp:Label runat="server" ID="lblCat1" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat1" />
                            <asp:DropDownList runat="server" ID="ddlCat1" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat1_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 control">
                            <asp:Label runat="server" ID="lblCat2" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat2" />
                            <asp:DropDownList runat="server" ID="ddlCat2" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat2_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 control">
                            <asp:Label runat="server" ID="lblCat3" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat3" />
                            <asp:DropDownList runat="server" ID="ddlCat3" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat3_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 control">
                            <asp:Label runat="server" ID="lblCat4" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat4" />
                            <asp:DropDownList runat="server" ID="ddlCat4" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat4_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 control">
                            <asp:Label runat="server" ID="lblCat5" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat5" />
                            <asp:DropDownList runat="server" ID="ddlCat5" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat5_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 control">
                            <asp:Label runat="server" ID="lblCat6" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat6" />
                            <asp:DropDownList runat="server" ID="ddlCat6" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat6_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 control">
                            <asp:Label runat="server" ID="lblCat7" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat7" />
                            <asp:DropDownList runat="server" ID="ddlCat7" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat7_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 control">
                            <asp:Label runat="server" ID="lblCat8" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat8" />
                            <asp:DropDownList runat="server" ID="ddlCat8" CssClass="control_dropdown" Visible="false"></asp:DropDownList>
                        </div>

                        <div class="col-md-12 btncontrol">
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                            <asp:Label ID="lblErrorMsg" runat="server" Style="color: red;"></asp:Label>
                            <asp:Button ID="btnFilter" Style="width: 10%; float: right;" runat="server" Text="Filter" CssClass="btn green" OnClientClick="validateFields();" OnClick="btnFilter_Click" />
                        </div>
                    </div>



                </div>
                <div class="row filter_panel" runat="server" id="divItem" visible="false">


                    <div class="col-md-12 nopadding">

                        <div class="col-md-2 control" style="padding-top: 20px; padding-left: 0; padding-right: 0;">

                            <div class="col-md-6 control" style="padding: 0; font-weight: 700;">
                                <asp:RadioButton ID="RadioButton1" Style="padding: 0; font-weight: 700;" Checked="true" Enabled="false" runat="server" Text="Special Item" GroupName="Itemtype" TextAlign="Left" />
                            </div>
                            <div class="col-md-6 control" style="padding: 0; font-weight: 700;">
                                <asp:RadioButton ID="RadioButton2" Style="padding: 0; font-weight: 700;" runat="server" Enabled="false" Text="Catalogue Item" GroupName="Itemtype" TextAlign="Left" />
                            </div>

                        </div>
                        <div class="col-md-2 control">
                            <asp:Label runat="server" ID="lblItem" CssClass="label">Special Item</asp:Label>
                            <asp:TextBox CssClass="control_dropdown" Enabled="false" runat="server" ID="txtItem"></asp:TextBox>
                        </div>
                        <div class="col-md-2 control">
                            <asp:Label runat="server" ID="lblOfferPrice" CssClass="label">OFFER PRICE</asp:Label>
                            <asp:TextBox runat="server" ID="txtPrice" CssClass="control_dropdown" Enabled="false"></asp:TextBox>
                        </div>
                        <%--  <div class="col-md-2 control">
                     <asp:Label runat="server" ID="Label1" CssClass="label">Reference Items</asp:Label>
                    <asp:DropDownList CssClass="control_dropdown" runat="server" ID="ddlItem"></asp:DropDownList>
                </div>--%>
                        <div class="col-md-5 control">
                            <asp:Label ID="Label2" runat="server" Style="color: red;"></asp:Label>
                            <asp:Button runat="server" Visible="false" Style="width: 25%; float: right;" ID="btnAdd" CssClass="btn btn-success button add" Text="Add Item" OnClientClick="AddRow(); return false;" />
                        </div>
                        <div class="col-md-1 control">

                            <asp:Button runat="server" Visible="false" Style="float: right;" ID="btnSave" CssClass="btn btn-success button" Text="Submit" OnClientClick="return false;" />

                        </div>
                    </div>
                </div>

            </div>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>


</asp:Content>


