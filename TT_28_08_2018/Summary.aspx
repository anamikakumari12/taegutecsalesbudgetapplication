﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Summary.aspx.cs" Inherits="TaegutecSalesBudget.WebForm7" Async="true" ViewStateMode="Disabled" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="GridviewScroll.css" rel="stylesheet" />
    <script type="text/javascript" src="gridscroll.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360">
    </asp:ScriptManager>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Budget</a>
                    </li>
                    <li>Summary</li>
                    <div>
                        <ul>
                            <li class="title_bedcrum" style="list-style: none;">SUMMARY</li>
                        </ul>
                    </div>
                    <div>
                        <ul style="float: right; list-style: none; margin-top: -4px; width: 247px; margin-right: -5px;" class="alert alert-danger fade in">
                            <li>
                                <span style="margin-right: 4px; vertical-align: text-bottom;">Val In '000</span>
                                <asp:RadioButton ID="rbtn_Thousand" OnCheckedChanged="rbtn_Thousand_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" Checked="True" />
                                <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">Val In Lakhs</span>
                                <asp:RadioButton ID="rbtn_Lakhs" OnCheckedChanged="rbtn_Lakhs_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" />
                            </li>
                        </ul>
                    </div>
                </ul>
            </div>
            <!-- End : Breadcrumbs -->
            <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />
            </div>
            <div class="row filter_panel" id="reportdrpdwns">

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label">CUSTOMER TYPE </label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="ddlcustomertype" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlcustomertype_SelectedIndexChanged"
                                CssClass="form-control" Width="230px" ViewStateMode="Enabled">
                                <asp:ListItem Text="ALL" Value="ALL" />
                                <asp:ListItem Text="CUSTOMER" Value="C" />
                                <asp:ListItem Text="CHANNEL PARTNER" Value="D" />

                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label">CUSTOMER NAME </label>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlCustomerList" runat="server" CssClass="form-control" Width="230px" ViewStateMode="Enabled">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="form-group">
                        <label class="col-md-4 control-label " style="width: 36%;">CUSTOMER NUMBER</label>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlCustomerNumber" runat="server" CssClass="form-control" Width="230px" ViewStateMode="Enabled">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 " style="margin-top: 10px;">
                    <div class="form-group">
                        <div class="col-md-3">
                            <asp:Button ID="summary" runat="server" Text="FILTER" OnClick="summary_Click" CssClass="btn green" />
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
                <%--     CssClass="table table-bordered table-hover responsive" --%>
                <asp:GridView ID="grdviewAllValues" runat="server" AutoGenerateColumns="False" ViewStateMode="Enabled" ShowHeader="false" Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/button_plus.gif" Visible='<%# (Eval("sumFlag").ToString() == "SubFamilyHeading") || (Eval("sumFlag").ToString() == "FamilyHeading") %>' ImageAlign="Left" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="GOLD">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblgold" Text='<%#( Eval("gold_flag").ToString()=="" ?"" :"GOLD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--  <asp:TemplateField HeaderText="TOP" >
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbltop" Text='<%# (Eval("top_flag").ToString()=="" ?"" :"TOP") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="5yrs" >
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl5yrs" Text='<%# (Eval("five_years_flag").ToString()=="" ?"" :"5yrs") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="BB">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblbb" Text='<%# (Eval("bb_flag").ToString()=="" ?"" :"BB") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="SPC" >
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblspc" Text='<%#( Eval("SPC_flag").ToString()=="" ?"" :"SPC") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="10yrs">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl10y" Text='<%#( Eval("ten_years_flag").ToString()=="" ?"" :"10yrs") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SFEED">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblsfeed" Text='<%#( Eval("SFEED_flag").ToString()=="" ?"" :"SFEED") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="1" ItemStyle-Width="35px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ins" Text="Ins." Visible='<%# (Eval("insert_or_tool_flag").ToString() =="I" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="lbl_tool" Text="P" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="P" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label1" Text="S" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="S" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label2" Text="Tools" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="T" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label3" Text="X" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="X" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label4" Text="" Visible='<%# (Eval("insert_or_tool_flag").ToString() =="" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label5" Text='<%# (Eval("item_sub_family_id").ToString()) %>' Visible='<%# (Eval("insert_or_tool_flag").ToString() =="SubFamHeading") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblproductcode" Text='<%# Eval("item_code".ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="T_CLAMP_PARTING_OFF ffffffffffffffffffffffff" ItemStyle-Width="240px"><%--HeaderStyle-CssClass="greendark">--%>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblFmilyName" Text='<%# (Eval("item_family_name").ToString()) %>' Visible='<%#Eval("sumFlag").ToString() == "FamilyHeading" || Eval("sumFlag").ToString() == "HidingHeading"  %>' CssClass="productLabel"></asp:Label>
                                <asp:Label runat="server" ID="lblProductName" Text='<%# (Eval("item_description").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblSumFlag" runat="server" Text='<%# Eval("sumFlag").ToString()%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="color_3" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_item_code" Text='<%# (Eval("item_code").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="60px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualQuantity_sales_qty_year_2" Text='<%# (Eval("sales_qty_year_2").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="60px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualQuantity_sales_qty_year_1" Text='<%# (Eval("sales_qty_year_1").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="60px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualQuantity_sales_qty_year_0" Text='<%# (Eval("sales_qty_year_0").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_ActualQuantity_sales_qty_year_0" Value='<%# (Eval("sales_qty_year_0").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txt_ActualQuantity_sales_qty_year_P" CssClass="form-control valuebud" MaxLength="6" Text='<%# Eval("estimate_qty_next_year") %>'
                                    Visible='<%# Eval("sumFlag").ToString() != "SubFamilyHeading" && Eval("sumFlag").ToString() != "FamilyHeading" &&Eval("sumFlag").ToString() != "HidingHeading" && Eval("sumFlag").ToString() != ""%>' Width="90px"
                                    Enabled="false"></asp:TextBox>
                                <asp:Label runat="server" ID="lbl_ActualQuantity_sales_qty_year_P" Text='<%# Eval("estimate_qty_next_year") %>' Visible='<%# Eval("sumFlag").ToString() == "SubFamilyHeading"  || (Eval("sumFlag").ToString() == "HidingHeading") %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- Inserts per tool --%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_insertpertool_year_1" Text='<%# (Eval("insertpertool_year_1").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_insertpertool_year_1" Value='<%# (Eval("insertpertool_year_1").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px" Visible="false">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_insertpertool_year_0" Text='<%# (Eval("insertpertool_year_0").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_insertpertool_year_0" Value='<%# (Eval("insertpertool_year_0").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_insertpertool_next_year" Text='<%# (Eval("insertpertool_next_year").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_insertpertool_next_year" Value='<%# (Eval("insertpertool_next_year").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_QuantityVariance_sales_qty_year_1" Text='<%# Eval("QuantityVariance_sales_qty_year_1")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="90px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_QuantityVariance_sales_qty_year_0" Text='<%# Eval("QuantityVariance_sales_qty_year_0").ToString() %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_QuantityPercentage_sales_qty_year_1" Text='<%# Eval("QuantityPercentage_sales_qty_year_1") %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_QuantityPercentage_sales_qty_year_0" Text='<%# Eval("QuantityPercentage_sales_qty_year_0").ToString() %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_2" Text='<%# (Eval("sales_value_year_2").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_1" Text='<%# (Eval("sales_value_year_1").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_0" Text='<%# (Eval("sales_value_year_0").ToString()) %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_ActualValue_sales_value_year_0" Value='<%# (Eval("sales_value_year_0").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_P_New" Text='<%# Eval("estimate_value_next_year") %>'
                                    Visible='<%# Eval("sumFlag").ToString() != "SubFamilyHeading" && Eval("sumFlag").ToString() != "FamilyHeading" && Eval("sumFlag").ToString() != "" && Eval("sumFlag").ToString() != "HidingHeading" %>' Width="90px"></asp:Label>
                                <asp:TextBox runat="server" ID="txt_ActualValue_sales_value_year_P" CssClass="form-control valuebud" MaxLength="28" Text='<%# Eval("estimate_value_next_year") %>' Style="display: none"
                                    Enabled="false"></asp:TextBox>
                                <asp:Label runat="server" ID="lbl_ActualValue_sales_value_year_P" Text='<%# (Eval("estimate_value_next_year").ToString()) %>' Visible='<%# Eval("sumFlag").ToString() == "SubFamilyHeading"  || (Eval("sumFlag").ToString() == "HidingHeading") %>'> </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- Percentage Value--%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ValuePercentage_sales_value_year_1" Text='<%# Eval("ValuePercentage_sales_value_year_1")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_5">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ValuePercentage_sales_value_year_0" Text='<%# Eval("ValuePercentage_sales_value_year_0")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- Price per Unit --%>
                         <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_PricePerUnit_sales_value_year_2" Text='<%#Eval("PricePerUnit_sales_value_year_2") %>'>  </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_PricePerUnit_sales_value_year_1" Text='<%#Eval("PricePerUnit_sales_value_year_1") %>'>  </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_PricePerUnit_sales_value_year_0" Text='<%#Eval("PricePerUnit_sales_value_year_0") %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_PricePerUnit_sales_value_year_0" Value='<%#Eval("PricePerUnit_sales_value_year_0") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" ItemStyle-Width="80px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txt_PricePerUnit_sales_value_year_P" CssClass="form-control valuebud" MaxLength="28" Text='<%# Eval("PricePerUnit_sales_value_year_P") %>'
                                    Visible='<%# Eval("sumFlag").ToString() != "SubFamilyHeading" && Eval("sumFlag").ToString() != "FamilyHeading" && Eval("sumFlag").ToString() != "" && Eval("sumFlag").ToString() != "HidingHeading" %>' Width="90px"
                                    Enabled="false"></asp:TextBox>
                                <asp:Label runat="server" ID="lbl_PricePerUnit_sales_value_year_P" Text='<%#Eval("PricePerUnit_sales_value_year_P") %>' Visible='<%# Eval("sumFlag").ToString() == "SubFamilyHeading"  || (Eval("sumFlag").ToString() == "HidingHeading")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- Price Changes --%>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2" Visible="false">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_priceschange_sales_value_year_2" Text='<%#Eval("priceschange_sales_value_year_2") %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_priceschange_sales_value_year_1" Text='<%#Eval("priceschange_sales_value_year_1") %>'>  </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_priceschange_sales_value_year_0" Text='<%#Eval("priceschange_sales_value_year_0") %>'> </asp:Label>
                                <asp:HiddenField runat="server" ID="hdn_priceschange_sales_value_year_0" Value='<%#Eval("priceschange_sales_value_year_0") %>' />
                                <%--Hidden values--%>
                                <asp:Label runat="server" ID="lbl_Family_Id" Text='<%# Eval("item_family_id").ToString() %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lbl_Sub_Family_Id" Text='<%# Eval("item_sub_family_id").ToString() %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lbl_Customer_region" Text='<%# Eval("Customer_region").ToString() %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lblItemCode" Text='<%# (Eval("item_code").ToString()) %>' Style="display: none"> </asp:Label>
                                <asp:Label runat="server" ID="lbl_SumFlag" Text='<%# Eval("sumFlag").ToString()%>' Style="display: none" />
                                <asp:HiddenField runat="server" ID="hdn_SumFlag" Value='<%# Eval("sumFlag").ToString()%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            </label>
             </label>
        </ContentTemplate>
        <Triggers>
            <%-- <asp:AsyncPostBackTrigger ControlID="ddlCustomerList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlCustomerNumber" EventName="SelectedIndexChanged" />--%>
            <asp:AsyncPostBackTrigger ControlID="ddlcustomertype" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="summary" EventName="Click" />

        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <script type="text/javascript">
        $(document).ready(function () {
            //$("#MainContent_ddlcustomertype").searchable();        
            //$("#MainContent_ddlCustomerList").searchable();
            //$("#MainContent_ddlCustomerNumber").searchable();

            $('#MainContent_ddlCustomerList').change(function () {
                var ddlslectedText = $("#MainContent_ddlCustomerList option:selected").val();
                $("#MainContent_ddlCustomerNumber").val(ddlslectedText);
            });
            $('#MainContent_ddlCustomerNumber').change(function () {

                var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
                $("#MainContent_ddlCustomerList").val(ddlslectedText);
            });

            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
            triggerPostGridLodedActions();
            $(window).resize(function () {
                triggerPostGridLodedActions();
            });

        });
        function triggerPostGridLodedActions() {

            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
            //$("#MainContent_ddlcustomertype").searchable();
            //$("#MainContent_ddlCustomerList").searchable();
            //$("#MainContent_ddlCustomerNumber").searchable();

            $('#MainContent_ddlCustomerList').change(function () {
                var ddlslectedText = $("#MainContent_ddlCustomerList option:selected").val();
                $("#MainContent_ddlCustomerNumber").val(ddlslectedText);
            });
            $('#MainContent_ddlCustomerNumber').change(function () {

                var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
                $("#MainContent_ddlCustomerList").val(ddlslectedText);
            });

            gridviewScroll();
            bindToogleForRowIndex();
            bindToogleForParentRowIndex();

            //$("#MainContent_grdviewAllValuesPanelHeaderContent").css({ 'height': '0px' });
            //$("#MainContent_grdviewAllValuesPanelHeader").css({ 'height': '0px' });
            var td = $("#MainContent_grdviewAllValuesCopy").find("tr:first").find("td:first");
            $(td).find('div').attr("style", "min-width:25px");
            td = $("#MainContent_grdviewAllValuesCopyFreeze").find("tr:first").find("td:first");
            $(td).find('div').attr("style", "min-width:25px");
        }

        function gridviewScroll() {

            gridView1 = $('#MainContent_grdviewAllValues').gridviewScroll({
                width: $(window).width() - 70,
                height: findminofscreenheight($(window).height(), 450),
                railcolor: "#F0F0F0",
                barcolor: "#606060",
                barhovercolor: "#606060",
                bgcolor: "#F0F0F0",
                freezesize: 9,
                arrowsize: 30,
                varrowtopimg: "Images/arrowvt.png",
                varrowbottomimg: "Images/arrowvb.png",
                harrowleftimg: "Images/arrowhl.png",
                harrowrightimg: "Images/arrowhr.png",
                headerrowcount: 2,
                railsize: 16,
                barsize: 14,
                verticalbar: "auto",
                horizontalbar: "auto",
                wheelstep: 1,
            });
        }
        function findminofscreenheight(a, b) {
            a = a - $("#MainContent_ddlcustomertype").offset().top;
            return a < b ? a : b;
        }
        $('#MainContent_grdView_T_Clamp_Partingoff tr:last').addClass("last_row");
        //jQuery(".parent_row_index td div img").click();
        function bindToogleForRowIndex() {
            jQuery(".row_index_image").click(function () {

                var currentRowElement = jQuery(this).closest(".row_index");
                var currentIndex = jQuery(currentRowElement).data("index");
                var minimised = false;
                jQuery(".subrowindex_" + currentIndex).each(function () {
                    if (jQuery(this).css("display") != "none") {
                        jQuery(this).slideUp();
                        minimised = true;
                    }
                    else {
                        jQuery(this).slideDown();
                        minimised = false;
                    }
                });
                if (minimised) {
                    jQuery(this).attr("src", "images/button_plus.gif");
                    jQuery(currentRowElement).data("toogle_status", "MINIMISED");
                }
                else {
                    jQuery(this).attr("src", "images/button_minus.gif");
                    jQuery(currentRowElement).data("toogle_status", "MAXIMISED");
                }
            });

            //jQuery(".row_index_image").click();
        }


        function bindToogleForParentRowIndex() {
            jQuery(".parent_row_index td div img").click(function () {

                var currentIndex = jQuery(this).closest(".parent_row_index").data("parent_row_index");
                var minimised = false;
                jQuery(".parent_row_index_" + currentIndex).each(function () {
                    if (jQuery(this).data("toogle_status") == "MAXIMISED")
                        jQuery(this).find("td div img").click();
                    var nextElement = jQuery(this).prev();
                    if (jQuery(this).css("display") != "none") {
                        jQuery(this).slideUp();
                        if (jQuery(nextElement).hasClass("empty_row"))
                            jQuery(nextElement).slideUp();
                    }
                    else {
                        jQuery(this).slideDown();
                        if (jQuery(nextElement).hasClass("empty_row"))
                            jQuery(nextElement).slideDown();
                        minimised = true;
                    }
                });
                if (minimised)
                    jQuery(this).attr("src", "images/button_minus.gif");
                else
                    jQuery(this).attr("src", "images/button_plus.gif");
            });
            //jQuery(".parent_row_index td div img").click();
        }

        //toogle row hide and show

        jQuery(".row_index").click(function () {

            var currentIndex = jQuery(this).data("index");

            jQuery(".subrowindex_" + currentIndex).each(function () {
                if (jQuery(this).css("display") != "none")
                    jQuery(this).slideUp();
                else
                    jQuery(this).slideDown();
            });
        });
    </script>
    <style type="text/css">
        td {
            height: 45px !important;
        }

        .noclose .ui-dialog-titlebar-close {
            display: none;
        }

        .row {
            margin-left: 10px;
            margin-right: -15px;
        }

        .control-label {
            font-weight: normal;
            padding-top: 9px;
        }

        td {
            height: 48px !important;
        }
    </style>
</asp:Content>
