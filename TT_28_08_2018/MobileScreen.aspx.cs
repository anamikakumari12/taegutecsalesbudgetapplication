﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.PowerBI.Api;
using Microsoft.PowerBI.Api.Models;

namespace TaegutecSalesBudget
{
    public partial class MobileScreen : System.Web.UI.Page
    {

            public string embedToken;
            public string embedUrl;
            public Guid reportId;
            CommonFunctions objCom = new CommonFunctions();
            public static string cter;
            public string report_id = "";
            protected void Page_Load(object sender, EventArgs e)
            {
                if (!IsPostBack)
                {
                    
                        try
                        {
                    System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                    using (var client = new PowerBIClient(new Uri(Configurations.ApiUrl), Authentication.GetTokenCredentials()))
                            {
                                // Get a list of reports
                                var reports = client.Reports.GetReportsInGroup(Configurations.WorkspaceId);

                                //// Populate dropdown list
                                foreach (Report item in reports.Value)
                                {
                                    // ddlReport.Items.Add(new ListItem(item.Name, item.Id.ToString()));
                                    if (item.Name == BMSResource.MonthlyReview)
                                    {
                                        report_id = item.Id.ToString();
                                    }
                                }
                                ViewState["report_id"] = report_id;
                                //// Select first item
                                //ddlReport.SelectedIndex = 2;
                            }
                            // Generate an embed token and populate embed variables
                            LoadReport();

                        }
                        catch (Exception ex)
                        {
                            objCom.LogError(ex);
                        }
                    
                }

            }


            protected void LoadReport()
            {
                try
                {
                    report_id = Convert.ToString(ViewState["report_id"]);
                    using (var client = new PowerBIClient(new Uri(Configurations.ApiUrl), Authentication.GetTokenCredentials()))
                    {

                        // Retrieve the selected report
                        var report = client.Reports.GetReportInGroup(Configurations.WorkspaceId, new Guid(report_id));

                        // Generate an embed token to view
                        var generateTokenRequestParameters = new GenerateTokenRequest(TokenAccessLevel.View);
                        var tokenResponse = client.Reports.GenerateTokenInGroup(Configurations.WorkspaceId, report.Id, generateTokenRequestParameters);
                        //var newUrl = "";
                        //// Populate embed variables (to be passed client-side)
                        //if (Convert.ToString(Session["RoleId"]) == "SE")
                        //    newUrl = report.EmbedUrl + "&$filter=LoginInfo/EngineerId eq '" + Convert.ToString(Session["UserId"]) + "'";
                        //else if (Convert.ToString(Session["RoleId"]) == "BM")
                        //    newUrl = report.EmbedUrl + "&$filter=LoginInfo/BranchCode eq '" + Convert.ToString(Session["BranchCode"]) + "'";
                        //else if (Convert.ToString(Session["RoleId"]) == "TM")
                        //    newUrl = report.EmbedUrl + "&$filter=LoginInfo/BranchCode in (" + Convert.ToString(Session["TMBranches"]) + ")";
                        //else if (Convert.ToString(Session["RoleId"]) == "HO")
                        //    newUrl = report.EmbedUrl + "&$filter=tt_customer_master/cter eq '" + cter + "'";
                        //objCom.LogMessage(newUrl);
                        ////+ "&$filter=SFEEDReport/salesengineer_id eq '" + Convert.ToString(Session["RoleId"]) + "'";
                        //report.EmbedUrl = newUrl;
                        embedToken = tokenResponse.Token;
                        embedUrl = report.EmbedUrl;
                        reportId = report.Id;
                    }
                }
                catch (Exception ex)
                {
                    objCom.LogError(ex);
                }
            }

        }
    }