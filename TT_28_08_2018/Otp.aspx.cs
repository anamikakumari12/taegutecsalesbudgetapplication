﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OtpNet;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime.CredentialManagement;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using System.Management;
using System.Management.Instrumentation;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;


namespace TaegutecSalesBudget
{
    public partial class Otp : System.Web.UI.Page
    {
        #region GlobalDeclarations
        LoginAuthentication authObj = new LoginAuthentication();
        PasswordSecurityGenerate psg = new PasswordSecurityGenerate();
        string defaultdevice1 = null;
        string defaultdevice2 = null;
        string RequestedDevice = null;
        static string OTP = null;
        static string StrOtp = null;
        string ipAddress = "";
        System.Timers.Timer t = null;
        string Phonenumber = null;
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                var userClaims = User.Identity as System.Security.Claims.ClaimsIdentity;
            }
        }

        protected void BtnOtp_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                if (Session["Failurecount"] == null)
                {
                    Session["Failurecount"] = 5;
                }
                else
                {
                    Session["Failurecount"] = Convert.ToInt32(Session["Failurecount"]) - 1;
                    lblOtpFailure.Text = "you are left with only " + Convert.ToInt32(Session["Failurecount"]) + "Attempt(s)";
                }

                if (Convert.ToInt32(Session["Failurecount"]) > 0)
                {
                    authObj = Session["authObj"] as LoginAuthentication;
                    LoadSQLotp();
                    if (Session["otp"] != null)
                    {
                        if (Convert.ToString(txtotp.Text) == Convert.ToString(Session["otp"]) || Convert.ToString(txtotp.Text) == Convert.ToString(Session["SqqlOtp"]))
                        {
                            String UserGuid = System.Guid.NewGuid().ToString();
                            Session["UserGuid"] = UserGuid;
                            //Session["IP"] = ipAddress;
                            Session["UserName"] = authObj.UserName;
                            Session["LoginMailId"] = authObj.LoginMailID;
                            Session["RoleId"] = authObj.RoleId;
                            Session["UserId"] = authObj.EngineerId;
                            Session["BranchCode"] = authObj.BranchCode;
                            Session["BranchDesc"] = authObj.BranchDesc;
                            Session["Territory"] = authObj.Territory;
                            Session["EngineerId"] = authObj.EngineerId;
                            Session["Password"] = authObj.MailPassword;
                            if(authObj.LoginMailID.Contains("duracarb-india.com") && authObj.RoleId=="HO")
                            {
                                Session["cter"] = "DUR";
                            }
                            if (Session["RoleId"] != null)
                            {
                                getFocusReportFlag(Convert.ToString(Session["UserId"]));
                                if (Session["RoleId"].ToString() == "Admin") { Session["AdminauthObj"] = Session["authObj"]; Response.Redirect("AdminProfile.aspx?Profile"); }
                                //else if (Convert.ToString(Session["RoleId"]) == "HO" && Convert.ToString(Session["Quote_ref"]) == "1")
                                //{
                                //    Response.Redirect("PendingQuotes.aspx?Quote");
                                //   // Context.ApplicationInstance.CompleteRequest();
                                //}
                                else
                                {
                                    //if (Convert.ToString(Session["RoleId"]) == "TM")
                                    //{
                                    //    SetTMBranches(Convert.ToString(Session["UserId"]));
                                    //}
                                    if (authObj.Menu_ID != "")
                                    {
                                        SetCompanyCode(Convert.ToString(Session["UserId"]));
                                        string[] menuids = null;
                                        menuids = authObj.Menu_ID.Split(',');
                                        if (menuids.Contains(BMSResource.ReportDashboardURLID))
                                        {
                                            Response.Redirect("ReportDashboard.aspx?RD");
                                        }
                                        else
                                        {
                                            int ID = Convert.ToInt32(menuids[0]);
                                            ds = authObj.GetAllMenusBL(authObj);
                                            if (ds.Tables.Count > 1)
                                            {
                                                var MenuName = ds.Tables[1].AsEnumerable()
                                     .Where(dataRow => dataRow.Field<int>("ID") == ID)
                                   .Select(row => new
                                   {
                                       Menu = row.Field<string>("Menu"),
                                       //URL = row.Field<string>("URL")
                                   }).AsEnumerable();

                                                foreach (var row1 in MenuName)
                                                {
                                                    var URL = ds.Tables[1].AsEnumerable()
                                                       .Where(dataRow => dataRow.Field<string>("Menu") == row1.Menu && dataRow.Field<string>("URL") != null)
                                                     .Select(row => new
                                                     {
                                                         URL = row.Field<string>("URL")
                                                     }).FirstOrDefault();
                                                    Response.Redirect(Convert.ToString(URL.URL));
                                                }
                                                // Response.Redirect("ReportDashboard.aspx?RD");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Please contact admin for module configuration');", true);
                                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please contact admin for module configuration'); showModal()", true);
                                        Response.Redirect("Login.aspx"); return;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "OTPalert", "alert('Invalid Otp'); showModal()", true);
                        }
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "OTPalert", "alert('OTP is timed out. please regenerate new OTP by clicking on Resend OTP link')", true);
                        lblOtpFailure.Text = "Your maximum attempts are exceeded";
                    }
                }
                else
                {
                    lblOtpFailure.Text = "";
                    Session["Failurecount"] = 5;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "OTPalert", "alert('Your maximum attempt(s) exceeded, please regenerate new OTP by clicking on Resend OTP link.')", true);
                    txtotp.Text = string.Empty;
                    Session["otp"] = null;
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }

        private void LoadSQLotp()
        {
            LoginAuthentication obj_auth = new LoginAuthentication();
            try
            {
                Session["SqqlOtp"] = obj_auth.GetConfiguredValue("SQL OTP");
            }
            finally
            {
                obj_auth = new LoginAuthentication();
            }
        }

        public void SetCompanyCode(string UserId)
        {
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                DataTable dt = objCom.GetMasterDropodwn(UserId, BMSResource.Company,null,null,null,null);
                if(dt.Rows.Count>1)
                    Session["CompanyCode"] = true;
                else
                    Session["CompanyCode"] = false;
                dt = objCom.GetMasterDropodwn(UserId, BMSResource.MappedUsers, null, null, null,null);
                if (dt.Rows.Count > 0)
                    Session["MappedUsers"] = Convert.ToString(dt.Rows[0]["MappedUsers"]);
                else
                    Session["MappedUsers"] = "'" + UserId + "'";
            }
            catch(Exception ex)
            {
                objCom.LogError(ex);
            }

            finally
            {
                objCom = new CommonFunctions();
            }
        }

        protected void ResendOtp_Click(object sender, EventArgs e)
        {
            try
            {
                //txtotp.Text = string.Empty;
                lblOtpFailure.Text = string.Empty;
                authObj = Session["authObj"] as LoginAuthentication;
                Session["otp"] = null;
                if (authObj.ErrorNum == 0)
                {
                    StrOtp = GenerateOtp();
                    authObj.LoginMailID = psg.ChangeMailId(authObj);
                    SendOtpToMail(StrOtp);
                    if (!string.IsNullOrEmpty(Phonenumber) || !string.IsNullOrWhiteSpace(Phonenumber))
                    {
                        sendOtpToMobile(StrOtp);
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }
        #endregion

        #region methods
        public string GenerateOtp()
        {
            try
            {
                byte[] b = new byte[] { Convert.ToByte(20), Convert.ToByte('A'), Convert.ToByte('T') };
                var otp = new Totp(b, mode: OtpHashMode.Sha512, step: 5);
                OTP = otp.ComputeTotp(DateTime.UtcNow);
                Session["otp"] = OTP;
                return OTP;
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return null;
            }
        }

        public void SendOtpToMail(string StrOtp)
        {
            try
            {
                authObj = Session["authObj"] as LoginAuthentication;
                MailMessage email = new MailMessage();
                email.To.Add(new MailAddress(authObj.LoginMailID)); //Destination Recipient e-mail address.
                string applicationPath = VirtualPathUtility.GetDirectory(Request.Path);//HttpContext.Current.Request.Url.Authority;
                string applicationPath1 = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                applicationPath = applicationPath1;
                email.Subject = "TSBA Login OTP";//Subject for your request
                string link = "Please <a href=\" " + applicationPath + ">login</a>";
                email.Body = " <br/><br/>" + StrOtp + Convert.ToString(ConfigurationManager.AppSettings["OtpMessage"]) + "<br/><br/>";
                email.IsBodyHtml = true;
                SmtpClient smtpc = new SmtpClient();
                smtpc.Send(email);
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);

            }
        }

        public void sendOtpToMobile(string otp)
        {
            string Message = otp + Convert.ToString(ConfigurationManager.AppSettings["OtpMessage"]);
            AmazonSimpleNotificationServiceClient snsClient = new AmazonSimpleNotificationServiceClient(Amazon.RegionEndpoint.USEast1);
            Dictionary<string, MessageAttributeValue> messageAttributes = new Dictionary<string, MessageAttributeValue>();
            MessageAttributeValue v1 = new MessageAttributeValue();
            v1.DataType = "String";
            v1.StringValue = "senderidx";
            messageAttributes.Add("AWS.SNS.SMS.SenderID", v1);
            MessageAttributeValue v2 = new MessageAttributeValue();
            v2.DataType = "String";
            v2.StringValue = "0.50";
            messageAttributes.Add("AWS.SNS.SMS.MaxPrice", v2);
            MessageAttributeValue v3 = new MessageAttributeValue();
            v3.DataType = "String";
            // Options: Promotional, Transactional
            v3.StringValue = "Promotional";
            messageAttributes.Add("AWS.SNS.SMS.SMSType", v3);
            SendSMSMessageAsync(snsClient, Message, Phonenumber, messageAttributes).Wait();
        }
        #endregion

        static async Task SendSMSMessageAsync(AmazonSimpleNotificationServiceClient snsClient, string message, string phoneNumber,

        Dictionary<string, MessageAttributeValue> messageAttributes)
        {
            PublishRequest publishRequest = new PublishRequest();
            publishRequest.PhoneNumber = phoneNumber;
            publishRequest.Message = message;
            publishRequest.MessageAttributes = messageAttributes;
            publishRequest.Subject = "test";
            try
            {
                var response = await snsClient.PublishAsync(publishRequest);
                Console.WriteLine(response.MessageId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        internal void getFocusReportFlag(string userId)
        {
            LoginAuthentication obj_auth = new LoginAuthentication();
            try
            {
                obj_auth.EngineerId = userId;
                Session["FocusFlag"] = obj_auth.setFocusFlag(obj_auth);
            }
            finally
            {
                obj_auth = new LoginAuthentication();
            }
        }
    }
}