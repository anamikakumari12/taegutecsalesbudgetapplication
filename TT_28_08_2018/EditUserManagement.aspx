﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="EditUserManagement.aspx.cs" ViewStateMode="Enabled" Inherits="TaegutecSalesBudget.EditUserManagement" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%-- <link href="GridviewScroll.css" rel="stylesheet" />
       <script type="text/javascript" src="gridscroll.js"></script>  --%>

    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <script type="text/javascript" src="js/app.js"></script>

    <script type="text/javascript">
      

        function bindGridView() {
            debugger;
            var head_content = $('#MainContent_GridEngInfo tr:first').html();
            $('#MainContent_GridEngInfo').prepend('<thead></thead>')
            $('#MainContent_GridEngInfo thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_GridEngInfo tbody tr:first').hide();
            $('#MainContent_GridEngInfo').DataTable(
                {
                    "info": false,
                    "paging": false,
                    "sScrollY": ($(window).height() - 200),
                });
        }
       
        var Checkedmappeduser = [];
        var uncheckedmappeduser = [];
        //$('.checkbox').on('change', function (evt, obj) {
        $(document).on("change", ".checkbox", function (e) {
            debugger;
            var checkid = this.children[0].id;
            if ($("#" + checkid).prop('checked') == true) {
            }
            else {
                var id = this.children[0].id.replace('chk_multi', 'hdn_chk_multi');
                uncheckedmappeduser.push($('#' + id).val())
            }


        });


        function EditValidation() {

            debugger;
            var name = $('#MainContent_txteditempname').val();
            //var contact = $('#Edituser #MainContent_txteditempcontact').val();
            var emial = $('#MainContent_txteditempmail').val();
            var usertype = $('#MainContent_txteditempusertype').val();
            var branch1 = $('#MainContent_editddlBranch').val();
            var branch = $('#Edituser #MainContent_Cblterritorybranch td input[type="checkbox"]:checked').serialize();

            if (name == "") {
                $('#Edituser #MainContent_lbltxteditempname').text('Enter the Employee name');
                return false;
            }
            else {
                $('#Edituser #MainContent_lbltxteditempname').remove();
            }
            if (branch1 == "--Select the Branch--") {
                $('#MainContent_lbleditddl').text('Enter the Branch');
                return false;
            }
            else {
                $('#MainContent_lbleditddl').remove();
            }
            emial = emial.split('@');
            if (!ValidateEmail($("#MainContent_txteditempmail").val())) {
                $('#Edituser #MainContent_lbltxteditempmail').text('Enter the valid email');
                return false;
            }
            else {
                $('#Edituser #MainContent_lbltxteditempmail').remove();
                if (emial[1] != 'taegutec-india.com' && $('#MainContent_rdBtnTaegutec').prop("checked")) {
                    $('#Edituser #MainContent_lbltxteditempmail').text('Enter the  valid email');
                    return false;
                }
                else if (emial[1] != 'duracarb-india.com' && $('#MainContent_rdBtnDuraCab').prop("checked")) {
                    $('#Edituser #MainContent_lbltxteditempmail').text('Enter the  valid email');
                    return false;
                }
                else {
                    $('#Edituser #MainContent_lbltxteditempmail').remove();
                }
            }
            //if (contact == "") {
            //    $('#Edituser #MainContent_lbltxteditempcontact').text('Enter the contact number');
            //    return false;
            //}
            //else {
            //    $('#Edituser #MainContent_lbltxteditempcontact').remove();

            //}

            if ($('#MainContent_Cblterritorybranch').is(':visible')) {
                if (branch == "") {
                    $('#Edituser #MainContent_EditUsertypevalidation').text('Please select atleast one branch');
                    return false
                }
                else {
                    $('#Edituser #MainContent_EditUsertypevalidation').remove();
                    // return true;
                }
            }


            if (usertype == "RO") {
                $('#Edituser #MainContent_EditUsertypevalidation').text('Select the User Type');
                return false;
            }
            else {
                $('#Edituser #MainContent_EditUsertypevalidation').remove();
                return true;
            }




        }

        function Cancels() {
            window.location.href = "UserManagement.aspx?User";
        }

        function EditAdd_Click() {
            if (!EditValidation()) {
                alert("Please check whether all the data what you have entered is proper or not");
            }
            else {
                var EngId = $("#MainContent_txteditempnumber").val();
                var EngName = $("#MainContent_txteditempname").val();
                var EngEmail = $("#MainContent_txteditempmail").val();
                var status = $("#MainContent_txteditempstatus").val();
                var contactNum = $("#MainContent_txteditempcontact").val();
                var Role = $("#MainContent_txteditempusertype").val();
                var branch = $("#MainContent_editddlBranch").val();
                var checkedusermapped = "";
                $("input[type=checkbox]:checked").each(function () {
                    var checkid = $(this).attr('id');
                    var id = checkid.replace('chk_multi', 'hdn_chk_multi');
                    Checkedmappeduser.push($('#' + id).val())
                });
                var uncheckedusermapped = "";
                if (Checkedmappeduser.length > 0) {
                    for (var i = 0; i < Checkedmappeduser.length; i++) {
                        checkedusermapped += Checkedmappeduser[i] + ",";
                    }
                }
                if (uncheckedmappeduser.length > 0) {
                    for (var i = 0; i < uncheckedmappeduser.length; i++) {
                        uncheckedusermapped += uncheckedmappeduser[i] + ",";
                    }
                }
                checkedusermapped = checkedusermapped.substring(0, checkedusermapped.length - 1);
                uncheckedusermapped = uncheckedusermapped.substring(0, uncheckedusermapped.length - 1);
                $.ajax({
                    url: 'EditUserManagement.aspx/EditAdd_Click',
                    method: 'post',
                    datatype: 'json',
                    data: JSON.stringify({ EngId: EngId, EngName: EngName, EngEmail: EngEmail, status: status, contactNum: contactNum, Role: Role, usermapped: checkedusermapped, nousermapped: uncheckedusermapped, branch: branch }),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d != "") {
                            data = JSON.parse(data.d);
                            msg = data.msg;
                            alert(msg);
                        }
                        else {
                            alert("User Updated successfully");
                            window.location.href = "EditUserManagement.aspx?id=" + EngId + "";
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            }
        }

        function selectAll(obj) {
            debugger;
            var id = obj.children[0].id.replace('chk_All', 'hdn_chk_All');
            if ($('#' + id).val() == "0")
                $('#' + id).val("1");
            else
                $('#' + id).val("0");
            var totaltrCount = $("[id*=GridEngInfo] tr").length;
            var trCount = totaltrCount;
            //sessionStorage.getItem("tot_Count");
            //$("[id*=grdPendingQuote] td").closest("tr").length;
            var multi_btn_flag = 0;
            var hdn_chk_id;
            var chk_id;
            if ($('#' + id).val() == "0") {
                for (i = 0; i < trCount; i++) {
                    hdn_chk_id = "MainContent_GridEngInfo_Hdnchk_" + i;
                    chk_id = "MainContent_GridEngInfo_chk_multi_" + i;

                    $("input[type='checkbox']").prop('checked', false);
                    //$("input[id=" + chk_id + "]").prop('checked', false);
                    $('#' + hdn_chk_id).val("0");

                }

            }
            else {
                for (i = 0; i < trCount; i++) {
                    hdn_chk_id = "MainContent_GridEngInfo_Hdnchk_" + i;
                    chk_id = "MainContent_GridEngInfo_chk_multi_" + i;

                    $("input[type='checkbox']").prop('checked', true);
                    //$("input[id=" + chk_id + "]").prop('checked', true);
                    $('#' + hdn_chk_id).val("1");

                }

            }
        }
        //$('.chk_multi_add').on('change', function (evt, obj) {
        $(document).on("change", ".chk_multi_add", function (e) {
            debugger;
            var id = this.children[0].id.replace('chk_multi', 'Hdnchk');
            if ($('#' + id).val() == "0")
                $('#' + id).val("1");
            else
                $('#' + id).val("0");
            var totaltrCount = $("[id*=GridEngInfo] tr").length;
            var trCount = totaltrCount - 3;
            //sessionStorage.getItem("tot_Count");
            //$("[id*=grdPendingQuote] td").closest("tr").length;
            var multi_btn_flag = 0;
            var all_flag = 0;
            var non_flag = 0;
            var chk_id;
            var chk_All;
            chk_All = "MainContent_GridEngInfo_chk_All";
            for (i = 0; i < trCount; i++) {
                chk_id = "MainContent_GridEngInfo_Hdnchk_" + i;
                if ($('#' + chk_id).val() == "1") {
                    multi_btn_flag++;
                    all_flag++;
                    //break;
                }
                else if ($('#' + chk_id).val() == "0") {
                    non_flag++;
                }
            }
            if (non_flag > 0) {
                $("input[id=" + chk_All + "]").prop('checked', false);
                //$('#body_grdAcceptedQuote_chk_All').prop("checked", false);
                $('#MainContent_GridEngInfo_hdn_chk_All').val("0");
            }
            else {
                $("input[id=" + chk_All + "]").prop('checked', true);
                //$('#body_grdAcceptedQuote_chk_All').prop("checked", true);
                $('#MainContent_GridEngInfo_hdn_chk_All').val("1");
            }


        })

        function checkDuplicateEditBranch(columnId, testid) {
            debugger;
            var table = $('#MainContent_GridEngInfo').DataTable();
            var split = testid.split('_');
            var sleng = split.length - 1;
            var spliid = parseInt(split[sleng]) + 1;
            //var value = $.trim($('#MainContent_GridEngInfo tr:eq(' + spliid + ') td:eq(' + columnId + ')').text());
            var data = table.row(spliid).data();
            var value = data[columnId].substring(data[columnId].indexOf("<span>") + 6, data[columnId].lastIndexOf("</span>"));
            return value;
        }

        function checkDuplicateEdit(columnId, testid) {

            var table = $('#MainContent_GridEngInfo').DataTable();
            var split = testid.split('_');
            var sleng = split.length - 1;
            var spliid = parseInt(split[sleng]) + 1;
            //var value = $.trim($('#MainContent_GridEngInfo tr:eq(' + spliid + ') td:eq(' + columnId + ')').text());
            var data = table.row(spliid).data();
            var value = data[columnId].substring(data[columnId].indexOf(">") + 1, data[columnId].lastIndexOf("<"));
            return value;
        }

        function checkDuplicateEditbranchcode(columnId, testid) {
            debugger;
            // clearTextvalue();
            var split = testid.split('_');
            var sleng = split.length - 1;
            var spliid = parseInt(split[sleng]) + 1;
            var value = $.trim($('#MainContent_GridEngInfo tr:eq(' + spliid + ') td:eq(' + columnId + ') input').attr('value'));
            return value;

        }

        function checkDuplicateEditstatus(columnId, testid) {

            var table = $('#MainContent_GridEngInfo').DataTable();
            var split = testid.split('_');
            var sleng = split.length - 1;
            var spliid = parseInt(split[sleng]) + 1;
            var value = $.trim($('#MainContent_GridEngInfo tr:eq(' + spliid + ') td:eq(' + columnId + ') input').attr('id'));
            var data = table.row(spliid).data();
            var value = data[columnId];
            if (value.indexOf("Active") > 0) {
                return true;
            }
            else {
                return false;
            }
        }

        function findminofscreenheight(a, b) {
            a = a - $("#divfilter").offset().top;
            return a < b ? a : b;
        }

        

        function isNumberKey(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false; }
            return true;
        }

        function checkDuplicate(textValue, columnId) {
            var rowCount = $('#MainContent_GridEngInfo tr').length;
            for (i = 1; i < rowCount; i++) {
                var columnOne = $.trim($('#MainContent_GridEngInfo tr:eq(' + i + ') td:eq(' + columnId + ')').text());


                if (columnOne == textValue) {
                    return false;
                }
            }
            return true;
        }

        function submit() {
            debugger;
            bindGridView();
            //$('#form1').submit();
        }

        function Disable() {
            return false;
        }

        


        function ValidateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            debugger;
            var output = re.test(email);
            console.log(output);
            // console.log(test(email));
            //  alert(email);
            debugger;
            //  return re.test(email);
            return output;

        }

    </script>

    <style>
        img#MainContent_notexits {
            margin-top: -24px;
            margin-right: 3px;
        }

        img#MainContent_mailexits {
            margin-top: -24px;
            margin-right: 3px;
        }

        img#MainContent_exits {
            margin-top: -24px;
            margin-right: 3px;
        }

        img#MainContent_mailnotexits {
            margin-top: -24px;
            margin-right: 3px;
        }

        #MainContent_GridEngInfo tr {
            vertical-align: top;
        }

        #MainContent_GridEngInfo td {
            padding-top: 10px;
        }

        .lblDisplay {
            display: none;
        }

        #MainContent_ddlBranchterritary input[type="checkbox"] {
            line-height: normal !important;
            margin: 4px 6px 0 !important;
        }

        #Edituser #MainContent_Cblterritorybranch input[type="checkbox"] {
            line-height: normal !important;
            margin: 4px 6px 0 !important;
        }
    </style>
    <style type="text/css">
        a {
            color: #428bca;
        }

        .hideelement {
            display: none;
        }

        .showelement {
            display: block;
        }

        td {
            height: 8px !important;
            padding-left: 5px !important;
            text-align: left !important;
        }


        th {
            /*background: #006780;
            color: #fff;
            font-weight: 600;
            font-size: 13px;
            border-color: #fff;
            height: 40px;
            padding-left: 10px;
            text-align: left;*/
            background: #ebeef5;
            color: #fff;
            font-weight: 600;
            text-align: left;
            border-color: #ebeef5;
            font-size: 12px;
        }

        .title_new {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-weight: bold;
            /* text-decoration: underline; */
            text-align: center;
            padding: 10px;
            background: #999;
            color: #fff;
            text-transform: uppercase;
        }

        .control-label2 {
            font-weight: normal;
            padding-top: 9px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--    <asp:ScriptManager ID="ScriptManager11" runat="server" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true" AsyncPostBackTimeout="360">
    </asp:ScriptManager>--%>
    <asp:ScriptManager ID="test" runat="server">
    </asp:ScriptManager>
    <%-- <asp:UpdatePanel ID="UpdatePanel11" runat="server">
        <ContentTemplate>--%>
    <div class="crumbs">
        <!-- Start : Breadcrumbs -->
        <ul id="breadcrumbs" class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a>Admin</a>
            </li>


            <li class="current">User Management</li>
            <div>

                <li class="title_bedcrum" id="tittle" runat="server" style="list-style: none;">EDIT USER DETAILS</li>

            </div>


        </ul>

    </div>
    <!-- End : Breadcrumbs -->
    <div class="col-lg-12" style="padding-top: 8px;">
        
    </div>
    <div class="clearfix"></div>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
           
            <div class="col-md-4 mn_margin" style="margin-top: 4%;">
                <asp:Panel runat="server" ID="pnlEdit">
                    <div id="Edituser" style="width: 100%;text-align: center !important; height: auto !important">
                        <div class="form-horizontal row-border1" id="Div2">
                            <div class="form-group">
                                <label class="col-md-5 control-label">
                                    Employee Number
                                       
                                </label>

                                <div class="col-md-7">
                                    <asp:TextBox ID="txteditempnumber" runat="server" CssClass="form-control required email"></asp:TextBox>

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-md-5 control-label">
                                    Employee Name 

                                </label>

                                <div class="col-md-7">
                                    <asp:TextBox runat="server" ID="txteditempname" CssClass="form-control required"></asp:TextBox>
                                    <asp:Label ID="lbltxteditempname" runat="server" ForeColor="Red" Style="float: right"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-5 control-label">
                                    Employee Email
                                          
                                </label>

                                <div class="col-md-7">
                                    <asp:TextBox runat="server" MaxLength="50" ID="txteditempmail" CssClass="form-control required email" placeholder="Username@taegutec-india.com"></asp:TextBox>

                                    <asp:Image ID="Editmailexists" ImageUrl="~/images/x.png" runat="server" Style="display: none" ImageAlign="Right" />
                                    <asp:Image ID="Editmailnotexits" ImageUrl="~/images/tick.png" runat="server" Style="display: none" ImageAlign="Right" />
                                    <asp:Label ID="lbltxteditempmail" runat="server" ForeColor="Red" Style="float: right"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-5 control-label">
                                    Status                                 
                                </label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="txteditempstatus" runat="server" class="form-control">
                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                        <asp:ListItem Value="0">Deactive</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-5 control-label">
                                    Contact Number   
                                </label>
                                <div class="col-md-7">
                                    <asp:TextBox runat="server" ID="txteditempcontact" MaxLength="12" CssClass="form-control required email" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    <asp:Label ID="lbltxteditempcontact" runat="server" ForeColor="Red" Style="float: right"></asp:Label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-5 control-label">
                                    User Type
                                </label>
                                <div class="col-md-7">
                                    <asp:DropDownList runat="server" ID="txteditempusertype" class="form-control">
                                        <asp:ListItem Value="RO">--Select the Role--</asp:ListItem>
                                        <asp:ListItem Value="SE">Sales Engineer</asp:ListItem>
                                        <asp:ListItem Value="Admin">Admin</asp:ListItem>
                                        <asp:ListItem Value="SU">Super User</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="EditUsertypevalidation" runat="server" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group" id="editbranch">
                                <label class="col-md-5 control-label" id="lbleditbranch">
                                    Branch
                                </label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="editddlBranch" runat="server" class="form-control">
                                    </asp:DropDownList>
                                </div>
                                 <asp:Label ID="lbleditddl" runat="server" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="form-group" id="EditBranchmultiple" style="display: none">
                                <label class="col-md-5 control-label" id="lblEditbrnachmultiple">
                                    Branch
                                </label>
                                <div class="col-md-7" style="height: 121px; border: 1px solid #ccc; overflow-y: scroll">
                                    <asp:CheckBoxList ID="Cblterritorybranch" runat="server" Style="font-size: 12px !important;"></asp:CheckBoxList>
                                </div>
                            </div>

                            <div class="form-actions" style="margin-bottom: -23px !important">
                              <%--  <asp:Button runat="server" ID="Cancel" Text="Cancel" CssClass="btn blue pull-right" OnClick="Cancels" />--%>

                                <%--<asp:Button runat="server" ID="EditAdd" Text="Update" CssClass="btn blue pull-right" OnClick="EditAdd_Click" OnClientClick="if(!EditValidation()){return false;}" />--%>
                                <input type="button" id="Editcancel" class="btn blue pull-right" value="Cancel" onclick="Cancels();" /> 
                                <input type="button" id="EditAdd" class="btn blue pull-right" value="Update" onclick="EditAdd_Click();" />
                                
                            </div>

                        </div>
                    </div>
                </asp:Panel>
            </div>

             <div class="col-md-8 mn_margin">
                <asp:Panel runat="server" ID="pnlData">
                    <%-- GRID VIEW STARTS--%>
                    <div>
                        <asp:GridView ID="GridEngInfo" runat="server" Width="100%" Style="text-align: center;" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="GridEngInfo_RowDataBound">
                            <Columns>
                                 <asp:TemplateField>
                                <HeaderTemplate>
                                    
                                    <asp:CheckBox ID="chk_All" runat="server" CssClass="chk_multi_All" onchange="selectAll(this);" />
                                    <asp:HiddenField ID="hdn_chk_All" runat="server" Value="0" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_multi" runat="server" CssClass="chk_multi_add" Checked='<%# GetStatus(Eval("EngineerId").ToString()) %>' />
                                    <asp:HiddenField ID="hdn_chk_multi" runat="server" Value='<%# Eval("EngineerId") %>' />
                                     <asp:HiddenField ID="Hdnchk" runat="server" Value="0" />
                                </ItemTemplate>
                            </asp:TemplateField>
                                <%--<asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chk_multi" runat="server" CssClass="chk_multi_add"  Checked='<%# GetStatus(Eval("EngineerId").ToString()) %>' />
                                        <asp:HiddenField ID="hdn_chk_multi" runat="server" Value='<%# Eval("EngineerId") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="ENGINEER ID">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_engid" runat="server" Text='<%# Eval("EngineerId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ENGINEER NAME">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_name" runat="server" Text='<%#Bind("EngineerName") %>'></asp:Label>
                                        <%--<asp:Label ID="lbl_name" runat="server" Text='<%# Eval("EngineerName") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="BRANCH">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="regioncode" runat="server" Value='<%# Eval("region_code") %>' />
                                        <asp:Label ID="lbl_branch" runat="server" Text='<%# Eval("region_description") %>' CssClass="lblDisplay" Visible="false"></asp:Label>
                                        <asp:PlaceHolder runat="server" ID="ph_Region"></asp:PlaceHolder>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>
                        </asp:GridView>
                        
                    </div>
                </asp:Panel>
            </div>
          

        </ContentTemplate>
        <%-- <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Okbtn" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnNotifctn" EventName="Click" />

        </Triggers>--%>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <%-- GRID VIEW END--%>
</asp:Content>
