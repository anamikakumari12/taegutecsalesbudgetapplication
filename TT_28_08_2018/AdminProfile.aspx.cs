﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class AdminProfile : System.Web.UI.Page
    {
        #region GlobalDeclareation
        public string Empnumber, Empname, Empmail, Empusertype, EmpPassword, EmpBranch, EditEmpstatus, editempcontact;
        public int Empstatus;
        public long Empcontact;
        EngineerInfo objEngInfo = new EngineerInfo();
        AdminConfiguration objAdmin = new AdminConfiguration();
        LoginAuthentication objauth = new LoginAuthentication();
        Reports objReports = new Reports();
        List<string> cssList = new List<string>();
        Review objRSum = new Review();
        #endregion

        #region Events
        public static string cter;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {
                if (Session["cter"] == null)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                    rdBtnTaegutec.Checked = true;
                    rdBtnDuraCab.Checked = false;

                }
                else if (Session["cter"].ToString() == "DUR")
                {
                    cter = "DUR";
                    rdBtnDuraCab.Checked = true;
                    rdBtnTaegutec.Checked = false;
                }
                else
                {
                    cter = "TTA";
                }
                LoadBranches();
                LoadBranchesforBranchmanager();
                LoadBranchesforTerritary();
                LoadEngineerInfo();
                bindgridColor();
            }
            if (ViewState["GridData"] != null)
            {
                DataTable dtData = ViewState["GridData"] as DataTable;
                if (dtData.Rows.Count != 0)
                {
                    GridEngInfo.DataSource = dtData;
                    GridEngInfo.DataBind();
                }
            }
            engineerIdScript();
            Ok_Click(null, null);
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);
        }
        protected void GridEngInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string data = "";
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string c = (e.Row.FindControl("lbl_branch") as Label).Text;
                string[] lines = Regex.Split(c, ",");
                foreach (var item in lines)
                {
                    PlaceHolder emails = e.Row.FindControl("ph_Region") as PlaceHolder;

                    data = item.ToString();
                    Label tags = new Label();
                    tags.EnableViewState = true;
                    tags.Text = data;
                    emails.Controls.Add(tags);
                    if (lines.Length != 1)
                    {
                        emails.Controls.Add(new LiteralControl("<br />"));
                    }
                }
            }
        }
        /// <summary>
        /// Mod By :Neha
        /// Mod Date:Dec 7th,2018
        /// Mod Des: deleted gridviewScrollTrigger function 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "gridviewScrollTrigger();$('#alertmsg').show().delay(5000).fadeOut();", true);

            if (rdBtnTaegutec.Checked)
            {
                cter = "TTA";
                rdBtnTaegutec.Checked = true;
                rdBtnDuraCab.Checked = false;
            }
            if (rdBtnDuraCab.Checked)
            {
                cter = "DUR";
                rdBtnDuraCab.Checked = true;
                rdBtnTaegutec.Checked = false;
            }
            LoadBranches();
            LoadBranchesforBranchmanager();
            GridEngInfo.DataSource = null;
            GridEngInfo.DataBind();
            Ok_Click(null, null);
            txtEmpcode.Text = "";
            txtEmpName.Text = "";
            txtEmpContact.Text = "";
            txtEmail.Text = "";

           // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopups", "submit();", true);
            engineerIdScript();


        }
        /// <summary>
        /// Mod By :Neha
        /// Mod Date:Dec 7th,2018
        /// Mod Des: deleted gridviewScrollTrigger function 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Click1(object sender, ImageClickEventArgs e)
        {
            int flag = 0;
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                ImageButton ibtn1 = sender as ImageButton;
                int rowIndex = Convert.ToInt32(ibtn1.Attributes["RowIndex"]);
                GridViewRow row = GridEngInfo.Rows[rowIndex];
                var id = row.FindControl("lbl_engid") as System.Web.UI.WebControls.Label;
                string EngId = id.Text;
                var role = row.FindControl("lbl_role") as System.Web.UI.WebControls.Label;
                string rolename = role.Text;
                if (rolename == "TERRITORY MANAGER")
                {
                    flag = 1;
                }
                objEngInfo.Deletebyadmin(EngId, flag);
                Ok_Click(null, null);

                //LoadEngineerInfo();
               // LoadBranches();
                LoadBranchesforBranchmanager();
                LoadBranchesforTerritary();
            }
           // ScriptManager.RegisterStartupScript(this, GetType(), "showalerts", "alert('Sucessfully deleted');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);
          
        }

        /// <summary>
        /// Mod By :Neha
        /// Mod Date:Dec 7th,2018
        /// Mod Des: deleted gridviewScrollTrigger function 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Deletbranch_Click(object sender, ImageClickEventArgs e)
        {
            int flag = 0;
            string ConfirmBranch = Request.Form["confirm_valueBranch"];
            if (ConfirmBranch == "Yes")
            {
                ImageButton ibtn1 = sender as ImageButton;
                int rowIndex = Convert.ToInt32(ibtn1.Attributes["RowIndex"]);
                GridViewRow row = GridEngInfo.Rows[rowIndex];
                var id = row.FindControl("lbl_engid") as System.Web.UI.WebControls.Label;
                string EngId = id.Text;
                var barnchcode = row.FindControl("lbl_branch") as System.Web.UI.WebControls.Label;
                string Branchcode = barnchcode.Text;
                var role = row.FindControl("lbl_role") as System.Web.UI.WebControls.Label;
                string rolename = role.Text;
                if (rolename == "TERRITORY MANAGER")
                {
                    flag = 1;
                }
                objEngInfo.DeletebyadminBranch(EngId, Branchcode, flag);
                Ok_Click(null, null);

                //LoadEngineerInfo();
               // LoadBranches();
                LoadBranchesforBranchmanager();
                LoadBranchesforTerritary();
            }
          
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            Empnumber = txtEmpcode.Text;
            Empname = txtEmpName.Text;
            Empmail = txtEmail.Text;
            string pwd = PasswordSecurityGenerate.Generate(8, 8);
            EmpPassword = objauth.Encrypt(pwd);
            Empcontact = string.IsNullOrEmpty(txtEmpContact.Text) ? 0 : Convert.ToInt64(txtEmpContact.Text);
            Empstatus = Convert.ToInt32(ddlstatus.SelectedValue);
            Empusertype = ddlUserType.SelectedValue;
            if (Empusertype == "BM")
            {
                EmpBranch = ddlBranch.SelectedValue;
                objEngInfo.Createnewuser(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype, EmpPassword);
            }
            else if (Empusertype == "TM")
            {
                EmpBranch = null;
                objEngInfo.Createnewuser(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype, EmpPassword);
                DataTable dtbranches = new DataTable();
                dtbranches.Columns.Add("regioncode", typeof(string));
                foreach (ListItem item in ddlBranchterritary.Items)
                {
                    if (item != null)
                    {
                        if (item.Selected)
                        {
                            dtbranches.Rows.Add(item.Value.ToString());

                        }
                    }
                }
                objEngInfo.InsertTag_for_territary(Empnumber, dtbranches);
                objEngInfo.Update_TE_for_Customer(Empnumber);
            }
            else
            {
                EmpBranch = null;
                objEngInfo.Createnewuser(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype, EmpPassword);
            }
            try
            {
                MailMessage email = new MailMessage();
                string pwd1 = pwd;
                email.To.Add(new MailAddress(Empmail)); //Destination Recipient e-mail address.
                email.Subject = " Welcome  To Sales-Budget & Performance Monitoring";//Subject for your request
                email.Body = " <br/><br/>Your Username: " + Empmail + "<br/><br/>Your Password: " + pwd + "<br/><br/>" + "From" + "<br/>" + "IT Team - TaeguTec";
                email.IsBodyHtml = true;
                SmtpClient smtpc = new SmtpClient();
                smtpc.Send(email);
            }
            catch (Exception Ex)
            {
                CommonFunctions.LogErrorStatic(Ex);
                ScriptManager.RegisterStartupScript(this, GetType(), "mailfailed", "alert('Failed to send email');", true);
            }
            Ok_Click(null, null);

          //  LoadBranches();
            LoadBranchesforBranchmanager();
            LoadBranchesforTerritary();
            string alert = "alert('User: " + Empname + " Created Successfully');";
            txtEmpcode.Text = "";
            txtEmpName.Text = "";
            txtEmpContact.Text = "";
            txtEmail.Text = "";
            ddlstatus.SelectedValue = "1";
            ddlUserType.SelectedValue = "RO";
            ScriptManager.RegisterStartupScript(this, GetType(), "showalerts", "alert('Sucessfully created');", true);
      
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "clearTextvalue();submit();", true);
           
        }
        /// <summary>
        /// Mod By :Neha
        /// Mod Date:Dec 7th,2018
        /// Mod Des: deleted gridviewScrollTrigger function and added check  for empty contact number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void EditAdd_Click(object sender, EventArgs e)
        //{
        //    int output = 0;
        //    string[] number, name, email, usertype, status, Branch, contact;
        //    Empnumber = txteditempnumber.Text;
        //    number = Empnumber.Split(',');
        //    Empnumber = number[0];
        //    Empname = txteditempname.Text;
        //    name = Empname.Split(',');
        //    Empname = name[0];
        //    Empmail = txteditempmail.Text;
        //    email = Empmail.Split(',');
        //    Empmail = email[0];
        //    editempcontact = Request.Form[txteditempcontact.UniqueID];
        //    contact = editempcontact.Split(',');
        //    if (editempcontact != ",")
        //    {
        //        Empcontact = string.IsNullOrEmpty(contact[0]) ? 0 : Convert.ToInt64(contact[0]); 
        //       // Empcontact = Convert.ToInt64(contact[0]);
        //    }

        //    Empusertype = Request.Form[txteditempusertype.UniqueID];
        //    usertype = Empusertype.Split(',');
        //    Empusertype = usertype[0];
        //    EditEmpstatus = Request.Form[txteditempstatus.UniqueID];
        //    status = EditEmpstatus.Split(',');
        //    Empstatus = Convert.ToInt32(status[0]);

        //    string[] splitemail;
        //    splitemail = Empmail.Split('@');
        //    if (Empname != "" && Empmail != "" && splitemail[0] != "")
        //    {
        //        if ((rdBtnTaegutec.Checked == true && splitemail[1] == "taegutec-india.com") || (rdBtnDuraCab.Checked == true && splitemail[1] == "duracarb-india.com"))
        //        {
        //            if (Empusertype == "BM")
        //            {
        //                EmpBranch = Request.Form[editddlBranch.UniqueID];
        //                Branch = EmpBranch.Split(',');
        //                EmpBranch = Branch[0];
        //                output = objEngInfo.EditUserInfo(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype);
        //            }
        //            else if (Empusertype == "TM")
        //            {
        //                EmpBranch = Request.Form[editddlBranch.UniqueID];
        //                Branch = EmpBranch.Split(',');
        //                EmpBranch = Branch[0];
        //                output = objEngInfo.EditUserInfo(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype);

        //                DataTable dtbranches = new DataTable();
        //                dtbranches.Columns.Add("regioncode", typeof(string));

        //                foreach (ListItem item in Cblterritorybranch.Items)
        //                {
        //                    if (item != null)
        //                    {
        //                        if (item.Selected)
        //                        {
        //                            dtbranches.Rows.Add(item.Value.ToString());

        //                        }
        //                    }
        //                }
        //                //Update TE Id in tt_territory_region_relation Table              
        //                objEngInfo.InsertTag_for_territary(Empnumber, dtbranches);

        //                //Update TE Id in tt_customer_master Table
        //                objEngInfo.Update_TE_for_Customer(Empnumber);
        //                // LoadBranchesforTerritary();
        //            }
        //            else
        //            {
        //                EmpBranch = null;
        //                output = objEngInfo.EditUserInfo(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype);
        //            }
        //        }
        //    }
        //    if (output == 0)
        //    {

        //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Not updated'); submit();", true);

        //    }
        //    else
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('sucessfully updated');", true);
        //    }

        //    //LoadEngineerInfo();
        //    Ok_Click(null, null);
        //    //LoadBranches();
        //    LoadBranchesforBranchmanager();
        //    LoadBranchesforTerritary();

        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup","submit();", true);

        //}

        /// <summary>
        /// Mod By :Monika M S
        /// Mod Date:Aug 25-08-2021
        /// Mod Des: updating menu details in login info table
        /// </summary>
        [WebMethod]
        public static string SaveMenuDetails(string menuids, string Empnumber, string Empname, string Empmail, string empcontactNumber, string editempusertype, string empstatus, string rdBtnTaegutec, string rdBtnDuraCab, string branch, string Cblterritorybranch)
        {

            int output = 0;
            string res = "";
            EngineerInfo objEngInfo = new EngineerInfo();
            string[] number, name, email, usertype, status, Branch, contact;
            number = Empnumber.Split(',');
            Empnumber = number[0];
            name = Empname.Split(',');
            Empname = name[0];
            email = Empmail.Split(',');
            Empmail = email[0];
            long Empcontact = 0;
            var editempcontact = empcontactNumber;
            contact = editempcontact.Split(',');
            if (editempcontact != ",")
            {
                Empcontact = string.IsNullOrEmpty(contact[0]) ? 0 : Convert.ToInt64(contact[0]);
                // Empcontact = Convert.ToInt64(contact[0]);
            }

            string Empusertype = editempusertype;
            usertype = Empusertype.Split(',');
            Empusertype = usertype[0];
            string EditEmpstatus = empstatus;
            status = EditEmpstatus.Split(',');
            int Empstatus = Convert.ToInt32(status[0]);
            string EmpBranch;
            string menu = "";

            //string role = HttpContext.Current.Session["RoleId"].ToString();
            var Json3 = JsonConvert.DeserializeObject<List<object>>(menuids);
            string[] menuid = Json3.Select(x => x.ToString()).ToArray();
            for (int i = 0; i < menuid.Length; i++)
            {
                menu += menuid[i] + ',';
            }

            if (menu.Length > 0)
            {
                menu = menu.Remove(menu.Length - 1);
            }

            string[] splitemail;
            splitemail = Empmail.Split('@');
            if (Empname != "" && Empmail != "" && splitemail[0] != "")
            {
                if ((splitemail[1] == "taegutec-india.com") || (splitemail[1] == "duracarb-india.com"))
                {
                    if (Empusertype == "BM")
                    {
                        EmpBranch = branch;
                        Branch = EmpBranch.Split(',');
                        EmpBranch = Branch[0];
                        output = objEngInfo.EditUserInfo(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype, menu);
                    }
                    else if (Empusertype == "TM")
                    {
                        EmpBranch = branch;
                        Branch = Cblterritorybranch.Split(',');
                        EmpBranch = Branch[0];
                        output = objEngInfo.EditUserInfo(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype, menu);

                        DataTable dtbranches = new DataTable();
                        dtbranches.Columns.Add("regioncode", typeof(string));

                        string[] terriarray = Cblterritorybranch.Split(',');
                        foreach (var item in terriarray)
                        {
                            if (item != null)
                            {
                                dtbranches.Rows.Add(item.ToString());

                            }
                        }
                        //Update TE Id in tt_territory_region_relation Table              
                        objEngInfo.InsertTag_for_territary(Empnumber, dtbranches);

                        //Update TE Id in tt_customer_master Table
                        objEngInfo.Update_TE_for_Customer(Empnumber);
                        // LoadBranchesforTerritary();
                    }
                    else
                    {
                        EmpBranch = null;
                        output = objEngInfo.EditUserInfo(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype, menu);
                    }
                }
            }
            if (output == 0)
            {

                res = "Not updated";

            }
            else
            {
                res = "sucessfully updated";
            }

            return res;
        }


        /// <summary>
        /// Modified by     :Neha
        /// Date            :11th feb,2019
        /// Description     :Binding gridview with null and calling submit 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNotifctn_Click(object sender, EventArgs e)
        {
            string confirm_emailNotification = Request.Form["confirm_emailNotification"];
            if (confirm_emailNotification == "No")
            {
                //Response.Redirect("AdminProfile.aspx?RD");
               // Page_Load(null, null);
             //   ClientScript.RegisterStartupScript(Page.GetType(), "closeWindow", "<script type='text/javascript'>RefreshParentPage()</script>");
               ScriptManager.RegisterStartupScript(this, this.GetType(), "scripts", "submit();", true);
              
            }
            else
            {
                DataTable dtActiveUsers = new DataTable();
                string password;
                dtActiveUsers = objAdmin.getActiveUsers();
                if (dtActiveUsers.Rows.Count > 0)
                {
                    for (int i = 0; i < dtActiveUsers.Rows.Count; i++)
                    {
                        string emailId = dtActiveUsers.Rows[0].ItemArray[0].ToString();
                        password = dtActiveUsers.Rows[0].ItemArray[1].ToString();
                        if (string.IsNullOrEmpty(password) && emailId != null)
                        {
                            //if the password is null,create a new password
                            string generatedPassword = PasswordSecurityGenerate.Generate(8, 8);
                            generatedPassword = objauth.Encrypt(generatedPassword);
                            string result = objauth.setPassword(emailId, generatedPassword);
                            if (result != "Success")
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "failedcreate", "alert('Failed to  create password for the new user');", true);
                            }
                            else
                            {
                                password = objauth.Decrypt(generatedPassword);
                            }
                        }
                        else
                        {
                            password = objauth.Decrypt(password);
                        }
                        try
                        {
                            MailMessage email = new MailMessage();
                            email.To.Add(new MailAddress(emailId)); //Destination Recipient e-mail address.
                            string applicationPath = VirtualPathUtility.GetDirectory(Request.Path);//HttpContext.Current.Request.Url.Authority;
                            string applicationPath1 = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                            applicationPath = applicationPath1;
                            email.Subject = " Welcome  To Sales-Budget & Performance Monitoring";//Subject for your request
                            string link = "Please <a href=\" " + applicationPath + ">login</a>";
                            email.Body = " <br/><br/>Your Username: " + emailId + "<br/><br/>Your Password: " + password + "<br/><br/>" + "From" + "<br/>" + "IT Team - TaeguTec" + "<br/>" + applicationPath;
                            email.IsBodyHtml = true;
                            SmtpClient smtpc = new SmtpClient();
                            smtpc.Send(email);
                        }
                        catch (Exception Ex)
                        {
                      
                            ScriptManager.RegisterStartupScript(this, GetType(), "failedmail", "submit(); alert('Failed to send email');", true);
                           
                        }
                    }
            
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "submit(); alert('Sucessfully sent email notification ');", true);
                   
                }
              
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scripts", "submit();", true);
            
            }
        }
        /// <summary>
        ///  Mod By :Neha
        /// Mod Date:Dec 7th,2018
        /// Mod Des: deleted gridviewScrollTrigger function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Ok_Click(object sender, EventArgs e)
        {
            string hoRoleId = null; string bmRoleId = null; string seRoleId = null; string adminRoleId = null; string TmRoledId = null;
            string region = ddlregiontype.SelectedItem.Value;
            DataTable dt = new DataTable();
            if (CheckBoxHo.Checked == true)
            {
                hoRoleId = "HO";
            }
            if (CheckBoxBm.Checked == true)
            {
                bmRoleId = "BM";
            }
            if (CheckBoxSe.Checked == true)
            {
                seRoleId = "SE";
            }
            if (CheckBoxAdmin.Checked == true)
            {
                adminRoleId = "Admin";
            }
            if (CheckBoxTM.Checked == true)
            {
                TmRoledId = "TM";
            }
            if (region == "ALL")
            {
                //anamika
                if (CheckBoxHo.Checked == false && CheckBoxBm.Checked == false && CheckBoxSe.Checked == false && CheckBoxAdmin.Checked == false && CheckBoxTM.Checked == false)
                {
                    hoRoleId = "HO";
                    bmRoleId = "BM";
                    seRoleId = "SE";
                    adminRoleId = "Admin";
                    TmRoledId = "TM";
                }
                //end
                region = null;
            }
            if (region != "ALL")
            {
                //anamika
                if (CheckBoxHo.Checked == false && CheckBoxBm.Checked == false && CheckBoxSe.Checked == false && CheckBoxAdmin.Checked == false && CheckBoxTM.Checked == false)
                {
                    hoRoleId = "HO";
                    bmRoleId = "BM";
                    seRoleId = "SE";
                    adminRoleId = "Admin";
                    TmRoledId = "TM";
                }
                //end
            }
            dt = objEngInfo.getEnginnerData(hoRoleId, bmRoleId, seRoleId, adminRoleId, TmRoledId, region, cter);
            ViewState["GridData"] = dt;
            GridEngInfo.DataSource = dt;
            GridEngInfo.DataBind();

            //divbminfo.Visible = false;
            if (CheckBoxSe.Checked == true && region != null)
            {
                //dt = objEngInfo.getEnginnerData("BM", region);
                //divbminfo.Visible = true;
                //foreach (DataRow row in dt.Rows)
                //{
                //    string name = row.Field<string>("EngineerName");
                //    LabelBmName.Text = "BRANCH MANGER :" + " " + name;
                //}

            }
            if (GridEngInfo.Rows.Count < 1)
            {
                GridEngInfo.Visible = false;
              
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();alert('No results found');", true);
              
            }
            else
            {
                GridEngInfo.Visible = true;
            }
         
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup","submit();", true);
         
            engineerIdScript();
        }
        /// <summary>
        /// Author     :Neha
        /// Date       :11th feb,2019
        /// Description:on click of cancel calling sub,it method through script manager
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancels(object sender, EventArgs e)
        {
            Ok_Click(null, null);
            //LoadBranches();
            LoadBranchesforBranchmanager();
            LoadBranchesforTerritary();

            //ClientScript.RegisterStartupScript(Page.GetType(), "closeWindow", "<script type='text/javascript'>RefreshParentPage()</script>");
            ScriptManager.RegisterStartupScript(this, GetType(), "ShowPopup", "submit();", true);
           
        }

        //protected void GeneratePopUp(object sender, EventArgs e)
        //{
        ////    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "S", "GeneratePopUp();", true);
        //}
        #endregion

        #region Methods
        protected void LoadEngineerInfo()
        {
            DataTable dt = new DataTable();
            dt = objEngInfo.getEnginnerData("HO", "BM", "SE", "Admin", "TM", null, cter);
            ViewState["GridData"] = null;
            ViewState["GridData"] = dt;
            GridEngInfo.DataSource = dt;
            GridEngInfo.DataBind();
        }

        protected void lbl_name_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                LoginAuthentication authObj = new LoginAuthentication();
                authObj.EngineerId = Convert.ToString((sender as LinkButton).CommandArgument);
                string ErrorMessage = authObj.AuthenticateUser(authObj);
                Session["Quote_ref"] = authObj.Quote_flag;
                Session["PhoneNumber"] = authObj.PhoneNumber;

                String UserGuid = System.Guid.NewGuid().ToString();
                Session["UserGuid"] = UserGuid;
                //Session["IP"] = ipAddress;
                Session["UserName"] = authObj.UserName;
                Session["LoginMailId"] = authObj.LoginMailID;
                Session["RoleId"] = authObj.RoleId;
                Session["UserId"] = authObj.EngineerId;
                Session["BranchCode"] = authObj.BranchCode;
                Session["BranchDesc"] = authObj.BranchDesc;
                Session["Territory"] = authObj.Territory;
                Session["EngineerId"] = authObj.EngineerId;
                Session["Password"] = authObj.MailPassword;
                Session["UserRole"] = "Admin";
                if (Session["RoleId"] != null)
                {
                    if (Session["RoleId"].ToString() == "Admin") { Response.Redirect("AdminProfile.aspx?Profile", false); }
                    else
                    {
                        if (authObj.LoginMailID.Contains("duracarb-india.com") && authObj.RoleId == "HO")
                        {
                            Session["cter"] = "DUR";
                        }
                        Otp obj = new Otp();
                        obj.getFocusReportFlag(Convert.ToString(Session["UserId"]));
                        obj.SetCompanyCode(Convert.ToString(Session["UserId"]));

                        if (authObj.Menu_ID != "")
                        {

                            string[] menuids = null;
                            menuids = authObj.Menu_ID.Split(',');
                            if (menuids.Contains(BMSResource.ReportDashboardURLID))
                            {
                                Response.Redirect("ReportDashboard.aspx?RD");
                            }
                            else {
                                int ID = Convert.ToInt32(menuids[0]);
                                ds = authObj.GetAllMenusBL(authObj);
                                if (ds.Tables.Count > 1)
                                {
                                    var MenuName = ds.Tables[1].AsEnumerable()
                         .Where(dataRow => dataRow.Field<int>("ID") == ID)
                       .Select(row => new
                       {
                           Menu = row.Field<string>("Menu"),
                       //URL = row.Field<string>("URL")
                   }).AsEnumerable();

                                    foreach (var row1 in MenuName)
                                    {
                                        var URL = ds.Tables[1].AsEnumerable()
                                           .Where(dataRow => dataRow.Field<string>("Menu") == row1.Menu && dataRow.Field<string>("URL") != null)
                                         .Select(row => new
                                         {
                                             URL = row.Field<string>("URL")
                                         }).FirstOrDefault();
                                        Response.Redirect(Convert.ToString(URL.URL));
                                    }
                                    //Response.Redirect("ReportDashboard.aspx?RD");
                                }
                            }
                        }
                        else
                        {
                            //ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Please contact admin for module configuration');", true);
                            //Response.Redirect("AdminProfile.aspx", false);

                            string scriptString = "<script type='text/javascript'> alert('Please contact admin for module configuration');</script>";
                            ClientScriptManager script = Page.ClientScript;
                            script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                        }
                        //if (Convert.ToString(Session["Quote_ref"]) == "1")
                        //{
                        //    if (Session["RoleId"].ToString() == "SE")
                        //    {
                        //        Response.Redirect("QuoteSummary.aspx?Quote", false);
                        //    }
                        //    else
                        //    {
                        //        if (Convert.ToString(Session["RoleId"]) == "TM")
                        //        {
                        //            //obj.SetTMBranches(Convert.ToString(Session["UserId"]));
                        //        }
                        //        Response.Redirect("PendingQuotes.aspx?Quote", false);
                        //    }
                        //    Context.ApplicationInstance.CompleteRequest();
                        //}
                        //else
                        //{
                        //    if (Convert.ToString(Session["RoleId"]) == "TM")
                        //    {
                        //        //obj.SetTMBranches(Convert.ToString(Session["UserId"]));
                        //    }
                        //    Response.Redirect("ReportDashboard.aspx?RD");
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                //ScriptManager.RegisterStartupScript(this, GetType(), "mailfailed", "alert('Failed to load');", true);
            }
           
        }

        /// <summary>
        ///  Mod By :Neha
        /// Mod Date:Dec 7th,2018
        /// Mod Des: deleted gridviewScrollTrigger function
        /// </summary>
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            DataTable dtData = new DataTable();
            objRSum.roleId = "HO";
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);

            ddlregiontype.DataSource = dtData;
            ddlregiontype.DataTextField = "BranchDesc";
            ddlregiontype.DataValueField = "BranchCode";
            ddlregiontype.DataBind();
            ddlregiontype.Items.Insert(0, "ALL");
        
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup","submit();", true);
           
        }
        protected void LoadBranchesforBranchmanager()
        {
            DataTable dtBranchesList = new DataTable();
            dtBranchesList = objEngInfo.LoadUserInfo(0, cter);

            ddlBranch.DataSource = dtBranchesList;
            ddlBranch.DataTextField = "region_description";
            ddlBranch.DataValueField = "region_code";
            ddlBranch.DataBind();

            editddlBranch.DataSource = dtBranchesList;
            editddlBranch.DataTextField = "region_description";
            editddlBranch.DataValueField = "region_code";
            editddlBranch.DataBind();
        }
        protected void LoadBranchesforTerritary()
        {
            DataTable dtBranchesList = new DataTable();
            dtBranchesList = objEngInfo.LoadUserInfo(1);

            ddlBranchterritary.DataSource = dtBranchesList;
            ddlBranchterritary.DataTextField = "region_description";
            ddlBranchterritary.DataValueField = "region_code";
            ddlBranchterritary.DataBind();

            Cblterritorybranch.DataSource = dtBranchesList;
            Cblterritorybranch.DataTextField = "region_description";
            Cblterritorybranch.DataValueField = "region_code";
            Cblterritorybranch.DataBind();
        }
        protected void bindgridColor()
        {
            if (GridEngInfo.Rows.Count != 0)
            {
                int color = 0;

                foreach (GridViewRow row in GridEngInfo.Rows)
                {

                    color++;
                    if (color == 1) { row.CssClass = "color_Product1 "; }
                    else if (color == 2)
                    {
                        row.CssClass = "color_Product2 ";
                        color = 0;
                    }

                }
            }
        }
        protected void LoadCSS()
        {
            cssList.Add("color_3");
            cssList.Add("color_4");
            cssList.Add("color_3");


        }
        protected string GetCSS(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssList.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssList.ElementAt(2); }
            else { return cssList.ElementAt(2); }

        }
        protected void engineerIdScript()
        {
            string[] engineerId = new string[] { };
            DataTable dtEngineer = new DataTable();
            dtEngineer = objEngInfo.getEnginnerData("HO", "BM", "SE", "Admin", "TM", null, null);

            engineerId = dtEngineer.AsEnumerable().Select(r => r.Field<string>("EngineerId")).ToArray();
            //this.ClientScript.RegisterArrayDeclaration("engineerID", engid);
            StringBuilder sb = new StringBuilder();
            sb.Append("<script>");
            sb.Append("var jEngineerId = new Array;");
            foreach (string str in engineerId)
            {
                sb.Append("jEngineerId.push('" + str + "');");
            }
            sb.Append("</script>");

            if (!Page.ClientScript.IsClientScriptBlockRegistered(this.GetType(), "engineerIdArrayScript"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "engineerIdArrayScript", sb.ToString(), false);
            }
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopups", "gridviewScrollTrigger(),submit();", true);

        }

        [WebMethod]
        public static List<Menu> GetMenus(string empmail)
        {
            String daresult = null;
            DataSet ds = new DataSet();
            LoginAuthentication authObj = new LoginAuthentication();
            authObj.LoginMailID = empmail;
            int count = 0;
            int count1 = 0;
            ds = authObj.GetAllMenusBL(authObj);
            List<Menu> objmenulist = new List<Menu>();


            var distinctMenus = ds.Tables[0].AsEnumerable().Select(s => new { name = s.Field<string>("Menu") }).Distinct().ToList();
            for (int i = 0; i < distinctMenus.Count; i++)
            {
                string menuname1 = "";
                Menu obj = new Menu();
                count = 0;
                var distinctMenusid = ds.Tables[0].AsEnumerable().Where(x => x.Field<string>("Menu") == distinctMenus[i].name).Select(s => new { id = s.Field<int>("ID") }).ToList();
                if (ds.Tables.Count > 1)
                {
                    var distinctMenusid1 = ds.Tables[1].AsEnumerable().Where(x => x.Field<string>("Menu") == distinctMenus[i].name).Select(s => new { id = s.Field<int>("ID") }).ToList();
                    if (count != distinctMenusid1.Count && distinctMenusid1.Count > 0)
                    {

                        if (distinctMenusid[0].id == distinctMenusid1[0].id)
                        {
                            count++;
                            if (distinctMenus[i].name.Contains(' '))
                            {

                                menuname1 = distinctMenus[i].name.Replace(" ", "");
                            }
                            else
                            {
                                menuname1 = distinctMenus[i].name;
                            }
                            obj.menu_Id = menuname1 + "_" + distinctMenusid[0].id;
                            //chk.CssClass = "MenuClass";
                            obj.menuClass = "MenuClass_" + menuname1;
                            obj.menuName = distinctMenus[i].name;
                            obj.menuChecked = "Checked";
                            objmenulist.Add(obj);

                        }
                        else
                        {
                            if (distinctMenus[i].name.Contains(' '))
                            {

                                menuname1 = distinctMenus[i].name.Replace(" ", "");
                            }
                            else
                            {
                                menuname1 = distinctMenus[i].name;
                            }
                            obj.menu_Id = menuname1 + "_" + distinctMenusid[0].id;
                            //chk.CssClass = "MenuClass";
                            obj.menuClass = "MenuClass_" + menuname1;
                            obj.menuName = distinctMenus[i].name;
                            obj.menuChecked = "";
                            objmenulist.Add(obj);
                        }

                    }
                    else
                    {
                        if (distinctMenus[i].name.Contains(' '))
                        {

                            menuname1 = distinctMenus[i].name.Replace(" ", "");
                        }
                        else
                        {
                            menuname1 = distinctMenus[i].name;
                        }

                        obj.menu_Id = menuname1 + "_" + distinctMenusid[0].id;
                        //chk.CssClass = "MenuClass";
                        obj.menuClass = "MenuClass_" + menuname1;
                        obj.menuName = distinctMenus[i].name;
                        obj.menuChecked = "";
                        objmenulist.Add(obj);
                    }
                }
                else
                {
                    if (distinctMenus[i].name.Contains(' '))
                    {

                        menuname1 = distinctMenus[i].name.Replace(" ", "");
                    }
                    else
                    {
                        menuname1 = distinctMenus[i].name;
                    }
                    obj.menu_Id = menuname1 + "_" + distinctMenusid[0].id;
                    //chk.CssClass = "MenuClass";
                    obj.menuClass = "MenuClass_" + menuname1;
                    obj.menuName = distinctMenus[i].name;
                    obj.menuChecked = "";
                    objmenulist.Add(obj);

                }


                var distinctsubMenus = ds.Tables[0].AsEnumerable().Where(x => x.Field<string>("Menu") == distinctMenus[i].name && x.Field<string>("SuperSubMenu_Name") == null).Select(s => new { name = s.Field<string>("SubMenu_Name"), id = s.Field<int>("ID") }).ToList();
                if (ds.Tables.Count > 1)
                {
                    //var distinctsubMenus1 = ds.Tables[1].AsEnumerable().Where(x => x.Field<string>("Menu") == distinctMenus[i].name && x.Field<string>("SuperSubMenu_Name") == null).Select(s => new { name = s.Field<string>("SubMenu_Name"), id = s.Field<int>("ID") }).ToList();

                    List<subMenu> objsubmenulist = new List<subMenu>();
                    for (int j = 0; j < distinctsubMenus.Count; j++)
                    {
                        subMenu objsubmenu = new subMenu();
                        DataRow[] rows = ds.Tables[1].Select("ID='" + distinctsubMenus[j].id + "'");
                        if (rows.Length > 0)
                        {

                            if (distinctsubMenus[j].name != null)
                            {

                                int idval = distinctsubMenus[j].id;
                                if (distinctMenus[i].name.Contains(' '))
                                {
                                    menuname1 = distinctMenus[i].name.Replace(" ", "");
                                }
                                else
                                {
                                    menuname1 = distinctMenus[i].name;
                                }


                                objsubmenu.subMenu_Id = menuname1 + "_submenuclass_" + idval;
                                objsubmenu.submenuClass = menuname1 + "submenuclass";
                                objsubmenu.subMenuName = distinctsubMenus[j].name;
                                objsubmenu.submenuChecked = "Checked";
                                objsubmenulist.Add(objsubmenu);



                                var distinctsupersubMenus = ds.Tables[0].AsEnumerable().Where(x => x.Field<string>("SubMenu_Name") == distinctsubMenus[j].name && x.Field<string>("SuperSubMenu_Name") != null).Select(s => new { name = s.Field<string>("SuperSubMenu_Name"), id = s.Field<int>("ID") }).Distinct().ToList();
                                if (ds.Tables.Count > 1)
                                {
                                    //var distinctsupersubMenus1 = ds.Tables[1].AsEnumerable().Where(x => x.Field<string>("SubMenu_Name") == distinctsubMenus[j].name && x.Field<string>("SuperSubMenu_Name") != null).Select(s => new { name = s.Field<string>("SuperSubMenu_Name"), id = s.Field<int>("ID") }).Distinct().ToList();

                                    var menuname = "";
                                    List<supersubMenu> objsupersubmenulist = new List<supersubMenu>();
                                    for (int k = 0; k < distinctsupersubMenus.Count; k++)
                                    {
                                        supersubMenu objsupersubmenu = new supersubMenu();
                                        DataRow[] rows1 = ds.Tables[1].Select("ID='" + distinctsupersubMenus[k].id + "'");
                                        if (rows1.Length > 0)
                                        {

                                            if (distinctsupersubMenus[k].name != null)
                                            {

                                                int idval1 = distinctsupersubMenus[k].id;

                                                if (distinctMenus[i].name.Contains(' '))
                                                {
                                                    menuname = distinctMenus[i].name.Replace(" ", "");
                                                }
                                                else
                                                {
                                                    menuname = distinctMenus[i].name;
                                                }

                                                objsupersubmenu.supersubMenu_Id = menuname + "_supersubmenuclass_" + idval1;
                                                objsupersubmenu.supersubMenuName = distinctsupersubMenus[k].name;
                                                objsupersubmenu.supersubmenuClass = menuname + "supersubmenuclass" + idval;
                                                objsupersubmenu.supersubmenuChecked = "Checked";
                                                objsupersubmenulist.Add(objsupersubmenu);




                                            }

                                        }
                                        else
                                        {
                                            if (distinctsupersubMenus[k].name != null)
                                            {
                                                int idval1 = distinctsupersubMenus[k].id;

                                                if (distinctMenus[i].name.Contains(' '))
                                                {
                                                    menuname = distinctMenus[i].name.Replace(" ", "");
                                                }
                                                else
                                                {
                                                    menuname = distinctMenus[i].name;
                                                }

                                                objsupersubmenu.supersubMenu_Id = menuname + "_supersubmenuclass_" + idval1;
                                                objsupersubmenu.supersubmenuClass = menuname + "supersubmenuclass" + idval;
                                                objsupersubmenu.supersubMenuName = distinctsupersubMenus[k].name;
                                                objsupersubmenu.supersubmenuChecked = "";
                                                objsupersubmenulist.Add(objsupersubmenu);


                                            }
                                        }
                                    }
                                    objsubmenu.supersubmenulist = objsupersubmenulist;
                                }
                                else
                                {
                                    string menuname = "";
                                    List<supersubMenu> objsupersubmenulist = new List<supersubMenu>();
                                    for (int k = 0; k < distinctsupersubMenus.Count; k++)
                                    {
                                        if (distinctsupersubMenus[k].name != null)
                                        {
                                            int idval1 = distinctsupersubMenus[k].id;

                                            if (distinctMenus[i].name.Contains(' '))
                                            {
                                                menuname = distinctMenus[i].name.Replace(" ", "");
                                            }
                                            else
                                            {
                                                menuname = distinctMenus[i].name;
                                            }
                                            supersubMenu objsupersubmenu = new supersubMenu();

                                            objsupersubmenu.supersubMenu_Id = menuname + "_supersubmenuclass_" + idval1;
                                            objsupersubmenu.supersubmenuClass = menuname + "supersubmenuclass" + idval;
                                            objsupersubmenu.supersubMenuName = distinctsupersubMenus[k].name;
                                            objsupersubmenu.supersubmenuChecked = "";
                                            objsupersubmenulist.Add(objsupersubmenu);

                                        }
                                    }
                                    objsubmenu.supersubmenulist = objsupersubmenulist;
                                }
                            }
                        }
                        else
                        {
                            string menuname = "";
                            if (distinctsubMenus[j].name != null)
                            {
                                int idval = distinctsubMenus[j].id;
                                if (distinctMenus[i].name.Contains(' '))
                                {
                                    menuname = distinctMenus[i].name.Replace(" ", "");
                                }
                                else
                                {
                                    menuname = distinctMenus[i].name;
                                }
                                objsubmenu.subMenu_Id = menuname + "_submenuclass_" + idval;
                                objsubmenu.submenuClass = menuname + "submenuclass";
                                objsubmenu.subMenuName = distinctsubMenus[j].name;
                                objsubmenu.submenuChecked = "";
                                objsubmenulist.Add(objsubmenu);


                                var distinctsupersubMenus = ds.Tables[0].AsEnumerable().Where(x => x.Field<string>("SubMenu_Name") == distinctsubMenus[j].name && x.Field<string>("SuperSubMenu_Name") != null).Select(s => new { name = s.Field<string>("SuperSubMenu_Name"), id = s.Field<int>("ID") }).Distinct().ToList();
                                if (ds.Tables.Count > 1)
                                {
                                    //var distinctsupersubMenus1 = ds.Tables[1].AsEnumerable().Where(x => x.Field<string>("SubMenu_Name") == distinctsubMenus[j].name && x.Field<string>("SuperSubMenu_Name") != null).Select(s => new { name = s.Field<string>("SuperSubMenu_Name"), id = s.Field<int>("ID") }).Distinct().ToList();

                                    List<supersubMenu> objsupersubmenulist = new List<supersubMenu>();
                                    for (int k = 0; k < distinctsupersubMenus.Count; k++)
                                    {
                                        supersubMenu objsupersubmenu = new supersubMenu();
                                        DataRow[] rows1 = ds.Tables[1].Select("ID='" + distinctsupersubMenus[k].id + "'");
                                        if (rows1.Length > 0)
                                        {

                                            if (distinctsupersubMenus[k].name != null)
                                            {

                                                int idval1 = distinctsupersubMenus[k].id;

                                                if (distinctMenus[i].name.Contains(' '))
                                                {
                                                    menuname = distinctMenus[i].name.Replace(" ", "");
                                                }
                                                else
                                                {
                                                    menuname = distinctMenus[i].name;
                                                }

                                                objsupersubmenu.supersubMenu_Id = menuname + "_supersubmenuclass_" + idval1;
                                                objsupersubmenu.supersubmenuClass = menuname + "supersubmenuclass" + idval;
                                                objsupersubmenu.supersubMenuName = distinctsupersubMenus[k].name;
                                                objsupersubmenu.supersubmenuChecked = "Checked";
                                                objsupersubmenulist.Add(objsupersubmenu);



                                            }

                                        }
                                        else
                                        {
                                            if (distinctsupersubMenus[k].name != null)
                                            {
                                                int idval1 = distinctsupersubMenus[k].id;

                                                if (distinctMenus[i].name.Contains(' '))
                                                {
                                                    menuname = distinctMenus[i].name.Replace(" ", "");
                                                }
                                                else
                                                {
                                                    menuname = distinctMenus[i].name;
                                                }

                                                objsupersubmenu.supersubMenu_Id = menuname + "_supersubmenuclass_" + idval1;
                                                objsupersubmenu.supersubmenuClass = menuname + "supersubmenuclass" + idval;
                                                objsupersubmenu.supersubMenuName = distinctsupersubMenus[k].name;
                                                objsupersubmenu.supersubmenuChecked = "";
                                                objsupersubmenulist.Add(objsupersubmenu);

                                            }
                                        }
                                    }
                                    objsubmenu.supersubmenulist = objsupersubmenulist;
                                }
                                else
                                {
                                    List<supersubMenu> objsupersubmenulist = new List<supersubMenu>();
                                    for (int k = 0; k < distinctsupersubMenus.Count; k++)
                                    {
                                        if (distinctsupersubMenus[k].name != null)
                                        {
                                            int idval1 = distinctsupersubMenus[k].id;

                                            if (distinctMenus[i].name.Contains(' '))
                                            {
                                                menuname = distinctMenus[i].name.Replace(" ", "");
                                            }
                                            else
                                            {
                                                menuname = distinctMenus[i].name;
                                            }
                                            supersubMenu objsupersubmenu = new supersubMenu();

                                            objsupersubmenu.supersubMenu_Id = menuname + "_supersubmenuclass_" + idval1;
                                            objsupersubmenu.supersubmenuClass = menuname + "supersubmenuclass" + idval;
                                            objsupersubmenu.supersubMenuName = distinctsupersubMenus[k].name;
                                            objsupersubmenu.supersubmenuChecked = "";
                                            objsupersubmenulist.Add(objsupersubmenu);

                                        }
                                    }
                                    objsubmenu.supersubmenulist = objsupersubmenulist;
                                }
                            }
                        }

                    }
                    obj.submenulist = objsubmenulist;
                }
                else
                {
                    string menuname = "";
                    List<subMenu> objsubmenulist = new List<subMenu>();
                    for (int j = 0; j < distinctsubMenus.Count; j++)
                    {
                        subMenu objsubmenu = new subMenu();
                        if (distinctsubMenus[j].name != null)
                        {
                            int idval = distinctsubMenus[j].id;
                            if (distinctMenus[i].name.Contains(' '))
                            {

                                menuname = distinctMenus[i].name.Replace(" ", "");
                            }
                            else
                            {
                                menuname = distinctMenus[i].name;
                            }
                            objsubmenu.subMenu_Id = menuname + "_submenuclass_" + idval;
                            objsubmenu.submenuClass = menuname + "submenuclass";
                            objsubmenu.subMenuName = distinctsubMenus[j].name;
                            objsubmenu.submenuChecked = "";
                            objsubmenulist.Add(objsubmenu);



                            var distinctsupersubMenus = ds.Tables[0].AsEnumerable().Where(x => x.Field<string>("SubMenu_Name") == distinctsubMenus[j].name && x.Field<string>("SuperSubMenu_Name") != null).Select(s => new { name = s.Field<string>("SuperSubMenu_Name"), id = s.Field<int>("ID") }).ToList();
                            if (ds.Tables.Count > 1)
                            {
                                //var distinctsupersubMenus1 = ds.Tables[1].AsEnumerable().Where(x => x.Field<string>("SubMenu_Name") == distinctsubMenus[j].name && x.Field<string>("SuperSubMenu_Name") != null).Select(s => new { name = s.Field<string>("SuperSubMenu_Name"), id = s.Field<int>("ID") }).ToList();

                                List<supersubMenu> objsupersubmenulist = new List<supersubMenu>();
                                for (int k = 0; k < distinctsupersubMenus.Count; k++)
                                {
                                    supersubMenu objsupersubmenu = new supersubMenu();
                                    DataRow[] rows1 = ds.Tables[1].Select("ID='" + distinctsupersubMenus[k].id + "'");
                                    if (rows1.Length > 0)
                                    {
                                        if (distinctsupersubMenus[k].name != null)
                                        {

                                            int idval1 = distinctsupersubMenus[k].id;

                                            if (distinctMenus[i].name.Contains(' '))
                                            {
                                                menuname = distinctMenus[i].name.Replace(" ", "");
                                            }
                                            else
                                            {
                                                menuname = distinctMenus[i].name;
                                            }

                                            objsupersubmenu.supersubMenu_Id = menuname + "_supersubmenuclass_" + idval1;
                                            objsupersubmenu.supersubmenuClass = menuname + "supersubmenuclass" + idval;
                                            objsupersubmenu.supersubMenuName = distinctsupersubMenus[k].name;
                                            objsupersubmenu.supersubmenuChecked = "Checked";
                                            objsupersubmenulist.Add(objsupersubmenu);


                                        }

                                    }
                                    else
                                    {
                                        if (distinctsupersubMenus[k].name != null)
                                        {
                                            int idval1 = distinctsupersubMenus[k].id;

                                            if (distinctMenus[i].name.Contains(' '))
                                            {
                                                menuname = distinctMenus[i].name.Replace(" ", "");
                                            }
                                            else
                                            {
                                                menuname = distinctMenus[i].name;
                                            }
                                            var name = distinctsubMenus[j].name.Replace(" ", "");
                                            objsupersubmenu.supersubMenu_Id = name + "_supersubmenuclass_" + idval1;
                                            objsupersubmenu.supersubmenuClass = menuname + name + "supersubmenuclass" + idval;
                                            objsupersubmenu.supersubMenuName = distinctsupersubMenus[k].name;
                                            objsupersubmenu.supersubmenuChecked = "";
                                            objsupersubmenulist.Add(objsupersubmenu);


                                        }
                                    }
                                }
                                objsubmenu.supersubmenulist = objsupersubmenulist;
                            }
                            else
                            {
                                List<supersubMenu> objsupersubmenulist = new List<supersubMenu>();
                                for (int k = 0; k < distinctsupersubMenus.Count; k++)
                                {
                                    if (distinctsupersubMenus[k].name != null)
                                    {
                                        int idval1 = distinctsupersubMenus[k].id;

                                        if (distinctMenus[i].name.Contains(' '))
                                        {
                                            menuname = distinctMenus[i].name.Replace(" ", "");
                                        }
                                        else
                                        {
                                            menuname = distinctMenus[i].name;
                                        }
                                        supersubMenu objsupersubmenu = new supersubMenu();

                                        objsupersubmenu.supersubMenu_Id = menuname + "_supersubmenuclass_" + idval1;
                                        objsupersubmenu.supersubmenuClass = menuname + "supersubmenuclass" + idval;
                                        objsupersubmenu.supersubMenuName = distinctsupersubMenus[k].name;
                                        objsupersubmenu.supersubmenuChecked = "";
                                        objsupersubmenulist.Add(objsupersubmenu);

                                    }
                                }
                                objsubmenu.supersubmenulist = objsupersubmenulist;
                            }
                        }


                    }
                    obj.submenulist = objsubmenulist;
                }
            }
            return objmenulist;
        }

        public class Menu
        {
            public string menu_Id { get; set; }
            public string menuClass { get; set; }
            public string menuName { get; set; }
            public string menuChecked { get; set; }
            public List<subMenu> submenulist { get; set; }
        }

        public class subMenu
        {
            public string subMenu_Id { get; set; }
            public string submenuClass { get; set; }
            public string subMenuName { get; set; }
            public string submenuChecked
            {
                get; set;
            }
            public List<supersubMenu> supersubmenulist { get; set; }
        }

        public class supersubMenu
        {
            public string supersubMenu_Id { get; set; }
            public string supersubmenuClass { get; set; }
            public string supersubMenuName { get; set; }
            public string supersubmenuChecked
            {
                get; set;
            }
        }
        #endregion


    }
}