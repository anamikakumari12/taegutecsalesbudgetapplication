﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class EditUserManagement : System.Web.UI.Page
    {
        #region GlobalDeclareation
        public string Empnumber, Empname, Empmail, Empusertype, EmpPassword, EmpBranch, EditEmpstatus, editempcontact;
        public int Empstatus;
        public long Empcontact;
        EngineerInfo objEngInfo = new EngineerInfo();
        List<string> cssList = new List<string>();
        Review objRSum = new Review();
        #endregion

        #region Events
        public static string cter;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {
                if (Session["cter"] == null)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";


                }
                else if (Session["cter"].ToString() == "DUR")
                {
                    cter = "DUR";

                }
                else
                {
                    cter = "TTA";
                }
                LoadBranches();
                LoadBranchesforBranchmanager();
                LoadEngineerInfo();
                //bindgridColor();
                string id = Request.QueryString["id"];
                DataTable dt = HttpContext.Current.Session["dt"] as DataTable;
                DataRow dataRow = dt.AsEnumerable().FirstOrDefault(r => Convert.ToString(r["EngineerId"]) == id);
                if (dataRow != null)
                {
                    LoadEditPanel(dataRow);
                }

            }
            if (ViewState["GridData"] != null)
            {
                DataTable dtData = ViewState["GridData"] as DataTable;
                if (dtData.Rows.Count != 0)
                {
                    GridEngInfo.DataSource = dtData;
                    GridEngInfo.DataBind();
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);
        }

        private void LoadEditPanel(DataRow dtfirst)
        {
            try
            {
                if (dtfirst != null)
                {
                    txteditempnumber.Text = Convert.ToString(dtfirst["EngineerId"]);
                    txteditempname.Text = Convert.ToString(dtfirst["EngineerName"]);
                    txteditempmail.Text = Convert.ToString(dtfirst["EmailId"]);
                    txteditempcontact.Text = Convert.ToString(dtfirst["PhoneNumber"]);
                    txteditempusertype.SelectedValue = Convert.ToString(dtfirst["RoleId"]);
                    txteditempstatus.SelectedValue = Convert.ToString(dtfirst["LoginStatus"]);
                    editddlBranch.SelectedValue = Convert.ToString(dtfirst["region_code"]);


                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }

        [WebMethod]
        public static string EditAdd_Click(string EngId, string EngName, string EngEmail, string status, long contactNum, string Role, string usermapped, string nousermapped, string branch)
        {

            string res = "";
            EngineerInfo objEngInfo = new EngineerInfo();
            objEngInfo.CreateUserDet(EngId, EngName, EngEmail, status, contactNum, Role, usermapped, nousermapped, branch);

            return res;
        }
        protected bool GetStatus(string Id)
        {
            bool res = false;
            string id = Request.QueryString["id"];
            DataTable dt = HttpContext.Current.Session["dt"] as DataTable;
            DataRow dataRow = dt.AsEnumerable().FirstOrDefault(r => Convert.ToString(r["EngineerId"]) == id);
            if (dataRow != null)
            {
                string mappeduser = dataRow["mappedUserId"].ToString();
                string[] userid = mappeduser.Split(',').ToArray();
                for (int i = 0; i < userid.Length; i++)
                {
                    if (Id == userid[i])
                    {
                        res = true;
                        break;
                    }

                }
            }
            return res;
        }

        #endregion

        protected void GridEngInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string data = "";
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string c = (e.Row.FindControl("lbl_branch") as Label).Text;
                string[] lines = Regex.Split(c, ",");
                foreach (var item in lines)
                {
                    PlaceHolder emails = e.Row.FindControl("ph_Region") as PlaceHolder;

                    data = item.ToString();
                    Label tags = new Label();
                    tags.EnableViewState = true;
                    tags.Text = data;
                    emails.Controls.Add(tags);
                    if (lines.Length != 1)
                    {
                        emails.Controls.Add(new LiteralControl("<br />"));
                    }
                }
            }
        }

        #region Methods
        protected void LoadEngineerInfo()
        {
            DataTable dt = new DataTable();
            dt = objEngInfo.getUserDetails();
            string id = Request.QueryString["id"];
            DataRow dataRow = dt.AsEnumerable().FirstOrDefault(r => Convert.ToString(r["EngineerId"]) == id);
            if (dataRow != null)
            {
                string mappeduser = dataRow["mappedUserId"].ToString();
                string[] userid = mappeduser.Split(',').ToArray();
                for (int i = 0; i < userid.Length; i++)
                {
                    DataRow dataRow1 = dt.AsEnumerable().FirstOrDefault(r => Convert.ToString(r["EngineerId"]) == userid[i]);
                    if (dataRow1 != null)
                    {
                        dataRow1["IsChecked"] = 2;
                    }
                }
            }
            dt.DefaultView.Sort = "IsChecked desc";
            dt = dt.DefaultView.ToTable();
            Session.Add("dt", dt);
            ViewState["GridData"] = null;
            ViewState["GridData"] = dt;
            GridEngInfo.DataSource = dt;
            GridEngInfo.DataBind();
        }

       

        protected void LoadBranchesforBranchmanager()
        {
            DataTable dtBranchesList = new DataTable();
            dtBranchesList = objEngInfo.LoadUserInfo(0, cter);

            editddlBranch.DataSource = dtBranchesList;
            editddlBranch.DataTextField = "region_description";
            editddlBranch.DataValueField = "region_code";
            editddlBranch.DataBind();
            editddlBranch.Items.Insert(0, "--Select the Branch--");
        }
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            DataTable dtData = new DataTable();
            objRSum.roleId = "HO";
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);


            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);

        }

        #endregion
    }
}