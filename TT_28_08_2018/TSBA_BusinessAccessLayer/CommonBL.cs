﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSBA_BusinessObjects;
using TSBA_DataAccessLayer;

namespace TSBA_BusinessAccessLayer
{
    public class CommonBL
    {
        CommonDAL objDAL = new CommonDAL();
        public DataTable GetMasterDropodwnBL(CommonBO obj)
        {
            return objDAL.GetMasterDropodwnDAL(obj);
        }
    }
}
