﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSBA_DataAccessLayer;
using TSBA_BusinessObjects;

namespace TSBA_BusinessAccessLayer
{
    public class QuoteBL
    {
        QuoteDAL objDAL = new QuoteDAL();
        public DataTable GetItemDescBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetItemDescDAL(objQuoteBO);
        }

        public DataTable GetFrequentItemsBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetFrequentItemsDAL(objQuoteBO);
        }

        public DataTable GetCustomerListBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetCustomerListDAL(objQuoteBO);
        }

        public DataTable GetRecentItemsBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetRecentItemsDAL(objQuoteBO);
        }

        public DataTable GetItemDetailsBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetItemDetailsDAL(objQuoteBO);
        }

        public QuoteBO SaveQuotesBL(DataTable dt)
        {
            return objDAL.SaveQuotesDAL(dt);
        }

        public DataTable GetQuoteDetailsBL(QuoteBO objquoteBO)
        {
            return objDAL.GetQuoteDetailsDAL(objquoteBO);
        }
        public DataTable GetPAQuoteDetailsBL(QuoteBO objquoteBO)
        {
            return objDAL.GetQuoteDetailsForPADAL(objquoteBO);
        }
        public DataTable GetQuoteSummaryBL(QuoteBO objquoteBO)
        {
            return objDAL.GetQuoteSummaryDAL(objquoteBO);
        }

        public string SubmitStatusBL(int ID, string status, string ocNumber, int ocflag, string comment, string changedDate, string loggedby)
        {
            return objDAL.SubmitStatusDAL(ID, status, ocNumber, ocflag, comment, changedDate, loggedby);
        }
        public DataTable GetCompetitorsBL()
        {
            return objDAL.GetCompetitorsDAL();
        }

        public QuoteBO PlaceOrderBL(QuoteBO objBO)
        {
            return objDAL.PlaceOrderDAL(objBO);
        }

        //public QuoteBO RequestForReApprovalBL(QuoteBO objBO)
        //{
        //    return objDAL.RequestForReApprovalDAL(objBO);
        //}

        public DataTable GetPendingTasksBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetPendingTasksDAL(objQuoteBO);
        }


        public List<QuoteBO> updateQuoteStatusBL(DataTable dt)
        {
            return objDAL.updateQuoteStatusDAL(dt);
        }

        public DataTable GetQuoteStatusLogBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetQuoteStatusLogDAL(objQuoteBO);
        }

        public DataTable GetEscalatedQuotesBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetEscalatedQuotesDAL(objQuoteBO);
        }

        public DataTable getQuoteFormatBL(QuoteBO objQuoteBO)
        {
            return objDAL.getQuoteFormatDAL(objQuoteBO);
        }

        public DataTable getQuotePOFormatBL(QuoteBO objQuoteBO)
        {
            return objDAL.getQuotePOFormatDAL(objQuoteBO);
        }

        public DataTable GetAgreementPriceBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetAgreementPriceDAL(objQuoteBO);
        }

        public QuoteBO DraftQuotesBL(DataTable dt)
        {
            return objDAL.DraftQuotesDAL(dt);
        }

        public DataTable GetAPListBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetAPListDAL(objQuoteBO);
        }

        public DataTable GetIntEscalatedQuotesBL(QuoteBO objQuoteBO)
        {
            return objDAL.GetIntEscalatedQuotesDAL(objQuoteBO);
        }

        public DataTable GetOfferPriceSplItemBL(SplQuoteBO objQuoteBO)
        {
            return objDAL.GetOfferPriceSplItemDAL(objQuoteBO);
        }
        public DataTable GetSplQuoteDropdownsBL(SplQuoteBO objQuoteBO)
        {
            return objDAL.GetSplQuoteDropdownsDAL(objQuoteBO);
        }

        public DataTable GetCustomerDetailsBL(SplQuoteBO objQuoteBO)
        {
            return objDAL.GetCustomerDetailsDAL(objQuoteBO);
        }

        public DataTable GetCPCustomerDetailsBL(SplQuoteBO objQuoteBO)
        {
            return objDAL.GetCPCustomerDetailsDAL(objQuoteBO);
        }

        public SplQuoteBO SaveSplQuotesBL(DataTable dt)
        {
            throw new NotImplementedException();
        }
    }
}
