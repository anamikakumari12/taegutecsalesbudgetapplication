﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdateQuoteStatus.aspx.cs" Inherits="TaegutecSalesBudget.UpdateQuoteStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
        <link href="css/Tabs.css" rel="stylesheet" />
    <style>
        .modal {
            overflow: hidden;
        }

            .modal a.close-modal {
                top: 0;
                right: 0;
            }

        .btncontrol {
            padding-left: 5px;
            padding-top: 10px;
        }

        .btn.green {
            margin-top: 10px;
        }


        .gridbutton {
            /*padding: 5px!important;*/
            top: auto;
            overflow: initial;
            padding: 3px 20px;
            border-radius: 3px !important;
            background: #0273ab;
            color: #fff;
            border: none;
            margin: 1px;
        }

        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px !important;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

        .control {
            padding-top: 2px;
        }

        .popupControl {
            margin: 5px;
            float: right;
        }

        th,
        td {
            white-space: nowrap;
        }

        div.dataTables_wrapper {
            /*width: 800px;*/
            margin: 0 auto;
        }
         .loader_div
        {
            position: fixed;
            top: 0;
            bottom: 0%;
            left: 0;
            right: 0%;
            z-index: 99;
            opacity: 0.7;
            display: none;
            background: lightgrey url('../../../images/loader.gif') center center no-repeat;
        }
    </style>
    <script type="text/javascript">
        var table1;

        $(document).ready(function () {
            debugger;

            LoadTable1();
            
        });
       
        function LoadDates() {
            //var SelectedStart = sessionStorage.getItem("selectedStart");
            //var SelectedEnd = sessionStorage.getItem("selectedEnd");
            var SelectedStart = null;
            var SelectedEnd = null;
            /* var start = (SelectedStart == null ? moment().subtract(7, 'days') : SelectedStart);*/
            var start = (SelectedStart == null ? moment().subtract(30, 'days') : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            $('#MainContent_txtDateRange').daterangepicker({
                autoUpdateInput: true,
                locale: {
                    format: 'MM/DD/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['07/20/2019', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                var start = start.format('MM/DD/YYYY');
                var end = end.format('MM/DD/YYYY');
                sessionStorage.setItem('selectedStart', start);
                sessionStorage.setItem('selectedEnd', end);
            });


        }
        function LoadTable1() {

            LoadDates();
            if ($.fn.dataTable.isDataTable('#MainContent_grdItemSummary')) {

            }
            else {

               
                var divplace = document.getElementById("divplace");

                var head_content = $('#MainContent_grdItemSummary tr:first').html();
                $('#MainContent_grdItemSummary').prepend('<thead></thead>')
                $('#MainContent_grdItemSummary thead').html('<tr>' + head_content + '</tr>');
                $('#MainContent_grdItemSummary tbody tr:first').hide();
                var table1 = $('#MainContent_grdItemSummary').dataTable({
                    "order": [[2, 'desc']],
                });
            }

        }

        function triggerPostGridLodedActions() {
            LoadTable1();
            var $RowSelected1 = $("#MainContent_grdItemSummary");

            if ($RowSelected1.length = 0) {
                alert("No Records Found");
            }
        }


        function SubmitOnOrderRecieved() {
            debugger;
            jQuery(".loader_div").show();
            var ID = $('#hdnsubmit').val();
            var status = $('#hdnstatus').val();
            var ocNumber = $('#MainContent_txtocnumber').val();
            var comment = $('#MainContent_txtComment').val();
            var ocflag = "1";


            if (ocNumber == "" || ocNumber == null || ocNumber == undefined) {
                $('#MainContent_txtocnumber').css("border", "1px solid red");
                jQuery(".loader_div").hide();
            }
            else {
                $("#btnSubmitReason").prop("disabled", true);
                $.ajax({
                    url: 'UpdateQuoteStatus.aspx/SubmitStatus',
                    method: 'post',
                    datatype: 'json',
                    data: '{ID:"' + ID + '", status:"' + status + '", ocNumber:"' + ocNumber + '", ocflag:"' + ocflag + '", comment:"' + comment + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        debugger;
                        alert(msg.d);
                        window.location.reload();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            }
        }

        function SubmitOnOrderLostOrPending() {
            debugger;
            jQuery(".loader_div").show();
            var ID = $('#hdnsubmit1').val();
            var status = $('#hdnstatus1').val();
            var comment = $('#MainContent_txtcomment1').val();
            var ocflag = "0";
            var ocNumber = $('#MainContent_txtocnumber').val();

            if (comment == "" || comment == null || comment == undefined) {
                $('#MainContent_txtcomment1').css("border", "1px solid red");
                jQuery(".loader_div").hide();
            }
            else {
                $("#btnSubmitReason1").prop("disabled", true);
                $.ajax({
                    url: 'UpdateQuoteStatus.aspx/SubmitStatus',
                    method: 'post',
                    datatype: 'json',
                    data: '{ID:"' + ID + '", status:"' + status + '", ocNumber:"' + ocNumber + '", ocflag:"' + ocflag + '", comment:"' + comment + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        alert(msg.d);
                        window.location.reload();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            }
        }

        function ItemAction(obj, status) {
            debugger;
            $('#MainContent_txtcomment1').css("border", "1px solid gray");
            var qty;
            var searchid = obj.id.substring(27, obj.id.lastIndexOf('_'))
            var Quote_id = obj.id.replace(searchid, "hdnitemID");
            var ID = $("#" + Quote_id).val();
            $("#hdnsubmit").val(ID);
            $("#hdnsubmit1").val(ID);
            $("#hdnstatus").val(status);
            $("#hdnstatus1").val(status);

        }

        $(document).on('click', '.filter', function (e) {
            jQuery(".loader_div").show();

        });


    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Quote</a>
                        </li>
                        <li class="current">Update Quote Status</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true" AsyncPostBackTimeout ="360000"></asp:ScriptManager>
    <asp:UpdatePanel ID="panel1" runat="server">

        <ContentTemplate>

            <div class="col-md-12 mn_margin">
                <div id="collapsebtn" class="row">
                    <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />
                </div>
                <div class="row filter_panel" id="reportdrpdwns" runat="server">
                    <div runat="server" id="cterDiv" visible="false">
                        <ul class="btn-info rbtn_panel">
                            <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>

                                <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                                <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                                <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            </li>
                        </ul>
                    </div>


                    <div class="col-md-12 nopadding">

                        <div class="col-md-2 control">
                            <label class="label ">Branch</label>
                            <asp:DropDownList ID="ddlBranch" runat="server" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                                <asp:ListItem>ALL</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="col-md-2 control">
                            <label class="label ">Sales Engineer</label>
                            <asp:DropDownList ID="ddlSE" runat="server" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlSE_SelectedIndexChanged">
                                <asp:ListItem>ALL</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="col-md-2 control">
                            <label class="label">Customer Class </label>
                            <asp:DropDownList ID="ddlCustomerClass" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCustomerClass_SelectedIndexChanged"
                                CssClass="control_dropdown">
                                <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                <asp:ListItem Value="KA">KEY FOCUSED CUSTOMER</asp:ListItem>
                                <asp:ListItem Value="KF">‘A’ CLASS CUSTOMER</asp:ListItem>
                                <asp:ListItem Value="OT"> OTHERS</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="col-md-2 control">
                            <label class="label">Customer</label>
                            <asp:DropDownList ID="ddlCustomers" runat="server" AutoPostBack="True" CssClass="control_dropdown" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged">
                                <asp:ListItem>ALL</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="col-md-2 control">
                            <label class="label">Requested Date</label>
                            <asp:TextBox ID="txtDateRange" CssClass="control_dropdown" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-2 btncontrol">
                            <asp:Button runat="server" ID="btnFilter" CssClass="btn green Filter" Text="Filter" OnClick="btnFilter_Click" />
                        </div>
                    </div>

                     

                </div>

               <label id="lblmessage" style="color:red"></label>
                  <div class="tabbed skin-turquoise round" id="skinable" style="margin-bottom: 10px;">
                            <ul>
                                <%--<li id="QuoteList" runat="server" class="active" onclick="tabchange(this);"> Quote Wise</li>--%>
                                <li id="ItemList" runat="server" class="active">Item Wise</li>
                            </ul>
                        </div>
                
                 <div class="col-md-12 nopad" id="divItemSummary" runat="server" style="display: block">
                <asp:GridView ID="grdItemSummary" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                     <Columns>
                         <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <a href="#mdReason" rel="modal:open" style="overflow: hidden;">
                                        <asp:Button ID="btnItemEscalate1" Style="background: #036ca5; color: white" Visible='<%# Eval("StatusName").ToString() == "Added In PA" ? true : false %>' runat="server" CssClass="btnSubmit" Text="Order Recieved" OnClientClick="ItemAction(this,'Order Recieved');" Enabled='<%# Eval("StatusName").ToString() == "Rejected"? false : true %>' ToolTip='<%# Eval("StatusName").ToString() == "Rejected"? "Escalate Option will not be available for Rejected quote" : "" %>' /></a>
                                    <%--<a href="#mdRejectReason\" rel="modal:open">--%>
                                    <a href="#mdRejectReason" rel="modal:open">
                                        <asp:Button runat="server" ID="btnItemAccept" Style="background: #036ca5; color: white" Visible='<%# Eval("StatusName").ToString() == "Added In PA" ? true : false %>' CssClass="btnSubmit" OnClientClick="ItemAction(this,'Order Lost');" Text="Order Lost" />
                                        <asp:Button runat="server" ID="btnRequest" Style="background: #036ca5; color: white" Visible='<%# Eval("StatusName").ToString() == "Added In PA" ? true : false %>' CssClass="btnSubmit" OnClientClick="ItemAction(this,'Order Pending');" Text="Order Pending" />

                                        <%--<asp:Image runat="server" ImageUrl="images/info.jpg" ID="imgView" onclick="ItemStatusLog(this);" title="View Status Log" Style="height: 25px;" />--%>

                                        <br />
                                </ItemTemplate>
                            </asp:TemplateField>
                                <asp:TemplateField HeaderText="Customer/Channel Partner Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustCPName" runat="server" Text='<%#Bind("CP_Name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                         <asp:TemplateField HeaderText="RFQ Number">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnitemID" Value='<%# Bind("ID")%>' runat="server" />
                                        <asp:Label ID="lblref" CssClass="link" runat="server" Text='<%#Bind("Ref_number") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Code">
                                    <ItemTemplate>
                                        <asp:Label ID="lblitem" runat="server" Text='<%#Bind("Item_code") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Desc">
                                    <ItemTemplate>
                                        <asp:Label ID="lblitemdesc" runat="server" Text='<%#Bind("Item_Desc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expected Price">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEPrice" runat="server" Text='<%#Bind("Expected_price") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Order Valildity">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOrder" Style="float: right;" runat="server" Text='<%#Bind("Order_Validity") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="MOQ">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMOQ" runat="server" Text='<%#Bind("MOQ") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               
                                <asp:TemplateField HeaderText="Special Price">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSP" runat="server" Text='<%#Bind("New_OfferPrice") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" runat="server" Text='<%#Bind("StatusName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                            </Columns>
                </asp:GridView>
                            </div>

            </div>
            <div id="divdetail" style="display: none;">
                <table id="grdDetailedPriceSummary1" class="display responsive nowrap" cellpadding="0" cellspacing="0">
                    <thead style="background-color: #DC5807; color: White; font-weight: bold">
                        <tr style="border: solid 1px #000000">
                         <td>Action</td>
                            <td>Item_code</td>
                            <td>Item_Desc</td>
                            <td>WHS</td>
                           <%-- <td>Order Type</td>--%>
                            <td>Approved Qty</td>
                           <%-- <td>QTY Per Order</td>
                             <td>Requested Price</td>--%>
                            <td>Approved Price</td>
                            <td>Validity(in Days)</td>
                            <td>File Uploaded By CP</td>
                            <td>File Uploaded By BM</td>
                            <td>File Uploaded By HO</td>
                            <td>Status</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div id="divplace" style="display: none;">fdgdsgfdg</div>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
     <div id="loader_div" class="loader_div"></div>
    <asp:UpdateProgress ID="updateProgress" runat="server">
            <ProgressTemplate>
                <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                    <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

    <div id="mdReason" class="modal" style="border: solid 1px #008a8a;">
        <div class="col-md-12 controls" style="height: 161px">

            <div class="ExpPriceDiv">
                <div class="col-md-5">
                    <p class="popupControl">OC Number : </p>
                </div>
                <div class="col-md-7">
                    <asp:TextBox ID="txtocnumber" CssClass="ddl" Style="width: 80px; border: solid 1px gray;" runat="server"></asp:TextBox>

                </div>
                <div class="col-md-5" style="bottom: -18px">
                    <p class="popupControl">Comment : </p>
                </div>
                <div class="col-md-7" style="bottom: -12px">
                    <asp:TextBox ID="txtComment" Rows="4" Columns="40" TextMode="MultiLine" runat="server" Style="width: 248px"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-12">

                <div class="col-md-7" style="bottom: -29px;">
                    <input type="hidden" id="hdnsubmit" />
                    <input type="hidden" id="hdnstatus" />
                    <input type="button" id="btnSubmitReason" class="btnSubmit" style="width: 75px; background: #036ca5; color: white" name="" onclick="SubmitOnOrderRecieved();" title="Submit" value="Submit" />
                    <a href="#" class="close1" rel="modal:close">Close</a>
                </div>
            </div>
        </div>
    </div>


    <div id="mdRejectReason" class="modal" style="border: solid 1px #008a8a;">
        <div class="col-md-12 controls" style="left: -82px; height: 110px;">

            <div class="col-md-5">
                <p class="popupControl">Comment : </p>
            </div>
            <div class="col-md-7">
                <asp:TextBox ID="txtcomment1" Rows="4" Columns="40" TextMode="MultiLine" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-12">

            <div class="col-md-7">
                <input type="hidden" id="hdnsubmit1" />
                <input type="hidden" id="hdnstatus1" />
                <input type="button" id="btnSubmitReason1" class="btnSubmit" style="width: 75px; background: #036ca5; color: white" name="" onclick="SubmitOnOrderLostOrPending();" title="Submit" value="Submit" />
                <a href="#" class="close1" rel="modal:close">Close</a>
            </div>
        </div>
    </div>

    
</asp:Content>
