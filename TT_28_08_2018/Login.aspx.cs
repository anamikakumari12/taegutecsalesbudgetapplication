﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OtpNet;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime.CredentialManagement;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using System.Management;
using System.Management.Instrumentation;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using Twilio.Rest.Api.V2010.Account;

namespace TaegutecSalesBudget
{
    public partial class Login : System.Web.UI.Page
    {
        #region GlobalDeclarations
        LoginAuthentication authObj = new LoginAuthentication();
        PasswordSecurityGenerate psg = new PasswordSecurityGenerate();
        string defaultdevice1 = null;
        string defaultdevice2 = null;
        string RequestedDevice = null;
        static string OTP = null;
        static string StrOtp = null;
        string ipAddress = "";
        System.Timers.Timer t = null;
        string Phonenumber = null;
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //authObj.Encrypt("H_302");
                //authObj.Encrypt("11640");
                //authObj.Encrypt("11640_22");

                string pwd= authObj.Decrypt("kVALLhE4CYAt88LI9CfpeGcf0rOKu12FnfWdDafDHpA=");
                loginform.Visible = true;
                //if (Session["UserName"] != null)
                //{
                //    if (Session["RoleId"].ToString() == "Admin") { Response.Redirect("AdminProfile.aspx?Profile"); }
                //    else Response.Redirect("ReportDashboard.aspx?RD");
                //}

                if (Session["RoleId"] != null)
                {
                    new Otp().getFocusReportFlag(Convert.ToString(Session["UserId"]));
                    if (Session["RoleId"].ToString() == "Admin") { Session["AdminauthObj"] = Session["authObj"]; Response.Redirect("AdminProfile.aspx?Profile"); }
                    //else if (Convert.ToString(Session["RoleId"]) == "HO" && Convert.ToString(Session["Quote_ref"]) == "1")
                    //{
                    //    Response.Redirect("PendingQuotes.aspx?Quote");
                    //   // Context.ApplicationInstance.CompleteRequest();
                    //}
                    else
                    {
                        new Otp().SetCompanyCode(Convert.ToString(Session["UserId"]));
                        if (authObj.Menu_ID != null)
                        {
                            DataSet ds = new DataSet();
                            string[] menuids = null;
                            menuids = authObj.Menu_ID.Split(',');
                            if (menuids.Contains(BMSResource.ReportDashboardURLID))
                            {
                                Response.Redirect("ReportDashboard.aspx?RD");
                            }
                            else
                            {
                                int ID = Convert.ToInt32(menuids[0]);
                                ds = authObj.GetAllMenusBL(authObj);
                                if (ds.Tables.Count > 1)
                                {
                                    var MenuName = ds.Tables[1].AsEnumerable()
                         .Where(dataRow => dataRow.Field<int>("ID") == ID)
                       .Select(row => new
                       {
                           Menu = row.Field<string>("Menu"),
                           //URL = row.Field<string>("URL")
                       }).AsEnumerable();

                                    foreach (var row1 in MenuName)
                                    {
                                        var URL = ds.Tables[1].AsEnumerable()
                                           .Where(dataRow => dataRow.Field<string>("Menu") == row1.Menu && dataRow.Field<string>("URL") != null)
                                         .Select(row => new
                                         {
                                             URL = row.Field<string>("URL")
                                         }).FirstOrDefault();
                                        Response.Redirect(Convert.ToString(URL.URL));
                                    }
                                    // Response.Redirect("ReportDashboard.aspx?RD");
                                }
                            }
                        }
                        else
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please contact admin for module configuration'); showModal()", true);
                            Response.Redirect("Login.aspx"); return;
                        }
                        //Response.Redirect("ReportDashboard.aspx?RD");
                    }
                }
            }
        }

        /// <summary>
        /// Author:
        /// Desc:
        /// Modified By:K.lakshmiBindu
        /// Modified Desc: on click of login button  security check for devices ip and otp generation is performed
        /// Modified Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void loginBtn_Click(object sender, EventArgs e)
        {
            try
            {
                lblOtpFailure.Text = "";
                Session["Failurecount"] = 5;
                authObj.LoginMailID = DecryptStringAES(Convert.ToString(HDusername.Value));
                authObj.MailPassword = authObj.Encrypt(DecryptStringAES(Convert.ToString(HDPassword.Value)));
                //authObj.LoginMailID = user.Text.ToString();
                //authObj.MailPassword = authObj.Encrypt(password.Text.ToString());
                string ErrorMessage = authObj.authLogin(authObj);
                Session["Quote_ref"] = authObj.Quote_flag;
                Session["PhoneNumber"] = authObj.PhoneNumber;
                Phonenumber = Convert.ToString(Session["PhoneNumber"]);
                Session["authObj"] = authObj;
                #region AppendingPhonenumber
                if (!string.IsNullOrEmpty(Phonenumber) || !string.IsNullOrWhiteSpace(Phonenumber))
                {
                    if (Phonenumber.Length == 10)
                    {
                        Phonenumber = "+91" + Phonenumber;
                    }
                    else
                    {
                        if (Phonenumber[0] != '+')
                        {
                            Phonenumber = "+" + Phonenumber;
                        }
                        if (Phonenumber.Length == 11)
                        {
                            Phonenumber = "91" + Phonenumber;
                        }

                    }

                }
                #endregion
                if (authObj.ErrorNum == 0)
                {
                    #region DeviceAuth
                    //   bool IsApprovedDevice = SecurityCheck(authObj);

                    //   if (IsApprovedDevice)
                    //{
                    //
                    #endregion
                    StrOtp = GenerateOtp();
                    authObj.LoginMailID = psg.ChangeMailId(authObj);
                    SendOtpToMail(StrOtp);
                    //if (!string.IsNullOrEmpty(Phonenumber) || !string.IsNullOrWhiteSpace(Phonenumber))
                    //{
                    //    sendOtpToMobile(StrOtp);

                    //}
                    //#region StoringLoginObectintoSessions

                    //String UserGuid = System.Guid.NewGuid().ToString();
                    //Session["UserGuid"] = UserGuid;
                    //Session["authObj"] = authObj;
                    //Session["IP"] = ipAddress;
                    //Session["UserName"] = authObj.UserName;
                    //Session["LoginMailId"] = authObj.LoginMailID;
                    //Session["RoleId"] = authObj.RoleId;
                    //Session["UserId"] = authObj.EngineerId;
                    //Session["BranchCode"] = authObj.BranchCode;
                    //Session["BranchDesc"] = authObj.BranchDesc;
                    //Session["Territory"] = authObj.Territory;
                    //Session["EngineerId"] = authObj.EngineerId;
                    //Session["Password"] = authObj.MailPassword;
                    //#endregion
                    Response.Redirect("Otp.aspx?RD");
                    //if (Session["RoleId"] != null)
                    //{
                    //    new Otp().getFocusReportFlag(Convert.ToString(Session["UserId"]));
                    //    if (Session["RoleId"].ToString() == "Admin") { Session["AdminauthObj"] = Session["authObj"]; Response.Redirect("AdminProfile.aspx?Profile"); }
                    //    //else if (Convert.ToString(Session["RoleId"]) == "HO" && Convert.ToString(Session["Quote_ref"]) == "1")
                    //    //{
                    //    //    Response.Redirect("PendingQuotes.aspx?Quote");
                    //    //   // Context.ApplicationInstance.CompleteRequest();
                    //    //}
                    //    else
                    //    {
                    //        new Otp().SetCompanyCode(Convert.ToString(Session["UserId"]));
                    //        if (authObj.Menu_ID != null)
                    //        {
                    //            DataSet ds = new DataSet();
                    //            string[] menuids = null;
                    //            menuids = authObj.Menu_ID.Split(',');
                    //            if (menuids.Contains(BMSResource.ReportDashboardURLID))
                    //            {
                    //                Response.Redirect("ReportDashboard.aspx?RD");
                    //            }
                    //            else
                    //            {
                    //                int ID = Convert.ToInt32(menuids[0]);
                    //                ds = authObj.GetAllMenusBL(authObj);
                    //                if (ds.Tables.Count > 1)
                    //                {
                    //                    var MenuName = ds.Tables[1].AsEnumerable()
                    //         .Where(dataRow => dataRow.Field<int>("ID") == ID)
                    //       .Select(row => new
                    //       {
                    //           Menu = row.Field<string>("Menu"),
                    //       //URL = row.Field<string>("URL")
                    //   }).AsEnumerable();

                    //                    foreach (var row1 in MenuName)
                    //                    {
                    //                        var URL = ds.Tables[1].AsEnumerable()
                    //                           .Where(dataRow => dataRow.Field<string>("Menu") == row1.Menu && dataRow.Field<string>("URL") != null)
                    //                         .Select(row => new
                    //                         {
                    //                             URL = row.Field<string>("URL")
                    //                         }).FirstOrDefault();
                    //                        Response.Redirect(Convert.ToString(URL.URL));
                    //                    }
                    //                    // Response.Redirect("ReportDashboard.aspx?RD");
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please contact admin for module configuration'); showModal()", true);
                    //            Response.Redirect("Login.aspx");
                    //        }
                    //        //Response.Redirect("ReportDashboard.aspx?RD");
                    //    }
                    //}

                    //  txtotp.Text = StrOtp;
                    //#region settingtimerforotp
                    //t = new System.Timers.Timer();
                    //t.Interval = 1000000;
                    //t.Elapsed += new System.Timers.ElapsedEventHandler(btnotpValidation_Click);
                    //t.Enabled = true;
                    //#endregion
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "showModal()", true);
                    //#region DeviceAuthe

                    //#endregion


                }

                else if (authObj.ErrorNum == 1)
                {
                    string scriptString = "<script type='text/javascript'> alert('User name and password does not match');</script>";
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                }
                else if (authObj.ErrorNum == 2)
                {
                    string scriptString = "<script type='text/javascript'> alert('Your account has been blocked');</script>";
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);

            }
        }

        /// <summary>
        /// Author:K.lakshmiBindu
        /// Desc: For Authenticating OTP
        /// Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnOtp_Click(object sender, EventArgs e)
        {
            if (Session["Failurecount"] == null)
            {
                Session["Failurecount"] = 5;
            }
            else
            {
                Session["Failurecount"] = Convert.ToInt32(Session["Failurecount"]) - 1;
                lblOtpFailure.Text = "your are left with only " + Convert.ToInt32(Session["Failurecount"]) + "Attempt(s)";
            }
            if (Convert.ToInt32(Session["Failurecount"]) > 0)
            {
                authObj = Session["authObj"] as LoginAuthentication;
                if (Session["otp"] != null)
                {
                    if (txtotp.Text == StrOtp)
                    {
                        psg.LogUsers(authObj, hdnipadress.Value);

                        #region StoringLoginObectintoSessions
                        Session["authObj"] = authObj;
                        Session["IP"] = ipAddress;
                        Session["UserName"] = authObj.UserName;
                        Session["LoginMailId"] = authObj.LoginMailID;
                        Session["RoleId"] = authObj.RoleId;
                        Session["UserId"] = authObj.EngineerId;
                        Session["BranchCode"] = authObj.BranchCode;
                        Session["BranchDesc"] = authObj.BranchDesc;
                        Session["Territory"] = authObj.Territory;
                        Session["EngineerId"] = authObj.EngineerId;
                        Session["Password"] = authObj.MailPassword;
                        Session["PhoneNumber"] = authObj.PhoneNumber;
                        Phonenumber = Convert.ToString(Session["PhoneNumber"]);
                        #endregion


                        String UserGuid = System.Guid.NewGuid().ToString();
                        Session["UserGuid"] = UserGuid;

                        if (Session["RoleId"] != null)
                        {
                            if (Session["RoleId"].ToString() == "Admin") { Response.Redirect("AdminProfile.aspx?Profile"); }

                            else
                            {
                                Response.Redirect("Otp.aspx?RD");

                            }

                        }
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "OTPalert", "alert('Invalid Otp'); showModal()", true);
                    }
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "OTPalert", "alert('OTP is timed out. Please try again.')", true);
                }
            }
            else
            {
                lblOtpFailure.Text = "";
                Session["Failurecount"] = 5;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "OTPalert", "alert('your maximum attempt(s) exceeded please try again with the new OTP that you recieve on your Mobile ')", true);
            }
        }

        #endregion

        #region Methods


        /// <summary>

        /// Author:K.lakshmiBindu
        /// Desc: For Generating OTP
        ///  Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public string GenerateOtp()
        {
            try
            {
                //{
                //    Random r = new Random();
                //    OTP = r.Next(100000, 1000000).ToString();

                byte[] b = new byte[] { Convert.ToByte(20), Convert.ToByte('A'), Convert.ToByte('T') };
                var otp = new Totp(b, mode: OtpHashMode.Sha512, step: 5);
                OTP = otp.ComputeTotp(DateTime.UtcNow);
                Session["otp"] = OTP;
                return OTP;
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return null;
            }
        }

        /// <summary>

        /// Author:K.lakshmiBindu
        /// Desc: For Sending OTP to Mail
        ///  Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SendOtpToMail(string StrOtp)
        {
            try
            {
                authObj = Session["authObj"] as LoginAuthentication;
                MailMessage email = new MailMessage();
                email.To.Add(new MailAddress(authObj.LoginMailID)); //Destination Recipient e-mail address.
                string applicationPath = VirtualPathUtility.GetDirectory(Request.Path);//HttpContext.Current.Request.Url.Authority;
                string applicationPath1 = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                applicationPath = applicationPath1;
                email.Subject = "TSBA Login OTP";//Subject for your request
                string link = "Please <a href=\" " + applicationPath + ">login</a>";
                email.Body = " <br/><br/>" + StrOtp + Convert.ToString(ConfigurationManager.AppSettings["OtpMessage"]) + "<br/><br/>";
                email.IsBodyHtml = true;
                SmtpClient smtpc = new SmtpClient();
                smtpc.Send(email);
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);

            }
        }

        /// <summary>

        /// Author :K.lakshmiBindu
        ///  Desc: For Authenticating Device
        ///  Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public bool SecurityCheck(LoginAuthentication authObj)
        {
            int counter1 = 0, counter2 = 0, counter3 = 0;
            bool IsApprovedDevice = false;
            ipAddress = hdnipadress.Value;
            Session["IP"] = ipAddress;
            try
            {
                DataTable dtLoginDeviceDetails = new DataTable();
                dtLoginDeviceDetails = psg.GetLoginDeviceDeatials(authObj);
                if (dtLoginDeviceDetails != null && dtLoginDeviceDetails.Rows.Count > 0)
                {

                    defaultdevice1 = Convert.ToString(dtLoginDeviceDetails.Rows[0]["defaultdevice1"]);
                    Session["defaultdevice1"] = defaultdevice1;
                    defaultdevice2 = Convert.ToString(dtLoginDeviceDetails.Rows[0]["defaultdevice2"]);
                    RequestedDevice = Convert.ToString(dtLoginDeviceDetails.Rows[0]["RequestedDevice"]);


                    if ((string.IsNullOrWhiteSpace(defaultdevice1)))
                    {
                        counter1++;
                        if ((string.IsNullOrWhiteSpace(defaultdevice2)))
                        {
                            counter2++;
                            if (string.IsNullOrWhiteSpace(RequestedDevice))
                                counter3++;
                            else
                            {
                                if (ipAddress == RequestedDevice)
                                {
                                    IsApprovedDevice = CheckForApprovalOfRequestedDevices(dtLoginDeviceDetails);
                                    if (!IsApprovedDevice)
                                    {
                                        Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFuncti", "alert('Your device is not yet Approved')", true);
                                    }

                                }
                            }
                        }
                        else
                        {
                            if (ipAddress == defaultdevice2)
                            {
                                IsApprovedDevice = true;
                            }
                            else
                            {
                                if (!(string.IsNullOrWhiteSpace(RequestedDevice)) && RequestedDevice != null)
                                {
                                    if (ipAddress == RequestedDevice)
                                    {

                                        IsApprovedDevice = CheckForApprovalOfRequestedDevices(dtLoginDeviceDetails);
                                        if (!IsApprovedDevice)
                                        {
                                            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFuncti", "alert('Your device is not yet Approved')", true);
                                        }

                                    }
                                    else
                                    {
                                        Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction2", "NewRequest()", true);

                                    }
                                }
                                else
                                {

                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction3", "RequestForDroping()", true);
                                }
                            }
                        }
                    }

                    else
                    {
                        if (ipAddress == defaultdevice1)
                        {
                            IsApprovedDevice = true;
                        }
                        else
                        {
                            if ((string.IsNullOrWhiteSpace(defaultdevice2)))
                            {
                                counter2++;
                                if (string.IsNullOrWhiteSpace(RequestedDevice))
                                    counter3++;
                                else
                                {
                                    if (ipAddress == RequestedDevice)
                                    {

                                        IsApprovedDevice = CheckForApprovalOfRequestedDevices(dtLoginDeviceDetails);
                                        if (!IsApprovedDevice)
                                        {
                                            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFuncti", "alert('Your device is not yet Approved')", true);
                                        }

                                    }
                                }
                            }
                            else
                            {
                                if (ipAddress == defaultdevice2)
                                {
                                    IsApprovedDevice = true;
                                }
                                else
                                {
                                    if (!(string.IsNullOrWhiteSpace(RequestedDevice)) && RequestedDevice != null)
                                    {
                                        if (ipAddress == RequestedDevice)
                                        {
                                            IsApprovedDevice = CheckForApprovalOfRequestedDevices(dtLoginDeviceDetails);
                                            if (!IsApprovedDevice)
                                            {
                                                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFuncti", "alert('Your device is not yet Approved')", true);
                                            }

                                        }
                                        else
                                        {
                                            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction2", "NewRequest()", true);

                                        }
                                    }
                                    else
                                    {
                                        counter3++;

                                        Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "RequestForDroping()", true);
                                    }

                                }
                            }
                        }
                    }


                    if (counter1 > 0 && counter2 == 0)
                    {
                        if (defaultdevice2 != ipAddress && RequestedDevice != ipAddress)
                            IsApprovedDevice = psg.UpdateDefaultdevice1(authObj, ipAddress);
                    }
                    else if (counter1 == 0 && counter2 > 0)
                    {
                        if (defaultdevice1 != ipAddress && RequestedDevice != ipAddress)
                            IsApprovedDevice = psg.UpdateDefaultdevice2(authObj, ipAddress, defaultdevice1);
                    }
                    else if (counter1 > 0 && counter2 > 0 && counter3 > 0)
                    {
                        IsApprovedDevice = psg.UpdateDefaultdevice1(authObj, ipAddress);
                    }

                }

                else
                {
                    IsApprovedDevice = psg.InsertRecordForFirstTime(authObj, ipAddress);
                }
                return IsApprovedDevice;
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return IsApprovedDevice;
            }
        }

        /// <summary>

        /// Author :K.lakshmiBindu
        ///  Desc: For cancelling old requested device and adding new Requested device
        ///  Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 

        protected void Btndevice_Click(object sender, EventArgs e)
        {
            LoginAuthentication authobj = Session["authObj"] as LoginAuthentication;
            defaultdevice1 = Convert.ToString(Session["defaultdevice1"]);
            LoginAuthentication authObj = Session["authObj"] as LoginAuthentication;
            if (psg.RequestForNewDevice(authObj, Convert.ToString(Session["IP"]), defaultdevice1))
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "alert('your request has been submitted sucessfyully')", true);
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "alert('your request has not been submitted')", true);
            }
        }

        /// <summary>
        /// Author :K.lakshmiBindu
        ///  Desc: for expiring the OTP after certain time
        ///  Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnotpValidation_Click(object sender, EventArgs e)
        {
            t.Enabled = false;
            Session["otp"] = null;

        }

        /// <summary>
        /// Author:K.LakshmiBindu
        /// Desc: For sending Phonenumber, Message for sending OTP  to Mobile
        /// Date: Feb 6, 2019
        /// </summary>
        /// <param name="otp"></param>
        public void sendOtpToMobile(string otp)
        {

            string Message = otp + Convert.ToString(ConfigurationManager.AppSettings["OtpMessage"]);
            AmazonSimpleNotificationServiceClient snsClient = new AmazonSimpleNotificationServiceClient(Amazon.RegionEndpoint.USEast1);
            Dictionary<string, MessageAttributeValue> messageAttributes = new Dictionary<string, MessageAttributeValue>();
            MessageAttributeValue v1 = new MessageAttributeValue();
            v1.DataType = "String";
            v1.StringValue = "senderidx";
            messageAttributes.Add("AWS.SNS.SMS.SenderID", v1);
            MessageAttributeValue v2 = new MessageAttributeValue();
            v2.DataType = "String";
            v2.StringValue = "0.50";
            messageAttributes.Add("AWS.SNS.SMS.MaxPrice", v2);
            MessageAttributeValue v3 = new MessageAttributeValue();
            v3.DataType = "String";
            // Options: Promotional, Transactional
            v3.StringValue = "Promotional";
            messageAttributes.Add("AWS.SNS.SMS.SMSType", v3);
            SendSMSMessageAsync(snsClient, Message, Phonenumber, messageAttributes).Wait();
        }

        /// <summary>
        /// Author:K.LakshmiBindu
        /// Desc: For sending OTP to Mobile
        /// Date: Feb 6, 2019
        /// </summary>
        /// <param name="snsClient"></param>
        /// <param name="message"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="messageAttributes"></param>
        /// <returns></returns>
        static async Task SendSMSMessageAsync(AmazonSimpleNotificationServiceClient snsClient, string message, string phoneNumber,
          Dictionary<string, MessageAttributeValue> messageAttributes)
        {
            PublishRequest publishRequest = new PublishRequest();
            publishRequest.PhoneNumber = phoneNumber;
            publishRequest.Message = message;
            publishRequest.MessageAttributes = messageAttributes;
            publishRequest.Subject = "test";
            try
            {
                var response = await snsClient.PublishAsync(publishRequest);
                Console.WriteLine(response.MessageId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        /// <summary>
        /// Author: K.LakshmiBindu
        /// Desc: For Checking approval of requested device
        /// Date: Feb 2, 2019 
        /// </summary>
        /// <param name="DtLoginDetails"></param>
        /// <returns></returns>

        public bool CheckForApprovalOfRequestedDevices(DataTable DtLoginDetails)
        {
            int status = Convert.ToInt32(DtLoginDetails.Rows[0]["StausOfRequestedDevice"]);
            if (status == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        public string DecryptStringAES(string cipherText)
        {
            var keybytes = Encoding.UTF8.GetBytes("8080808080808080");
            var iv = Encoding.UTF8.GetBytes("8080808080808080");

            var encrypted = Convert.FromBase64String(cipherText);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
            return string.Format(decriptedFromJavascript);
        }
        private string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold  
            // the decrypted text.  
            string plaintext = null;

            // Create an RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings  
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                try
                {
                    // Create the streams used for decryption.  
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {

                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream  
                                // and place them in a string.  
                                plaintext = srDecrypt.ReadToEnd();

                            }

                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }

        //protected void btnWP_Click(object sender, EventArgs e)
        //{
        //    //string Error;
        //    //string password;
        //    //string number = "919765309837";
        //    ////string id = WhatsAppApi.Register.WhatsRegisterV2.GenerateIdentity(number, "");
        //    ////string tokan = WhatsAppApi.Register.WhatsRegisterV2.GetToken(number);
        //    //if(WhatsAppApi.Register.WhatsRegisterV2.RequestCode(number, out password, out Error, "sms",null))
        //    //{

        //    //}
        //    //else
        //    //{

        //    //}
        //    //you will recieve a sms, use it to get password  
        //    //password = WhatsAppApi.Register.WhatsRegisterV2.RegisterCode(number, "307617");

        //    string AccountSid = "ACe2c21d9216d131a84dbfb950b26c015e";
        //    string token = "315e276ee73e421b9792a5af7b3f13dd";
        //    Twilio.TwilioClient.Init(AccountSid, token);
        //    var message = MessageResource.Create(
        //    body: "This is test message.",
        //    from: new Twilio.Types.PhoneNumber("+919765309837"),
        //    to: new Twilio.Types.PhoneNumber("+919538251686")
        //);

        //    Console.WriteLine(message.Sid);
        //}
    }


}
