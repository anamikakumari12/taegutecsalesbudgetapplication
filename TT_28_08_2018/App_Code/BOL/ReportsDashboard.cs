﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TaegutecSalesBudget
{
    public class DashboardReports
    {
        public DataTable getTopAndBottomSalesReports(string flag, string BranchCode, string territory_engineer_id, string salesengineer_id, string customer_type, string sortby, string budget_val = null,string cter=null)
        {
            DataTable dt = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getReportsDashboardData", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 10).Value = BranchCode;
                command.Parameters.Add("@territory_engineer_id", SqlDbType.VarChar, 10).Value = territory_engineer_id;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 10).Value = salesengineer_id;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 10).Value = customer_type;
                command.Parameters.Add("@topfiveflag", SqlDbType.VarChar, 100).Value = flag;
                command.Parameters.Add("@sortby", SqlDbType.VarChar, 10).Value = sortby;
                command.Parameters.Add("@budget_val", SqlDbType.VarChar, 50).Value = budget_val;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dt;
        }
        public DataTable getValueSum(string BranchCode, string Branch_Manager_Id, string salesengineer_id = null, string CustomerNumber = null, string flag = null, string familyname = null, string custtype = null,string cter=null)
        {
            DataTable dstotals = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getConsolidatedSum", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = BranchCode;
                command.Parameters.Add("@Branch_Manager_Id", SqlDbType.VarChar, 50).Value = Branch_Manager_Id;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = salesengineer_id;
                command.Parameters.Add("@flag", SqlDbType.VarChar, 50).Value = flag;
                command.Parameters.Add("@item_family_name", SqlDbType.VarChar, 50).Value = familyname;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = custtype;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50).Value = CustomerNumber;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dstotals);

                return dstotals;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dstotals;
        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 05, 2017
        /// </summary>
        /// <param name="flag"></param>
        /// <param name="BranchCode"></param>
        /// <param name="territory_engineer_id"></param>
        /// <param name="salesengineer_id"></param>
        /// <param name="customer_type"></param>
        /// <param name="sortby"></param>
        /// <param name="budget_val"></param>
        /// <param name="cter"></param>
        /// <returns></returns>
        public DataSet getTopAndBottomSalesReports_ds(string flag, string BranchCode, string territory_engineer_id, string salesengineer_id, string customer_type, string sortby, string budget_val = null, string cter = null)
        {
            DataSet dt = new DataSet();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getReportsDashboardData", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, -1).Value = BranchCode;
                command.Parameters.Add("@territory_engineer_id", SqlDbType.VarChar, 100).Value = territory_engineer_id;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, -1).Value = salesengineer_id;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 10).Value = customer_type;
                command.Parameters.Add("@topfiveflag", SqlDbType.VarChar, 100).Value = flag;
                command.Parameters.Add("@sortby", SqlDbType.VarChar, 50).Value = sortby;
                command.Parameters.Add("@budget_val", SqlDbType.VarChar, 50).Value = budget_val;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 10).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                command.CommandTimeout = 100000;
                sqlDa.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dt;
        }
        

    }
}