﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using TaegutecSalesBudget.App_Code.BOL;
using System.Globalization;

/// <summary>
/// EventDAO class is the main class which interacts with the database. SQL Server express edition
/// has been used.
/// the event information is stored in a table named 'event' in the database.
///
/// Here is the table format:
/// event(event_id int, title varchar(100), description varchar(200),event_start datetime, event_end datetime)
/// event_id is the primary key
/// </summary>
public class EventDAO
{
    //change the connection string as per your database connection.
    private static string connectionString = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();

    //this method retrieves all events within range start-end
    public static List<CalendarEvent> getEvents(DateTime start, DateTime end, string branchcode = null, string seid = null, string territoryEngineerId = null, string cter = null)
    {
        //VisitorEvents
        string tstart1 = start.ToString("MM-dd-yyyy",
                                CultureInfo.InvariantCulture);
        string tend1 = end.AddDays(1).ToString("MM-dd-yyyy",
                            CultureInfo.InvariantCulture);

        List<CalendarEvent> events = new List<CalendarEvent>();
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("VisitorEvents", con);
        cmd.CommandType = CommandType.StoredProcedure;


        cmd.Parameters.Add("@branchcode", SqlDbType.VarChar).Value = branchcode;
        cmd.Parameters.Add("@seid", SqlDbType.VarChar).Value = seid;
        cmd.Parameters.Add("@territoryEnginner", SqlDbType.VarChar).Value = territoryEngineerId;
        cmd.Parameters.Add("@cter", SqlDbType.VarChar).Value = cter;
        cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = tstart1;
        cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = tend1;

        using (con)
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                CalendarEvent cevent = new CalendarEvent();
                cevent.id = (int)reader["id"];
                cevent.agenda = (string)reader["agenda"];
                cevent.customerNumber = ConvertFromDBVal<string>(reader["customer_number"]);
                cevent.customer_short_name = ConvertFromDBVal<string>(reader["customer_short_name"]);
                cevent.customer_class = ConvertFromDBVal<string>(reader["customer_class"]);

                DateTime Startdatevisit = Convert.ToDateTime((string)reader["visit_date_start"]);
                DateTime Enddatevisit = Convert.ToDateTime((string)reader["visit_date_end"]);

                string start1 = Startdatevisit.ToString("MM-dd-yyyy HH:mm:ss tt",
                                CultureInfo.InvariantCulture);
                string end1 = Enddatevisit.ToString("MM-dd-yyyy HH:mm:ss tt",
                                    CultureInfo.InvariantCulture);
                cevent.visit_date_start = start1; // (string)reader["visit_date_start"];
                cevent.visit_date_end = end1;// (string)
                cevent.remarks = (string)reader["remarks"];
                cevent.distributor_number = ConvertFromDBVal<string>(reader["distributor_number"]);
                cevent.project_id = ConvertFromDBVal<string>(reader["project_id"]);
                cevent.isSubmit = (int)reader["is_submit"];
                cevent.salesEngineer = ConvertFromDBVal<string>(reader["salesEngineer"]);
                cevent.visited_salesengineer_id = ConvertFromDBVal<string>(reader["visited_salesengineer_id"]);
                cevent.visited_salesengineer_name = ConvertFromDBVal<string>(reader["visited_salesengineer_name"]);
                events.Add(cevent);
            }
        }
        return events;
        //side note: if you want to show events only related to particular users,
        //if user id of that user is stored in session as Session["userid"]
        //the event table also contains an extra field named 'user_id' to mark the event for that particular user
        //then you can modify the SQL as:
        //SELECT event_id, description, title, event_start, event_end FROM event where user_id=@user_id AND event_start>=@start AND event_end<=@end
        //then add paramter as:cmd.Parameters.AddWithValue("@user_id", HttpContext.Current.Session["userid"]);
    }
    public static T ConvertFromDBVal<T>(object obj)
    {
        if (obj == null || obj == DBNull.Value)
        {
            return default(T); // returns the default value for the type
        }
        else
        {
            return (T)obj;
        }
    }


    //this method updates the event start and end time ... allDay parameter added for FullCalendar 2.x
    public static void updateEventTime(int id, DateTime start, DateTime end)
    {
        try
        {
            string start1 = start.ToString("MM-dd-yyyy HH:mm:ss tt",
                                CultureInfo.InvariantCulture);
            string end1 = end.ToString("MM-dd-yyyy HH:mm:ss tt",
                                CultureInfo.InvariantCulture);
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("UPDATE tt_visit_entry SET visit_date_start=@event_start, visit_date_end=@event_end WHERE id=@event_id", con);
            cmd.Parameters.Add("@event_start", SqlDbType.VarChar).Value = start1;
            cmd.Parameters.Add("@event_end", SqlDbType.VarChar).Value = end1;
            cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = id;

            using (con)
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
        catch (Exception Ex) { }
    }


    public static int addVisit(CalendarEvent cevent)
    {
        int Result;

        string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
        SqlConnection connection = new SqlConnection(connstring);
        connection.Open();
        SqlCommand command = new SqlCommand("insertVisitEntry", connection);
        command.CommandType = CommandType.StoredProcedure;



        command.CommandType = CommandType.StoredProcedure;
        command.Parameters.Add("@eventID", SqlDbType.Int).Value = cevent.id;
        command.Parameters.Add("@agenda", SqlDbType.VarChar, 5000).Value = cevent.agenda;
        command.Parameters.Add("@customer_number", SqlDbType.VarChar, 500).Value = cevent.customerNumber;
        command.Parameters.Add("@customer_short_name", SqlDbType.VarChar, 500).Value = cevent.customer_short_name;
        command.Parameters.Add("@customer_class", SqlDbType.VarChar, 500).Value = cevent.customer_class;
        command.Parameters.Add("@project_id", SqlDbType.VarChar, 500).Value = cevent.project_id;
        command.Parameters.Add("@visit_start_date", SqlDbType.VarChar, 500).Value = cevent.visit_date_start;
        command.Parameters.Add("@visit_end_date", SqlDbType.VarChar, 500).Value = cevent.visit_date_end;
        command.Parameters.Add("@remarks", SqlDbType.NVarChar, 5000).Value = cevent.remarks;
        command.Parameters.Add("@modified_date", SqlDbType.VarChar, 500).Value = cevent.modified_date;
        command.Parameters.Add("@isSubmit", SqlDbType.VarChar, 500).Value = cevent.isSubmit;
        command.Parameters.Add("@distributor_number", SqlDbType.VarChar, 500).Value = cevent.distributor_number;
        command.Parameters.Add("@created_date", SqlDbType.VarChar, 500).Value = cevent.created_date;
        command.Parameters.Add("@visited_salesengineer_id", SqlDbType.VarChar, 500).Value = cevent.visited_salesengineer_id;
        command.Parameters.Add("@visited_salesengineer_name", SqlDbType.VarChar, 500).Value = cevent.visited_salesengineer_name;

        command.Parameters.Add(new SqlParameter("@Error", SqlDbType.VarChar, 500));
        command.Parameters["@Error"].Direction = ParameterDirection.Output;

        try
        {
            command.ExecuteNonQuery();
            string sResult = command.Parameters["@Error"].Value.ToString();
            if (sResult == "Failed") { Result = -1; } else { Result = 1; }
            return Result;
        }

        catch (Exception ex)
        {
            Result = -1;
            return Result;
        }

        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }
}
