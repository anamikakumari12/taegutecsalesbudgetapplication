﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TaegutecSalesBudget
{
    class DashboardOverView
    {
        public DataTable getDashboardData(string Region, string SalesEngineer, string Role, string cter_type)
        {
            DataTable dtBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("DashboardData", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Region", SqlDbType.VarChar, 50).Value = Region;
                command.Parameters.Add("@salesEngId", SqlDbType.VarChar, 50).Value = SalesEngineer;
                command.Parameters.Add("@role", SqlDbType.VarChar, 50).Value = Role;
                command.Parameters.Add("@cter_type", SqlDbType.VarChar, 50).Value = cter_type;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBudget);
                return dtBudget;
            }

            catch (Exception ex)
            {
                //LogFile("ERROR ", ex.Message, ex.InnerException.Message,  "=============================================================================================================");
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBudget;
        }

        public DataTable getCustomerDetails_for_BM()
        {
            DataTable dtBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getCustomerDetails_for_BM", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBudget);
                return dtBudget;
            }

            catch (Exception ex)
            {
                //LogFile("ERROR ", ex.Message, ex.InnerException.Message,  "=============================================================================================================");
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBudget;
        }

        public DataTable GetFilterData(string MainFilterQuery)
        {
            DataTable dtDashboard = new DataTable();

            // Retrieve the connection string stored in the Web.config file.
            String connectionString = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ConnectionString;

            try
            {
                // Connect to the database and run the query.
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter adapter = new SqlDataAdapter(MainFilterQuery, connection);

                // Fill the DataSet.
                adapter.Fill(dtDashboard);

            }
            catch (Exception ex)
            {

                // The connection failed. Display an error message.
                //Message.Text = "Unable to connect to the database.";

            }
            return dtDashboard;
        }

        internal string getVisibilityStatus(string strLink, string strRole)
        {
            string result = "0";
            DataTable dt = new DataTable();
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getVisibilityStatus", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@LINK", SqlDbType.VarChar, 50).Value = strLink;
                command.Parameters.Add("@ROLE", SqlDbType.VarChar, 50).Value = strRole;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dt);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        result = dt.Rows[0]["Output"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return result;
        }

        public DataTable getBudgetDashboardData(string Branch, string UserId, string Role, string cter_type, string cter)
        {
            DataTable dtBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("sp_budgetDashboard", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BRANCH", SqlDbType.VarChar, 50).Value = Branch;
                command.Parameters.Add("@USERID", SqlDbType.VarChar, 50).Value = UserId;
                command.Parameters.Add("@ROLE", SqlDbType.VarChar, 50).Value = Role;
                command.Parameters.Add("@CTYPE", SqlDbType.VarChar, 50).Value = cter_type;
                command.Parameters.Add("@CTER", SqlDbType.VarChar, 50).Value = cter;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBudget);
                return dtBudget;
            }

            catch (Exception ex)
            {
                //LogFile("ERROR ", ex.Message, ex.InnerException.Message,  "=============================================================================================================");
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBudget;
        }
    }
}
