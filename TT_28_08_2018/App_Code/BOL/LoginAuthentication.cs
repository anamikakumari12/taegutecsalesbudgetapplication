﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace TaegutecSalesBudget
{
    public class LoginAuthentication
    {
        public string UserName;
        public string LoginMailID;
        public string MailPassword;
        public string EngineerId;
        public string RoleId;
        public string ErrorMessege;
        public int ErrorNum;
        public string BranchCode;
        public string BranchDesc;
        public string Territory;
        public string PhoneNumber;
        public int Quote_flag;
        public string Menu_ID;
        public string authLogin(LoginAuthentication login)
        {

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("LoginAuthentication", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@LoginMailId", SqlDbType.VarChar, 500).Value = login.LoginMailID;
                command.Parameters.Add("@MailPassword", SqlDbType.VarChar, 1000).Value = login.MailPassword;

                command.Parameters.Add(new SqlParameter("@EngineerName", SqlDbType.VarChar, 500));
                command.Parameters["@EngineerName"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@EngineerId", SqlDbType.VarChar, 255));
                command.Parameters["@EngineerId"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@RoleID", SqlDbType.VarChar, 500));
                command.Parameters["@RoleID"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@ErrorMessege", SqlDbType.VarChar, 100));
                command.Parameters["@ErrorMessege"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@ErrorNum", SqlDbType.Int));
                command.Parameters["@ErrorNum"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@Branchcode", SqlDbType.VarChar, 100));
                command.Parameters["@Branchcode"].Direction = ParameterDirection.Output;

                command.Parameters.Add(new SqlParameter("@BranchDesc", SqlDbType.VarChar, 100));
                command.Parameters["@BranchDesc"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@Territory", SqlDbType.VarChar, 100));
                command.Parameters["@Territory"].Direction = ParameterDirection.Output;

                command.Parameters.Add(new SqlParameter("@PhoneNumber", SqlDbType.VarChar, 15));
                command.Parameters["@PhoneNumber"].Direction = ParameterDirection.Output;

                command.Parameters.Add(new SqlParameter("@Quote_Flag", SqlDbType.Int));
                command.Parameters["@Quote_Flag"].Direction = ParameterDirection.Output;

                command.Parameters.Add(new SqlParameter("@Menu_ID", SqlDbType.VarChar, -1));
                command.Parameters["@Menu_ID"].Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();


                UserName = command.Parameters["@EngineerName"].Value.ToString();
                LoginMailID = command.Parameters["@LoginMailId"].Value.ToString();
                RoleId = command.Parameters["@RoleID"].Value.ToString();
                EngineerId = command.Parameters["@EngineerId"].Value.ToString();
                ErrorMessege = command.Parameters["@ErrorMessege"].Value.ToString();
                // ErrorNum = Convert.ToInt32(command.Parameters["@ErrorNum"].Value.ToString()==""||null?1:0);
                ErrorNum = Convert.ToString(command.Parameters["@ErrorNum"].Value) == "" || Convert.ToString(command.Parameters["@ErrorNum"].Value) == null ? 1 : Convert.ToInt32(Convert.ToString(command.Parameters["@ErrorNum"].Value));
                BranchCode = command.Parameters["@Branchcode"].Value.ToString();
                BranchDesc = command.Parameters["@BranchDesc"].Value.ToString();
                Territory = command.Parameters["@Territory"].Value.ToString();
                PhoneNumber = command.Parameters["@PhoneNumber"].Value.ToString();
                Quote_flag = Convert.ToInt32(command.Parameters["@Quote_Flag"].Value);
                Menu_ID = Convert.ToString(command.Parameters["@Menu_ID"].Value);
            }
            catch (Exception ex)
            {
                LogFile("Error: Login Page ", ex.Message.ToString(), ex.Data.Values.ToString(),
                    "-----------------------------------------------------------------------------------------------------------------");
                ErrorMessege = ex.Message;
                ErrorNum = 1;
            }
            finally
            {
                con.Close();
            }
            return ErrorMessege;

        }

        internal DataTable AuthenticateUserEmailId(LoginAuthentication authObj)
        {
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            DataTable dtOutput = new DataTable();
            SqlDataAdapter sqa;
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("sp_AuthenticateUserEmailId", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@LoginMailId", SqlDbType.VarChar, 500).Value = authObj.LoginMailID;
                sqa = new SqlDataAdapter(command);
                sqa.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                LogFile("Error: Login Page ", ex.Message.ToString(), ex.Data.Values.ToString(),
                    "-----------------------------------------------------------------------------------------------------------------");
                ErrorMessege = ex.Message;
                ErrorNum = 1;
            }
            finally
            {
                con.Close();
            }
            return dtOutput;
        }

        internal string GetTMBranches(LoginAuthentication obj_auth)
        {
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            DataTable dtOutput = new DataTable();
            SqlDataAdapter sqa;
            string output = string.Empty;
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("sp_getTMBranches", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@TM_id", SqlDbType.VarChar, 500).Value = obj_auth.RoleId;
                sqa = new SqlDataAdapter(command);
                sqa.Fill(dtOutput);
                if (dtOutput != null)
                    if (dtOutput.Rows.Count > 0)
                        output = Convert.ToString(dtOutput.Rows[0]["region"]);
            }
            catch (Exception ex)
            {
                output = string.Empty;
                LogFile("Error: Login Page ", ex.Message.ToString(), ex.Data.Values.ToString(),
                    "-----------------------------------------------------------------------------------------------------------------");
                ErrorMessege = ex.Message;
                ErrorNum = 1;
            }
            finally
            {
                con.Close();
            }
            return output;
        }
        internal string setFocusFlag(LoginAuthentication obj_auth)
        {
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            DataTable dtOutput = new DataTable();
            SqlDataAdapter sqa;
            string output = string.Empty;
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("sp_getFocusReportFlag", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@userId", SqlDbType.VarChar, 100).Value = obj_auth.EngineerId;
                sqa = new SqlDataAdapter(command);
                sqa.Fill(dtOutput);
                if (dtOutput != null)
                    if (dtOutput.Rows.Count > 0)
                        output = Convert.ToString(dtOutput.Rows[0]["Flag"]);
            }
            catch (Exception ex)
            {
                output = string.Empty;
                LogFile("Error: Login Page ", ex.Message.ToString(), ex.Data.Values.ToString(),
                    "-----------------------------------------------------------------------------------------------------------------");
                ErrorMessege = ex.Message;
                ErrorNum = 1;
            }
            finally
            {
                con.Close();
            }
            return output;
        }
        /// <summary>
        /// Author : Anamika
        /// Date : 07 Aug 2020
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public string AuthenticateUser(LoginAuthentication login)
        {

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("AuthenticationAsUser", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@EngineerId", SqlDbType.VarChar, 500).Value = login.EngineerId;

                command.Parameters.Add(new SqlParameter("@EngineerName", SqlDbType.VarChar, 500));
                command.Parameters["@EngineerName"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@LoginMailId", SqlDbType.VarChar, 255));
                command.Parameters["@LoginMailId"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@RoleID", SqlDbType.VarChar, 500));
                command.Parameters["@RoleID"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@ErrorMessege", SqlDbType.VarChar, 100));
                command.Parameters["@ErrorMessege"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@ErrorNum", SqlDbType.Int));
                command.Parameters["@ErrorNum"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@Branchcode", SqlDbType.VarChar, 100));
                command.Parameters["@Branchcode"].Direction = ParameterDirection.Output;

                command.Parameters.Add(new SqlParameter("@BranchDesc", SqlDbType.VarChar, 100));
                command.Parameters["@BranchDesc"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@Territory", SqlDbType.VarChar, 100));
                command.Parameters["@Territory"].Direction = ParameterDirection.Output;

                command.Parameters.Add(new SqlParameter("@PhoneNumber", SqlDbType.VarChar, 15));
                command.Parameters["@PhoneNumber"].Direction = ParameterDirection.Output;

                command.Parameters.Add(new SqlParameter("@Quote_Flag", SqlDbType.Int));
                command.Parameters["@Quote_Flag"].Direction = ParameterDirection.Output;

                command.Parameters.Add(new SqlParameter("@Menu_ID", SqlDbType.VarChar, -1));
                command.Parameters["@Menu_ID"].Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();


                UserName = command.Parameters["@EngineerName"].Value.ToString();
                LoginMailID = command.Parameters["@LoginMailId"].Value.ToString();
                RoleId = command.Parameters["@RoleID"].Value.ToString();
                EngineerId = command.Parameters["@EngineerId"].Value.ToString();
                ErrorMessege = command.Parameters["@ErrorMessege"].Value.ToString();
                // ErrorNum = Convert.ToInt32(command.Parameters["@ErrorNum"].Value.ToString()==""||null?1:0);
                ErrorNum = Convert.ToString(command.Parameters["@ErrorNum"].Value) == "" || Convert.ToString(command.Parameters["@ErrorNum"].Value) == null ? 1 : Convert.ToInt32(Convert.ToString(command.Parameters["@ErrorNum"].Value));
                BranchCode = command.Parameters["@Branchcode"].Value.ToString();
                BranchDesc = command.Parameters["@BranchDesc"].Value.ToString();
                Territory = command.Parameters["@Territory"].Value.ToString();
                PhoneNumber = command.Parameters["@PhoneNumber"].Value.ToString();
                Quote_flag = Convert.ToInt32(command.Parameters["@Quote_Flag"].Value);
                Menu_ID = Convert.ToString(command.Parameters["@Menu_ID"].Value);
            }
            catch (Exception ex)
            {
                LogFile("Error: Login Page ", ex.Message.ToString(), ex.Data.Values.ToString(),
                    "-----------------------------------------------------------------------------------------------------------------");
                ErrorMessege = ex.Message;
                ErrorNum = 1;
            }
            finally
            {
                con.Close();
            }
            return ErrorMessege;

        }

       public DataSet GetAllMenusBL(LoginAuthentication login)
        {
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            SqlDataAdapter sqlda;
            DataSet dtoutput = new DataSet();
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("sp_getMenusForBMS", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@UserEmailId", SqlDbType.VarChar, -1).Value = login.LoginMailID;
                sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dtoutput);

            }
            catch (Exception ex)
            {
                LogFile("Error: Login Page ", ex.Message.ToString(), ex.Data.Values.ToString(),
                    "-----------------------------------------------------------------------------------------------------------------");
                ErrorMessege = ex.Message;
                ErrorNum = 1;
            }
            finally
            {
                con.Close();
            }
            return dtoutput;
        }


        //reset password
        public string resetpwd(LoginAuthentication reset)
        {

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("resetpwd", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@EngineerId", SqlDbType.VarChar, 500).Value = reset.EngineerId;
                command.Parameters.Add("@Password", SqlDbType.VarChar, 1000).Value = reset.MailPassword;
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                LogFile("Error: Login Page ", ex.Message.ToString(), ex.Data.Values.ToString(),
                    "-----------------------------------------------------------------------------------------------------------------");
                ErrorMessege = ex.Message;
                ErrorNum = 1;
            }
            finally
            {
                con.Close();
            }
            return ErrorMessege;

        }
        //encrypt
        public string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        //decrpt
        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        public string setPassword(string emailId, string Password)
        {
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            string result;
            SqlConnection con = new SqlConnection(connstring);
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("setPassword", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Password", SqlDbType.VarChar, 500).Value = Password;
                command.Parameters.Add("@LoginMailId", SqlDbType.VarChar, 1000).Value = emailId;

                command.Parameters.Add(new SqlParameter("@ErrorMessege", SqlDbType.VarChar, 200));
                command.Parameters["@ErrorMessege"].Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                result = command.Parameters["@ErrorMessege"].Value.ToString();

            }
            catch (Exception ex)
            {

                result = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return result;
        }


        public void LogFile(string use1, string count, string names, string use)
        {
            StreamWriter log;

            if (!File.Exists("logfile.txt"))
            {
                log = new StreamWriter(("C:\\TSBA_Log\\logfile.txt"), true);
            }
            else
            {
                log = File.AppendText(("C:\\TSBA_Log\\logfile.txt"));
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(use1);
            log.WriteLine(count);
            log.WriteLine(names);
            log.WriteLine(use + "\n");
            // Close the stream:
            log.Close();

        }

        public string GetConfiguredValue(string module)
        {
            string Output = string.Empty;
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("select dbo.[fn_getSQLOTP]('"+ module + "')", con);
                command.CommandType = CommandType.Text;
                Output = Convert.ToString(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                LogFile("Error: Otp Page ", ex.Message.ToString(), ex.Data.Values.ToString(),
                    "-----------------------------------------------------------------------------------------------------------------");
            }
            finally
            {
                con.Close();
            }
            return Output;
        }
    }
}
