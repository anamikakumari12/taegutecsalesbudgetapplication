﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TaegutecSalesBudget
{
    public class EmailPassword
    {
        public string LoginMailID;
        public string Password;
        public string ErrorMessege;
        public int ErrorNum;

        public string sendmail(EmailPassword sndmail)
        {
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("EmailPassword", con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@LoginMailId", SqlDbType.VarChar, 500).Value = sndmail.LoginMailID;

                command.Parameters.Add(new SqlParameter("@Password", SqlDbType.VarChar, 500));
                command.Parameters["@Password"].Direction = ParameterDirection.Output;

                command.Parameters.Add(new SqlParameter("@ErrorMessege", SqlDbType.VarChar, 200));
                command.Parameters["@ErrorMessege"].Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@ErrorNum", SqlDbType.Int));
                command.Parameters["@ErrorNum"].Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                //MailMessage Msg = new MailMessage();
                Password = command.Parameters["@Password"].Value.ToString();
                ErrorMessege = command.Parameters["@ErrorMessege"].Value.ToString();
                ErrorNum = Convert.ToInt32(command.Parameters["@ErrorNum"].Value.ToString());


            }
            catch (Exception ex)
            {
                ErrorMessege = ex.Message;
                ErrorNum = 1;
            }
            finally
            {
                con.Close();
            }
            return ErrorMessege;
        }
    }
}
