﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;


namespace TaegutecSalesBudget
{
    public class EngineerInfo
    {
        public DataTable getEnginnerData(string hoRoleId = null, string bmRoleId = null, string seRoleId = null, string adminRoleId = null, string TmRoledId = null, string region = null, string cter = null)
        {

            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand cmnd = new SqlCommand("AdminEnquiry", conn);
                cmnd.CommandType = CommandType.StoredProcedure;
                cmnd.Parameters.Add("@hoflag", SqlDbType.VarChar, 50).Value = hoRoleId;
                cmnd.Parameters.Add("@bmflag", SqlDbType.VarChar, 50).Value = bmRoleId;
                cmnd.Parameters.Add("@seflag", SqlDbType.VarChar, 50).Value = seRoleId;
                cmnd.Parameters.Add("@adminflag", SqlDbType.VarChar, 50).Value = adminRoleId;
                cmnd.Parameters.Add("@tmflag", SqlDbType.VarChar, 50).Value = TmRoledId;
                cmnd.Parameters.Add("@regionselected", SqlDbType.VarChar, 50).Value = region;
                cmnd.Parameters.Add("@cter", SqlDbType.VarChar, 10).Value = cter;

                SqlDataAdapter da = new SqlDataAdapter(cmnd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dt;

        }
        public DataTable getUserDetails()
        {

            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand cmnd = new SqlCommand("sp_getAllEnginners", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmnd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            return dt;

        }
        public void Createnewuser(string Empnumber, string Empname, string EmpBranch, string Empmail, long Empcontact, int Empstatus, string Empusertype, string EmpPassword)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Createnewuser", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Empnumber", SqlDbType.VarChar, 50).Value = Empnumber;
                cmd.Parameters.Add("@Empname", SqlDbType.VarChar, 50).Value = Empname;
                cmd.Parameters.Add("@EmpBranch", SqlDbType.VarChar, 50).Value = EmpBranch;
                cmd.Parameters.Add("@Empmail", SqlDbType.VarChar, 50).Value = Empmail;
                cmd.Parameters.Add("@Empcontact", SqlDbType.BigInt).Value = Empcontact;
                cmd.Parameters.Add("@Empstatus", SqlDbType.Int).Value = Empstatus;
                cmd.Parameters.Add("@Empusertype", SqlDbType.VarChar, 50).Value = Empusertype;
                cmd.Parameters.Add("@EmpPassword", SqlDbType.VarChar, 50).Value = EmpPassword;
                cmd.ExecuteNonQuery();


            }
            catch (Exception e)
            {

            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }


        public int EditUserInfo(string Empnumber, string Empname, string EmpBranch, string Empmail, long Empcontact, int Empstatus, string Empusertype,string menuid)
        {
            int output = 0;
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("EditUserinfo", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Empnumber", SqlDbType.VarChar, 50).Value = Empnumber;
                cmd.Parameters.Add("@Empname", SqlDbType.VarChar, 50).Value = Empname;
                cmd.Parameters.Add("@EmpBranch", SqlDbType.VarChar, 50).Value = EmpBranch;
                cmd.Parameters.Add("@Empmail", SqlDbType.VarChar, 50).Value = Empmail;
                cmd.Parameters.Add("@Empcontact", SqlDbType.BigInt).Value = Empcontact;
                cmd.Parameters.Add("@Empstatus", SqlDbType.Int).Value = Empstatus;
                cmd.Parameters.Add("@Empusertype", SqlDbType.VarChar, 50).Value = Empusertype;
                cmd.Parameters.Add("@Menu_ID", SqlDbType.VarChar, -1).Value = menuid;
                SqlDataAdapter sqlda = new SqlDataAdapter(cmd);
                //cmd.ExecuteNonQuery();
                sqlda.Fill(dt);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        output = Convert.ToInt32(dt.Rows[0][0]);
                    }
                }

            }
            catch (Exception e)
            {

            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
            return output;
        }


        public void Deletebyadmin(string EngId, int flag)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand cmnd = new SqlCommand("DeletebyAdmin", conn);
                cmnd.CommandType = CommandType.StoredProcedure;
                cmnd.Parameters.Add("@Enggid", SqlDbType.VarChar, 50).Value = EngId;
                cmnd.Parameters.Add("@flag", SqlDbType.Int).Value = flag;
                cmnd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }

        public void CreateUserDet(string EngId, string EngName, string EngEmail, string status, long contactNum, string Role, string usermapped,string nousermapped,string branch)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand cmnd = new SqlCommand("sp_UpdateUserinfo", conn);
                cmnd.CommandType = CommandType.StoredProcedure;
                cmnd.Parameters.Add("@Empnumber", SqlDbType.VarChar, 50).Value = EngId;
                cmnd.Parameters.Add("@Empname", SqlDbType.VarChar,50).Value = EngName;
                cmnd.Parameters.Add("@Empmail", SqlDbType.VarChar, 50).Value = EngEmail;
                cmnd.Parameters.Add("@Empcontact", SqlDbType.BigInt).Value = contactNum;
                cmnd.Parameters.Add("@Empstatus", SqlDbType.Int).Value = status;
                cmnd.Parameters.Add("@Empusertype", SqlDbType.VarChar, 50).Value = Role;
                cmnd.Parameters.Add("@Empusermapping", SqlDbType.VarChar,-1).Value = usermapped;
                cmnd.Parameters.Add("@Empnousermapping", SqlDbType.VarChar, -1).Value = nousermapped;
                cmnd.Parameters.Add("@branch", SqlDbType.VarChar, -1).Value = branch;
                cmnd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }
        public void DeletebyadminBranch(string EngId, string Branchcode, int flag)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand cmnd = new SqlCommand("DeletebyAdminBranch", conn);
                cmnd.CommandType = CommandType.StoredProcedure;
                cmnd.Parameters.Add("@Enggid", SqlDbType.VarChar, 50).Value = EngId;
                cmnd.Parameters.Add("@Branchcode", SqlDbType.VarChar, 50).Value = Branchcode;
                cmnd.Parameters.Add("@flag", SqlDbType.Int).Value = flag;
                cmnd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }


        public DataTable LoadUserInfo(int flag, string cter = null)
        {
            DataTable dtUserInfo = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getBranchnotassigned", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@flag", SqlDbType.Int).Value = flag;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtUserInfo);
                return dtUserInfo;
            }

            catch (Exception ex) { }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return dtUserInfo;
        }

        public DataTable GetEngineerInfo(string Id)
        {
            DataTable dtUserInfo = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getEnginfo", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Id", SqlDbType.VarChar,50).Value = Id;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtUserInfo);
                return dtUserInfo;
            }

            catch (Exception ex) { }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return dtUserInfo;
        }


        public string InsertTag_for_territary(string Id, DataTable dtbranches)
        {
            DataTable tempCamp = new DataTable();
            tempCamp.Columns.Add("territory_engineer_id", typeof(string));
            tempCamp.Columns.Add("region_code", typeof(string));
            foreach (DataRow dr in dtbranches.Rows)
            {
                tempCamp.Rows.Add(Id, dr.ItemArray[0].ToString());
            }

            using (var bulkCopy = new SqlBulkCopy(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ConnectionString, SqlBulkCopyOptions.KeepNulls & SqlBulkCopyOptions.KeepIdentity))
            {
                //bulkCopy.BatchSize = (int)DetailLines;
                bulkCopy.DestinationTableName = "tt_territory_region_relation";
                bulkCopy.ColumnMappings.Clear();

                bulkCopy.ColumnMappings.Add("territory_engineer_id", "territory_engineer_id");
                bulkCopy.ColumnMappings.Add("region_code", "region_code");

                bulkCopy.WriteToServer(tempCamp);
                bulkCopy.Close();
            }
            return "";
        }

        public void Update_TE_for_Customer(string teId)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                conn.Open();
                SqlCommand cmnd = new SqlCommand("sp_UpdateRegion_for_TE", conn);
                cmnd.CommandType = CommandType.StoredProcedure;
                cmnd.Parameters.Add("@te_Id", SqlDbType.VarChar, 50).Value = teId;
                cmnd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }
    }

}