﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TaegutecSalesBudget
{
    public class AdminConfiguration
    {
        public string EngineerId, EngineerName, BranchCode, EmailId, RoleId, Password, defaultdevice1, defaultdevice2, requestedevice ;
        public int LoginStatus, Error_count;
        public long PhoneNumber;
        public DateTime requestedate, requestedtilldate, approveddate;
        CommonFunctions objCom = new CommonFunctions();
        public string GetProfile(string profile_name)
        {
            string strResult;
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("GetProfile", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@profile_name", SqlDbType.VarChar,50), //1
                new SqlParameter("@profile_display_value", SqlDbType.VarChar,50), //2
                new SqlParameter("@profile_value", SqlDbType.VarChar,50) //3
               };
            parameters[0].Value = profile_name;
            parameters[1].Direction = ParameterDirection.Output;
            parameters[2].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                string profile_value = command.Parameters["@profile_value"].Value.ToString();
                return strResult = profile_value;
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
                objCom.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return strResult;

        }

        public string UpdateProfile(string profile_name, string profile_value)
        {

            string strResult;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("Update_Profile", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@profile_name", SqlDbType.VarChar,100), //1
                new SqlParameter("@profile_value", SqlDbType.VarChar,100), //2
                new SqlParameter("@Error", SqlDbType.VarChar,200) //3
               };



            parameters[0].Value = profile_name;
            parameters[1].Value = profile_value;
            parameters[2].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                strResult = command.Parameters["@Error"].Value.ToString();

                return strResult;
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                objCom.LogError(ex);
                return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            // return strResult;

        }
        public int getBudgetYear()
        {
            int year = 0;

            string strResult;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("GetProfile", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@profile_name", SqlDbType.VarChar,50), //1
                new SqlParameter("@profile_display_value", SqlDbType.VarChar,50), //2
                new SqlParameter("@profile_value", SqlDbType.VarChar,50) //3
               };

            parameters[0].Value = "BUDGET_YEAR";
            parameters[1].Direction = ParameterDirection.Output;
            parameters[2].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                string profile_value = command.Parameters["@profile_value"].Value.ToString();
                year = Convert.ToInt32(profile_value);
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                objCom.LogError(ex);
                //return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return year;
        }
        public int getActualYear()
        {
            int year = 0;

            string strResult;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("GetProfile", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@profile_name", SqlDbType.VarChar,50), //1
                new SqlParameter("@profile_display_value", SqlDbType.VarChar,50), //2
                new SqlParameter("@profile_value", SqlDbType.VarChar,50) //3
               };

            parameters[0].Value = "ACTUAL_YEAR";
            parameters[1].Direction = ParameterDirection.Output;
            parameters[2].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                string profile_value = command.Parameters["@profile_value"].Value.ToString();
                year = Convert.ToInt32(profile_value);
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                objCom.LogError(ex);
                //return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return year;
        }
        public int getActualMonth()
        {
            int Month = 0;
            string strResult;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("GetProfile", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@profile_name", SqlDbType.VarChar,50), //1
                new SqlParameter("@profile_display_value", SqlDbType.VarChar,50), //2
                new SqlParameter("@profile_value", SqlDbType.VarChar,50) //3
               };



            parameters[0].Value = "ACTUAL_MONTH";
            parameters[1].Direction = ParameterDirection.Output;
            parameters[2].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                string profile_value = command.Parameters["@profile_value"].Value.ToString();
                Month = Convert.ToInt32(profile_value);
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                objCom.LogError(ex);
                //return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return Month;
        }

        public int getBudgetYear_B()
        {
            int year = 0;

            string strResult;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("GetProfile", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@profile_name", SqlDbType.VarChar,50), //1
                new SqlParameter("@profile_display_value", SqlDbType.VarChar,50), //2
                new SqlParameter("@profile_value", SqlDbType.VarChar,50) //3
               };

            parameters[0].Value = "B_BUDGET_YEAR";
            parameters[1].Direction = ParameterDirection.Output;
            parameters[2].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                string profile_value = command.Parameters["@profile_value"].Value.ToString();
                year = Convert.ToInt32(profile_value);
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                objCom.LogError(ex);
                //return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return year;
        }
        public int getActualYear_B()
        {
            int year = 0;

            string strResult;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("GetProfile", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@profile_name", SqlDbType.VarChar,50), //1
                new SqlParameter("@profile_display_value", SqlDbType.VarChar,50), //2
                new SqlParameter("@profile_value", SqlDbType.VarChar,50) //3
               };

            parameters[0].Value = "B_ACTUAL_YEAR";
            parameters[1].Direction = ParameterDirection.Output;
            parameters[2].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                string profile_value = command.Parameters["@profile_value"].Value.ToString();
                year = Convert.ToInt32(profile_value);
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                objCom.LogError(ex);
                //return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return year;
        }
        public int getActualMonth_B()
        {
            int Month = 0;
            string strResult;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("GetProfile", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@profile_name", SqlDbType.VarChar,50), //1
                new SqlParameter("@profile_display_value", SqlDbType.VarChar,50), //2
                new SqlParameter("@profile_value", SqlDbType.VarChar,50) //3
               };



            parameters[0].Value = "B_ACTUAL_MONTH";
            parameters[1].Direction = ParameterDirection.Output;
            parameters[2].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                string profile_value = command.Parameters["@profile_value"].Value.ToString();
                Month = Convert.ToInt32(profile_value);
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                objCom.LogError(ex);
                //return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return Month;
        }
        public string Insert_SalesEnginner(AdminConfiguration objAdmin)
        {

            string strResult;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("Insert_SalesEnginner", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters ={new SqlParameter("@EngineerId", SqlDbType.VarChar,100),//0
                                        new SqlParameter("@EngineerName", SqlDbType.VarChar,100), //1
                                       
                                        new SqlParameter("@EmailId", SqlDbType.VarChar,200), //3
                                        new SqlParameter("@RoleId", SqlDbType.VarChar,100),//4
                                        new SqlParameter("@Password", SqlDbType.VarChar,100),//5
                                        new SqlParameter("@LoginStatus", SqlDbType.Int),//6
                                         new SqlParameter("@PhoneNumber", SqlDbType.BigInt),//7
                                        new SqlParameter("@Error", SqlDbType.VarChar,100) //8
                                       };
            parameters[0].Value = objAdmin.EngineerId;
            parameters[1].Value = objAdmin.EngineerName;
            //parameters[2].Value = objAdmin.BranchCode;
            parameters[2].Value = objAdmin.EmailId;
            parameters[3].Value = objAdmin.RoleId;
            parameters[4].Value = objAdmin.Password;
            parameters[5].Value = objAdmin.LoginStatus;
            parameters[6].Value = objAdmin.PhoneNumber;
            parameters[7].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                strResult = command.Parameters["@Error"].Value.ToString();

                return strResult;
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                objCom.LogError(ex);
                return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            // return strResult;

        }
        public string Insert_SalesEnginner_original(AdminConfiguration objAdmin)
        {

            string strResult;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("Insert_SalesEnginner_original", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters ={new SqlParameter("@EngineerId", SqlDbType.VarChar,100),//0
                                        new SqlParameter("@EngineerName", SqlDbType.VarChar,100), //1
                                        new SqlParameter("@BranchCode", SqlDbType.VarChar,100), //2
                                        new SqlParameter("@EmailId", SqlDbType.VarChar,200), //3
                                        new SqlParameter("@RoleId", SqlDbType.VarChar,100),//4
                                        new SqlParameter("@Password", SqlDbType.VarChar,100),//5
                                        new SqlParameter("@LoginStatus", SqlDbType.Int),//6
                                         new SqlParameter("@PhoneNumber", SqlDbType.BigInt),//7
                                        new SqlParameter("@Error", SqlDbType.VarChar,100) //8
                                       };
            parameters[0].Value = objAdmin.EngineerId;
            parameters[1].Value = objAdmin.EngineerName;
            parameters[2].Value = objAdmin.BranchCode;
            parameters[3].Value = objAdmin.EmailId;
            parameters[4].Value = objAdmin.RoleId;
            parameters[5].Value = objAdmin.Password;
            parameters[6].Value = objAdmin.LoginStatus;
            parameters[7].Value = objAdmin.PhoneNumber;
            parameters[8].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                strResult = command.Parameters["@Error"].Value.ToString();

                return strResult;
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                objCom.LogError(ex);
                return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            // return strResult;

        }


        public string Update_SalesEnginner(AdminConfiguration objAdmin)
        {

            string strResult;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("Update_SalesEnginner", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters ={new SqlParameter("@EngineerId", SqlDbType.VarChar,100),//0
                                        new SqlParameter("@EngineerName", SqlDbType.VarChar,100), //1
                                        //new SqlParameter("@BranchCode", SqlDbType.VarChar,100), //2
                                        new SqlParameter("@EmailId", SqlDbType.VarChar,200), //3
                                        new SqlParameter("@RoleId", SqlDbType.VarChar,100),//4
                                         new SqlParameter("@PhoneNumber", SqlDbType.BigInt),//5
                                        new SqlParameter("@Error", SqlDbType.VarChar,100) //6
                                       };
            parameters[0].Value = objAdmin.EngineerId;
            parameters[1].Value = objAdmin.EngineerName;
            //parameters[2].Value = objAdmin.BranchCode;
            parameters[2].Value = objAdmin.EmailId;
            parameters[3].Value = objAdmin.RoleId;
            parameters[4].Value = objAdmin.PhoneNumber;
            parameters[5].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                strResult = command.Parameters["@Error"].Value.ToString();

                return strResult;
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                objCom.LogError(ex);
                return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public string Delete_or_block_SalesEngineer(string SE_Id, string flag)
        {
            string strResult;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("Delete_or_block_SalesEngineer", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters ={new SqlParameter("@EngineerCode", SqlDbType.VarChar,100),//0
                                        new SqlParameter("@flag", SqlDbType.VarChar,100), //1
                                        new SqlParameter("@Error", SqlDbType.VarChar,100), //2
                                        new SqlParameter("@Error_count", SqlDbType.Int) //3
                                       };
            parameters[0].Value = SE_Id;
            parameters[1].Value = flag;
            parameters[2].Direction = ParameterDirection.Output;
            parameters[3].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                strResult = command.Parameters["@Error"].Value.ToString();
                Error_count = Convert.ToInt32(command.Parameters["@Error_count"].Value.ToString());
                return strResult;
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                objCom.LogError(ex);
                return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public AdminConfiguration getEngineerDetails(string EnginnerCode)
        {
            AdminConfiguration objAdmin = new AdminConfiguration();
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("getEngineerDetails", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters ={new SqlParameter("@EngineerId", SqlDbType.VarChar,100),//0
                                        new SqlParameter("@EngineerName", SqlDbType.VarChar,100), //1
                                        new SqlParameter("@BranchCode", SqlDbType.VarChar,100), //2
                                        new SqlParameter("@EmailId", SqlDbType.VarChar,200), //3
                                        new SqlParameter("@RoleId", SqlDbType.VarChar,100),
                                         new SqlParameter("@PhoneNumber", SqlDbType.BigInt)
                                       };
            parameters[0].Value = EnginnerCode;
            parameters[1].Direction = ParameterDirection.Output;
            parameters[2].Direction = ParameterDirection.Output;
            parameters[3].Direction = ParameterDirection.Output;
            parameters[4].Direction = ParameterDirection.Output;
            parameters[5].Direction = ParameterDirection.Output;
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                objAdmin.EngineerId = EnginnerCode;
                objAdmin.EngineerName = command.Parameters["@EngineerName"].Value.ToString();
                objAdmin.BranchCode = command.Parameters["@BranchCode"].Value.ToString();
                objAdmin.EmailId = command.Parameters["@EmailId"].Value.ToString();
                objAdmin.RoleId = command.Parameters["@RoleId"].Value.ToString();
                objAdmin.PhoneNumber = Convert.ToInt64(command.Parameters["@PhoneNumber"].Value);
            }

            catch (Exception ex)
            {
                objCom.LogError(ex);
                //strResult = ex.Message;
                //return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return objAdmin;
        }

        public DataTable getUserInfo()
        {
            DataTable dtUserInfo = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getUserInfo", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtUserInfo);
                return dtUserInfo;
            }

            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return dtUserInfo;
        }
        public DataTable getActiveUsers()
        {
            DataTable dtActiveUsers = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("activeUsers", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtActiveUsers);
                return dtActiveUsers;
            }

            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return dtActiveUsers;
        }
        //public DataTable getcustomers(string branchcode)
        //{
        //    DataTable dtcust = new DataTable();
        //    string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
        //    SqlConnection connection = new SqlConnection(connstring);
        //    try
        //    {
        //        connection.Open();
        //        SqlCommand command = new SqlCommand("getCustDetailsbybranch", connection);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add("@branchcode", SqlDbType.VarChar, 50).Value = branchcode;
        //        SqlDataAdapter sqlDa = new SqlDataAdapter(command);
        //        sqlDa.Fill(dtcust);
        //        return dtcust;
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //        if (connection.State == ConnectionState.Open)
        //        {
        //            connection.Close();
        //        }
        //    }
        //    return dtcust;
        //}

        //public DataTable getcustomersby_empcode(string empcode, string branchcode)
        //{
        //    DataTable dtcust = new DataTable();
        //    string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
        //    SqlConnection connection = new SqlConnection(connstring);
        //    try
        //    {
        //        connection.Open();
        //        SqlCommand command = new SqlCommand("getCustDetailsbySE", connection);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add("@assigned_salesengineer_id", SqlDbType.VarChar, 50).Value = empcode;
        //        command.Parameters.Add("@branchcode", SqlDbType.VarChar, 50).Value = branchcode;
        //        SqlDataAdapter sqlDa = new SqlDataAdapter(command);
        //        sqlDa.Fill(dtcust);
        //        return dtcust;
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //        if (connection.State == ConnectionState.Open)
        //        {
        //            connection.Close();
        //        }
        //    }
        //    return dtcust;
        //}

        //public string Update_Cust_SalesEnginner(string se_id, string cust_num)
        //{

        //    string strResult;

        //    string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
        //    SqlConnection connection = new SqlConnection(connstring);
        //    connection.Open();
        //    SqlCommand command = new SqlCommand("update_cust_seid", connection);
        //    command.CommandType = CommandType.StoredProcedure;
        //    command.Parameters.Add(new SqlParameter("@salesengineer_id", SqlDbType.VarChar, 30)).Value = se_id;
        //    command.Parameters.Add(new SqlParameter("@customer_number", SqlDbType.VarChar, 200)).Value = cust_num;
        //    command.Parameters.Add(new SqlParameter("@Error", SqlDbType.VarChar, 200));
        //    command.Parameters["@Error"].Direction = ParameterDirection.Output;




        //    try
        //    {
        //        command.ExecuteNonQuery();
        //        strResult = command.Parameters["@Error"].Value.ToString();

        //        return strResult;
        //    }

        //    catch (Exception ex)
        //    {
        //        strResult = ex.Message;
        //        return strResult;
        //    }

        //    finally
        //    {
        //        if (connection.State == ConnectionState.Open)
        //        {
        //            connection.Close();
        //        }
        //    }
        //}
        //public string insert_Cust_SalesEnginner(string se_id, string cust_num)
        //{

        //    string strResult;

        //    string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
        //    SqlConnection connection = new SqlConnection(connstring);
        //    connection.Open();
        //    SqlCommand command = new SqlCommand("insert_cust_seid", connection);
        //    command.CommandType = CommandType.StoredProcedure;
        //    command.Parameters.Add(new SqlParameter("@salesengineer_id", SqlDbType.VarChar, 30)).Value = se_id;
        //    command.Parameters.Add(new SqlParameter("@customer_number", SqlDbType.VarChar, 200)).Value = cust_num;
        //    command.Parameters.Add(new SqlParameter("@Error", SqlDbType.VarChar, 200));
        //    command.Parameters["@Error"].Direction = ParameterDirection.Output;




        //    try
        //    {
        //        command.ExecuteNonQuery();
        //        strResult = command.Parameters["@Error"].Value.ToString();

        //        return strResult;
        //    }

        //    catch (Exception ex)
        //    {
        //        strResult = ex.Message;
        //        return strResult;
        //    }

        //    finally
        //    {
        //        if (connection.State == ConnectionState.Open)
        //        {
        //            connection.Close();
        //        }
        //    }
        //}


        internal string saveVisibility(string strLink, int TM, int HO)
        {
            string result = "0";
            DataTable dt = new DataTable();
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_savevisibility", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@LINK", SqlDbType.VarChar, 50).Value = strLink;
                command.Parameters.Add("@TM", SqlDbType.VarChar, 50).Value = TM;
                command.Parameters.Add("@HO", SqlDbType.VarChar, 50).Value = HO;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dt);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        result = dt.Rows[0]["Output"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return result;
        }

        internal DataTable getVisibility()
        {
            DataTable dt = new DataTable();
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getvisibility", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dt);
                
            }
            catch (Exception ex)
            {

                objCom.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dt;
        }

        internal DataTable getBranch()
        {
            DataTable dt = new DataTable();
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getAllBranch", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dt);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dt;
        }

        internal string saveEnableBudget(int year, string branch)
        {
            string output = string.Empty;
            int rows;
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_EnableBudgetForBranch", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@YEAR", SqlDbType.Int).Value = year;
                command.Parameters.Add("@BRANCH", SqlDbType.VarChar, 100).Value = branch;
                rows=command.ExecuteNonQuery();
                if(rows>0)
                    output = "SUCCESS";
                else
                    output = "FAILED";
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
                output = "FAILED";
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return output;
        }

        internal int jobstart()
        {
            int output = 1;
            //int rows;
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_GALSyncProcessJobStart", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter FLAG= new SqlParameter("@FLAG",SqlDbType.Int);
                FLAG.Direction = ParameterDirection.Output;
                command.Parameters.Add(FLAG);
                output=Convert.ToInt32(command.ExecuteScalar());    
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);               
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return output;
        }


        internal DataTable devicedetails()
        {
            DataTable dt = new DataTable();
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getLoginDevicedetails", connection);
                command.CommandType = CommandType.StoredProcedure;
               // command.Parameters.Add("@emailid", SqlDbType.VarChar, 100).Value = EmailId;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dt);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dt;
        }



        internal string EditDeviceDetails(AllowDevicesBOL objDev)
        {
            DataTable dt = new DataTable();
            string msg = "Not Updated please try again ";
           
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            try
            {
            var dateAndTime = DateTime.Now;

                connection.Open();
                SqlCommand command = new SqlCommand("sp_updateLoginDevicedetails", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@email", SqlDbType.VarChar, 100).Value = Convert.ToString(objDev.EmailID);
                command.Parameters.Add("@defaultdevice1", SqlDbType.VarChar, 100).Value = Convert.ToString(objDev.DefaultDevice1);
                command.Parameters.Add("@defaultdevice2", SqlDbType.VarChar, 100).Value = Convert.ToString(objDev.DefaultDevice2);
                command.Parameters.Add("@requesteddevice", SqlDbType.VarChar, 100).Value = Convert.ToString(objDev.RequestedDevice);
                command.Parameters.Add("@statusofrequesteddevice ", SqlDbType.Int, 100).Value = Convert.ToInt32(objDev.Status);
                command.Parameters.Add("@timeperiod", SqlDbType.Int, 100).Value = Convert.ToInt32(objDev.TimePeriod);
                command.Parameters.Add("@flag", SqlDbType.VarChar, 100).Value = "UpdateStatusOfRequestedDevice";
                command.Parameters.Add("@approveddate  ", SqlDbType.Date, 100).Value = dateAndTime.Date;

                int count =command.ExecuteNonQuery();
                if (count > 0)
                {
                    msg="Successfully updated";
                }
                else
                {
                    msg = "not updated sucessfully please try again";
                }


            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return msg;
        }
    }

    public partial class AllowDevicesBOL
    {
        public string EmailID { get; set; }
        public string DefaultDevice1 { get; set; }
        public string DefaultDevice2 { get; set; }
        public string RequestedDevice { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public int Status { get; set; }
        public int TimePeriod { get; set; }
    }

}