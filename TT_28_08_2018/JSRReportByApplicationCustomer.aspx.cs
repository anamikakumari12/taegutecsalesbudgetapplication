﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class JSRReportByApplicationCustomer : System.Web.UI.Page
    {
        #region Global Declaration
        csJSR objJSR = new csJSR();
        csJSRDAL objJSRDAL = new csJSRDAL();
        CommonFunctions objCom = new CommonFunctions();
        List<string> cssList = new List<string>();
        List<string> cssListFamilyHead = new List<string>();
        public static int ActualYear_tooltip, CurrentYear_tooltip;
        AdminConfiguration objAdmin = new AdminConfiguration();
        public static string ActualMonth_tooltip;
        #endregion

        #region Events
        /// <summary>
        /// Author : Anamika
        /// Date : May 1, 2017
        /// Desc : Load the grid table with the selected filters on the JSRReportByApplication page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            ActualYear_tooltip = objAdmin.getActualYear() - 1;
            CurrentYear_tooltip = objAdmin.getActualYear();
            int ActualMonthNumber = objAdmin.getActualMonth();
            ActualMonth_tooltip = getActualMonthinString(ActualMonthNumber);
            if (!IsPostBack)
            {
                if (Session["ValueIn"] == null) { Session["ValueIn"] = 1000; rbtn_Thousand.Checked = true; }
                else if (Convert.ToInt32(Session["ValueIn"]) == 100000) rbtn_Lakhs.Checked = true;
                LoadReport();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 1, 2017
        /// Desc : Export to Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void exportexcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "JSRReportByCustomer.xls"));
                Response.ContentType = "application/ms-excel";
                DataTable dtExcel = new DataTable();
                dtExcel = (DataTable)Session["dtCustomer"];
                DataTable dtExport;
                dtExport = dtExcel.Copy();
                dtExport.Columns.Remove("ID");
                dtExport.Columns.Remove("BRANCH_CODE");
                dtExport.Columns.Remove("SE_ID");
                dtExport.Columns.Remove("sumFlag");
                dtExport.Columns.Remove("DISPLAY_VALUE");
                dtExport.Columns.Remove("Line_desc");
                if (!(Convert.ToBoolean(Session["CompanyCode"])) || Convert.ToString(dtExport.Rows[1]["ShowGP"])=="0")
                {
                    dtExport.Columns.Remove("GP_MONTHLY");
                    dtExport.Columns.Remove("GP_YEARLY");
                }
                objJSR.jsr_month = Convert.ToInt32(Request.QueryString["month"]);
                objJSR.jsr_year = Convert.ToInt32(Request.QueryString["year"]);
                if (Convert.ToString(Request.QueryString["branch"]) != null)
                    objJSR.branch = Convert.ToString("'" + Request.QueryString["branch"] + "'");
                else
                    objJSR.branch = Convert.ToString(Session["ExportBranchList"]);
                if (Convert.ToString(Request.QueryString["se"]) != null)
                    objJSR.salesengineer_id = Convert.ToString("'" + Request.QueryString["se"] + "'");
                else
                    objJSR.salesengineer_id = Convert.ToString(Session["ExportSalesEngineers"]);
                if (Convert.ToString(Request.QueryString["cnum"]) != null)
                    objJSR.c_num = Convert.ToString("'" + Request.QueryString["cnum"] + "'");
                else
                    objJSR.c_num = Convert.ToString(Session["ExportCustomers"]);

                objJSR.customer_type = Convert.ToString(Session["ddlcustomertype.SelectedItem.Value"]);
                objJSR.family = Convert.ToString(Session["ExportFamily"]);
                objJSR.subfamily = Convert.ToString(Session["ExportSubFamily"]);
                objJSR.application = Convert.ToString(Session["ExportApp"]);
                objJSR.p_group = Convert.ToString(Session["ExportProductGroup"]);
                objJSR.cter = Convert.ToString(Session["cter"]);

                GridView grdExportExcel = new GridView();
                grdExportExcel.DataSource = dtExport;
                grdExportExcel.DataBind();

                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a table to contain the grid
                        Table table = new Table();
                        table.GridLines = grdExportExcel.GridLines;

                        foreach (GridViewRow row in grdExportExcel.Rows)
                        {
                            row.Attributes.Add("font-weight", "bold");
                            table.Rows.Add(row);
                        }
                        table.Rows[0].Height = 30;
                        table.Rows[0].BackColor = Color.LightSeaGreen;
                        table.Rows[0].Font.Bold = true;

                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            if (Convert.ToString(table.Rows[i].Cells[3].Text).Contains("CUTTING TOOLS TOTAL"))
                            {
                                table.Rows[i].BackColor = Color.LightCyan;
                                table.Rows[i].Font.Bold = true;
                            }
                            else if (Convert.ToString(table.Rows[i].Cells[3].Text).Contains("TOTAL"))
                            {
                                if (Convert.ToString(table.Rows[i].Cells[3].Text).Contains("SE"))
                                {
                                    table.Rows[i].BackColor = Color.Black;
                                    table.Rows[i].ForeColor = Color.White;
                                    table.Rows[i].Font.Bold = true;
                                }
                                else
                                {
                                    table.Rows[i].BackColor = Color.Gray;
                                    // table.Rows[i].ForeColor = Color.White;
                                    table.Rows[i].Font.Bold = true;
                                }
                            }
                        }
                        sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px; '>JSR Report By Customer</td></table>");
                        sw.WriteLine("<table style='margin-left: 200px;'>");

                        
                            sw.WriteLine("<tr><td></td><td></td><td></td><td style='font-weight: bold;'>TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.cter + "</td></tr>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>MONTH :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + objJSR.jsr_month + "</td></tr>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>YEAR :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + objJSR.jsr_year + "</td></tr>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.branch+ "</td></tr>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.salesengineer_id + "</td></tr>");
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER TYPE : " + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.customer_type + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.c_num + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT GROUP :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.p_group + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT FAMILY :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.family + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT SUB-FAMILY :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.subfamily + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>APPLICATION :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.application + "</td></tr>");
                        sw.WriteLine("</table><br/>");
                        table.RenderControl(htw);


                        //sw.WriteLine("TERRITORY :" + "" + objJSR.cter + "<br/>");
                        //sw.WriteLine("MONTH :" + "" + objJSR.jsr_month + "<br/>");
                        //sw.WriteLine("YEAR :" + "" + objJSR.jsr_year + "<br/>");
                        //sw.WriteLine("BRANCH :" + "" + objJSR.branch + "<br/>");
                        //sw.WriteLine("SALES ENGINEERS :" + "" + objJSR.salesengineer_id + "<br/>");
                        //sw.WriteLine("CUSTOMER TYPE : " + "" + objJSR.customer_type + "<br/>");
                        //sw.WriteLine("CUSTOMER NAME :" + "" + objJSR.c_num + "<br/>");
                        //sw.WriteLine("PRODUCT GROUP :" + "" + objJSR.p_group + "<br/>" + "<br/>");
                        //sw.WriteLine("PRODUCT FAMILY :" + "" + objJSR.family + "<br/>" + "<br/>");
                        //sw.WriteLine("PRODUCT SUB-FAMILY :" + "" + objJSR.subfamily + "<br/>" + "<br/>");
                        //sw.WriteLine("APPLICATION :" + "" + objJSR.application + "<br/>" + "<br/>");


                        //table.RenderControl(htw);
                    }

                    Response.Write(sw.ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                    Response.End();
                }


            }
            catch (Exception ex)
            {
                // objFunc.LogError(ex);
            }

        }

        /// <summary>
        /// Author : Anamika
        /// Date : July 10, 2017
        /// Desc : Export in Excel family wise
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void exportfamily_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "JSRReportByCustomer_Branch.xls"));
                Response.ContentType = "application/ms-excel";
                DataTable dtExcel = new DataTable();
                dtExcel = (DataTable)Session["dtCustomer"];
                DataTable dtFamilyExport;
                dtFamilyExport = dtExcel.Clone();
                DataRow drFirst = dtExcel.Rows[0];
                dtFamilyExport.Rows.Add(drFirst.ItemArray);
                DataRow[] dr = dtExcel.Select("sumFlag ='FamilySum'");
                foreach (DataRow dr1 in dr)
                {
                    dtFamilyExport.Rows.Add(dr1.ItemArray);
                }
                DataTable dtExport;
                dtExport = dtFamilyExport.Copy();
                dtExport.Columns.Remove("ID");
                dtExport.Columns.Remove("BRANCH_CODE");
                dtExport.Columns.Remove("SE_ID");
                dtExport.Columns.Remove("sumFlag");
                dtExport.Columns.Remove("DISPLAY_VALUE");
                dtExport.Columns.Remove("Line_desc");
                if (!(Convert.ToBoolean(Session["CompanyCode"])) || Convert.ToString(dtExport.Rows[1]["ShowGP"]) == "0")
                {
                    dtExport.Columns.Remove("GP_MONTHLY");
                    dtExport.Columns.Remove("GP_YEARLY");
                }

                objJSR.jsr_month = Convert.ToInt32(Request.QueryString["month"]);
                objJSR.jsr_year = Convert.ToInt32(Request.QueryString["year"]);
                if (Convert.ToString(Request.QueryString["branch"]) != null)
                    objJSR.branch = Convert.ToString("'" + Request.QueryString["branch"] + "'");
                else
                    objJSR.branch = Convert.ToString(Session["ExportBranchList"]);
                if (Convert.ToString(Request.QueryString["se"]) != null)
                    objJSR.salesengineer_id = Convert.ToString("'" + Request.QueryString["se"] + "'");
                else
                    objJSR.salesengineer_id = Convert.ToString(Session["ExportSalesEngineers"]);
                if (Convert.ToString(Request.QueryString["cnum"]) != null)
                    objJSR.c_num = Convert.ToString("'" + Request.QueryString["cnum"] + "'");
                else
                    objJSR.c_num = Convert.ToString(Session["ExportCustomers"]);

                objJSR.customer_type = Convert.ToString(Session["ddlcustomertype.SelectedItem.Value"]);
                objJSR.family = Convert.ToString(Session["ExportFamily"]);
                objJSR.subfamily = Convert.ToString(Session["ExportSubFamily"]);
                objJSR.application = Convert.ToString(Session["ExportApp"]);
                objJSR.p_group = Convert.ToString(Session["ExportProductGroup"]);
                objJSR.cter = Convert.ToString(Session["cter"]);

                GridView grdExportExcel = new GridView();
                grdExportExcel.DataSource = dtExport;
                grdExportExcel.DataBind();

                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a table to contain the grid
                        Table table = new Table();
                        table.GridLines = grdExportExcel.GridLines;

                        foreach (GridViewRow row in grdExportExcel.Rows)
                        {
                            table.Rows.Add(row);
                        }
                        table.Rows[0].Height = 30;
                        table.Rows[0].BackColor = Color.LightSeaGreen;
                        table.Rows[0].Font.Bold = true;
                        sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px; '>JSR Report By Customer Summary</td></table>");
                        sw.WriteLine("<table style='margin-left: 200px;'>");


                        sw.WriteLine("<tr><td></td><td></td><td></td><td style='font-weight: bold;'>TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.cter + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>MONTH :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + objJSR.jsr_month + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>YEAR :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + objJSR.jsr_year + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.branch + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.salesengineer_id + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER TYPE : " + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.customer_type + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.c_num + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT GROUP :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.p_group + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT FAMILY :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.family + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT SUB-FAMILY :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.subfamily + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>APPLICATION :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.application + "</td></tr>");
                        sw.WriteLine("</table><br/>");

                        table.RenderControl(htw);
                    }

                    Response.Write(sw.ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                    Response.End();
                }


            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Thread") == false)
                {
                    //Response.Redirect("~/Error.aspx?ErrorMsg = " + ex.Message);
                }
                // objFunc.LogError(ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Thousand_CheckedChanged(object sender, EventArgs e)
        {
            Session["ValueIn"] = 1000;
            LoadReport();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void Lakhs_CheckedChanged(object sender, EventArgs e)
        {

            Session["ValueIn"] = 100000;
            LoadReport();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }
        #endregion

        #region Methods
        /// <summary>
        /// Author : Anamika
        /// Date : May 1, 2017
        /// Desc : Bind the gridview with the data fetched from database based on all the filters
        /// </summary>
        protected void LoadReport()
        {
            DataTable dtReport = new DataTable();
            try
            {
                objJSR.jsr_month = Convert.ToInt32(Request.QueryString["month"]);
                objJSR.jsr_year = Convert.ToInt32(Request.QueryString["year"]);

                objJSR.branch = Convert.ToString(Session["SelectedBranchList"]);
                objJSR.salesengineer_id = Convert.ToString(Session["SelectedSalesEngineers"]);
                objJSR.c_num = Convert.ToString(Session["SelectedCustomerNumbers"]);
                objJSR.customer_type = Convert.ToString(Session["ddlcustomertype.SelectedItem.Value"]);
                if (Convert.ToString(Request.QueryString["family"]) != null)
                    objJSR.family = Convert.ToString("'" + Request.QueryString["family"] + "'");
                else
                    objJSR.family = Convert.ToString(Session["SelectedProductFamily"]);
                if (Convert.ToString(Request.QueryString["subfamily"]) != null)
                    objJSR.subfamily = Convert.ToString("'" + Request.QueryString["subfamily"] + "'");
                else
                    objJSR.subfamily = Convert.ToString(Session["SelectedSubFamily"]);
                if (Convert.ToString(Request.QueryString["appcode"]) != null)
                    objJSR.application = Convert.ToString("'" + Request.QueryString["appcode"] + "'");
                else
                    objJSR.application = Convert.ToString(Session["SelectedApplications"]);
                objJSR.p_group = Convert.ToString(Session["SelectedProductGroup"]);
                objJSR.cter = Convert.ToString(Session["cter"]);
                objJSR.fiterby = "CUSTOMER";
                objJSR.valueIn = Convert.ToInt32(Session["ValueIn"]);
                objJSR.roleId = Convert.ToString(Session["RoleId"]);
                LoadCSS();
                dtReport = objJSRDAL.getReport(objJSR);
                dtReport.Columns.Add("ShowGP");
                var col = dtReport.Columns["ShowGP"];
                
                if (Convert.ToBoolean(Request.QueryString["flag"]) == true)
                {
                    foreach (DataRow row in dtReport.Rows)
                        row[col] = 1;
                }
                else
                {
                    foreach (DataRow row in dtReport.Rows)
                        row[col] = 0;
                }
                if (dtReport.Rows.Count > 0)
                {
                    if (objJSR.valueIn == 100000)
                        lblResult.Text = "All vlaues are in lakhs.";
                    else
                        lblResult.Text = "All vlaues are in thousands.";
                    grdviewAllValues.DataSource = dtReport;
                    grdviewAllValues.DataBind();
                    bindgridColor();
                    Session["dtCustomer"] = dtReport;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                }
                else
                {
                    lblResult.Text = "No data to display.";
                    grdviewAllValues.DataSource = null;
                    grdviewAllValues.DataBind();
                    Session["dtCustomer"] = null;
                }

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 1, 2017
        /// Desc : Apply css with rows
        /// </summary>
        protected void bindgridColor()
        {
            if (grdviewAllValues.Rows.Count != 0)
            {
                int colorIndex = 0;
                int rowIndex = 1, subRowIndex = 0, productTypeIndex = 1; int color = 0, parent_row_index = 0;
                bool currentRowIsLast = false;
                foreach (GridViewRow row in grdviewAllValues.Rows)
                {

                    var check = row.FindControl("lblSumFlag") as Label;
                    string txt = check.Text;


                    //3.% Growth Monthly Sale / GROWTH_MONTHLY
                    Label lbl_GROWTH_MONTHLY = row.FindControl("lbl_GROWTH_MONTHLY") as Label;
                    lbl_GROWTH_MONTHLY.ToolTip = "(MTD Sale " + CurrentYear_tooltip + " – MTD Sale" + ActualYear_tooltip + ")*100/MTD sale" + ActualYear_tooltip;

                    //2.% ACH W.R.T Jan Target / BUDGET_MONTHLY
                    Label lbl_BUDGET_MONTHLY = row.FindControl("lbl_BUDGET_MONTHLY") as Label;
                    lbl_BUDGET_MONTHLY.ToolTip = "(MTD Sale" + CurrentYear_tooltip + "/Budget" + ActualMonth_tooltip + ") *100";

                    //8. Monthly G.P %
                    Label lbl_GP_MONTHLY = row.FindControl("lbl_GP_MONTHLY") as Label;
                    lbl_GP_MONTHLY.ToolTip = " (MTD Sale" + CurrentYear_tooltip + "– SUM(C_MONTH" + ActualMonth_tooltip + ")/1000)/MTD sale" + CurrentYear_tooltip + " *100";

                    //12. Yearly G.P%/ GP_YEARLY
                    Label lbl_GP_YEARLY = row.FindControl("lbl_GP_YEARLY") as Label;

                    lbl_GP_YEARLY.ToolTip = "(YTD SALE" + ActualYear_tooltip + " – COST)*100/ YTD SALE" + ActualYear_tooltip;

                    //13.% GROWTH YEARLY SALE
                    Label lbl_GROWTH_YEARLY = row.FindControl("lbl_GROWTH_YEARLY") as Label;
                    lbl_GROWTH_YEARLY.ToolTip = "(YTD SALE" + CurrentYear_tooltip + "– YTD SALE" + ActualYear_tooltip + ")*100/YTD SALE" + ActualYear_tooltip;

                    //16.% ACH W.R.T 2019 TARGET

                    Label lbl_BUDGET_YEARLY = row.FindControl("lbl_BUDGET_YEARLY") as Label;
                    lbl_BUDGET_YEARLY.ToolTip = "(YTD SALE" + CurrentYear_tooltip + "/ Budget value for" + CurrentYear_tooltip + ")*100";

                    //20.%Growth Yearly Qty / GROWTH_YEARLY_QTY

                    Label lbl_GROWTH_YEARLY_QTY = row.FindControl("lbl_GROWTH_YEARLY_QTY") as Label;

                    lbl_GROWTH_YEARLY_QTY.ToolTip = "(YTS Sale" + CurrentYear_tooltip + " – YTD Sale" + ActualYear_tooltip + ")*100/YTD Sale " + ActualYear_tooltip;




                    if (txt == "typeSum")
                    {
                        row.CssClass = "product_row_hide subTotalRowGrid subrowindex subrowindex_" + subRowIndex + " ";

                        productTypeIndex++;
                    }
                    else if (txt == "SubFamilySum")
                    {
                        row.CssClass = "product_row_hide greendarkSubFamSum subrowindex subrowindex_" + subRowIndex;
                    }
                    else if (txt == "SubFamilyHeading")
                    {
                        row.CssClass = "product_row_hide greendark row_index row_" + rowIndex + " parent_row_index_" + parent_row_index;

                        var image = row.FindControl("Image1") as System.Web.UI.WebControls.Image;
                        image.CssClass = "row_index_image";

                        row.Attributes["data-index"] = rowIndex.ToString();
                        rowIndex++;
                        subRowIndex++;
                    }
                    else if (txt == "products")
                    {
                        color++;
                        if (color == 1) { row.CssClass = "color_Product1 "; }
                        else if (color == 2)
                        {
                            row.CssClass = "color_Product2 ";
                            color = 0;
                        }

                        row.CssClass += "product_row_hide product_type_" + productTypeIndex + " subrowindex subrowindex_" + subRowIndex;
                        //var txtQuantityP = row.FindControl("txt_ActualQuantity_sales_qty_year_P") as TextBox;
                        //txtQuantityP.CssClass += " product_type product_type_" + productTypeIndex;
                        //txtQuantityP.Attributes["data-product_type"] = productTypeIndex.ToString();

                        //var lblnextYearValue = row.FindControl("lbl_ActualValue_sales_value_year_P_New") as Label;
                        //lblnextYearValue.CssClass += "lblnextYearValue_" + productTypeIndex;
                    }
                    else if (txt == "FamilyHeading")
                    {
                        row.CssClass = "parent_row_index";
                        row.Attributes["data-parent_row_index"] = "" + (++parent_row_index);

                        if (colorIndex <= 6)
                        {
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                            }
                            //row.CssClass = GetCssFam(colorIndex); 
                            colorIndex++;
                        }
                        else
                        {
                            colorIndex = 0;
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                            }
                        }



                    }
                    else if (txt == "BranchHeading")
                    {
                        row.CssClass = "parent_row_index";
                        row.Attributes["data-parent_row_index"] = "" + (++parent_row_index);

                        if (colorIndex <= 6)
                        {
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                            }
                            //row.CssClass = GetCssFam(colorIndex); 
                            colorIndex++;
                        }
                        else
                        {
                            colorIndex = 0;
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                            }
                        }



                    }
                    else if (txt == "SEHeading")
                    {
                        row.CssClass = "product_row_hide greendark row_index row_" + rowIndex + " parent_row_index_" + parent_row_index;

                        var image = row.FindControl("Image1") as System.Web.UI.WebControls.Image;
                        image.CssClass = "row_index_image";

                        row.Attributes["data-index"] = rowIndex.ToString();
                        rowIndex++;
                        subRowIndex++;
                    }
                    else if (txt == "FamilySum")
                    {
                        row.CssClass = "product_row_hide TotalRowGrid" + " parent_row_index_" + parent_row_index;
                        currentRowIsLast = true;

                    }
                    else if (txt == "MainSum")
                    {
                        row.CssClass = "MainTotal";
                    }

                    else if (txt == "")
                    {
                        row.CssClass = "empty_row";
                        if (!currentRowIsLast)
                        {
                            row.CssClass += " product_row_hide ";
                        }
                        currentRowIsLast = false;
                        for (int i = 0; i < row.Cells.Count; i++)
                        {
                            row.Cells[i].CssClass = "HidingHeading";
                        }
                    }
                    else if (txt == "HidingHeading")
                    {

                        for (int i = 0; i < row.Cells.Count; i++)
                        { row.Cells[i].CssClass = "greendark MainHeader"; }
                    }
                    else if (txt == "sumFlag")
                    {
                        for (int i = 0; i < row.Cells.Count; i++)
                        { row.Cells[i].CssClass = "greendark MainHeader"; }
                    }

                }
            }




        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 1, 2017
        /// Desc :
        /// </summary>
        protected void LoadCSS()
        {
            cssList.Add("color_3");
            cssList.Add("color_4");
            cssList.Add("color_3");
            //cssList.Add("color_5");
            //cssList.Add("color_4");
            //cssList.Add("color_2");
            //cssList.Add("greendark");

            cssListFamilyHead.Add("heading1");
            cssListFamilyHead.Add("heading2");
            cssListFamilyHead.Add("heading3");
            cssListFamilyHead.Add("heading4");
            cssListFamilyHead.Add("heading5");
            cssListFamilyHead.Add("heading6");
            cssListFamilyHead.Add("heading7");

        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 1, 2017
        /// Desc :
        /// </summary>
        /// <param name="colorIndex"></param>
        /// <returns></returns>
        protected string GetCSS(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssList.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssList.ElementAt(2); }
            //else if (cIndex.Contains("3"))
            //{ return cssList.ElementAt(3); }
            //else if (cIndex.Contains("4"))
            //{ return cssList.ElementAt(4); }
            //else if (cIndex.Contains("5"))
            //{ return cssList.ElementAt(5); }
            else { return cssList.ElementAt(2); }

        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 1, 2017
        /// Desc :
        /// </summary>
        /// <param name="colorIndex"></param>
        /// <returns></returns>
        protected string GetCssFam(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssListFamilyHead.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssListFamilyHead.ElementAt(2); }
            else if (cIndex.Contains("3"))
            { return cssListFamilyHead.ElementAt(3); }
            else if (cIndex.Contains("4"))
            { return cssListFamilyHead.ElementAt(4); }
            else if (cIndex.Contains("5"))
            { return cssListFamilyHead.ElementAt(5); }
            else if (cIndex.Contains("6"))
            { return cssListFamilyHead.ElementAt(6); }
            else { return cssListFamilyHead.ElementAt(0); }
        }


        public string getActualMonthinString(int ActualMonthNumber)
        {
            switch (ActualMonthNumber)
            {
                case 1: ActualMonth_tooltip = "Jan";
                    break;
                case 2: ActualMonth_tooltip = "Jan";
                    break;
                case 3: ActualMonth_tooltip = "Jan";
                    break;
                case 4: ActualMonth_tooltip = "Jan";
                    break;
                case 5: ActualMonth_tooltip = "Jan";
                    break;
                case 6: ActualMonth_tooltip = "Jan";
                    break;
                case 7: ActualMonth_tooltip = "Jan";
                    break;
                case 8: ActualMonth_tooltip = "Jan";
                    break;
                case 9: ActualMonth_tooltip = "Jan";
                    break;
                case 10: ActualMonth_tooltip = "Jan";
                    break;
                case 11: ActualMonth_tooltip = "Jan";
                    break;
                case 12: ActualMonth_tooltip = "Jan";
                    break;
                default:
                    ActualMonth_tooltip = "Nothing ";
                    break;
            }
            return ActualMonth_tooltip;

        }
        #endregion

    }
}