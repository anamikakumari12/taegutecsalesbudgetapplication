﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class SiteOld : System.Web.UI.MasterPage
    {
        LoginAuthentication authObj = new LoginAuthentication();
        AdminConfiguration objAdmin = new AdminConfiguration();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx"); }
            displayProjectDashboard();
            displayBudgetTab();

            //lblUserName.Text = Session["UserName"].ToString();
            if (!IsPostBack)
            {

                System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                if (Session["UserName"] != null)
                {
                    #region SaveModules
                    SaveModules();
                    #endregion
                    if (String.IsNullOrEmpty(Convert.ToString(Session["UserRole"])))
                    {
                        lblUserName.Text = Convert.ToString(Session["UserName"]);
                        lnkAdminProfile.Visible = false;
                    }
                    else
                    {
                        lblUserName.Text = "Admin(Logged In As " + Convert.ToString(Session["UserName"]) + ")";
                        lnkAdminProfile.Visible = true;
                    }

                    if (Session["Territory"].ToString() == "DUR")
                    {
                        usernav.Style.Add("background-color", "#044758 ");
                        // lblUserName.Style.Add("color", "#FFDE00");
                    }
                }
                displayBudgetTab();

            }
            if (Session["RoleId"] != null)
            {
                if (Session["RoleId"].ToString() == "Admin")
                {
                    if (Request.QueryString.Get(0) == "Budget") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "Summary") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "Reports") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "Dashboard") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RS") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RO") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RMT") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RV") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RD") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "Quote") { Response.Redirect("AdminProfile.aspx?Profile"); return; }

                    if (Request.QueryString.Get(0) == "MDPEntry" || Request.QueryString.Get(0) == "MDPReports" || Request.QueryString.Get(0) == "Projects")
                    {
                        Response.Redirect("AdminProfile.aspx?Profile");
                        //Session["Project"] = "New";
                        return;
                    }
                }

                if (Convert.ToString(Session["FocusFlag"]) == "1")
                {

                    FP1.Visible = true;
                    FP2.Visible = true;
                    FP3.Visible = true;
                    FP4.Visible = true;
                    FP5.Visible = true;
                }
                if (Convert.ToString(Session["Quote_ref"]) == "1")
                {
                    //if (Convert.ToString(Session["RoleId"]) == "HO" || Convert.ToString(Session["RoleId"]) == "BM" || Convert.ToString(Session["RoleId"]) == "TM")
                    //{
                    qmList1.Visible = true;
                    qmList2.Visible = true;
                    qmList3.Visible = true;
                    qmList4.Visible = true;
                    qmList5.Visible = true;
                    if (Convert.ToString(Session["RoleId"]) == "HO")
                    {
                        quoteReqList.Visible = false;
                        quoteReqList1.Visible = false;
                        quoteReqList2.Visible = false;
                        quoteReqList3.Visible = false;
                        quoteReqList4.Visible = false;
                    }
                    else
                    {
                        quoteReqList.Visible = true;
                        quoteReqList1.Visible = true;
                        quoteReqList2.Visible = true;
                        quoteReqList3.Visible = true;
                        quoteReqList4.Visible = true;
                    }
                    if (Convert.ToString(Session["RoleId"]) == "SE")
                    {
                        quoteList.Visible = false;
                        quoteList1.Visible = false;
                        quoteList2.Visible = false;
                        quoteList3.Visible = false;
                        quoteList4.Visible = false;
                    }
                    else
                    {
                        quoteList.Visible = true;
                        quoteList1.Visible = true;
                        quoteList2.Visible = true;
                        quoteList3.Visible = true;
                        quoteList4.Visible = true;
                    }
                    //}
                    //else
                    //{
                    //    qmList1.Visible = true;
                    //    qmList2.Visible = true;
                    //    qmList3.Visible = true;
                    //    qmList4.Visible = true;
                    //    qmList5.Visible = true;
                    //    quoteReqList.Visible = true;
                    //    quoteReqList1.Visible = true;
                    //    quoteReqList2.Visible = true;
                    //    quoteReqList3.Visible = true;
                    //    quoteReqList4.Visible = true;
                    //    quoteList.Visible = false;
                    //    quoteList1.Visible = false;
                    //    quoteList2.Visible = false;
                    //    quoteList3.Visible = false;
                    //    quoteList4.Visible = false;
                    //}
                }
                else
                {
                    qmList1.Visible = false;
                    qmList2.Visible = false;
                    qmList3.Visible = false;
                    qmList4.Visible = false;
                    qmList5.Visible = false;
                }
                //if (Session["RoleId"].ToString() == "HO")
                //{
                //    sereview1.Visible = true;
                //    sereview2.Visible = true;
                //    sereview3.Visible = true;
                //    sereview4.Visible = true;
                //    //li_review.Visible = false;
                //    //li_review1.Visible = false;
                //    //li_review2.Visible = false;
                //    //if (Request.QueryString.Get(0) == "RS") { Response.Redirect("Reports.aspx?Reports"); return; }
                //    //if (Request.QueryString.Get(0) == "RO") { Response.Redirect("Reports.aspx?Reports"); return; }
                //    //if (Request.QueryString.Get(0) == "RMT") { Response.Redirect("Reports.aspx?Reports"); return; }
                //    //if (Request.QueryString.Get(0) == "RV") { Response.Redirect("Reports.aspx?Reports"); return; }
                //}
                //else
                //{
                //    sereview1.Visible = false;
                //    sereview2.Visible = false;
                //    sereview3.Visible = false;
                //    sereview4.Visible = false;
                //}

            }
            Session["organization_id"] = "1";
            Session["p_user_name"] = "bsalla";
            Session["p_user_id"] = "1";
            Session["employee_id"] = "1";
            Session["employee_name"] = "Bakta G Salla";
            Session["exp_supervisor_flag"] = "Y";
            Session["vst_supervisor_flag"] = "Y";
            Session["access_exp_module"] = "1";
            Session["access_vst_module"] = "1";
            Session["access_crm_module"] = "1";
            Session["error_code"] = "0";
            Session["error_message"] = "Login Successful";
        }

        private void displayProjectDashboard()
        {
            if (Convert.ToString(Session["ProjectDashboardDisplay"]) == "N")
            {
                Dashboard1.Visible = false;
                Dashboard2.Visible = false;
                Dashboard3.Visible = false;
                Dashboard4.Visible = false;
                Dashboard5.Visible = false;
            }
            else
            {
                Dashboard1.Visible = true;
                Dashboard2.Visible = true;
                Dashboard3.Visible = true;
                Dashboard4.Visible = true;
                Dashboard5.Visible = true;
            }
        }

        protected void lnkbtnLogout_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] != null)
            {

                Session["UserName"] = null;
                Session["LoginMailId"] = null;
                Session["RoleId"] = null;
                Session["UserId"] = null;
                Session["Customer_Number"] = null;
                Session["Project_Number"] = null;
                Session["Distributor_Number"] = null;
                Session["cter"] = null;

                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("Login.aspx");

                //ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }
        protected void confirmpwd_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] != null)
            {

                string strpassword = authObj.Encrypt(pwd.Text);
                authObj.MailPassword = strpassword;
                authObj.EngineerId = Convert.ToString(Session["UserId"]);
                string ErrorMessage = authObj.resetpwd(authObj);
                if (ErrorMessage == null)
                {
                    string scriptString = "<script type='text/javascript'> alert('Password was successfully reset');window.location='Login.aspx';</script>";
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                }
                else
                {
                    string scriptString = "<script type='text/javascript'> alert('Failed to reset');window.location='Login.aspx';</script>";
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void displayBudgetTab()
        {
            if (Session["RoleId"] != null)
            {
                string role = Session["RoleId"].ToString();
                string startDate = "", endDate = "";
                int currentDate = Convert.ToInt32(DateTime.Today.ToString("yyyyMMdd"));
                if (role == "SE")
                {
                    startDate = objAdmin.GetProfile("START_DATE_SE");
                    endDate = objAdmin.GetProfile("END_DATE_SE");
                    int start = startDate == "" ? -1 : Convert.ToInt32(Regex.Replace(startDate, "-", ""));
                    int end = endDate == "" ? -1 : Convert.ToInt32(Regex.Replace(endDate, "-", ""));
                    int maincount = start == -1 ? start : start - currentDate;
                    int count = end == -1 ? end : end - currentDate;
                    if (count >= 0 && maincount <= 0)
                    {
                        li_budgetTab.Visible = true;
                        li_budgetTab1.Visible = true;
                        li_budgetTab2.Visible = true;
                        li_budgetTab3.Visible = true;
                        li_budgetTab4.Visible = true;
                    }
                    BDD1.Visible = false;
                    BDD2.Visible = false;
                    BDD3.Visible = false;
                    BDD4.Visible = false;
                    BDD5.Visible = false;
                }
                else if (role == "BM")
                {
                    startDate = objAdmin.GetProfile("START_DATE_BM");
                    endDate = objAdmin.GetProfile("END_DATE_BM");
                    int start = startDate == "" ? -1 : Convert.ToInt32(Regex.Replace(startDate, "-", ""));
                    int end = endDate == "" ? -1 : Convert.ToInt32(Regex.Replace(endDate, "-", ""));
                    int maincount = start == -1 ? start : start - currentDate;
                    int count = end == -1 ? end : end - currentDate;
                    if (count >= 0 && maincount <= 0)
                    {
                        li_budgetTab.Visible = true;
                        li_budgetTab1.Visible = true;
                        li_budgetTab2.Visible = true;
                        li_budgetTab3.Visible = true;
                        li_budgetTab4.Visible = true;
                    }
                }
                else if (role == "TM")
                {

                    startDate = objAdmin.GetProfile("START_DATE_TM");
                    endDate = objAdmin.GetProfile("END_DATE_TM");
                    int start = startDate == "" ? -1 : Convert.ToInt32(Regex.Replace(startDate, "-", ""));
                    int end = endDate == "" ? -1 : Convert.ToInt32(Regex.Replace(endDate, "-", ""));
                    int maincount = start == -1 ? start : start - currentDate;
                    int count = end == -1 ? end : end - currentDate;
                    if (count >= 0 && maincount <= 0)
                    {
                        li_budgetTab.Visible = true;
                        li_budgetTab1.Visible = true;
                        li_budgetTab2.Visible = true;
                        li_budgetTab3.Visible = true;
                        li_budgetTab4.Visible = true;
                    }
                }
                else if (role == "HO")
                {
                    startDate = objAdmin.GetProfile("START_DATE_HO");
                    endDate = objAdmin.GetProfile("END_DATE_HO");
                    int start = startDate == "" ? -1 : Convert.ToInt32(Regex.Replace(startDate, "-", ""));
                    int end = endDate == "" ? -1 : Convert.ToInt32(Regex.Replace(endDate, "-", ""));
                    int maincount = start == -1 ? start : start - currentDate;
                    int count = end == -1 ? end : end - currentDate;
                    if (count >= 0 && maincount <= 0)
                    {
                        li_budgetTab.Visible = true;
                        li_budgetTab1.Visible = true;
                        li_budgetTab2.Visible = true;
                        li_budgetTab3.Visible = true;
                        li_budgetTab4.Visible = true;
                    }
                }

            }
        }


        [WebMethod]
        public static string SetSession()
        {
            string output = "";
            try
            {
                HttpContext.Current.Session["Chart"] = "gdfjgsdf";

            }
            catch (Exception ex)
            {
                // objCom.ErrorLog(ex);
            }
            return output;
        }



        /// <summary>
        /// Author:K.LakshmiBindu
        /// Desc: For Saving userlogs based on time with module info used by user
        /// Date : Feb 12, 2019
        /// </summary>
        public void SaveModules()
        {
            int i = 0;
            SqlConnection cn = null;

            try
            {
                cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
                SqlCommand cmd = new SqlCommand("sp_saveModuleLogs", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter[] sp = new SqlParameter[]{
               new SqlParameter("@EngineerId",Convert.ToString(Session["UserId"])),
               new SqlParameter("@UniqueId",Convert.ToString(Session["UserGuid"])),
               new SqlParameter("@Module",Request.FilePath)
            };
                cmd.Parameters.AddRange(sp);
                cn.Open();
                i = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }


        protected void lnkAdminProfile_Click(object sender, EventArgs e)
        {
            lblUserName.Text = "Admin";
            lnkAdminProfile.Visible = false;
            authObj = Session["AdminauthObj"] as LoginAuthentication;
            String UserGuid = System.Guid.NewGuid().ToString();
            Session["UserGuid"] = UserGuid;
            Session["UserName"] = authObj.UserName;
            Session["LoginMailId"] = authObj.LoginMailID;
            Session["RoleId"] = authObj.RoleId;
            Session["UserId"] = authObj.EngineerId;
            Session["BranchCode"] = authObj.BranchCode;
            Session["BranchDesc"] = authObj.BranchDesc;
            Session["Territory"] = authObj.Territory;
            Session["EngineerId"] = authObj.EngineerId;
            Session["Password"] = authObj.MailPassword;
            Response.Redirect("AdminProfile.aspx?Profile");
        }
    }
}