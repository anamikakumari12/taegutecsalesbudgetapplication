﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using Microsoft.PowerBI.Api;
using Microsoft.PowerBI.Api.Models;


namespace TaegutecSalesBudget
{
    public partial class ReportDashboard : System.Web.UI.Page
    {
        public string embedToken;
        public string embedUrl;
        public Guid reportId;
        CommonFunctions objCom = new CommonFunctions();
        public static string cter;
        public string report_id = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserName"] != null)
                {
                    try
                    {
                        cter = null;
                        if (Convert.ToBoolean(Session["CompanyCode"]))
                        {
                            if (Session["cter"] == null)
                            {
                                Session["cter"] = "TTA";
                                cter = "TTA";

                            }
                            if (Session["cter"].ToString() == "DUR")
                            {
                                rdBtnDuraCab.Checked = true;
                                rdBtnTaegutec.Checked = false;
                                cter = "DUR";
                            }
                            else
                            {
                                rdBtnTaegutec.Checked = true;
                                rdBtnDuraCab.Checked = false;
                                cter = "TTA";
                            }
                            cterDiv.Visible = true;
                        }


                        System.Net.ServicePointManager.SecurityProtocol |= System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                        using (var client = new PowerBIClient(new Uri(Configurations.ApiUrl), Authentication.GetTokenCredentials()))
                        {
                            // Get a list of reports
                            var reports = client.Reports.GetReportsInGroup(Configurations.WorkspaceId);

                            //// Populate dropdown list
                            foreach (Report item in reports.Value)
                            {
                                // ddlReport.Items.Add(new ListItem(item.Name, item.Id.ToString()));
                                if (item.Name == BMSResource.ReportDashboard)
                                {
                                    report_id = item.Id.ToString();
                                }
                            }
                            ViewState["report_id"] = report_id;
                            //// Select first item
                            //ddlReport.SelectedIndex = 2;
                        }
                        // Generate an embed token and populate embed variables
                        LoadReport();
                    }
                    catch (Exception ex)
                    {
                        objCom.LogError(ex);
                    }
                }
            }

        }


        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                if (rdBtnTaegutec.Checked)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                }
                if (rdBtnDuraCab.Checked)
                {
                    Session["cter"] = "DUR";
                    cter = "DUR";
                }
                LoadReport();


            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void clear_Click(object sender, EventArgs e)
        {
            LoadReport();
        }

        protected void Export_Click(object sender, EventArgs e)
        {
            try
            {
                string date = DateTime.Now.Date.ToString("yyyy-MM-dd");
                Budget objBudget = new Budget();
                DataSet dt = objBudget.GetDataForSaleReport();

                dt.Tables[0].Rows[0].Delete();
                dt.Tables[0].AcceptChanges();

                dt.Tables[1].Rows[0].Delete();
                dt.Tables[1].AcceptChanges();

                dt.Tables[2].Rows[0].Delete();
                dt.Tables[2].AcceptChanges();

                dt.Tables[3].Rows[0].Delete();
                dt.Tables[3].AcceptChanges();

                dt.Tables[4].Rows[0].Delete();
                dt.Tables[4].AcceptChanges();

                GridView grdExportExcel = new GridView();
                grdExportExcel.DataSource = dt.Tables[0];
                grdExportExcel.DataBind();

                GridView grdExportExcel1 = new GridView();
                grdExportExcel1.DataSource = dt.Tables[1];
                grdExportExcel1.DataBind();

                GridView grdExportExcel2 = new GridView();
                grdExportExcel2.DataSource = dt.Tables[2];
                grdExportExcel2.DataBind();

                GridView grdExportExcel3 = new GridView();
                grdExportExcel3.DataSource = dt.Tables[3];
                grdExportExcel3.DataBind();

                GridView grdExportExcel4 = new GridView();
                grdExportExcel4.DataSource = dt.Tables[4];
                grdExportExcel4.DataBind();

                GridView[] gvExcel = new GridView[] { grdExportExcel, grdExportExcel1, grdExportExcel2, grdExportExcel3, grdExportExcel4 };
                string[] name = new string[] { "Total Sales", "Engineer Wise Sales", "Customer Wise Sales", "CP Wise Sales", "Region Wise Sales" };
                var aCode = 65;
                int i = 0;
                using (XLWorkbook wb = new XLWorkbook())
                {
                    foreach (GridView grid in gvExcel)
                    {
                        var ws = wb.Worksheets.Add(name[i]);
                        int rowIndex = 3;
                        int columnIndex = 0;
                        var range = ws.Range("E1:G1");
                        range.Merge().Style.Font.SetBold().Font.FontSize = 12;
                        range.Merge().Style.Font.Bold = true;
                        range.Merge().Style.Font.FontColor = XLColor.Black;
                        range.Merge().Value = name[i];

                        foreach (TableCell cell in grid.HeaderRow.Cells)
                        {
                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Value = cell.Text;
                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Fill.BackgroundColor = XLColor.LightSeaGreen;
                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Font.Bold = true;
                            columnIndex++;
                        }
                        rowIndex++;
                        foreach (GridViewRow row in grid.Rows)
                        {
                            int valueCount = 0;
                            foreach (TableCell rowValue in row.Cells)
                            {
                                if (rowValue.Text == "&nbsp;")
                                {
                                    ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Value = null;
                                    ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                    valueCount++;
                                }
                                else
                                {
                                    ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Value = rowValue.Text;
                                    ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                    valueCount++;
                                }
                            }
                            rowIndex++;
                        }
                        i++;

                    }
                    //Response.Clear();
                    //Response.Buffer = true;
                    //Response.Charset = "";
                    //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //Response.AddHeader("content-disposition", "attachment;filename=Sales Report " + date + ".xlsx");
                    //using (MemoryStream MyMemoryStream = new MemoryStream())
                    //{

                    //    wb.SaveAs(MyMemoryStream);
                    //    MyMemoryStream.WriteTo(Response.OutputStream);
                    //    //Response.Flush();
                    //    //Response.End();
                    //    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    //    //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    //    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    //}
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=Sales Report " + date + ".xlsx");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {

                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
                


            }


            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        private string GetNextFileName(string pathname, string fileName)
        {
            string date = DateTime.Now.Date.ToString("yyyy-MM-dd");
            string extension = Path.GetExtension(fileName);
            string pathName = Path.GetDirectoryName(pathname);
            string onlyfilename = Path.GetFileNameWithoutExtension(fileName);

            int fCount = Directory.GetFiles(pathname, fileName, SearchOption.AllDirectories).Length;
            int fCount1 = Directory.GetFiles(pathname, "Sales Report " + date + "(*", SearchOption.AllDirectories).Length;
            if (fCount == 0 && fCount1 == 0)
            {
                fileName = fileName;
            }
            else if (fCount == 1 && fCount1 >= 0)
            {
                fileName = string.Format("{0}({1}){2}", onlyfilename, fCount1 + 1, extension);
            }

            return fileName;
        }
        protected void LoadReport()
        {
            try
            {
                report_id = Convert.ToString(ViewState["report_id"]);
                using (var client = new PowerBIClient(new Uri(Configurations.ApiUrl), Authentication.GetTokenCredentials()))
                {

                    // Retrieve the selected report
                    var report = client.Reports.GetReportInGroup(Configurations.WorkspaceId, new Guid(report_id));

                    // Generate an embed token to view
                    var generateTokenRequestParameters = new GenerateTokenRequest(TokenAccessLevel.View);
                    var tokenResponse = client.Reports.GenerateTokenInGroup(Configurations.WorkspaceId, report.Id, generateTokenRequestParameters);
                    var newUrl = "";
                    // Populate embed variables (to be passed client-side)
                    //if (Convert.ToString(Session["RoleId"]) == "SE")
                    //    newUrl = report.EmbedUrl + "&$filter=vw_ReportDashboard/assigned_salesengineer_id eq '" + Convert.ToString(Session["UserId"]) + "'";
                    //else if (Convert.ToString(Session["RoleId"]) == "BM")
                    //    newUrl = report.EmbedUrl + "&$filter=vw_ReportDashboard/Customer_region eq '" + Convert.ToString(Session["BranchCode"]) + "'";
                    //else if (Convert.ToString(Session["RoleId"]) == "TM")
                    //    newUrl = report.EmbedUrl + "&$filter=vw_ReportDashboard/Customer_region in (" + Convert.ToString(Session["TMBranches"]) + ")";
                    //else if (Convert.ToString(Session["RoleId"]) == "HO")
                    //    newUrl = report.EmbedUrl + "&$filter=vw_ReportDashboard/cter eq '" + cter + "'";

                    newUrl = report.EmbedUrl + "&$filter=vw_ReportDashboard/assigned_salesengineer_id in (" + Convert.ToString(Session["MappedUsers"]) + ")";
                    if (!string.IsNullOrEmpty(cter))
                        newUrl = newUrl + " and vw_ReportDashboard/cter eq '" + cter + "'";
                    objCom.LogMessage(newUrl);
                    report.EmbedUrl = newUrl;
                    embedToken = tokenResponse.Token;
                    embedUrl = report.EmbedUrl;
                    reportId = report.Id;
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void ExportOverAllFeedBack_Click(object sender, EventArgs e)
        {
            try
            {

                string date = DateTime.Now.Date.ToString("yyyy-MM-dd");
                string month = DateTime.Now.ToString("MMM");
                Budget objBudget = new Budget();
                DataSet dt = objBudget.getMonthlyOverallSFEEDSales();

                GridView grdExportExcel = new GridView();
                grdExportExcel.DataSource = dt.Tables[1];
                grdExportExcel.DataBind();

                GridView grdExportExcel1 = new GridView();
                grdExportExcel1.DataSource = dt.Tables[0];
                grdExportExcel1.DataBind();

                GridView[] gvExcel = new GridView[] { grdExportExcel, grdExportExcel1 };
                string[] name = new string[] { "Overall_Jan_To_"+month+"", "SFEEDTEC_Jan_To_" + month + "" };
                var aCode = 65;
                int i = 0;
                using (XLWorkbook wb = new XLWorkbook())
                {
                    foreach (GridView grid in gvExcel)
                    {
                        var ws = wb.Worksheets.Add(name[i]);
                        int rowIndex = 3;
                        int columnIndex = 0;
                        var range = ws.Range("E1:G1");
                        range.Merge().Style.Font.SetBold().Font.FontSize = 12;
                        range.Merge().Style.Font.Bold = true;
                        range.Merge().Style.Font.FontColor = XLColor.Black;
                        range.Merge().Value = name[i];
                        foreach (TableCell cell in grid.HeaderRow.Cells)
                        {
                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Value = cell.Text;
                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Fill.BackgroundColor = XLColor.LightSeaGreen;
                            ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + columnIndex), rowIndex)).Style.Font.Bold = true;
                            columnIndex++;
                        }
                        rowIndex++;
                        foreach (GridViewRow row in grid.Rows)
                        {
                            int valueCount = 0;
                            foreach (TableCell rowValue in row.Cells)
                            {
                                if (rowValue.Text == "&nbsp;")
                                {
                                    ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Value = null;
                                    ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                    valueCount++;
                                }
                                else
                                {
                                    ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Value = rowValue.Text;
                                    ws.Cell(string.Format("{0}{1}", Char.ConvertFromUtf32(aCode + valueCount), rowIndex)).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                                    valueCount++;
                                }
                            }
                            rowIndex++;
                        }

                        i++;

                    }

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=OverAll TTA Sales Report.xlsx");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {

                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        
    }
}