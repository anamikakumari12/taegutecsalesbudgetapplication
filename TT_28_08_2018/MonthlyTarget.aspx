﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlyTarget.aspx.cs" Inherits="TaegutecSalesBudget.MonthlyTarget" MasterPageFile="~/Site.Master" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%--  <asp:DropDownList ID="ddlBranchList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged"
                                CssClass="form-control select2" Width="230px">
                            </asp:DropDownList>--%>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <script src="js/jquery.sumoselect.min.js"></script>
    <link href="css/sumoselect.css" rel="stylesheet" />
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />

    <script src="js/dataTables.fixedColumns.js"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            debugger;

            //$('#Imgcollapsedropdwns').unbind('click').bind('click', function (e) {
            //        var attr = $('#Imgcollapsedropdwns').attr('src');
            //        $("#MainContent_reportdrpdwns").slideToggle();
            //        if (attr == "images/up_arrow.png") {
            //            $("#Imgcollapsedropdwns").attr("src", "images/down_arrow.png");
            //        } else {
            //            $("#Imgcollapsedropdwns").attr("src", "images/up_arrow.png");
            //        }
            //});

            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#MainContent_reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });


            var currentmonth = "";
            $.ajax({
                url: 'MonthlyTarget.aspx/LoadCurrentMonth',
                method: 'post',
                datatype: 'json',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    debugger;
                    data = JSON.parse(data.d);
                    currentmonth = data.msg;
                    $('select.month')[0].sumo.selectItem(data.msg);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });



        });

        //function selectAll() {
        //    debugger;
        //    var cumulateddata = 0;
        //    var totaltrCount = $("[id*=MainContent_grdMonthlySales] tr").length;
        //    var trCount = totaltrCount;
        //    var askingRate = 0;

        //    for (i = 0; i < trCount; i++) {
        //        var askingRate = $("#MainContent_grdMonthlySales_lblAsking_Rate_" + i + "").text();
        //        var asr = askingRate;
        //        var monthlist = $("#MainContent_MonthList").val();
        //        var currentmonthvalue = $("#MainContent_grdMonthlySales_txtValue" + monthlist + "_" + i + "").val();
        //        if (currentmonthvalue == "") {
        //            currentmonthvalue = "0.00";
        //        }
        //        var askingRate1 = askingRate;
        //        if ($('#MainContent_ValInUnit').is(':checked') == true) {
        //            currentmonthvalue = currentmonthvalue;
        //            askingRate = askingRate;
        //        }
        //        if ($('#MainContent_ValInThsnd').is(':checked') == true) {
        //            currentmonthvalue = parseFloat((currentmonthvalue) * 1000).toFixed(2);
        //            askingRate = parseFloat((askingRate) * 1000).toFixed(2);

        //        }
        //        if ($('#MainContent_ValInLakh').is(':checked') == true) {
        //            currentmonthvalue = parseFloat((currentmonthvalue) * 100000).toFixed(2);
        //            askingRate = parseFloat((askingRate) * 100000).toFixed(2);

        //        }
        //        if (asr != "0") {
        //            var completionrate = parseFloat((((currentmonthvalue - askingRate) / askingRate) * 100).toFixed(2));
        //            if (monthlist.length == 1) {
        //                if (completionrate < 0) {
        //                    if (completionrate == -100) {
        //                        $("#MainContent_grdMonthlySales_lblCompletionRate_" + i + "").text("0%");
        //                    }
        //                    else {
        //                        $("#MainContent_grdMonthlySales_lblCompletionRate_" + i + "").text(completionrate + "%");
        //                    }
        //                }
        //                else {
        //                    $("#MainContent_grdMonthlySales_lblCompletionRate_" + i + "").text("+" + completionrate + "%");
        //                }
        //            }
        //            else {
        //                for (var i = 0; i < monthlist.length; i++) {
        //                    var hdnvalue = $("#MainContent_grdMonthlySales_txtValue" + monthlist[i] + "_" + i + "").val();
        //                    if ($('#MainContent_ValInUnit').is(':checked') == true) {
        //                        hdnvalue = hdnvalue;
        //                    }
        //                    if ($('#MainContent_ValInThsnd').is(':checked') == true) {
        //                        hdnvalue = parseFloat((hdnvalue) * 1000).toFixed(2);
        //                    }
        //                    if ($('#MainContent_ValInLakh').is(':checked') == true) {
        //                        hdnvalue = parseFloat((hdnvalue) * 100000).toFixed(2);
        //                    }
        //                    cumulateddata = parseFloat(cumulateddata) + parseFloat(hdnvalue);
        //                    cumulateddata = parseFloat((cumulateddata).toFixed(2));
        //                }
        //                if ($('#MainContent_ValInUnit').is(':checked') == true) {

        //                    askingRate1 = askingRate1 * monthlist.length;
        //                }
        //                if ($('#MainContent_ValInThsnd').is(':checked') == true) {
        //                    askingRate1 = parseFloat((askingRate1 * monthlist.length) * 1000).toFixed(2);
        //                }
        //                if ($('#MainContent_ValInLakh').is(':checked') == true) {
        //                    askingRate1 = parseFloat((askingRate1 * monthlist.length) * 100000).toFixed(2);
        //                }

        //                var completionrate1 = parseFloat((((cumulateddata - askingRate1) / askingRate1) * 100).toFixed(2));
        //                if (completionrate1 < 0) {
        //                    if (completionrate1 == -100) {
        //                        $("#MainContent_grdMonthlySales_lblCompletionRate_" + i + "").text("0%");
        //                    }
        //                    else {
        //                        $("#MainContent_grdMonthlySales_lblCompletionRate_" + i + "").text(completionrate1 + "%");
        //                    }
        //                }
        //                else {
        //                    $("#MainContent_grdMonthlySales_lblCompletionRate_" + i + "").text("+" + completionrate1 + "%");
        //                }
        //            }
        //        }
        //        else {
        //            $("#MainContent_grdMonthlySales_lblCompletionRate_" + i + "").text("0%");
        //        }
        //    }

        //}



        function selectAll() {
            debugger;
            var cumulateddata = 0;
            var askingRate = 0;
            var customerArray = [];
            var oTable = $("#MainContent_grdMonthlySales").dataTable();
            $('td > span', oTable.fnGetNodes()).each(function () {
                id = $(this).attr('id');
                if (id.includes("MainContent_grdMonthlySales_lblCustomerNumber")) {
                    customerArray.push($(this).text());
                } else {

                }
            });
            var totaltrCount = customerArray.length;
            var trCount = totaltrCount;


            for (var i = 0; i < trCount; i++) {
                var askrate = "MainContent_grdMonthlySales_lblAsking_Rate_" + i;
                var askingRate = $("#" + askrate).text();
                if (askingRate == "") {
                    askingRate = "0";
                }
                var asr = askingRate;
                var askingRate1 = askingRate;
                var monthlist = $("#MainContent_MonthList").val();
                
                if (asr != "0") {
                    
                    if (monthlist.length == 1) {
                        var currentmonthvalue = $("#MainContent_grdMonthlySales_txtValue" + monthlist + "_" + i).val();
                        if (currentmonthvalue == "") {
                            currentmonthvalue = "0.00";
                        }
                      
                        if ($('#MainContent_ValInUnit').is(':checked') == true) {
                            currentmonthvalue = currentmonthvalue;
                            askingRate = askingRate;
                        }
                        if ($('#MainContent_ValInThsnd').is(':checked') == true) {
                            currentmonthvalue = parseFloat((currentmonthvalue) * 1000).toFixed(2);
                            askingRate = parseFloat((askingRate) * 1000).toFixed(2);

                        }
                        if ($('#MainContent_ValInLakh').is(':checked') == true) {
                            currentmonthvalue = parseFloat((currentmonthvalue) * 100000).toFixed(2);
                            askingRate = parseFloat((askingRate) * 100000).toFixed(2);

                        }
                        var completionrate = parseFloat((((currentmonthvalue - askingRate) / askingRate) * 100).toFixed(2));
                        if (completionrate < 0) {
                            if (completionrate == -100) {
                                $("#MainContent_grdMonthlySales_lblCompletionRate_" + i).text("0%");
                            }
                            else {
                                $("#MainContent_grdMonthlySales_lblCompletionRate_" + i).text(completionrate + "%");
                            }
                        }
                        else {
                            $("#MainContent_grdMonthlySales_lblCompletionRate_" + i).text("+" + completionrate + "%");
                        }
                    }
                    else {
                        for (var j = 0; j < monthlist.length; j++) {
                            var hdnvalue = $("#MainContent_grdMonthlySales_txtValue" + monthlist[j] + "_" + i).val();
                            if ($('#MainContent_ValInUnit').is(':checked') == true) {
                                hdnvalue = hdnvalue;
                            }
                            if ($('#MainContent_ValInThsnd').is(':checked') == true) {
                                hdnvalue = parseFloat((hdnvalue) * 1000).toFixed(2);
                            }
                            if ($('#MainContent_ValInLakh').is(':checked') == true) {
                                hdnvalue = parseFloat((hdnvalue) * 100000).toFixed(2);
                            }
                            cumulateddata = parseFloat(cumulateddata) + parseFloat(hdnvalue);
                            cumulateddata = parseFloat((cumulateddata).toFixed(2));
                        }
                        if ($('#MainContent_ValInUnit').is(':checked') == true) {

                            askingRate1 = askingRate1 * monthlist.length;
                        }
                        if ($('#MainContent_ValInThsnd').is(':checked') == true) {
                            askingRate1 = parseFloat((askingRate1 * monthlist.length) * 1000).toFixed(2);
                        }
                        if ($('#MainContent_ValInLakh').is(':checked') == true) {
                            askingRate1 = parseFloat((askingRate1 * monthlist.length) * 100000).toFixed(2);
                        }

                        var completionrate1 = parseFloat((((cumulateddata - askingRate1) / askingRate1) * 100).toFixed(2));
                        if (completionrate1 < 0) {
                            if (completionrate1 == -100) {
                                $("#MainContent_grdMonthlySales_lblCompletionRate_" + i).text("0%");
                            }
                            else {
                                $("#MainContent_grdMonthlySales_lblCompletionRate_" + i).text(completionrate1 + "%");
                            }
                        }
                        else {
                            $("#MainContent_grdMonthlySales_lblCompletionRate_" + i).text("+" + completionrate1 + "%");
                        }
                    }
                }
                else {
                    $("#MainContent_grdMonthlySales_lblCompletionRate_" + i).text("0%");
                }
            }

        }


        

        function calcompletionRate(evt, obj) {
            var id = obj;
            var cumulateddata = 0;

            var idarry = id.split('_');
            idarry = idarry[idarry.length - 1];
            var askingRate = $("#MainContent_grdMonthlySales_lblAsking_Rate_" + idarry).text();
            var asr = askingRate;
            var monthlist = $("#MainContent_MonthList").val(); 
            var currentmonthvalue = $("#" + id).val();
            var totalaskingrate = $("#MainContent_grdMonthlySales_askingrate").text();
            //$("#" + id).val(addCommas(currentmonthvalue));
            if (currentmonthvalue == "") {
                currentmonthvalue = "0.00";
            }
            $("#" + id).val(parseFloat(currentmonthvalue).toFixed(2));
            var askingRate1 = askingRate;
            var totalaskingrate1 = totalaskingrate;
            if ($('#MainContent_ValInUnit').is(':checked') == true) {
                currentmonthvalue = currentmonthvalue;
                askingRate = askingRate;
                totalaskingrate = totalaskingrate;
            }
            if ($('#MainContent_ValInThsnd').is(':checked') == true) {
                //currentmonthvalue = currentmonthvalue + "000";
                //askingRate = askingRate + "000";
                currentmonthvalue = parseFloat((currentmonthvalue) * 1000).toFixed(2);
                askingRate = parseFloat((askingRate) * 1000).toFixed(2);
                totalaskingrate = parseFloat((totalaskingrate) * 1000).toFixed(2);

            }
            if ($('#MainContent_ValInLakh').is(':checked') == true) {
                //currentmonthvalue = currentmonthvalue + "00000";
                //askingRate = askingRate + "00000";
                currentmonthvalue = parseFloat((currentmonthvalue) * 100000).toFixed(2);
                askingRate = parseFloat((askingRate) * 100000).toFixed(2);
                totalaskingrate = parseFloat((totalaskingrate) * 100000).toFixed(2);
            }

            if (asr != "0") {
                var completionrate = parseFloat((((currentmonthvalue - askingRate) / askingRate) * 100).toFixed(2));
                if (monthlist.length == 1) {
                    if (completionrate < 0) {
                        $("#MainContent_grdMonthlySales_lblCompletionRate_" + idarry).text(completionrate + "%");
                    }
                    else {
                        $("#MainContent_grdMonthlySales_lblCompletionRate_" + idarry).text("+" + completionrate + "%");
                    }
                }
                else {
                    for (var i = 0; i < monthlist.length; i++) {
                        var hdnvalue = $("#MainContent_grdMonthlySales_txtValue" + monthlist[i] + "_" + idarry).val();
                        if ($('#MainContent_ValInUnit').is(':checked') == true) {
                            hdnvalue = hdnvalue;
                        }
                        if ($('#MainContent_ValInThsnd').is(':checked') == true) {
                            hdnvalue = parseFloat((hdnvalue) * 1000).toFixed(2);
                        }
                        if ($('#MainContent_ValInLakh').is(':checked') == true) {
                            hdnvalue = parseFloat((hdnvalue) * 100000).toFixed(2);
                        }
                        cumulateddata = parseFloat(cumulateddata) + parseFloat(hdnvalue);
                        cumulateddata = parseFloat(cumulateddata).toFixed(2);
                    }
                    if ($('#MainContent_ValInUnit').is(':checked') == true) {

                        askingRate1 = askingRate1 * monthlist.length;
                        totalaskingrate1 = totalaskingrate1 * monthlist.length;
                    }
                    if ($('#MainContent_ValInThsnd').is(':checked') == true) {
                        askingRate1 = parseFloat(askingRate1 * monthlist.length) * 1000;
                        totalaskingrate1 = parseFloat(totalaskingrate1 * monthlist.length) *1000;
                    }
                    if ($('#MainContent_ValInLakh').is(':checked') == true) {
                        askingRate1 = parseFloat(askingRate1 * monthlist.length) * 100000;
                        totalaskingrate1 = parseFloat(totalaskingrate1 * monthlist.length) * 100000;
                    }

                    var completionrate1 = parseFloat((((cumulateddata - askingRate1) / askingRate1) * 100).toFixed(2));
                    if (completionrate1 < 0) {
                        $("#MainContent_grdMonthlySales_lblCompletionRate_" + idarry).text(completionrate1 + "%");
                    }
                    else {
                        $("#MainContent_grdMonthlySales_lblCompletionRate_" + idarry).text("+" + completionrate1 + "%");
                    }
                }
            }
            else {
                $("#MainContent_grdMonthlySales_lblCompletionRate_" + idarry).text("0%");
            }


            var calculated_total_sum = 0;
            var idarry = id.split('_');
            var idarry1 = idarry[idarry.length - 2];
            var totalidarray = idarry1.replace("txtValue", "txttotalValue");
            var totalid = "MainContent_grdMonthlySales_" + totalidarray;
            var oTable = $("#MainContent_grdMonthlySales").dataTable();
            $('input[type="text"]', oTable.fnGetNodes()).each(function () {
                id1 = $(this).attr('id');
                if (id1.includes("MainContent_grdMonthlySales_" + idarry1)) {

                    var value = $(this).val();
                    calculated_total_sum += parseFloat(value);
                }
                else {

                }
            });
            $('#MainContent_grdMonthlySales tfoot').remove();
            $("#" + totalid).text(parseFloat(calculated_total_sum).toFixed(2));
            
           
            if (monthlist.length == 1) {
                var totalmonthvalue = $("#MainContent_grdMonthlySales_txttotalValue" + monthlist[0]).text();
                if ($('#MainContent_ValInUnit').is(':checked') == true) {
                    totalmonthvalue = totalmonthvalue;
                }
                if ($('#MainContent_ValInThsnd').is(':checked') == true) {
                    totalmonthvalue = parseFloat((totalmonthvalue) * 1000).toFixed(2);
                }
                if ($('#MainContent_ValInLakh').is(':checked') == true) {
                    totalmonthvalue = parseFloat((totalmonthvalue) * 100000).toFixed(2);
                }
                var totalcompletionrate = parseFloat((((totalmonthvalue - totalaskingrate) / totalaskingrate) * 100).toFixed(2));
                if (totalcompletionrate < 0) {
                    $("#MainContent_grdMonthlySales_lblCompletionRate_" + idarry).text(totalcompletionrate + "%");
                }
                else {
                    $("#MainContent_grdMonthlySales_lblCompletionRate_" + idarry).text("+" + totalcompletionrate + "%");
                }
            }
            else {
                var cumulateddata1 = 0;
                for (var i = 0; i < monthlist.length; i++) {
                    var hdnvalue1 = $("#MainContent_grdMonthlySales_txttotalValue" + monthlist[i]).text();
                    if ($('#MainContent_ValInUnit').is(':checked') == true) {
                        hdnvalue1 = hdnvalue1;
                    }
                    if ($('#MainContent_ValInThsnd').is(':checked') == true) {
                        hdnvalue1 = parseFloat((hdnvalue1) * 1000).toFixed(2);
                    }
                    if ($('#MainContent_ValInLakh').is(':checked') == true) {
                        hdnvalue1 = parseFloat((hdnvalue1) * 100000).toFixed(2);
                    }
                    cumulateddata1 = parseFloat(cumulateddata1) + parseFloat(hdnvalue1);
                    cumulateddata1 = parseFloat(cumulateddata1).toFixed(2);
                }
                var totalcompletionrate1 = parseFloat((((cumulateddata1 - totalaskingrate1) / totalaskingrate1) * 100).toFixed(2));
                if (totalcompletionrate1 < 0) {
                    $("#MainContent_grdMonthlySales_percentage").text(totalcompletionrate1 + "%");
                }
                else {
                    $("#MainContent_grdMonthlySales_percentage").text("+" + totalcompletionrate1 + "%");
                }
            }

            //var calculated_total_sum = 0;
            //var idarry = id.split('_');
            //var idarry1 = idarry[idarry.length - 2];
            //var customerArray = [];
            //var oTable = $("#MainContent_grdMonthlySales").dataTable();
            //$('td > span', oTable.fnGetNodes()).each(function () {
            //    id = $(this).attr('id');
            //    if (id.includes("MainContent_grdMonthlySales_lblCustomerNumber")) {
            //        customerArray.push($(this).text());
            //    } else {

            //    }
            //});
            //var totaltrCount = customerArray.length;
            //var totalidarray = idarry1.replace("txtValue", "txttotalValue");
            //var totalid = "MainContent_grdMonthlySales_" + totalidarray;
            //var totalval = $("#" + totalid).text();
            //for (var j = 0; j < totaltrCount; j++) {
            //    var txtvalue = $("#MainContent_grdMonthlySales_" + idarry1 + "_" + j).val();
            //    if (txtvalue == "" || txtvalue == undefined)
            //    {
            //        txtvalue = totalval;
            //    }
            //    calculated_total_sum += parseFloat(txtvalue);
            //}
            //$('#MainContent_grdMonthlySales tfoot').remove();
            //$("#" + totalid).text(parseFloat(calculated_total_sum).toFixed(2));
          


            
        }

        function addCommas(x) {
            return x.replace(x,x+".00");
        }


        function bindGridView() {
            var RoleID = '<%=Session["RoleId"].ToString()%>';
            //var head_content = $('#MainContent_grdviewAllValues tr:first').html();
            //$('#MainContent_grdviewAllValues').prepend('<thead></thead>')
            //$('#MainContent_grdviewAllValues thead').html('<tr>' + head_content + '</tr>');
            //$('#MainContent_grdviewAllValues tbody tr:first').hide();
            //$('#MainContent_grdviewAllValues').DataTable(
            //    {
            //        "info": false,
            //        "order": [],
            //        "columnDefs": [{ orderable: false, targets: [0] }]
            //    });
            var head_content = $('#MainContent_grdMonthlySales tr:first').html();
            $('#MainContent_grdMonthlySales').prepend('<thead></thead>')
            $('#MainContent_grdMonthlySales thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdMonthlySales tbody tr:first').hide();
            var foot_content = $('#MainContent_grdMonthlySales tr:last').html();
            $('#MainContent_grdMonthlySales').prepend('<tfoot></tfoot>')
            $('#MainContent_grdMonthlySales tfoot').html('<tr>' + foot_content + '</tr>');
            $('#MainContent_grdMonthlySales tbody tr:last').remove();
             //$('div.dataTables_scrollFoot tfoot').css({ display: 'none' });

            var table = $('#MainContent_grdMonthlySales').DataTable({
                scrollY: "300px",
                scrollX: true,
                scrollCollapse: true,
                columnDefs: [
                    //{ width: "100px", targets: 0 },
                    //{ width: "100px", targets: 1 },
                    //{ width: "100px", targets: 2 },
                    //{ width: "100px", targets: 3 },
                    //{ width: "150px", targets: 4 },
                    //{ width: "100px", targets: 5 },
                    //{ width: "100px", targets: 6 },
                    //{ width: "100px", targets: 7 },
                    //{ width: "100px", targets: 8 },
                    //{ width: "100px", targets: 9 },
                    //{ width: "100px", targets: 10 },
                    //{ width: "100px", targets: 11 },
                    //{ width: "100px", targets: 12 }

                    { width: "100px", targets: 0 },
                    { width: "50px", targets: 1 },
                    { width: "150px", targets: 2 },
                    { width: "60px", targets: 3 },
                    { width: "150px", targets: 4 },
                    { width: "50px", targets: 5 },
                    { width: "50px", targets: 6 },
                    { width: "50px", targets: 7 },
                    { width: "50px", targets: 8 },
                    { width: "50px", targets: 9 },
                    { width: "50px", targets: 10 },
                    { width: "70px", targets: 11 },
                    { width: "70px", targets: 12 }


                ],
                //fixedColumns: {
                //    leftColumns: 5
                //} 

            });



            $('#MainContent_grdMonthlySales').on('draw.dt', function () {
                selectAll();
                var monthlist = $("#MainContent_MonthList").val();
                var now = new Date();
                var thisMonth = [now.getMonth()];
                for (var j = 0; j < monthlist.length; j++) {
                    var month = monthlist[j];
                    var currentmonth = thisMonth[0];
                    if (month <= currentmonth) {
                        $(".month" + month + "").prop("disabled", true);
                        //$(".month").prop("disabled", true);
                    }
                }


            });

            var monthlist = $("#MainContent_MonthList").val();
            var now = new Date();
            var thisMonth = [now.getMonth()];
            for (var j = 0; j < monthlist.length; j++) {
                var month = monthlist[j];
                var currentmonth = thisMonth[0];
                if (month <= currentmonth) {
                    $(".month" + month + "").prop("disabled", true);
                    //$(".month").prop("disabled", true);
                }
            }






            var $RowSelected = $("#MainContent_grdMonthlySales");

            if ($RowSelected.length > 0) {

                selectAll();
                if (RoleID == "HO") {
                    $("#Approve").show();
                    $("#Reject").hide();
                    $("#Submit").hide();
                }
                if (RoleID == "BM" || RoleID == "TM") {
                    $("#Approve").show();
                    $("#Reject").hide();
                    $("#Submit").hide();
                }
                if (RoleID == "SE") {
                    $("#Approve").hide();
                    $("#Reject").hide();
                    $("#Submit").show();
                }

                if ($('#MainContent_ValInUnit').is(':checked') == true) {
                    $("#MainContent_errmsg").text("All Values are in units, please enter monthly target value in units.");
                }
                if ($('#MainContent_ValInThsnd').is(':checked') == true) {
                    $("#MainContent_errmsg").text("All Values are in thousands, please enter monthly target value in thousands.");
                }
                if ($('#MainContent_ValInLakh').is(':checked') == true) {
                    $("#MainContent_errmsg").text("All Values are in lakhs, please enter monthly target value in lakhs.");
                }
            }

            var monthlist = $("#MainContent_MonthList").val();
            var monthdata = $("#MainContent_MonthList option:selected").map(function () {
                return $(this).val();
            }).get().join(',');
            var arry = monthdata.split(',');
            //var prevmonth = 0;
            //var prevmonthcount = 0;
            //var enable = 0;
            var tablecontent = $("#MainContent_CustNameList").val();
            for (var i = 0; i < tablecontent.length; i++) {
                for (var j = 0; j < monthlist.length; j++) {
                    var prevmonth = 0;
                    var prevmonthcount = 0;
                    var enable = 0;
                    var month = monthlist[j];
                    var currentmonth = thisMonth[0];
                    var id = "#MainContent_grdMonthlySales_txtValue" + monthlist[j] + "_" + i + "";

                    if (RoleID == "SE") {
                        if ($(id).prop('disabled') == false) {
                           <%-- if (month <= currentmonth) {
                                prevmonth = 1;
                                prevmonthcount = prevmonthcount + 1;
                                $("#Submit").prop("disabled", true);
                                //$("#<%= msg.ClientID %>").html("");
                            }
                            else {--%>
                                enable = 1;
                                $("#Submit").prop("disabled", false);
                                //$("#<%= msg.ClientID %>").html("");
                           /* }*/
                        }
                        else {
                            if (arry.length == 12) {

                            }
                            else {
                                if (month <= currentmonth) {
                                    prevmonth = 1;
                                    prevmonthcount = prevmonthcount + 1;
                                    $("#Submit").prop("disabled", true);
                                }
                                
                            }
                        }
                        if (prevmonthcount == monthlist.length) {
                            $("#Submit").prop("disabled", true);
                            //$("#<%= msg.ClientID %>").html("");
                        }
                        if (enable == 1) {
                            $("#Submit").prop("disabled", false);
                            //$("#<%= msg.ClientID %>").html("");
                        }
                    }
                    if (RoleID == "BM" || RoleID == "TM") {
                        if ($(id).prop('disabled') == false) {
                           <%-- if (month <= currentmonth) {
                                prevmonth = 1;
                                prevmonthcount = prevmonthcount + 1;
                                $("#Submit").show();
                                $("#Approve").hide();
                                $("#Submit").prop("disabled", true);
                                //$("#<%= msg.ClientID %>").html("");
                            }
                            else {--%>
                                enable = 1;
                                $("#Submit").show();
                                $("#Approve").hide();
                                $("#Submit").prop("disabled", false);
                                //$("#<%= msg.ClientID %>").html("");
                            /*}*/

                        }
                        else {
                            if (arry.length == 12) {

                            }
                            else {
                                if (month <= currentmonth) {
                                    prevmonth = 1;
                                    prevmonthcount = prevmonthcount + 1;
                                    $("#Approve").prop("disabled", true);
                                }
                                
                            }
                        }
                        if (prevmonthcount == monthlist.length) {
                            $("#Submit").prop("disabled", true);
                            //$("#<%= msg.ClientID %>").html("");
                        }
                        if (enable == 1) {
                            $("#Submit").prop("disabled", false);
                            //$("#<%= msg.ClientID %>").html("");
                        }
                    }
                    if (RoleID == "HO") {

                        if ($(id).prop('disabled') == false) {
                            <%--if (month <= currentmonth) {
                                prevmonth = 1;
                                prevmonthcount = prevmonthcount + 1;
                                $("#Approve").prop("disabled", true);
                                //$("#<%= msg.ClientID %>").html("");
                            }
                            else {--%>
                                enable = 1;
                                var message = $("#<%= msg.ClientID %>").text();
                                if (message == "") {
                                    $("#Approve").prop("disabled", false);
                                    //$("#<%= msg.ClientID %>").html("");
                                }

                            /*}*/
                        }
                        else {
                            if (arry.length == 12) {

                            }
                            else {
                                if (month <= currentmonth) {
                                    prevmonth = 1;
                                    prevmonthcount = prevmonthcount + 1;
                                    $("#Approve").prop("disabled", true);
                                }
                               
                            }
                        }
                        if (prevmonthcount == monthlist.length) {
                            $("#Submit").prop("disabled", true);
                            //$("#<%= msg.ClientID %>").html("");
                        }
                        if (enable == 1) {
                            $("#Submit").prop("disabled", false);
                            //$("#<%= msg.ClientID %>").html("");
                        }
                    }

                }
            }
            var message = $("#<%= msg.ClientID %>").text();
            if (RoleID == "SE") {
                if (message == "Sales are already sent for Approval") {
                    $("#Submit").prop("disabled", true);
                }
                if (message == "Sales are already approved") {
                    $("#Submit").prop("disabled", true);
                }
                if ((message != "Sales are already sent for Approval") && (message != "Sales are already approved") && prevmonth == 0) {
                    $("#Submit").prop("disabled", false);
                    $("#<%= msg.ClientID %>").html("");
                    //$("#msg").text("Sales are already sent for Approval");
                }
            }

            if (RoleID == "BM" || RoleID == "TM") {
                if (message == "Sales are already sent for Approval") {
                    $("#Approve").prop("disabled", true);
                }
                if (message == "Sales are already approved") {
                    $("#Approve").prop("disabled", true);
                }
                if ((message != "Sales are already sent for Approval") && (message != "Sales are already approved") && prevmonth == 0) {
                    $("#Approve").prop("disabled", false);
                    $("#<%= msg.ClientID %>").html("");
                }
            }

            if (RoleID == "HO") {
                if (message === "Target Values are under process") {
                    $("#Approve").prop("disabled", true);
                }
                if (message === "Sales are already approved") {
                    $("#Approve").prop("disabled", true);
                }
                if (message === "Some of the Values are not submitted yet And New customer details are displaying here.") {
                    $("#Approve").prop("disabled", false);
                }
                if (message === "Submitted Values And New customer details are displaying here.") {
                    $("#Approve").prop("disabled", false);
                }
                if (message.includes("are not approved by BM.")) {
                    $("#Approve").prop("disabled", false);
                }
                if (message.includes("Red colored customer numbers are still under process.")) {
                    $("#Approve").prop("disabled", false);
                }
                if ((message != "Target Values are under process") && (message != "Sales are already approved") && (message != "Some of the Values are not submitted yet And New customer details are displaying here.") && (message != "Submitted Values And New customer details are displaying here.") && (!message.includes("Red colored customer numbers are still under process.")) && prevmonth == 0) {
                    $("#Approve").prop("disabled", false);
                    $("#<%= msg.ClientID %>").html("");
                }
            }

        }
        function triggerPostGridLodedActions() {
            debugger;
            bindGridView();

            var RoleID = '<%=Session["RoleId"].ToString()%>';
            console.log("Roleid=" + RoleID);
            $(<%=MonthList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
            if (RoleID == "HO" || RoleID == "TM") {

                $(<%=BranchList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });

                $(<%=SalesEngList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });

                $(<%=CustNameList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });


                $(<%=CustNumList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });

            }
            if (RoleID == "BM") {

                $(<%=SalesEngList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
                $(<%=CustNameList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
                $(<%=CustNumList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
            }

            if (RoleID == "SE") {
                $(<%=CustNameList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
                $(<%=CustNumList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
            }
            else {
            }

            //$('#Imgcollapsedropdwns').unbind('click').bind('click', function (e) {
            //    var attr = $('#Imgcollapsedropdwns').attr('src');
            //    $("#MainContent_reportdrpdwns").slideToggle();
            //    if (attr == "images/up_arrow.png") {
            //        $("#Imgcollapsedropdwns").attr("src", "images/down_arrow.png");
            //    } else {
            //        $("#Imgcollapsedropdwns").attr("src", "images/up_arrow.png");
            //    }
            //});

            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#MainContent_reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });

        }



        var validate = function (e) {
            var t = e.value;
            e.value = (t.indexOf(".") >= 0) ? (t.substr(0, t.indexOf(".")) + t.substr(t.indexOf("."), 3)) : t;
        }


        function Approve_Click() {
            Submit_Click();
        }


        function Submit_Click() {

            var customerArray = [];
            var id = "";
            var monthvalueArray = [];
            var id1 = "";
            var id2 = "";
            var valuearray = [];
            var montharry = new Array();
            var count = 0;
            var oTable = $("#MainContent_grdMonthlySales").dataTable();


            $('td > span', oTable.fnGetNodes()).each(function () {
                id = $(this).attr('id');
                if (id.includes("MainContent_grdMonthlySales_lblCustomerNumber")) {
                    customerArray.push($(this).text());
                } else {

                }
            });
            $('input[type="text"]', oTable.fnGetNodes()).each(function () {
                id1 = $(this).attr('id');
                var valdata = "";
                if (id1.includes("MainContent_grdMonthlySales_txtValue")) {
                    if ($('#MainContent_ValInUnit').is(':checked') == true) {
                        valdata = $(this).val();
                        monthvalueArray.push(valdata);

                    }
                    if ($('#MainContent_ValInThsnd').is(':checked') == true) {
                        //valdata = $(this).val() + "000";
                        var value = $(this).val();
                        //valdata = value * 1000;
                        valdata = parseFloat((value) * 1000).toFixed(2);
                        monthvalueArray.push(valdata);

                    }
                    if ($('#MainContent_ValInLakh').is(':checked') == true) {
                        //valdata = $(this).val() + "00000";
                        var value = $(this).val();
                        //valdata = value * 100000;
                        valdata = parseFloat((value) * 100000).toFixed(2);
                        monthvalueArray.push(valdata);

                    }

                } else {

                }

            });
            var monthlist = $("#MainContent_MonthList").val();
            var custcount = customerArray.length;
            var myarrycount = monthvalueArray.length;
            var total = parseInt(myarrycount / custcount);
            for (var i = 0; i < myarrycount; i++) {
                id2 += monthvalueArray[i] + ",";
                count = count + 1;
                if (count == total) {
                    id2 = id2.substring(0, id2.length - 1);
                    valuearray.push(id2);
                    id2 = "";
                    count = 0;
                }
            }

            for (var i = 0; i < customerArray.length; i++) {

                if (valuearray[i].includes(',')) {
                    var idval = valuearray[i].split(',');
                    for (var k = 0; k < idval.length; k++) {
                        var datavalue = "";
                        var custn = customerArray[i];
                        datavalue += custn + ",";
                        datavalue += monthlist[k] + ",";
                        //var quantity = $("#MainContent_Quantity_" + monthlist[j] + "_" + tablecontent[i] + "").val();
                        //if (quantity == "" || quantity == null)
                        quantity = 0;
                        datavalue += quantity + ",";
                        var value = idval[k];
                        if (value == "" || value == null)
                            value = 0;
                        datavalue += value + ",";
                        datavalue = datavalue.substring(0, datavalue.length - 1);
                        montharry.push(datavalue);
                    }
                }
                else {
                    var datavalue = "";
                    var custn = customerArray[i];
                    datavalue += custn + ",";
                    datavalue += monthlist[0] + ",";
                    //var quantity = $("#MainContent_Quantity_" + monthlist[j] + "_" + tablecontent[i] + "").val();
                    //if (quantity == "" || quantity == null)
                    quantity = 0;
                    datavalue += quantity + ",";
                    var value = valuearray[i];
                    if (value == "" || value == null)
                        value = 0;
                    datavalue += value + ",";
                    datavalue = datavalue.substring(0, datavalue.length - 1);
                    montharry.push(datavalue);
                }

            }

            $.ajax({
                url: 'MonthlyTarget.aspx/Submit_Click',
                method: 'post',
                datatype: 'json',
                //data: "{'custnum':'" + JSON.stringify(custnumaarry) + "','quantity':'" + JSON.stringify(quantityarry) + "','value':'" + JSON.stringify(valuearry) + "','month':'" + JSON.stringify(montharry) + "'}",
                data: "{'month':'" + JSON.stringify(montharry) + "'}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data != "") {
                        alert("Target Values are added successfully");
                        //window.location.reload();
                        window.location.href = "MonthlyTarget.aspx?MT";
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });

        }

        function isNumberKey(evt, obj) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                    // Number of digits right of decimal point.
                    (match[1] ? match[1].length : 0)
                    // Adjust for scientific notation.
                    - (match[2] ? +match[2] : 0));
                if (decCount >= 2) return false;
                if (charCode == 46) return false;
                //alert("It should not accept decimal numbers please remove dot operator");
                //return false;
            }
            else {
                if (value.length > 10) {
                    if (charCode == 46) return true;
                    else return false;
                }
            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>

    <style type="text/css">
        table.dataTable, tfoot {
            /*     background:azure;*/
            background: #006780;
            border: solid 1px gainsboro !important;
        }

            table.dataTable thead td {
                border-bottom: 1px solid aqua !important;
            }

            table.dataTable tbody td {
                border-bottom: solid 1px aqua !important;
            }

            table.dataTable.compact thead th, table.dataTable.compact thead td {
                color: white !important;
            }

        .footer {
            text-align: right;
            background: #006780;
            border: solid 1px gainsboro !important;
            color: white;
        }

        .footer1 {
            text-align: left;
            background: #006780;
            border: solid 1px gainsboro !important;
            color: white;
        }

        th, td {
            padding: 5px;
            text-align: left;
        }

        input[type="text"] {
            width: 100%;
        }

        .dataTables_scroll input {
            height: 27px;
            padding: 0 5px !important
        }

        .SumoSelect p {
            margin: 0;
            width: 200px;
        }

        .SumoSelect {
            width: 252px;
        }

        .SelectBox {
            padding: 5px 0px;
            /* margin-bottom:5px;*/
        }

        /* Filtering style */

        .SumoSelect .hidden {
            display: none;
        }

        .SumoSelect .search-txt {
            display: none;
            outline: none;
        }

        .SumoSelect .no-match {
            display: none;
            padding: 6px;
        }

        .SumoSelect.open .search-txt {
            display: inline-block;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            margin: 0;
            padding: 5px 8px;
            border: none;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            border-radius: 5px;
        }

        .SumoSelect.open > .search > span,
        .SumoSelect.open > .search > label {
            visibility: hidden;
        }


        /*this is applied on that hidden select. DO NOT USE display:none; or visiblity:hidden; and Do not override any of these properties. */

        .SelectClass,
        .SumoUnder {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            border: none;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
            filter: alpha(opacity=0);
            -moz-opacity: 0;
            -khtml-opacity: 0;
            opacity: 0;
        }

        .SelectClass {
            z-index: 1;
        }

        .SumoSelect > .optWrapper > .options li.opt label,
        .SumoSelect > .CaptionCont,
        .SumoSelect .select-all > label {
            user-select: none;
            -o-user-select: none;
            -moz-user-select: none;
            -khtml-user-select: none;
            -webkit-user-select: none;
            overflow-wrap: normal;
        }

        .SumoSelect {
            display: inline-block;
            position: relative;
            outline: none;
        }

            .SumoSelect:focus > .CaptionCont,
            .SumoSelect:hover > .CaptionCont,
            .SumoSelect.open > .CaptionCont {
                box-shadow: 0 0 2px #7799D0;
                border-color: #7799D0;
            }

            .SumoSelect > .CaptionCont {
                position: relative;
                border: 1px solid #A4A4A4;
                min-height: 14px;
                background-color: #fff;
                border-radius: 2px;
                margin: 0;
            }

                .SumoSelect > .CaptionCont > span {
                    display: block;
                    padding-right: 30px;
                    text-overflow: ellipsis;
                    white-space: nowrap;
                    overflow: hidden;
                    cursor: default;
                    margin-left: 5px;
                }


                    /*placeholder style*/

                    .SumoSelect > .CaptionCont > span.placeholder {
                        color: #ccc;
                        font-style: italic;
                    }

                .SumoSelect > .CaptionCont > label {
                    position: absolute;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    width: 30px;
                }

                    .SumoSelect > .CaptionCont > label > i {
                        background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAANCAYAAABy6+R8AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wMdBhAJ/fwnjwAAAGFJREFUKM9jYBh+gBFKuzEwMKQwMDB8xaOWlYGB4T4DA0MrsuapDAwM//HgNwwMDDbYTJuGQ8MHBgYGJ1xOYGNgYJiBpuEpAwODHSF/siDZ+ISBgcGClEDqZ2Bg8B6CkQsAPRga0cpRtDEAAAAASUVORK5CYII=');
                        background-position: center center;
                        width: 16px;
                        height: 16px;
                        display: block;
                        position: absolute;
                        top: 0;
                        left: 0;
                        right: 0;
                        bottom: 0;
                        margin: auto;
                        background-repeat: no-repeat;
                        opacity: 0.8;
                    }

            .SumoSelect > .optWrapper {
                display: none;
                z-index: 1000;
                top: 30px;
                width: 100%;
                position: absolute;
                left: 0;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                background: #fff;
                border: 1px solid #ddd;
                box-shadow: 2px 3px 3px rgba(0, 0, 0, 0.11);
                border-radius: 3px;
                overflow: visible;
                padding-bottom: 0px;
                padding-right: 0px;
                padding-left: 0px;
                padding-top: 0px;
            }

            .SumoSelect.open > .optWrapper {
                top: 35px;
                display: block;
            }

                .SumoSelect.open > .optWrapper.up {
                    top: auto;
                    bottom: 100%;
                    margin-bottom: 5px;
                }

            .SumoSelect > .optWrapper ul {
                list-style: none;
                display: block;
                padding: 0;
                margin: 0;
                overflow: auto;
            }

            .SumoSelect > .optWrapper > .options {
                border-radius: 2px;
                position: relative;
                /*Set the height of pop up here (only for desktop mode)*/
                max-height: 250px;
                /*height*/
            }

                .SumoSelect > .optWrapper > .options li.group.disabled > label {
                    opacity: 0.5;
                }

                .SumoSelect > .optWrapper > .options li ul li.opt {
                    padding-left: 22px;
                }

            .SumoSelect > .optWrapper.multiple > .options li ul li.opt {
                padding-left: 50px;
            }

            .SumoSelect > .optWrapper.isFloating > .options {
                max-height: 100%;
                box-shadow: 0 0 100px #595959;
            }

            .SumoSelect > .optWrapper > .options li.opt {
                padding: 6px 6px;
                position: relative;
                border-bottom: 1px solid #f5f5f5;
            }

            .SumoSelect > .optWrapper > .options > li.opt:first-child {
                border-radius: 2px 2px 0 0;
            }

            .SumoSelect > .optWrapper > .options > li.opt:last-child {
                border-radius: 0 0 2px 2px;
                border-bottom: none;
            }

            .SumoSelect > .optWrapper > .options li.opt:hover {
                background-color: #E4E4E4;
            }

            .SumoSelect > .optWrapper > .options li.opt.sel {
                background-color: #a1c0e4;
                border-bottom: 1px solid #a1c0e4;
            }

            .SumoSelect > .optWrapper > .options li label {
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                display: block;
                cursor: pointer;
            }

            .SumoSelect > .optWrapper > .options li span {
                display: none;
            }

            .SumoSelect > .optWrapper > .options li.group > label {
                cursor: default;
                padding: 8px 6px;
                font-weight: bold;
            }


            /*Floating styles*/

            .SumoSelect > .optWrapper.isFloating {
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
                width: 90%;
                bottom: 0;
                margin: auto;
                max-height: 90%;
            }


            /*disabled state*/

            .SumoSelect > .optWrapper > .options li.opt.disabled {
                background-color: inherit;
                pointer-events: none;
            }

                .SumoSelect > .optWrapper > .options li.opt.disabled * {
                    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
                    /* IE 5-7 */
                    filter: alpha(opacity=50);
                    /* Netscape */
                    -moz-opacity: 0.5;
                    /* Safari 1.x */
                    -khtml-opacity: 0.5;
                    /* Good browsers */
                    opacity: 0.5;
                }


            /*styling for multiple select*/

            .SumoSelect > .optWrapper.multiple > .options li.opt {
                padding-left: 35px;
                cursor: pointer;
            }

        .multiple {
            padding: 8px 10px;
            /*  height: 300px !important;*/
            font-size: 12px;
            border: 1px solid #dadada;
        }

        .SumoSelect > .optWrapper.multiple > .options li.opt span,
        .SumoSelect .select-all > span {
            position: absolute;
            display: block;
            width: 30px;
            top: 0;
            bottom: 0;
            margin-top: 5px;
        }

            .SumoSelect > .optWrapper.multiple > .options li.opt span i,
            .SumoSelect .select-all > span i {
                position: absolute;
                margin: auto;
                left: 0;
                right: 0;
                top: 0;
                bottom: 0;
                width: 14px;
                height: 14px;
                border: 1px solid #AEAEAE;
                border-radius: 2px;
                box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.15);
                background-color: #fff;
            }

        .SumoSelect > .optWrapper > .MultiControls {
            display: inline;
            border-top: 1px solid #ddd;
            background-color: #fff;
            box-shadow: 0 0 2px rgba(0, 0, 0, 0.13);
            border-radius: 0 0 3px 3px;
        }

        .SumoSelect > .optWrapper.multiple.isFloating > .MultiControls {
            display: block;
            margin-top: 5px;
            position: absolute;
            bottom: 0;
            width: 100%;
        }


        .SumoSelect > .optWrapper.multiple.okCancelInMulti > .MultiControls {
            display: block;
        }

            .SumoSelect > .optWrapper.multiple.okCancelInMulti > .MultiControls > p {
                /* padding: 6px;*/
            }

        .SumoSelect > .optWrapper.multiple > .MultiControls > p {
            display: inline-block;
            cursor: pointer;
            /*padding: 12px;*/
            width: 50%;
            box-sizing: border-box;
            text-align: center;
        }

            .SumoSelect > .optWrapper.multiple > .MultiControls > p:hover {
                background-color: #f1f1f1;
                margin-bottom: 10px;
            }

            .SumoSelect > .optWrapper.multiple > .MultiControls > p.btnOk {
                border-right: 1px solid #DBDBDB;
                border-radius: 0 0 0 3px;
            }

            .SumoSelect > .optWrapper.multiple > .MultiControls > p.btnCancel {
                border-radius: 0 0 3px 0;
                /* margin-bottom:5px;*/
            }


        /*styling for select on popup mode*/

        .SumoSelect > .optWrapper.isFloating > .options li.opt {
            padding: 12px 6px;
        }


        /*styling for only multiple select on popup mode*/

        .SumoSelect > .optWrapper.multiple.isFloating > .options li.opt {
            padding-left: 35px;
        }

        .SumoSelect > .optWrapper.multiple.isFloating {
            /*padding-bottom: 43px;*/
        }

        .SumoSelect > .optWrapper.multiple > .options li.opt.selected span i,
        .SumoSelect .select-all.selected > span i,
        .SumoSelect .select-all.partial > span i {
            background-color: rgb(5, 130, 183);
            box-shadow: none;
            border-color: transparent;
            background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAGCAYAAAD+Bd/7AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNXG14zYAAABMSURBVAiZfc0xDkAAFIPhd2Kr1WRjcAExuIgzGUTIZ/AkImjSofnbNBAfHvzAHjOKNzhiQ42IDFXCDivaaxAJd0xYshT3QqBxqnxeHvhunpu23xnmAAAAAElFTkSuQmCC');
            background-repeat: no-repeat;
            background-position: center center;
            padding: 0px;
            margin-top: 0px;
        }


        /*disabled state*/

        .SumoSelect.disabled {
            opacity: 0.7;
            cursor: not-allowed;
        }

            .SumoSelect.disabled > .CaptionCont {
                border-color: #ccc;
                box-shadow: none;
            }


        /**Select all button**/

        .SumoSelect .select-all {
            border-radius: 3px 3px 0 0;
            position: relative;
            border-bottom: 1px solid #ddd;
            background-color: #fff;
            padding: 8px 0 3px 35px;
            height: 30px;
            cursor: pointer;
        }

            .SumoSelect .select-all > label,
            .SumoSelect .select-all > span i {
                cursor: pointer;
            }

            .SumoSelect .select-all.partial > span i {
                background-color: #ccc;
            }


        /*styling for optgroups*/

        .SumoSelect > .optWrapper > .options li.optGroup {
            padding-left: 5px;
            text-decoration: underline;
        }


        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            /*border: 1px solid #ddd;*/
            padding: 5px;
            text-align: center;
        }

        .height {
            text-space-collapse: collapse;
        }

        .HeadergridAll {
            background: #ebeef5;
            color: black;
            font-weight: 600;
            text-align: center !important;
            /*border-color:#ebeef5;*/
        }


        #AmchartQTY {
            width: 100%;
            height: 500px;
            /*  background-image: url('images/Back.png');*/
            background-repeat: no-repeat;
            background-size: cover;
        }

        .legend-title {
            font-family: Verdana;
            font-weight: bold;
            font-size: 15px;
            margin-left: 550px;
            margin-bottom: 5px;
        }

        #AmchartVAL {
            width: 100%;
            height: 500px;
            /*   background-image: url('images/Back.png');*/
            background-repeat: no-repeat;
            background-size: cover;
        }

        td {
            /*   border-color: #ebeef5;*/
            border-color: azure;
            background: #fff;
            text-align: left;
        }

        #MainContent_grdviewAllValues td:first-child {
            text-align: left;
        }

        #MainContent_grdviewAllQuantites td:first-child {
            text-align: left;
        }

        /*.HeadergridAll {
            background: #ebeef5;
            color: black;
            font-weight: 600;
            text-align: center !important;
        }*/

        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px !important;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

        .control {
            padding-top: 2px;
        }

        #amchartqtysave, #amchartvalsave, #amchartqtysavelak, #amchartvalsavelak {
            width: 100%;
            height: 300px;
            background-image: url('images/Back.png');
            background-repeat: no-repeat;
            background-size: cover;
        }

        .SelectBox {
            padding-top: 9px;
            width: 180px !important;
            border-radius: 4px !important;
        }


        .btn.green {
            margin-top: 22px;
        }

        .amcharts-chart-div a {
            display: none !important;
        }

        .GVFixedFooter {
            font-weight: bold;
            position: relative;
            bottom: expression(getScrollBottom(this.parentNode.parentNode.parentNode.parentNode));
        }

        .GVFixedHeader {
            background-color: blue;
            position: relative;
            top: expression(this.offsetParent.scrollTop);
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hdnSearch" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout ="360000" ScriptMode="Debug" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Monthly Target</a>
                    </li>
                    <li class="title_bedcrum" align="left" style="margin-left: 6%;">MONTHLY TARGET</li>

                    <div>
                        <ul style="float: right; list-style: none; margin-top: -4px; width: 300px; margin-right: -5px;" class="alert alert-danger fade in">
                            <li>
                                <span style="margin-right: 4px; vertical-align: text-bottom;">Val In Units</span>
                                <asp:RadioButton ID="ValInUnit" name="Unit" OnCheckedChanged="Unit_CheckedChanged" AutoPostBack="true" GroupName="byValueInradiobtn" runat="server" />

                                <span style="margin-right: -1px; margin-left: 5px; vertical-align: text-bottom;">Val In '000</span>
                                <asp:RadioButton ID="ValInThsnd" OnCheckedChanged="Thousand_CheckedChanged" AutoPostBack="true" GroupName="byValueInradiobtn" runat="server" />
                                <span style="margin-right: 0px; margin-left: 6px; vertical-align: text-bottom;">Val In Lakhs</span>
                                <asp:RadioButton ID="ValInLakh" OnCheckedChanged="Lakhs_CheckedChanged" AutoPostBack="true" Checked="true" GroupName="byValueInradiobtn" runat="server" />


                            </li>
                        </ul>
                    </div>

                </ul>
                <!-- End : Breadcrumbs -->
            </div>

            <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />

            </div>


            <div id="MainContent_reportdrpdwns" class="row filter_panel ">
                <div runat="server" id="cterDiv" visible="false">
                    <ul id="divCter" runat="server" class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>
                            <asp:RadioButton ID="rdBtnTaegutec" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" AutoPostBack="true" onChange="hideTable();" />
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" AutoPostBack="true" onChange="hideTable();" />
                        </li>
                    </ul>
                </div>

                <div class="col-md-2 control" runat="server" id="divBranch">
                    <label class="label">BRANCH</label>
                    <input type="text" id="BranchText" runat="server" visible="false" />
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="BranchList" SelectionMode="Multiple" OnSelectedIndexChanged="BranchList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>

                    <%--  <asp:DropDownList ID="ddlBranchList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged"
                                CssClass="form-control select2" Width="230px">
                            </asp:DropDownList>--%>
                </div>
                <input type="hidden" id="blist" runat="server" />
                <div class="col-md-2 control" runat="server" id="divSE">
                    <label class="label">SALES ENGINEER </label>
                    <input type="text" id="SaleText" runat="server" visible="false" />
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="SalesEngList" SelectionMode="Multiple" OnSelectedIndexChanged="SalesEngList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                    <input type="hidden" id="slist" runat="server" />
                    <%--<asp:DropDownList ID="ddlSalesEngineerList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSalesEngineerList_SelectedIndexChanged"
                                CssClass="form-control" Width="230px">
                                <asp:ListItem>--SELECT SALES ENGINEER--</asp:ListItem>
                            </asp:DropDownList>--%>
                </div>

                <div class="col-md-2 control">
                    <label class="label">CUSTOMER TYPE </label>
                    <asp:DropDownList ID="ddlcustomertype" runat="server" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlcustomertype_SelectedIndexChanged">
                        <asp:ListItem Text="ALL" Value="ALL" />
                        <asp:ListItem Text="CUSTOMER" Value="C" />
                        <asp:ListItem Text="CHANNEL PARTNER" Value="D" />
                    </asp:DropDownList>
                </div>
                <asp:Label ID="Sctype" runat="server"></asp:Label>

                <div class="col-md-2 control">
                    <label class="label">CUSTOMER NAME </label>
                    <%-- <asp:DropDownList ID="ddlCustomerList" runat="server"
                                CssClass="form-control" Width="230px">
                                <asp:ListItem>--SELECT CUSTOMER --</asp:ListItem>
                            </asp:DropDownList>--%>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="CustNameList" SelectionMode="Multiple" class="search-txt" OnSelectedIndexChanged="CustNameList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                    <input type="hidden" id="lcna" runat="server" />
                </div>

                <div class="col-md-2 control">
                    <label class="label ">CUSTOMER NUMBER</label>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="CustNumList" SelectionMode="Multiple" class="search-txt" OnSelectedIndexChanged="CustNumList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                </div>
                <div class="col-md-2 control">
                    <label class="label">MONTH </label>
                    <asp:ListBox runat="server" CssClass="control_dropdown month" ID="MonthList" SelectionMode="Multiple" class="search-txt" OnSelectedIndexChanged="MonthList_SelectedIndexChanged" AutoPostBack="false"></asp:ListBox>
                    <input type="hidden" id="Hidden1" runat="server" />
                </div>
                <input type="hidden" id="lcnu" runat="server" />

                <input type="hidden" runat="server" id="apl" />
                <div class="col-md-2 ">
                    <%-- <input type="button" id="reports" class="btn green" style="top: -5px !important;" onclick="reports_Click();" value="FILTER" />--%>

                    <label id="alertmsg" style="display: none; font-weight: bold; color: #0582b7;">Now click on Filter to view results</label>
                </div>
                <asp:Button ID="reports" runat="server" CssClass="btn green" OnClick="reports_Click" Style="top: -5px !important;" Text="FILTER" />
            </div>

            <br />
            <asp:Label ID="errmsg" runat="server" Style="font-weight: bold; color: red;"></asp:Label>
            <div id="test1" style="float: right">
                <asp:Label ID="msg" runat="server" Style="font-weight: bold; color: #0582b7;"></asp:Label>
                <%-- <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="btn green"  Style="top: -5px !important;" OnClick="Submit_Click"/>--%>
                <input type="button" id="Submit" class="btn green" style="top: -5px !important; display: none" onclick="Submit_Click();" value="Submit" />
                <input type="button" id="Approve" class="btn green" style="top: -5px !important; display: none" onclick="Approve_Click();" value="Approve" />
                <input type="button" id="Reject" class="btn green" style="top: -5px !important; display: none" onclick="Reject_Click();" value="Reject" />
                <%--<asp:Button ID="Horeport" runat="server" Text="Export Report" CausesValidation="true" CssClass="btn green" Visible="false" Style="top: -5px !important;" OnClick="download_Click" />--%>
                <%--<label id="msg" style="font-weight: bold; color: #0582b7;"></label>--%>
                <asp:Button ID="export" runat="server" CssClass="btn green" OnClick="export_Click1" Text="Export Report" Style="top: -5px !important;" Visible="False" />

            </div>
            <%--  <div id="div1" runat="server" style="width: 100%;overflow: auto;">
            </div>--%>
            <%--<asp:GridView ID="grdMonthlySales" CssClass="display compact tab" runat="server" AutoGenerateColumns="false"  OnRowDataBound="gvProduct_RowDataBound" ShowFooter="true" AlternatingRowStyle-BackColor="Wheat">--%>
            <asp:GridView ID="grdMonthlySales" CssClass="display compact tab" runat="server" AutoGenerateColumns="false" ShowFooter="true" AlternatingRowStyle-BackColor="Wheat">
                <FooterStyle CssClass="GVFixedFooter" />
                <Columns>
                    <asp:TemplateField HeaderText="Branch Name">
                        <ItemTemplate>
                            <asp:Label ID="lblbranchname" runat="server" Text='<%#Bind("region_description") %>'></asp:Label>
                        </ItemTemplate>
                       <%-- <FooterTemplate>
                        </FooterTemplate>--%>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cust Type">
                        <ItemTemplate>
                            <asp:Label ID="lblcusttype" runat="server" Text='<%#Bind("customer_type") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Engineer Name">
                        <ItemTemplate>
                            <asp:Label ID="lblEngNumber" runat="server" Text='<%#Bind("EngineerName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cust Number">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerNumber" runat="server" Text='<%#Bind("customer_number") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer Name">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerName" runat="server" Text='<%#Bind("customer_short_name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Budget Value">
                        <ItemTemplate>
                            <asp:Label ID="lblBudget_Value" runat="server" Text='<%#Bind("Budget_Value") %>' Style="float: right;"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblsales_current_year" runat="server" Text='<%#Bind("sales_current_year") %>' Style="float: right;"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asking Rate">
                        <ItemTemplate>
                            <asp:Label ID="lblAsking_Rate" runat="server" Text='<%#Bind("Asking_Rate") %>' Style="float: right;"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblsales_value" runat="server" Text='<%#Bind("Sale_LM") %>' Style="float: right;"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Open Order">
                        <ItemTemplate>
                            <asp:Label ID="lblopenorder" runat="server" Text='<%#Bind("open_order") %>' Style="float: right;"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total Allocation">
                        <ItemTemplate>
                            <asp:Label ID="lblTotal_Allocation" runat="server" Text='<%#Bind("Total_Allocation") %>' Style="float: right;"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblsupply" runat="server" Text='<%#Bind("can_supply_now") %>' Style="float: right;"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblAllocation_CM" runat="server" Text='<%#Bind("Allocation_CM") %>' Style="float: right;"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="January" Visible="false" HeaderStyle-Width="70px">
                        <ItemTemplate>
                            <asp:TextBox CssClass="month1" Style="text-align: right" ID="txtValue1" Enabled='<%# Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE1") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE1") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM1") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM1") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO1") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO1") != "Y"?true:  false %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month1") %>' onchange="calcompletionRate(event,this.id);"></asp:TextBox>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Febraury" Visible="false" HeaderStyle-Width="70px">

                        <ItemTemplate>
                            <div style="text-align: right" id="divmonth1">
                                <%--<asp:Label ID="lblmonth1" runat="server" Text='<%#Bind("month2") %>'></asp:Label>--%>
                                <asp:TextBox CssClass="month2" Style="text-align: right" ID="txtValue2" Enabled='<%#Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE2") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE2") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM2") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM2") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO2") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO2") != "Y"?true:  false  %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month2") %>' onchange="calcompletionRate(event,this.id);"></asp:TextBox>

                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="March" Visible="false" HeaderStyle-Width="70px">

                        <ItemTemplate>
                            <div style="text-align: right">
                                <%--<asp:Label ID="lblmonth2" runat="server" Text='<%#Bind("month3") %>'></asp:Label>--%>
                                <asp:TextBox CssClass="month3" Style="text-align: right" ID="txtValue3" Enabled='<%#Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE3") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE3") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM3") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM3") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO3") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO3") != "Y"?true:  false  %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month3") %>' onchange="calcompletionRate(event,this.id);"></asp:TextBox>

                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="April" Visible="false" HeaderStyle-Width="70px">

                        <ItemTemplate>
                            <div style="text-align: right">
                                <%--<asp:Label ID="lblmonth4" runat="server" Text='<%#Bind("month4") %>'></asp:Label>--%>
                                <asp:TextBox CssClass="month4" Style="text-align: right" ID="txtValue4" Enabled='<%# Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE4") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE4") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM4") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM4") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO4") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO4") != "Y"?true:  false %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month4") %>' onchange="calcompletionRate(event,this.id);"></asp:TextBox>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="May" Visible="false" HeaderStyle-Width="70px">

                        <ItemTemplate>
                            <div style="text-align: right">
                                <%--<asp:Label ID="lblmonth5" runat="server" Text='<%#Bind("month5") %>'></asp:Label>--%>
                                <%--<asp:TextBox CssClass="month" Style="text-align: right" ID="txtValue5" AutoPostBack="true" Enabled='<%# Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE5") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE5") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM5") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM5") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO5") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO5") != "Y"?true:  false %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month5") %>' onchange="calcompletionRate(event,this.id);" OnTextChanged="txtValue5_TextChanged"></asp:TextBox>--%>
                                <asp:TextBox CssClass="month5" Style="text-align: right" ID="txtValue5" Enabled='<%# Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE5") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE5") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM5") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM5") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO5") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO5") != "Y"?true:  false %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month5") %>' onchange="calcompletionRate(event,this.id);"></asp:TextBox>

                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="June" Visible="false" HeaderStyle-Width="70px">

                        <ItemTemplate>
                            <div style="text-align: right">
                                <%--<asp:Label ID="lblmonth6" runat="server" Text='<%#Bind("month6") %>'></asp:Label>--%>
                                <asp:TextBox CssClass="month6" Style="text-align: right" ID="txtValue6" Enabled='<%# Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE6") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE6") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM6") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM6") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO6") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO6") != "Y"?true:  false %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month6") %>' onchange="calcompletionRate(event,this.id);"></asp:TextBox>

                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="July" Visible="false" HeaderStyle-Width="70px">

                        <ItemTemplate>
                            <div style="text-align: right">
                                <%--<asp:Label ID="lblmonth7" runat="server" Text='<%#Bind("month7") %>'></asp:Label>--%>
                                <asp:TextBox CssClass="month7" Style="text-align: right" ID="txtValue7" Enabled='<%# Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE7") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE7") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM7") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM7") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO7") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO7") != "Y"?true:  false %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month7") %>' onchange="calcompletionRate(event,this.id);"></asp:TextBox>

                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="August" Visible="false" HeaderStyle-Width="70px">

                        <ItemTemplate>
                            <div style="text-align: right">
                                <%--<asp:Label ID="lblmonth8" runat="server" Text='<%#Bind("month8") %>'></asp:Label>--%>
                                <asp:TextBox CssClass="month8" Style="text-align: right" ID="txtValue8" Enabled='<%# Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE8") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE8") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM8") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM8") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO8") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO8") != "Y"?true:  false %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month8") %>' onchange="calcompletionRate(event,this.id);"></asp:TextBox>

                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="September" Visible="false" HeaderStyle-Width="70px">

                        <ItemTemplate>
                            <div style="text-align: right">
                                <%--<asp:Label ID="lblmonth9" runat="server" Text='<%#Bind("month9") %>'></asp:Label>--%>
                                <asp:TextBox CssClass="month9" Style="text-align: right" ID="txtValue9" Enabled='<%# Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE9") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE9") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM9") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM9") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO9") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO9") != "Y"?true:  false %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month9") %>' onchange="calcompletionRate(event,this.id);"></asp:TextBox>

                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="October" Visible="false" HeaderStyle-Width="70px">

                        <ItemTemplate>
                            <div style="text-align: right">
                                <%--<asp:Label ID="lblmonth10" runat="server" Text='<%#Bind("month10") %>'></asp:Label>--%>
                                <asp:TextBox CssClass="month10" Style="text-align: right" ID="txtValue10" Enabled='<%# Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE10") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE10") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM10") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM10") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO10") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO10") != "Y"?true:  false %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month10") %>' onchange="calcompletionRate(event,this.id);"></asp:TextBox>

                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="November" Visible="false" HeaderStyle-Width="70px">

                        <ItemTemplate>
                            <div style="text-align: right">
                                <%--<asp:Label ID="lblmonth11" runat="server" Text='<%#Bind("month11") %>'></asp:Label>--%>
                                <asp:TextBox CssClass="month11" Style="text-align: right" ID="txtValue11" Enabled='<%# Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE11") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE11") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM11") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM11") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO11") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO11") != "Y"?true:  false %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month11") %>' onchange="calcompletionRate(event,this.id);"></asp:TextBox>

                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="December" Visible="false" HeaderStyle-Width="70px">

                        <ItemTemplate>
                            <div style="text-align: right">
                                <%--<asp:Label ID="lblmonth12" runat="server" Text='<%#Bind("month12") %>'></asp:Label>--%>
                                <asp:TextBox CssClass="month12" Style="text-align: right" ID="txtValue12" Enabled='<%# Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE12") == "Y"? false : Convert.ToString(Session["RoleId"])=="SE" && (string) Eval("Status_SE12") !="Y"?true :(Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM12") == "Y"?false : (Convert.ToString(Session["RoleId"])=="BM" || Convert.ToString(Session["RoleId"])=="TM") &&  (string) Eval("Status_BM12") != "Y"?true: Convert.ToString(Session["RoleId"])=="HO"&&  (string) Eval("Status_HO12") == "Y"?false : Convert.ToString(Session["RoleId"])=="HO" &&  (string) Eval("Status_HO12") != "Y"?true:  false %>' onkeypress="return isNumberKey(event,this);" runat="server" Text='<%#Bind("month12") %>' onchange="calcompletionRate(event,this.id);"></asp:TextBox>

                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblMarchSale" runat="server" Text='<%#Bind("Sale_CM") %>' Style="float: right;"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Completion Rate">
                        <ItemTemplate>
                            <asp:Label ID="lblCompletionRate" runat="server" Text='<%#Bind("completionRate") %>' Style="float: right;"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                  </asp:GridView>
            <br />



            <div class="row" runat="server" id="divgridchart" visible="false">
                <div class="row">
                    <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
                        <div id="test">
                        </div>
                    </div>
                </div>
            </div>



            <%--<asp:Button runat="server" OnClientClick="return  handleRender()" />--%>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BranchList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="SalesEngList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlcustomertype" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="MonthList" EventName="SelectedIndexChanged" />
              <asp:PostBackTrigger ControlID="export" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="loading">
        <asp:UpdateProgress ID="updateProgress" runat="server">
            <ProgressTemplate>
                <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                    <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>


</asp:Content>
