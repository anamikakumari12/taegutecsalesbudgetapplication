﻿using Microsoft.PowerBI.Api;
using Microsoft.PowerBI.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class KITItems : System.Web.UI.Page
    {
        public string embedToken;
        public string embedUrl;
        public Guid reportId;
        public static string cter;
        public static string ISTS;
        public string report_id = "";
        CommonFunctions objCom = new CommonFunctions();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserName"] != null)
                {
                    try
                    {
                        cter = null;
                        ISTS = null;
                        if (Convert.ToString(Session["RoleId"]) == "HO")
                        {
                            if (Convert.ToBoolean(Session["CompanyCode"]))
                            {
                                if (Session["cter"] == null)
                                {
                                    Session["cter"] = "TTA";
                                    cter = "TTA";
                                    ISTS = "S";
                                }
                                if (Session["cter"].ToString() == "DUR")
                                {
                                    rdBtnDuraCab.Checked = true;
                                    rdBtnTaegutec.Checked = false;
                                    cter = "DUR";
                                    ISTS = "D";
                                }
                                else
                                {
                                    rdBtnTaegutec.Checked = true;
                                    rdBtnDuraCab.Checked = false;
                                    cter = "TTA";
                                    ISTS = "S";
                                }
                                cterDiv.Visible = true;
                            }
                        }
                        else
                        {
                            if (Session["Territory"].ToString() == "DUR")
                            {
                                cter = "DUR";
                                ISTS = "D";
                            }
                            else
                            {
                                cter = "TTA";
                                ISTS = "S";
                            }
                        }

                        using (var client = new PowerBIClient(new Uri(Configurations.ApiUrl), Authentication.GetTokenCredentials()))
                        {
                            // Get a list of reports
                            var reports = client.Reports.GetReportsInGroup(Configurations.WorkspaceId);

                            //// Populate dropdown list
                            foreach (Report item in reports.Value)
                            {
                                // ddlReport.Items.Add(new ListItem(item.Name, item.Id.ToString()));
                                if (item.Name == BMSResource.KitItemPriceList)
                                {
                                    report_id = item.Id.ToString();
                                    break;
                                }
                            }
                            ViewState["report_id"] = report_id;

                            //// Select first item
                            //ddlReport.SelectedIndex = 2;
                        }
                        LoadReport();
                    }
                    catch (Exception ex)
                    {
                        objCom.LogError(ex);
                    }
                }
            }
        }


        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

                if (rdBtnTaegutec.Checked)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                }
                if (rdBtnDuraCab.Checked)
                {
                    Session["cter"] = "DUR";
                    cter = "DUR";
                }
                LoadReport();


            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void LoadReport()
        {
            try
            {
                report_id = Convert.ToString(ViewState["report_id"]);
                // Generate an embed token and populate embed variables
                using (var client = new PowerBIClient(new Uri(Configurations.ApiUrl), Authentication.GetTokenCredentials()))
                {

                    // Retrieve the selected report
                    var report = client.Reports.GetReportInGroup(Configurations.WorkspaceId, new Guid(report_id));

                    // Generate an embed token to view
                    var generateTokenRequestParameters = new GenerateTokenRequest(TokenAccessLevel.View);
                    var tokenResponse = client.Reports.GenerateTokenInGroup(Configurations.WorkspaceId, report.Id, generateTokenRequestParameters);
                    var newUrl = report.EmbedUrl;
                   // newUrl = report.EmbedUrl + "&$filter=KITDetails/Item_x0020_Stat_x0020_Cat in (" + Convert.ToString(Session["MappedUsers"]) + ")";
                    if (!string.IsNullOrEmpty(ISTS))
                        newUrl = newUrl + "&$filter=KITDetails/Item_x0020_Stat_x0020_Cat eq '" + ISTS + "'";
                    objCom.LogMessage(newUrl);
                    report.EmbedUrl = newUrl;
                    embedToken = tokenResponse.Token;
                    embedUrl = report.EmbedUrl;
                    reportId = report.Id;
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
    }
}