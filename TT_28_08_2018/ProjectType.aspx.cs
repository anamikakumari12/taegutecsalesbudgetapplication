﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class ProjectType : System.Web.UI.Page
    {
        #region Global Declaration
        projecttypecs objprojtype = new projecttypecs();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                GvBindprojecttype();
                Add_Click(null, null);
            }
           // Add_Click(null, null);

        }

        protected void GvBindprojecttype()
        {
            DataTable dt = new DataTable();
            dt = objprojtype.getdataprojecttype();
            ViewState["GridData"] = null;
            ViewState["GridData"] = dt;
            Gridviewprojecttype.DataSource = dt;
            Gridviewprojecttype.DataBind();
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            
            string strProjectType = string.Empty;
            strProjectType = txtprojecttype.Text;
            if (strProjectType == "")
            {
                GvBindprojecttype();
            }
            else
            {
               // objprojtype.Add_projecttype(strProjectType);
              int output = objprojtype.Add_projecttype(strProjectType);               
             
              if (output == 0)
              {
                  ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Name Already Exists');", true);
              }
              else
              {
                  ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('sucessfully updated');", true);
              }

              GvBindprojecttype();
            }
            txtprojecttype.Text = "";

            
        }

        protected void Delete_Click(object sender, ImageClickEventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue.Contains(','))
                confirmValue = confirmValue.Substring(confirmValue.LastIndexOf(',') + 1);
            if (confirmValue == "Yes")
            {
                ImageButton ibtn1 = sender as ImageButton;
                int rowIndex = Convert.ToInt32(ibtn1.Attributes["RowIndex"]);
                GridViewRow row = Gridviewprojecttype.Rows[rowIndex];
                var id = row.FindControl("txtprojecttype") as System.Web.UI.WebControls.Label;
                string Projecttype = id.Text;
                objprojtype.Deleteprojecttype(Projecttype);
            }

            GvBindprojecttype();

        }


    }
}