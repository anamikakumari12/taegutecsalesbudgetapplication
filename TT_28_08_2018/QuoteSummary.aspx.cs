﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using TSBA_BusinessAccessLayer;
using TSBA_BusinessObjects;

namespace TaegutecSalesBudget
{
    public partial class QuoteSummary : System.Web.UI.Page
    {
        #region GlobalDeclaration
        CommonFunctions objCom = new CommonFunctions();
        public static string cter;
        DataTable dtCutomerDetails;
        Review objRSum = new Review();
        Reports objReports = new Reports();
        QuoteBL objQuoteBL = new QuoteBL();
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    LoadAllDropDownAtFirst();
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            BindGrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadDates", "LoadDates()", true);
        }

        protected void grdPriceSummary_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName.Equals("Open"))
            //{
            //    QuoteBL objquoteBL = new QuoteBL();
            //    DataTable dtQuote = new DataTable();
            //    QuoteBO objQuoteBO = new QuoteBO();
            //    objQuoteBO.Ref_Number = Convert.ToString(e.CommandArgument);
            //    dtQuote = objquoteBL.GetQuoteDetailsBL(objQuoteBO);
            //    if (dtQuote.Rows.Count > 0)
            //    {
            //        grdDetailedPriceSummary.DataSource = dtQuote;
            //    }
            //    else
            //    {
            //        grdDetailedPriceSummary.DataSource = null;
            //    }
            //    grdDetailedPriceSummary.DataBind();
            // }
        }

        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (rdBtnTaegutec.Checked)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                }
                if (rdBtnDuraCab.Checked)
                {
                    Session["cter"] = "DUR";
                    cter = "DUR";
                }
                LoadBranches();
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        /// <summary>
        /// To load the sales engineer for the selected branch only
        /// Modified By : Anamika
        /// Date : Oct 19, 2016
        /// Desc : LoadAllDropDown is called.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                //hdnsearch.Value = "";
                DataTable dtSalesEngDetails = new DataTable();
                string branchcode = ddlBranch.SelectedItem.Value;
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                if (branchcode == "ALL")
                {
                    if(roleId=="TM")
                        dtSalesEngDetails = objReports.LoadUserInfo(null, "ALL",null,strUserId);
                    else
                    dtSalesEngDetails = objReports.LoadUserInfo(null, "ALL");
                    
                }

                else
                {
                    if (roleId == "TM")
                        dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode, branchcode, strUserId);
                    else
                        dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);
                   
                }
                if (dtSalesEngDetails != null)
                {
                    ddlSE.DataSource = dtSalesEngDetails;
                    ddlSE.DataTextField = "EngineerName";
                    ddlSE.DataValueField = "EngineerId";
                    ddlSE.DataBind();
                    ddlSE.Items.Insert(0, "ALL");
                }
                ddlSE_SelectedIndexChanged(null, null);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                ////LoadAllDropDown();
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void ddlCustomerClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                string branchCode = Session["BranchCode"].ToString();
                LoadCustomers(strUserId, roleId, branchCode);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void ddlSE_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                string branchCode = Session["BranchCode"].ToString();
                LoadCustomers(strUserId, roleId, branchCode);
                grdPriceSummary.DataSource = null;
                grdPriceSummary.DataBind();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Load All the branches
        /// </summary>
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

                string roleId = Convert.ToString(Session["RoleId"]);
                string userId = Convert.ToString(Session["UserId"]);
                string branchcode = Convert.ToString(Session["BranchCode"]);
                Review objRSum = new Review();
                objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
                objRSum.roleId = roleId;
                objRSum.flag = "Branch";
                objRSum.cter = Convert.ToString(Session["cter"]);
                DataTable dtData = new DataTable();
                dtData = objRSum.getFilterAreaValue(objRSum);
                if (dtData != null)
                {
                    if (dtData.Rows.Count != 0)
                    {
                        ddlBranch.DataSource = dtData;
                        ddlBranch.DataTextField = "BranchDesc";
                        ddlBranch.DataValueField = "BranchCode";
                        ddlBranch.DataBind();
                        ddlBranch.Items.Insert(0, "ALL");
                    }
                    if (roleId == "BM" || roleId == "SE")
                    {
                        string branchdesc = Session["BranchDesc"].ToString();
                        dtData.Columns.Add("BranchCode");
                        dtData.Columns.Add("BranchDesc");
                        dtData.Rows.Add(branchcode, branchdesc);
                        ddlBranch.DataSource = dtData;
                        ddlBranch.DataTextField = "BranchDesc";
                        ddlBranch.DataValueField = "BranchCode";
                        ddlBranch.DataBind();
                    }
                }
            }

            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

            // ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        /// <summary>
        /// Author : Anamika
        /// Date : Oct 20, 2016
        /// Desc : Load all the dropdowns at first
        /// </summary>
        private void LoadAllDropDownAtFirst()
        {
            try
            {
                cter = null;

                if (Session["UserId"].ToString() != null)
                {
                    string strUserId = Session["UserId"].ToString();
                    string roleId = Session["RoleId"].ToString();
                    string salesEngName = Session["UserName"].ToString();
                    string branchCode = Session["BranchCode"].ToString();
                    LoadBranches();
                    LoadCustomers(strUserId, roleId, branchCode);
                    LoadAllSEs();
                    if (roleId == "SE")
                    {

                        //LoadAllReviewer_SE(strUserId);
                        //LoadAllEscalator_BM(strUserId);
                        //LoadCustomerDetails_SE(strUserId);
                        ddlBranch.SelectedValue = branchCode;
                        ddlSE.SelectedValue = strUserId;
                        ddlSE.Enabled = false;
                        ddlBranch.Enabled = false;
                        //LoadAllSEs();
                        //save.Visible = false;
                        //export.Visible = false;
                        //back.Visible = false;

                    }
                    if (roleId == "BM")
                    {

                        //LoadAllReviewer_BM(strUserId);
                        //LoadAllEscalator_BM(strUserId);
                        //LoadAllSE_BM();
                        //LoadCustomerDetails_BM(branchCode);
                        ddlBranch.SelectedValue = branchCode;
                        ddlBranch.Enabled = false;
                        //save.Visible = false;
                        //export.Visible = false;
                        //back.Visible = false;
                    }
                    if (roleId == "HO")
                    {
                        if (Session["cter"] == null && roleId == "HO")
                        {
                            Session["cter"] = "TTA";
                            cter = "TTA";

                        }
                        if (Session["cter"].ToString() == "DUR")
                        {
                            rdBtnDuraCab.Checked = true;
                            rdBtnTaegutec.Checked = false;
                            cter = "DUR";
                        }
                        else
                        {
                            rdBtnTaegutec.Checked = true;
                            rdBtnDuraCab.Checked = false;
                            cter = "TTA";
                        }
                        cterDiv.Visible = true;

                        //LoadCustomerDetails_HO();
                        //LoadAllSEs();
                        //LoadAllReviewers();
                        //LoadAllEscalator();
                        //save.Visible = false;
                        //export.Visible = false;
                        //back.Visible = false;

                    }

                    if (roleId == "TM")
                    {
                        //LoadBranches_TM(strUserId);
                        //LoadAllSE_TM(strUserId);
                        //LoadAllReviewer_TM(strUserId);
                        //LoadAllEscalator_TM(strUserId);
                        //LoadCustomerDetails_TM(strUserId);
                        //save.Visible = false;
                        //export.Visible = false;
                        //back.Visible = false;
                    }

                }
                else { Response.Redirect("Login.aspx?Login"); }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void LoadAllSEs()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtSEDetails = new DataTable();
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Session["BranchCode"].ToString();
                objRSum.BranchCode = roleId == "TM" && ddlBranch.SelectedItem.Value == "ALL" ? userId : ddlBranch.SelectedItem.Value;
                objRSum.roleId = roleId;
                objRSum.flag = "SalesEngineer";
                objRSum.cter = cter;
                dtSEDetails = objRSum.getFilterAreaValue(objRSum);
                if (dtSEDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("SE_number", typeof(string));
                    dtDeatils.Columns.Add("SE_name", typeof(string));

                    for (int i = 0; i < dtSEDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtSEDetails.Rows[i].ItemArray[0].ToString(), dtSEDetails.Rows[i].ItemArray[1].ToString());
                    }
                    ddlSE.DataSource = dtDeatils;
                    ddlSE.DataTextField = "SE_name";
                    ddlSE.DataValueField = "SE_number";
                    ddlSE.DataBind();
                    ddlSE.Items.Insert(0, "ALL");
                }
            }

            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        private void LoadCustomers(string strUserId, string roleId, string branchCode)
        {
            try
            {
                MDP objMDP = new MDP();
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                QuoteBO objQuoteBO = new QuoteBO();
                dtCutomerDetails = new DataTable();
                string salesEngineerId = string.Empty;
                string ddCustomerClass = Convert.ToString(ddlCustomerClass.SelectedItem.Value) == "ALL" ? null : ddlCustomerClass.SelectedItem.Value;
                if (roleId == "SE")
                {
                    objQuoteBO.branch = null;
                    objQuoteBO.SE_id = strUserId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;

                   // dtCutomerDetails = objMDP.LoadCustomerDetailstype(strUserId, roleId, null, null, cter, ddCustomerClass);
                }
                else if (roleId == "BM")
                {
                    salesEngineerId = ddlSE.SelectedItem.Value == "ALL" ? null : ddlSE.SelectedItem.Value;
                    objQuoteBO.branch = branchCode;
                    objQuoteBO.SE_id = salesEngineerId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    //dtCutomerDetails = objMDP.LoadCustomerDetailstype(salesEngineerId, "BM", null, branchCode, cter, ddCustomerClass);
                }
                else if (roleId == "TM")
                {
                    string branch = ddlBranch.SelectedItem.Value == "ALL" ? null : ddlBranch.SelectedItem.Value;
                    salesEngineerId = ddlSE.SelectedItem.Value == "ALL" ? null : ddlSE.SelectedItem.Value;
                    objQuoteBO.branch = branch;
                    objQuoteBO.SE_id = salesEngineerId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    objQuoteBO.TM_id = strUserId;
                    //dtCutomerDetails = objMDP.LoadCustomerDetailstype(null, "TM", null, branch, cter, ddCustomerClass, strUserId);
                }
                else
                {
                    string branch = ddlBranch.SelectedItem.Value;
                    salesEngineerId = ddlSE.SelectedItem.Value == "ALL" ? null : ddlSE.SelectedItem.Value;
                    objQuoteBO.branch = branch;
                    objQuoteBO.SE_id = salesEngineerId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    //dtCutomerDetails = objMDP.LoadCustomerDetailstype(null, "HO", null, branch, cter, ddCustomerClass);

                }
                dtCutomerDetails = objQuoteBL.GetCustomerListBL(objQuoteBO);
                if (dtCutomerDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customernumber", typeof(string));
                    dtDeatils.Columns.Add("customername", typeof(string));
                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(Convert.ToString(dtCutomerDetails.Rows[i]["customer_number"]), Convert.ToString(dtCutomerDetails.Rows[i]["customer_short_name"]) + "(" + Convert.ToString(dtCutomerDetails.Rows[i]["customer_number"]) + ")");
                    }
                    //Session["dtCustForQuote"] = dtDeatils;
                    ddlCustomers.DataSource = dtDeatils;
                    ddlCustomers.DataTextField = "customername";
                    ddlCustomers.DataValueField = "customernumber";
                    ddlCustomers.DataBind();
                    ddlCustomers.Items.Insert(0, "ALL");
                }
                else
                {
                    ddlCustomers.DataSource = null;
                    ddlCustomers.DataBind();
                }


            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }
        private void BindGrid()
        {
            DataTable dtoutput = new DataTable();
            try
            {
                string start_date = string.Empty;
                string end_date = string.Empty;
                string selectedDate = txtDateRange.Text;
                if (selectedDate.Contains("/"))
                {
                    string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                    start_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                    end_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                }
                else
                {
                    end_date = DateTime.Now.ToString("MM/dd/yyyy").Replace("-", "/");
                    start_date = DateTime.Now.AddDays(-7).ToString("MM/dd/yyyy").Replace("-", "/");
                }
                QuoteBO objQuoteBO = new QuoteBO();
                objQuoteBO.StartDate = start_date;
                objQuoteBO.EndDate = end_date;
                objQuoteBO.Cust_Number = Convert.ToString(ddlCustomers.SelectedItem.Value) == "ALL" ? null : Convert.ToString(ddlCustomers.SelectedItem.Value);
                objQuoteBO.branch = Convert.ToString(ddlBranch.SelectedItem.Value) == "ALL" ? null : Convert.ToString(ddlBranch.SelectedItem.Value);
                objQuoteBO.SE_id = Convert.ToString(ddlSE.SelectedItem.Value) == "ALL" ? null : Convert.ToString(ddlSE.SelectedItem.Value);
                string roleId = Convert.ToString(Session["RoleId"]);
                string strUserId = Convert.ToString(Session["UserId"]);
                string ddCustomerClass = Convert.ToString(ddlCustomerClass.SelectedItem.Value) == "ALL" ? null : ddlCustomerClass.SelectedItem.Value;
                string salesEngineerId = string.Empty;
                if (roleId == "SE")
                {
                    objQuoteBO.branch = null;
                    objQuoteBO.SE_id = strUserId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    
                    // dtCutomerDetails = objMDP.LoadCustomerDetailstype(strUserId, roleId, null, null, cter, ddCustomerClass);
                }
                else if (roleId == "BM")
                {
                    salesEngineerId = ddlSE.SelectedItem.Value == "ALL" ? null : ddlSE.SelectedItem.Value;
                    objQuoteBO.branch = Convert.ToString(Session["BranchCode"]);
                    objQuoteBO.SE_id = salesEngineerId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    //dtCutomerDetails = objMDP.LoadCustomerDetailstype(salesEngineerId, "BM", null, branchCode, cter, ddCustomerClass);
                }
                else if (roleId == "TM")
                {
                    string branch = ddlBranch.SelectedItem.Value == "ALL" ? null : ddlBranch.SelectedItem.Value;
                    salesEngineerId = ddlSE.SelectedItem.Value == "ALL" ? null : ddlSE.SelectedItem.Value;
                    objQuoteBO.branch = branch;
                    objQuoteBO.SE_id = salesEngineerId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    objQuoteBO.TM_id = strUserId;
                    //dtCutomerDetails = objMDP.LoadCustomerDetailstype(null, "TM", null, branch, cter, ddCustomerClass, strUserId);
                }
                else
                {
                    string branch = ddlBranch.SelectedItem.Value == "ALL" ? null : ddlBranch.SelectedItem.Value;
                    salesEngineerId = ddlSE.SelectedItem.Value == "ALL" ? null : ddlSE.SelectedItem.Value;
                    objQuoteBO.branch = branch;
                    objQuoteBO.SE_id = salesEngineerId;
                    objQuoteBO.role_id = roleId;
                    objQuoteBO.cust_type = null;
                    objQuoteBO.cter = cter;
                    objQuoteBO.customer_class = ddCustomerClass;
                    //dtCutomerDetails = objMDP.LoadCustomerDetailstype(null, "HO", null, branch, cter, ddCustomerClass);

                }
                objQuoteBO.user_id = strUserId;
                QuoteBL objQuoteBL = new QuoteBL();
                dtoutput = objQuoteBL.GetQuoteSummaryBL(objQuoteBO);
                if (dtoutput != null)
                {
                    if (dtoutput.Rows.Count > 0)
                    {
                        grdPriceSummary.DataSource = dtoutput;
                        grdPriceSummary.DataBind();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                        LoadDetailQuote(objQuoteBO);
                    }
                    else
                    {
                        grdPriceSummary.DataSource = null;
                        grdPriceSummary.DataBind();
                    }

                }
                else
                {
                    grdPriceSummary.DataSource = null;
                    grdPriceSummary.DataBind();
                }


            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        private void LoadDetailQuote(QuoteBO objQuoteBO)
        {
            try
            {
                QuoteBL objquoteBL = new QuoteBL();
               
                objQuoteBO.Cust_Number = Convert.ToString(ddlCustomers.SelectedValue);
                DataTable dtQuote = new DataTable();
                dtQuote = objquoteBL.GetQuoteDetailsBL(objQuoteBO);
                Session["dtQuote"] = dtQuote;
                if (dtQuote != null)
                {
                    if (dtQuote.Rows.Count > 0)
                    {
                        grdItemSummary.DataSource = dtQuote;
                        grdItemSummary.DataBind();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                    }
                    else
                    {
                        grdItemSummary.DataSource = null;
                        grdItemSummary.DataBind();
                    }

                }
                else
                {
                    grdItemSummary.DataSource = null;
                    grdItemSummary.DataBind();
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        [WebMethod]
        public static string LoadDetailedGrid(string ref_no)
        {
            string output = string.Empty;
            try
            {
                DataTable dt = (DataTable)HttpContext.Current.Session["dtQuote"];
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        var rows = from row in dt.AsEnumerable()
                                   where row.Field<string>("Ref_number").Trim() == ref_no
                                   select row;
                        dt = rows.CopyToDataTable();
                        if (dt.Rows.Count > 0)
                        {
                            output = DataTableToJSONWithStringBuilder(dt);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return output;
        }

        [WebMethod]
        public static string PlaceOrder(string ref_num, string ID, string quantity, string item, string order_type, string schedule, string OrderStartDate, string cust_num, string cust_name, string PO_comment)
        {
            string output = string.Empty;
            QuoteSummary objSumm = new QuoteSummary();
            try
            {
                CommonFunctions objCom = new CommonFunctions();
                QuoteBO objBO = new QuoteBO();
                objBO.Ref_Number = ref_num;
                objBO.Item_Number = item;
                objBO.QuoteID = Convert.ToInt32(ID);
                objBO.OrderType = order_type;
                if (order_type == "schedule")
                    objBO.scheduleType = schedule;
                else
                    objBO.scheduleType = "";
                objBO.quantity = quantity;
                objBO.orderbyId = Convert.ToString(HttpContext.Current.Session["UserId"]);
                objBO.orderbyName = Convert.ToString(HttpContext.Current.Session["UserName"]);
                objBO.Cust_Number = cust_num;
                objBO.Cust_Name = cust_name;
                objBO.POComment = PO_comment;
                objBO.OrderStartDate = OrderStartDate;
                QuoteBL objBL = new QuoteBL();
                objBO = objBL.PlaceOrderBL(objBO);
                if (objBO.Err_code == 200)
                {
                    if (!String.IsNullOrEmpty(Convert.ToString(objBO.to)))
                    {
                        string attachment = objSumm.GenerateQuotePOFormat(objBO.Ref_Number, objBO.Item_Number);
                        EmailDetails objEmail = new EmailDetails();
                        objEmail.toMailId = objBO.to;
                        objEmail.ccMailId = objBO.cc;
                        objEmail.subject = objBO.subject;
                        objEmail.body = objBO.message;
                        objEmail.attachment = attachment;
                        objCom.SendMail(objEmail);
                    }
                }
                output = "{\"code\":\"" + objBO.Err_code + "\",\"msg\":\"" + objBO.Err_msg + "\"}";
            }
            catch (Exception ex)
            {
                output = "{\"code\":\"105\",\"msg\":\"" + ex.Message + "\"}";
                CommonFunctions.LogErrorStatic(ex);
            }
            return output;
        }




        [WebMethod]
        //public static string RequestForReApproval(string ref_num, string ID, string item, string status, string reason)
        public static string RequestForReApproval(List<QuoteStatus> objList)
        {
            string output = string.Empty;
            List<QuoteStatusDB> quoteList = new List<QuoteStatusDB>();
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                QuoteStatusDB objQuoteBO = new QuoteStatusDB();
                QuoteBL objQuoteBL = new QuoteBL();
                string UIpattern = "MM/dd/yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                string Expirydate;
                foreach (QuoteStatus obj in objList)
                {
                    objQuoteBO = new QuoteStatusDB();
                    objQuoteBO.ID = Convert.ToInt32(obj.id);
                    objQuoteBO.Reason = Convert.ToString(obj.comment);
                    objQuoteBO.changeby = Convert.ToString(HttpContext.Current.Session["UserId"]);
                    objQuoteBO.RefNumber = Convert.ToString(obj.ref_no);
                    objQuoteBO.item = Convert.ToString(obj.item);
                    objQuoteBO.flag = Convert.ToString(HttpContext.Current.Session["RoleId"]);
                    if (obj.status == "Escalated")
                    {
                        if (objQuoteBO.flag == "BM" || objQuoteBO.flag == "TM")
                        {
                            objQuoteBO.Status = "Escalated By BM";
                        }
                        else if (objQuoteBO.flag == "SE")
                        {
                            objQuoteBO.Status = obj.status;
                        }
                    }
                    else if (obj.status == "Approved")
                    {
                        if (objQuoteBO.flag == "BM" || objQuoteBO.flag == "TM")
                        {
                            objQuoteBO.Status = "Approved By BM";
                            objQuoteBO.RecommendedPrice = obj.RecPrice;
                        }
                        else
                        {
                            //if (DateTime.TryParseExact(obj.expiry_date, UIpattern, null, DateTimeStyles.None, out parsedDate))
                            //{
                            //    Expirydate = parsedDate.ToString(DBPattern);
                            //    objQuoteBO.expiry_date = DateTime.ParseExact(Expirydate, "MM-dd-yyyy", null);
                            //}
                            //else
                            //{
                            //    objQuoteBO.expiry_date = null;
                            //}
                            objQuoteBO.Status = obj.status;
                            objQuoteBO.OfferPrice = obj.offerPrice;
                        }
                    }
                    else
                    {
                        objQuoteBO.Status = obj.status;
                    }

                    quoteList.Add(objQuoteBO);
                }

                //QuoteBO objBO = new QuoteBO();
                //objBO.Ref_Number = ref_num;
                //objBO.Item_Number = item;
                //objBO.QuoteID = Convert.ToInt32(ID);
                //objBO.ChangeBy = Convert.ToString(HttpContext.Current.Session["UserId"]);
                //objBO.role_id = Convert.ToString(HttpContext.Current.Session["RoleId"]);
                //if (status == "Escalated")
                //{
                //    if (objBO.role_id == "BM" || objBO.role_id == "TM")
                //    {
                //        objBO.Status = "Escalated By BM";
                //    }
                //    else if (objBO.role_id == "SE")
                //    {
                //        objBO.Status = status;
                //    }
                //}
                //else if (status == "Approved")
                //{
                //    if (objBO.role_id == "BM" || objBO.role_id == "TM")
                //    {
                //        objBO.Status = "Approved By BM";
                //    }
                //    else
                //    {
                //        objBO.Status = status;
                //    }
                //}
                //else
                //{
                //    objBO.Status = status;
                //}
               // objBO.Reason = reason;
                DataTable dtQuote = new DataTable();
                dtQuote = CommonFunctions.ToDataTable<QuoteStatusDB>(quoteList);
                List<QuoteBO> objQuoteList = new List<QuoteBO>();
                objQuoteList = objQuoteBL.updateQuoteStatusBL(dtQuote);

                //objBO = objBL.updateQuoteStatusBL(objBO);
                if (objQuoteList.Count > 0)
                {
                    foreach (QuoteBO objOutputBO in objQuoteList)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(objOutputBO.to)))
                        {
                            EmailDetails objEmail = new EmailDetails();
                            objEmail.toMailId = objOutputBO.to;
                            objEmail.ccMailId = objOutputBO.cc;
                            objEmail.subject = objOutputBO.subject;
                            objEmail.body = objOutputBO.message;
                            objCom.SendMail(objEmail);
                        }

                        output = "{\"code\":\"" + objOutputBO.Err_code + "\",\"msg\":\"" + objOutputBO.Err_msg + "\"}";
                    }
                }
            }
            catch (Exception ex)
            {
                output = "{\"code\":\"105\",\"msg\":\"" + ex.Message + "\"}";
                CommonFunctions.LogErrorStatic(ex);
            }
            return output;
        }

        [WebMethod]
        public static string DownloadFile(string file)
        {
            string output = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                string filePath = ConfigurationManager.AppSettings["Escalation_Folder"].ToString() + Convert.ToString(file);

                //if (File.Exists(filePath))
                //{
                byte[] bytes = File.ReadAllBytes(filePath);

                //Convert File to Base64 string and send to Client.
                return Convert.ToBase64String(bytes, 0, bytes.Length);
                //Response.ContentType = "application/octet-stream";
                //byte[] bts = System.IO.File.ReadAllBytes(filePath);
                //MemoryStream ms = new MemoryStream(bts);
                //HttpContext.Current.Response.Clear();
                //HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=\"" + Path.GetFileName(filePath) + "\"");
                //HttpContext.Current.Response.TransmitFile(filePath);
                //HttpContext.Current.Response.End();
                //}
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return ex.Message;
            }

        }
        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        //if (Convert.ToString(table.Columns[j].ColumnName) == "SALES_MTD_VALUE")
                        //{
                        //    if (j < table.Columns.Count - 1)
                        //    {
                        //        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + table.Rows[i][j].ToString() + ",");
                        //    }
                        //    else if (j == table.Columns.Count - 1)
                        //    {
                        //        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":"  + table.Rows[i][j].ToString() );
                        //    }
                        //}
                        //else
                        //{ 
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\"");
                        }
                        //}
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }

        #endregion

        #region reportGenerate

        private string GenerateQuotePOFormat(string ref_number, string item_code)
        {
            string file = string.Empty;
            try
            {
                string html = string.Empty;
                string filename = string.Empty;
                string filepath = string.Empty;
                QuoteBL objQuoteBL = new QuoteBL();
                QuoteBO objQuoteBO = new QuoteBO();
                DataTable dt = new DataTable();

                html = "<!DOCTYPE html><html><head><style>table td{border:solid 1px #ddd;padding:5px;}</style></head><body>";
                objQuoteBO.Ref_Number = ref_number;
                objQuoteBO.Item_Number = item_code;
                dt = objQuoteBL.getQuotePOFormatBL(objQuoteBO);

                html += Getheading(dt);

                html += "</body></html>";
                filename = "Quote_PO_" + Convert.ToString(ref_number) + ".pdf";
                filepath = ConfigurationManager.AppSettings["PDF_Folder"].ToString();
                convertPDF(html, filepath, filename);
                file = String.Concat(filepath, filename);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return file;
        }

        private void convertPDF(string html, string filepath, string filename)
        {
            try
            {
                using (FileStream fs = new FileStream(Path.Combine(filepath, "test1.htm"), FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                    {
                        w.WriteLine(html);
                    }
                }
                GeneratePdfFromHtml(filepath, filename, html);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        private void GeneratePdfFromHtml(string filepath, string filename, string html)
        {
            string outputFilename = Path.Combine(filepath, filename);
            string inputFilename = Path.Combine(filepath, "test1.htm");

            using (var input = new FileStream(inputFilename, FileMode.Open))
            using (var output = new FileStream(outputFilename, FileMode.Create))
            {
                CreatePdf(filepath, filename, input, output, html);
            }
        }

        private void CreatePdf(string filepath, string filename, FileStream htmlInput, FileStream pdfOutput, string html)
        {
            try
            {
                using (var document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 30, 30, 30, 30))
                {
                    var writer = PdfWriter.GetInstance(document, pdfOutput);
                    var worker = XMLWorkerHelper.GetInstance();
                    TextReader tr = new StreamReader(htmlInput);
                    document.Open();
                    worker.ParseXHtml(writer, document, htmlInput, null, Encoding.UTF8);
                    //worker.ParseXHtml(writer, document, new StringReader(html));
                    document.Close();
                }

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        private string Getheading(DataTable dt)
        {

            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            string imageURL = string.Empty;
            string Heading = string.Empty;
            string taegutec_add = string.Empty;
            string Customer_Name = string.Empty;
            string Customer_Address = string.Empty;
            string Customer_Number = string.Empty;
            string Quotation_No = string.Empty;
            string Date = string.Empty;
            string remarks = string.Empty;
            string PO_no = string.Empty;
            try
            {
                Heading = Convert.ToString(ConfigurationManager.AppSettings["QuotePO_Heading"]);
                imageURL = Convert.ToString(ConfigurationManager.AppSettings["Logo"]);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        Customer_Name = Convert.ToString(dt.Rows[0]["Customer_Name"]);
                        Customer_Address = Convert.ToString(dt.Rows[0]["Customer_Address"]);
                        Customer_Number = Convert.ToString(dt.Rows[0]["Customer_Number"]);
                        Quotation_No = Convert.ToString(dt.Rows[0]["Quotation_No"]);
                        Date = Convert.ToString(dt.Rows[0]["Date"]);
                        PO_no = Convert.ToString(dt.Rows[0]["PO_No"]);
                        taegutec_add = Convert.ToString(dt.Rows[0]["Taegutec_Address"]);
                        remarks = Convert.ToString(dt.Rows[0]["PurchaseOrderComment"]);

                        strHTMLBuilder.Append("<table style='border: 1px solid darkgray; font-family: Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif; border-collapse:collapse;'>");
                        strHTMLBuilder.Append("<tr style=' background-color: #5faae6c7;'>");
                        strHTMLBuilder.Append("<td colspan='9' style='text-align:center; font-size:30px; font-weight:bold; color:black;'>");
                        strHTMLBuilder.Append(Heading);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td colspan='4'><img style='float:left;width: 80%;' src='");
                        strHTMLBuilder.Append(imageURL);
                        strHTMLBuilder.Append("'/>");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5' style='font-weight:bold; font-size: 15px;'>");
                        strHTMLBuilder.Append(Customer_Name);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td colspan='4' style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append(taegutec_add);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5' style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append(Customer_Address);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr >");
                        strHTMLBuilder.Append("<td colspan='4' >");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5'  style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append("REMARKS:");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr >");
                        strHTMLBuilder.Append("<td colspan='4' style='padding:0px'>");
                        strHTMLBuilder.Append("<table width='100%'>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>CUSTOMER NO : ");
                        strHTMLBuilder.Append(Customer_Number);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>PURCHASE ORDER NO : ");
                        strHTMLBuilder.Append(PO_no);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>QTN REFERENCE NO : ");
                        strHTMLBuilder.Append(Quotation_No);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>DATE : ");
                        strHTMLBuilder.Append(Date);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("</table>");
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='5' style='font-size: 10px;'>");
                        strHTMLBuilder.Append(remarks);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");

                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>SL<br/>NO</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>ITEM DESCRIPTION</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>CATALOGUE NO</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>ITEM QTY</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>UNIT<br/>PRICE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>LINE<br/>VALUE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>SCHEDULE<br/>DATE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>W/H</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;'>END<br/>CUSTOMER</td>");
                        strHTMLBuilder.Append("</tr>");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            strHTMLBuilder.Append("<tr>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(i + 1));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Catalogue_No"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item_Qty"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Unit_Price"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Line_Value"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Schedule_Date"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["WHS"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 10px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["End_Customer"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("</tr>");
                        }


                        strHTMLBuilder.Append("</table>");
                    }
                }

                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }
        #endregion

    }
}