USE [Taegutec_Sales_Budget]
GO

/****** Object:  Table [dbo].[tbl_Monthly_Tartget]    Script Date: 08-02-2021 09:59:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_Monthly_Tartget](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerNum] [varchar](50) NULL,
	[Month] [varchar](50) NULL,
	[Year] [varchar](50) NULL,
	[Quantity] [varchar](50) NULL,
	[value] [varchar](50) NULL,
	[Status_SE] [varchar](50) NULL,
	[Status_BM] [varchar](50) NULL,
	[Status_HO] [varchar](50) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[GetMonthlytargetValue]    Script Date: 08-02-2021 10:00:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER Procedure [dbo].[GetMonthlytargetValue]
( @CustomerNumber varchar(50)

)
As

BEGIN
Select * from tbl_Monthly_Tartget where CustomerNum = @CustomerNumber
end




USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[insert_MonthlyTargetInfo]    Script Date: 08-02-2021 10:01:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[insert_MonthlyTargetInfo]
(   
    @CustomerNumber varchar(MAX) ,
	@Month varchar(50) ,
	@Year varchar(50),
	@Quantity varchar(50) ,	
	@Value varchar(50),
	@role varchar(50),
	
	@error_code INT OUTPUT,
@error_msg VARCHAR(MAX) OUTPUT
	 )
AS

BEGIN
If @Quantity is null
set @Quantity=0
If @Value is null
set @Value=0
DECLARE  @Status_SE varchar(10) =null,
	@Status_BM varchar(10)=null,
	@Status_HO varchar(10)=null

If EXISTS(Select * from dbo.tbl_Monthly_Tartget where CustomerNum = @CustomerNumber and Month = @Month)
Begin
if(@role ='SE')
begin
set @Status_SE = 'Y';
update dbo.tbl_Monthly_Tartget set Status_SE = @Status_SE where CustomerNum = CustomerNum;
End
else if(@role ='BM')
begin
set @Status_BM = 'Y';
set @Status_SE = 'Y';
update dbo.tbl_Monthly_Tartget set Status_BM = @Status_BM,Status_SE =@Status_SE where CustomerNum = CustomerNum;
End
else if(@role ='HO')
begin
set @Status_HO = 'Y';
set @Status_BM = 'Y';
set @Status_SE = 'Y';
update dbo.tbl_Monthly_Tartget set Status_HO = @Status_HO,Status_BM = @Status_BM,Status_SE =@Status_SE where CustomerNum = CustomerNum;
End

SET @error_code=0
	SET @error_msg='Updated successfully.'
End
Else
Begin

if(@role ='SE')
begin
set @Status_SE = 'Y';
insert into dbo.tbl_Monthly_Tartget(CustomerNum,Month,Year,Quantity,value,Status_SE,Status_BM,Status_HO)values(@CustomerNumber,@Month,@Year,@Quantity,@Value,@Status_SE,@Status_BM,@Status_HO);

End
else if(@role ='BM')
begin
set @Status_BM = 'Y';
set @Status_SE = 'Y';
insert into dbo.tbl_Monthly_Tartget(CustomerNum,Month,Year,Quantity,value,Status_SE,Status_BM,Status_HO)values(@CustomerNumber,@Month,@Year,@Quantity,@Value,@Status_SE,@Status_BM,@Status_HO);

End
else if(@role ='HO')
begin
set @Status_HO = 'Y';
insert into dbo.tbl_Monthly_Tartget(CustomerNum,Month,Year,Quantity,value,Status_SE,Status_BM,Status_HO)values(@CustomerNumber,@Month,@Year,@Quantity,@Value,@Status_SE,@Status_BM,@Status_HO);
END

SET @error_code=0
	SET @error_msg='inserted successfully.'
END
End



